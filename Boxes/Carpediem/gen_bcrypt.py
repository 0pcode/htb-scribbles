from bcrypt import gensalt, hashpw
from sys import argv


def gen_bcrypt_hash(password):
    password = password.encode()
    salt = gensalt(rounds=10, prefix=b'2b')
    return hashpw(password, salt)

if __name__ == "__main__":
    if len(argv) == 2:
        hashed = gen_bcrypt_hash(argv[1])
    elif len(argv) ==1:
        hashed = gen_bcrypt_hash('opcode')
    print(hashed.decode())
