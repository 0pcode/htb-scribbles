from Crypto.Cipher import AES
from Crypto.Protocol.KDF import PBKDF2
from Crypto.Util.Padding import unpad
import base64

def decrypt_string(encrypted_string):
    if not encrypted_string:
        return ""

    # return decrypt(encrypted_string, "N3st22", "88552299", 2, "464R5DFA5DL6LE28", 256)
    return decrypt(encrypted_string, "667912", "1313Rf99", 3, "1L1SA61493DRV53Z", 256)

def decrypt(cipher_text, pass_phrase, salt_value, password_iterations, init_vector, key_size):
    init_vector_bytes = init_vector.encode('ascii')
    salt_value_bytes = salt_value.encode('ascii')
    cipher_text_bytes = base64.b64decode(cipher_text)
    key_bytes = PBKDF2(pass_phrase, salt_value_bytes, dkLen=key_size // 8, count=password_iterations)

    cipher = AES.new(key_bytes, AES.MODE_CBC, init_vector_bytes)
    decrypted_bytes = cipher.decrypt(cipher_text_bytes)
    plain_text_bytes = unpad(decrypted_bytes, AES.block_size)
    plain_text = plain_text_bytes.decode('ascii')

    return plain_text

# encrypted_string = "fTEzAfYDoz1YzkqhQkH6GQFYKp1XY5hm7bjOP86yYxE="
encrypted_string = "yyEq0Uvvhq2uQOcWG8peLoeRQehqip/fKdeG/kjEVb4="
decrypted_string = decrypt_string(encrypted_string)
print(decrypted_string)
