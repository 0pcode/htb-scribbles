from Crypto.Cipher import DES
from binascii import unhexlify

ct = unhexlify('6bcf2a4b6e5aca0f')
key = unhexlify('e84ad660c4721ae0')

cipher = DES.new(key, DES.MODE_ECB)

pt = cipher.decrypt(ct)
print(pt.decode())
