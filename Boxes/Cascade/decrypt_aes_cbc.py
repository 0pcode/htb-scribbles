from Crypto.Cipher import AES
from base64 import b64decode

ct  = b64decode('BQO5l5Kj9MdErXx6Q6AGOw==')
key = b'c4scadek3y654321'
iv  = b'1tdyjCbY1Ix49842'

cipher = AES.new(key, AES.MODE_CBC, iv)

pt = cipher.decrypt(ct)
print(pt.decode())
