#!/usr/bin/python3
from re import search, sub


def _parseInt(s):
    n = search(r'\d+', s)
    return int(n[0])


def rotate(values):
    values.append(values.pop(0))
    return values


def lookup(i):
    return values[i-155]


values = ["EwCVL", "ugPGw", "Router is ", "-Bold", "data", "30158095HXLvSs", "post", "eAgent",
          "http://rou", "10BrHGoD", "gray", "80%", "applicatio", "white", "ck your in",
          "ternet con", "tb/api/v4/", "Please pro", "Image", "XvhFJ", "2111347AIyazK",
          "v/check/de", "vide an IP", "working fi", "DKyDg", "YnNsf", "tzoEq", "EKNxl",
          "the server", "log", "ne!.", "NunitoSans", "OgZoU", "TouchableO", "32457sfggQZ",
          "nection.", "[ RESPOND ", "center", "createElem", "__esModule", "per", "mGNnc",
          "then", "catch", "contain", "uAiCt", "bottom", "42740dmWhFN", "Text", "ButtonWrap",
          "OLDvc", "Sorry !", "terspace.h", "n/json", "StyleSheet", "/router/de", "darkgray",
          "JHvFI", "transparen", "UWIVj", "Please che", "SZqEq", "default", "HrHYj", "Hey !",
          "monitoring", "StatusBar", "error", "1013605BwxVJG", "[ DEBUG ] ", "defineProp",
          "gUnlE", "Unable to ", "25%", "pacity", "ButtonText", "gKQYs", "1006000MsdmAT",
          "handleSubm", "PpdRl", "shxxV", "ent", "View", "erty", "show", "Formik", "Check Stat",
          "0.0.0.0", "128BJBUSC", "6BAxhAU", "4584186MTHGwP", "connet to ", "vESlr", "GHjuW",
          " Address.", "container", "create", "RouterSpac", "viceAccess", "72dIvHGU", "info"]

replacables = ['_0x32f76d', '_0x36a162', '_0x8054be',
               '_0x3f41d2', '_0x155ca5', '_0x47a8f2']

while True:
    try:
        result = -_parseInt(lookup(208)) / 1 + _parseInt(lookup(228)) / 2 * (-_parseInt(lookup(174)) / 3) + \
            -_parseInt(lookup(239)) / 4 * (_parseInt(lookup(187)) / 5) + _parseInt(lookup(229)) / 6 * \
            (_parseInt(lookup(160)) / 7) + -_parseInt(lookup(217)) / 8 + -_parseInt(lookup(230)) / 9 + - \
            _parseInt(lookup(250)) / 10 * (-_parseInt(lookup(246)) / 11)
        if result == 548277.0:
            break
    except TypeError:
        rotate(values)
        continue

with open('540.js', 'r') as f:
    doc = f.read()


def re_operate(matchobj):
    m = int(matchobj.group(1))
    return '"' + lookup(m) + '"'


for i in replacables:
    pattern = f'{i}' + '\(([0-9]{2,3})\)'
    doc = sub(pattern, re_operate, doc)

with open('resolved.js', 'w') as g:
    g.write(doc)
