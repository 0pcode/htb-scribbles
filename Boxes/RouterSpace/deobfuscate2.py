#!/usr/bin/python3
from re import sub


with open('resolved.js', 'r') as f:
    doc = f.read()


str_dict = {
    'gUnlE': "info",
    'uAiCt': "Hey !",
    'PpdRl': "Router is working fine!.",
    'JHvFI': "[ DEBUG ] Router is working fine!.",
    'SZqEq': "[ RESPOND ] ",
    'EKNxl': "error",
    'DKyDg': "Unable to connet to the server !",
    'XvhFJ': "Please check your internet connection.",
    'shxxV': "[ DEBUG ] Please check your internet connection.",
    'mGNnc': "Sorry !",
    'HrHYj': "Please provide an IP Address.",
    'tzoEq': "[ DEBUG ] Please provide an IP Address.",
    'EwCVL': "http://routerspace.htb/api/v4/monitoring/router/dev/check/deviceAccess",
    'ugPGw': "RouterSpaceAgent",
    'UWIVj': "application/json",
    'OLDvc': "transparent",
    'gKQYs': "Check Status",
    'YnNsf': "bottom",
    'GHjuW': "0.0.0.0",
}

for i in str_dict:
    pattern = '_0x1f5205.' + i
    repl = '"' + str_dict[i] + '"'
    doc = sub(pattern, repl, doc)

with open('final.js', 'w') as g:
    g.write(doc)
