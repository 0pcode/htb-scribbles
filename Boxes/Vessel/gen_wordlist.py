from PySide2.QtCore import qrand, qsrand

charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890~!@#$%^&*()_-+={}[]|:;<>,.?'
passwords = []

for seed in range(1000):
    qsrand(seed)

    password = ''
    for i in range(32):
        idx = qrand() % len(charset)
        nchar = charset[idx]
        password += str(nchar)

    passwords.append(password)

with open('passwords.txt', 'w') as f:
    f.write('\n'.join(passwords))
