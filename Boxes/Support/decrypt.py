from base64 import b64decode
from itertools import cycle

ct = b'0Nv32PTwgYjzg9/8j5TbmvPd3e7WhtWWyuPsyO76/Y+U193E'
key = b'armando'

password = [chr(a ^ b ^ 223) for a, b in zip(b64decode(ct), cycle(key))]
print(''.join(password))
