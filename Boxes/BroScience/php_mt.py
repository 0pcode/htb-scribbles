from argparse import ArgumentParser


class Mersenne_Twister:
    def __init__(self, seed=None):
        self.state = [0] * 624
        self.index = 0

        if seed is None:
            seed = randrange(2**32)

        self.setSeed(seed)

    def setSeed(self, seed):
        self.state[0] = seed & 0xFFFFFFFF

        for i in range(1, 624):
            self.state[i] = (
                0x6C078965 * (self.state[i - 1] ^ (self.state[i - 1] >> 30)) + i
            ) & 0xFFFFFFFF

    def generateTwister(self):
        for i in range(624):
            y = (self.state[i] & 0x80000000) + (self.state[(i + 1) % 624] & 0x7FFFFFFF)
            self.state[i] = self.state[(i + 397) % 624] ^ (y >> 1)

            if y % 2 != 0:
                self.state[i] = self.state[i] ^ 0x9908B0DF

    def getNext(self, min_val=None, max_val=None):
        if min_val is not None and max_val is not None and min_val > max_val:
            raise ValueError("min_val must be less than or equal to max_val")

        if self.index == 0:
            self.generateTwister()

        y = self.state[self.index]
        y = y ^ (y >> 11)
        y = y ^ ((y << 7) & 0x9D2C5680)
        y = y ^ ((y << 15) & 0xEFC60000)
        y = y ^ (y >> 18)

        self.index = (self.index + 1) % 624

        if min_val is None and max_val is None:
            return y

        range_size = max_val - min_val + 1
        return min_val + (y % range_size)

if __name__ == "__main__":
    parser = ArgumentParser(
        add_help=True,
        description="Given a seed, return the same values as PHP's rand()",
    )
    parser.add_argument(
        "--seed",
        action="store",
        required=True,
        metavar="SEED",
        help="The seed to initialize with",
    )
    parser.add_argument(
        "--count",
        action="store",
        default=1,
        metavar="COUNT",
        help="Number of values to return",
    )
    parser.add_argument(
        "--lower",
        action="store",
        required=True,
        metavar="LOWER",
        help="The lower bound",
    )
    parser.add_argument(
        "--upper",
        action="store",
        required=True,
        metavar="UPPER",
        help="The upper bound",
    )

    options = parser.parse_args()
    seed, count, lower_bound, upper_bound = int(options.seed), int(options.count), int(options.lower), int(options.upper)

    mt = Mersenne_Twister(seed)

    for i in range(count):
        print(mt.getNext(lower_bound, upper_bound))
