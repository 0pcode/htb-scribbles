#!/usr/bin/env python3

import os.path
import sys
import uuid
import getpass
import typing as t
from flask import Flask, jsonify


app = Flask(__name__)


@app.route("/")
def pin_components():
    modname = getattr(app, "__module__", t.cast(object, app).__class__.__module__)

    bits = {}
    bits["username"] = getpass.getuser()
    bits["modname"] = modname
    bits["__name__"] = getattr(app, "__name__", type(app).__name__)
    bits["__file__"] = getattr(sys.modules.get(modname), "__file__", None)

    if os.path.isfile("/etc/machine-id"):
        machine_id = open("/etc/machine-id", "rb").readline().strip()
    else:
        machine_id = open("/proc/sys/kernel/random/boot_id", "rb").readline().strip()

    machine_id += open("/proc/self/cgroup", "rb").readline().strip().rpartition(b"/")[2]
    bits["machine_id"] = machine_id.decode()
    bits["mac"] = str(uuid.getnode())

    return jsonify(bits)


@app.route("/wheatley")
def divbyzero():
    return 1/0

if __name__ == "__main__":
    from werkzeug.debug import DebuggedApplication
    app.wsgi_app = DebuggedApplication(app.wsgi_app, True)
    app.debug = True
    app.run("0.0.0.0", 8000, debug=True)
