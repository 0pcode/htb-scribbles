from http.server import BaseHTTPRequestHandler, HTTPServer
from json import loads


def get_page(port):

    class WebhookHandler(BaseHTTPRequestHandler):
        def do_POST(self):
            content_length = int(self.headers['Content-Length'])
            post_data = self.rfile.read(content_length)
            post_data = loads(post_data.decode('utf-8'))
            contents = post_data['body']
            print(f'\u001b[31mRequest body:\u001b[0m\n\n{contents}')

    httpd = HTTPServer(('', port), WebhookHandler)

    return httpd


if __name__ == '__main__':
    httpd = get_page(8000)
    print('\u001b[32m[-] Listening...\u001b[0m')

    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
