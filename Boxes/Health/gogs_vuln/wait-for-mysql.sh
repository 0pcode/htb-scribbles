#!/bin/sh

set -e

host="$1"
# Shift arguments for `exec "$@"` below to work correctly
shift

until mysql -h "$host" --user=gogs --password=opcode -e 'exit'; do
  >&2 echo "Database is unavailable - sleeping"
  sleep 2
done

>&2 echo "Database is up - executing command"

exec "$@"