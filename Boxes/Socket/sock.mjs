import WebSocket from "ws";
import { createInterface } from "readline";

const rl = createInterface({ input: process.stdin, output: process.stdout });

async function main() {
  const ws = new WebSocket("ws://qreader.htb:5789/version");
  ws.on("open", () => {
    rl.question(">> ", (userInput) => {
      const payload = JSON.stringify({ version: userInput });
      ws.send(payload);
    });
  });

  ws.on("message", (response) => {
    console.log(JSON.parse(response).message.version);
    ws.close();
    setTimeout(main, 0);
  });
}

main();
