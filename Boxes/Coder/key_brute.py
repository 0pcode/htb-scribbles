from json import loads
from base64 import b64decode
from binascii import hexlify
from Crypto.Hash import MD5
from Crypto.Cipher import AES
from Crypto.Util.Padding import unpad


def EVP_BytesToKey(data, salt, key_len, iv_len):
    d = [b""]
    m = (key_len + iv_len + 15) // 16

    for _ in range(m):
        nd = MD5.new(d[-1] + data + salt).digest()
        d.append(nd)

    d = b"".join(d)

    return d[:key_len], d[key_len : key_len + iv_len]


def decrypt_totp_secret(secret_enc, key_enc, passphrase):
    key_salt = key_enc[8:16]
    key_enc = key_enc[16:]

    aes_key, aes_iv = EVP_BytesToKey(passphrase, key_salt, 32, 16)
    # print(hexlify(aes_key), hexlify(aes_iv))
    cipher = AES.new(aes_key, AES.MODE_CBC, aes_iv)
    key = cipher.decrypt(key_enc)
    key = unpad(key, 16)
    # print(hexlify(key))

    secret_salt = secret_enc[8:16]
    secret_enc = secret_enc[16:]

    aes_key, aes_iv = EVP_BytesToKey(hexlify(key), secret_salt, 32, 16)
    cipher = AES.new(aes_key, AES.MODE_CBC, aes_iv)
    secret = cipher.decrypt(secret_enc)
    secret = unpad(secret, 16)

    return secret.decode()


if __name__ == "__main__":
    with open("authenticator.json", "r") as f:
        json_data = loads(f.read())

    with open("/home/opcode/CTF/wordlists/rockyou.txt", "rb") as f:
        wordlist = f.read().split(b"\n")

    key_enc = json_data["key"]["enc"]
    secret_enc = json_data[list(json_data.keys())[0]]["secret"]

    key_enc = b64decode(key_enc)
    secret_enc = b64decode(secret_enc)

    for passphrase in wordlist:
        try:
            secret = decrypt_totp_secret(secret_enc, key_enc, passphrase)
        except ValueError:
            continue
        else:
            print(f"Passphrase: {passphrase.decode()}")
            print(f"TOTP secret: {secret}")
            break
