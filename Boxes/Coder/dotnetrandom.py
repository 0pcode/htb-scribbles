from math import fmod
from Crypto.Cipher import AES
from Crypto.Util.Padding import unpad


def int32(x):
    x = x & 0xFFFFFFFF
    return (x ^ 0x80000000) - 0x80000000


class Random:
    def __init__(self, seed):
        self.inext = 0
        self.inextp = 21
        self.SeedArray = [0] * 56
        self.MBIG = 2**31 - 1
        self.MSEED = 161803398

        subtraction = self.MBIG if seed == -(self.MBIG + 1) else abs(int32(seed))
        mj = self.MSEED - subtraction
        self.SeedArray[55] = mj

        mk = 1
        for i in range(1, 55):
            ii = (21 * i) % 55
            self.SeedArray[ii] = int32(mk)
            mk = mj - mk
            mk = mk + self.MBIG if mk < 0 else mk
            mj = self.SeedArray[ii]

        for _ in range(4):
            for i in range(1, 56):
                self.SeedArray[i] -= self.SeedArray[(i + 30) % 55 + 1]
                self.SeedArray[i] = int32(self.SeedArray[i])

                if self.SeedArray[i] < 0:
                    self.SeedArray[i] += self.MBIG

    def InternalSample(self):
        locINext = self.inext
        locINextp = self.inextp

        locINext = 1 if (locINext + 1 >= 56) else locINext + 1
        locINextp = 1 if (locINextp + 1 >= 56) else locINextp + 1

        retVal = self.SeedArray[locINext] - self.SeedArray[locINextp]

        retVal = retVal - 1 if retVal == self.MBIG else retVal
        retVal = retVal + self.MBIG if retVal < 0 else retVal

        self.SeedArray[locINext] = retVal
        self.inext = locINext
        self.inextp = locINextp

        return retVal

    def NextBytes(self, length):
        buffer = []
        for i in range(length):
            buffer.append(int(fmod(self.InternalSample(), 256)))

        return bytes(buffer)


if __name__ == "__main__":
    with open("s.blade.enc", "rb") as f:
        encrypted = f.read()

    r = Random(1668205028)
    iv = r.NextBytes(16)
    key = r.NextBytes(32)

    cipher = AES.new(key, AES.MODE_CBC, iv)
    plaintext = cipher.decrypt(encrypted)
    plaintext = unpad(plaintext, 16)

    with open("s.blade", "wb") as f:
        f.write(plaintext)
