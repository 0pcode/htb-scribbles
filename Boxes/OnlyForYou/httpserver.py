from socket import socket, AF_INET, SOCK_STREAM


def get_queries(path):
    data = {}
    queries = path.split('&')
    for query in queries:
        key, value = query.split('=')
        data[key] = value

    return data


def draw_table(data_dict):
    max_key_length = max(len(str(key)) for key in data_dict.keys())
    max_value_length = max(len(str(value)) for value in data_dict.values())

    top_border = f'┌{"─" * (max_key_length + 2)}┬{"─" * (max_value_length + 2)}┐'
    middle_border = f'├{"─" * (max_key_length + 2)}┼{"─" * (max_value_length + 2)}┤'
    bottom_border = f'└{"─" * (max_key_length + 2)}┴{"─" * (max_value_length + 2)}┘'

    table = [top_border,
             f'│ Key{" " * (max_key_length - 3)} │ Value{" " * (max_value_length - 5)} │',
             middle_border]

    for key, value in data_dict.items():
        table.append(f'│ {str(key).ljust(max_key_length)} │ {str(value).ljust(max_value_length)} │')

    table.append(bottom_border)

    return '\n'.join(table)


def print_path(clientsocket):
    request_line = clientsocket.recv(1024).split(b'\r\n')[0]
    path = request_line.split(b'/')[1].rstrip(b' HTTP').decode()
    if path.startswith('?'):
        print(draw_table(get_queries(path[1:])))
    else:
        print(path)

    response = 'HTTP/1.1 200 OK\r\nContent-Length: 0\r\n\r\n'
    clientsocket.send(response.encode())
    clientsocket.close()


def listener(host, port):
    serversocket = socket(AF_INET, SOCK_STREAM)
    serversocket.bind((host, port))
    serversocket.listen(1)

    print(f'\033[93m[-] Listener started\033[0m')

    while True:
        clientsocket, address = serversocket.accept()
        print_path(clientsocket)


if __name__ == '__main__':
    listener('', 8000)
