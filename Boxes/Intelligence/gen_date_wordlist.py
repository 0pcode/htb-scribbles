from datetime import datetime, timedelta


def generate_filenames(start_date, end_date):
    current_date = start_date
    while current_date <= end_date:
        yield f"{current_date.strftime('%Y-%m-%d')}-upload.pdf"
        current_date += timedelta(days=1)


start_date = datetime(2019, 1, 1)
end_date = datetime(2021, 12, 31)

with open('wordlist.txt', 'w') as f:
    for filename in generate_filenames(start_date, end_date):
        f.write(filename + '\n')
