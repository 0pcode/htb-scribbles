from io import BytesIO
from pypdf import PdfReader
import requests


def parse_pdf_content(url):
    r = requests.get(url)
    pdf_handle = BytesIO(r.content)
    reader = PdfReader(pdf_handle)

    contents = ''
    for i in reader.pages:
        contents += i.extract_text()

    return contents


def detect_sensitive_information(data):
    if 'intelligence' in data.lower():
        return True

    return False


if __name__ == "__main__":
    proxy = {}
    # proxy['http'] = 'http://127.0.0.1:8080'

    with open('valid.txt', 'r') as f:
        valid_urls = f.read().split()

    for url in valid_urls:
        contents = parse_pdf_content(url)
        if detect_sensitive_information(contents):
            print(contents)
