# HTB Scribbles

Write-ups for a few Machines and Challenges from HTB.

## Tid-bits

| # | Name | Difficulty | OS | Released on |
| --- | --- | --- | --- | --- |
| 3 | [Trickster](/Boxes/Trickster/README.md) | Medium | Linux | 21 Sep 2024 |
| 2 | [Gofer](/Boxes/Gofer/README.md) | Hard | Linux | 29 Jul 2023 |
| 1 | [Agile](/Boxes/Agile/README.md) | Medium | Linux | 04 Mar 2023 |

## Boxes

| # | Name | Difficulty | OS | Released on |
| --- | --- | --- | --- | --- |
| 71 | [Cicada](/Boxes/Cicada/README.md) | Easy | Windows | 28 Sep 2024 |
| 70 | [Compiled](/Boxes/Compiled/README.md) | Medium | Windows | 27 Jul 2024 |
| 69 | [Blazorized](/Boxes/Blazorized/README.md) | Hard | Windows | 29 Jun 2024 |
| 68 | [Axlle](/Boxes/Axlle/README.md) | Hard | Windows | 22 Jun 2024 |
| 67 | [Freelancer](/Boxes/Freelancer/README.md) | Hard | Windows | 01 Jun 2024 |
| 66 | [SolarLab](/Boxes/SolarLab/README.md) | Medium | Windows | 11 May 2024 |
| 65 | [Runner](/Boxes/Runner/README.md) | Medium | Linux | 20 Apr 2024 |
| 64 | [Mist](/Boxes/Mist/README.md) | Insane | Windows | 30 Mar 2024 |
| 63 | [Jab](/Boxes/Jab/README.md) | Medium | Windows | 24 Feb 2024 |
| 62 | [Office](/Boxes/Office/README.md) | Hard | Windows | 17 Feb 2024 |
| 61 | [Pov](/Boxes/Pov/README.md) | Medium | Windows | 27 Jan 2024 |
| 60 | [Bizness](/Boxes/Bizness/README.md) | Easy | Linux | 06 Jan 2024 |
| 59 | [Hospital](/Boxes/Hospital/README.md) | Medium | Windows | 18 Nov 2023 |
| 58 | [Manager](/Boxes/Manager/README.md) | Medium | Windows | 21 Oct 2023 |
| 57 | [Visual](/Boxes/Visual/README.md) | Medium | Windows | 30 Sep 2023 |
| 56 | [Rebound](/Boxes/Rebound/README.md) | Insane | Windows | 09 Sep 2023 |
| 55 | [Authority](/Boxes/Authority/README.md) | Medium | Windows | 15 Jul 2023 |
| 54 | [Sau](/Boxes/Sau/README.md) | Easy | Linux | 08 Jul 2023 |
| 53 | [Pilgrimage](/Boxes/Pilgrimage/README.md) | Easy | Linux | 24 Jun 2023 |
| 52 | [PC](/Boxes/PC/README.md) | Easy | Linux | 20 May 2023 |
| 51 | [MonitorsTwo](/Boxes/MonitorsTwo/README.md) | Easy | Linux | 29 Apr 2023 |
| 50 | [OnlyForYou](/Boxes/OnlyForYou/README.md) | Medium | Linux | 22 Apr 2023 |
| 49 | [Busqueda](/Boxes/Busqueda/README.md) | Easy | Linux | 08 Apr 2023 |
| 48 | [Coder](/Boxes/Coder/README.md) | Insane | Windows | 01 Apr 2023 |
| 47 | [Socket](/Boxes/Socket/README.md) | Easy | Linux | 25 Mar 2023 |
| 46 | [Cerberus](/Boxes/Cerberus/README.md) | Hard | Windows | 18 Mar 2023 |
| 45 | [Escape](/Boxes/Escape/README.md) | Medium | Windows | 25 Feb 2023 |
| 44 | [Bagel](/Boxes/Bagel/README.md) | Medium | Linux | 18 Feb 2023 |
| 43 | [Interface](/Boxes/Interface/README.md) | Medium | Linux | 11 Feb 2023 |
| 42 | [Stocker](/Boxes/Stocker/README.md) | Easy | Linux | 14 Jan 2023 |
| 41 | [BroScience](/Boxes/BroScience/README.md) | Medium | Linux | 07 Jan 2023 |
| 40 | [Soccer](/Boxes/Soccer/README.md) | Easy | Linux | 17 Dec 2022 |
| 39 | [Flight](/Boxes/Flight/README.md) | Hard | Windows | 05 Nov 2022 |
| 38 | [MetaTwo](/Boxes/MetaTwo/README.md) | Easy | Linux | 29 Oct 2022 |
| 37 | [Ambassador](/Boxes/Ambassador/README.md) | Medium | Linux | 01 Oct 2022 |
| 36 | [Absolute](/Boxes/Absolute/README.md) | Insane | Windows | 24 Sep 2022 |
| 35 | [Vessel](/Boxes/Vessel/README.md) | Hard | Linux | 27 Aug 2022 |
| 34 | [Health](/Boxes/Health/README.md) | Medium | Linux | 20 Aug 2022 |
| 33 | [Support](/Boxes/Support/README.md) | Easy | Windows | 30 Jul 2022 |
| 32 | [Shared](/Boxes/Shared/README.md) | Medium | Linux | 23 Jul 2022 |
| 31 | [RedPanda](/Boxes/RedPanda/README.md) | Easy | Linux | 09 Jul 2022 |
| 30 | [Faculty](/Boxes/Faculty/README.md) | Medium | Linux | 02 Jul 2022 |
| 29 | [Carpediem](/Boxes/Carpediem/README.md) | Hard | Linux | 25 Jun 2022 |
| 28 | [Scrambled](/Boxes/Scrambled/README.md) | Medium | Windows | 11 Jun 2022 |
| 27 | [StreamIO](/Boxes/StreamIO/README.md) | Medium | Windows | 04 Jun 2022 |
| 26 | [OpenSource](/Boxes/OpenSource/README.md) | Easy | Linux | 21 May 2022 |
| 25 | [Late](/Boxes/Late/README.md) | Easy | Linux | 23 Apr 2022 |
| 24 | [Talkative](/Boxes/Talkative/README.md) | Hard | Linux | 09 Apr 2022 |
| 23 | [Retired](/Boxes/Retired/README.md) | Medium | Linux | 02 Apr 2022 |
| 22 | [Timelapse](/Boxes/Timelapse/README.md) | Easy | Windows | 26 Mar 2022 |
| 21 | [RouterSpace](/Boxes/RouterSpace/README.md) | Easy | Linux | 26 Feb 2022 |
| 20 | [Undetected](/Boxes/Undetected/README.md) | Medium | Linux | 19 Feb 2022 |
| 19 | SteamCloud | Easy | Linux | 14 Feb 2022 |
| 18 | [NodeBlog](/Boxes/NodeBlog/README.md) | Easy | Linux | 10 Jan 2022 |
| 17 | [Return](/Boxes/Return/README.md) | Easy | Windows | 27 Sep 2021 |
| 16 | [Intelligence](/Boxes/Intelligence/README.md) | Medium | Windows | 03 Jul 2021 |
| 15 | [Delivery](/Boxes/Delivery/README.md) | Easy | Linux | 09 Jan 2021 |
| 14 | [Academy](/Boxes/Academy/README.md) | Easy | Linux | 07 Nov 2020 |
| 13 | [Blackfield](/Boxes/Blackfield/README.md) | Hard | Windows | 06 Jun 2020 |
| 12 | [Cascade](/Boxes/Cascade/README.md) | Medium | Windows | 28 Mar 2020 |
| 11 | [Sauna](/Boxes/Sauna/README.md) | Easy | Windows | 15 Feb 2020 |
| 10 | [Nest](/Boxes/Nest/README.md) | Easy | Windows | 25 Jan 2020 |
| 9 | [Monteverde](/Boxes/Monteverde/README.md) | Medium | Windows | 11 Jan 2020 |
| 8 | [Resolute](/Boxes/Resolute/README.md) | Medium | Windows | 07 Dec 2019 |
| 7 | [Traverxec](/Boxes/Traverxec/README.md) | Easy | Linux | 16 Nov 2019 |
| 6 | [Forest](/Boxes/Forest/README.md) | Easy | Windows | 12 Oct 2019 |
| 5 | [Help](/Boxes/Help/README.md) | Easy | Linux | 19 Jan 2019 |
| 4 | [Active](/Boxes/Active/README.md) | Easy | Windows | 28 Jul 2018 |
| 3 | [Bank](/Boxes/Bank/README.md) | Easy | Linux | 16 Jun 2017 |
| 2 | [Devel](/Boxes/Devel/README.md) | Easy | Windows | 15 Mar 2017 |
| 1 | [Beep](/Boxes/Beep/README.md) | Easy | Linux | 15 Mar 2017 |

## Challenges

| # | Name | Category | Points |
| --- | ---  | --- | --- |
| 1 | [RsaCtfTool](/Challenges/RsaCtfTool/RsaCtfTool.pdf) | Crypto | 20 |
| 2 | Protein Cookies | Crypto | 20 |
| 3 | mysterybox | Crypto | 30 |
